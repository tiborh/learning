\babel@toc {british}{}\relax 
\contentsline {chapter}{\chapternumberline {1}Tutorial: Forest Calendar}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Documentclass}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Font settings}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}June}{4}{section.1.3}%
\contentsline {section}{\numberline {1.4}July}{5}{section.1.4}%
\contentsline {section}{\numberline {1.5}August}{6}{section.1.5}%
\contentsline {section}{\numberline {1.6}The document}{7}{section.1.6}%
\contentsline {chapter}{\chapternumberline {2}Tutorial: Translations}{8}{chapter.2}%
\contentsline {section}{\numberline {2.1}Files}{8}{section.2.1}%
\contentsline {section}{\numberline {2.2}Translations setup}{9}{section.2.2}%
\contentsline {section}{\numberline {2.3}Document setup}{10}{section.2.3}%
\contentsline {chapter}{\chapternumberline {3}Tutorial: Load Events from CSV}{14}{chapter.3}%
\contentsline {section}{\numberline {3.1}CSV files}{14}{section.3.1}%
\contentsline {section}{\numberline {3.2}Event formatting}{14}{section.3.2}%
\contentsline {section}{\numberline {3.3}Document setup}{15}{section.3.3}%
\contentsline {chapter}{\chapternumberline {4}Example: Year Planner Page}{16}{chapter.4}%
\contentsline {section}{\numberline {4.1}Document setup}{16}{section.4.1}%
\contentsline {section}{\numberline {4.2}\textbackslash YearPlannerPage}{16}{section.4.2}%
\contentsline {section}{\numberline {4.3}Use it}{17}{section.4.3}%
\contentsline {chapter}{\chapternumberline {5}Example: Photo Thumbnails Page}{18}{chapter.5}%
\contentsline {section}{\numberline {5.1}Document setup}{18}{section.5.1}%
\contentsline {section}{\numberline {5.2}\textbackslash ThumbWithCaptionLeftSide}{18}{section.5.2}%
\contentsline {section}{\numberline {5.3}\textbackslash ThumbWithCaptionRightSide}{19}{section.5.3}%
\contentsline {section}{\numberline {5.4}\textbackslash ThumbsPage}{20}{section.5.4}%
\contentsline {section}{\numberline {5.5}Setup the photo keys}{21}{section.5.5}%
\contentsline {section}{\numberline {5.6}Use it}{22}{section.5.6}%
\contentsline {chapter}{\chapternumberline {6}Documentclass Options}{22}{chapter.6}%
\contentsline {chapter}{\chapternumberline {7}User Commands}{22}{chapter.7}%
\contentsline {chapter}{\chapternumberline {8}Page Layout}{24}{chapter.8}%
\contentsline {chapter}{\chapternumberline {9}Contact}{24}{chapter.9}%
\contentsfinish 
