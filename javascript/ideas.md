# Ideas #

## Improve Existing ##

1. pattern logic drawer:
	* window width for pattern space? (window resize does not count)
    	* cannot yet, best: manual resize (Done)
    * some-preset options (selectable with radio buttons)
    * save and load formulas
    * more flexible formula editing (editing the middle?)
    * starting line series generation (e.g. increments, ranges))
2. smiley face: change params with sliders (and then redraw)
	* zoom
	* vertical/horizontal distortion
	* eye-size, distance between eyes
	* smile form
	* rotation
3. mouseover demo:
	* add touch Listener too (use: canvas_draw_with_mouse.html) (no success: checked these:
		[touchmovements](http://www.javascriptkit.com/javatutors/touchevents.shtml "touchmovements"))
4. draw shapes:
	* merge with pattern logic drawer: random squares, ellipses, etc.
5. colour coding:
    * letters to colour (done)
    * and back (not yet)
6. sine (other functions) page rewrite:
    * slider for amplitude/wavelength/point-density changes 
    * turn on/off graphs
    * set point size
    * data-points as tooltips

## Possible New Targets ##

1. bouncing ball (+ other physics)
2. favicon editor (dot drawing)
3. hand drawing
4. animation
5. use canvas for page decoration: 
    * header, side bars, footer?
    * gradient can be put to good use
    * random shapes can be useful
    * pattern logic -> Paint can be useful.


# Sources of Information #

1. [classes](https://developer.mozilla.org/en-us/docs/web/javascript/reference/classes "Classes (MDN)")
2. [canvas_api_tutorial](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial "Canvas Tutorial (MDN)")
3. [canvas_starter](https://codeburst.io/creating-and-drawing-on-an-html5-canvas-using-javascript-93da75f001c1 "creating and drawing on an html5 canvas using javascript")
4. [canvas_boilerplate](https://medium.com/dev-compendium/setup-a-canvas-starter-boilerplate-4f9188238f34 "setup a canvas starter boilerplate")
5. [game_development](https://medium.com/dev-compendium/game-development-with-javascript-ed8b302756bf "Canvas and Game Development with JavaScript")
6. [ball_animation](https://medium.com/dev-compendium/creating-a-bouncing-ball-animation-using-javascript-and-canvas-1076a09482e0 "Creating a Bouncing Ball Animation Using JavaScript and Canvas")
7. [canvas_reference](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D "CanvasRenderingContext2D")
