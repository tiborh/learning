class Graph {
#nu_vertices;
#nu_edges;
#li_adj;
#routes;
#shortest_route;
#shortest_length;
    static #asep = ":";
    static #lsep = ",";
    static #ssep = " ";
    static #endl = "\n";
    static #quot = '"';
    
    constructor(vertices) {
	this.#nu_vertices = vertices;
	this.#nu_edges = 0;
	this.#li_adj = [];
	this.#shortest_length = this.#nu_vertices;
	this.#shortest_route = [];
	for (let i = 0; i < this.#nu_vertices; ++i)
	    this.#li_adj.push([]);
    }
    from_file(fn) {
	try {
	    // read contents of the file
	    const data = fs.readFileSync(fn, 'UTF-8');
	    
	    // split the contents by new line
	    const lines = data.split(/\r?\n/);
	    
	    this.#nu_vertices = Number(lines[0].trim());
	    this.#shortest_length = this.#nu_vertices;
	    this.#nu_edges = Number(lines[1].trim());

	    for(let i = 0; i < this.#nu_vertices; ++i)
		this.#li_adj.push([]);
	    for(let i = 2; i < lines.length; ++i) {
		const edge = lines[i].split(" ");
		if (edge.length != 2)
		    continue;
		this.#li_adj[Number(edge[0])].push(Number(edge[1]));
		this.#li_adj[Number(edge[1])].push(Number(edge[0]));
	    }
	    
	} catch (err) {
	    console.error(err);
	}
    }

    nu_edges() { return this.#nu_edges; }
    nu_vertices() { return this.#nu_vertices; }
    get_shortest() { return this.#shortest_route; }

    add_edge(v1,v2) {
	this.add_uni_edge(v1,v2);
	if (v1 != v2)
	    this.add_uni_edge(v2,v1);
    }
    rem_edge(v1,v2) {
	this.rem_uni_edge(v1,v2);
	if (v1 != v2)
	    this.rem_uni_edge(v2,v1);
    }
    add_uni_edge(v1,v2) {
	this.#li_adj[v1].push(v2);
	++this.#nu_edges;
    }
    rem_uni_edge(v1,v2) {
	this.#li_adj[v1].splice(this.#li_adj[v1].indexOf(v2),1);
	--this.#nu_edges;
    }
    get_adj(v) {
	return this.clone_array(this.#li_adj[v]);
	//return this.#li_adj[v]; // to avoid stack overflow
    }
    clone_array(arr) {
	const cloned_arr = []
	for(let it of arr)
	    cloned_arr.push(it);
	return(cloned_arr);
    }
    find_route(from,to,shortest,route) {
	//console.log("from: " + from + " to " + to + " (route so far: " + route + ")");
	if(route.includes(from))
	    return;
	route.push(from);
	if (shortest && route.length >= this.#shortest_length) {
	    //console.log("discarded:");
	    //console.log(route);
	    return;
	}
	//console.log("pushed: " + route);
	if (route.includes(to)) {
	    //console.log("pushing to routes: " + route);
	    if (shortest) {
		this.#shortest_route = route;
		this.#shortest_length = route.length;
		console.log("shortest route so far:");
		console.log(this.#shortest_route);
	    } else 
		this.#routes.push(route);
	    return;
	}
	for (let vertex of this.#li_adj[from]) {
	    //console.log("vertex: " + vertex);
	    const a_clone_route = this.clone_array(route);
	    this.find_route(vertex,to,shortest,a_clone_route);
	}
	return;
    }
    find_routes(f,t,shortest) {
	this.#routes = [];
	if (f >= this.#li_adj.length || t >= this.#li_adj.length)
	    return;
	this.find_route(f,t,shortest,[]);
    }
    static print_routes(g) {
	for(let a_route of g.#routes)
	    console.log(a_route);
    }
    static is_edge(g,v1,v2) {
	return g.#li_adj[v1].indexOf(v2) > -1;
    }
    static degree(g,v) { return g.#li_adj[v].length; }
    static max_degree(g) {
	let max_deg = 0;
	for (let i = 0; i < g.#nu_vertices; ++i) {
	    const d = this.degree(g,i);
	    if (d > max_deg)
		max_deg = d;
	}
	return max_deg;
    }
    static ave_degree(g) { return 2.0 * g.#nu_edges / g.#nu_vertices; }
    static nu_self_loops(g) {
	let counter = 0;
	for(let i = 0; i < g.#nu_edges; ++i) {
	    if (g.#li_adj[i] === undefined)
		continue;
	    for(let it of g.#li_adj[i])
		if (i == it)
		    ++counter;
	}
	return counter;
    }
    static to_str(g) {
	let out_str = "";
	for(let i = 0; i < g.#nu_vertices; ++i)
	    out_str += i + this.#asep + g.#li_adj[i] + this.#endl;
	return out_str;
    }
    static to_edge_list(g) {
	const out_arr = [];
	for(let i = 0; i < g.#nu_vertices; ++i) {
	    const len = g.#li_adj[i].length;
	    for (let j = 0; j < len; ++j) {
		const edge = [i,g.#li_adj[i][j]];
		out_arr.push(edge);
	    }
	}
	return(out_arr);
    }
    static to_file(g,fn) {
	let writeStream = fs.createWriteStream(fn);
	let edge_list = this.to_edge_list(g);
	// write some data

	for(let it of edge_list) {
	    writeStream.write(this.#quot + it[0] + this.#quot + this.#lsep + this.#quot + it[1] + this.#quot + this.#endl);
	}
	
	// the finish event is emitted when all data has been flushed from the stream
	writeStream.on('finish', () => {
	    console.log('data written to: ' + fn);
	});
	
	// close the stream
	writeStream.end();
    }
    static print_adj(g) {
	console.log(this.to_str(g));
    }
    static print_edge_list(g) {
	const edge_list = this.to_edge_list(g);
	for(let it of edge_list) {
	    console.log(this.#quot + it[0] + this.#quot + this.#lsep + this.#quot + it[1] + this.#quot);
	}
    }
}

class DepthFirstSearchIterative {
#marked;
#count;
#stack;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#stack = [];
	this.#stack.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    while(this.#stack.length > 0) {
	const vertex = this.#stack.pop();
	if(this.#marked[vertex])
	    continue;
	this.#marked[vertex] = true;
	++this.#count;
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w])
		this.#stack.push(w);
    }
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class BreadthFirstSearchIterative {
#marked;
#count;
#queue;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#queue = [];
	this.#queue.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    while(this.#queue.length > 0) {
	const vertex = this.#queue.shift();
	if(this.#marked[vertex])
	    continue;
	this.#marked[vertex] = true;
	++this.#count;
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w])
		this.#queue.push(w);
    }
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class DepthFirstSearchRecursive {
#marked;
#count;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#dfs(g,s);
    }
#dfs(g,v) {
    this.#marked[v] = true;
    ++this.#count;
    const adj = g.get_adj(v);
    for(let w of adj)
	if (!this.#marked[w])
	    this.#dfs(g,w);
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class BreadthFirstSearchRecursive {
#marked;
#count;
#queue;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#queue = [];
	this.#marked[s] = true;
	this.#queue.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    if(this.#queue.length == 0)
	return;
    const vert = this.#queue.shift();
    ++this.#count;
    const adj = g.get_adj(vert);
    for(let w of adj)
	if (!this.#marked[w]) {
	    this.#marked[w] = true;
	    this.#queue.push(w);
	}
    this.#dfs(g);
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

