  function add_elem_to(elem,attribs,values,parent) {
      const e = document.createElement(elem);
      for (let i = 0; i < attribs.length; ++i)
          e.setAttribute(attribs[i],values[i]);
      document.querySelector(parent).appendChild(e);
      return(e);
  }
  function init_page() {
      const author = "tibor";
      const description = "template";
      const style = "styles/style.css";
      const default_text = "Defer";
      const favicon = "img/favicon.png"
      document.title = default_text;
      add_elem_to("link",["rel","href"],["stylesheet",style],"head");
      add_elem_to("link",["rel","href","type"],["icon",favicon,"image/png"],"head");
      add_elem_to("meta",["charset"],["utf-8"],"head");
      add_elem_to("meta",["author"],[author],"head");
      add_elem_to("meta",["description"],[description],"head");
      document.querySelector("h1").innerText = default_text;
      const p = document.querySelector("p");
      p.innerText = default_text;
      p.setAttribute("style","color: red");
  }
  init_page();