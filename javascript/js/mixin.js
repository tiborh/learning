let target = { numa: 3 };
let source = { numb: 7 };

console.log("before mixin:");
console.log("source:");
console.log(source);
console.log("target:");
console.log(target);

Object.assign(target, source);

console.log("after mixin:");
console.log("target:");
console.log(target);

let target2 = {numb: 2 };

console.log("before mixin:");
console.log("target2:");
console.log(target2);

Object.assign(target2, source);

console.log("after mixin:");
console.log("target2:");
console.log(target2);

console.log("Multiple mixin:");

const Duck = {
  hasBill: true
};
console.log("Duck:");
console.log(Duck);

const Beaver = {
  hasTail: true
};
console.log("Beaver:");
console.log(Beaver);

const Otter = {
  hasFur: true,
  feet: 'webbed'
};
console.log("Otter:");
console.log(Otter);

const Platypus = Object.assign({}, Duck, Beaver, Otter);

console.log("Platypus:");
console.log(Platypus);
console.log("Constructor:");
console.log(Platypus.constructor);

const Platypus2 = Object.assign(Duck, Beaver, Otter);
console.log("Platypus2:");
console.log(Platypus2);
console.log("Duck:");
console.log(Duck);

console.log(`Is Duck prototype of Platypus2? ${Duck.isPrototypeOf(Platypus2)}`);
console.log("Is Platypus2 identical with Duck?");
console.log(Platypus2 === Duck);
//console.log(Platypus2 instanceof Duck); right side is not callable

