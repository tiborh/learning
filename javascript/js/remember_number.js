function remember(n) {
    return function() {
        return n++;
    }
}

const returned_function = remember(0);
for (let i = 0; i < 5; i++) {
    console.log(returned_function());
}
