//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
let original = new Map([
  [1, 'one']
])

let clone = new Map(original)

console.log(clone.get(1))       // one
console.log(original === clone) // false (useful for shallow comparison)
