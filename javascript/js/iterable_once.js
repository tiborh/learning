function* makeIterator() {
    yield 1;
    yield 2;
}

const it = makeIterator();

console.log(it[Symbol.iterator]() === it)
//once iterable:
for (const itItem of it) {
    console.log(itItem);
}
//trying second time:
for (const itItem of it) {
    console.log(itItem);
}
// nothing happens
console.log(it[Symbol.iterator]() === it)
console.log(it);
console.log(Symbol.iterator);
