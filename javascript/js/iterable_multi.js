function* makeIterator() {
    yield 1;
    yield 2;
}

const it = makeIterator();

//make it iterable multiple times:
it[Symbol.iterator] = function* () {
  yield 2;
  yield 1;
};

console.log(it[Symbol.iterator]() === it)
//once iterable:
for (const itItem of it) {
    console.log(itItem);
}
//trying second time:
for (const itItem of it) {
    console.log(itItem);
}
console.log(it[Symbol.iterator]() === it);
console.log(it);

