#!/usr/bin/env node
class Matrix {
#m;
#s;
#full;
#empty;
    constructor(num,ini=" ",full="x",empt=" ") {
	this.#s = num;
	this.#m = Array(num).fill().map(() => Array(num).fill(ini));
	this.#full = full;
	this.#empty = empt;
    }
    place_dot(n=Math.floor(this.#s/2)) {
	for (let i = 0; i < this.#s; ++i) {
	    this.#m[0][i] = (i == n) ? this.#full : this.#empty;
	}
    }
    place_dots(nums) {
	for(let i in nums) {
	    //console.log("i == " + i);
	    this.#m[0][nums[i]] = this.#full;
	}
    }
    reset() {
	for (let i = 0; i < this.#s; ++i) {
	    this.#m[0][i] = this.#empty;
	}
    }
    get size() { return this.#s; }
    get m() { return this.#m; }
    get empty() { return this.#empty; }
    get full() { return this.#full; }
    static crawler(rule,m) {
	for(let i = 1; i < m.size; ++i) {
	    for(let j = 0; j < m.size; ++j) {
		if (rule(i,j,m)) {
		    m.m[i][j] = m.full;
		} else {
		    m.m[i][j] = m.empty;
		}
	    }
	}
    }
    to_str() {
	let out = "";
	for(let i of this.#m) {
	    for(let j of i) {
		out = out.concat(j);
	    }
	    out = out.concat('\n');
	}
	return out;
    }
    print() {
	for(let i of this.#m) {
	    for(let j of i) {
		process.stdout.write(j);
		process.stdout.write(' ');
	    }
	    process.stdout.write('\n');
	}
    }
}



function tester1(rule,size) {
    let m = new Matrix(size);
    //m.print();
    m.place_dot();
    //m.print();
    Matrix.crawler(rule,m);
    m.print();
}

function tester2(rule,size,starting) {
    let m = new Matrix(size);
    //m.print();
    m.place_dots(starting);
    //m.print();
    Matrix.crawler(rule,m);
    m.print();
}

function stester(rule,size,starting) {
    let m = new Matrix(size);
    //m.print();
    m.place_dots(starting);
    //m.print();
    Matrix.crawler(rule,m);
    console.log(m.to_str());
}


const dot = 'x';

const rule1 = function(x,y,m) { return false; }; // test rule: all blank
const rule2 = function(x,y,m) { return true; };	 // rest rule: all filled
const rule3 = function(x,y,m) {
    return(m.m[x-1][y] == dot);	// only if horizontally above
}
const rule4 = function(x,y,m) {
    return(m.m[x-1][y+1] == dot); // only if upper left
}
const rule5 = function(x,y,m) {
    return(m.m[x-1][y-1] == dot || m.m[x-1][y] == dot); // upper left or directly above
}
const rule6 = function(x,y,m) {
    return(m.m[x-1][y-1] == dot); // only if upper right
}
const rule7 = function(x,y,m) {
    return(m.m[x-1][y+1] == dot || m.m[x-1][y] == dot); // upper right or directly above
}
const rule8 = function(x,y,m) {
    return(m.m[x-1][y-1] == dot || m.m[x-1][y+1] == dot); // upper left or upper right
}

// repeated:
//  #
// ###
const rule9 = function(x,y,m) {
    return((m.m[x-1][y-1] == dot || m.m[x-1][y] == dot || m.m[x-1][y+1] == dot)  && !((m.m[x-1][y-1] == dot && m.m[x-1][y] == dot) || (m.m[x-1][y] == dot && m.m[x-1][y+1] == dot)));
    // one of the three neighbouring above but neither of the diagonal ones together with the middle one
}

// fractal!
const rule10 = function(x,y,m) {
    return((m.m[x-1][y-1] == dot || m.m[x-1][y] == dot || m.m[x-1][y+1] == dot)  && !((m.m[x-1][y-1] == dot && m.m[x-1][y] == dot) || (m.m[x-1][y] == dot && m.m[x-1][y+1] == dot) || (m.m[x-1][y-1] == dot && m.m[x-1][y+1] == dot)));
    // one of the three neighbouring above but none of the pairs together
}

// fractal!
const rule11 = function(x,y,m) {
    return((m.m[x-1][y-1] == dot || m.m[x-1][y+1] == dot) && !(m.m[x-1][y-1] == dot && m.m[x-1][y+1] == dot));
    // upper left or upper right, but not both
}

// fractal!!
const rule12 = function(x,y,m) {
    return((m.m[x-1][y-1] == dot || m.m[x-1][y+1] == dot || m.m[x-1][y] == dot) && !(m.m[x-1][y-1] == dot && m.m[x-1][y+1] == dot && m.m[x-1][y] == dot));
    // upper left or upper right or directly above, but not all three at the same time
}
// to do:
// 1. make more pieces placeable in row 0 (vector input in place of scalar)
// 2. do away with the _ init (' ' is enough)
// 3. see the bottom with three starting points (two at each end and third in the middle
// 4. invent new rules:
//    e.g. add odd/even rules,
//          or create some interference in more boring forms,
//          or playing with all or two empty above.



stester(rule12,158,[0,39,78,117,157]);		// the bottom part with these settings looks promising

