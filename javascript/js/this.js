let die = {
    sides: 6,
    colour: 'white',
    dot_colour: 'black',
    roll: function() {
	return(Math.floor((Math.random() * this.sides) + 1));
    }
}

console.log(die);
console.log(die.roll());
