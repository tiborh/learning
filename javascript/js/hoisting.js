a = 12
console.log("a == " + a);
console.log("b == " + b); // undefined
var b = 10;
var a;
// this is called hoisting:
//  	javascript moves up declarations
//	but not definitions.
try {
    console.log("c == " + c); // this leads to error: not declared
} catch(e) {
    console.log("Error message: " + e.message);
    console.log("Full error: " + e);
}
