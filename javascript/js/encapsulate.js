let a = function(starting_state=0) {
    let internal_var = starting_state;
    return function() {
	return internal_var;
    }
};

console.log(a);
console.log(a());
console.log(a()());

a = (function(starting_state=0) {
    let internal_var = starting_state;
    return function() {
	return internal_var;
    }
})();

console.log(a);
console.log(a());

let counter = (function(starting_state=0) {
    let internal_var = starting_state;
    return function() {
	return internal_var++;
    }
})();

console.log("Counter;");
for (let i = 0; i < 5; ++i) {
    console.log(counter());
}

counter = (function() {
    let _starter = 0;
    let _internal_var = _starter;
    function _incr() {
	return _internal_var++;
    }
    function _reset(starter = _starter) {
	_starter = starter;
	_internal_var = _starter;
	return true;
    }
    function _peek() {
	return _internal_var;
    }
    return function() {
	return {
	    count: _incr,
	    peek:  _peek,
	    reset: _reset
	};
    }
})();

console.log(counter);
let c = new counter();
c.reset(1);
console.log(c);
console.log("Peek:");
console.log(c.peek());
console.log("Count;");
for (let i = 0; i < 5; ++i) {
    console.log(c.count());
}
console.log("Reset:");
console.log(c.reset());
console.log("Peek:");
console.log(c.peek());
