const driver = {
  name: 'Danica',
  displayName: function () {
    console.log(`Name: ${this.name}`);
  }
};

const car = {
  name: 'Fusion'
};

console.log("first: call()");
driver.displayName.call(car,driver.displayName)
console.log("then: bind()");
const disp_car_name = driver.displayName.bind(car);
disp_car_name();
