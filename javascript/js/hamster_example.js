// https://classroom.udacity.com/courses/ud711/lessons/7a95cd0f-752d-422e-b5a4-af8ddeaca0aa/concepts/ab0184cb-2341-4ece-91f9-8c1a971769d0

function Hamster() {
  this.hasFur = true;
}

let waffle = new Hamster();
let pancake = new Hamster();

Hamster.prototype.eat = function () {
  console.log('Chomp chomp chomp!');
};

waffle.eat();
// 'Chomp chomp chomp!'

pancake.eat();
// 'Chomp chomp chomp!'

Hamster.prototype = {
  isHungry: false,
  color: 'brown'
};

console.log(waffle.color);
// undefined

waffle.eat();
// 'Chomp chomp chomp!'

console.log(pancake.isHungry);
// undefined

const muffin = new Hamster();
console.log(muffin);

try {
    muffin.eat();
    // TypeError: muffin.eat is not a function
} catch(e) {
    console.log(e.message);
}

console.log(muffin.isHungry);
// false

console.log(muffin.color);
// 'brown'

