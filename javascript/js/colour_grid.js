#!/usr/bin/env node

let b = 0;
for(let r = 0; r < 255; r+=16) {
    for(let g = 0; g < 255; g+=16) {
	process.stdout.write(`rgb(${r},${g},${b++})`);
    }
    process.stdout.write('\n');
}
