const building = {
    floors: 5,
    addFloor: function () {
	this.floors += 1;
    },
    print_this: function() {
	console.log(this);
    }
};

console.log("floors: " + building.floors);
building.addFloor();
console.log("floors: " + building.floors);
building.print_this();
