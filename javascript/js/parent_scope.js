b = "bang";
function a_parent(msg = 1) {
    let a = 2
    function a_child() {
        console.log("msg == " + msg);
        console.log("  a == " + a);
        console.log("  b == " + b);
    }
    a_child();
}

a_parent("hello");

var b;