const dave = {
  name: 'Dave'
};

function say_hello(message) {
  console.log(`${message}, ${this.name}. You're looking well today.`);
}

console.log("What would calling the function directly say?");
say_hello("hello");

console.log("Now with call, and including 'dave':");
say_hello.call(dave,"hello");
