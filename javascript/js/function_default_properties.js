function add(a,b) {
    return(a + b);
}

console.log(`name:   ${add.name}`);
console.log(`length: ${add.length}`);
console.log(`prototype: ${add.prototype}`);
console.log(add.prototype);

function Adder(a,b) {
    this.a =  a;
    this.b = b;
    // this.adder = function() {
    // 	return(this.a + this.b);
    // }
}

Adder.prototype.adder = function() {
    return(this.a + this.b);
}

console.log(`prototype: ${Adder.prototype}`);
console.log(Adder.prototype);
const a = new Adder(1,2);
console.log(Object.getPrototypeOf(a));
console.log(a);
console.log(a.adder());

console.log("Alternative way:");
let A = {
    constructor: function setup(a,b) {
	this.first = a;
	this.second = b;
    },
    adder: function inner_adder(a,b) {
	return(this.first + this.second);
    }
};

console.log(A);
const b = Object.create(A);
console.log(b);
b.constructor(2,3);
console.log(b);
console.log(`b.adder: ${b.adder()}`);

// this too:
// const b = Object.create(Adder);
// console.log(b);
// console.log(b.adder());
