class Point {
    constructor(x, y, c="black") {
	this.x = x;
	this.y = y;
	this.c = c;
    }

    static distance(a, b) {
	const dx = a.x - b.x;
	const dy = a.y - b.y;
	return Math.hypot(dx, dy);
    }
}

class Canvas {
#width = 0;
#height = 0;
#colour = "";
#points = [];
    constructor(w,h,c="#EBF6F7"){ // 藍白
	this.#width = w;
	this.#height = h;
	this.#colour = c;
    }
    log() {
	console.log("canvas:");
	console.log(`   width: ${this.#width}`);
	console.log(`  height: ${this.#height}`);
	console.log(`  colour: ${this.#colour}`);
    }
    set width(w)  { this.#width =  w; }
    set height(h) { this.#height = h; }
    set colour(c) { this.#colour = c; }
    set_size (w,h) {
	this.#width = w;
	this.#height = h;
    }
    get width()  { return this.#width;  }
    get height() { return this.#height; }
    get colour() { return this.#colour; }
    get size()   { return [this.#width,
			   this.#height]; }
}

c = new Canvas(300,200);
c.log();
console.log(c.size);
c.set_size(400,600);
c.log();
