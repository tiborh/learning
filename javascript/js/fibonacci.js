// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
function* fibonacci() {
  let current = 0;
  let next = 1;
  while (true) {
    let reset = yield current;
    [current, next] = [next, next + current];
    if (reset) {
        current = 0;
        next = 1;
    }
  }
}

const f = fibonacci();
console.log(f);
for(let i = 0; i < 10; ++i) {
    console.log(f.next().value);
}
console.log("reset:");
console.log(f.next(true).value);
for(let i = 0; i < 9; ++i) {
    console.log(f.next().value);
}
