function invoke_thrice(cb) {
    cb();
    cb();
    cb();
}

const cat = {
    age: 2,
    grow_one_year: function () {
	this.age++;
    },
    print_age: function() {
	console.log("age: " + this.age);
    }
};

console.log("behaviour of 'invoke_thrice'");
invoke_thrice(function() { console.log("invoked"); });
console.log("cat experiments:");
cat.print_age();
cat.grow_one_year();
cat.print_age();
console.log("now invoke thrice through binding");
const bound_grow = cat.grow_one_year.bind(cat);
invoke_thrice(bound_grow);
cat.print_age();
