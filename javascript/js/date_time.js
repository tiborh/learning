#!/usr/bin/env node
console.log("system date/time:");
console.log(new Date());
console.log("local date/time:");
console.log(new Date().toLocaleString());
