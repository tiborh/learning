function average(n1, n2, n3) {
    return((n1+n2+n3)/3);
}
console.log("function as an object:");
console.log("average.length: " + average.length);
console.log("average.name: " + average.name);

const nums = [1,2,3];

const ave = function(func,arr) {
    console.log("the numbers: " + arr);
    return(func(arr[0],arr[1],arr[2]));
}
result = ave(average,nums);
console.log("the result: " + result);

const ave2 = function(msg,func) {
    console.log(msg);
    console.log("func name: " + func.name);
    return(function(arr) {
	return(func(arr[0],arr[1],arr[2]));
    });
}

const msg = "Passing average as arg";
cl = ave2(msg,average);
console.log("executing assigned closure with nums array: " + cl(nums));
console.log("in one step:");
console.log(ave2(msg,average)(nums));
