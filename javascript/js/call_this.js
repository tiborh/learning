const ivanhoe = {
    title: 'Ivanhoe',
    type: 'historical',
    describe: function () {
	console.log(`${this.title} is a ${this.type} novel.`);
    }
};

ivanhoe.describe();

const pride = {
    title: 'Pride and Prejudice',
    type: 'classic'
};

ivanhoe.describe.call(pride);
