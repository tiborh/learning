//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
let contacts = new Map();
contacts.set('Jessie', {phone: "213-555-1234", address: "123 N 1st Ave"});
contacts.has('Jessie'); // true
contacts.get('Hilary'); // undefined
contacts.set('Hilary', {phone: "617-555-4321", address: "321 S 2nd St"});
contacts.get('Jessie'); // {phone: "213-555-1234", address: "123 N 1st Ave"}
console.log("Print the object directly:");
console.log(contacts);
console.log("Trying to imitate output with a loop:");
for (let [key, value] of contacts) {
    console.log(key + ' = ' + value);
}
console.log("a better way:");
for (let [key, value] of contacts) {
    console.log(key + ':');
    v_arr = [ value ];
    console.log("    phone: " + value.phone);
    console.log("  address: " + value.address);
}
console.log("an alternative way:");
for (let [key, value] of contacts) {
    console.log(key + ':');
    v_arr = [ value ];
    const a_contact = v_arr.map(function(hash) { return("  phone: " + hash.phone + "\n  address: " + hash.address); });
    console.log(a_contact[0]);
}
contacts.delete('Raymond'); // false
contacts.delete('Jessie'); // true
console.log("Contacts size: " + contacts.size); // 1

// further alternatives:
// for (let key of myMap.keys()) {
//   console.log(key)
// }

// for (let value of myMap.values()) {
//   console.log(value)
// }

// for (let [key, value] of myMap.entries()) {
//   console.log(key + ' = ' + value)
// }

// myMap.forEach(function(value, key) {
//   console.log(key + ' = ' + value)
// })
