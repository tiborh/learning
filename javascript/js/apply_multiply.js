function multiply(n1, n2) {
  return n1 * n2;
}

console.log("3 * 5 == " + multiply(3,5));

//console.log("4 * 6 == " + multiply.call(global,4,6));
console.log("4 * 6 == " + multiply.apply(global,[4,6]));
