function Animal(name, age, weight, word) {
    this.name = name;
    this.age = age;
    this.weight = weight;
    this.word = word;
}

// for multiple objects, same one function
Animal.prototype.says = function () {
    console.log(`${this.name} says '${this.word}'.`);
};

const d = new Animal("Dave", 3, 23, "woof");
console.log(d);
d.says();

console.log("prototype replacement:");

Animal.prototype.says = function () {
    console.log(`${this.word}, ${this.word}, ${this.word}`);
};

const b = new Animal("Chirpy", 1, 0.05, "tweet");
console.log(b);

b.says();
d.says();

Animal.prototype = {
    is_angry: false,
    colour: 'brown'
}

let s = new Animal('Slyther',5,2,'Sssss');
console.log(s);
console.log(s.is_angry);
console.log(s.colour);
console.log(s.says);

console.log(`is name   its own property? ${s.hasOwnProperty('name')}`);
console.log(`is colour its own property? ${s.hasOwnProperty('colour')}`);

function Cat() {
    this.fav_place = "sofa";
}

Cat.prototype = Animal;

const kate = new Cat();
console.log(kate);
console.log(`is Animal the prototype of kate? ${Animal.isPrototypeOf(kate)}`);
console.log(`The prototype of kate is: ${Object.getPrototypeOf(kate)}`);
console.log("What's kate's constructor?");
console.log(kate.constructor);
console.log("What about 's'?");
console.log(s.constructor);
console.log("And d?");
console.log(d.constructor);
console.log("And b?");
console.log(b.constructor);
