function print_odd(num) {
    if((num % 2) !== 0) {
	console.log(num);
    }
}
sample_arr = [1,2,3,4,5,6,7,8,9,10];
console.log("Original: " + sample_arr);
console.log("odd ones: ");
sample_arr.forEach(print_odd);

function is_odd(num) { return((num % 2) !== 0); }
console.log("Mapped with is_odd:");
console.log(sample_arr.map(is_odd));

console.log("multiplication with map:");
console.log(sample_arr.map(function(num) {return num * 2;}));

console.log("Filter with is_odd:");
console.log(sample_arr.filter(is_odd));
