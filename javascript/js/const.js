a = 10;
b = 11;
console.log("a + b == " + (Number(a)+Number(b)));
try {
    const b = 12;
} catch(e) {
    console.log(e.message);
}
/*
try {
    const a; // SyntaxError: Missing initializer in const declaration
} catch(e) {
    console.log(e.message);
}
*/