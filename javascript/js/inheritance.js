function GuineaPig (name) {
  this.name = name;
  this.isCute = true;
}

const waffle = new GuineaPig('Waffle');

console.log(waffle);
console.log(GuineaPig.__proto__);
console.log(waffle.__proto__);


function Car (color, year) {
  this.color = color;
  this.year = year;
}

Car.prototype.drive = function () {
  console.log('Vroom vroom!');
};

const car = new Car('silver', 1988);
car.drive();

const mammal = {
  vertebrate: true,
  earBones: 3
};
const rabbit = Object.create(mammal);
console.log(rabbit);
console.log(rabbit.__proto__);
// lost interest. this old way of inheritence is too murky to go into. better waY:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes

