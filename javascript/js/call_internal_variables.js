function invoke_thrice(cb) {
    cb();
    cb();
    cb();
}

const cat = {
    age: 2,
    grow_one_year: function () {
	this.age++;
    },
    print_age: function() {
	console.log("age: " + this.age);
    }
};

console.log("behaviour of 'invoke_thrice'");
invoke_thrice(function() { console.log("invoked"); });
console.log("cat experiments:");
cat.print_age();
cat.grow_one_year();
cat.print_age();
console.log("now invoke thrice + call");
invoke_thrice.call(cat,cat.grow_one_year);
cat.print_age();
console.log("A way to work around is passing an anonymous function:");
invoke_thrice(function() { cat.grow_one_year(); });
cat.print_age();
// see bind() for another way to solve the same.
