class Die {
#sides;
#colour;
#dot_colour;
    constructor(sides=6,colour='white',dot_colour='black') {
	this.#sides = sides;
	this.#colour = colour;
	this.#dot_colour = dot_colour;
    }
    get sides() { return this.#sides; }
    get colour() { return this.#colour; }
    get dot_colour() { return this.#dot_colour; }
    set colour(c) { this.#colour = c; }
    set dot_colour(c) { this.#dot_colour = c; }
    roll() {
	return(Math.floor((Math.random() * this.sides) + 1));
    }
    *nrolls(num) {
	for(let i = 0; i < num; ++i){
	    yield this.roll();
	}
    }
    get properties() {
	return( { sides: this.#sides,
		  colour: this.#colour,
		  dot_colour: this.#dot_colour} );
    }
}
console.log("print class:");
console.log(Die);
console.log("typeof Die: " + typeof(Die)); 

const d = new Die();
console.log("print its instance:");
console.log(d);
console.log("typeof d: " + typeof(d)); 
console.log("is d an instance of Die? ");
console.log(d instanceof Die);

console.log("call and print properties:");
console.log(d.properties);

const num_of_rolls = 96;
console.log("Let's make a few rolls ("+num_of_rolls+"):");
const rolls = [];
for(let i = 0; i < 96; ++i) {
    rolls.push(d.roll());
}
console.log(rolls);
console.log("Another way:");
console.log([...d.nrolls(num_of_rolls)]);
console.log("Generation one by one:");
for(const i of d.nrolls(10)) {
    console.log("Roll: " + i);
}
