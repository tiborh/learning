function Die(sides=6,colour='white',dot_colour='black') {
    this.sides = sides;
    this.colour = colour;
    this.dot_colour = dot_colour;
    this.roll = function() {
	return(Math.floor((Math.random() * this.sides) + 1));
    }
}

console.log("print class:");
console.log(Die);
const die1 = new Die();
console.log("print its instance:");
console.log(die1);
const die2 = Die();
console.log("trying to create without 'new' keyword: " + die2);
const num_of_rolls = 96;
console.log("Let's make a few rolls ("+num_of_rolls+"):");
const rolls = [];
for(let i = 0; i < num_of_rolls; ++i) {
    rolls.push(die1.roll());
}
console.log(rolls);

console.log("What if small letter?");
function die(sides=6,colour='white',dot_colour='black') {
    this.sides = sides;
    this.colour = colour;
    this.dot_colour = dot_colour;
    this.roll = function() {
	return(Math.floor((Math.random() * this.sides) + 1));
    }
    this.isthis = function() {
	console.log(this);
    }
}

console.log(die)
const d = new die();
console.log(d);
console.log("is d an instance of Die? ");
console.log(d instanceof Die);
console.log("printing 'this':");
d.isthis();
console.log("typeof die: " + typeof(die)); 
console.log("typeof d: " + typeof(d)); 

