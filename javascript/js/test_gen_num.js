#!/usr/bin node

let max_num = 4;
let min_num = 2;

function gen_num1(nmax=max_num,nmin=min_num) {
    return(Math.floor((Math.random() * nmax) + nmin));
}

function gen_num2(nmax=max_num,nmin=min_num) {
    return(Math.floor((Math.random() * (nmax-nmin+1)) + nmin));
}

function tester(func,times=100) {
    for (let i = 0; i < times; ++i) {
	process.stdout.write(func().toString());
    }
}

tester(gen_num1);
console.log("");
tester(gen_num2);
console.log("");
