//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
function* makeRangeIterator(start = 0, end = 100, step = 1) {
    let iterationCount = 0;
    for (let i = start; i < end; i += step) {
        iterationCount++;
        yield i;
    }
    return iterationCount;
}

for (const i of makeRangeIterator(1,10,2)) {
    console.log(i);
}

console.log("The other example: ");
const it = makeRangeIterator(1, 15, 3);
let result = it.next();
while (!result.done) {
 console.log(result.value);
 result = it.next();
}

console.log("Iterated over sequence of size: ", result.value);