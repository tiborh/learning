var a = "hello";
let b = "john";
let counter = 5;

console.log(a + " " + b);

if (counter > 3) {
    a = "hi";
    b = "james";
    console.log(a + " " + b);
}

console.log(a + " " + b);

var a = "bonjour";
try {
    let b = "richard"; // SyntaxError: Identifier 'b' has already been declared
} catch(e) {
    console.log(e.message);
}

b = "pierre";

console.log(a + " " + b);
