#!/usr/bin/env node

const fs = require("fs");
const ifn = "japanese.txt"
const ofn = "streams_pipe.txt";

// Create a readable stream
const readerStream = fs.createReadStream(ifn);

// Create a writable stream
const writerStream = fs.createWriteStream(ofn);

// Pipe the read and write operations
// read input.txt and write data to output.txt
readerStream.pipe(writerStream);
console.log("Stream has been piped from " + ifn + " into " + ofn);

console.log("__END__");

