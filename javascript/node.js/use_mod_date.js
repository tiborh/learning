#!/usr/bin/env node

const http = require('http');
const dt = require('./mod_date');

const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("Curent date and time:<br />" + dt.show_date_time());
    res.end("<br />__END__");
}).listen(portnu);
