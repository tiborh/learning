#!/usr/bin/env node

console.log("__BEGIN__");
const net = require('net');
const portnu = 8888;
console.log("Listening on port: " + portnu);

const server = net.createServer(socket => {
    socket.once('data', data => {
	socket.write(data); // only once
    });
    socket.on('data', data => {
	process.stdout.write(data); // writes to console every single char
    });
});
server.listen(portnu);
console.log("__END__");
