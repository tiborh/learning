#!/usr/bin/env node

main();

function main() {
    const yargs = require('yargs');
    const argv = yargs
	  .argv;
    
    function usage(){
	console.log("Usage:");
	console.log("    " + argv["$0"] + " <input string>");
    }

    if (argv["_"].length == 0) {
	// usage();
	// process.exit();
	pipe_handling();
    }

    for(it of argv["_"]) {
	proc_str(it);
    }
}

function proc_str(str) {
    // letters = str.split("");
    console.log("");
    const len = str.length;
    for (let i = 0; i <  len; ++i) {
	console.log(str[i] + ": " + str.charCodeAt(i).toString().padStart(3,'0') + "\t0x" + str.charCodeAt(i).toString(16).padStart(2,'0'));
    }
}

function pipe_handling() {
    const stdin = process.openStdin();
    let data = "";

    stdin.on('data', function(chunk) {
	data += chunk;
    });

    stdin.on('end', function() {proc_str(data);});
}
