#!/usr/bin/env node

main();

function main() {
    const yargs = require('yargs');
    const argv = yargs
	  .usage('Usage:\n  $0 <input string>')
	  .help()
	  .argv;
    
    if (argv["_"].length == 0) {
	pipe_handling();
	//process.exit();
    }

    for(it of argv["_"]) {
	proc_str(it);
    }
}

function proc_str(str) {
    const out_codes = Array();
    const len = str.length;
    let a_code = "#";
    for (let i = 0; i <  len; ++i) {
	let a_char = str.charCodeAt(i).toString(16).padStart(2,'0');
	if(a_code.length + a_char.length > 7) {
	    a_code = a_code.padEnd(7,'0');
	    out_codes.push(a_code);
	    a_code = "#";
	}
	a_code = a_code.concat(a_char);
	console.log(a_char);
	console.log(a_code);
    }
    if (a_code.length > 1) {
	a_code = a_code.padEnd(7,'0');
	out_codes.push(a_code);
    }
    console.log(out_codes);
    return(out_codes);
}

function pipe_handling() {
    const stdin = process.openStdin();
    let data = "";

    stdin.on('data', function(chunk) {
	data += chunk;
    });

    stdin.on('end', function() {proc_str(data);});
}
