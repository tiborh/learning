module.exports = function(n,d) {
    let name = n;
    let descr = d;

    return {
	set_name: function(a_name) {
	    name = a_name;
	},
	set_descr: function(a_descr) {
	    descr = a_descr;
	},
	get_data: function() {
	    return {
		name: name,
		descr: descr
	    };
	}
    };
};
