#!/usr/bin/env node

const http = require('http');
const portnu = 1234;
const whost = "localhost";
const connect = require('connect');
//const _end = "__END__";

const app = connect().use(function(req,res){
    if(req.url == "/hello") {
	console.log("sending plain txt");
	res.end("Hello from app");
    } else if(req.url == "/printRequestHeaders") {
	const headers = req.headers;
	console.log("echoing headers");
	console.log(headers);
	res.end("Headers printed in console");
    } else {
	console.log("sending 404 status code");
	res.statusCode = 404;
	res.end("404: Try hello or printRequestHeaders");
    }
}).listen(portnu);
console.log("Listening on port: " + whost + ":" + portnu);
