#!/usr/bin/env node

const http = require('http');
const fs = require('fs');
const addr = '127.0.0.1';
const port = 8000;
const html_tmpl = './data/template.html';
const json_inpt = './data/poem.json';
const server = http.createServer((req, res) => {
    getTitles(res);
});
server.listen(port, addr, () => {
    console.log('Server listening on: http://%s:%s', addr, port);
});

function getTitles(res) {
    fs.readFile(json_inpt, (err, data) => {
	if (err) return hadError(err, res);
	getTemplate(JSON.parse(data.toString()), res);
    });
}

function getTemplate(titles, res) {
    fs.readFile(html_tmpl, (err, data) => {
	if (err) return hadError(err, res);
	formatHtml(titles, data.toString(), res);
    });
}

function formatHtml(titles, tmpl, res) {
    const html = tmpl.replace('%', titles.join("</li><li>"));
    res.writeHead(200, { 'Content-Type': 'text/html'});
    res.end(html);
}

function hadError(err, res) {
    console.error(err);
    res.end('Server Error');
}

