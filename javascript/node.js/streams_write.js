#!/usr/bin/env node

const fs = require("fs");
const fn = 'streams_out.txt';
const data = '野島が初めて杉子に会ったのは帝劇の二階の正面の廊下だった。野島は脚本家をもって私ひそかに任じてはいたが、芝居を見る事は稀まれだった。此日も彼は友人に誘われなければ行かなかった。誘われても行かなかったかも知れない。その日は村岡の芝居が演やられるので、彼はそれを読んだ時から閉口していたから。然し友達の仲田に勧められると、ふと行く気になった。それは杉子も一緒に行くと聞いたので。\n';

// Create a writable stream
const writerStream = fs.createWriteStream(fn);

// Write the data to stream with encoding to be utf8
writerStream.write(data,'UTF8');

// Mark the end of file
writerStream.end();

// Handle stream events --> finish, and error
writerStream.on('finish', function() {
   console.log("data written to " + fn);
});

writerStream.on('error', function(err) {
   console.log(err.stack);
});

console.log("__END__");
