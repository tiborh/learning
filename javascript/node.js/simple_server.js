#!/usr/bin/env node

const http = require('http');
const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World!');

}).listen(portnu);
