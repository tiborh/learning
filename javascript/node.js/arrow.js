#!/usr/bin/env node

const len = 10;
const sequence = [...Array(len).keys()]; // convert iterable to array
console.log("original:");
console.log(sequence);
const multip = 2;
console.log("multiplied:");
console.log(sequence.map(n => n * multip));
console.log("filtered:");
console.log(sequence.map(n => n * multip).filter(n => n >= len));
console.log("__END__");

