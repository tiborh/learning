#!/usr/bin/env node

console.log("__BEGIN__");
const net = require('net');
const portnu = 8888;
console.log("Listening on port: " + portnu);

const server = net.createServer(socket => {
 socket.on('data', data => {
     //socket.write(data); // repeats every single char in telnet session
     process.stdout.write(data); // writes to console once enter is pressed
 });
});
server.listen(portnu);
console.log("__END__");
