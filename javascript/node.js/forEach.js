#!/usr/bin/env node

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map

const path = require('path');
const gen = require('./mod_gen_arr');
const num =  5;
const max = 10;
const min =  0;
const num_of_nums = process.argv[2] === undefined ? num : Number(process.argv[2]);
const max_num = process.argv[3] === undefined ? max : Number(process.argv[3]);
const min_num = process.argv[4] === undefined ? min : Number(process.argv[4]);
const usage = `Usage:\n\t${path.basename(process.argv[1])} [num_of_nums (default: ${num})] [max_num (exclusive) (default: ${max})] [min_num (inclusive) (default: ${min})]`;
const array1 = gen.nums(num_of_nums,max_num,min_num);
const array2 = gen.nums(num_of_nums,max_num,min_num);

if (process.argv[2] === undefined)
    console.info(usage);
console.log("__BEGIN__");
console.log("Starting with:");
console.log(array1);
console.log("Twice:");
array1.forEach(function(x,i,arr){ arr[i] = 2 * x; });
console.log(array1);

console.log("index added:");
array1.forEach(function(x,i,arr){ arr[i] = x + i; });
console.log(array1);

console.log("plus previous:");
array1.forEach((x,ind,arr) => arr[ind] = ind === 0 ? x : x + arr[ind-1]);
console.log(array1);

array1.forEach(function(x,ind,arr) { arr[ind] = x+this[ind]; }, array2);
console.log("add the following:");
console.log(array2);
console.log("result:");
console.log(array1);

console.log("testing async funcs:");
let sum = 0;
array1.forEach(async function(x) { sum += x; });
console.log(`sum === ${sum} (internal func only)`);
async function add(a,b) { return a + b; }
sum = 0;
array1.forEach(async function(x) { sum = await add(sum,x); });
console.log(`sum === ${sum} (external func call)`);

console.log("for Counter object:");
function Counter() {
  this.sum = 0
  this.count = 0
}
Counter.prototype.add = function(array) {
  array.forEach(function countEntry(entry) {
    this.sum += entry
    ++this.count
  }, this)
}

const obj = new Counter()
obj.add(array1)
console.log(`Count: ${obj.count}`);
console.log(`Sum:   ${obj.sum}`);
//array1.forEach( x => {console.log(this);}); // {}

console.log("copy implementation:");
function copy(obj) {
    const copy = Object.create(Object.getPrototypeOf(obj))
    const propNames = Object.getOwnPropertyNames(obj)

    propNames.forEach(function(name) {
	const desc = Object.getOwnPropertyDescriptor(obj, name)
	Object.defineProperty(copy, name, desc)
    })

    return copy;
}

const hash = gen.alphHash(0,5);
console.log(hash);
const hash_copy = copy(hash);
hash_copy['a']=-1;
console.log("copy, changed:");
console.log(hash_copy);
console.log("original (unchanged):");
console.log(hash);

function flatten(arr) {
  const result = []

  arr.forEach(function(i) {
    if (Array.isArray(i)) {
      result.push(...flatten(i))
    } else {
      result.push(i)
    }
  })

  return result
}
const nested = [1, 2, 3, [4, 5, [6, 7], 8, 9]];
console.log("nested:");
console.log(nested);
console.log("flattened:");
console.log(flatten(nested));

console.log("__END__");
