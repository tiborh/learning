#!/usr/bin/env -S node --stack-size=8192 --harmony

const fs = require('fs');
const yargs = require('yargs');

class Graph {
#digraph;
#nu_vertices;
#nu_edges;
#li_adj;
    static #asep = ":";
    static #lsep = ",";
    static #ssep = " ";
    static #endl = "\n";
    static #quot = '"';
    
    constructor(vertices,digraph) {
	this.#digraph = digraph;
	this.#nu_vertices = vertices;
	this.#nu_edges = 0;
	this.#li_adj = new Array();
	for (let i = 0; i < this.#nu_vertices; ++i)
	    this.#li_adj.push(new Array());
    }
    from_file(fn) {
	try {
	    // read contents of the file
	    const data = fs.readFileSync(fn, 'UTF-8');
	    
	    // split the contents by new line
	    const lines = data.split(/\r?\n/);
	    
	    this.#nu_vertices = Number(lines[0].trim());
	    this.#nu_edges = Number(lines[1].trim());

	    for(let i = 0; i < this.#nu_vertices; ++i)
		this.#li_adj.push([]);
	    for(let i = 2; i < lines.length; ++i) {
		const edge = lines[i].split(" ");
		if (edge.length != 2)
		    continue;
		this.#li_adj[Number(edge[0])].push(Number(edge[1]));
		this.#li_adj[Number(edge[1])].push(Number(edge[0]));
	    }
	    
	} catch (err) {
	    console.error(err);
	}
    }

    nu_edges() { return this.#nu_edges; }
    nu_vertices() { return this.#nu_vertices; }

    add_edge(v1, v2) {
	if(v1 == v2)
	    return false;
	if(this.#li_adj.length > v1) {
	    if(this.#li_adj[v1].includes(v2) || this.#li_adj[v2].includes(v1))
		return false;
	    this.#li_adj[v1].push(v2);
	} else {
	    console.error("out of range: " + v1);
	    process.exit(1);
	}
	if(this.#digraph) {
	    if (v1 != v2) {
		if (this.#li_adj.length > v2)
		    this.#li_adj[v2].push(v1);
		else {
		    console.error("out of range: " + v2);
		    process.exit(1);
		}
	    }
	}
	++this.#nu_edges;
	return true;
    }
    get_adj(v) {
	return this.clone_array(this.#li_adj[v]);
	//return this.#li_adj[v]; // to avoid stack overflow
    }
    clone_array(arr) {
	const cloned_arr = []
	for(let it of arr)
	    cloned_arr.push(it);
	return(cloned_arr);
    }
    static is_edge(g,v1,v2) {
	return g.#li_adj[v1].includes(v2);
    }
    static degree(g,v) { return g.#li_adj[v].length; }
    static max_degree(g) {
	let max_deg = 0;
	for (let i = 0; i < g.#nu_vertices; ++i) {
	    const d = this.degree(g,i);
	    if (d > max_deg)
		max_deg = d;
	}
	return max_deg;
    }
    static ave_degree(g) { return 2.0 * g.#nu_edges / g.#nu_vertices; }
    static nu_self_loops(g) {
	let counter = 0;
	for(let i = 0; i < g.#nu_edges; ++i) {
	    if (g.#li_adj[i] === undefined)
		continue;
	    for(let it of g.#li_adj[i])
		if (i == it)
		    ++counter;
	}
	return counter;
    }
    static to_str(g) {
	let out_str = "";
	for(let i = 0; i < g.#nu_vertices; ++i)
	    out_str += i + this.#asep + g.#li_adj[i] + this.#endl;
	return out_str;
    }
    static to_edge_list(g) {
	const out_arr = [];
	for(let i = 0; i < g.#nu_vertices; ++i) {
	    const len = g.#li_adj[i].length;
	    for (let j = 0; j < len; ++j)
		    out_arr.push([i,g.#li_adj[i][j]]);
	}
	return(out_arr);
    }
    static to_sfile(g,fn) {
	let writeStream = fs.createWriteStream(fn);
	let edge_list = this.to_edge_list(g);
	// write some data
	writeStream.write(g.nu_vertices() + this.#endl);
	writeStream.write(g.nu_edges() + this.#endl);
	for(let it of edge_list) {
	    writeStream.write(it[0] + this.#ssep + it[1] + this.#endl);
	}
	
	// the finish event is emitted when all data has been flushed from the stream
	writeStream.on('finish', () => {
	    console.log('data written to: ' + fn);
	});
	
	// close the stream
	writeStream.end();
    }
    static to_file(g,fn) {
	let writeStream = fs.createWriteStream(fn);
	let edge_list = this.to_edge_list(g);
	// write some data

	for(let it of edge_list) {
	    writeStream.write(this.#quot + it[0] + this.#quot + this.#lsep + this.#quot + it[1] + this.#quot + this.#endl);
	}
	
	// the finish event is emitted when all data has been flushed from the stream
	writeStream.on('finish', () => {
	    console.log('data written to: ' + fn);
	});
	
	// close the stream
	writeStream.end();
    }
    static print_adj(g) {
	console.log(this.to_str(g));
    }
    static print_edge_list(g) {
	const edge_list = this.to_edge_list(g);
	for(let it of edge_list) {
	    console.log(this.#quot + it[0] + this.#quot + this.#lsep + this.#quot + it[1] + this.#quot);
	}
    }
}

// kind of interface, not a really useful implementation:
class Search {
    constructor(g,v) {
	this.graph = g;
	this.from = v;
    }
    marked(v) {
	if (this.graph.get_adj(this.from).includes(v))
	    return true;
	return false;
    }
    count() {
	return Graph.degree(this.graph,this.from)+1;
    }
}

class DepthFirstSearchRecursive {
#marked;
#count;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#dfs(g,s);
    }
#dfs(g,v) {
    this.#marked[v] = true;
    ++this.#count;
    const adj = g.get_adj(v);
    for(let w of adj)
	if (!this.#marked[w])
	    this.#dfs(g,w);
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class BreadthFirstSearchRecursive {
#marked;
#count;
#queue;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#queue = [];
	this.#marked[s] = true;
	this.#queue.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    if(this.#queue.length == 0)
	return;
    const vert = this.#queue.shift();
    ++this.#count;
    const adj = g.get_adj(vert);
    for(let w of adj)
	if (!this.#marked[w]) {
	    this.#marked[w] = true;
	    this.#queue.push(w);
	}
    this.#dfs(g);
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class DepthFirstSearchIterative {
#marked;
#count;
#stack;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#stack = [];
	this.#stack.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    while(this.#stack.length > 0) {
	const vertex = this.#stack.pop();
	if(this.#marked[vertex])
	    continue;
	this.#marked[vertex] = true;
	++this.#count;
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w])
		this.#stack.push(w);
    }
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class BreadthFirstSearchIterative {
#marked;
#count;
#queue;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#count = 0;
	this.#queue = [];
	this.#queue.push(s);
	this.#dfs(g);
    }
#dfs(g) {
    while(this.#queue.length > 0) {
	const vertex = this.#queue.shift();
	if(this.#marked[vertex])
	    continue;
	this.#marked[vertex] = true;
	++this.#count;
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w])
		this.#queue.push(w);
    }
}
    marked(w) { return this.#marked[w]; }
    count() { return this.#count; }
}

class PathTo {
    constructor(g,s) {
	this.graph = g;
	this.from = s;
    }
    has_path_to(v) {
	if (this.graph.get_adj(this.from).includes(v))
	    return true;
	return false;	
    }
    path_to(v) {
	if (this.hasPath(v))
	    return [s,v];
	else
	    [];
    }
}

class BreadthFirstPathsIterative {
#marked;
#edge_to;
#queue;
#source;
    constructor(g,s) {
	this.#source = s;
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#marked[this.#source] = true;
	this.#edge_to = [];
	this.#queue = [];
	this.#queue.push(this.#source);
	this.#bfs(g);
    }
#bfs(g) {
    while(this.#queue.length > 0) {
	const vertex = this.#queue.shift();
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w]) {
		this.#edge_to[w] = vertex;
		this.#marked[w] = true;
		this.#queue.push(w);
	    }
    }
}
    has_path_to(w) { return this.#marked[w]; }
    path_to(v) {
	if(!this.has_path_to(v)) return null;
	let path = [];
	for(let x = v; x != this.#source; x = this.#edge_to[x])
	    path.unshift(x);
	path.unshift(this.#source);
	return path;
    }
}


class DepthFirstPathToRecursive {
#marked;
#edge_to;
#source;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#edge_to = [];
	this.#source = s;
	this.#dfs(g,this.#source);
    }
#dfs(g,v) {
    this.#marked[v] = true;
    const adj = g.get_adj(v);
    for(let w of adj)
	if (!this.#marked[w]) {
	    this.#edge_to[w] = v;
	    this.#dfs(g,w);
	}
}
    has_path_to(v) { return this.#marked[v]; }
    path_to(v) {
	if(!this.has_path_to(v)) return null;
	let path = [];
	for(let x = v; x != this.#source; x = this.#edge_to[x])
	    path.unshift(x);
	path.unshift(this.#source);
	return path;
    }
}

class DepthFirstPathToIterative {
#marked;
#edge_to;
#stack;
#source;
    constructor(g,s) {
	this.#marked = new Array(g.nu_vertices());
	for (let i = 0; i < this.#marked.length; ++i)
	    this.#marked[i] = false;
	this.#source = s;
	this.#edge_to = [];
	this.#stack = [];
	this.#stack.push(this.#source);
	this.#dfs(g);
    }
#dfs(g) {
    while(this.#stack.length > 0) {
	const vertex = this.#stack.pop();
	if(this.#marked[vertex])
	    continue;
	this.#marked[vertex] = true;
	const adj = g.get_adj(vertex);
	for(let w of adj)
	    if (!this.#marked[w]) {
		this.#stack.push(w);
		this.#edge_to[w] = vertex;
	    }
    }
}
    has_path_to(w) { return this.#marked[w]; }
    path_to(v) {
	if(!this.has_path_to(v)) return null;
	let path = [];
	for(let x = v; x != this.#source; x = this.#edge_to[x])
	    path.unshift(x);
	path.unshift(this.#source);
	return path;
    }
}


class Generator {
    constructor(verts,edges,gralg,digr) {
	this.verts = verts;
	this.edges = edges;
	this.gralg = gralg;
	this.graph = new Graph(verts,digr);
	this.gengraph();
    }
    gengraph() {
	switch(this.gralg) {
	case "r2r":
	    this.make_r2r_graph();
	    break;
	case "s2r":
	    this.make_s2r_graph();
	    break;
	default:
	    console.log("Invalid graph algorithm code: " + graph_algorithm);
	}
    }
    full_connect_nu() {
	let nu_edges = 0;
	for(let i = 0; i < this.verts; ++i)
	    nu_edges += i;
	return nu_edges;
    }
    make_r2r_graph() {
	const theoretical_max = this.full_connect_nu(this.verts);
	const max_num = (this.edges <= theoretical_max) ? this.edges : theoretical_max;
	this.edges = 0;
	while(this.graph.nu_edges() < max_num) {
	    let n1 = Math.floor(Math.random()*this.verts);
	    let n2 = Math.floor(Math.random()*this.verts);
	    this.graph.add_edge(n1,n2);
	}
	this.edges = this.graph.nu_edges();
    }
    make_s2r_graph() {
	const theoretical_max = this.full_connect_nu(this.verts);
	const max_num = (this.edges <= theoretical_max) ? this.edges : theoretical_max;
	this.edges = 0;
	for(let n = 0; this.graph.nu_edges() < max_num; ++n) {
	    if (n == this.verts)
		n = 0;
	    const n1 = n;
	    let retval = false;
	    let n2 = n1;
	    while (n2 == n1)
		n2 = Math.floor(Math.random()*this.verts);
	    this.graph.add_edge(n1,n2);
	}
    }
}

const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('if', {
	  alias: 'i',
	  demandOption: false,
	  //default: 'graph_in.txt',
	  describe: 'input file with Sedgewick input',
	  type: 'string'
      })
      .option('of', {
	  alias: 'o',
	  demandOption: false,
	  //default: 'graph_out.csv',
	  describe: 'output file in csv format (produces an edge list)',
	  type: 'string'
      })
      .option('from', {
	  alias: 'f',
	  demandOption: false,
	  describe: 'from <vertex> (used in searches and paths)',
	  type: 'number'
      })
      .option('to', {
	  alias: 't',
	  demandOption: false,
	  describe: 'to <vertex> (used in searches and paths)',
	  type: 'number'
      })
      .option('adj', {
	  alias: 'a',
	  demandOption: false,
	  default: false,
	  describe: 'print adjacency table',
	  type: 'boolean'
      })
      .option('dt', {
	  alias: 'd',
	  demandOption: false,
	  default: false,
	  describe: 'print degree table',
	  type: 'boolean'
      })
      .option('el', {
	  alias: 'e',
	  demandOption: false,
	  default: false,
	  describe: 'print edge list',
	  type: 'boolean'
      })
      .command('shortest_path','which is the first path (bf) --from <vertex> --to <vertex>?', {
	  shortest_path: {
	      default: true,
	      type: 'boolean'
	  }
      })
      .command('is_path','is there a path --from <vertex> --to <vertex>?', {
	  is_path: {
	      default: true,
	      type: 'boolean'
	  }
      })
      .command('generate','generate graph (options: --verts, --edges, and --graphalg)', {
	  generate: {
	      default: true,
	      type: 'boolean'
	  }
      })
      .option('verts', {
	  demandOption: false,
	  default: 10,
	  describe: 'number of vertices (meaningful with "generate")',
	  type: 'number'
      })
      .option('edges', {
	  demandOption: false,
	  default: 10,
	  describe: 'number of edges (meaningful with "generate")',
	  type: 'number'
      })
      .option('graphalg', {
	  demandOption: false,
	  default: 'r2r',
	  describe: 'graph generation algorithm ("r2r" or "s2r")',
	  type: 'string'
      })
      .option('sedgeout', {
	  demandOption: false,
	  default: false,
	  describe: 'should output be Sedgewick format?',
	  type: 'boolean'
      })
      .command('search','examine connectedness (options: --algtype and --searchtype)', {
	  search: {
	      default: 0,
	      type: 'number'
	  }
      })
      .option('algtype', {
	  demandOption: false,
	  default: 'iter',
	  describe: 'algorithm type to use ("iter" and "recur" are accepted)',
	  type: 'string'
      })
      .option('searchtype', {
	  demandOption: false,
	  default: 'df',
	  describe: 'type of search ("df" and "bf" are accepted)',
	  type: 'string'
      })
      .option('stat', {
	  demandOption: false,
	  default: false,
	  describe: 'print graph statistics',
	  type: 'boolean'
      })
      .option('verbose', {
	  alias: 'v',
	  demandOption: false,
	  default: false,
	  describe: 'print extra info',
	  type: 'boolean'
      })
      .help()
      .argv;

let g;

if(argv["verbose"]) {
    console.log("argv:");
    console.table(argv);
}

if(argv["if"] === undefined) {
    //if(argv["_"].length == 0) {
    g = new Graph(7,true);
    //Graph.print_adj(g);
    g.add_edge(1,2);
    g.add_edge(4,0);
    g.add_edge(3,2);
    g.add_edge(3,4);
    g.add_edge(1,0);
    g.add_edge(4,5);
    g.add_edge(6,6);
    g.add_edge(3,2);
} else {
    g = new Graph(0,true);
    g.from_file(argv["if"]);
}
    
let nu_verts = g.nu_vertices();
if(argv["dt"]) {
    for(let i = 0; i < nu_verts; ++i)
	console.log("adjacency for " + i + ": " + g.get_adj(i) + ", degree: " + Graph.degree(g,i));
}
if(argv["stat"]) {
    console.log("number of vertices: " + nu_verts);
    console.log("number of edges: " + g.nu_edges());
    console.log("maximum degree: " + Graph.max_degree(g));
    console.log("average degree: " + Graph.ave_degree(g));
    console.log("number of self-loops: " + Graph.nu_self_loops(g));
}
if(argv["adj"]) {
    console.log("full adjacency table: ");
    Graph.print_adj(g);
}
if(argv["el"]) {
    console.log("edge list:")
    Graph.print_edge_list(g);
}
if (!argv["of"] === undefined)
    Graph.to_file(g,argv["of"]);

if (argv["verbose"]) {
    const len = g.nu_vertices();
    for(let from = 0; from < len; ++from)
	for(let to = 0; to < len; ++to)
	    if (Graph.is_edge(g,from,to))
		console.log(from + " -> " + to);
}

if (Object.keys(argv).includes("search")) {
    //const args = argv["search"].split(/\s+/);
    const source = (argv["from"]===undefined) ? 0 : argv["from"];
    if (source >= g.nu_vertices()) {
	console.log("No such vertex: " + source);
	process.exit(1);
    }
    console.log("Type of search: " + argv["searchtype"]);
    console.log("Type of algorithm: " + argv['algtype']);
    console.log("Starting vertex: " + source);
    let search;
    const warn_txt = "--algtype must be either 'iter' (iterative) or 'recur' (recursive).";
    if (argv["searchtype"] == "df") {
	if (argv["algtype"] == "iter")
	    search = new DepthFirstSearchIterative(g,source);
	else if (argv["alg"] == "recur")
	    search = new DepthFirstSearchRecursive(g,source);
	else {
	    console.log(warn_txt);
	    process.exit(1);
	}
    } else if (argv["searchtype"] == "bf") {
	if (argv["algtype"] == "iter")
	    search = new BreadthFirstSearchIterative(g,source);
	else if (argv["algtype"] == "recur")
	    search = new BreadthFirstSearchRecursive(g,source);
	else {
	    console.log(warn_txt);
	    process.exit(1);
	}
    } else {
	console.log("--searchtype must be either 'df' (depth first) or 'bf' (breadth first).");
	process.exit(1);
    }
    const verts = []
    for(let v = 0; v < g.nu_vertices(); ++v)
	if(search.marked(v))
	    verts.push(v);
    console.log((search.count() != g.nu_vertices() ? "Not c" : "C") + "onnected");
}

if (argv["generate"]) {
    console.log("number of vertices: " + argv["verts"]);
    console.log("number of edges: " + argv["edges"]);
    console.log("selected graphalg: " + argv["graphalg"]);
    const gen = new Generator(Number(argv['verts']),Number(argv['edges']),argv['graphalg'],false);
    if (!(argv["of"]===undefined))
	argv["sedgeout"] ? Graph.to_sfile(gen.graph,argv["of"]) : Graph.to_file(gen.graph,argv["of"]);
    else
	Graph.print_edge_list(gen.graph);
}

if (argv["is_path"] || argv["shortest_path"]) {
    if(argv["from"]===undefined) {
	console.error("--from <vertex> is missing");
	process.exit(1);
    }
    if(argv["to"]===undefined) {
	console.error("--to <vertex> is missing");
	process.exit(1);
    }
    //console.log("from: " + );
    //console.log("to:   " + argv["to"]);
    console.log(argv["algtype"] == 'iter' ? "iterative" : "recursive");
    let pt;
    if (argv["is_path"])
	pt = argv["algtype"] == 'iter' ? new DepthFirstPathToIterative(g,argv["from"]) : new DepthFirstPathToRecursive(g,argv["from"]);
    else if (argv["shortest_path"])
	pt = new BreadthFirstPathsIterative(g,argv["from"]);
    else {
	console.error("unknown option");
	process.exit(1);
    }
    const is_path = pt.has_path_to(argv["to"]);
    console.log("is there path from " + argv["from"] + " to " + argv["to"] + "? " + (is_path ? "yes" : "no"));
    if (is_path) {
	const the_path = pt.path_to(argv["to"]);
	console.log("Path length: " + the_path.length);
	console.log("Path found:");
	console.log(the_path);
    }
}
