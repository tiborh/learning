#!/usr/bin/env node

console.log("__BEGIN__");
const net = require('net');
const portnu = 8888;
console.log("Listening on port: " + portnu);

const server = net.createServer(socket => {
    // define emitter:
    const EventEmitter = require('events').EventEmitter;
    const channel = new EventEmitter();
    channel.on('join', () => {
	console.log('Client joined!');
    });
    channel.emit('join');
});
server.listen(portnu);
console.log("__END__");
