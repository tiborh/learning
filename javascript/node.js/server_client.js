#!/usr/bin/env node

const request = require('request');
const fs = require('fs');

function req_builder(res) {
    const portnu = 1234;
    const whost = "localhost";
    return("http://" + whost + ":" + portnu + "/" + res);
}

function req_maker(req) {
    const fn = "server_pipe.dat";
    request(req,function(error,response,body){
	if(error) {
	    console.log("error: ",error);
	    console.log("exiting...");
	    process.exit();
	}
	console.log("status code: " + response.statusCode);
	console.log("headers:");
	console.log(response.headers);
	//console.log("response: ",response);
	if(body)
	    console.log("body: ", body);
	console.log("press a key...");
    }).pipe(fs.createWriteStream(fn));
}

const keypress = async () => {
  process.stdin.setRawMode(true)
  return new Promise(resolve => process.stdin.once('data', () => {
    process.stdin.setRawMode(false)
    resolve()
  }))
}

;(async () => {
    const res1 = "hello";
    const res2 = "printRequestHeaders";
    req_maker(req_builder(res1));
    await keypress();
    req_maker(req_builder(res2));
    await keypress();
    req_maker(req_builder("null"));
    await keypress();
    console.log("end");
})().then(process.exit);
