#!/usr/bin/env node

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply

const path = require('path');
const gen = require('./mod_gen_arr');
const num_of_nums = process.argv[2] === undefined ? 5 : Number(process.argv[2]);
const max_num = process.argv[3] === undefined ? 10 : Number(process.argv[3]);
const min_num = process.argv[4] === undefined ? 0 : Number(process.argv[4]);
const usage = `Usage:\n\t${path.basename(process.argv[1])} [num_of_nums (default: ${num_of_nums})] [max_num (exclusive) (default: ${max_num})] [min_num (inclusive) (default: ${min_num})]`;
const numbers = gen.nums(num_of_nums,max_num,min_num);
const numbers2 = gen.nums(num_of_nums,max_num,min_num);
const max = Math.max.apply(null, numbers);
const min = Math.min.apply(null, numbers);
const plusone = function(...args){
    const outarr = [];
    for(let it of args)
	outarr.push(it+1);
    return outarr;
}.apply(null, [1,2,3,4,5,6,7]);
// does not work in node.js:
// const assoc = function(...args){
//     console.log(args);
//     const outarr = [];
//     for(let it in args)
// 	//console.log(it);
// 	outarr.push(it);
//     return outarr;
//     //return 0;
// }.apply(null, {a: 1,b: 2,c: 3,d: 4,e: 5,f: 6,g: 7});

if (process.argv[2] === undefined)
    console.info(usage);
console.log("__BEGIN__");
console.log("apply on the fly:");
console.log(plusone);
console.log("working with random array:");
console.log(numbers);
console.log(`min: ${min}, max: ${max}`);
console.log("second:");
console.log(numbers2);
let numbers_clone = numbers.slice(); // clone array
numbers_clone.push.apply(numbers_clone,numbers2);
console.log("concatenated:");
console.log(numbers_clone);


console.log("__END__");
