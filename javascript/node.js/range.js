#!/usr/bin/env node

const path = require('path');
const gen = require('./mod_gen_arr');
const num =  5;
const start = 0;
const incr =  1;
const num_of_nums = process.argv[2] === undefined ? num : Number(process.argv[2]);
const start_num = process.argv[3] === undefined ? start : Number(process.argv[3]);
const num_incr = process.argv[4] === undefined ? incr : Number(process.argv[4]);
const usage = `Usage:\n\t${path.basename(process.argv[1])} [length (default: ${num})] [start (default: ${start})] [increment (default: ${incr})]`;
if (process.argv[2] === undefined)
    console.info(usage);

console.log("__BEGIN__");
console.log(gen.range(num_of_nums,start_num,num_incr));
console.log("__END__");
