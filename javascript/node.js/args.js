#!/usr/bin/env node

const path = require('path');

console.log("__BEGIN__");
console.log(process.argv);
const args = process.argv.slice(2);
console.log("Script name:\n\t" + path.basename(process.argv[1]));
console.log("In directory:\n\t" + path.dirname(process.argv[1]));

if (args.length > 0) {
    console.log("Args:");
    args.forEach(function(it) {
	console.log("\t" + it);
    });
}
console.log("__END__");
