#!/usr/bin/env node

const http = require('http');
const portnu = 8080;
console.log("Listening on port: " + portnu);
const fs = require('fs');
console.log(new Date());

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(new Date().toString());
    res.end();
}).listen(portnu);
