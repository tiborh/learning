#!/usr/bin/env node

const fs = require("fs");
const fn = "poem.txt";
const data = fs.readFileSync(fn);

console.log("Blocking read demo.");
console.log("Binary:");
console.log(data);
console.log("As string:");
console.log(data.toString());
console.log("__END__");
