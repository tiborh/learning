#!/usr/bin/env node

console.log("global:");
console.log(global);
console.log("====================");
console.log("process:");
console.log(process);
console.log("====================");
console.log("require:");
console.log(require);
console.log("====================");
console.log("module:");
console.log(module);
