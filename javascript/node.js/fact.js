#!/usr/bin/env node
const path = require('path');
const defnum = 4;
const usage = `Usage:\n\t${path.basename(process.argv[1])} [number (default: ${defnum})]`;
if (process.argv[2] === undefined)
    console.info(usage);
function fact(num) {
    console.log(num);
    if (num == 1 || num == 0)
	return 1;
    return num * fact(num-1);
}
function fact_wrap(num) {
    if (num !== num)
	return;
    if (num < 0)
	return;
    return fact(num);
}
console.log("__BEGIN__");
// max seems to be 170, after that Infinity
const num = process.argv[2] === undefined ? defnum : Number(process.argv[2]);
const factorial = fact_wrap(num);
console.log(`${num}! === ${factorial}`);
console.log("__END__");
