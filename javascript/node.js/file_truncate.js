#!/usr/bin/env node

const fs = require("fs");
const buf = Buffer.alloc(1024);
const fm = 'r+';
const tmpfn = "file_truncate.tmp";

const yargs = require('yargs');
const argv = yargs
      .argv;

const fn = argv["_"].length == 0 ? "input.txt" : argv["_"][0];
const trunc_len = argv["_"].length <= 1 ? 10 : Number(argv["_"][0]);

fs.copyFile(fn, tmpfn, (err) => { 
  if (err) { 
    console.log("Error Found:", err); 
  } 
});

console.log("'" + fn + "' has been copied into '" + tmpfn + "'");
console.log("Opening file: '" + tmpfn + "' with '" + fm + "'");
fs.open(tmpfn, fm, function(err, fd) {
    if (err) {
	return console.error(err);
    }
    console.log("'" + tmpfn + "' opened successfully");
    console.log("Going to truncate the file after " + trunc_len + " bytes");
    
    // Truncate the opened file.
    fs.ftruncate(fd, trunc_len, function(err) {
	if (err) {
            console.log(err);
	} 
	console.log("File truncated successfully.");
	console.log("Going to read the same file"); 
	
	fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
            if (err) {
		console.log(err);
            }

            // Print only read bytes to avoid junk.
            if(bytes > 0) {
		console.log(buf.slice(0, bytes).toString());
            }

            // Close the opened file.
            fs.close(fd, function(err) {
		if (err) {
		    console.log(err);
		}
		console.log("'" + tmpfn + "' closed successfully.");
            });
	});
    });
});
