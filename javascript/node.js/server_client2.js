#!/usr/bin/env node

const request = require('request');
const fs = require('fs');

function req_builder(res) {
    const portnu = 1234;
    const whost = "localhost";
    return("http://" + whost + ":" + portnu + "/" + res);
}

function opt_maker(res) {
    return(
	{
	    url: req_builder(res),
	    headers:{'A-DEMO-HEADER':"my_demo_header" }
	}
    );
}

const keypress = async () => {
  process.stdin.setRawMode(true)
  return new Promise(resolve => process.stdin.once('data', () => {
    process.stdin.setRawMode(false)
    resolve()
  }))
}

;(async () => {
    const res1 = "hello";
    const res2 = "printRequestHeaders";
    const callback = function(err,resp,bdy) {
	if(err) console.log(err);
	else console.log(bdy);
	console.log("press a key...");
    }
    request(opt_maker(res1),callback);
    await keypress();
    request(opt_maker(res2),callback);
    await keypress();
    request(opt_maker("null"),callback);
    await keypress();
    console.log("end");
})().then(process.exit);
