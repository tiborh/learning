#!/usr/bin/env node

const http = require('http');
const fs = require('fs');
const path = require('path');

const portnu = 1234;
const whost = "localhost";
const fn = "form.html";
const fp = path.join(__dirname,'public',fn);


const connect = require('connect');
const bparser = require('body-parser');

const app = connect().use(bparser.urlencoded(
    {extended:true}
)).use(function(req,res){
    const pinfo = {};

    pinfo.fname = req.body.ufname;
    pinfo.lname = req.body.ulname;

    res.end("<p>User info parsed from form: " + pinfo.fname + " " + pinfo.lname + "</p>");
});

http.createServer(app).listen(portnu);
console.log("Listening on port: " + whost + ":" + portnu);
