#!/usr/bin/env node

console.log("0. Import events module");
const events = require('events');
console.log("1. Create an eventEmitter object");
const eventEmitter = new events.EventEmitter();

console.log("2. define listeners (2)");
// listener #1
const listner1 = function listner1() {
   console.log("\tlistner1 executed.");
}

// listener #2
const listner2 = function listner2() {
    console.log("\tlistner2 executed.");
}

console.log("3. Bind the connection event with the listner1 function");
eventEmitter.addListener('connection', listner1);

console.log("4. Bind the connection event with the listner2 function");
eventEmitter.on('connection', listner2);

console.log("5. Count the listeners");
let eventListeners = require('events').EventEmitter.listenerCount
   (eventEmitter,'connection');
console.log("\t" + eventListeners + " Listner(s) listening to connection event");

console.log("6. Fire the connection event");
eventEmitter.emit('connection');

console.log("7. Remove the binding of listner1 function");
eventEmitter.removeListener('connection', listner1);
console.log("\tListner1 will not listen now.");

console.log("8. Fire the connection event" );
eventEmitter.emit('connection');

console.log("9. Recount the listeners");
eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log("\t" + eventListeners + " Listner(s) listening to connection event");

console.log("__END__");
