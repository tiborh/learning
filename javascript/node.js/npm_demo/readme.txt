1. npm init
--------------------------------------------------------------------------------
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help init` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (npm_demo) 
version: (1.0.0) 0.0.1
description: npm demo
entry point: (index.js) 
test command: 
git repository: 
keywords: 
author: tibor
license: (ISC) 
About to write to /home/etibhar/repos/learning/javascript/node.js/npm_demo/package.json:

{
  "name": "npm_demo",
  "version": "0.0.1",
  "description": "npm demo",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "tibor",
  "license": "ISC"
}


Is this OK? (yes) 
--------------------------------------------------------------------------------
2. file created:
   package.json (with content above {})
3. create entry point (manually):
   index.js
4. make it produce some output
5. execute with node or make it executable with:
   #!/usr/bin/env node
   and then
   chmod 755 index.js
6. install a module (e.g. npm install express --save)
   (--save adds it to package.json as dependency)
7. npm uninstall <module> can uninstall a module and its dependencies
8. it can word the other way around too: manually adding dependency and
   version to package.json and then npm install
   "^..." can indicate that `npm update` can look for a newer version
9. `npm prune` can help remove modules no longer used as dependency.
