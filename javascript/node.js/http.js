#!/usr/bin/env node

const http = require('http');
const portnu = 1234;
const whost = "localhost";
console.log("Listening on port: " + whost + ":" + portnu);

const hndlr = function(req,res) {
    res.end("Hello");
};

http.createServer(hndlr).listen(portnu,whost);
