#!/usr/bin/env node
const path = require('path');
const hex = (function() {
    if (process.argv[2] === 'true')
	return true;
    if (process.argv[2] === 'false')
	return false;
    return;
})();
const slicer = hex === undefined ? 2 : 3;
const usage = "Usage:\n\t" + path.basename(process.argv[1]) + " [true/false] string(s)";
const args = process.argv.slice(slicer);
if (args.length === 0) {
    console.log(usage);
} else {
    for (it of args) {
	console.log(it + ": " + it.split("").map((x) => {
	    return (hex) ? x.charCodeAt(0).toString(16) : x.charCodeAt(0)
	}).join(" "));
    }
}

