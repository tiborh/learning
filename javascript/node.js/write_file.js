#!/usr/bin/env node

const fs = require("fs");
const fn = 'write_file.txt';
const txt = '今晩は。';

console.log("Going to write into existing file");
fs.writeFile(fn, txt, function(err) {
    if (err) {
	return console.error(err);
    }
    
    console.log("Data written to '" + fn + "'");
    console.log("Let's read newly written data");
   
    fs.readFile(fn, function (err, data) {
	if (err) {
            return console.error(err);
	}
	console.log("Asynchronous read: " + data.toString());
	console.log("No conversion:     " + data);
    });
});

