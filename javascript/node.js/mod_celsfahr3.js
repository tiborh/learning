class Converter {
    static diff = 32;
    static coef = 9/5;
    constructor(temp,roun=1) {
	this.temp = temp;
	this.rounding = roun;
    }
    c2f() {
	return (this.temp * Converter.coef + Converter.diff).toFixed(this.rounding);
    }
    f2c() {
	return  ((this.temp - Converter.diff) / Converter.coef).toFixed(this.rounding);
    }
}
module.exports = exports = Converter;
