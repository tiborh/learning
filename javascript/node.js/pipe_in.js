#!/usr/bin/env node

const stdin = process.openStdin();

let data = "";

stdin.on('data', function(chunk) {
  data += chunk;
});

stdin.on('end', end);

function end() {
    console.log("in end()");
    console.log(data.trim());
    console.log("end of end()");
    main();
}

function main() {
    console.log("in main()");
    console.log(data.trim());
    console.log("__END__");
}
