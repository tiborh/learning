#!/usr/bin/env node

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions

const materials = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];

console.log(materials);
console.log(materials.map(material => material.length));

const a = a => a + 100;
// in place of:
// const a = function(a) { return(a + 100); };

const b = 10;
const c = 20;

const d = () => (b + c) * 100;
console.log(d());

const e = (a,b) => (a + b) * 100;
console.log(e(b,c));

const f = (a,b) => {
    const c = 100;
    return (a + b) * c;
}
console.log(f(b,c));

// pars are important:
const g = (k1,v1,k2,v2) => ({k1: v1, k2: v2});
console.log(g('a',1,'b',2));

const h = (...pars) => {
    const a = [];
    for (let it of pars)
	a.push(it);
    return a;
}

console.log(h(1,2,3,4,5));

const i = (a=100, b=200, c) => a + b + c;
console.log(i(5,10,15));	// i(5) gives NaN

const j = ([a, b] = [10, 20]) => a + b;
console.log(j());		// 30

const k = ({ a, b } = { a: 10, b: 20 }) => a + b;
console.log(k());		// 30

// not good for methods:
const l = { // does not create a new scope
    i: 10,
    b: () => console.log(this.i, this),
    c: function() {
	console.log(this.i, this);
    }
}

l.b(); // prints undefined, Window {...} (or the global object)
l.c(); // prints 10, Object {...}

// it has no "this"
const m = {
  a: 10
};

Object.defineProperty(m, 'b', {
    get: () => {
	console.log(this.a, typeof this.a, this); // undefined undefined {}
	return this.a + 10; // NaN
    }
});

console.log(m);
console.log(m.b);



console.log("__END__");
