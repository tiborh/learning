#!/usr/bin/env node

const http = require('http');
const fs = require('fs');
const path = require('path');

const portnu = 8080;
const fn = "file_read.html";
const fp = path.join(__dirname,'public',fn);

console.log("Looking for: " + fp);
console.log("__dirname: " + __dirname);

http.createServer(function (req, res) {
    fs.readFile(fp,function(err,data) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(data);
	return res.end();
    });
}).listen(portnu);
console.log("Listening on port: " + portnu);
