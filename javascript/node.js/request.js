#!/usr/bin/env node

const http = require('http');

const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("Request URL:<br />" + req.url); // what comes after the domain name (try writing some resource names)
    console.log(req.url);
    res.end("<br />__END__");
}).listen(portnu);
