#!/usr/bin/env node

const yargs = require('yargs');
const assert = require('assert');
const prompt = require('prompt-sync')();

const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('maxnu', {
	  alias: 'a',
	  demandOption: false,
	  default: 100,
	  describe: 'maxinum number',
	  type: 'number'
      })
      .option('minnu', {
	  alias: 'i',
	  demandOption: false,
	  default: 0,
	  describe: 'maxinum number',
	  type: 'number'
      })
      .option('tests', {
	  alias: 't',
	  demandOption: false,
	  default: 0,
	  describe: 'number of autotests',
	  type: 'number'
      })
      .help()
      .argv;

//console.log(argv);

const guesser = function() {
    const minnu = argv["minnu"];
    const my_num = Math.ceil(Math.random()*(argv["maxnu"]-minnu))+minnu;

    return{
	guess: function(num) {
	    if (num > my_num) {
		console.log("Greater");
		return 1;
	    }
	    if (num < my_num) {
		console.log("Less");
		return -1;
	    }
	    if (num === my_num) {
		console.log("Equal");
		return 0;
	    }
	    console.log("Wrong input. Try again.");
	    return -2;
	}
    };
};

function game_play() {
    const game = new guesser();
    let result = 1;

    while(result != 0){
	const answer = prompt('Guess my number: ');
	result = game.guess(Number(answer));
    };
}

function get_mid(min, max) {
    assert(min <= max);
    return Math.ceil((min+max)/2);
}

function test_plays() {
    for(let i = 0; i < argv.tests; ++i) {
	let min = argv.minnu;
	let max = argv.maxnu;
	console.log(`*** Test ${i+1} ***`);
	const game = new guesser();
	let result = 1;
	while(result != 0){
	    const mid = get_mid(min,max);
	    //console.log(`guess: ${mid} (${min}:${max})`);
	    console.log(`guess: ${mid}`);
	    result = game.guess(mid);
	    if(result == 1)
		max = mid - 1;
	    else if (result == -1)
		min = mid + 1;
	};
    }
}

--argv.minnu;
if (argv.tests == 0)
    game_play();
else
    test_plays();
