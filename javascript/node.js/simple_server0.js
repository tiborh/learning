#!/usr/bin/env node

const http = require('http');
const hostname = '127.0.0.1';
const port = 8080;
const server = http.createServer((req, res) => {
    res.end('Hello, world.');
});

server.listen(port, hostname, () => { // hostname, if omitted, defaults to localhost
    console.log('Server listening on: http://%s:%s', hostname, port);
});
