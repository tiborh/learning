#!/usr/bin/env node

// https://www.javascripttutorial.net/es6/javascript-new-target/

function Animal(name) {
    this.name = name;
}

function Person(name) {
    if (!new.target) {
        throw "must use new operator with Person";
    }
    this.name = name;
}

const bo = Animal("Bo");
console.log(`without "new": ${bo}`);		// undefined
try {
    const bobo = Person("Bobo");
} catch (err) {
    console.error(err);
}

const bobo = new Person("Bobo");
console.log(bobo);

console.log("__END__");
