#!/usr/bin/env node

const http = require('http');
const req = require('request');

const portnu = 8080;
const waddr = "http://silurus.in";

req(waddr,function(error,resp,body) {
    if(!error && resp.statusCode == 200) {
	console.log(body);
    }
    if(error) {
	console.log(error);
    }
});

console.log("Listening on port: " + portnu);
