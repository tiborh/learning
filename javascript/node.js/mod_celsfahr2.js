const diff = 32;
const coef = 9/5;
function c2f(cels) {
    return cels * coef + diff;
}
function f2c(fahr) {
    return  (fahr - diff) / coef;
}
exports.c2f = cels => c2f(cels);
exports.f2c = fahr => f2c(fahr);
