#!/usr/bin/env node

const yargs = require('yargs');
const deftxt = "default text";
const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('decor', {
	  alias: 'd',
	  demandOption: false,
	  default: '*',
	  describe: 'decoration character',
	  type: 'string'
      })
      .option('padding', {
	  alias: 'p',
	  demandOption: false,
	  default: ' ',
	  describe: 'padding character',
	  type: 'string'
      })
      .option('blank_rows', {
	  alias: 'b',
	  demandOption: false,
	  default: false,
	  describe: 'blank row above and below',
	  type: 'boolean'
      })
      .option('wrap_text', {
	  alias: 'w',
	  demandOption: false,
	  default: false,
	  describe: 'wrap text if longer than box width',
	  type: 'boolean'
      })
      .option('fix_width', {
	  alias: 'f',
	  demandOption: false,
	  describe: 'fix width',
	  type: 'number'
      })
      .usage("$0 [-d <decor char>] [-p <padding char>] [-f <box width>] [-b] [-w] string(s)")
      .help()
      .argv;

function insert_at(str,at,what) {
    return str.slice(0, at) + what + str.slice(at);
}

function pad_filling(str,tgt_len,dec_len,spc_ch) {
    let retstr = str;
    while(retstr.length < tgt_len) {
	retstr = insert_at(retstr,retstr.length-(dec_len+1),spc_ch);
	if (retstr.length < tgt_len)
	    retstr = insert_at(retstr,dec_len,spc_ch);
	//console.log(retstr);
    }
    return retstr;
}

function wrap_text(str,dcr,spc,wth) {
    if (wth < 2)
	return str;
    const decpad_len = (dcr.length * 2) + (spc.length * 2);
    const is_decor = (decpad_len + 2 > wth) ? false : true;
    let outstr = "";
    let remaining_str = str;
    let lnstr = "";
    const fillen = is_decor ? wth - decpad_len - 1 : wth - 1;
    const endind = is_decor ? (wth - dcr.length - spc.length) : wth;
    while (remaining_str.length > 0) {
	if (is_decor)
	    lnstr += dcr + spc;
	lnstr += remaining_str.slice(0,fillen);
	if (is_decor)
	    lnstr +=  spc + dcr;
	lnstr += '\n';
	if (lnstr.length < wth)
	    lnstr = pad_filling(lnstr,wth,dcr.length,spc);
	outstr += lnstr;
	lnstr = "";
	remaining_str = remaining_str.slice(fillen);
    }
    return outstr;
}

function decor(txt,ch,sp,fw,br,wt) {
    const boxwidth = (fw !== undefined) ? Math.floor(fw/ch.length) : Math.ceil((txt.length / ch.length) + (2 / ch.length) + ((2 * ch.length) / ch.length));
    const topbott = ch.repeat(boxwidth) + '\n';
    const blankrow = (br) ? ch + sp.repeat((topbott.length - (2 * ch.length) - 1)) + ch + '\n' : "";
    let filling = ch + sp + txt + sp + ch + '\n';
    if (filling.length < topbott.length)
	filling = pad_filling(filling,topbott.length,ch.length,sp);
    if (wt && filling.length > topbott.length)
	filling = wrap_text(txt,ch,sp,topbott.length);
    let outstr = topbott;
    if (br)
	outstr += blankrow;
    outstr += filling;
    if (br)
	outstr += blankrow;
    outstr += topbott;
    return outstr;
}

if (argv._.length == 0)
    console.log(decor(deftxt,argv['d'],argv['p'],argv['f'],argv['b'],argv['w']));
else
    for (let it of argv._)
	console.log(decor(it,argv['d'],argv['p'],argv['f'],argv['b'],argv['w']));

