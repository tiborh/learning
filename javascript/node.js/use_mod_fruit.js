#!/usr/bin/env node

const fruit = require("./mod_fruit");
const apple_input = ["apple","mid-sized round fruit, red/yellow/green"];
console.log("input:");
console.log(apple_input);
const apple = fruit(...apple_input);

console.log("apple.get_data()");
console.log(apple.get_data());

console.log("Changing description (set_descr())");
apple.set_descr("a common fruit in Europe and North America");
console.log(apple.get_data());
