#!/usr/bin/env node
const path = require('path');
const gen = require('./mod_gen_arr');
const deflen = 10;
const usage = `Usage:\n\t${path.basename(process.argv[1])} [length (default: ${deflen})]`;
if (process.argv[2] === undefined)
    console.info(usage);
console.log("__BEGIN__");
const len = process.argv[2] === undefined ? deflen : Number(process.argv[2]);
const fact = gen.fact(len);
console.log(fact);
if (len > 100)			// max seems to be 171, after that Infinity
    console.log(`last item: ${fact[len-1]}`);
console.log("__END__");
