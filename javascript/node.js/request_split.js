#!/usr/bin/env node

const http = require('http');
const url = require('url');

const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    const r = url.parse(req.url, true).query;
    res.write("you need: /?one={sth}&two={else}<br />");
    console.log(r);
    res.write(r.one + " " + r.two);
    //res.write("Request URL:<br />" + req.url); // what comes after the domain name (try writing some resource names)
    //console.log(req.url);
    res.end("<br />__END__");
}).listen(portnu);

// url:
// Url {
//   protocol: null,
//   slashes: null,
//   auth: null,
//   host: null,
//   port: null,
//   hostname: null,
//   hash: null,
//   search: null,
//   query: [Object: null prototype] {}, // the object
//   pathname: '/{path}',
//   path: '/{path}',
//   href: '/{path}'
// }
