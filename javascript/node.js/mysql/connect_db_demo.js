#!/usr/bin/env node

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "nodejs",
  password: "Mah2Ov1u"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

process.on('SIGINT', function() {
    console.log("\nCaught interrupt signal");
    con.destroy();
    console.log("db connection destroyed, exiting...");
    process.exit();
});

// a more peaceful close:
// connection.end(function(err) {
//   if (err) {
//     return console.log('error:' + err.message);
//   }
//   console.log('Close the database connection.');
// });
