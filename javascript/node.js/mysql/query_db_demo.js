#!/usr/bin/env node

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "nodejs",
  password: "Mah2Ov1u"
});

const sql = "use nodejs_test;";
//const sql = "show tables;";

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Query: " + sql);
      console.log("Result: " + result);
  });
});

process.on('SIGINT', function() {
    console.log("\nCaught interrupt signal");
    con.destroy();
    console.log("db connection destroyed, exiting...");
    process.exit();
});
