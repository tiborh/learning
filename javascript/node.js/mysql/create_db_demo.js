#!/usr/bin/env node

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "nodejs",
  password: "Mah2Ov1u"
});

const sql_create = "create database if not exists testdb";
const sql_drop = "drop database if exists testdb";
//const sql = "show tables;";

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query(sql_create, function (err, result) {
      if (err) throw err;
      console.log("sql: " + sql_create);
      console.log("Database created (if does not exist)");
  });
});

process.on('SIGINT', function() {
    console.log("\nCaught interrupt signal");
    con.query(sql_drop, function (err, result) {
	if (err) throw err;
	console.log("sql: " + sql_drop); // does not print
	console.log("Database dropped (if exists)"); // does not print
    });
    con.destroy();
    console.log("db connection destroyed, exiting...");
    process.exit();
});
