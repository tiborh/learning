#!/usr/bin/env node

const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "nodejs",
  password: "Mah2Ov1u"
});

//const sql = "show tables;";

function query(con,sql) {
    con.query(sql, function (err, result) {
	if (err) throw err;
	console.log("sql: " + sql);
	console.log("result: " + result);
    });
}

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    // con.query(sql_create_db, function (err, result) {
    // 	if (err) throw err;
    // 	console.log("sql: " + sql_create_db);
    // });
    query(con,"create database if not exists testdb");
    query(con,"use testdb");
    query(con,"create table if not exists customers (name varchar(255), address varchar(255))");
    query(con,"alter table customers add column id int auto_increment primary key");
    query(con,"show tables");
    query(con,"show columns from customers");
    query(con,"insert into customers (name, address) values ('Company Inc', 'Highway 37')");
    query(con,"select * from customers");
});

process.on('SIGINT', function() {
    const sql_drop = "drop database if exists testdb";
    console.log("\nCaught interrupt signal");
    con.query(sql_drop, function (err, result) {
	if (err) throw err;
	console.log("sql: " + sql_drop); // does not print
	console.log("Database dropped (if exists)"); // does not print
    });
    con.destroy();
    console.log("db connection destroyed, exiting...");
    process.exit();
});
