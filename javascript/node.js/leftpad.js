#!/usr/bin/env node

const path = require('path');

function usage() {
    console.log(path.basename(process.argv[1]) + " str len ch");
}

function leftpad(str,len,ch) {
    str = String(str);
    let i = -1;
    if(!ch && ch !== 0) ch = ' ';

    len = len - str.length;

    while(++i < len)
	str = ch + str;

    return str;
}

console.log("__BEGIN__");
const args = process.argv.slice(2);
if (args.length < 3) {
    usage();
    console.log("__END__");
    process.exit();
}
const str = args[0];
const len = parseInt(args[1]);
const ch = args[2];
console.log(leftpad(str,len,ch));
console.log("__END__");
