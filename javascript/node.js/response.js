#!/usr/bin/env node

const http = require('http');
const portnu = 1234;
const whost = "localhost";
const connect = require('connect');
const _end = "__END__";

const app = connect().use(function(req,res){
    if(req.url == "/hello") {
	console.log("sending plain txt");
	res.end("Hello from app");
    } else if(req.url == "/hello.json") {
	console.log("sending json");
	const dat = ["Hello","World"];
	const json_dat = JSON.stringify(dat);
	res.setHeader('Content-Type','application/json');
	res.end(json_dat);
    } else {
	console.log("sending 404 status code");
	res.statusCode = 404;
	res.end("Try hello or hello.json");
    }
}).listen(portnu);
console.log("Listening on port: " + whost + ":" + portnu);
