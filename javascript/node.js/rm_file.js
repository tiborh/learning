#!/usr/bin/env node

const fs = require("fs");

const yargs = require('yargs');
const argv = yargs
      .argv;

const fn = argv["_"].length == 0 ? "input.txt" : argv["_"][0];

console.log("Going to delete '" + fn + "'");
fs.unlink(fn, function(err) {
   if (err) {
      return console.error(err);
   }
   console.log("File deleted successfully!");
});
