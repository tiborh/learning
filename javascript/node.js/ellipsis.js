#!/usr/bin/env node

function add(addend1,addend2,...addends) {
    let sum = addend1 + addend2;
    while(addends.length > 0)
	sum += addends.pop();
    return sum;
}
console.log(add(1,2));
console.log(add(1,2,3));
console.log(add(1,2,3,4));
console.log(add(1,2,...[3,4]));	// spread
console.log(add(1,2,[3,4])); 	// string coercion
const len = 5;
const seq1 = [...Array(len).keys()]; // convert iterable to array
let seq2 = [...Array(len).keys()];
seq2 = seq2.map(n => n + 5);
console.log(seq1);
console.log(seq2);
console.log(seq1.concat(seq2));	// result in third
console.log([...seq1,...seq2]);	// spread operator
seq1.push.apply(seq1,seq2);	// result in seq1
console.log(seq1);
const array = [...(function*() { // generator
    for (let i = 10; i > 0; i--) {
	yield i;
    }
    yield 'Launch';
})()];
console.log(array);
let [a, b, ...rest] = [10, 20, 30, 40, 50];
console.log(`a: ${a}; b: ${b}; rest: ${rest}`);
let {c, d, ...rest2} = {a: 10, b: 20, c: 30, d: 40}; // reverse assignment
console.log(`c: ${c}; d: ${d}; rest2:`);
console.log(rest2);
let seq1_copy = [...seq1];
let seq1_ref = seq1;
console.log(`seq1 == seq1_copy: ${seq1 == seq1_copy}`);
console.log(`seq1 == seq1_ref: ${seq1 == seq1_ref}`);
console.log(`seq1 === seq1_ref: ${seq1 === seq1_ref}`);
console.log(`Max of seq1: ${Math.max(...seq1)}`);
console.log("__END__");
