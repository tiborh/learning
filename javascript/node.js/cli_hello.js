#!/usr/bin/env node

const [nodePath, scriptPath, name] = process.argv;
console.log(`nodePath: ${nodePath}`);
console.log(`scriptPath: ${scriptPath}`);
console.log('Hello', name);
console.log("process.argv0: %s",process.argv0);
console.log("process.argv:");
console.log(process.argv);
