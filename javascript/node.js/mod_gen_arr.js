function gen_num(max,min) {
    return Math.floor(Math.random() * (max - min) + min);
}
// function gen_nums(num_of_nums,max,min) {
//     const out_arr = [];
//     for (let i = 0; i < num_of_nums; ++i)
// 	out_arr.push(gen_num(max,min));
//     return out_arr;
// }
function gen_nums(num_of_nums,max,min) {
    const out_arr = Array(num_of_nums).fill(0);
    out_arr.forEach( (x,ind,arr) => arr[ind] = gen_num(max,min));
    return out_arr;
}
function gen_range(num,start,incr) {
    const out_arr = Array(num).fill(start);
    out_arr.slice(1).forEach(function(it,ind,arr) {
	this[ind+1] = this[ind] + incr;
    },out_arr);
    return out_arr;
}
function gen_fib(len) {
    const fib = Array(len).fill(1);
    fib.slice(2).forEach( function(x,ind,arr) {
	this[ind+2] = this[ind]+this[ind+1];
    },fib);
    return fib;
}
function gen_fact(len) {
    const fact = Array(len).fill(1);
    fact.slice(1).forEach( function(x,ind,arr) {
	this[ind+1] = (ind+1)*this[ind];
    },fact);
    return fact;
}
function gen_alph(num,lower=true) {
    if (num < 0)
	num = 0;
    if (num > 26)
	num = 26;
    const shift_num = lower ? 97 : 65;
    return [...Array(num)].map((x,i)=>String.fromCharCode(i + shift_num));
}
function gen_alph_hash(start,num,lower) {
    const alph = gen_alph(num,lower);
    const out_arr = Array();
    let i = start;
    alph.forEach( x => out_arr[alph[i]] = i++);
    return out_arr;
}
exports.alph = (nu=26) => gen_alph(nu,true);
exports.Alph = (nu=26) => gen_alph(nu,false);
exports.alphHash = (st=0,nu=26,lo=true) => gen_alph_hash(st,nu,lo);
exports.num = (max,min) => gen_num(max,min);
exports.nums = (nu,mx,mn) => gen_nums(nu,mx,mn);
exports.range = (num,start,incr) => gen_range(num,start,incr);
exports.fib = (len) => gen_fib(len);
exports.fact = (len) => gen_fact(len);
