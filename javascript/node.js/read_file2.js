#!/usr/bin/env node

const fs = require("fs");
const fn = "poem.txt";
fs.readFile(fn, function(err,data) {
    if (err) {
	console.log(err);
	//console.log(err.stack);
    } else {
	console.log("Non-blocking read demo.");
	console.log("Binary:");
	console.log(data);
	console.log("As string:");
	console.log(data.toString());
    }
    console.log("__END__");
});
console.log("__BEGIN__");
