#!/usr/bin/env node

const yargs = require('yargs');
const definfn = "lorem.txt"
const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('infile', {
	  alias: 'i',
	  demandOption: true,
	  default: definfn,
	  describe: 'file to gzip',
	  type: 'string'
      })
      .option('ouyfile', {
	  alias: 'o',
	  demandOption: false,
	  describe: 'name of gzipped file',
	  type: 'string'
      })
      .usage("$0 -i <input filename> [-o <input filename>]")
      .help()
      .argv;

const infn = (argv['i'] === undefined) ? definfn : argv['i'];
const outfn = (argv['o'] === undefined) ? infn + ".gz" : argv['o'];

const fs = require('fs');
const zlib = require('zlib');
const gzip = zlib.createGzip();
const outStream = fs.createWriteStream(outfn);
fs.createReadStream(infn)
    .pipe(gzip)
    .pipe(outStream);
console.log(`File written to: ${outfn}`);

