#!/usr/bin/env node

const yargs = require('yargs');

class Percolator {
#sq = [];
#side;
#blocked;
#free;
#visited;
#hdiv;
#nu_blocked;
#nu_visited;
    constructor(s,b='X',f=' ',v='·',hd=' ') {
	this.#side = s;
	this.#blocked = b;
	this.#free = f;
	this.#visited = v;
	this.#hdiv = hd;
	this.init();
    }
    init() {
	this.#sq = [];
	for(let x = 0; x < this.#side; ++x) {
	    const row = [];
	    for(let y = 0; y < this.#side; ++y)
		row.push(this.#blocked);
	    this.#sq.push(row);
	}
	this.#nu_visited = 0;
	this.#nu_blocked = this.#side * this.#side;
    }
    scan_bottom() {
	const x = this.#side - 1;
	for (let y = 0; y < this.#side; ++y)
	    if (this.is_free(x,y))
		this.visit(x,y);
    }
    propagate_from_bottom(x) {
	for (let y = 0; y < this.#side; ++y)
	    if (this.is_visited(x+1,y))
		this.visit(x,y);
    }
    propagate_from_top(x) {
	for (let y = 0; y < this.#side; ++y)
	    if (this.is_visited(x-1,y))
		this.visit(x,y);
    }
    propagate_from_left(x) {
	for (let y = 1; y < this.#side; ++y)
	    if (this.is_visited(x,y-1))
		this.visit(x,y);
    }
    propagate_from_right(x) {
	for (let y = this.#side-2; y >= 0; --y)
	    if (this.is_visited(x,y+1))
		this.visit(x,y);
    }
    is_percolate() {
	const x = 0;
	for (let y = 0; y < this.#side; ++y)
	    if (this.is_visited(x,y))
		return true;
	return false;
    }
    backscan() {
	for(let x = 2; x < this.#side; ++x) {
	    this.propagate_from_top(x);
	    this.propagate_from_left(x);
	    this.propagate_from_right(x);
	}
    }
    scan() {
	this.scan_bottom();
	let prev_visited = -1;
	while(this.#nu_visited != prev_visited) {
	    prev_visited = this.#nu_visited;
	    for(let x = this.#side-2; x >= 0; --x) {
		this.propagate_from_bottom(x);
		this.propagate_from_left(x);
		this.propagate_from_right(x);
	    }
	    this.backscan();
	}
	return this.is_percolate();
    }
    free(x,y) {
	if (this.is_blocked(x,y)) {
	    this.#sq[x][y] = this.#free;
	    --this.#nu_blocked;
	    return true;
	}
	return false;
    }
    random_free() {
	let res = false;
	while(!res) {
	    const x = Math.floor(Math.random() * this.#side);
	    const y = Math.floor(Math.random() * this.#side);
	    res = this.free(x,y);
	}
    }
    is_blocked(x,y) { return this.#sq[x][y] == this.#blocked; }
    is_free(x,y) { return this.#sq[x][y] == this.#free; }
    is_visited(x,y) { return this.#sq[x][y] == this.#visited; }
    visit(x,y) {
	if (this.is_free(x,y)) {
	    this.#sq[x][y] = this.#visited;
	    ++this.#nu_visited;
	}
    }
    percolate() {
	let nu_steps = 0;
	while(!this.scan() && this.#nu_blocked > 0) {
	    if (argv["verbose"]) {
		this.print();
		console.log();
	    }
	    s.random_free();
	    ++nu_steps;
	}
	if(argv["show"] || argv["verbose"])
	    this.print();
	return nu_steps;
    }
    tests(num) {
	let results = [];
	for (let i = 0; i < num; ++i) {
	    results.push(this.percolate());
	    this.init();
	}
	console.log("Results:");
	console.log(results);
	let sum = 0
	results.forEach((item) => (sum += item));
	console.log("Average: " + sum / results.length);
	console.log("In percentage: " + ((sum / results.length) / (this.#side * this.#side))*100);
    }
    print_table() { console.table(this.#sq); }
    print() {
	//console.log();
	for(let x = 0; x < this.#side; ++x) {
	    let row = '';
	    for(let y = 0; y < this.#side; ++y)
		row += this.#hdiv + this.#sq[x][y];
	    console.log(row);
	}
    }
}

const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('size', {
	  alias: 's',
	  demandOption: false,
	  default: 10,
	  describe: 'size of square side',
	  type: 'number'
      })
      .option('tests', {
	  alias: 't',
	  demandOption: false,
	  default: 1,
	  describe: 'number of tests',
	  type: 'number'
      })
      .option('verbose', {
	  alias: 'v',
	  demandOption: false,
	  default: false,
	  describe: 'print the steps too',
	  type: 'boolean'
      })
      .option('show', {
	  //alias: 'v',
	  demandOption: false,
	  default: false,
	  describe: 'show solved',
	  type: 'boolean'
      })
      .help()
      .argv;

let s = new Percolator(argv["size"]);
if(argv["tests"] > 1)
    s.tests(argv["tests"]);
else {
    let steps = s.percolate();
    console.log("Number of steps: " + steps);
}
