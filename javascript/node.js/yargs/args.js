#!/usr/bin/env node

const yargs = require('yargs');
const argv = yargs
      .argv;
console.log("argv:");
console.log(argv);
console.log("i in argv");
for(i in argv) {
    console.log(i);
    console.log("  " + argv[i]);
}
console.log("Capture script name: " + argv["$0"]);
console.log("Capture the command line args: " + argv["_"]);
console.log("Command line args one by one:");
for(it of argv["_"]) {
    console.log(`  ${it}`);
}
