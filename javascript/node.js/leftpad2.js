#!/usr/bin/env node

// source: https://youtu.be/FyCYva9DhsI

const path = require('path');

function usage() {
    console.log(path.basename(process.argv[1]) + " str len ch");
}

function leftpad(str,len,ch) {
    str = String(str);
    ch = String(ch || ch === 0 ? ch : ' ')[0];
    const left = Math.max(len - str.length, 0);
    return ch.repeat(left) + str;
}

console.log("__BEGIN__");
const args = process.argv.slice(2);
if (args.length < 3) {
    usage();
    console.log("__END__");
    process.exit();
}
const str = args[0];
const len = parseInt(args[1]);
const ch = args[2];
console.log(leftpad(str,len,ch));
console.log("__END__");
