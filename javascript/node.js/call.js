#!/usr/bin/env node

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call

function Product(name, price, category="generic") {
    this.name = name;
    this.price = price;
    this.category = category;
    this.to_str = function() {
	return `name: ${this.name}, price: ${this.price}, category: ${this.category}`;
    }
}

function Food(name, price) {
    Product.call(this, name, price);
    this.category = 'food';
}

function Toy(name, price) {
    Product.call(this, name, price);
    this.category = 'toy';
}

const genprod = new Product('mumbo', 10);
console.log(genprod.to_str());
const cheese = new Food('feta', 5);
console.log(cheese.to_str());
const fun = new Toy('robot', 40);
console.log(fun.to_str());

// invoke anonymous function:
const animals = [
    { species: 'Lion', name: 'King' },
    { species: 'Whale', name: 'Fail' }
];

for (let i = 0; i < animals.length; i++) {
    (function(i) {
	this.print = function() {
	    console.log('#' + i + ' ' + this.species
			+ ': ' + this.name);
	}
	this.print();
    }).call(animals[i], i);
}

// call function with context:
function greet() {
    const reply = [this.animal, 'typically sleep between', this.sleepDuration].join(' ');
    console.log(reply);
}

const obj = {
    animal: 'cats', sleepDuration: '12 and 16 hours'
};

greet.call(obj);  // cats typically sleep between 12 and 16 hours

// call without args:
const sData = 'Wisen';

function display() {
  console.log('sData value is %s ', this.sData);
}

display.call();  // this does not work with modern node.js, where the default is strict mode
display.call({sData: sData});

console.log("__END__");
