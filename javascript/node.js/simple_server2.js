#!/usr/bin/env node

const http = require('http');
const whost = "localhost";
const portnu = 8080;
console.log("Listening on port: " + whost + ":" + portnu);

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('__END__');
}).listen(portnu,whost);
