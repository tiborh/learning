#!/usr/bin/env node

const path = require('path');
const Conv = require("./mod_celsfahr3");
const dec = 1;			// default decimal places
const arg0 = path.basename(process.argv[1]);
const arg2 = process.argv[2];	// number
const arg3 = process.argv[3];	// unit
const arg4 = process.argv[4];	// decimal places
const usage = arg0 + " <num> <C/F> [decimal places (default: " + dec + ")]";

if(arg2 !== undefined && arg3 !== undefined) {
    const conv = new Conv(Number(arg2),arg4 === undefined ? 1 : Number(arg4));
    if(arg3.toUpperCase() === 'C')
	console.log("%d°F",conv.c2f());
    else if (arg3.toUpperCase() === 'F')
	console.log("%f°C",conv.f2c());
} else {
    console.log(usage);
}

console.log("__END__");
