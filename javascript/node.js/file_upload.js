#!/usr/bin/env node

const http = require('http');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const mv = require('mv');

const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
  if (req.url == '/fileupload') {
    const form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
	const oldpath = files.filetoupload.path;
	console.log("old path: " + oldpath);
	const newpath = path.join(__dirname,'public',files.filetoupload.name);
	console.log("new path: " + newpath);
	// Error: EXDEV: cross-device link not permitted, rename
	// fs.rename(oldpath, newpath, function (err) {
        //     if (err) throw err;
        //     res.write('File uploaded and moved!');
        //     res.end();
	// });
	mv(oldpath, newpath, function (err) {
            if (err) throw err;
	    res.write('File uploaded and moved!');
            res.end();
	});
    });
  } else {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
      res.write('<input type="file" name="filetoupload"><br>');
      res.write('<input type="submit">');
      res.write('</form>');
      return res.end();
  }
}).listen(portnu);
