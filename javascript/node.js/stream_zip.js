#!/usr/bin/env node

const fs = require("fs");
const zlib = require('zlib');
const ifn = "japanese.txt";
const tmp = "stream_zip.txt";
const ofn = ifn + ".gz";

// Compress the file input.txt to input.txt.gz

fs.copyFile(ifn, tmp, (err) => { 
  if (err) { 
    console.log("Error Found:", err); 
  } 
});

fs.createReadStream(ifn)
   .pipe(zlib.createGzip())
   .pipe(fs.createWriteStream(ofn));
  
console.log(ifn + " compressed into " + ofn);
// fs.unlink(ifn, (err) => {
//     if (err) {
//         throw err;
//     }
// });
