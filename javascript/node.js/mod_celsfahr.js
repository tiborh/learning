module.exports = (function() {
    const diff = 32;
    const coef = 9/5;
    return {
	f2c: function(fahr) {
	    return  (fahr - diff) / coef;
	},
	c2f: function(cels) {
	    return cels * coef + diff;
	}
    };
})();
