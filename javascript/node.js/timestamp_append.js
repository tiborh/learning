#!/usr/bin/env node

const http = require('http');
const portnu = 8080;
console.log("Listening on port: " + portnu);
const ts = new Date();
console.log(ts);
const fs = require('fs');
const fn = "timestamp_append.txt"

fs.appendFile(fn, ts.toString() + "\n", function (err) {
  if (err) throw err;
  console.log('Saved!');
});

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(ts.toString());
    res.end();
}).listen(portnu);

// other options:
// create empty file:
// fs.open('mynewfile2.txt', 'w', function (err, file) {
//   if (err) throw err;
//   console.log('Saved!');
// });

// overwrite file if exists:
// fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
//   if (err) throw err;
//   console.log('Saved!');
// });

// delete file:
// fs.unlink('mynewfile2.txt', function (err) {
//   if (err) throw err;
//   console.log('File deleted!');
// });

// rename file:
// fs.rename('mynewfile1.txt', 'myrenamedfile.txt', function (err) {
//   if (err) throw err;
//   console.log('File Renamed!');
// });

// flags:
// Sr.No.	Flag & Description
// 1	
// r
// Open file for reading. An exception occurs if the file does not exist.

// 2	
// r+
// Open file for reading and writing. An exception occurs if the file does not exist.

// 3	
// rs
// Open file for reading in synchronous mode.

// 4	
// rs+
// Open file for reading and writing, asking the OS to open it synchronously. See notes for 'rs' about using this with caution.

// 5	
// w
// Open file for writing. The file is created (if it does not exist) or truncated (if it exists).

// 6	
// wx
// Like 'w' but fails if the path exists.

// 7	
// w+
// Open file for reading and writing. The file is created (if it does not exist) or truncated (if it exists).

// 8	
// wx+
// Like 'w+' but fails if path exists.

// 9	
// a
// Open file for appending. The file is created if it does not exist.

// 10	
// ax
// Like 'a' but fails if the path exists.

// 11	
// a+
// Open file for reading and appending. The file is created if it does not exist.

// 12	
// ax+
// Like 'a+' but fails if the the path exists.
