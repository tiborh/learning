#!/usr/bin/env node

const fs = require("fs");
const yargs = require('yargs');

const argv = yargs
      .argv;

console.log("length of argv: " + argv["_"].length);
//console.log("Going to get file info!");
for(it of argv["_"]) {
    fs.stat(it, function (err, stats) {
	if (err) {
	    return console.error(err);
	}
	console.log(stats);
	console.log("Got file info successfully!");
	
	// Check file type
	console.log("isFile?            " + stats.isFile());
	console.log("isDirectory?       " + stats.isDirectory());
	console.log("isBlockDevice?     " + stats.isBlockDevice());
	console.log("isCharacterDevice? " + stats.isCharacterDevice());
	console.log("isSymbolicLink?    " + stats.isSymbolicLink()); // does not work
	console.log("isFIFO?            " + stats.isFIFO());
	console.log("isSocket?          " + stats.isSocket());
    });
}
