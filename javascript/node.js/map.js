#!/usr/bin/env node

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map

const path = require('path');
const gen = require('./mod_gen_arr');
const num =  5;
const max = 10;
const min =  0;
const num_of_nums = process.argv[2] === undefined ? num : Number(process.argv[2]);
const max_num = process.argv[3] === undefined ? max : Number(process.argv[3]);
const min_num = process.argv[4] === undefined ? min : Number(process.argv[4]);
const usage = `Usage:\n\t${path.basename(process.argv[1])} [num_of_nums (default: ${num})] [max_num (exclusive) (default: ${max})] [min_num (inclusive) (default: ${min})]`;
const array1 = gen.nums(num_of_nums,max_num,min_num);
const array2 = gen.nums(num_of_nums,max_num,min_num);

if (process.argv[2] === undefined)
    console.info(usage);
console.log("__BEGIN__");
console.log("Starting with:");
console.log(array1);
console.log("Twice:");
const map1 = array1.map(x => x * 2);
console.log(map1);

const map2 = array1.map((x,ind) => x + ind);
console.log("index added:");
console.log(map2);

const map3 = array1.map((x,ind,arr) => ind === 0 ? x : x + arr[ind-1]);
console.log("plus previous:");
console.log(map3);

const map4 = array1.map(function(x,ind,arr) { return(x+this[ind]); }, array2);
console.log("add the following:");
console.log(array2);
console.log(map4);

const map5 = array1.map(function(x,ind) {
    if(ind < 3)
	return x;
});
console.log("filtered output:");
console.log(map5);

let kvArray = [{key: 1, value: 10},
               {key: 2, value: 20},
               {key: 3, value: 30}]
console.log("original object:");
console.log(kvArray);
let reformattedArray = kvArray.map(obj => {
   let rObj = {}
   rObj[obj.key] = obj.value
   return rObj
})
console.log("reformatted object:");
console.log(reformattedArray);

const hello_str = "Hello World!";
console.log(`Original string: ${hello_str}`);
let map = Array.prototype.map
let hello_codes = map.call(hello_str, function(x) {
  return x.charCodeAt(0)
})
console.log("character codes:");
console.log(hello_codes);

console.log("__END__");
