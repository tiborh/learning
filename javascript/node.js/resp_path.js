#!/usr/bin/env node

const http = require('http');
const portnu = 1234;
const whost = "localhost";
const connect = require('connect');
const _end = "__END__";

const app = connect().use(function(req,res){
    res.write("<p>");
    res.write(req.url);
    res.write("</p>");
    res.end(_end);
}).listen(portnu);
console.log("Listening on port: " + whost + ":" + portnu);
