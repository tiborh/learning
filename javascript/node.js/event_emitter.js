#!/usr/bin/env node

// source:
// https://www.tutorialspoint.com/nodejs/nodejs_event_loop.htm

console.log("0. Import events module");
const events = require('events');

console.log("1. Create an eventEmitter object");
const eventEmitter = new events.EventEmitter();

console.log("2. Create an event handler");
const connectHandler = function connected() {
   console.log('connection succesful.');
  
   // Fire the data_received event 
   eventEmitter.emit('data_received');
}

console.log("3. Bind the connection event with the handler");
eventEmitter.on('connection', connectHandler);
 
console.log("4. Bind the data_received event with the anonymous function");
eventEmitter.on('data_received', function() {
   console.log('data received succesfully.');
});

console.log("5. Fire the connection event");
eventEmitter.emit('connection');

console.log("__END__");
