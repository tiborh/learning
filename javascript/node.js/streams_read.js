#!/usr/bin/env node

const fs = require("fs");
const fn = "japanese.txt";
let data = '';

// Create a readable stream
const readerStream = fs.createReadStream(fn);

// Set the encoding to be utf8. 
readerStream.setEncoding('UTF8');
//readerStream.setEncoding('hex');

// Handle stream events --> data, end, and error
readerStream.on('data', function(chunk) {
    //console.log("chunk: " + chunk);
    data += chunk;
});

readerStream.on('end',function() {
    console.log(data);
    console.log("__END__");
});

readerStream.on('error', function(err) {
   console.log(err.stack);
});

console.log("__BEGIN__");
