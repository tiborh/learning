#!/usr/bin/env node

console.log("__BEGIN__");
const events = require('events');
const net = require('net');
const channel = new events.EventEmitter();
const portnu = 8888;
console.log("Listening on port: " + portnu);
channel.clients = {};
channel.subscriptions = {};
channel.setMaxListeners(2);	// warning only

channel.on('join', function(id, client) {
    // Adds a listener for the join event
    // that stores a user’s client
    // object, allowing the application
    // to send data back to the user
    const welcome = `Welcome!\r\nGuests online: ${this.listeners('broadcast').length}\r\n`;
    client.write(`${welcome}\n`);
    this.clients[id] = client;
    this.subscriptions[id] = (senderId, message) => {
	if (id != senderId) {
	    // Ignores data if it’s been
	    // directly broadcast by the user
	    this.clients[id].write(message);
	}
    };
    // Adds a listener, specific
    // to the current user, for
    // the broadcast event:
    this.on('broadcast', this.subscriptions[id]);
});

channel.on('leave', function(id) {
    // Creates listener for leave event
    channel.removeListener(
	'broadcast', this.subscriptions[id]
    );
    // Removes broadcast listener for specific client:
    channel.emit('broadcast', id, `${id} has left the chatroom.\r\n`);
});

channel.on('shutdown', () => {	//  does not seem to be effective (putty telnet)
    channel.emit('broadcast', '', 'The server has shut down.\n');
    channel.removeAllListeners('broadcast');
});

const server = net.createServer(client => {
    const id = `${client.remoteAddress}:${client.remotePort}`;
    // Emits a join event when a user
    // connects to the server, specifying
    // the user ID and client object:
    channel.emit('join', id, client);
    client.on('data', data => {
	data = data.toString();
	if (data === 'shutdown\r\n') {
	    channel.emit('shutdown');
	}
	// Emits a channel broadcast event,
	// specifying the user ID and message,
	// when any user sends data:
	channel.emit('broadcast', id, data);
    });
    client.on('close', () => {
	// Emits leave event when
	// client disconnects
	channel.emit('leave', id);
    });
    
});
server.listen(portnu);
console.log("__END__");
