#!/usr/bin/env node

const yargs = require('yargs');
const deftxt = "default text";
const argv = require('yargs/yargs')(process.argv.slice(2))
      .option('spacer', {
	  alias: 's',
	  demandOption: true,
	  default: ' ',
	  describe: 'spacer character or string',
	  type: 'string'
      })
      .usage("$0 [-s <spacer char>] string(s)")
      .help()
      .argv;
function spacer(txt,ch) {
    return txt.split("").join(ch);
}

if (argv._.length == 0)
    console.log(spacer(deftxt,argv['s']));
else
    for (let it of argv._)
	console.log(spacer(it,argv['s']));

//console.log(argv);
//console.log(argv._);
