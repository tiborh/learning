#!/usr/bin/env node

const path = require('path');
const celsfahr = require("./mod_celsfahr");
const c2f = 'c2f.js';
const f2c = 'f2c.js';
const argv2 = process.argv[2];

if(argv2 !== undefined) {
    const input = Number(argv2);
    if(path.basename(process.argv[1]) === c2f)
	console.log("%d°F",celsfahr.c2f(input));
    else if (path.basename(process.argv[1]) === f2c)
	console.log("%f°C",celsfahr.f2c(input).toFixed(1));
}

console.log("__END__");
