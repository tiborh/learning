#!/usr/bin/env node

const fs = require("fs");
const fm = 'r+';
const bufsize = 1024;

const yargs = require('yargs');
const argv = yargs
      .argv;

const fn = argv["_"].length == 0 ? "japanese.txt" : argv["_"][0];

// Asynchronous - Opening File
console.log("Opening file: '" + fn + "'");
fs.open(fn, fm, function(err, fd) {
   if (err) {
      return console.error(err);
   }
    console.log("'" + fn + "' opened successfully");

    const buf = Buffer.alloc(bufsize);

    fs.read(fd, buf, 0, buf.length, 0, function(err, bytes){
	if (err){
            console.log(err);
	}
	console.log(bytes + " bytes read");
	
	// Print only read bytes to avoid junk.
	if(bytes > 0){
            console.log(buf.slice(0, bytes).toString());
	}
	fs.close(fd, function(err) {
            if (err) {
		console.log(err);
            } 
            console.log("'" + fn + "' closed successfully.");
	});
    });
});
