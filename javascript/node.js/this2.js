#!/usr/bin/env node

const what_is_this = (function () {
    const alpha = 'a';
    const beta = 'b';
    let strit = function () {return alpha + " " + beta;};
    return{
	inside: this,
	pasted: strit
    };
})();

console.log(what_is_this.inside);
console.log(what_is_this.pasted());
