#!/usr/bin/env node

function what_is_this() {
    const alpha = 'a';
    const beta = 'b';
    console.log(alpha + " " + beta);
    this.m = 'm';		// adding a global or window variable
    return this;
}

console.log(what_is_this());
