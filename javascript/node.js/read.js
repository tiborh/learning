#!/usr/bin/env node

const fs = require("fs");
const yargs = require('yargs');

const argv = require('yargs/yargs')(process.argv.slice(2))
  .option('file', {
      alias: 'f',
      default: 'read_file.txt',
      describe: 'read file'
  })
  .option('buffer', {
      alias: 'b',
      default: 1024,
      describe: 'buffer size'
  })
  //.demandOption(['run', 'path'], 'Please provide both run and path arguments to work with this tool')
  .help()
  .argv

console.log("Read buffer size: " + argv["buffer"]);
if (argv["buffer"] <= 0) {
    console.log("Read buffer must be greater than zero.");
    process.exit();
}

const fn = argv["file"];
const buf = Buffer.alloc(argv["buffer"]);
const fm = 'r+';

console.log("Trying to open: '" + fn + "'");
fs.open(fn, fm, function(err, fd) {
   if (err) {
      return console.error(err);
   }
   console.log("File opened successfully!");
   console.log("Going to read the file");
   
   fs.read(fd, buf, 0, buf.length, 0, function(err, bytes) {
      if (err) {
         console.log(err);
      }

      // Print only read bytes to avoid junk.
      if(bytes > 0) {
         console.log(buf.slice(0, bytes).toString());
      }

      // Close the opened file.
      fs.close(fd, function(err) {
         if (err) {
            console.log(err);
         } 
         console.log("File closed successfully.");
      });
   });
});
