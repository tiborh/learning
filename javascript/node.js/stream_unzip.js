#!/usr/bin/env node

const fs = require("fs");
const zlib = require('zlib');

const ifn = "japanese.txt.gz";
const ofn = "stream_unzip.txt";

// Decompress the file input.txt.gz to input.txt
fs.createReadStream(ifn)
   .pipe(zlib.createGunzip())
   .pipe(fs.createWriteStream(ofn));
  
console.log(ifn + " decompressed to " + ofn);


