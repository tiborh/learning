#!/usr/bin/env node

const fs = require("fs");
const fn = "japanese.txt";
const fm = 'r+';

// Asynchronous - Opening File
console.log("Opening file: '" + fn + "'");
fs.open(fn, fm, function(err, fd) {
   if (err) {
      return console.error(err);
   }
    console.log("'" + fn + "' opened successfully");

    fs.close(fd, function(err) {
        if (err) {
	    console.log(err);
        } 
        console.log("'" + fn + "' closed successfully.");
    });
});
