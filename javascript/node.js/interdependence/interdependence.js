#!/usr/bin/env node
let value = "the value of value";
exports.value = value;		// pass by value
const sub = require('./sub');
console.log(`sub value: ${sub.value}`);
console.log(`own value: ${value}`);
console.log("__END of interdependence.js (in interdependence)__");
