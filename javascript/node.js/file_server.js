#!/usr/bin/env node

const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');

const portnu = 8080;
console.log("Listening on port: " + portnu);

http.createServer(function (req, res) {
    const q = url.parse(req.url, true);
    //console.log(q);
    const fn = q.pathname;
    console.log("fn: " + fn);
    const fp = path.join(__dirname,'public',fn);
    console.log("file path: " + fp);
    
    fs.readFile(fp, function(err, data) {
	if (err) {
	    res.writeHead(404, {'Content-Type': 'text/html'});
	    return res.end("404 Not Found");
	} 
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(data);
	return res.end();
    });
}).listen(portnu);
