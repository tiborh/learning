#!/usr/bin/env node

//> (node:19205) [DEP0005] DeprecationWarning: Buffer() is deprecated due to security and usability issues. Please use the Buffer.alloc(), Buffer.allocUnsafe(), or Buffer.from() methods instead.
//(Use `node --trace-deprecation ...` to show where the warning was created)

console.log("Allocate 10 Octets:");
//var buf = new Buffer(10);
let buf = Buffer.alloc(10);

console.log(buf);

console.log("Storing a 漢字:");
buf = Buffer.from("冬");

console.log(buf);
console.log(buf.toString());

console.log("Cctets from an Array (with dec->hex conversion)");
//var buf = new Buffer([10, 20, 30, 40, 50]);
buf = Buffer.from([33, 43, 53, 63, 73, 83, 93, 103]);

console.log(buf);
console.log(buf.toString());

console.log("Character Encoding: utf-8");
//var buf = new Buffer("Simply Easy Learning", "utf-8");
buf = Buffer.from("Simply Easy Learning", "utf-8");

console.log(buf);
console.log(buf.toString());

console.log("write returns a value:");
//buf = new Buffer(256);
buf = Buffer.alloc(256);
let len = buf.write("Simply Easy Learning");

console.log("Octets written: " + len);

console.log("Reading from buffers:");
//buf = new Buffer(26);
buf = Buffer.alloc(26);
for (var i = 0 ; i < 26 ; i++) {
  buf[i] = i + 97;
}

// help: buf.toString([encoding][, start][, end])
console.log( buf.toString('ascii'));       // outputs: abcdefghijklmnopqrstuvwxyz
console.log( buf.toString('ascii',0,5));   // outputs: abcde
console.log( buf.toString('utf8',0,5));    // outputs: abcde
console.log( buf.toString(undefined,0,5)); // encoding defaults to 'utf8', outputs abcde

console.log("Convert Buffer to JSON");
//var buf = new Buffer('Simply Easy Learning');
buf = Buffer.from('Simply Easy Learning');
let json = buf.toJSON(buf);

console.log(json);

// {
//   type: 'Buffer',
//   data: [
//      83, 105, 109, 112, 108, 121,
//      32,  69,  97, 115, 121,  32,
//      76, 101,  97, 114, 110, 105,
//     110, 103
//   ]
// }

console.log("Concatenate Buffers:");
//var buffer1 = new Buffer('TutorialsPoint ');
let buffer1 = Buffer.from('TutorialsPoint ');
//var buffer2 = new Buffer('Simply Easy Learning');
let buffer2 = Buffer.from('Simply Easy Learning');
let buffer3 = Buffer.concat([buffer1,buffer2]);

console.log("buffer3 content: " + buffer3.toString());

console.log("Compare Buffers:");
//var buffer1 = new Buffer('ABC');
buffer1 = Buffer.from('ABC');
//var buffer2 = new Buffer('ABCD');
buffer2 = Buffer.from('ABCD');
let result = buffer1.compare(buffer2);

if(result < 0) {
   console.log(buffer1 +" comes before " + buffer2);
} else if(result === 0) {
   console.log(buffer1 +" is same as " + buffer2);
} else {
   console.log(buffer1 +" comes after " + buffer2);
}

//var buffer1 = new Buffer('ABC');
buffer1 = Buffer.from('ABC');

console.log("Copy a Buffer:");
//var buffer2 = new Buffer(3);
buffer2 = Buffer.alloc(3);
buffer1.copy(buffer2);
console.log("buffer2 content: " + buffer2.toString());

//var buffer1 = new Buffer('TutorialsPoint');
buffer1 = Buffer.from('TutorialsPoint');

console.log("Slicing a Buffer:");
//var buffer2 = buffer1.slice(0,9);
buffer2 = buffer1.slice(0,9);
console.log("buffer2 content: " + buffer2.toString());

console.log("Buffer Length:");
//var buffer = new Buffer('TutorialsPoint');
buffer = Buffer.from('TutorialsPoint');

//length of the buffer
console.log("buffer length: " + buffer.length);
