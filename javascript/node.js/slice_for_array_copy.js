#!/usr/bin/env node

function print_all(all) {
    for(let a_key of Object.keys(all))
	console.log(`${a_key} == ${all[a_key]}`);
}

const a = [1,2,3];
console.log(`a == ${a} (const a = [1,2,3])`);
const b = a;
console.log(`b == ${b} (const b = a)`);
const c = a.slice();
console.log(`c == ${c} (const c = a.slice())`);
const all = {a: a, b: b, c: c};
//console.log(`all == ${all} (const all = {a: a, b: b, c: c})`);
a[0] = 0;
console.log("a[0] changed to 0 (zero)");
print_all(all);
