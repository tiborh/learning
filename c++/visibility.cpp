#include <iostream>

// friend classes and functions can access private (but otherwise, only the class itself where they are resided

struct Y {
  //public: // can remain implicit as default for struct
  Y(): y(0),x(0) {}
  void printXY(void) {
    std::cout << "x == " << x << ", ";
    std::cout << "y == " << y << std::endl;
  }
protected:
  int x;
private:
  int y;
};

class X {
  //private: // can remain implicit as default for class
  int x;
protected:
  int y;			// subclasses can access this one
public:
  X() = default;
};

// subclass of X:
class Xx: public X {		// it can only access public members of X
  
};

struct Yy: protected Y{
  // public:
  Yy() { Y::x = 1; }
  Yy(int num) { Y::x = num; }
  void printX(void) {
    std::cout << "x == " << x << std::endl;
  }
  void print_parent() {printXY();}
};

int main(int argc, char** argv) {

  Y y;				// warning: empty parentheses interpreted as a function declaration (when trying Y y()
  y.printXY();
  
  Yy ya;
  ya.printX();

  Yy yy(3);
  yy.printX();
  // yy.printXY(); // protected
  yy.print_parent();
  
  return 0;
}
