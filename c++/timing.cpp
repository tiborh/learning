#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <iomanip>
#include <limits>

#define MAXNUM 100

void counter(int n=MAXNUM) {
  for (int i = 0; i < n; ++i);
}

int main(int argc, char** argv) {
  using namespace std::literals::chrono_literals;

  int num;
  
  if (argc > 1)
    num = atoi(argv[1]);
  else
    num = MAXNUM;
  
  auto start = std::chrono::high_resolution_clock::now();
  //std::this_thread::sleep_for(1s);
  counter(num);
  auto end = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double> duration = end - start;

  std::cout << std::fixed;
  std::cout << std::setprecision(std::numeric_limits<double>::digits10 + 1); // float is too short and double and long double are too long
  std::cout << "Time difference (" << num << "): " << duration.count() << "s" << std::endl;
  
  return 0;
}
