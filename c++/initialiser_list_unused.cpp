#include <iostream>

struct X {
  X(int x = 0): _x(x) {
    std::cout << "Constructor called.\n";
  }
  int _x;
};

struct Y {
  Y(int x = 0) { _x = X(x); }
  void use() { std::cout << "used\n"; }
  X _x;
};

struct Z {
  Z(int x = 0): _x(x) {}
  void use() { std::cout << "used\n"; }
  X _x;
};

int main(int argc, char** argv) {

  std::cout << "Without initialiser list Y:\n";
  Y y;
  y.use();

  std::cout << "with initialiser list Z:\n";
  Z z;
  z.use();

  
  return 0;
}
