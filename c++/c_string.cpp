#include <iostream>
#include <cstring>

void print_str(const char* instr) {
  int len = strlen(instr);
  for (int i = 0; i < len; ++i)
    std::cout << instr[i] << '\n';
  std::cout << std::endl;
}

void usage(const char* progname) {
  std::cout << "Usage:\n\t" << progname << " <a string>" << std::endl;
  exit(0);
}

int main(int argc, char** argv) {

  if (argc < 2)
    usage(argv[0]);
    
  const char* constr = argv[1];

  std::cout << constr << std::endl;
  print_str(constr);
  
  return 0;
}
