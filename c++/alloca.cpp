#include <iostream>

class Arr {
private:
  int* _arr;
  size_t _len;
public:
  Arr(size_t len): _len(len) { _arr = (int*)alloca(_len*sizeof(int)); }
  int arr_len() const { return(_len); }
  int& operator[](size_t index) { return(_arr[index]); }
  friend std::ostream& operator<<(std::ostream& out, const Arr& obj) {
    size_t obj_len = obj.arr_len();
    out << "< ";
    for (size_t i = 0; i < obj_len; ++i)
      out << obj._arr[i] << " ";
    out << ">";
    return(out);
  }
};

int main(int argc, char** argv) {

  size_t len = 5;
  Arr ar(len);
  for (size_t i = 0; i < len; ++i)
    ar[i] = i+1;
  std::cout << "array length: " << ar.arr_len() << std::endl;
  std::cout << "allocated area size: " << len*sizeof(int) << std::endl;
  std::cout << ar << std::endl;
  std::cout << "It does not work well.\n";

  // valgrind result:
  // ==17799== Invalid write of size 4
  // ==17799==    at 0x400A14: main (alloca.cpp:26)
  // ...
  // ==17799== Invalid read of size 4
  // ==17799==    at 0x400BDC: operator<<(std::ostream&, Arr const&) (alloca.cpp:15)
  // ==17799==    by 0x400AA7: main (alloca.cpp:29)

  
  return 0;
}
