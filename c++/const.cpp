#include <iostream>

class X {
  int _x,_y;
  mutable int _v;
public:
  // cannot modify the class variables
  int getx() const {
    _v = 2;			// as _v is mutable
    return _x;
  }
  int gety() const {
    return _y;
  }

  // X& to prevent copy, const X& to signal it will not be changed
  // note: const here is only possible if the called methods are also const
  friend std::ostream& operator<<(std::ostream& stream, const X& obj) {
    stream << "<" << obj.getx() << "," << obj.gety() << ">";
    return stream;
  }
};

int main(int argc, char** argv) {

  // simplest:
  const int MAXNUM = 100;	    // cannot modify its value after assignment.
  int num = MAXNUM;
  
  // next level: pointers
  int* a = new int;
  std::cout << "a: " << *a << std::endl;
  // two things can be changed:
  // 1. value:
  *a = 2;
  std::cout << "a: " << *a << std::endl;
  // to prevent this type of change:
  // const int* a; or int const* a;
  // 2. the reference:
  a = &num;
  std::cout << "a: " << *a << std::endl;
  // to prevent this change:
  // int* const a;
  return 0;
}
