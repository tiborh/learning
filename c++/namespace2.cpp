#include <iostream>
#include <string>		// std::string
#include <algorithm>		// std::reverse

namespace my_space {
  void print(const char* t) {
    std::cout << t << std::endl;
  }
}

namespace my_reverse_space {
  void print(const std::string& t) {
    std::string temp = t;
    std::reverse(temp.begin(),temp.end());
    std::cout << temp << std::endl;
  }
}

using namespace my_space;
using namespace my_reverse_space;

int main(int argc, char** argv) {
  print(argv[0]);		// in my_space
  print(std::string(argv[0]));	// in my_reverse_space
  return 0;
}
