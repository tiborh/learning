#include <iostream>
#include <cassert>

template<typename T>
class DynArr {
public:
  DynArr(): _objID(objCount++) {
    std::cout << "DynArr empty construct. (objID:" << _objID << ")\n";
    _realloc(2);
  }
  DynArr(const DynArr& other): _objID(objCount++) {
    std::cout << "DynArr copy construct. (from objID:" << other._objID << " to objID: " << _objID << ")\n";
    _realloc(other._full_capac);
    for (size_t i = 0; i < other._fill_level; ++i)
      _data_held[i] = other._data_held[i];
  }
  ~DynArr() {
    clear();
    ::operator delete(_data_held, _full_capac * sizeof(T));
    std::cout << "DynArr destroyed. (objID:" << _objID << ")\n\n";
  }
  void push_back(const T& new_elem) {
    std::cout << "DynArr copy push: " << new_elem << "\n";
    if (_fill_level >= _full_capac)
      _realloc(_full_capac*2);
    _data_held[_fill_level++] = new_elem;
  }
  void push_back(T&& new_elem) {
    std::cout << "DynArr move push: " << new_elem << "\n";
    if (_fill_level >= _full_capac)
      _realloc(_full_capac*2);
    _data_held[_fill_level++] = std::move(new_elem);
  }
  void pop_back(void) {
    if (_fill_level > 0)
      _fill_level--;
    _data_held[_fill_level].~T();
  }
  void clear(void) {
    for(size_t i = 0; i < _fill_level; ++i)
      _data_held[i].~T();
    _fill_level = 0;
  }
  template<typename... Args>
  T& emplace_back(Args&&... args) {
    std::cout << "emplace_back called\n";
    if (_fill_level >= _full_capac)
      _realloc(_full_capac*2);
    new(&_data_held[_fill_level]) T(std::forward<Args>(args)...);
    return(_data_held[_fill_level++]);
  }
  
  size_t len(void) const { return _fill_level; }
  size_t cap(void) const { return _full_capac; }
  const T& operator[](size_t index) const {
    assert(index < _fill_level);
    return(_data_held[index]);
  }
  T& operator[](size_t index) {
    assert(index < _fill_level);
    return(_data_held[index]);
  }
  friend std::ostream& operator<<(std::ostream& outstrm, const DynArr obj) {
    size_t len = obj.len();
    for (size_t i = 0; i < len; ++i)
      outstrm << obj[i] << "\n";
    outstrm << "DynArr objID:" << obj._objID << "\n";
    return(outstrm);
  }
private:
  T* _data_held = nullptr;
  size_t _fill_level = 0;
  size_t _full_capac = 0;
  size_t _objID;
  static size_t objCount;
  void _realloc(size_t new_capac) {
    std::cout << "Realloc started:\n";
    assert(new_capac > _full_capac);
    // 1. allocate new
    T* new_data = (T*)::operator new(new_capac * sizeof(T));
    // if (new_capac < _fill_level)
    //   _fill_level=new_capac;
    // 2. copy/move over old
    for (size_t i = 0; i < _fill_level; ++i) {
      new_data[i] = std::move(_data_held[i]);
      std::cout << "alive\n";
    }
    // 3. do away with old container
    for (size_t i = 0; i < _fill_level; ++i)
      _data_held[i].~T();
    ::operator delete(_data_held, _full_capac * sizeof(T));
    // 4. set up new data
    _data_held = new_data;
    _full_capac = new_capac;
    std::cout << "Realloc success.\n";
    std::cout << "Fill level: " << _fill_level << std::endl;
    std::cout << "Full capac: " << _full_capac << std::endl;
  }
};

template<typename T>
size_t DynArr<T>::objCount = 0;

struct Point {
  float x=0,y=0,z=0;
  Point(): objID(objCount++) {
    std::cout << "created: " << *this << "\n";
  }
  Point(float val): x(val),y(val),z(val),objID(objCount++) {
    std::cout << "created: " << *this << "\n";
  }
  Point(float x, float y, float z): x(x),y(y),z(z),objID(objCount++) {
    std::cout << "created: " << *this << "\n";
  }
  Point(const Point& other): x(other.x),y(other.y),z(other.z),objID(objCount++) {
    std::cout << "Point copied from " << other << " to " << *this << "\n";
  }
  Point(Point&& other): x(other.x),y(other.y),z(other.z),objID(objCount++) {
    std::cout << "Point moved from " << other << " to " << *this << "\n";
  }
  ~Point() { std::cout << "Point destroyed. (objID:" << objID << ")\n"; }
  Point& operator=(const Point& other) {
    std::cout << "Point copy assigned from " << other << " to " << *this << "\n";
    x=other.x;
    y=other.y;
    z=other.z;
    return *this;
  }
  static void reset_count() {
    objCount=0;
  }
  friend std::ostream& operator<< (std::ostream& stream, const Point& obj) {
    stream << "<" << obj.x << "," << obj.y << "," << obj.z << "> (objID:" << obj.objID << ")";
    return stream;
  }
private:
  size_t objID;
  static size_t objCount;
};

struct Compl {
  Compl() = default;
  Compl(size_t s) {
    mblock = new int[s];
    bsize = s;
    init();
  }
  Compl(const Compl& other) {
    mblock = new int[other.bsize];
    bsize = other.bsize;
    init(other.mblock[0]);
  }
  Compl(Compl&& other) {
    mblock = other.mblock;
    bsize = other.bsize;
    other.mblock = nullptr;
    other.bsize = 0;
  }
  ~Compl() { delete[] mblock; }
  void init(int v = 0) {
    for(size_t i = 0; i < bsize; ++i)
      mblock[i] = v;
  }
  Compl& operator=(const Compl& other) {
    if (bsize != other.bsize) {
      delete[] mblock;
      mblock = new int[other.bsize];
      bsize = other.bsize;
    }
    for (size_t i = 0; i < bsize; ++i)
      mblock[i] = other.mblock[i];
    return *this;
  }
  friend std::ostream& operator<<(std::ostream& outstrm, const Compl obj) {
    size_t len = obj.bsize;
    for (size_t i = 0; i < len; ++i)
      outstrm << obj.mblock[i] << " ";
    return(outstrm);
  }
private:
  int* mblock = nullptr;
  size_t bsize = 0;  
};

size_t Point::objCount = 0;

int main(int argc, char** argv) {

  {
    std::cout << "DynArr dry run (create/destroy)\n";
    DynArr<std::string> poem2;
    std::cout << poem2;
    DynArr<std::string> poem3;
    std::cout << poem3;
    std::cout << "End of block.\n";
  }
  // {
  //   std::cout << "Realloc test: push_back only\n";
  //   DynArr<std::string> poem;
  //   poem.push_back("you were with me");
  //   poem.push_back("and my life was full");
  //   poem.push_back("you are no longer with me");
  //   poem.push_back("and my life is");
  //   poem.push_back("full");
  //   poem.push_back("(based on rupi kaur's poem)");
  //   std::cout << "Length: " << poem.len() << std::endl;
  //   std::cout << "Capacity: " << poem.cap() << std::endl;
  //   std::cout << poem << std::endl;
  //   std::cout << "End of block.\n";
  // }
  // {
  //   std::cout << "Point array creation:\n";
  //   DynArr<Point> points;
  //   std::cout << "Empty constructor push:\n";
  //   points.push_back(Point());
  //   std::cout << "One-member constructor push:\n";
  //   points.push_back(Point(1));
  //   std::cout << "Three-member constructor push:\n";
  //   points.push_back(Point(1,2,3));
  //   std::cout << "Length: " << points.len() << std::endl;
  //   std::cout << "Capacity: " << points.cap() << std::endl;
  //   std::cout << points << std::endl;
  //   std::cout << "Clearing out vector:\n";
  //   points.clear();
  //   std::cout << "End of block.\n";
  // }
  {
    Point::reset_count();
    std::cout << "Same as before but with 'EmplaceBack'\n";
    std::cout << "Point array creation:\n";
    DynArr<Point> points;
    std::cout << "Empty constructor push:\n";
    points.emplace_back();
    std::cout << "One-member constructor push:\n";
    points.emplace_back(1.0f);
    std::cout << "Three-member constructor push:\n";
    points.emplace_back(1,2,3);
    std::cout << "Length: " << points.len() << std::endl;
    std::cout << "Capacity: " << points.cap() << std::endl;
    std::cout << points << std::endl;
    std::cout << "Clearing out vector:\n";
    points.clear();
    std::cout << "End of block.\n";
  }
  {
    Point::reset_count();
    std::cout << "Point experiments:\n";
    std::cout << "Create without value:\n";
    Point z;
    std::cout << "Point z: " << z << std::endl;
    std::cout << "Create with block assignment:\n";
    Point a = {1,2,3};
    std::cout << "Point a: " << a << std::endl;
    std::cout << "Create with constructor:\n";
    Point b(2,3,4);
    std::cout << "Point b: " << b << std::endl;
    std::cout << "Create from an existing one:\n";
    Point c = b;
    std::cout << "Point c: " << c << std::endl;
    std::cout << "re-assignment:\n";
    a = c;
    std::cout << "Point a: " << a << std::endl;
  }
  {
    std::cout << "Compl creation:\n";
    size_t s = 4;
    Compl c(s);
    std::cout << c << std::endl;
    std::cout << "end of block\n";
  }
  {
    std::cout << "Compl array creation:\n";
    DynArr<Compl> arrays;
    std::cout << "Pusing four elements:\n";
    arrays.push_back(Compl(1));
    arrays.push_back(Compl(2));
    arrays.push_back(Compl(3));
    arrays.push_back(Compl(4));
    std::cout << "Length: " << arrays.len() << std::endl;
    std::cout << "Capacity: " << arrays.cap() << std::endl;
    std::cout << "Popping two elements:\n";
    arrays.pop_back();
    arrays.pop_back();
    std::cout << "Length: " << arrays.len() << std::endl;
    std::cout << "Capacity: " << arrays.cap() << std::endl;
    std::cout << "Pusing three elements:\n";
    arrays.push_back(Compl(1));
    arrays.push_back(Compl(2));
    arrays.push_back(Compl(3));
    std::cout << "Length: " << arrays.len() << std::endl;
    std::cout << "Capacity: " << arrays.cap() << std::endl;
    std::cout << "End of block.\n";   
  }

  
  return 0;
}
