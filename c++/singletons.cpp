#include <iostream>

// singleton: instantiate once (or configure like a closure),
//            then use it multiple times

struct Sngltn {
  Sngltn(const Sngltn&) = delete; // to prevent: Sngltn instnc = Sntltn::get(); (copy construction)
  static Sngltn& get(void) { return _instnc; } // a strange way to instantiate
  void use(void) {}		// this one instantiates, and would not be needed in a true sinleton, or should be "static" 
private:
  Sngltn() = default;
  static Sngltn _instnc;
};

Sngltn Sngltn::_instnc;

int main(int argc, char** argv) {
  Sngltn::get().use();
  Sngltn& instnc = Sngltn::get();	// workaround for private and deleted constructors, auto& is also possible
  instnc.use();
  return 0;
}
