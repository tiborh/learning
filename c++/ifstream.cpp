#include <iostream>
#include <fstream>
#include <string>

// ifstream infile(filename)
// alternatively:
// ifstream infile;
// infile.open(filename);

void read_by_char(std::string fn) {
  std::ifstream fh(fn);
  char c = fh.get();
  std::cout << "Reading by character:\n";
  std::cout << "---------------------\n";
  while (fh.good()) {
    std::cout << c;
    c = fh.get();
  }
  std::cout << "---------------------\n";
  fh.close();
}

void redirect_from_file0(std::string fn) {
  std::ifstream fh(fn);
  std::string fc;
  std::cout << "Redirect from file0:\n";
  std::cout << "--------------------\n";
  fh >> fc;
  while (fh.good()) {
    std::cout << fc << std::endl;
    fh >> fc;
  }
  std::cout << "--------------------\n";
  fh.close();
}

void redirect_from_file1(std::string fn) {
  std::ifstream fh(fn);
  std::string fc;
  std::cout << "Redirect from file0:\n";
  std::cout << "--------------------\n";
  while (fh >> fc) {
    std::cout << fc << std::endl;
    fh >> fc;
  }
  std::cout << "--------------------\n";
  fh.close();
}

void read_by_line(std::string fn) {
  std::ifstream fh(fn);
  std::string ln;
  std::getline(fh,ln);
  std::cout << "Reading by line:\n";
  std::cout << "----------------\n";
  while (fh.good()) {
    std::cout << ln << std::endl;
    std::getline(fh,ln);
  }
  std::cout << "----------------\n";
  fh.close();
}

void read_by_delimiter(std::string fn, char delim) {
  std::ifstream fh(fn);
  std::string ln;
  std::getline(fh,ln,delim);
  std::cout << "Reading by delimiter (" << delim << "):\n";
  std::cout << "-------------------------\n";
  while (fh.good()) {
    std::cout << ln << std::endl;
    std::getline(fh,ln,delim);
  }
  std::cout << "-------------------------\n";
  fh.close();
}

void read_till_eof(std::string fn) {
  std::ifstream fh(fn);
  std::string ln;
  std::getline(fh,ln);
  std::cout << "Reading till end:\n";
  std::cout << "-----------------\n";
  while (!fh.eof()) {
    std::cout << ln << std::endl;
    std::getline(fh,ln);
  }
  std::cout << "-----------------\n";
  fh.close();
}


void file_read_wrapper(std::string fn) {
  read_by_char(fn);
  redirect_from_file0(fn);
  redirect_from_file1(fn);
  read_by_line(fn);
  read_till_eof(fn);
  read_by_delimiter(fn,' ');
}

int main(int argc, char** argv) {
  std::string fn = "data/test.txt";
  file_read_wrapper(fn);

  return 0;
}
