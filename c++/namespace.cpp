#include <iostream>
#include <string>		// std::string
#include <algorithm>		// std::reverse

namespace my_space {
  void print(const char* t) {
    std::cout << t << std::endl;
  }
}

namespace my_reverse_space {
  void print(const std::string& t) {
    std::string temp = t;
    std::reverse(temp.begin(),temp.end());
    std::cout << temp << std::endl;
  }
}

int main(int argc, char** argv) {
  for (int i = 0; i < argc; ++i) {
    if (argc % 2)
      my_space::print(argv[i]);
    else
      my_reverse_space::print(argv[i]);
  }
  return 0;
}
