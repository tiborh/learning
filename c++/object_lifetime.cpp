#include <iostream>

struct X {
  X() {
    ID = counter++;
    std::cout << "X (" << ID << "): created.\n";
  }
  ~X() {
    std::cout << "X (" << ID << "): destroyed.\n";
  }
  void use() {}
private:
  unsigned ID;
  static unsigned counter;
};

struct Y {
  Y(X* ptr) : m_ptr(ptr) {
    std::cout << "Y: created.\n";
  }
  ~Y() {
    std::cout << "Y: to be destroyed.\n";
    delete(m_ptr);
    std::cout << "Y: destroyed.\n";
  }
private:
  X* m_ptr;
};

unsigned X::counter = 0;

X* func() {
  std::cout << "func: starts\n";
  X x;
  X* x2 = new X();
  std::cout << "func: ends\n";
  return x2;
}

int main(int argc, char** argv) {

  std::cout << "main: starts\n";

  X x;
  
  {
    std::cout << "unnamed block: starts\n";
    X x;			// stack based
    X* x2 = new X();		// heap based, when the block ends, it becomes inaccessible and undestroyable
    x2->use();			// to avoid: error: unused variable 'y' [-Werror,-Wunused-variable]
    Y y = new X();		// to make sure it gets auto-destroyed. (stack variable)
    std::cout << "unnamed block: ends\n";
  }
  std::cout << "after unnamed block\n";

  X* x2 = func();
  std::cout << "after func\n";
  x2->use();
  delete(x2);
  std::cout << "after manual deletion\n";
  
  std::cout << "main: ends\n";
  
  return 0;
}
