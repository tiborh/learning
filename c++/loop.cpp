#include <iostream>

int main() {
  while(1)
    ;
}

// with -O1, infinite loop got optimised and the hello() function also executes. (but only with clang++

void hello() {
  std::cout << "Hello!\n";
}
