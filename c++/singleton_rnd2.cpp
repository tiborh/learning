#include <iostream>

struct Rnd {
  Rnd(const Rnd&) = delete; // to prevent: Rnd instnc = Sntltn::get(); (copy construction)
  static float gen_float() { return get()._fake; }
  
private:
  Rnd() = default;
  float _fake = 0.64479874f;
  static Rnd _instnc;
  static Rnd& get(void) { return _instnc; } // a strange way to instantiate
};

Rnd Rnd::_instnc;

int main(int argc, char** argv) {

  float r = Rnd::gen_float();
  std::cout << "The fake rnd: " << r << std::endl;
  
  return 0;
}
