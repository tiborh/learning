#include <iostream>

struct S {
  S() = default;
  S(std::string n):_name(n) {}
  void use(void){}
  std::string _name;
};

int main(int argc, char** argv) {
  S a = std::string("a"); 	// works because there is a string constructor
  a.use();
  //S b = "b";			// would not work because it would require two implicit conversions:
  				// 1. from const char to string, 2. from string to object S
  				// error: no viable conversion from 'const char [2]' to 'S'
  S b = (S)"b";			// with explicit casting, it works
  b.use();
  std::cout << "See explanation in source.\n";
  
  
  return 0;
}
