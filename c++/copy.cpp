#include <iostream>

struct Pair {
  Pair(int x=0, int y=0): x(x),y(y),_objID(_objCount) {
    std::cout << "constructor called (objID:" << _objID << ")" << std::endl;
    ++_objCount;
  }
  Pair(const Pair& other): x(other.x),y(other.y),_objID(_objCount) {
    std::cout << "copy constructor called (objID:" << _objID << ")" << std::endl;
    ++_objCount;
  }
  ~Pair() {
    std::cout << "destructor called on objID:" << _objID << std::endl;
  }
  int x,y;
  Pair operator++(void) {
    ++x;
    ++y;
    return(Pair(x,y));
  }
  Pair operator--(void) {
    --x;
    --y;
    return(Pair(x,y));
  }
  Pair operator++(int) {
    Pair output = {x,y};
    ++x;
    ++y;
    return(output);
  }
  Pair operator--(int) {
    Pair output = {x,y};
    --x;
    --y;
    return(output);
  }
  friend std::ostream& operator<<(std::ostream& out,const Pair& obj) {
    return(out << "<" << obj.x << "," << obj.y << "> (objID:" << obj._objID << ")");
  }
private:
  unsigned _objID;
  static unsigned _objCount;
};

unsigned Pair::_objCount = 0;

int main(int argc, char** argv) {
  int a = 2;
  int b = a;			// copying the value of a into b
  // a and b are independent of each other
  ++b;
  std::cout << "a: " << a << ", b: " << b << std::endl;

  Pair c = { a,b };
  std::cout << "c created: " << c << std::endl;
  Pair d = c;
  std::cout << "d created: " << d << std::endl;

  std::cout << "c (pre)decremented: " << --c << std::endl;
  std::cout << "after state: " << c << std::endl;

  std::cout << "d (pre)incremented: " << --d << std::endl;
  std::cout << "after state: " << d << std::endl;

  std::cout << "c (post)decremented: " << c-- << std::endl;
  std::cout << "after state: " << c << std::endl;

  std::cout << "d (post)incremented: " << d++ << std::endl;
  std::cout << "after state: " << d << std::endl;
  
  Pair e;
  std::cout << "e created: " << e << std::endl;

  Pair* pe = &e;
  std::cout << "pointer to e created: " << pe << std::endl;
  std::cout << "address of e: " << &e << std::endl;
  std::cout << "address of pointer: " << &pe << std::endl;
  
  return 0;
}
