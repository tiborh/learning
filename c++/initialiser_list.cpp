#include <iostream>

struct X {
  // order is important:
  // error: field '_z' will be initialized after field '_y'
  X(int x=0,int y=0,int z=0): _x(x),_y(y),_z(z) {}
  int _x,_y,_z;
  friend std::ostream& operator<<(std::ostream& os, const X& obj) {
    return(os << "<" << obj._x << "," << obj._y << "," << obj._z << ">");
  }
};

int main(int argc, char** argv) {

  X xa;
  std::cout << "xa == " << xa << std::endl;
  X xb(1,2,3);
  std::cout << "xb == " << xb << std::endl;
  
  return 0;
}
