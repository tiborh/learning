#include <iostream>
#include <cstring>

class Str {
public:
  // Str() = default; results in segfault
  Str(const char* str="") {
    step_id();
    m_size = strlen(str)+1;
    m_data = new char[m_size];
    memcpy(m_data,str,m_size);
    std::cout << "Created: Str object. (" << m_data << ", ID:" << m_objID << ")" << std::endl;
  }
  Str(const Str& other) {
    step_id();
    m_size = other.m_size;
    m_data = new char[m_size];
    memcpy(m_data,other.m_data,m_size);
    std::cout << "Copied: Str object. (" << m_data << ", ID:" << m_objID << ")" << std::endl;
  }
  Str(Str&& other) /* noexcept */ {
    step_id();
    m_size = other.m_size;
    m_data = other.m_data;
    other.m_size = 0;
    other.m_data = nullptr;
    std::cout << "Moved: Str object. (" << m_data << ", ID:" << m_objID << ")" << std::endl;
  }
  ~Str() {
    std::cout << "Destroying (ID:" << m_objID << "): " << (m_data == nullptr ? "<empty>" : m_data) << std::endl;
    delete[](m_data);
  }
  Str& operator=(const Str& other) {
    if (this == &other)
      return(*this);
    if (other.m_size != m_size) {
      delete[](m_data);
      m_size = other.m_size;
      m_data = new char[m_size];      
    }
    memcpy(m_data,other.m_data,m_size);
    std::cout << "Copy assignment: Str object. (" << m_data << ", ID:" << m_objID << ")" << std::endl;
    return(*this);
  }
  Str& operator=(Str&& other) {	// move assignment operator
    if (this != &other) {
      delete[](m_data);
      step_id();
      m_size = other.m_size;
      m_data = other.m_data;
      other.m_size = 0;
      other.m_data = nullptr;
      std::cout << "Assigment operator: Str object. (" << m_data << ", ID:" << m_objID << ")" << std::endl;
    }
    return(*this);
  }
  friend std::ostream& operator<< (std::ostream& stream, const Str& obj) {
    stream << obj.m_data << " (ID:" << obj.m_objID << ")";
    return stream;
  }
protected:
  void step_id() {
    m_objID = m_counter++;
  }
private:
  char* m_data;
  uint32_t m_size;
  size_t m_objID;
  static size_t m_counter;
};

size_t Str::m_counter = 0;

class Entity {
public:
  Entity(const Str& name): m_name(name) {}
  Entity(Str&& name): m_name(std::move(name)) {}
  friend std::ostream& operator<< (std::ostream& stream, const Entity& obj) {
    stream << obj.m_name;
    return stream;
  }
private:
  Str m_name;
};

int main(int argc, char** argv) {
  Entity an_ent("Ulysses");
  std::cout << "The entity: " << an_ent << std::endl;

  Str str = "Hello";
  std::cout << "str: " << str << std::endl;
  Str dest = str;		// copies it
  std::cout << "dest: " << dest << std::endl;
  Str dest2 = (Str&&)str;	// a way of moving instead of copying
  std::cout << "dest2: " << dest2 << std::endl;
  Str dest3((Str&&)dest);	// another way, not failsafe
  std::cout << "dest3: " << dest3 << std::endl;
  Str dest4(std::move(dest2));	// the standard way of moving
  std::cout << "dest4: " << dest4 << std::endl;
  Str dest5 = std::move(dest3);	// the standard way of moving
  std::cout << "dest5: " << dest5 << std::endl;

  Str str2;
  str2 = std::move(dest4);	// move assignment operator is needed
  std::cout << "str2: " << str2 << std::endl;

  Str apple = "Apple";
  Str apfel;
  apfel = apple;		// copy assignment operator is needed
  std::cout << "apfel: " << apfel << std::endl;
  
  return 0;
}
