#include <iostream>
#include <cstdlib> 
#include <ctime>

const int JMAX = 4;
const int KMAX = 5;
const int MAXRAND = 100;

void print_arr(int, const int[]);
void print_2d(int, const int[][KMAX]);
void print_3d(int, const int[][JMAX][KMAX]);
void fill_arr(int, int[]);
void fill_2d(int, int[][KMAX]);
void fill_3d(int, int[][JMAX][KMAX]);

int main(int argc, char** argv) {
  srand((unsigned)time(0));
  const int IMAX = 3;
  int arr3d[IMAX][JMAX][KMAX] = {{{0}}};
  std::cout << "After creation:\n";
  print_3d(IMAX,arr3d);
  fill_3d(IMAX,arr3d);
  std::cout << "After filling in:\n";
  print_3d(IMAX,arr3d);
  return 0;
}

void fill_arr(int nrow, int arr[]) {
  for (int i = 0; i < nrow; ++i) {
    arr[i] = rand() % MAXRAND;
  }
}

void print_arr(int nrow, const int arr[]) {
  for (int i = 0; i < nrow; ++i) {
    std::cout << '\t' << arr[i] << ',';
  }
}

void print_2d(int nrow, const int arr2[][KMAX]) {
  for (int j = 0; j < nrow; ++j) {
    print_arr(KMAX,arr2[j]);
    std::cout << '\n';
  }
}

void print_3d(int nrow, const int arr3[][JMAX][KMAX]) {
  for (int i = 0; i < nrow; ++i) {
    print_2d(JMAX,arr3[i]);
    std::cout << "\n";
  }
  std::cout << std::endl;
}

void fill_2d(int nrow, int arr2[][KMAX]) {
  for (int j = 0; j < nrow; ++j) {
    fill_arr(KMAX,arr2[j]);
  }
}

void fill_3d(int nrow, int arr3[][JMAX][KMAX]) {
  for (int i = 0; i < nrow; ++i) {
    fill_2d(JMAX,arr3[i]);
  }
}
