#include <iostream>
#include <vector>

int main(int argc, char** argv) {
  struct AnObject {
    AnObject(int n) {
      a = n;
      std::cout << "Object #" << a << " has been created" << std::endl;
    }
    ~AnObject() {
      std::cout << "Object #" << a << " has been destroyed" << std::endl;
    }
    int a = 0;
  };  

  std::vector<AnObject*> vObjs;

  for (int i = 1; i < 8; ++i)
    vObjs.push_back(new AnObject(i));

  vObjs.clear();		// does not free up the objects
  
  system("read -p \"press [Enter] to go on\"");  		// for demonstrative purposes only
  return 0;
}
