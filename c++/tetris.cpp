#include <iostream>

// source:
// https://youtu.be/8OK8_tHeCIA
// Code-It-Yourself! Tetris - Programming from Scratch (Quick and Simple C++)
// or git:
// https://github.com/OneLoneCoder/videos/blob/master/OneLoneCoder_Tetris.cpp

#define WIDTH 4
#define NFIELDWIDTH  12
#define NFIELDHEIGHT 18
#define NSCREENWIDTH 80
#define NSCREENHEIGHT 25;
const int CORNERS[] = {0,3,12,15};
unsigned char *pField = nullptr;

std::wstring tetronimo[7];	// wide string of wchar_t elements

// Logic:
//      0  1  2  3
//     -----------
// 0 |  0  1  2  3
// 1 |  4  5  6  7
// 2 |  8  9 10 11
// 3 | 12 13 14 15
//
// instead of: a[x][y]
// i = y * w + x (where w == width)
// so, a[2][2] == 2 * 4 + 2 == 10
//
// Right-rotated (90 deg):
//      0  1  2  3
//     -----------
// 0 | 12  8  4  0
// 1 | 13  9  5  1
// 2 | 14 10  6  2
// 3 | 15 11  7  3
//
// i = 12 + y - (x * 4)
// a[1][2] == 12 + 2 - (1 * 4) == 10
//
//      0  1  2  3
//     -----------
// 0 | 15 14 13 12
// 1 | 11 10  9  8
// 2 |  7  6  5  4
// 3 |  3  2  1  0
// 
// 180 degree:
// i = 15 - (y * 4) - x
// a[2][1] == 15 - (1 * 4) - 2 == 9
// 
//      0  1  2  3
//     -----------
// 0 |  3  7 11 15
// 1 |  2  6 10 14
// 2 |  1  5  9 13
// 3 |  0  4  8 12
//
// 270 degree:
// i = 3 - y + x * 4
// a[2][3] == 3 - 3 + (2 * 4) == 8

void create_shapes(void) {
  tetronimo[0].append(L"..X.");
  tetronimo[0].append(L"..X.");
  tetronimo[0].append(L"..X.");
  tetronimo[0].append(L"..X.");

  tetronimo[1].append(L"..X.");
  tetronimo[1].append(L"..XX");
  tetronimo[1].append(L"..X.");
  tetronimo[1].append(L"....");

  tetronimo[2].append(L".X..");
  tetronimo[2].append(L".XX.");
  tetronimo[2].append(L"..X.");
  tetronimo[2].append(L"....");

  tetronimo[3].append(L"....");
  tetronimo[3].append(L".XX.");
  tetronimo[3].append(L".XX.");
  tetronimo[3].append(L"....");

  tetronimo[4].append(L"..X.");
  tetronimo[4].append(L".XX.");
  tetronimo[4].append(L".X..");
  tetronimo[4].append(L"....");

  tetronimo[5].append(L"....");
  tetronimo[5].append(L".XX.");
  tetronimo[5].append(L"..X.");
  tetronimo[5].append(L"..X.");

  tetronimo[6].append(L"....");
  tetronimo[6].append(L".XX.");
  tetronimo[6].append(L".X..");
  tetronimo[6].append(L".X..");
}

int rotate(int px, int py, int r) {
  switch (r % WIDTH) {
  case 0: return py * WIDTH + px;              //   0 degrees
  case 1: return CORNERS[2] + py - px * WIDTH; //  90 degrees
  case 2: return CORNERS[3] - py * WIDTH -px;  // 180 degrees
  case 3: return CORNERS[1] - py + px * WIDTH; // 270 degrees
  default: return 0;
  }
}


int main(int argc, char** argv) {
  create_shapes();
  pField = new unsigned char[NFIELDWIDTH * NFIELDHEIGHT]; // create play field
  for(int i = 0; x < NFIELDWIDTH; ++i) // board boundary
    for(int j = 0; j < NFIELDHEIGHT; ++j)
      pField[y * NFIELDWIDTH + x] = (x == 0 || x == NFIELDWIDTH - 1 || y == NFIELDHEIGHT - 1) ? 9 : 0;

  wchar_t* screen = new wchar_t[NSCREENWIDTH * NSCREENHEIGHT];
  for(int i; i < NSCREENWIDTH * NSCREENHEIGHT; ++i) screen[i] = L' ';
  // windows stuff from windows.h starting here, need to find linux equivalent
  //HANDLE hConsole =
  // need to study this one:
  // https://youtu.be/xW8skO7MFYw
  // Code-It-Yourself! First Person Shooter (Quick and Simple C++)
  // https://github.com/OneLoneCoder/CommandLineFPS
  std::cout << "to be continued..." << std:endl; 
  
  return 0;
}
