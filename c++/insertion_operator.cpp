#include <iostream>
#include <string>

int main(int argc, char** argv) {
  std::cout << "Insertion operator << reads one word a time.\n";
  std::string inp;
  do {
    std::cin >> inp;
    std::cout << std::hex << "|" << inp << "|";
    std::cout << " (" << inp.length() << ")" << std::endl;
  }while(inp != "q" && inp != "Q");
  
  return 0;
}
