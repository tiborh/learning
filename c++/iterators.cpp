#include <iostream>
#include <vector>
#include <unordered_map>

int main(int argc, char** argv) {

  std::vector<int> values = {6,5,4,3,2,1};

  // does not work:
  // std::cout << "vector: " << values << std::endl;

  // one way:
  std::cout << "Vector with for-loop:\n";
  for (int i = 0; i < values.size(); ++i)
    std::cout << values[i] << " ";
  std::cout << std::endl;

  // a simpler way, which uses the iterator implicitly
  std::cout << "Vector with for(each) (or ranged-based) loop:\n";
  for(int value: values)
    std::cout << value << " ";
  std::cout << std::endl;

  // using the iterator explicitly:
  std::cout << "Vector with iterator:\n";
  for (std::vector<int>::iterator it = values.begin();
       it != values.end(); ++it)
    std::cout << *it << " ";
  std::cout << std::endl;

  // reverse iterator:
  std::cout << "Vector with reverse_iterator:\n";
  for (std::vector<int>::reverse_iterator it = values.rbegin();
       it != values.rend(); ++it)
    std::cout << *it << " ";
  std::cout << std::endl;

  std::unordered_map<char,int> mpairs;

  mpairs['a'] = 10;
  mpairs['b'] = 20;
  mpairs['c'] = 30;
  mpairs['d'] = 40;
  mpairs['e'] = 50;
  mpairs['f'] = 60;

  // a simplification:
  using UnorderedCharMap = std::unordered_map<char,int>;
  // iterating a map (no short hand)
  // (const because we do not want to change values):
  std::cout << "Map with iterator:\n";
  for (UnorderedCharMap::const_iterator it = mpairs.cbegin();
       it != mpairs.cend(); ++it) {
    auto& key = it->first;
    auto& val = it->second;
    std::cout << key << " => " << val << '\n';
  }

  std::cout << "Map with auto pairs:\n";
  // further simplification:
  for (auto a_pair: mpairs) {
    auto& key = a_pair.first;
    auto& val = a_pair.second;
    std::cout << key << " => " << val << '\n';
  }

  std::cout << "Map with structured binding (C++17 feature):\n";
  // further simplification:
  for (auto [k,v]: mpairs)
    std::cout << k << " => " << v << '\n';

  
  return 0;
}
