#include <iostream>
#include <vector>
#include <sstream>
#include <string>

struct Vert {
  float x,y,z = {};
  std::string to_str(void) const {
    std::stringstream ss;
    ss << "<" << x << "," << y << "," << z << ">";
    return(ss.str());
  }
  friend std::ostream& operator<< (std::ostream& stream, const Vert& obj) { return stream << obj.to_str(); }
};

std::string vect_to_string(const std::vector<Vert>& invect) {
  std::string ostr;
  for (std::vector<Vert>::const_iterator i = invect.begin(); i != invect.end(); ++i) {
    ostr.append(i->to_str());
    if (i + 1 != invect.end())	// this is why 'for(const Vert& v: invect)' was not used
      ostr.append(" ");
  }
  return(ostr);
}

int main(int argc, char** argv) {
  std::vector<Vert> verts;
  std::cout << "The vector: '" << vect_to_string(verts) << "'\n";
  verts.push_back({1,2,3});
  verts.push_back({4,5,6});
  verts.push_back({7,8,9});    
  std::cout << "The vector: '" << vect_to_string(verts) << "'\n";

  verts.erase(verts.begin()+1);
  std::cout << "Second erased: " << vect_to_string(verts) << std::endl;
  verts.clear();
  std::cout << "The whole cleared: '" << vect_to_string(verts) << "'\n";
  
  return 0;
}
