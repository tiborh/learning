#include <iostream>

const char* def_name = "anonymous";
const int def_age = -1;

struct P {
  P(): _name(def_name),_age(def_age),_objID(_obj_count) { ++_obj_count; }
  P(const char* n): _name(n),_age(def_age),_objID(_obj_count) { ++_obj_count; }
  P(int a): _name(def_name),_age(a),_objID(_obj_count) { ++_obj_count; } // std::string has "const char" constructor, so not implicit
  friend std::ostream& operator<<(std::ostream& out, const P& obj) {
    return(out << "(objID:" << obj._objID << ") name: \"" << obj._name << "\",\t age: " << obj._age);
  }
  ~P() { std::cout << "~P() destructor called on objID:" << _objID <<".\n"; }
  std::string _name;
  int _age;
private:
  unsigned _objID;
  static unsigned _obj_count;
};

unsigned P::_obj_count = 0;

void print(P obj) {
  std::cout << obj << std::endl;
}


int main(int argc, char** argv) {
  // ways to create a variable
  P a;
  print(a);
  P b(28);			// most common usage
  print(b);
  P c = P("Abe Ba");		// very explicit
  c._age = int(2);		// rare
  print(c);
  P d = 32;			// implicit conversion from int to P
  d._name = "William";
  print(d);
  print(22);			// even more unexpected implicit conversion
  print("Eve");
  
  return 0;
}
