#include <iostream>
#include <string>
#include <tuple>
#include <utility>

std::pair<std::string,std::string> sample_func(void) {
  std::string str1 = "first";
  std::string str2 = "second";

  return( std::make_pair(str1,str2) );
}

std::tuple<std::string,int> entage(const char* n = "Entity", int a = 12) { // with two values std::pair could also be used
  return { n,a };
}

int main(int argc, char** argv) {

  std::cout << "with pair and make_pair:\n";
  std::pair<std::string,std::string> returned = sample_func();
  std::cout << std::get<0>(returned) << " " << std::get<1>(returned) << std::endl; // a strange way
  // std::cout << returned.first << " " << returned.second << std::endl; // could be better, but just a little.

  std::cout << "a more minimalist example with std::tuple and without std::make_pair\n";
  std::tuple<std::string,int> e = entage(); // auto type could also be used
  std::cout << std::get<0>(e) << " " << std::get<1>(e) << std::endl;

  std::string name;
  int age;
  std::tie(name,age) = entage(); // perhaps, a little better

  std::cout << "name: " << name << ", age: " << age << std::endl;

  // the c++17 way:
  auto[obj,years] = entage("something else",13);
  std::cout << "obj: " << obj << ", years: " << years << std::endl;
  
  return 0;
}
