#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

class Device {};

class DeviceManager {
private:
  std::unordered_map<std::string, std::vector<Device *>> m_devices;
public:
  const std::unordered_map<std::string, std::vector<Device *>>& getDevices() const {
    return(m_devices);
  }
};

int main(int argc, char** argv) {

  // this is also a possible solution for long names:
  // using DeviceMap = const std::unordered_map<std::string, std::vector<Device *>>;
  // or
  // typedef const std::unordered_map<std::string, std::vector<Device *>> DeviceMap;
  
  DeviceManager dm;
  // in place of:
  // const std::unordered_map<std::string, std::vector<Device *>>&
  // we can use "using" or "typedef" above, pared with:
  // const DeviceMap&
  // or we can use:
  const auto& devices = dm.getDevices();
  for (auto [k,v]: devices)
    std::cout << "never printed" << std::endl;
  
  return 0;
}
