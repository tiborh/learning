#include <iostream>
using namespace std;

int main() {

  // buffer on stack
  unsigned char buf[sizeof(int)*3];

  // placement new in buffer
  int *pInt = new (buf) int(3);

  pInt[0] = 0;
  pInt[1] = 1;
  pInt[2] = 2;
  
  return 0;
}
