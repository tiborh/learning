#include <iostream>

template <typename T>
void print_arr(const T& arr) {
  std::cout << arr[0] << "," << arr[1] << "," << arr[2] << std::endl;
}

int main(int argc, char** argv) {
  int arr1[3] = {0};
  print_arr(arr1);
  int arr2[3] = {1};
  print_arr(arr2);
  char arr3[3] = {'a'};
  print_arr(arr3);
}
