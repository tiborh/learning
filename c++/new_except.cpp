#include <iostream>

void* operator new(std::size_t sz) {
  // std::malloc(0) can return nullptr on success
  if(sz == 0)
    ++sz;

  if(void *ptr = std::malloc(sz))
    return ptr;

  //return nullptr; // the difference from malloc
  throw std::bad_alloc();
}

void operator delete(void* ptr) noexcept {
  std::free(ptr);
}

int main() {
  std::cout << "throw version of new:\n";
  try {
    while(true) {
      new int[100000000ul];
    }
  } catch(const std::bad_alloc& e) {
    std::cout << e.what() << '\n';
  }

  std::cout << "non-throw version of new:\n";
  while(true) {
    int* p = new(std::nothrow) int[100000000ul];
    if(p == nullptr) {
      std::cout << "Allocation returned nullptr\n";
      break;
    }
  }
}

