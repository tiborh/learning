#include <iostream>

struct Pair {
  float x;
  float y;
  Pair(float x, float y): x(x),y(y) {}
  Pair operator+(const Pair& other) const {
    return(Pair(x + other.x, y + other.y));
  }
  Pair operator*(const Pair& other) const {
    return(Pair(x * other.x, y * other.y));
  }
  bool operator==(const Pair& other) const {
    if (this == &other)
      return true;
    return(x == other.x && y == other.y);
  }
  bool operator!=(const Pair& other) const {
    return(!(*this == other));
  }
  friend std::ostream& operator<<(std::ostream& stream, const Pair& obj) {
    stream << "<" << obj.x << "," << obj.y << ">";
    return stream;
  }
};

int main(int argc, char** argv) {

  Pair a = {12,34};
  Pair b = {5,3};
  Pair c = {1.1,1.3};
  Pair d = {5,3};
  std::cout << "a: " << a << std::endl;
  std::cout << "b: " << b << std::endl;
  std::cout << "c: " << c << std::endl;
  std::cout << "d: " << d << std::endl;
  std::cout << "a + b * c: " << a + b * c << std::endl;
  std::cout << "a == a?: " << (a==a) << std::endl;
  std::cout << "a == b?: " << (a==b) << std::endl;
  std::cout << "b == d?: " << (b==d) << std::endl;
  std::cout << "a != a?: " << (a!=a) << std::endl;
  std::cout << "a != b?: " << (a!=b) << std::endl;
  std::cout << "b != d?: " << (b!=d) << std::endl;
  
  return 0;
}
