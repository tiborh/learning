#include <iostream>

struct Point {
  float x,y,z;
  Point(): x(0),y(0),z(0) {}
  Point(float inx,float iny,float inz): x(inx),y(iny),z(inz) {}
  friend std::ostream& operator<< (std::ostream& stream, const Point& obj) {
    stream << "<" << obj.x << "," << obj.y << "," << obj.z << ">";
    return stream;
  }
};

int main(int argc, char** argv) {

  Point A;
  Point B(12,14,-5);
  Point C = {1,2,3};
  std::cout << "struct is the simplest way to group variables\n";
  std::cout << "Point A: " << A << "\n";
  std::cout << "Point B: " << B << "\n";
  std::cout << "Point C: " << C << "\n";
  
  return 0;
}
