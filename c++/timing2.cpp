#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <iomanip>
#include <limits>

#define MAXNUM 100

struct Timer {
  std::chrono::time_point<std::chrono::system_clock> start,end;
  std::chrono::duration<double> duration;
  
  Timer() {
    start = std::chrono::high_resolution_clock::now();
  }
  ~Timer() {
    end = std::chrono::high_resolution_clock::now();
    duration = end - start;
    double ms = (float)duration.count() * 1000;
    std::cout << std::fixed;
    std::cout << std::setprecision(std::numeric_limits<float>::digits10 + 1); // do not need double full precision to display
    std::cout << "Time difference: " << ms << "ms" << std::endl;
  }
};

void counter(int n=MAXNUM) {
  Timer timer;
  for (int i = 0; i < n; ++i);
}

int main(int argc, char** argv) {
  using namespace std::literals::chrono_literals;

  int num;
  
  if (argc > 1)
    num = atoi(argv[1]);
  else
    num = MAXNUM;
  
  counter(num);
    
  return 0;
}
