#include <iostream>
#include <cstring>

class Str {
public:
  Str(): _len(0) {
    _str = new char[1];
    _str[0] = '\0';
  }
  Str(const char* str) {
    init(str);
  }
  Str(const Str& str): _len(str._len) {
    std::cout << "Copy constructor called.\n";
    _str = new char[_len + 1];
    strcpy(_str,str._str);
  }
  ~Str() {
    delete[](_str);
  }
  char& operator[](unsigned index) {
    if (_len-1 < index)
      throw "Index is too great.";
    else
      return(_str[index]);
  }
  void operator=(const char* str) {
    this->~Str();
    this->init(str);
  }
  unsigned get_len(void) { return _len; }
  friend std::ostream& operator<<(std::ostream& out,const Str& obj) {
    return(out << obj._str);
  }
private:
  char* _str;
  unsigned _len;
  void init(const char* str) {
    _len = strlen(str);
    _str = new char[_len + 1];
    memcpy(_str,str,_len);
    _str[_len] = '\0';
  }
};

void needless_copy(Str instr) {
  std::cout << "In needles copy function: " << instr << std::endl;
}

void no_copy(const Str& instr) {
  std::cout << "In no copy function: " << instr << std::endl;
}

int main(int argc, char** argv) {
  Str a = "Lorem ipsum dolor sit amet.";
  std::cout << "Str a: " << a << std::endl;
  Str b = a;
  std::cout << "Str b: " << b << std::endl;
  b = "consectetur adipiscing elit.";
  std::cout << "Str b (  changed): " << b << std::endl;
  std::cout << "Str a (unchanged): " << a << std::endl;
  a[0] = 'l';
  std::cout << "Str a (  changed): " << a << std::endl;
  std::cout << "Str b (unchanged): " << b << std::endl;
  try {
    std::cout << "Testing exception handling:\n";
    b[b.get_len()] = 'x';		// to test exception
   } catch (const char* msg) {
    std::cerr << msg << std::endl;
   }

  needless_copy(a);
  no_copy(b);
  
  return 0;
}
