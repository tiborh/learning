#include <iostream>

void* operator new(std::size_t sz) {
  // std::malloc(0) can return nullptr on success
  if(sz == 0)
    ++sz;

  if(void *ptr = std::malloc(sz))
    return ptr;

  //return nullptr; // the difference from malloc
  throw std::bad_alloc();
}

void operator delete(void* ptr) noexcept {
  std::free(ptr);
}

int main() {
  int* p1 = new int;
  if(nullptr == p1)
    std::cout << "nullptr has been returned\n";
  else {
    delete p1;
    std::cout << "created and deleted with no issue\n";
  }
}
