#include <iostream>

struct Log {
  Log() = delete;
  static void write() {
    std::cout << "logged\n";
  }
};

int main(int argc, char** argv) {
  // Log x;			// not allowed;
  Log::write();

  return 0;
}
