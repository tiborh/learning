#include <iostream>
#include <cstring>

class Str {
public:
  Str(): _len(0) {
    _str = new char[1];
    _str[0] = '\0';
  }
  Str(const char* str) {
    _len = strlen(str);
    _str = new char[_len + 1];
    memcpy(_str,str,_len);
    _str[_len] = '\0';
  }
  Str(const Str& str) = delete;
  ~Str() {
    delete[](_str);
  }
  unsigned get_len(void) { return _len; }
  friend std::ostream& operator<<(std::ostream& out,const Str& obj) {
    return(out << obj._str);
  }
private:
  char* _str;
  unsigned _len;
};

int main(int argc, char** argv) {
  Str a = "Lorem ipsum dolor sit amet.";
  std::cout << "Str a: " << a << std::endl;
  //Str b = a;			// if this is attempted, you get the following error:
  // error: call to deleted constructor of 'Str'
  
  return 0;
}
