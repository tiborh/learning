#include <iostream>
#include <random>
#include <cstdlib>
#include <cassert>

const int DEFAULT_NUM=10;
const int DEFAULT_MIN=1;
const int DEFAULT_MAX=DEFAULT_NUM;

int* create_random_integers(int num=DEFAULT_NUM,
			    int mini=DEFAULT_MIN,
			    int maxi=DEFAULT_MAX) {
  assert(maxi >= mini);
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(mini, maxi);

  int* out_arr = new int[num];
  
  for(int i=0; i < num; ++i)
    out_arr[i] = dist(mt);
  
  return(out_arr);
}

int main(int argc, char** argv) {
  int maxi = (argc > 3) ? atoi(argv[3]) : DEFAULT_MAX;
  int mini = (argc > 2) ? atoi(argv[2]) : DEFAULT_MIN;
  int num  = (argc > 1) ? atoi(argv[1]) : DEFAULT_NUM;

  // std::cout << "num == " << num << std::endl;
  // std::cout << "mini == " << mini << std::endl;
  // std::cout << "maxi == " << maxi << std::endl;
  
  int* res = create_random_integers(num,mini,maxi);
  for (int i = 0; i < num; ++i)
    std::cout << *(res+i) << " ";
  std::cout << std::endl;
  delete[](res);
  return 0;
}
