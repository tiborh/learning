#include <iostream>
#include <cstring>

class Str {
public:
  Str() = default;
  Str(const char* str) {
    step_id();
    m_size = strlen(str)+1;
    m_data = new char[m_size];
    memcpy(m_data,str,m_size);
    std::cout << "Created: Str object. (" << m_data << ", ID" << m_objID << ")" << std::endl;
  }
  Str(const Str& other) {
    step_id();
    m_size = other.m_size;
    m_data = new char[m_size];
    memcpy(m_data,other.m_data,m_size);
    std::cout << "Copied: Str object. (" << m_data << ", ID" << m_objID << ")" << std::endl;
  }
  Str(Str&& other) noexcept {
    step_id();
    m_size = other.m_size;
    m_data = other.m_data;
    other.m_size = 0;
    other.m_data = nullptr;
    std::cout << "Moved: Str object. (" << m_data << ", ID" << m_objID << ")" << std::endl;
  }
  ~Str() {
    std::cout << "Destroying (ID" << m_objID << "): " << (m_data == nullptr ? "<empty>" : m_data) << std::endl;
    delete[](m_data);
  }
  friend std::ostream& operator<< (std::ostream& stream, const Str& obj) {
    stream << obj.m_data << " (ID:" << obj.m_objID << ")";
    return stream;
  }
protected:
  void step_id() {
    m_objID = m_counter++;
  }
private:
  char* m_data;
  uint32_t m_size;
  size_t m_objID;
  static size_t m_counter;
};

size_t Str::m_counter = 0;

class Entity {
public:
  Entity(const Str& name): m_name(name) {}
  Entity(Str&& name): m_name((Str&&)name) {} // cast to force the use of move constructor, std::move(name) would also do the job
  friend std::ostream& operator<< (std::ostream& stream, const Entity& obj) {
    stream << obj.m_name;
    return stream;
  }
private:
  Str m_name;
};

int main(int argc, char** argv) {
  Entity an_ent("Ulysses");
  std::cout << "The entity: " << an_ent << std::endl;
  
  return 0;
}
