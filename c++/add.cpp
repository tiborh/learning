#include <iostream>

int add(int a, int b) {
  std::cout << "Integer addition.\n";
  return a+b;
}

float add(float a, float b) {
  std::cout << "Float addition.\n";
  return a+b;
}

double add(double a, double b) {
  std::cout << "Double addition.\n";
  return a+b;
}


int main(int argc, char** argv) {
  { // add.cpp:24:32: error: call to 'add' is ambiguous
    // int a = 4;
    // float b = 0.4;
    // std::cout << "add " << a << " and " << b << std::endl;
    // std::cout << "result: " << add(a,b) << std::endl;
  }
  { // error: call to 'add' is ambiguous
    // std::cout << "same with directly typing params\n";
    // std::cout << "result: " << add(4,0.4) << std::endl;
  }
  {
    float a = 4.0;
    float b = 0.4;
    std::cout << "add " << a << " and " << b << std::endl;
    std::cout << "result: " << add(a,b) << std::endl;
  }
  {
    std::cout << "same with directly typing params\n";
    std::cout << "result: " << add(4.0,0.4) << std::endl;
  }
  return 0;
}
