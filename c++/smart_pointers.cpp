#include <iostream>
#include <memory>		// where smart pointers live

struct X {
  X() { std::cout << "-->X created\n"; }
  ~X() { std::cout << "<--X destroyed\n"; }
  void use() {}
};

// 1. use unique_pointer wherever possible (safety and low overhead
// 2. use shared pointer only when you need to share same object between several pointers

int main(int argc, char** argv) {
  // unique_pointer: it cannot be copied
  std::cout << "beginning of main()\n";
  {
    std::cout << "entered a block\n";
    std::unique_ptr<X> x(new X()); // = new X() is not implemented in std object
    x->use();
    std::cout << "end of block\n";
  }
  {
    std::cout << "entered a second block\n";
    std::unique_ptr<X> x = std::make_unique<X>(); // this is the appropriate way of doing it.
    x->use();
    // std::unique_ptr<X> xa = x;	// error: call to deleted constructor of 'std::unique_ptr<X>' this is how the pointer protects uniqueness.

    std::cout << "end of second block\n";
  }
  {
    std::cout << "entered a block\n";
    std::shared_ptr<X> x(new X()); // this works with reference counting, but this does not start it porperly (no problem with valgrind, though)
    std::cout << "second empty pointer is also created\n";
    std::shared_ptr<X> y = std::shared_ptr<X>(); // the recommended way
    std::cout << "and is set assigned to previous pointer\n";
    y = x;
    y->use();
    // std::unique_ptr<X> xa = (std::unique_ptr<X>)x; // error: no matching conversion for C-style cast from 'std::shared_ptr<X>' to 'std::unique_ptr<X>'
    std::shared_ptr<X> xa = x;
    std::cout << "now a second shared pointer also points at x object\n";
    x->use();
    xa->use();
    std::cout << "end of block\n";
  }
  {
    std::cout << "entered a block and creating a shared pointer\n";
    std::shared_ptr<X> x;
    {
      std::cout << "entered an embedded block, creating a pointer pointing at a shared object, and setting the outer object pointer equal to it\n";
      std::shared_ptr<X> inner = std::make_shared<X>();
      x = inner;
      x->use();
      std::cout << "end of embedded block\n";
    }
    x->use();
    std::cout << "end of outer block\n";
  }
  {
    std::cout << "entered a block and creating a weak pointer\n";
    std::weak_ptr<X> x;
    {
      std::cout << "entered an embedded block, creating a pointer pointing at a shared object, and setting the outer object pointer equal to it\n";
      std::shared_ptr<X> inner = std::make_shared<X>();
      x = inner;
      if(!x.expired()) {
	std::cout << "weak pointer is not expired, there is " << x.use_count() << " shared object pointing at the same object\n";
      }	
      std::cout << "end of embedded block\n";
    }
    if(x.expired())
      std::cout << "weak pointer has expired.\n";
    std::cout << "end of outer block\n";
  }
  //std::cout << "unique pointer has been automatically deleted (and pointed object distroyed)\n";
  //std::cout << "x usage has ended.\n";
  std::cout << "end of main.\n";
  
  return 0;
}
