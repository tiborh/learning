#include <iostream>

int main(int argc, char** argv) {
  std::cout << "new and new[] are two different operators.\n";
  std::cout << "this is why you need delete and delete[] to free the memory allocated by them.\n";

  int* i = new int;		// pointer to a single int
  int* j = new int[10];		// pointer to an array of ints

  delete i;
  delete[] j;
  
  return 0;
}
