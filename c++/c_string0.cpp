#include <iostream>
#include <cstring>

void print_arr(int len, const char* instr, bool suppr=false) {
  if (!suppr)
    std::cout << "Array print:\n";
  for (int i = 0; i < len; ++i)
    std::cout << instr[i] << '\n';
  std::cout << std::endl;
}


void print_str(const char* instr) {
  std::cout << "String print:\n";
  int len = strlen(instr);
  print_arr(len,instr,true);
}

void usage(const char* progname) {
  std::cout << "Usage:\n\t" << progname << " <length> <a string>" << std::endl;
  exit(0);
}

int main(int argc, char** argv) {

  if (argc < 3)
    usage(argv[0]);

  int len = atoi(argv[1]);
  char strarr[len];
  strcpy(strarr,argv[2]);

  std::cout << strarr << std::endl;
  print_str(strarr);
  print_arr(len,strarr);
  std::cout << "Length of array: " << len << std::endl;
  std::cout << "Length of string: " << strlen(strarr) << std::endl;
  
  return 0;
}
