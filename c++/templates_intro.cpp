#include <iostream>

// templates do not exist until something starts using it
template<typename T>
void Printit(T v) {
  std::cout << v << std::endl;
}

// templates are evaluated at compile time, so they can be used to generate C-style arrays:
template <typename T, int size>
class Array {
private:
  T m_array[size];
public:
  Array() = default;
  int getSize() const { return(size); }
};

int main(int argc, char** argv) {
  Printit<int>(5);		// typename can be omitted if it is obvious
  Printit<float>(3.3);
  Printit<const char*>("something");
  Array<int,5> arr;
  std::cout << "Array: getSize() == " << arr.getSize() << std::endl;
  return 0;
}
