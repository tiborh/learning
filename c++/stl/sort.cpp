#include <iostream>
#include <cassert>
#include <random>
#include <bits/stdc++.h>	// sort

const int DEFAULT_NUM=10;
const int DEFAULT_MIN=1;
const int DEFAULT_MAX=DEFAULT_NUM;

struct Period {
  int start,end;
};

int* create_random_integers(int num=DEFAULT_NUM,
			    int mini=DEFAULT_MIN,
			    int maxi=DEFAULT_MAX) {
  assert(maxi >= mini);
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(mini, maxi);

  int* out_arr = new int[num];
  
  for(int i=0; i < num; ++i)
    out_arr[i] = dist(mt);
  
  return(out_arr);
}

Period* create_periods(int num=DEFAULT_NUM,
		       int mini=DEFAULT_MIN,
		       int maxi=DEFAULT_MAX) {
  Period *retpers = new Period[num];
  int* starters = create_random_integers(num,mini,maxi);
  int* diffs = create_random_integers(num,mini,maxi);
  for (int i = 0; i < num; ++i) {
    retpers[i].start = starters[i];
    retpers[i].end = starters[i]+diffs[i];
  }
  return retpers;
}

bool comparePeriods(Period p1, Period p2) {
  return (p1.start != p2.start ? p1.start < p2.start : p1.end < p2.end);
}

void print_array(int* arr,int len,std::string label="the array") {
  std::cout << label << ":" << std::endl;
  for (int i = 0; i < len; ++i)
    std::cout << arr[i] << " ";
  std::cout << std::endl;
}

void print_periods(Period* per, int len, std::string label="the periods") {
  std::cout << label << ":" << std::endl;
  for (int i = 0; i < len; ++i)
    std::cout << "{" << per[i].start << "," << per[i].end << "} ";
  std::cout << std::endl;
}

int main(int argc, char** argv) {
  const int n = 10;
  int* ten = create_random_integers();
  print_array(ten,n);
  std::sort(ten,ten+n);
  print_array(ten,n,"sorted");
  std::sort(ten,ten+n,std::greater<int>());
  print_array(ten,n,"reverse sorted");
  Period* pers = create_periods();
  print_periods(pers,n);
  std::sort(pers,pers+n,comparePeriods);
  print_periods(pers,n,"sorted");
  return 0;
}
