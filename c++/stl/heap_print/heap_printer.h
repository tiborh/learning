#ifndef HEAP_PRINTER_H_INCLUDED
#define HEAP_PRINTER_H_INCLUDED

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

//std::string do_padding (unsigned long, unsigned long);
//void printer (std::vector<int> const&, unsigned long, unsigned long);
void print_tree (std::vector<int>&);

#endif // HEAP_PRINTER_H_INCLUDED
