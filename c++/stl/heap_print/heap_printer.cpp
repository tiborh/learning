#include "heap_printer.h"

std::string do_padding (unsigned long index, unsigned long mlength){
  std::string padding;
  if (int((index-1)/2) != 0){
    return (int((index-1)/2) % 2 == 0) ?
    (do_padding(static_cast<unsigned long>((index-1)/2),mlength) + std::string(mlength+4,' ') + " ")  :
    (do_padding(static_cast<unsigned long>((index-1)/2),mlength) + std::string(mlength+3,' ') + " |") ;
  }
  return padding;
}

void printer (std::vector<int> const & tree, unsigned long index, unsigned long mlength){
  unsigned long last = tree.size() - 1 ;
  unsigned long left = 2 * index + 1 ;
  unsigned long right = 2 * index + 2 ;
  std::cout << " " << tree[index] << " ";
  if (left <= last){
    unsigned long llength = std::to_string(tree[left]).size();
    std::cout << "---" << std::string(mlength - llength,'-');
    printer(tree,left,mlength);
    if (right <= last) {
      unsigned long rlength = std::to_string(tree[right]).size();
      std::cout << "\n" << do_padding(right,mlength) << std::string(mlength+ 3,' ') << " | ";
      std::cout << "\n" << do_padding(right,mlength) << std::string(mlength+ 3,' ') << " └" <<
      std::string(mlength - rlength,'-');
      printer(tree,right,mlength);
    }
  }
}

void print_tree (std::vector<int> & tree){
  unsigned long mlength = 0;
  for (int & element : tree){
    unsigned long clength = std::to_string(element).size();
    if (clength > mlength) {
      mlength = std::to_string(element).size();
    }
  }
  std::cout <<  std::string(mlength- std::to_string(tree[0]).size(),' ');
  printer(tree,0,mlength);
  std::cout << std::endl;
}
