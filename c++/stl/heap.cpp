#include <iostream>
#include <algorithm>		// for heap
#include <vector>		// to build heap
#include <cassert>
#include <random>

const int DEFAULT_NUM=10;
const int DEFAULT_MIN=1;
const int DEFAULT_MAX=DEFAULT_NUM;

int* create_random_integers(int num=DEFAULT_NUM,
			    int mini=DEFAULT_MIN,
			    int maxi=DEFAULT_MAX) {
  assert(maxi >= mini);
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(mini, maxi);

  int* out_arr = new int[num];
  
  for(int i=0; i < num; ++i)
    out_arr[i] = dist(mt);
  
  return(out_arr);
}

void print_heap(std::vector<int> v) {
  for ( int &pt : v )
    std::cout << pt << " ";
  std::cout << std::endl;
}

void print_heap_with_label(std::vector<int> v, std::string s) {
  std::cout << s << ":" << std::endl;
  print_heap(v);
}

int main(int argc, char** argv) {
  int ten[] = {0,1,2,3,4,5,6,7,8,9};
  int n = sizeof(ten) / sizeof(ten[0]);
  std::cout << "heap input array:\n";
  for (int i = 0; i < n; ++i)
    std::cout << ten[i] << " ";
  std::cout << std::endl;
  std::vector<int> v(ten,ten+n);
  std::cout << "heap input vector:\n";
  print_heap_with_label(v,"the heap vector");
  
  std::make_heap(v.begin(),v.end());
  // std::cout << "heap input vector: " << v << '\n';  // error: invalid operand to binary expression
  std::cout << "initial front of the heap : " << v.front() << '\n';
  std::cout << "initial back of the heap  : " << v.back() << '\n';
  print_heap_with_label(v,"the heap");

  v.push_back(10);
  print_heap_with_label(v,"after vector push_back");
  push_heap(v.begin(),v.end());
  print_heap_with_label(v,"after push_heap");

  pop_heap(v.begin(),v.end());
  print_heap_with_label(v,"after pop_heap");
  v.pop_back();
  print_heap_with_label(v,"after vector pop_back:");

  int n1 = 10;
  int* randten = create_random_integers(n1,0,100);
  std::vector<int> v1(randten,randten+n1);
  print_heap_with_label(v1,"rand heap vector");

  std::make_heap(v1.begin(),v1.end());
  print_heap_with_label(v1,"heapified");

  std::sort_heap(v1.begin(),v1.end());
  print_heap_with_label(v1,"sorted");

  std::reverse(v1.begin(),v1.end());
  print_heap_with_label(v1,"reversed");

  return 0;
}
