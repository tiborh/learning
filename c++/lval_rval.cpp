#include <iostream>

void func(int a) {
  std::cout << "This function accepts both l and r values" << std::endl;
  std::cout << "input: " << a << std::endl;
  a*=2;
  std::cout << "after internal change: " << a << std::endl;
}

void func2(const int& a) {
  std::cout << "This function also accepts both l and r values" << std::endl;
  std::cout << "input: " << a << std::endl;
  // a*=2; cannot assign to variable 'a' with const-qualified type 'const int &'
}

void str_func(std::string& a) {	// const std::string& would also be good, esp when worrying about side effects
  std::cout << "This is a left value string function" << std::endl;
  std::cout << "input: " << a << std::endl;
  a+="gimel";
  std::cout << "after internal change: " << a << std::endl;
}

void str_func(std::string&& a) { // temporary, so it can be changed in all ways
  std::cout << "This is right value string function" << std::endl;
  std::cout << "input: " << a << std::endl;
  a+="gimel";
  std::cout << "after internal change: " << a << std::endl;
}

void lval_func(int& a) {
  std::cout << "This is left value function" << std::endl;
  std::cout << "input: " << a << std::endl;
  a*=2;
  std::cout << "after internal change: " << a << std::endl;
}

int main(int argc, char** argv) {
  int i = 10;
  int j = i;
  std::string a = "aleph";
  std::string b = "bet";
  func(i);
  func(10);
  
  lval_func(i);
  // lval_func(i + j); candidate function not viable: expects an l-value for 1st argument
  // lval_func(10); candidate function not viable: expects an l-value for 1st argument

  func2(i);
  func2(i + j);

  str_func(a);
  std::cout << "after function run: a == " << a << std::endl;
  str_func(a + b);
  std::cout << "after function run: a == " << a << std::endl;
  str_func("dalet");
  
  return 0;
}
