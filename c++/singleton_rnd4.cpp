#include <iostream>

struct Rnd {
  Rnd(const Rnd&) = delete; // to prevent: Rnd instnc = Sntltn::get(); (copy construction)
  static float gen_float() { return get().generate(); }
  
private:
  Rnd() = default;
  float _fake = 0.64479874f;
  float generate() { return _fake; }
  static Rnd& get(void) {
    static Rnd _instnc;
    return _instnc;
  }
};

int main(int argc, char** argv) {

  float r = Rnd::gen_float();	// the whole thing can be done with namespace too without class, the header file hiding the implementation
  std::cout << "The fake rnd: " << r << std::endl;
  
  return 0;
}
