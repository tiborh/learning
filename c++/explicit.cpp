#include <iostream>

struct Sa {
  Sa() = default;
  Sa(std::string n):_name(n) {}
  void use(void){}
  std::string _name;
};

struct Sb {
  Sb() = default;
  explicit Sb(std::string n):_name(n) {}
  void use(void){}
  std::string _name;
};


int main(int argc, char** argv) {
  Sa a = std::string("one");	// implicit cast allowed, so no problem
  a.use();
  //Sb b = std::string("two");	// only explicit cast is allowed, so it does not work
  				// error: no viable conversion from 'std::string' (aka 'basic_string<char>') to 'Sb'
  Sb b = Sb("two");
  b.use();
  std::cout << "See explanation in source.\n";
  
  return 0;
}
