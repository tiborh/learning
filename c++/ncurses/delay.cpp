#include <ncurses.h>
#include <cstdlib>
#include <csignal>
#include <cctype>

#define EXITCHAR ' '

static void finish(int sig);

void print_help() {
  attron(A_BLINK);
  mvprintw(LINES-1,0,"Press '%c' to exit.",EXITCHAR);
  attroff(A_BLINK);
}

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  // cbreak();              // make typed characters immediately available
  // nocbreak();            // turns back on line buffering (after cbreak() turned it off)
  halfdelay(5);     // delay in tenth of a second to wait for user input, then error
  // nodelay(stdscr, true); //
  // timeout(delay);        //
  // raw();                 // almost like cbreak but the interrupt, quit, suspend, and
                            // flow control characters are all passed through
                            // uninterpreted, instead of generating  a  signal.
                            // it gets overriden by cbreak();
  // noraw();               // places the terminal out of raw mode
  // noecho();              // typed characters (in getch()) are not echoed
  // echo();                // turns back echoing chars after noecho() turned it off
  // curs_set(FALSE);       // to hide the cursor

  // 2. write something on the screen:
  print_help();
  
  int i = 0;
  for(int c = -1; c != EXITCHAR && c != 'X'; c = getch(), ++i) {
    mvprintw(i,0,"%d\n",c);
    if (i == LINES-1) {
      clear();
      i = -1;
      print_help();
      //refresh();		// getch takes care of it.
    }
  }
  
  // 3. make it appear on the current screen:
  //refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  //getch();

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
