#include "player.hpp"

Player::Player(WINDOW* win, int y, int x, char c) {
  curwin = win;
  y = yLoc;
  x = xLoc;
  getmaxyx(curwin,yMax,xMax);
  character = c;
}

void Player::mvup() {
  if (yLoc > 0) {
    mvwaddch(curwin,yLoc,xLoc,TRAILCHAR);
    --yLoc;
  }
}

void Player::mvdown() {
  if (yLoc < yMax-1) {
    mvwaddch(curwin,yLoc,xLoc,TRAILCHAR);
    ++yLoc;
  }
}

void Player::mvleft() {
  if (xLoc > 0) {
    mvwaddch(curwin,yLoc,xLoc,TRAILCHAR);
    --xLoc;
  }
}

void Player::mvright() {
  if (xLoc < xMax-1) {
    mvwaddch(curwin,yLoc,xLoc,TRAILCHAR);
    ++xLoc;
  }
}

int Player::getmv() {
  int c = wgetch(curwin);
  switch(c) {
  case KEY_UP:
    mvup();
    break;
  case KEY_DOWN:
    mvdown();
    break;
  case KEY_LEFT:
    mvleft();
    break;
  case KEY_RIGHT:
    mvright();
    break;
  default:
    break;
  }
  return c;
}

void Player:: display() {
  //wclear(curwin);
  mvwaddch(curwin,yLoc,xLoc,character);
  wrefresh(curwin);
}
