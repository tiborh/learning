#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include "player.hpp"

#define LOOPDELAY 10000
#define PLAYERCHAR '@'

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  const char help_str[] = "Press <Esc> to exit.";
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  int nlines = 20;
  int ncols = 50;
  int starty = LINES/2-10;
  int startx = 10;

  WINDOW* framewin = newwin(nlines+2,ncols+2,starty-1,startx-1);
  WINDOW* playwin = newwin(nlines, ncols, starty, startx);
  keypad(playwin,true);
  box(framewin, 0, 0);
  attron(A_BLINK);
  mvprintw(LINES-1,0,help_str);
  attroff(A_BLINK);
  
  // 3. make it appear on the current screen:
  refresh();
  wrefresh(framewin);
  wrefresh(playwin);
  Player* p0 = new Player(playwin,0,0,PLAYERCHAR);
  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  do{
    p0->display();
    usleep(LOOPDELAY);
  } while(p0->getmv() != 27);

  // 5. clean up:
  delete p0;
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
