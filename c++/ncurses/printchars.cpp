#include <ncurses.h>
#include <cstdlib>
#include <signal.h>

static void finish(int sig);

void char_printer(int startval, int upperlimit) {
  for (int i = startval; i < upperlimit; ++i) {
    printw("%3d %2c  ",i,i);
    if ((i - startval + 1) % 10 == 0)
      printw("\n");
  }
  printw("\n");
}

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  // cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  // noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  printw("Normal char set:\n");
  char_printer(32,127);
  printw("Alternative char set:\n");
  attron(A_ALTCHARSET);
  char_printer(32,127);
  attroff(A_ALTCHARSET);
  
  // 3. make it appear on the current screen:
  refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
