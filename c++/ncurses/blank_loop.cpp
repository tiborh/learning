#define _DEFAULT_SOURCE		// for usleep
#include <ncurses.h>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1. initialisation (of screen and memory) and configuration:
  initscr();
  // cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  // noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  while(1) {
    // 2. write something on the screen:
    printw("Press Ctrl-C to exit.");
    
    // 3. make it appear on the current screen:
    refresh();
    sleep(1);
    // 4. give time for the user to see it:
    //    (a loop may also come handy)
    clear();
    refresh();
    sleep(1);
  }
    
  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
