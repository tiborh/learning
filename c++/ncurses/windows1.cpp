#include <ncurses.h>
#include <cstdlib>
#include <signal.h>
#include <cstring>
#include <unistd.h>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  char msg[] = "Press Ctrl-C to exit.";
  unsigned msg_len = strlen(msg);
  // 1. initialisation (of screen and memory):
  initscr();
  curs_set(FALSE); // to hide the cursor

  int height,width,startx,starty;
  height = 10; 			// char lines
  width = 20;			// char columns
  startx = starty = 10;
  
  WINDOW* win = newwin(height,width,starty,startx); // a window
  char top_bottom = argc > 1 ? argv[1][0] : '-';
  char left_right = argc > 2 ? argv[2][0] : '|';
  box(win,left_right,top_bottom);
  
  // 2. write something on the screen:
  mvwprintw(win,1,1,"a box");
  mvwprintw(win,2,1,"in a window");
    
  // 3. make it appear on the current screen:
  refresh();
  wrefresh(win);
  while(1) {
    WINDOW* wm = newwin(1,msg_len,LINES-1,0);
    wprintw(wm,msg);
    wrefresh(wm);
    sleep(1);
    wclear(wm);
    wrefresh(wm);
    sleep(1);
    delwin(wm);
  }

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
