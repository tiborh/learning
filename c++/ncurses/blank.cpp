#include <ncurses.h>
#include <cstdlib>
#include <csignal>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  // cbreak();              // make typed characters immediately available
  // nocbreak();            // turns back on line buffering (after cbreak() turned it off)
  // halfdelay(tenths);     // delay in tenth of a second to wait for user input, then error
  // nodelay(stdscr, true); // returns error without delay (if there is no char in the input buffer
  // timeout(delay);        // -1 waits until input arrives, 0 is same as nodelay,
                            // greater than zero: waits as many milliseconds as 'delay'
  // raw();                 // almost like cbreak but the interrupt, quit, suspend, and
                            // flow control characters are all passed through
                            // uninterpreted, instead of generating  a  signal.
                            // it gets overriden by cbreak();
  // noraw();               // places the terminal out of raw mode
  // noecho();              // typed characters (in getch()) are not echoed
  // echo();                // turns back echoing chars after noecho() turned it off
  // curs_set(FALSE);       // to hide the cursor

  // 2. write something on the screen:
  attron(A_BLINK);
  mvprintw(LINES-1,0,"Press a key to exit.");
  attroff(A_BLINK);
  
  // 3. make it appear on the current screen:
  refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
