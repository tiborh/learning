#include <ncurses.h>
#include <cstdlib>
#include <unistd.h>

#define BREAK 4

static void finish(int sig);

int main(int argc, char** argv) {
  initscr();
  curs_set(FALSE); // to hide the cursor
  if (!has_colors()) {
    printw("Your terminal cannot display colours.\n");
    sleep(1);
    finish(0);
  }
  
  /*
   * COLOR_PAIR(n)
   * COLOR_BLACK   0
   * COLOR_RED     1
   * COLOR_GREEN   2
   * COLOR_YELLOW  3
   * COLOR_BLUE    4
   * COLOR_MAGENTA 5
   * COLOR_CYAN    6
   * COLOR_WHITE   7
   */

  start_color();
  int pairnum = 0;
  for(int fore = 0; fore < 8; ++fore)
    for(int back = 0; back < 8; ++back)
      if (fore != back)
	init_pair(pairnum++, fore, back);

  for (int i = 0; i < pairnum; ++i) {
    attron(COLOR_PAIR(i));
    printw("colour pair %2d ",i);
    attroff(COLOR_PAIR(i));
    if ((i+1) % BREAK == 0)
      printw("\n");
  }

  getch();

  endwin();
  
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
