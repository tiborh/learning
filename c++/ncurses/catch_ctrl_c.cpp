#define _DEFAULT_SOURCE		// for usleep
#include <ncurses.h>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

int main(int argc, char** argv) {
  // 1. initialisation (of screen and memory) and configuration:
  initscr();
  // cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  int usr_inp = 0;
  while(usr_inp != 3) {
    // 2. write something on the screen:
    printw("Press Ctrl-C to exit.\n");
    
    // 3. make it appear on the current screen:
    refresh();
    usr_inp = getch();
    printw("User input: %d\n",usr_inp);
  }
    
  // 5. clean up:
  endwin();

  return(0);
}
