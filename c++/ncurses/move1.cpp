#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <signal.h>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  char message[] = "Look at the corners.";
  long msg_len = strlen(message);
  int cols = 0;
  int lines = 0;
  
  // 1. initialisation (of screen and memory):
  initscr();
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  while(1) {
    if (cols != COLS || lines != LINES) {
      cols = COLS;
      lines = LINES;
      clear();
      int i = 0;
      printw("%d",i++);		// it starts at 0,0, so no reason to move
      mvprintw(0,cols-1,"%d",i++);
      mvprintw(lines-1,cols-1,"%d",i++);
      mvprintw(lines-1,0,"%d",i++);
      int xpos = cols/2-msg_len/2;
      int ypos = lines/2-1;
      if (xpos < 0)
	xpos = 0;
      if (ypos < 0)
	ypos = 0;
      mvprintw(ypos,xpos,"%s",message);
    }
  
    // 3. make it appear on the current screen:
    refresh();

    // 4. give time for the user to see it:
    //    (a loop comes handy)
    usleep(1000);
  }

  // 5. clean up:
  endwin();
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(0);
}
