#include <ncurses.h>
#include <unistd.h>
#include <string>
#include <cassert>

int main(int argc, char** argv) {
  std::string msg = (argc > 1) ? argv[1] : "Hello, world!";

  // 1. initialisation (of screen and memory):
  initscr();
  curs_set(FALSE);

  // 2. write something on the screen:
  //    e.g. addch(c) where the cursor is
  //         or mvaddch(y,x,c) add a char at (x,y)
  //         or printw(const char*fmt, ...) mvprintw(y,x,const char* fmt, ...)
  int xpos = COLS/2-msg.length()/2;
  int ypos = LINES/2-1;
  assert(xpos >= 0);
  assert(ypos >= 0);
  mvprintw(ypos,xpos,"%s",msg.c_str());

  // 3. make it appear on the current screen:
  //    curscr is what you see
  //    stdscr is what you are working on
  //    refresh() overwrites curscr with stdscr
  //    (wrefresh() to refresh another window than curscr)
  refresh();

  // 4. give time for the user to see it:
  sleep(1);
  
  // 5. clean up:
  endwin();
  return 0;
}
