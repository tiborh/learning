#include <ncurses.h>
#include <cstring>
#include <cassert>

int main(int argc, char** argv) {
  char message[] = "Look at the corners.";
  long msg_len = strlen(message);
  
  // 1. initialisation (of screen and memory):
  initscr();
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  int i = 0;
  printw("%d",i++);		// it starts at 0,0, so no reason to move
  mvprintw(0,COLS-1,"%d",i++);
  mvprintw(LINES-1,COLS-1,"%d",i++);
  mvprintw(LINES-1,0,"%d",i++);
  int xpos = COLS/2-msg_len/2;
  int ypos = LINES/2-1;
  assert(xpos >= 0);
  assert(ypos >= 0);
  mvprintw(ypos,xpos,"%s",message);
  
  // 3. make it appear on the current screen:
  refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();
  return 0;
}
