#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>
#include <cstdlib>
#include <csignal>
#include <random>
#include <unistd.h>

#define PRINTABLE_START 32
#define PRINTABLE_END 126
#define LOOPDELAY 1000
#define WNCOLS 50
#define WNLINES 15
#define WSTARTX 3
#define WSTARTY 2

static void finish(int sig);

void fill_win(WINDOW* win) {
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(PRINTABLE_START, PRINTABLE_END);

  int xMax = -1;
  int yMax = -1;
  getmaxyx(win,yMax,xMax);
  for (int x = 0; x < xMax; ++x)
    for (int y = 0; y < yMax; ++y) {
      mvwprintw(win,y,x,"%c",dist(mt));
      wrefresh(win);
      usleep(LOOPDELAY);
    }
}

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  // cbreak();              // make typed characters immediately available
  // nocbreak();            // turns back on line buffering (after cbreak() turned it off)
  // halfdelay(tenths);     // delay in tenth of a second to wait for user input, then error
  // nodelay(stdscr, true); // returns error without delay (if there is no char in the input buffer
  // timeout(delay);        // -1 waits until input arrives, 0 is same as nodelay,
                            // greater than zero: waits as many milliseconds as 'delay'
  // raw();                 // almost like cbreak but the interrupt, quit, suspend, and
                            // flow control characters are all passed through
                            // uninterpreted, instead of generating  a  signal.
                            // it gets overriden by cbreak();
  // noraw();               // places the terminal out of raw mode
  // noecho();              // typed characters (in getch()) are not echoed
  // echo();                // turns back echoing chars after noecho() turned it off
  curs_set(FALSE);       // to hide the cursor

  // 2. write something on the screen:
  mvprintw(0,0,"Filled randomly:");
  mvprintw(WSTARTY+WNLINES+2*WSTARTY-2,0,"Copied below:");
  attron(A_BLINK);
  mvprintw(LINES-1,0,"Press a key to exit.");
  attroff(A_BLINK);

  WINDOW* framewin = newwin(WNLINES+2,WNCOLS+2,WSTARTY-1,WSTARTX-1);
  WINDOW* playwin = newwin(WNLINES, WNCOLS, WSTARTY, WSTARTX);
  box(framewin, 0, 0);
  refresh();
  wrefresh(framewin);
  wrefresh(playwin);
  fill_win(playwin);
  WINDOW* resframewin = newwin(WNLINES+2,WNCOLS+2,WSTARTY+WNLINES+2*WSTARTY-1,WSTARTX-1);
  WINDOW* reswin = newwin(WNLINES, WNCOLS, WSTARTY+WNLINES+2*WSTARTY, WSTARTX);
  box(resframewin, 0, 0);
  wrefresh(resframewin);
  for(int li = 0; li < WNLINES; ++li) {
    for (int col = 0; col < WNCOLS; ++ col) {
      chtype wininputstr = 0;
      mvwinchstr(playwin,li,col,&wininputstr);
      mvwprintw(reswin,li,col,"%c",wininputstr);
      wrefresh(reswin);
      usleep(LOOPDELAY);
    }
  }

  // try inchstr

  
  // 3. make it appear on the current screen:


  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
