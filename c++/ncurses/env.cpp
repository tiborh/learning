#include <ncurses.h>
#include <cstdlib>
#include <signal.h>

static void finish(int sig);

void print_env(void);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. do something:

  print_env();
  move(15,3);
  print_env();
  printw("Press a key to exit.");
  
  // 3. make it appear on the current screen:
  refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

void print_env(void) {
  int x_pos,y_pos,x_bpos,y_bpos,width,height;
  x_pos = y_pos = x_bpos = y_bpos = width = height = -1;
  getyx(stdscr,y_pos,x_pos);			// top left y,x
  printw("getyx():\n\tx_pos: %d\n\ty_pos: %d\n",x_pos,y_pos);
  getbegyx(stdscr,y_bpos,x_bpos);
  printw("getbegyx():\n\tx_bpos: %d\n\ty_bpos: %d\n",x_bpos,y_bpos);
  getmaxyx(stdscr,height,width);			// height and width
  printw("gmaxyx():\n\twidth: %d\n\theight: %d\n",width,height);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
