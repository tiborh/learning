#include <ncurses.h>
#include <cstdlib>
#include <unistd.h>

#define BREAK  20
#define STEP  100
#define MAXI 1000

static void finish(int sig);

int main(int argc, char** argv) {
  initscr();
  curs_set(FALSE); // to hide the cursor
  if (!has_colors()) {
    printw("Your terminal cannot display colours.\n");
    refresh();
    sleep(2);
    finish(-1);
  }
  if (!can_change_color()) {
    printw("Your terminal cannot display more than 8 ANSI colours.\n");
    refresh();
    sleep(2);
    finish(-2);
  }
  
  /*
   * COLOR_PAIR(n)
   * COLOR_BLACK   0
   * COLOR_RED     1
   * COLOR_GREEN   2
   * COLOR_YELLOW  3
   * COLOR_BLUE    4
   * COLOR_MAGENTA 5
   * COLOR_CYAN    6
   * COLOR_WHITE   7
   */

  int counter = 0;
  start_color();
  init_color(0,0,0,0);
  init_color(1,1000,1000,1000);
  while(1) {
    clear();
    int i = 2;
    for(short r = 0; r <= MAXI; r+=STEP) {
      for(short g = 0; g <= MAXI; g+=STEP) {
	for(short b = 0; b <= MAXI; b+=STEP) {
	  init_color(i,r,g,b);
	  init_pair(i,i,(counter % 2 ? 1 : 0));
	  attron(COLOR_PAIR(i));
	  printw("(%3x,%3x,%3x)",r,g,b);
	  attroff(COLOR_PAIR(i));
	  if ((i) % BREAK == 0)
	    printw("\n");
	  ++i;
	}
      }
    }
    refresh();
    counter++;
    getch();
  }
	
  endwin();
  
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(sig);
}
