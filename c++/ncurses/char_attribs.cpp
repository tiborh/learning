#include <ncurses.h>
#include <cstdlib>

static void finish(int sig);

int main(int argc, char** argv) {
  initscr();
  curs_set(FALSE); // to hide the cursor

  /* for full list, see curses.h or ncurses.h
   * !!! not all works on all terminals !!!
   * A_NORMAL
   * A_STANDOUT
   * A_REVERSE
   * A_BLINK
   * A_DIM
   * A_BOLD
   * A_PROTECT
   * A_INVIS
   * A_ALTCHARSET
   * A_CHARTEXT
   * attron();  to turn on an attibute
   * attroff(); to turn off an attribute
   */

  printw("Normal text\n");
  attron(A_STANDOUT);
  printw("Standout text\n");
  attroff(A_STANDOUT);
  attron(A_REVERSE);
  printw("Reverse text\n");
  attroff(A_REVERSE);
  attron(A_BLINK);
  printw("Blink text\n");
  attroff(A_BLINK);
  attron(A_DIM);
  printw("Dim text\n");
  attroff(A_DIM);
  printw("Not bold. ");
  attron(A_BOLD);
  printw("Bold text\n");
  attroff(A_BOLD);
  attron(A_PROTECT);
  printw("Protect text\n");
  attroff(A_PROTECT);
  printw("Invisible text >");
  attron(A_INVIS);
  printw("Invisible text");
  attroff(A_INVIS);
  printw("< Invisible text\n");
  printw("Alternative charset >");
  attron(A_ALTCHARSET);
  printw("ALTCHARSET text");
  attroff(A_ALTCHARSET);
  printw("< Alternative charset\n");
  attron(A_CHARTEXT);
  printw("CHARTEXT text\n");
  attroff(A_CHARTEXT);
  
  getch();
  endwin();
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
