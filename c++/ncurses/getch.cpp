#include <iostream>
#include <ncurses.h>
#include <unistd.h>

int main(int argc, char** argv) {

  // 1. initialisation (of screen and memory):
  initscr();
  (void) noecho(); // do not echo key pressed
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  printw("Input a character: ");
  int user_input = getch();
  printw("\nYour input: '%c' (%d, 0%o, 0x%x)\n",user_input,
	 user_input,user_input,user_input);
  
  // 3. make it appear on the current screen:
  refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  //sleep(1);
  printw("Press a key to exit.");
  getch();

  // 5. clean up:
  endwin();
  return 0;
}
