#ifndef _DRAWER_H_
#define _DRAWER_H_

#include <curses.h>
#include <cassert>

#define EMPTYCHAR 0x20

class Drawer {
 public:
  Drawer(WINDOW* win, int y, int x, char c);

  void mvup();
  void mvdown();
  void mvleft();
  void mvright();
  int getmv();
  void display();
  void chgchar(int);
  int getcurchar();

 private:
  int xLoc, yLoc, xMax, yMax;
  char character,trailchar;
  chtype curchar;
  bool draw_mode;
  WINDOW* curwin;
  void fillwin();
};

#endif
