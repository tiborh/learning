#include "drawer.hpp"

Drawer::Drawer(WINDOW* win, int y, int x, char c) {
  curwin = win;
  y = yLoc;
  x = xLoc;
  getmaxyx(curwin,yMax,xMax);
  character = c;
  trailchar = character;
  draw_mode = true;
  curchar = EMPTYCHAR;
  fillwin();
}

void Drawer::fillwin() {
  for(int y = 0; y < yMax; ++y)
    for(int x = 0; x < xMax; ++x)
      mvwaddch(curwin,y,x,EMPTYCHAR);
}

void Drawer::mvup() {
  if (yLoc > 0) {
    if (draw_mode)
      mvwaddch(curwin,yLoc,xLoc,trailchar);
    else if (curchar)
      mvwaddch(curwin,yLoc,xLoc,curchar);
    else
      mvwaddch(curwin,yLoc,xLoc,EMPTYCHAR);
    --yLoc;
  }
}

void Drawer::mvdown() {
  if (yLoc < yMax-1) {
    if (draw_mode)
      mvwaddch(curwin,yLoc,xLoc,trailchar);
    else if (curchar)
      mvwaddch(curwin,yLoc,xLoc,curchar);
    else
      mvwaddch(curwin,yLoc,xLoc,EMPTYCHAR);
    ++yLoc;
  }
}

void Drawer::mvleft() {
  if (xLoc > 0) {
    if (draw_mode)
      mvwaddch(curwin,yLoc,xLoc,trailchar);
    else if (curchar)
      mvwaddch(curwin,yLoc,xLoc,curchar);
    else
      mvwaddch(curwin,yLoc,xLoc,EMPTYCHAR);
    --xLoc;
  }
}

void Drawer::mvright() {
  if (xLoc < xMax-1) {
    if (draw_mode)
      mvwaddch(curwin,yLoc,xLoc,trailchar);
    else if (curchar)
      mvwaddch(curwin,yLoc,xLoc,curchar);
    else
      mvwaddch(curwin,yLoc,xLoc,EMPTYCHAR);
    ++xLoc;
  }
}

int Drawer::getmv() {
  int c = wgetch(curwin);
  switch(c) {
  case KEY_UP:
    mvup();
    break;
  case KEY_DOWN:
    mvdown();
    break;
  case KEY_LEFT:
    mvleft();
    break;
  case KEY_RIGHT:
    mvright();
    break;
  // case KEY_F(2):
  //   if (draw_mode)
  //     draw_mode = false;
  //   else
  //     draw_mode = true;
  default:
    break;
  }
  return c;
}

void Drawer:: display() {
  //wclear(curwin);
  wattron(curwin,A_BLINK);
  if (draw_mode)
    wattron(curwin,A_REVERSE);
  else
    getcurchar();
  mvwaddch(curwin,yLoc,xLoc,character);
  if (draw_mode)
    wattroff(curwin,A_REVERSE);
  wattroff(curwin,A_BLINK);
  wrefresh(curwin);
}

void Drawer::chgchar(int c)  {
  character = c;
  trailchar = character;
}

int Drawer::getcurchar() {
  wprintw(curwin,"Still alive.");
  wprintw(curwin,"Curr char: 0x%x",curchar);
  wrefresh(curwin);
  int retval = mvwinchstr(curwin,yLoc,xLoc,&curchar);
  assert(retval != ERR);
  return retval;
}
