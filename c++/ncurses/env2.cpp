#include <string>
#include <ncurses.h>
#include <cstdlib>
#include <signal.h>

static void finish(int sig);

void print_env(WINDOW*,std::string*);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. do something:
  int height,width,startx,starty;
  height = 14; 			// char lines
  width = 20;			// char columns
  startx = starty = 0;
  
  WINDOW* win0 = newwin(height,width,starty,startx); // a window
  WINDOW* win1 = newwin(height,width,starty,startx+width+1); // a window
  print_env(win0,new std::string("Window 0:"));
  print_env(win1,new std::string("Window 1:"));
  attron(A_BLINK);
  mvprintw(height+1,0,"Press a key to exit.");
  attroff(A_BLINK);
  refresh();
  wrefresh(win0);
  wrefresh(win1);

  //
  
  // 3. make it appear on the current screen:
  //refresh();

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

void print_env(WINDOW* win,std::string* msg) {
  int x_pos,y_pos,x_bpos,y_bpos,par_x,par_y,width,height;
  x_pos = y_pos = x_bpos = y_bpos = par_x = par_y = width = height = -1;
  wprintw(win,"%s\n",msg->c_str());
  getyx(win,y_pos,x_pos); // top left y,x
  wprintw(win,"getyx():\n\tx_pos: %d\n\ty_pos: %d\n",x_pos,y_pos);
  getbegyx(win,y_bpos,x_bpos);
  wprintw(win,"getbegyx():\n\tx_bpos: %d\n\ty_bpos: %d\n",x_bpos,y_bpos);
  getparyx(win,par_y,par_x);
  wprintw(win,"getparyx():\n\tpar_x: %d\n\tpar_y: %d\n",par_x,par_y);
  getmaxyx(win,height,width);			// height and width
  wprintw(win,"gmaxyx():\n\twidth: %d\n\theight: %d\n",width,height);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
