#include <ncurses.h>
#include <cstdlib>
#include <signal.h>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  int nlines = 6;
  int ncols = COLS-12;
  int starty = LINES-8;
  int startx = 5;

  printw("Main window.\n");
  WINDOW* inputframewin = newwin(nlines+2, ncols+2, starty-1, startx-1);
  box(inputframewin, 0, 0);
  WINDOW* inputwin = newwin(nlines, ncols, starty, startx);
  keypad(inputwin,true);
  mvwprintw(inputwin,0,0,"Press a key\n");
  attron(A_BLINK);
  mvprintw(LINES-1,0,"Press a key to exit.");
  attroff(A_BLINK);
  
  // 3. make it appear on the current screen:
  refresh();
  wrefresh(inputframewin);
  wrefresh(inputwin);

  int c = wgetch(inputwin);
  mvwprintw(inputwin,1,0,"Char read from second window: %c\n",c);
  mvwprintw(inputwin,2,0,"Look up into the main window\n");
  wrefresh(inputwin);

  mvprintw(1,0,"the char here too: %c\n",c); // no need for refresh
  mvprintw(2,0,"press a key again\n");	     // ditto
  getch();
  
  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  mvprintw(2,0,"once more to exit"); // and ditto
  getch();

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
