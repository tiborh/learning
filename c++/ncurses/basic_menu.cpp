#include <ncurses.h>
#include <cstdlib>
//#include <cstring>
#include <signal.h>
#include <unistd.h>

#define NU_OF_CHOICES 5
#define MAX_CHOICE_LENGTH 6
#define STATUS_BAR_HEIGHT 2

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor

  // 2. write something on the screen:
  int nlines = NU_OF_CHOICES + 2;
  int ncols = COLS-12;
  int starty = LINES-nlines - STATUS_BAR_HEIGHT;
  int startx = 5;
  
  WINDOW* menuwin = newwin(nlines, ncols, starty, startx);
  WINDOW* statwin = newwin(STATUS_BAR_HEIGHT,ncols,LINES-STATUS_BAR_HEIGHT,0);
  keypad(menuwin,true);
  box(menuwin, 0, 0);
  char choices[NU_OF_CHOICES][MAX_CHOICE_LENGTH] = {"Stand",
						    "Walk",
						    "Jog",
						    "Run",
						    "Race"};
  //printw("|%s| |%s| |%s|\n",choices[0],choices[1],choices[2]);
  int choice = -1;
  int highlight = 0;
  
  while(1) {
    for(int i = 0; i < NU_OF_CHOICES; ++i) {
      if(i == highlight)
	wattron(menuwin, A_REVERSE);
      mvwprintw(menuwin, i+1, 1, choices[i]);
      wattroff(menuwin, A_REVERSE);
    }
    choice = wgetch(menuwin);

    switch(choice) {
    case KEY_UP:
      highlight--;
      break;
    case KEY_DOWN:
      highlight++;
      break;
    default:
      break;
    }
    if (highlight < 0)
      highlight = NU_OF_CHOICES-1;
    else if (highlight >= NU_OF_CHOICES)
      highlight = 0;
    if (choice == 10)		// <ENTER>
      break;
  }
  mvwprintw(statwin,0,0,"Selected option: %s (%d)\n",choices[highlight],highlight);
  wattron(statwin, A_BLINK);
  mvwprintw(statwin,1,0,"(Press a key to exit.)");
  wattroff(statwin, A_BLINK);
  
  // 3. make it appear on the current screen:
  //refresh();
  wrefresh(menuwin);
  wrefresh(statwin);

  //sleep(2);

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  wgetch(statwin);

  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
