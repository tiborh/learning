#define _DEFAULT_SOURCE		// for usleep
#include <ncurses.h>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <cstring>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  char hmover = '-';
  char vmover = '|';
  char message[] = "Press Ctrl-C to exit.";
  long msg_len = strlen(message);
  int cols = 0;
  int lines = 0;
  unsigned wait_time = 10000;
  // 1. initialisation (of screen and memory):
  initscr();
  curs_set(FALSE); // to hide the cursor
  while(1) {
    int xpos = 1;
    int ypos = 0;
    clear();
    {
      cols = COLS;
      lines = LINES;
      int xpos = cols/2-msg_len/2;
      int ypos = lines/2;
      if (xpos < 0)
	xpos = 0;
      if (ypos < 0)
	ypos = 0;
      mvprintw(ypos,xpos,"%s",message);
    }
    refresh();
    for(; xpos < cols; ++xpos) {
      mvaddch(ypos,xpos,hmover);
      refresh();
      usleep(wait_time);
    }
    for(--xpos; ypos < lines; ++ypos){
      mvaddch(ypos,xpos,vmover);
      refresh();
      usleep(wait_time);
    }
    for(--ypos,--xpos; xpos >= 0; --xpos) {
      mvaddch(ypos,xpos,hmover);
      refresh();
      usleep(wait_time);
    }
    for(++xpos; ypos >= 0; --ypos){
      mvaddch(ypos,xpos,vmover);
      refresh();
      usleep(wait_time);
    }
    sleep(1);
  }
  // 5. clean up:
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Bye!\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
