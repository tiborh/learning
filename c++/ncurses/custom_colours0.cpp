#include <ncurses.h>
#include <cstdlib>
#include <unistd.h>

static void finish(int sig);

int main(int argc, char** argv) {
  initscr();
  curs_set(FALSE); // to hide the cursor
  if (!has_colors()) {
    printw("Your terminal cannot display colours.\n");
    refresh();
    sleep(2);
    finish(0);
  }
  if (!can_change_color()) {
    printw("Your terminal cannot display more than 8 ANSI colours.\n");
    refresh();
    sleep(2);
    finish(-2);
  }
  
  /*
   * COLOR_PAIR(n)
   * COLOR_BLACK   0
   * COLOR_RED     1
   * COLOR_GREEN   2
   * COLOR_YELLOW  3
   * COLOR_BLUE    4
   * COLOR_MAGENTA 5
   * COLOR_CYAN    6
   * COLOR_WHITE   7
   */
  start_color();
  init_pair(1, COLOR_CYAN, COLOR_WHITE);
  printw("default colours\n");
  attron(COLOR_PAIR(1));
  printw("original colour cannot be retrieved\n");
  refresh();
  attroff(COLOR_PAIR(1));
  init_color(COLOR_CYAN,1000,0,1000);
  printw("default colours\n");
  attron(COLOR_PAIR(1));
  printw("changed cyan colour\n");
  attroff(COLOR_PAIR(1));
  printw("default again\n");

  getch();

  endwin();
  
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
