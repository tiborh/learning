#include <random>
#include <iostream>
#include <cstring>

// based on
// https://youtu.be/iChalAKXffs

#define ARR_SIZE 10
#define MIN_NUM 0
#define UPPER_BOUND 100

void fill_array(int* arr) {
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(MIN_NUM, UPPER_BOUND);
  
  for (int i=0; i<ARR_SIZE; ++i)
    *(arr+i) = dist(mt);
}

void alt_print_array(int* arr) {
  int* ploc0 = arr;
  for (int i=0; i<ARR_SIZE; ++i)
    std::cout << *ploc0++ << " ";
  std::cout << std::endl;
}

void print_array(int* arr) {
  for (int i=0; i<ARR_SIZE; ++i)
    std::cout << *(arr+i) << " ";
  std::cout << std::endl;
}

template <class T>
void print_array (T arr[]) {
  //  for (int i=0; i<ARR_SIZE; ++i)
  //   std::cout << arr[i] << "\n";
  // std::cout << std::endl;
  std::cout << *arr << "\n...\n" << arr[ARR_SIZE-1] << std::endl;
}

void print_array_addresses(int* arr) {
  for (int i=0; i<ARR_SIZE; ++i)
    std::cout << arr+i << " == " << *(arr+i) << "\n";
}

using namespace std;

struct aStruct {
  static const int x = 0xa3a2a1a0;
  static const int y = 0xb3b2b1b0;
  friend std::ostream& operator<< (std::ostream& stream, const aStruct& obj) {
    stream << "x == " << hex << x << ", y == " << y;
    return stream;
  }
};

struct bStruct {
  int x = 0xa3a2a1a0;
  int y = 0xb3b2b1b0;
  bStruct() {
    x = 0xc3c2c1c0;
    y = 0xd3d2d1d0;
  }
  friend std::ostream& operator<< (std::ostream& stream, const bStruct& obj) {
    stream << "x == " << hex << obj.x << " y == " << obj.y;
    return stream;
  }
};
  
void analyse_string(char s[]) {
  long slen = strlen(s);
  char* ps0 = s;
  char* ps_last = &s[slen-1];
  cout << "String: " << s << "\n";
  cout << "Hex print:\n";
  for (int i = 0; i < slen; ++i)
    cout << hex << (int)s[i] << " ";
  cout << endl;
  cout << "strlen(s): " << slen << "\n";
  cout << "pointer to s[" << slen-1 << "]: " << (long)ps_last << "\n";
  cout << "pointer to s[0]: " << (long)ps0 << "\n";
  cout << "   numeric diff: " << (long)ps_last - (long)ps0 << "\n";
  cout << "   pointer diff: " << ps_last - ps0 << "\n";
  cout << "Trying to print pointers 'as is' (interprets them as string):\n";
  for (int i = 0; i < slen; ++i) {
    char* psi = &s[i];
    cout << "pointer to s[" << i << "]: " << psi << "\n";
  }
}

int main(int argc, char** argv) {
  int anArray[ARR_SIZE];

  int* ploc6 = &anArray[6];
  int* ploc0 = &anArray[0];

  cout << "pointer 6 = " << (long)ploc6 << endl;
  cout << "pointer 0 = " << (long)ploc0 << endl;
  cout << " num diff = " << (long)ploc6 - (long)ploc0 << endl;
  cout << "poin diff = " << ploc6 - ploc0 << endl;

  fill_array(anArray);

  cout << "Generated numbers:\n";
  print_array(anArray);
  cout << "with alt print:\n";
  alt_print_array(anArray);
  
  cout << "addresses in the array:\n";
  print_array_addresses(anArray);

  char s[] = "hello";
  analyse_string(s);
  char sj[] = "春夏秋冬";
  analyse_string(sj);

  cout << "Init ints with hex values:\n";
  // stack allocation at compile time:
  aStruct anObjArr[ARR_SIZE];
  print_array<aStruct>(anObjArr);

  cout << "Constructor modifies the default:\n";
  bStruct othObjArr[ARR_SIZE];
  print_array<bStruct>(othObjArr);

  //heap allocation at run time:
  bStruct* objArr3 = new bStruct[ARR_SIZE];
  cout << "Dynamically allocated:\n" << *objArr3 << "\n...\n" << objArr3[ARR_SIZE-1] << endl;
  delete[] objArr3;

  // array of pointers
  bStruct** objArr4 = new bStruct*[ARR_SIZE]{ 0 };
  cout << "Array of pointers (null initiated):\n" << *objArr4 << "\n...\n" << objArr4[ARR_SIZE-1] << endl;
  for (int i = 0; i < ARR_SIZE; ++i)
    objArr4[i] = new bStruct();
  cout << "Each initiated:\n" << *objArr4 << "\n...\n" << objArr4[ARR_SIZE-1] << endl;
  cout << "values:\n" << **objArr4 << "\n...\n" << *objArr4[ARR_SIZE-1] << endl;
  for (int i = 0; i < ARR_SIZE; ++i)
    delete objArr4[i];
  delete[] objArr4;
  
  return 0;
}
