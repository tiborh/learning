#include <iostream>

int main(int argc, char** argv) {
  const int len = 5;
  size_t* ar = new size_t[len];
  size_t arr[len];
  std::cout << "'len' is defined as: " << len << std::endl; 
  std::cout << "size of static array: " << sizeof(arr) / sizeof(arr[0]) << std::endl;
  std::cout << "size of dynamic array: " << sizeof(*ar) / sizeof(ar[0]) << std::endl;
  std::cout << "(calculation of latter is not good because this method only works for static)\n";
  std::cout << "(sizeof(ar) throws an error because it would measure the size of the pointer)\n";
  
  return 0;
}
