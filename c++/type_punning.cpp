#include <iostream>

struct Ent {
  int a,b;
};

struct Ent2 {
  int a,b;
  int* getPos() {
    return &a;
  }
};


int main(int argc, char** argv) {

  {
    int a = 25;
    double b = a;
    double c = *(double*)&a;	// over-reads memory!

    std::cout << "b: " << b << std::endl;
    std::cout << "c: " << c << std::endl;
  }

  {
    // more meaningful example:
    Ent e = {12, 17};
    int* pos = (int*)&e;
    // crazy way:
    int b = *(int*)((char*)&e+(sizeof(int)));

    std::cout << "In structure: e.a == " << e.a << ", e.b == " << e.b << std::endl;
    std::cout << "As raw array: " << pos[0] << " " << pos[1] << std::endl;
    std::cout << "b == " << b << std::endl;
  }

  {
    Ent2 e = {24, 12};
    std::cout << "Original structure: e.a == " << e.a << ", e.b == " << e.b << std::endl;
    int* pos = e.getPos();
    pos[1] = 14;
    std::cout << "Modified structure: e.a == " << e.a << ", e.b == " << e.b << std::endl;
  }
  
  return 0;
}
