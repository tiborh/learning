#include <iostream>
#include <string>

struct X {
  X() = default;
  X(std::string s): _s(s) {}
  const std::string& get_s() const {
    ++call_counter;
    ++_call_count;
    return _s;
  }
  const int get_call_counter() const { return call_counter; }
  const int get_call_count() const { return _call_count; }
private:
  std::string _s;
  mutable unsigned _call_count = 0;
  static unsigned call_counter;
};

unsigned X::call_counter = 0;

int main(int argc, char** argv) {
  X x(argc > 1 ? argv[1] : "something");
  std::string my_s = x.get_s();
  std::cout << my_s << std::endl;

  const X y;
  std::cout << "'" << y.get_s() << "'\n";

  std::cout << "Call counter: " << y.get_call_counter() << std::endl;
  std::cout << "Call count: " << y.get_call_count() << std::endl;
  
  return 0;
}
