#include <iostream>

int main(int argc, char** argv) {

  int x = 5; 			// on stack
  std::cout << "x address: " << &x << ", value: " << x << std::endl;
  int* y = new int;		// on heap, memory allocate (malloc), the slowest part, slower than stack
  *y = 6;
  std::cout << "y address: " << y << ", value: " << *y << std::endl;
  delete y;			// deallocate memory manually
  
  return 0;
}
