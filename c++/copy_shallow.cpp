#include <iostream>
#include <cstring>

class Str {
public:
  Str(): _len(0) {
    _str = new char[1];
    _str[0] = '\0';
  }
  Str(const char* str) {
    init(str);
  }
  ~Str() {
    delete[](_str);
  }
  char& operator[](unsigned index) {
    if (_len-1 < index) {
      std::cerr << "Index is too great.\n";
      throw "";
    } else
      return(_str[index]);
  }
  void operator=(const char* str) {
    this->~Str();
    this->init(str);
  }
  unsigned get_len(void) { return _len; }
  friend std::ostream& operator<<(std::ostream& out,const Str& obj) {
    return(out << obj._str);
  }
private:
  char* _str;
  unsigned _len;
  void init(const char* str) {
    _len = strlen(str);
    _str = new char[_len + 1];
    memcpy(_str,str,_len);
    _str[_len] = '\0';
  }
};

int main(int argc, char** argv) {
  Str a = "Lorem ipsum dolor sit amet.";
  std::cout << "Str a: " << a << std::endl;
  Str b = a;
  std::cout << "Str b: " << b << std::endl;
  b = "consectetur adipiscing elit.";
  std::cout << "Str b (  directly changed): " << b << std::endl;
  std::cout << "Str a (indirectly changed): " << a << std::endl;
  a[0] = 'C';
  std::cout << "Str a (  directly changed): " << a << std::endl;
  std::cout << "Str b (indirectly changed): " << b << std::endl;
  b[b.get_len()] = 'x';		// to test exception
  // when copy constructor is missing, a shallow copy is created of the object variables.
  // free(): double free detected in tcache 2
  // Aborted (core dumped)

  return 0;
}
