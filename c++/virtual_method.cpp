#include <random>
#include <iostream>
#include <string>
#include <cstdlib>

// based on
// https://youtu.be/iChalAKXffs

#define ARR_SIZE 10

struct baseObj {
  virtual ~baseObj() {}
  virtual std::string identifySelf() { // to help children override this
    return "Base object";
  }
};

struct subObjA : public baseObj {
  std::string identifySelf() {
    return "Sub object A";
  }
};

struct subObjB : public baseObj {
  std::string identifySelf() {
    return "Sub object B";
  }
};

void fill_array(baseObj** arr) {
  std::random_device rd;
  std::mt19937 mt(rd());	// seeding
  std::uniform_int_distribution<int> dist(0, 2);
  
  for (int i=0; i<ARR_SIZE; ++i) {
    int choice = dist(mt);
    std::cout << "Choice: " << choice << std::endl;
    switch(choice) {
    case 0:
      *(arr+i) = new baseObj();
      break;
    case 1:
      *(arr+i) = new subObjA();
      break;
    case 2:
      *(arr+i) = new subObjB();
      break;
    default:
      std::cout << "Undefined choice: " << choice << std::endl;
      exit(1);
      }
  }
}

using namespace std;

int main(int argc, char** argv) {
  baseObj** objs = new baseObj*[ARR_SIZE]{ 0 };
  fill_array(objs);

  for (int i = 0; i < ARR_SIZE; ++i)
    cout << objs[i]->identifySelf() << endl;
  
  for (int i = 0; i < ARR_SIZE; ++i)
    delete objs[i];
  delete[] objs;
  
  return 0;
}
