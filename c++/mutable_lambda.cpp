#include <iostream>

int main(int argc, char** argv) {

  int x = 5;
  std::cout  << "x == " << x << std::endl;
  auto l = [=]() mutable { 	// [=] pass by value, [&] pass by reference
	     ++x;
	     std::cout  << "x == " << x << std::endl;
	   };

  l();
  std::cout  << "x == " << x << std::endl;
  
  return 0;
}
