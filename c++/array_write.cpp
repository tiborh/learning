#include <iostream>
#include <cassert>
// #include <cstdlib>
#include <cstring> // memset

template <typename T, size_t _len>
class Arr {
private:
  T _arr[_len] = {};
public:
  Arr() = default;
  // Arr<T,_len>(T* inarr) { // impossible
  //   for(size_t i = 0; i < _len; ++i)
  //     _arr[i] = inarr[i];
  // }
  constexpr int arr_len() const { return(_len); } // can be evaluated at compile time
  T& operator[](size_t index) {
    assert(index < _len);
    return(_arr[index]);
  }
  const T& operator[](size_t index) const {
    assert(index < _len);
    return(_arr[index]);                     // if const input is received, const output will be provided
  }
  // T* get_arr(void) { return(_arr); } // not a good idea to expose the whole
  const T* get_arr(void) const { return(_arr); }
  friend std::ostream& operator<<(std::ostream& out, const Arr& obj) {
    size_t obj_len = obj.arr_len();
    out << "< ";
    for (size_t i = 0; i < obj_len; ++i)
      out << obj._arr[i] << " ";
    out << ">";
    return(out);
  }
};

int main(int argc, char** argv) {
  // size_t len = argc > 1 ? atoi(argv[1]) : 5; //  error: non-type template argument is not a constant expression
  // Arr<int,5> ar = {1,2,3,4,5}; // impossible
  const size_t len = 5;
  Arr<int,len> ar;
  const auto& ar_ref = ar;
  std::cout << "After creation:\n";
  std::cout << "index 1: " << ar_ref[1] << std::endl;
  // ar_ref[1] = 4; error: cannot assign to return value because function 'operator[]' returns a const value
  static_assert(ar.arr_len() < 10, "Array length is too large");
  for (size_t i = 0; i < len; ++i)
    ar[i] = i+1;
  std::cout << "After filling up with index:\n";
  std::cout << ar << std::endl;
  std::cout << "index 4: " << ar.get_arr()[4] << std::endl;
  std::cout << "After memset reset:\n"; 
  // memset(ar.get_arr(),0,ar.arr_len() * sizeof(int)); // protected against this one
  memset(&ar[0],0,ar.arr_len() * sizeof(int)); // but not protected against this
  std::cout << ar << std::endl;
  
  std::cout << "Creating a new with the arr_len() output of the previous:\n";
  Arr<int,ar.arr_len()> ar2;
  std::cout << ar2 << std::endl;
  
  return 0;
}
