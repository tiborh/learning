#include <iostream>
#include <string>
#include <array>

// perhaps one of the best: a structure:
struct TwoStrings {
  std::string first;
  std::string second;
};

TwoStrings sample_func(void) {
  std::string first = "first";
  std::string second = "second";

  return( TwoStrings() = {first,second} );
}

void sample_func2(std::string& first, std::string& second) {
    first = "first";
    second = "second";
}

void sample_func3(std::string* first, std::string* second) { // more explicit on the sender side
  if (first)			// if not nullptr
    *first = "1st";
  if (second)  
    *second = "2nd";
}

std::string* sample_func4(void) {
  std::string first = "一番";
  std::string second = "二番";

  return( new std::string[]{first,second} ); // heap allocation
}

std::array<std::string,2> sample_func5(void) {
  std::string first = "いちばん";
  std::string second = "にばん";

  std::array<std::string,2> res = {first,second};
  
  return( res ); // heap allocation
}

int main(int argc, char** argv) {

  std::cout << "with structure:\n";
  TwoStrings returned = sample_func();
  std::cout << returned.first << " " << returned.second << std::endl;

  std::cout << "with side effect (giving control):\n";
  std::string first,second;
  sample_func2(first,second);
  std::cout << first << " " << second << std::endl;

  std::cout << "with side effect (passing pointer):\n";
  sample_func3(&first,&second);
  std::cout << first << " " << second << std::endl;

  std::cout << "returning a(n) (C-style) array:\n";
  std::string* res = sample_func4();
  std::cout << res[0] << " " << res[1] << std::endl;
  delete[](res);

  std::cout << "returning a(n) (C++-style) array:\n";
  std::array<std::string,2> res2 = sample_func5();
  std::cout << res2[0] << " " << res2[1] << std::endl;
  
  
  return 0;
}
