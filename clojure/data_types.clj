#!/usr/bin/env clj

(def args *command-line-args*)

;;(. System exit 0)

(def e 1)
(println e)
(println (str "type: " (type e)))

(def f 1.0)
(println f)
(println (str "type: " (type f)))

(def g "a")
(println g)
(println (str "type: " (type g)))

(def h \a)
(println h)
(println (str "type: " (type h)))

(def a (list 1 2 3 4 5))
(println a)
(println (str "type: " (type a)))

(def b [1 2 3 4 5])
(println b)
(println (str "type: " (type b)))

(def c #{1 2 3 4 5})
(println c)
(println (str "type: " (type c)))

(def d {:1 \a :2 \b :3 \c :4 \d :5 \e})
(println d)
(println (str "type: " (type d)))
