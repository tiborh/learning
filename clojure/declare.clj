#!/usr/bin/env clj

(def args *command-line-args*)

;;(. System exit 0)
(defn print-odd[n]
  (print "odd: ")(println n)
  )

(defn print-even[n]
  (print "even: ")(println n)
  )

(declare evenp)
(defn oddp [num]
  (if (odd? num)
    (print-odd num)
    (evenp num)
    )
  )

(defn evenp [num]
  (if (even? num)
    (print-even num)
    (oddp num)
    )
  )

(if-not (empty? args)
  (evenp (Integer/parseInt (first args)))
  )
