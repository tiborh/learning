#!/usr/bin/env clj

(def a #{1 2 3})
(def b #{2 3 4})
(def c #{3 4 5})

(print "a: ")(println a)
(print "b: ")(println b)
(print "c: ")(println c)

(require '[clojure.set :as cset :refer [union]])

;; because of ":as cset":
(print "union of a and b: ")
(println (cset/union a b))

;; because of ":refer [union]":
(print "union of a and c: ")
(println (union a c))

;; but other than union, only the former way:
(print "intersection of a and c: ")
(println (cset/intersection a c))
