#!/usr/bin/env clj

(defn namespace-now []
  (print "namespace now: ")
  (println *ns*)
  )

(namespace-now)

(ns hello.ns)
(defn namespace-now []
  (print "overriden: ")
  (user/namespace-now)  
  )

(namespace-now)
