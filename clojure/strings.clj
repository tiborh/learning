#!/usr/bin/env clj

(def args *command-line-args*)

;;(. System exit 0)

(use '[clojure.string :only (join split)])

(def s0 "This is a sample string to test.")

(println s0)
(println (join "\n" (split s0 #"\s+")))
