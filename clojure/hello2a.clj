#!/usr/bin/env clj
(ns clojure-examples.hello (:gen-class)) ; namespace

(defn hello []                          ; defun
  (println (str "Hello, world!"))             ; `print` also exists
  (print (+ 1 2))
  (println (* 3 4))
  )

(hello)
