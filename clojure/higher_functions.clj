#!/usr/bin/env clj

(def args *command-line-args*)

(def nums [0 1 2 3 4 5 6])

;;(. System exit 0)

(println (str "The nums: " nums))
(print "squared: ") (println (map #(* % %) nums))
(print "filtered: ") (println (filter even? nums))
(print "removed: ") (println (remove even? nums))
(print "filtered a mapped output: ") (println (filter even? (map #(* 3 %) nums)))
(println (str "adding numbers in vector: " (str nums) ": " (apply + nums)))
(println (str "reduce behaves the same way: " (str nums)  ": " (reduce + nums)))

(defn concat-fields [& fields]          ; any number of args, passed as a list
  (clojure.string/join ", " (remove empty? fields))
  )

(def sample-txt-list ["" "1 Main Street" "Toronto" nil "Canada"])

(println (str "The input: " sample-txt-list))
(println (str "The output: " (apply concat-fields sample-txt-list)))
(assert (= "" (concat-fields)))
(assert (= "" (concat-fields nil)))
(assert (= "" (concat-fields "")))
