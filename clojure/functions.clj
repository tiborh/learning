#!/usr/bin/env clj

(def args *command-line-args*)

;; assigning an anonymous function to a variable is identical to defining a named function

(def a (fn [arg] (println arg)))
(defn b [arg] (println arg))

;;(. System exit 0)

(def txt "Same result")
(a txt)
(b txt)
((fn [arg] (println arg)) txt)
(#(println %) txt)
(#(println %1 %2 %3) "one" "two" "three")

;; no distintion between functions and variables
(defn add-two [arg1 arg2]
  (+ arg1 arg2)
  )

(defn add-two2 [fun1 arg1 arg2]
  (fun1 arg1 arg2)
  )

(println (str "1 + 2 == " (add-two 1 2)))
(println (str "1 + 2 == " (add-two2 add-two 1 2)))
