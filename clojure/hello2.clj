#!/usr/bin/env clj
(ns clojure-examples.hello (:gen-class)) ; namespace

(defn hello []                          ; defun
  (println "Hello, world!")             ; `print` also exists
  )

(hello)
