(ns second.core)

(defn greet
  ([] (greet "World"))
  ([name] (greet "Hello" name))
  ([salutation name]
    (println (str salutation ", " name "!"))))
