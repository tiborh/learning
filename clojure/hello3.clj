#!/usr/bin/env clj

(def args *command-line-args*)

(defn hello [name]
  (println (str "Hello, " name "!")))

(if (empty? args)
  (hello "World")
  (doseq [name args]
    (hello name)))
