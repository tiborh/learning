#!/usr/bin/env clj

(def args *command-line-args*)

;;(. System exit 0)

(defn greeting [greeting-string]
  (fn [guest] (println (str greeting-string guest)))
  )

(let [greet (greeting "Welcome to the wonderful world of Clojure, ")
      names (list "Jane" "John" "Xenia" "Yorwick" "Zane")
      names2 ["Jane" "John" "Xenia" "Yorwick" "Zane"]
      ]
  (greet "Abel")
  (greet "Abraham")
  (println "Names: " names)
  ;; (println (type (remove nil? (map greet names)))) ; lazy sequence
  ;; (println (remove nil? (map greet names)))
  ;; (println (type (map greet names2))) ; lazy sequence
  ;; (println (remove nil? (map greet names2)))
  
  ;; this is the real one:
  (doall (map greet names2))
  )
