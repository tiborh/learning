#!/usr/bin/env clj

(def args *command-line-args*)

;;(. System exit 0)

(require '[clojure.string :as str])

(def s0 "This is a sample   string to		test.")

(println s0)
(println (str/join "\n" (str/split s0 #"\s+")))
(println (str/split s0 #""))
