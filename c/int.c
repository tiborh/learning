#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */

int main (int argc, char** argv) {
  unsigned int a = 0;
  unsigned long long int b = 0;
  printf("unsigned 0 and one minus: %u %u\n",a,a-1);
  printf("unsigned 0 and one minus: %llu %llu\n",b,b-1);
  printf("size of char: %d\n", sizeof(char));
  printf("size of short int: %d\n", sizeof(short));
  printf("size of int: %d\n", sizeof(int));
  printf("size of long int: %d\n", sizeof(long int));
  printf("size of long long int: %d\n", sizeof(long long int));
  return 0;
}
