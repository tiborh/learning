#include <stdio.h>
#include <math.h>		/* do not forget -lm at compilation time */
#include <libgen.h>		/* for basename and dirname */
#include <string.h>
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */

int remaindr(int dividend, int divisor) {
  return(dividend - (dividend/divisor)*divisor);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    printf("Usage: %s <int-dividend> <int-divisor>\n",basename(argv[0]));
    exit(1);
  }
  int a,b;
  a = b = 0;
  sscanf(argv[1],"%d",&a);
  sscanf(argv[2],"%d",&b);
  printf("the remainder of %d mod %d is %d\n",a,b,remaindr(a,b));
  printf("the remainder from math lib: %lf\n",remainder((double)a,(double)b));
  printf("same with modulus operator: %d\n",a % b);
  return 0;
}
