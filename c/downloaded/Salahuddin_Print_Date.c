/*
   Program that prints the current date and the next day.
   The functions assume that February has 28 days and know how many days there are in each month.
   4/1/2020
   Antonio Piazzolla
*/

#include <stdio.h>

typedef enum month {
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEPT,
    OCT,
    NOV,
    DEC
} month;

typedef struct date {
    month m;
    short int d;
} date;

void nextMonth(date *d);
void nextDay(date *d);
void printDate(date d);

int main(void)
{
    // Prints current date and next day for February 28
    date d1 = {FEB, 28};
    printf("%s", "Current date: ");
    printDate(d1);
    printf("%s", "Next day: ");
    nextDay(&d1);
    printDate(d1);
    // Adds new line
    puts("");
    // Prints current date and next day for March 14
    date d2 = {MAR, 14};
    printf("%s", "Current date: ");
    printDate(d2);
    printf("%s", "Next day: ");
    nextDay(&d2);
    printDate(d2);
    // Adds new line
    puts("");
    // Prints current date and next day for October 31
    date d3 = {OCT, 31};
    printf("%s", "Current date: ");
    printDate(d3);
    printf("%s", "Next day: ");
    nextDay(&d3);
    printDate(d3);
    // Adds new line
    puts("");
    // Prints current date and next day for December 31
    date d4 = {DEC, 31};
    printf("%s", "Current date: ");
    printDate(d4);
    printf("%s", "Next day: ");
    nextDay(&d4);
    printDate(d4);
}

// Sets the next month
void nextMonth(date *d)
{
    d->m = (d->m + 1) % 12;
}

// Sets the next day
void nextDay(date *d)
{
    short int daysInMonth = 0; // The total days in month

    // Calculate how many days there are in the month
    switch (d->m) {
        case NOV:
        case APR:
        case JUN:
        case SEPT:
            daysInMonth = 30;
            break;
        case JAN:
        case MAR:
        case MAY:
        case JUL:
        case AUG:
        case OCT:
        case DEC:
            daysInMonth = 31;
            break;
        case FEB:
            daysInMonth = 28;
            break;
    }

    if (d->d < daysInMonth) {
        d->d++;
    } else {
        // if the day is greater than daysInMonth it means that we
        // have exceeded the number of days of the current month, then
        // the day is set to 1 and the month is set to the next.
        d->d = 1;
        nextMonth(d);
    }
}

// Prints the date legibly
void printDate(date d)
{
    switch (d.m) {
        case 0:
            printf("%s", "January");
            break;
        case 1:
            printf("%s", "February");
            break;
        case 2:
            printf("%s", "March");
            break;
        case 3:
            printf("%s", "April");
            break;
        case 4:
            printf("%s", "May");
            break;
        case 5:
            printf("%s", "June");
            break;
        case 6:
            printf("%s", "July");
            break;
        case 7:
            printf("%s", "August");
            break;
        case 8:
            printf("%s", "September");
            break;
        case 9:
            printf("%s", "October");
            break;
        case 10:
            printf("%s", "November");
            break;
        case 11:
            printf("%s", "December");
            break;
    }

    printf(" %hd\n", d.d);
}
