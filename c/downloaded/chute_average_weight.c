/*
 * Peer-graded Assignment: Compute the average weight for a population of elephant seals read into an array
 * Coursera C for Everyone: Fundamentals (week 5)
 * wjs - 2019/11/03
 *
 * Note: An array is used here, though not needed, to comply with the assignment requisites
 *
 * Usage: prog < elephant_seal_data.txt
 *
 * Expected average weight with supplied data set: 6840.015
 */

#include <stdio.h>      // needed for printf() and scanf()

#define MAXSIZE 5000    // maximum allowed sample size of elephant seals - increase according to needs

int main(void)
{ 
    int sampleData[MAXSIZE] = {0};  // array holding individual weights
    int sampleSize = 0;             // counter for number of weights in array
    double weightSum = 0.0;         // double to force decimals in the average - overkill in this case

    // using pointer arithmetic in scanf() to assign weights into the array
    // sampleSize < MAXSIZE serves as a watchdog to prevent array overflow 
    while (scanf("%d", sampleData + sampleSize) == 1 && sampleSize < MAXSIZE)
        sampleSize++;

    // sum the weights of the elephant seals, coercing them to doubles
    for (int i = 0; i < sampleSize; i++)
        weightSum += sampleData[i];

    printf("\nThe average weight of the %d elephant seal sample is: %.3lf\n\n", sampleSize, weightSum / sampleSize);

    return 0; // all OK
}
