/*
 * Peer-graded Assignment: Assignment 2 - Working with lists - Option 1 (General)
 * Coursera C for Everyone: Structured Programming (week 1)
 * wjs - 2019/12/13
 *
 * Two bubble sorting functions are implemented:
 * - sortList() sorts the list by swapping adjacent node data
 * - sortNodes() sorts the list by rearranging node sequence
 *
 * Note: sortList() is, as expected, more than twice as fast as sortNodes()
 *
 * sortList() is used by default - comment & uncomment in main() to test either
 *
 */

#include <stdio.h>      // for printf()
#include <time.h>       // for random seeding
#include <stdlib.h>     // for random


// structure holding payload (unsigned int) and pointer to next node
typedef struct node_
{
    unsigned int data;  // payload
    struct node_ *next; // next node or NULL
} node_;


// function prototypes
void prependNode(node_**, unsigned int); // create new node and append old list to it
void printList(node_*); // print list in rows of 5 columns to console
unsigned int countNodes(node_*); // count nodes in list pointed by head
void sortList(node_*); // bubble sort list by swapping node data
void sortNodes(node_**); // bubble sort list by swapping node links



int main()
{
    const unsigned int listSize = 100; // number of nodes - 100 for assignment - slow above 30000
    node_ *list = NULL; // create empty list

    srand(time(NULL)); // seed random generator once

    // create and prepend listSize nodes to list
    for (unsigned int i = 0; i < listSize; i++)
        prependNode(&list, rand() % 1000); // populate list with random data (ints < 1000)

    puts("unsorted:");
    printList(list);


    // choose sort method by commenting one function & uncommenting the other one
    //sortList(list); // bubble sort by swapping adjacent node data
    sortNodes(&list); // bubble sort by rearranging node links - needs address of list head

    puts("sorted:");
    printList(list);

    return 0;
}


// create new node and append old list to it
void prependNode(node_ **head, unsigned int data)
{
    node_ *node = malloc(sizeof(node_)); // allocate memory for new node
    node->data = data;
    node->next = *head; // link old list to new node
    *head = node; // update list head
}


// print list in rows of 5 columns to console
void printList(node_ *head)
{
    unsigned short cnt = 0; // new line counter
    node_ *ptr = head; // point to first list node  

    printf("\n"); // cosmetics

    while (ptr) // loop until end of list (ptr == NULL)
    {
        // print data and add a new line every five columns
        printf("%6d%s", ptr->data, ++cnt % 5 ? "" : "\n");

        ptr = ptr->next; // advance to next list node
    }

    printf("\n"); // cosmetics
}


// count nodes in list pointed by head - needed by bubble sort
unsigned int countNodes(node_ *head)
{
    unsigned int count = 0;
    node_ *ptr = head; // point to first list node  
    
    while (ptr) // loop until end of list (ptr == NULL)
    {
        ptr = ptr->next; // advance to next list node
        count++;
    }
    return count;
}


// bubble sort list by swapping node data
void sortList(node_ *head)// pointer of list head
{
    unsigned int count = countNodes(head); // number of unsorted nodes in list
    unsigned short sorted; // flag to check if list is sorted
    node_ *ptr; // temporary node pointer
    unsigned int tmp; // temporary storage for data swapping

    while (count > 1) // count holds nbr of possibly unsorted nodes
    {
        ptr = head; // set pointer to beginning of list
        sorted = 1; // presume list is sorted 

        // loop through the remaining unsorted nodes
        for (unsigned int i = 1; i < count; i++)
        {
            // swap data if node data > next node data
            if (ptr->data > ptr->next->data)
            {
                tmp = ptr->data;
                ptr->data = ptr->next->data;
                ptr->next->data = tmp;
                sorted = 0; // flag list as modified (unsorted)
            }
            ptr = ptr->next; // advance to next list node
        }
        if (sorted) // early loop exit if list is sorted
            break;
        count--; // decrement unsorted list length
    }
}


// bubble sort list by swapping node links
void sortNodes(node_ **head) // pointer to pointer of list head
{
    unsigned int count = countNodes(*head); // number of unsorted nodes in list
    unsigned short sorted; // flag to check if list is sorted
    node_ *ptr, *prv, *tmp; // temporary list node pointers

    while (count > 1) // count holds nbr of possibly unsorted nodes
    {
        ptr = *head; // set pointer to beginning of list
        prv = NULL; // reset previous node - used for head change detection
        sorted = 1; // presume nodes are sorted 

        // loop through the remaining unsorted nodes
        for (unsigned int i = 1; i < count; i++)
        {
            // swap node links if node data > next node data
            if (ptr->data > ptr->next->data)
            {
                tmp = ptr->next;
                ptr->next = tmp->next;
                tmp->next = ptr;
                sorted = 0; // flag list as modified (unsorted)

                if (!prv) // at top of list so change list head
                    *head = ptr = tmp;
                else // else change previous node link
                    prv->next = ptr = tmp;
            }
            prv = ptr; // save node pointer as previous node 
            ptr = ptr->next; // advance to next node
        }
        if (sorted) // early loop exit if list is sorted
            break;
        count--;
    }
}
