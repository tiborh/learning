/*  Problem Statement: Read the elephant seal weights from the file elephant_seal_data.txt into an array and compute the average weight for     a set of elephant seals.
    Author: striker
    Date: 04 Nov 2019
*/

/*  The program is command line arguments based program which takes the name of the directory in which the file is as input.  
    E.g. If the file is in the current working directory, then just pass the name of the file other-wise if the file is in some other directory, then pass the full path of that file in the program.

    I am using the better algorithm to calculate the average weight as discussed by professor in the pdf: https://www.coursera.org/learn/c-for-everyone/discussions/weeks/2/threads/lCkGXvYlEemGUw6LmFrj5g
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define MAX_SIZE 1000 // As there are weights of 1000 elephant seal given in the file elephant_seal_data.

double compute_average_elephant_seal_weight(int *const); // Return the average weight.

int main(int argc,const char *argv[]) {
    if(argc < 2 || argc > 2) {
        fprintf(stderr,"Insufficient or More number of arguments supplied\n");
        return EXIT_FAILURE;
    } else {
        FILE *elephant_weight = fopen(argv[1],"r"); // Open the file elephat_seal_data.txt in read-mode.
        if(elephant_weight) {
            int elephant_weights[MAX_SIZE];
            for(int i = 0; i < MAX_SIZE; ++i) {
                fscanf(elephant_weight,"%d",&elephant_weights[i]);
            }
            printf("\nAverage Elephant Seal Weight for the given set of data is %e.\n\n",compute_average_elephant_seal_weight(elephant_weights));
            fclose(elephant_weight);
        } else {
            fprintf(stderr,"File not openened successfully\n");
        }
    }
    return EXIT_SUCCESS;
}

double compute_average_elephant_seal_weight(int *const elephant_weights) {
    double avg = 0.0;
    for(int i = 0; i < MAX_SIZE; ++i) {
        avg += ((elephant_weights[i] - avg) / (i + 1));
    }
    return avg;
}
