#include <stdio.h>
#include <stdlib.h>

int main(void){

    FILE *fptr;
    int i;
    
    fptr = fopen("elephant_seal_data.txt","r");
    if(fptr == NULL){
       printf("Error!");
       exit(1);
    }

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);

    int value;
    double sum = 0;
    int num_of_values = 0;
    
    for(i = 0; i < fsize; i++){
        fscanf(fptr, "%d ", &value);
	printf("%d, ",value);
	++ num_of_values;
    	sum += value; 
    }

    printf("The average weight of elephant seals is %lf.\n", sum/fsize);
    printf("Number of values: %d\n",num_of_values);

    fclose(fptr);

    return 0;
}
