/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#define NUM_OF_TESTS 5

typedef enum month{ Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec} month;
typedef struct date{ month mont; int day;} date;

const int MAX_DAYS_PER_MONTH [12] = {31,28,31,30,31,30,31,31,30,31,30,31};
const char* MonthNames[] = {"Jan", "Feb",  "March", "April", "May", "June", "July", "August", "Sep", "Oct", "Nov", "Dec"};

date next_day (date Input);
void test_dates(void);

/* Test data from assignment: */
const date d0 = {Jan, 1};
const date d1 = {Feb, 28};
const date d2 = {Mar, 14};
const date d3 = {Oct, 31};
const date d4 = {Dec, 31};
const date dates[] = {d0,d1,d2,d3,d4};

int main()
{
    date Today;
    date Tomorrow;
    Today.mont = Jan;
    Today.day = 1;
    
    Tomorrow = next_day(Today);
    
    printf ("Today is %d %s \n", Today.day, MonthNames[Today.mont]);
    printf("Next day is : %d  %s\n ",Tomorrow.day, MonthNames[Tomorrow.mont]);

    puts("Testing dates:");
    test_dates();

    return 0;
}

void test_dates(void) {
  for(int i = 0; i < NUM_OF_TESTS; ++i) {
    printf ("Today is %d %s \n", dates[i].day, MonthNames[dates[i].mont]);
    printf("Next day is : %d  %s\n ",next_day(dates[i]).day, MonthNames[next_day(dates[i]).mont]);
  }
}

date next_day (date Input)
{
    date Out;
   if (Input.day == MAX_DAYS_PER_MONTH[Input.mont])
   {
       Out.mont  = Input.mont + 1;
       Out.day = 1;
   }
   else
   {
     Out.day = Input.day +1;
     Out.mont = Input.mont;
   }
   return Out;
}
