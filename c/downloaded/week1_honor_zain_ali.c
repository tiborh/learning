#include <stdio.h>
#include <stdlib.h>
#include<time.h>
#include <stdbool.h>

#define SUIT_SIZE 4 // Suit size
#define PIP_SIZE 13 // Pip Size
#define DECK_SIZE SUIT_SIZE * PIP_SIZE  // Deck Size
#define HAND_SIZE 7     // 7 Cards Hand Size
#define SAMPLE_SIZE 1000000 // 1 Million Sample Size

int Prob_noPair = 0;
int Prob_onePair = 0;
int Prob_twoPair = 0;
int Prob_threeOfKind = 0;
int Prob_fullHouse = 0;
int Prob_fourOfKind = 0;

typedef enum suit {Heart, Diamond, Spades, Clubs} suit;
const char SuitNames[][10] = {"Heart", "Diamond", "Spades", "Clubs"};
const char PipNames[][10] = {"King", "Queen", "Jack", "Ten", "Nine", "Eight", "Seven", "Six", "Five", "Four", "Three", "Two", "Ace"};
typedef struct card{    suit _suit;    short _pip;} card;

void print_hand(card mycards[]){
  int i;
  for(i = 0; i < HAND_SIZE; i++) {
    printf("\n%s %s\n",SuitNames[mycards[i]._suit],PipNames[mycards[i]._pip]);
  }
}

void print_deck(card mycards[]){
  int i;
  for(i = 0; i < DECK_SIZE; i++) {
    printf("\n%s %s\n",SuitNames[mycards[i]._suit],PipNames[mycards[i]._pip]);
  }
}
// TODO Deal Deck
void deal_deck(card mycards[]){
  short i = 0;
  short j = 0;
  short itt = 0;
  for(i = 0; i < SUIT_SIZE; i++) {
    for(j = 0; j < PIP_SIZE; j++) {
      mycards[itt]._suit = i;
      mycards[itt]._pip  = j;
      itt++;
    }
  }
}
// TODO Swap two cards
void swap_cards(card *card_1, card *card_2){
  card card_temp = *card_2;
  *card_2 = *card_1;
  *card_1 = card_temp;
}
// TODO Shuffle Deck
void shuffle_deck(card mycards[]){
  short i = 0;
  // Short variable for itteration
  short r = 0;
  // Short variable for random number
  for(i = 0; i < DECK_SIZE; i++) {
    r = (int)((double)rand() / ((double)RAND_MAX + 1) * DECK_SIZE);
    // Generating random number between 0-51 i.e the size of deck
    swap_cards(&mycards[i],&mycards[r]);
    // Swap the present index card with the newly generated random number card
  }
} 
// TODO Get 7 hand
void get_7Hand(card mycards[],card cards_hand[]){
  int i=0;
  //int r=0;
  for(i=0;i<HAND_SIZE;i++) {
    cards_hand[i] = mycards[i];
  }
}

// TODO Get Frequencies of ranks

void get_rank_count(card mycards[],int **count) {
  int *_count;
  int i;
  // Create array of count and initialize elements with zeros
  _count = (int*)calloc(PIP_SIZE,sizeof(int));
  for(i = 0; i < HAND_SIZE; i++) {
    _count[mycards[i]._pip]++;
  }
  *count = _count;
}    

void check_Hand(card cards_hand[]){
  int i;
  int *freq;
  int pair_count = 0;
  bool noPair = true;
  bool onePair = false;
  bool twoPair = false;
  bool threeOfKind = false;
  bool fullHouse = false;
  bool fourOfKind = false;
  get_rank_count(cards_hand,&freq);      
  for (i = 0; i < PIP_SIZE; i++) {
    if(freq[i] == 2) {
      noPair = false;
      onePair = true;
      pair_count++;
    } else if (freq[i] == 3) {
      noPair = false;
      threeOfKind = true;
    } else if(freq[i] >= 4) {
      noPair = false;
      fourOfKind = true;
    }
  }       
    if(fourOfKind) {
      threeOfKind = false;
      twoPair = false;
      onePair = false;
    } else if(threeOfKind & onePair) {
      fullHouse = true;
      threeOfKind = false;
      twoPair = false;
      onePair = false;
    } else if(pair_count > 1) {
      twoPair = true;
      onePair = false;
    }
    if(noPair) {
      Prob_noPair++;
      //printf("\n No Pair");
    } else if(onePair) {
	Prob_onePair++;
        //printf("\n One Pair");
    } else if(twoPair) {
      Prob_twoPair++;
      //printf("\n Two Pair");
    } else if(threeOfKind) {
      Prob_threeOfKind++;
      // printf("\n Three of a kind");
    } else if(fullHouse) {
      Prob_fullHouse++;
      // printf("\n Full house");
    } else if(fourOfKind) {
      Prob_fourOfKind++;
      //printf("\n Four of a kind");
    }
}

card ran_card[DECK_SIZE];
card cards_hand[HAND_SIZE];
int i=0;

int main(){
  srand(time(0));
  // Setting Seed for Random Number Generator
  for (i = 0; i < SAMPLE_SIZE; i++) {
    //    printf("\nIteration %d",i);
    deal_deck(ran_card);
    // Deal a deck
    shuffle_deck(ran_card);
    // Shuffle the deck
    get_7Hand(ran_card,cards_hand);
    check_Hand(cards_hand);
  }
  printf("\n----------------------------------------------");
  printf("\nNo Pair Occurances %d",Prob_noPair);
  printf("\nOne Pair Occurances %d",Prob_onePair);
  printf("\nTwo Pair Occurances %d",Prob_twoPair);
  printf("\nThree of a kind Occurances %d",Prob_threeOfKind);
  printf("\nFull House Occurances %d",Prob_fullHouse);
  printf("\nFour of a kind Occurances %d",Prob_fourOfKind);
  printf("\n----------------------------------------------");
  printf("\nNo Pair Probability %f",(double)Prob_noPair/SAMPLE_SIZE);
  printf("\nOne Pair Probability %f",(double)Prob_onePair/SAMPLE_SIZE);
  printf("\nTwo Pair Probability %f",(double)Prob_twoPair/SAMPLE_SIZE);
  printf("\nThree of a kind Probability %f",(double)Prob_threeOfKind/SAMPLE_SIZE);
  printf("\nFull House Probability %f",(double)Prob_fullHouse/SAMPLE_SIZE);
  printf("\nFour of a kind Probability %f",(double)Prob_fourOfKind/SAMPLE_SIZE);
  printf("\n----------------------------------------------");
  puts("");

  return 0;
}
