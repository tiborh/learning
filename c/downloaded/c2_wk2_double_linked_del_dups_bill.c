/*
 * Peer-graded Assignment: Honors Peer-graded Assignment: Honors Assignment 2
 * Coursera C for Everyone: Structured Programming (week 2)
 * wjs - 2019/12/18
 *
 * Three deduplication functions are implemented:
 * - dedupFast() deduplicates using a "seen" array
 * - dedupMed() deduplicates using "remove seen dups"
 * - dedupSlow() deduplicates using "sort and remove adjacent dups"
 *
 * As the list size (200) is small, the speed difference is hardly noticeable,
 * however with big lists (> 100,000), dedupSlow() become close to unusable
 *
 * dedupFast() is used by default - comment & uncomment in main() to test either
 * Change the listSize and randLimit values to see the timing variations.
 *
 *
 * Here timings for three different list size and random value runs:
 * 
 * Measuring timings for 200 nodes & 50 values...
 *  - dedupSlow:       0.104 ms - 50 nodes left
 *  - dedupMed:        0.023 ms - 50 nodes left
 *  - dedupFast:       0.006 ms - 50 nodes left
 * 
 * Measuring timings for 100000 nodes & 5000 values...
 *  - dedupSlow:   18182.694 ms - 5000 nodes left
 *  - dedupMed:     1320.616 ms - 5000 nodes left
 *  - dedupFast:       1.620 ms - 5000 nodes left
 * 
 *  Measuring timings for 100000 nodes & 100000 values...
 *  - dedupSlow:   21300.770 ms - 63110 nodes left
 *  - dedupMed:    17786.762 ms - 63110 nodes left
 *  - dedupFast:       2.544 ms - 63110 nodes left
 *
 */

#include <stdio.h>  // for printf()
#include <time.h>   // for random seeding and clock
#include <stdlib.h> // for random, malloc & free


// structure holding payload (unsigned int) and pointers to next & previous node
typedef struct node_
{
    unsigned int data;  // payload
    struct node_ *prev; // previous node ptr or NULL
    struct node_ *next; // next node ptr or NULL
} node_;


// function prototypes
void prependNode(node_**, unsigned int); // create new node and append old list to it
void deleteNode(node_**, node_*); // delete node and deallocate memory
unsigned int countNodes(node_*); // count nodes in a list
void deleteList(node_**); // delete all nodes in a list
void copyList(node_*, node_**); // make a copy of a list
void sortList(node_*); // bubble sort list by swapping node data
void printList(node_*, unsigned short); // print list in rows of x columns
void cleanPrint(node_*, char*, unsigned int, unsigned int); // print list reasonable sized list only
void dedupFast(node_**, unsigned int); // fast deduplication using a "seen" array
void dedupMed(node_**); // medium speed deduplication using "remove seen dups'
void dedupSlow(node_**); // slow deduplication using "sort and remove adjacent dups"
void testFuncs(node_*, unsigned int); // timing test of the 3 deduplication functions


int main()
{
    unsigned int listSize = 200; // number of nodes - 200 for assignment - max +/- 50,000,000 for my PC RAM
    unsigned int randLimit = 50; // excluded upper bound of random numbers (0 to 49)
    node_ *list = NULL; // create empty doubly linked list
    node_ *copy = NULL; // used to compare deduplication methods

    srand(time(NULL)); // seed random generator once

    // create and prepend listSize nodes to list
    for (unsigned int i = 0; i < listSize; i++)
        prependNode(&list, rand() % randLimit); // populate list with random data (ints < randLimit)

    copyList(list, &copy); // make a new copy of the list for comparable timings

    cleanPrint(list, "Full", 500, 10); // print full list if reasonable size only

    // +++++ choose deduplication method by commenting one function & uncommenting the others +++++
    dedupFast(&list, randLimit); // deduplication using a "seen" array
    // dedupMed(&list); // deduplication using remove seen dups
    // dedupSlow(&list); // deduplication using sort and remove adjacent dups

    cleanPrint(list, "Deduplicated", 200, 10); // print full list if reasonable size only

    testFuncs(copy, randLimit); // clocks the three deduplication functions with the same list

    return 0;
}


// create new node and append old list to it
void prependNode(node_ **head, unsigned int data)
{
    node_ *node = malloc(sizeof(node_)); // allocate memory for new node
    node->data = data;
    node->prev = NULL;
    node->next = *head; // link old list to new node
    if (*head) // update prev if old list not empty
        (*head)->prev = node;
    *head = node; // update list head
}


// delete node and deallocate its memory
void deleteNode(node_ **head, node_ *node)  // ** pointer to pointer of head
{
    if (node == NULL || *head == NULL) // list/node is empty, do nothing
        return;
        
    if (node == *head) // node is head
        *head = node->next; // change head to next node 

    if (node->next) // if not tail / last node in list
        node->next->prev = node->prev; // correct link (NULL if head)

    if (node->prev) // if not head / first node in list
        node->prev->next = node->next; // correct link (NULL if tail)

    free(node); // deallocate memory
}


// count nodes in list pointed by head
unsigned int countNodes(node_ *head)
{
    unsigned int count = 0;
    node_ *ptr = head; // point to first list node  
    
    while (ptr) // loop until end of list (ptr == NULL)
    {
        ptr = ptr->next; // advance to next list node
        count++;
    }
    return count;
}


// delete all nodes in a list
void deleteList(node_ **list)
{
    while (*list)
        deleteNode(list, *list);
}

// make a copy of list1 to list2 in same order
void copyList(node_ *list1, node_ **list2)
{
    while (*list2) // delete list2 if not empty
        deleteNode(list2, *list2);

    while (list1->next) // go to last node of list1
        list1 = list1->next;

    // prepend reversed list1 nodes to list2
    while (list1)
    {
        prependNode(list2, list1->data);
        list1 = list1 ? list1->prev : NULL;
    }
}


// bubble sort list by swapping node data only - head ptr does never change
void sortList(node_ *head)
{
    unsigned int count = countNodes(head); // number of unsorted nodes in list
    unsigned short sorted; // flag to check if list is sorted
    node_ *ptr; // temporary node pointer
    unsigned int tmp; // temporary storage for data swapping

    while (count > 1) // count holds nbr of possibly unsorted nodes
    {
        ptr = head; // set pointer to beginning of list
        sorted = 1; // presume list is sorted 

        // loop through the remaining unsorted nodes
        for (unsigned int i = 1; i < count; i++)
        {
            // swap data if node data > next node data
            if (ptr->data > ptr->next->data)
            {
                tmp = ptr->data;
                ptr->data = ptr->next->data;
                ptr->next->data = tmp;
                sorted = 0; // flag list as modified (unsorted)
            }
            ptr = ptr->next; // advance to next list node
        }
        if (sorted) // early loop exit if list is sorted
            break;
        count--; // decrement unsorted list length
    }
}


// print list in rows of cols columns to console
void printList(node_ *head, unsigned short cols)
{
    unsigned short cnt = 0; // new line counter
    node_ *ptr = head; // point to first list node  

    if (ptr == NULL)
    {
        printf("[ empty ]\n\n");
        return;
    }

    printf("[");
    while (ptr) // loop until end of list (ptr == NULL)
    {
        // print data and add a new line every nbr columns
        printf("%3d", ptr->data);
        printf("%s%s", ptr->next ? "," : " ]", ++cnt % cols ? "" : "\n ");

        ptr = ptr->next; // advance to next list node
    }
    printf("%s\n", cnt % cols ? "\n" : ""); // cosmetics
}


// print list if reasonable size only - uses printList()
void cleanPrint(node_ *list, char *txt, unsigned int max, unsigned int cols)
{
    unsigned int listSize = countNodes(list);
    unsigned short cnt = 0; // printed node counter
    node_ *ptr = list; // point to first list node  

    printf("\n%s %d-node list:\n", txt, listSize);

    if (listSize <= max || listSize <= cols * 3) // print list only if size acceptable
        printList(list, cols);
    
    else // print head and foot cols nodes of list
    {
        printf("[");
        while (ptr && ++cnt <= cols) // loop cols first nodes
        {
            printf(" %d,", ptr->data);
            ptr = ptr->next; // advance to next node
        }
        
        printf("\n ... too long to print all nodes ...\n");

        while (ptr->next) // goto last node of list
            ptr = ptr->next;
        
        while (ptr && --cnt > 1) // revert back cols nodes
            ptr = ptr->prev;

        while (ptr) // loop until end of list
        {
            printf(" %d", ptr->data);
            printf("%s", ptr->next ? "," : " ]");
            ptr = ptr->next ? ptr->next : NULL; // advance to next node
        }
        printf("\n\n"); // cosmetics
    }
}


// fast deduplication using a "seen" array - head ptr does never change
void dedupFast(node_ **list, unsigned int rndLim)
{
    unsigned int *seen = malloc(rndLim * sizeof(unsigned int)); // allocate duplicate checking array
    node_ *node = *list, *tmp; // temporary node pointers
    
    for (unsigned int i = 0; i < rndLim; i++)
        seen[i] = 0; // init array elements to "not seen";

    while (node)  // node != NULL
    {
        if (seen[node->data]) // delete node if data already seen
        {
            tmp = node->prev;
            deleteNode(list, node);
            node = tmp; // update node address to node before deleted node
        }
        else
            seen[node->data]++; // mark node as seen

        node = node ? node->next : NULL; // check pointer validity and advance to next node
    }
    free(seen); // deallocate array memory
}


// medium speed deduplication using "remove seen dups'
void dedupMed(node_ **list) // needs ** to change head of list (never happens here)
{
    node_ *node = *list, *tmp, *next; // temporary node pointers

    while (node)  // node != NULL
    {
        next = node->next;
        while (next)
        {
            if (next && node->data == next->data) // delete seen node if data is equal
            {
                tmp = next->prev;;
                deleteNode(list, next);
                next = tmp; // update node address to node before deleted node
            }
            next = next ? next->next : NULL;
        }
        node = node ? node->next : NULL; // check pointer validity and advance to next node
    }
}


// slow deduplication using "sort and remove adjacent dups"
void dedupSlow(node_ **list) // needs ** to change head of list (never happens here)
{
    node_ *node = *list; // temporary node pointer

    sortList(*list); // dereferenced list pointer here

    while (node)  // node != NULL
    {
        while (node->next && node->data == node->next->data) // delete next node if adjacent data is equal
            deleteNode(list, node->next); // remember, list is already pointer to pointer here

        node = node ? node->next : NULL; // check pointer validity and advance to next node
    }
}


// timing test of the 3 deduplication functions
void testFuncs(node_ *list, unsigned int rndLim)
{
    node_ *lst = NULL; // copy of original list
    clock_t begin, end; // for timing purposes
    unsigned int lstSiz = countNodes(list); // nodes in list

    printf("\nMeasuring timings for %d nodes & %d values...\n", lstSiz, rndLim);

    if (lstSiz <= 100000)
    {
        copyList(list, &lst);
        begin = clock();
        dedupSlow(&lst); // deduplication using sort and remove adjacent dups
        end = clock();
        printf(" - dedupSlow: %11.3lf ms - %d nodes left\n", (end - begin) * 1000.0 / CLOCKS_PER_SEC, countNodes(lst));
        deleteList(&lst);
    }
    else
        printf(" - dedupSlow: too slow for %d nodes\n", lstSiz);


    if (lstSiz <= 50000000 && (long) lstSiz * rndLim <= 10000000000)
    {
        copyList(list, &lst);
        begin = clock();
        dedupMed(&lst); // deduplication using remove seen dups
        end = clock();
        printf(" - dedupMed:  %11.3lf ms - %d nodes left\n", (end - begin) * 1000.0 / CLOCKS_PER_SEC, countNodes(lst));
        deleteList(&lst);
    }
    else
        printf(" - dedupMed:  too slow for %d nodes & %d values\n", lstSiz, rndLim);


    if (lstSiz <= 50000000)
    {
        copyList(list, &lst);
        begin = clock();
        dedupFast(&lst, rndLim); // deduplication using a "seen" array
        end = clock();
        printf(" - dedupFast: %11.3lf ms - %d nodes left\n\n", (end - begin) * 1000.0 / CLOCKS_PER_SEC, countNodes(lst));
        deleteList(&lst);
    }
    else
        printf(" - dedupFast: too big with %d nodes\n", lstSiz);
} 
