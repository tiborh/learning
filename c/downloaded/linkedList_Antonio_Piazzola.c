/*
   Program that uses a linear linked list to store a randomly generated set of 100 integers, 
   sorts the list of these values and prints these values in rows of 5 on the screen.
   08/01/2020
   Antonio Piazzolla
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LEN_LIST 10000   // number of linked list items

typedef struct node {
    int data;
    struct node *next;
} node;

node *createNode(int num);
node *addNode(node *head, node *newNode);
void swap(node *a, node *b);
void bubbleSort(node *head);
void printList(const node *head);

int main(void)
{
    // randomize random number generator using current time
    srand(time(NULL));

    node *head = NULL;

    // create a linked list of LEN_LIST elements
    for (unsigned int i = 0; i < LEN_LIST; i++) {
        int number = rand();
        node *newNode = createNode(number);
        head = addNode(head, newNode);
    }

    bubbleSort(head);
    printList(head);
}

// Create a node and set it with the passed value
node *createNode(int num)
{
    node *newNode = (node *) malloc(sizeof(node));

    // if the memory to be allocated is available, sets the node with data // and return it, otherwise return NULL
    if (newNode != NULL) {
        newNode->data = num;
        newNode->next = NULL;

        return newNode;
    }

    return NULL;
}

// Add a node to the end of the linked list
node *addNode(node *head, node *newNode)
{
    node *current = NULL;

    // if head is NULL, it means that the linked list is empty
    // then head point to newNode
    if (head == NULL) {
        head = newNode;
    } else {
        // the linked list is not empty, then current point to head
        // and iterate through the linked list until the end
        current = head;

        while (current->next != NULL) {
            current = current->next;
        }

        // the end of the linked list is always NULL
        // and the last node of the list point to newNode
        current->next = newNode;
    }

    return head;
}

// Swap the data of the two passed nodes
void swap(node *a, node *b)
{
    // temporary variable for the exchange
    node temp = {0, NULL};

    temp.data = a->data;
    a->data = b->data;
    b->data = temp.data;
}

// Sort the linked list in ascending order
void bubbleSort(node *head)
{
    // sorting is performed if the linked list is not empty and 
    // if it does not consist of a single node.
    if (head != NULL && head->next != NULL) {
        node *current = head;

        while (current != NULL) {
            // set following to point to the adjacent current node
            node *following = current->next;
        
            while (following != NULL) {
                
                if (current->data > following->data) {
                    // swap the data of the two passed nodes
                    swap(current, following);
                }
            
                following = following->next;
            }
        
            current = current->next;
        }
    }
}

// Prints the data stored in a linked list
void printList(const node *head)
{
    const node *current = head;
    int i = 0;

    while (current != NULL) {
        // formatted print
        printf("%-12d", current->data);
        i++;

        // prints a new line each five integers
        if (i % 5 == 0) {
            puts("");
        }

        current = current->next;
    }
}