/*
 * Peer-graded Assignment: Assignment 1 - Option 1 (General)
 * Coursera C for Everyone: Structured Programming (week 1)
 * wjs - 2019/12/06
 *
 */

#include <stdio.h>

typedef enum month_ {jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec} month_;

typedef struct date_ {month_ month; unsigned short day;} date_;


// increments given date by one day and returns a pointer to the newly calculated date
date_* nextDay(date_ *date)
{
    static date_ nextDate;  // static as we do not want it to be dimissed/released at function exit
    unsigned short days;

    // assigns number of days of present month - needed for month roll-over detection
    switch(date->month)
    {
        case feb:
            days = 28;
            break;

        case apr:
        case jun:
        case sep:
        case nov:
            days = 30;
            break;

        default:
            days = 31;
    }

    nextDate.day = date->day % days + 1;  // modulus used for month roll-over handling
    nextDate.month = (date->month + (nextDate.day == 1)) % 12;  // modulus used for year roll-over handling
    // (nextDate.day == 1) is equivalent to (nextDate.day == 1 ? 1 : 0)

    return &nextDate;
}


// prints a date structure in a more human readable form - note: no "new line" appended - returns number of chars printed
unsigned int printDate(date_ *date)
{
    char monthName[12][10] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    return printf("%s, %u", monthName[date->month], date->day);
}


int main()
{
    date_ dates[5] = {{jan, 1}, {feb, 28}, {mar, 14}, {oct, 31}, {dec, 31}};  // array of dates - see assignment

    // loop through the required assignment dates
    for (unsigned short idx = 0; idx < 5; idx++)
    {
        printf("%s\t -> \t", printDate(dates + idx) < 8 ? "   " : "");  // (?:) needed for correct table output (ex: May, 1 versus September, 11)
        printDate(nextDay(dates + idx));  // print next date (original date + 1 day)
        printf("\n");
    }
    
    return 0;
}
