#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char *argv[]){
  FILE *iF; int datasize,i;
  if (NULL == (iF = fopen("listofintegers.txt", "r")))  {
    printf("could not open input file\n");
    exit(1);
  }

  if(iF!=NULL) {
    fscanf(iF, "%d", &datasize);
  }

  printf("size: %d\n", datasize);
  
  int* data = calloc(datasize,sizeof(int));
  for(i=0;i<datasize;i++) {
    printf ("%d ", i);
    fscanf (iF, "%d ", data+i);
    printf("%d",data[i]);
  }
  putchar('\n');
  fclose(iF);

  double average(int data[],int datasize) {
    double avg=0;
    for(i=0;i<datasize;i++){
      avg=avg+data[i];
    }

    return (avg/datasize);
  }

  int max(int data[],int datasize) {
    int max = INT_MIN;

    for (i=0;i<datasize;i++)
      if(max<data[i]){
	max=data[i];
      }
    return max;
  }
  free(data);

  FILE *file;
  if (NULL == (file = fopen("answer-hw3.txt", "w")))
    fprintf(stderr,"Could not open output file.\n");
  else {
    fprintf(file,"average : %f and max %d",average(data,datasize),max(data,datasize));
    fclose(file);
  }

  return 0;
}
