/*
 * Peer-graded Assignment: Peer-graded Assignment: Assignment 3: Arrays (option 1)
 * Coursera C for Everyone: Structured Programming (week 3)
 * wjs - 2019/12/23 - start of project
 * wjs - 2019/12/25 - added sorting and median
 * wjs - 2019/12/26 - extensive refactoring
 *
 * General notes:
 * - may only work with file sizes < 2GB on 32-bit operating systems or compilers
 * - supposes integers only in file, separated by white space characters
 * - no more than 100 million integers - limited by design
 * - many checks are overkill for this assignment, but were fun to implement
 * - reports are by default appended (not overwritten) to file "answer-hw3.txt",
 *   change fopen("answer-hw3.txt", "a") to fopen("answer-hw3.txt", "w") in
 *   reportData(), if overwriting is preferred.
 * - change #define BOTH 1 to #define BOTH 0 if no reporting to file is preferred
 * - median calculation may take a long time for very large data amounts
 *
 * This assignment could have been written in less than 30 code lines (see elephant seal
 * average weight - C for Everyone: Fundamentals (week 5)). Many new concepts and ideas
 * have been tested here, and extensive sanity checks applied. Also, more statistical 
 * values than required are computed: min, max, avg, median and standard deviation. Some
 * lines exceed 80 characters so view the code in wide mode for your convenience.
 *
 * Note: add -lm at the end of the gcc command to compile and link the code correctly
 *       i.e.: gcc mySource.c -o myExec -lm
 *
 * Sorry for the length and thanks for reading & reviewing my code - Bill
 *
 *
 * Here a sample run on a 585 MB file with 100 million random integers:
 * 
 * Report created on 2019/12/27 at 15:37:51
 * =========================================
 * 
 * File information:
 * -----------------
 * file name: big.txt
 * file size: 585195789 bytes
 * content:   100000666 ints
 * first int: 100000000
 * 
 * General statistics:
 * -------------------
 * count:         100000000
 * minimum:          -12345
 * maximum:           54321
 * range:             66666
 * sum:       2098952455867
 * median:          20986.0
 * average:       20989.525
 * pstd dev:      19245.668
 * std dev:       19245.668
 * 
 * real    0m23,418s
 * user    0m22,850s
 * sys     0m0,568s
 *
 */


#include <stdio.h>  // for printf and file funcs
#include <stdlib.h> // for exit, calloc & free
#include <time.h>   // for date/time handling
#include <limits.h> // for min and max init values
#include <assert.h> // for sanity checks
#include <math.h>   // for sqrt, log10 & ceil

#define BOTH 1 // 1: file & console - 0: console only

// holds metadata and stats about input file
typedef struct fileInfo
{
    char *name;
    long int size;
    long int intsClaim;
    long int intsTot;
    long int minInt;
    long int maxInt;
    long int sumInts;
    long int median1;
    long int median2;
    double average;
    double runAvg;
    double pstdDev;
    double stdDev;
} fileInfo;


// function prototypes
void readFile(char*, fileInfo*, long int**); // open data file and read integers to an array
void analyzeData(fileInfo*, long int**); // analyze array data, then free array memory
void reportData(fileInfo*); // report data on screen and append report to an output file
void print(char*, FILE*, short); // output buffer to console, and possibly to file
int cmpfunc(const void*, const void*); // comparison function required by qsort
void errExit(char*, FILE*, long int*, short); // print error message, close file & exit



int main(int argc, char *argv[])
{
    fileInfo file; // struct holding file information
    long int *array = NULL; // pointer to data array

    if (argc != 2) // check command for single file name
        return printf("\nSyntax: %s dataFile\n\n", argv[0]); // always > 0

    readFile(argv[1], &file, &array); // overkill checking, read ints to array
    
    analyzeData(&file, &array); // array mem is freed after this function

    reportData(&file); // generate report and save it

    return 0;
}


// open data file, (overkill) sanity checks & read integers into an array
void readFile(char *name, fileInfo *file, long int **arr)
{
    FILE *fptr = NULL; // file handle/pointer
    char msg[100]; // error message buffer
    long int intNbr = 0, cnt;

    if ((fptr = fopen((file->name = name), "r")) == NULL) // open file if exists
        errExit(msg, fptr, *arr, sprintf(msg, "file %s does not exist", name));

    if (fseek(fptr, 0, SEEK_END) < 0) // point to end of file
        errExit(msg, fptr, *arr, sprintf(msg, "failure seeking file %s", name));

    file->size = ftell(fptr); // save file size
    rewind(fptr); // repoint to start of file

    if (fscanf(fptr, "%ld", &intNbr) != 1 || intNbr <= 0) // read first integer in file
        errExit(msg, fptr, *arr, sprintf(msg, "no integers to read in %s", name));

    if ((file->intsClaim = intNbr) > 100000000) // max allowed number of integers to read (100 million)
        errExit(msg, fptr, *arr, sprintf(msg, "too many integers (%ld) to read in %s", intNbr, name));

    if ((*arr = calloc(intNbr, sizeof(long int))) == NULL) // memory allocation for array failed
        errExit(msg, fptr, *arr, sprintf(msg, "memory allocation failed for %ld integers", intNbr));

    // read annouced ints to array, pre-calculating running average for standard deviation
    for (cnt = 0, file->runAvg = 0; cnt < intNbr && fscanf(fptr, "%ld", (*arr + cnt)) == 1; cnt++)
        file->runAvg += ((*arr)[cnt] - file->runAvg) / (cnt + 1);  // pre-calculate average

    if ((file->intsTot = cnt) < intNbr) // check data consistency
        errExit(msg, fptr, *arr, sprintf(msg, "found less integers (%ld) than expected (%ld)", cnt, intNbr));

    // count rest of integers in file, if any left
    while (fscanf(fptr, "%ld", &intNbr) == 1) file->intsTot++; 

    fclose(fptr);
}


// analyze array data, then free array memory
void analyzeData(fileInfo *file, long int **arr)
{
    long int min = LONG_MAX, max = LONG_MIN, sum = 0;
    double runAvg = 0.0; // used for runnning average
    double sdSum = 0.0; // temporary for std deviation

    // calculate min, max, tot, std dev and average of array
    for (long int idx = 0; idx < file->intsClaim; idx++)
    {
        min = min > (*arr)[idx] ? (*arr)[idx] : min;
        max = max < (*arr)[idx] ? (*arr)[idx] : max;
        sum += (*arr)[idx];
        sdSum += pow(((*arr)[idx] - file->runAvg), 2.0);
        runAvg += ((*arr)[idx] - runAvg) / (idx + 1);
    }

    // save results to file structure
    file->minInt = min;
    file->maxInt = max;
    file->sumInts = sum;
    file->average = runAvg;
    file->pstdDev = file->intsClaim < 2 ? 0 : sqrt(sdSum / file->intsClaim);
    file->stdDev = file->intsClaim < 2 ? 0 : sqrt(sdSum / (file->intsClaim - 1));

    // check new average result against previously calculated average
    assert(runAvg == file->runAvg); // sanity check - overkill
    
    // median calculation - takes time for large data amounts
    qsort(*arr, file->intsClaim, sizeof(long int), cmpfunc); // expensive sort
    file->median1 = (*arr)[(file->intsClaim - 1) >> 1]; // div by 2
    file->median2 = (*arr)[file->intsClaim >> 1]; // div by 2

    // free array memory and mark array as empty
    free(*arr);
    *arr = NULL;
}


// report data on screen and append report to an output file
void reportData(fileInfo *file)
{
    FILE *fptr = NULL; // output file handle/pointer
    time_t local; // used for report date
    struct tm tm; // needed for date/time display
    char buf[100]; // general char buffer
    unsigned int fmtSize = 1; // for results formatting
    unsigned int len = 0; // used for written buffer length
    char line[100]; // '---' or '===' line

    // prepare formatting params - max length needed
    fmtSize += log10(file->sumInts) < log10(file->average) + 4 ? ceil(log10(file->average) + 4) : ceil(log10(file->sumInts));

    while (len < 99) line[len++] = '=';
    line[len] = '\0'; // end of string of double line (===)

    // get local time
    local = time(NULL);
    tm = *localtime(&local);

    if (BOTH) // only if output to file flag is set
        if ((fptr = fopen("answer-hw3.txt", "a")) == NULL) // open/creat file for append
            errExit(buf, fptr, (long int*) NULL, sprintf(buf, "Could not open or create file answer-hw3.txt"));


    // --- print full report to console and file, or console only ---
    
    print(buf, fptr, len = sprintf(buf, "\nReport created on %d/%d/%d at %02d:%02d:%02d\n",
          tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec));
    print(buf, fptr, sprintf(buf, "%.*s\n\n", len - 1, line));

    len = 0; // single dash line (---)
    while (len < 99) line[len++] = '-';

    print(buf, fptr, len = sprintf(buf, "File information:\n"));
    print(buf, fptr, sprintf(buf, "%.*s\n", len - 1, line));
    print(buf, fptr, sprintf(buf, "file name: %s\n", file->name));
    print(buf, fptr, sprintf(buf, "file size: %ld bytes\n", file->size));
    print(buf, fptr, sprintf(buf, "content:   %ld ints\n", file->intsTot + 1));
    print(buf, fptr, sprintf(buf, "first int: %ld\n\n", file->intsClaim));

    print(buf, fptr, len = sprintf(buf, "General statistics:\n"));
    print(buf, fptr, sprintf(buf, "%.*s\n", len - 1, line));
    print(buf, fptr, sprintf(buf, "count:    %*ld\n", fmtSize, file->intsClaim));
    print(buf, fptr, sprintf(buf, "minimum:  %*ld\n", fmtSize, file->minInt));
    print(buf, fptr, sprintf(buf, "maximum:  %*ld\n", fmtSize, file->maxInt));
    print(buf, fptr, sprintf(buf, "range:    %*ld\n", fmtSize, file->maxInt - file->minInt));
    print(buf, fptr, sprintf(buf, "sum:      %*ld\n", fmtSize, file->sumInts));
    if (file->intsClaim & 1) // uneven number of ints - middle one
        print(buf, fptr, sprintf(buf, "median:   %*ld\n", fmtSize, file->median1));
    else // even number of ints - average of two middle ones
        print(buf, fptr, sprintf(buf, "median:   %*.1lf\n", fmtSize, (file->median1 + file->median2) / 2.0));
    print(buf, fptr, sprintf(buf, "average:  %*.3lf\n", fmtSize, file->average));
    print(buf, fptr, sprintf(buf, "pstd dev: %*.3lf\n", fmtSize, file->pstdDev));
    print(buf, fptr, sprintf(buf, "std dev:  %*.3lf\n\n", fmtSize, file->stdDev));

    printf("\nReport was%s saved to answer-hw3.txt\n\n", BOTH ? "" : " not");

    if (BOTH) fclose(fptr); // close file
}


// output buffer to console, and possibly to file
void print(char *buf, FILE* fptr, short tmp)
{
    printf("%s", buf);
    if (BOTH) fprintf(fptr, "%s", buf);
}


// comparison function required by qsort
int cmpfunc (const void *a, const void *b)
{
    // sorting needed for median
    return (*(long int*) a - *(long int*) b);
}


// print error message, clean-up and exit
void errExit(char *msg, FILE* fptr, long int* arr, short tmp)
{
    printf("\nError: %s - exiting...\n\n", msg); // display error message

    if (fptr) // close file before exiting
        fclose(fptr);
    
    if (arr) // free allocated array memory before exiting - overkill
        free(arr);
    
    exit(1); // abrupt abort/exit of program
}
