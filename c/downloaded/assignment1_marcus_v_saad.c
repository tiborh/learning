/*
Author: Marcus V. Saad
Date: 02/16/2020
Program: print date and print next date

Input: date as a struct (month and day)
Output: next date as a struct (month and day)
*/

// libraries needed
#include <stdio.h>

typedef enum month{jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec} month;
typedef struct date{ month m; int d;} date;

// function to print name of month (enumerated type)
void print_month(date d){
    switch(d.m){
    case jan:
        printf("January");
        break;
    case feb:
        printf("February");
        break;
    case mar:
        printf("March");
        break;
    case apr:
        printf("April");
        break;
    case may:
        printf("May");
        break;
    case jun:
        printf("June");
        break;
    case jul:
        printf("July");
        break;
    case aug:
        printf("August");
        break;
    case sep:
        printf("September");
        break;
    case oct:
        printf("October");
        break;
    case nov:
        printf("November");
        break;
    case dec:
        printf("December");
        break;
    default:
        printf("Invalid month");
    }
}

// Function to print the date: "name_of_month day"
void printdate(date d){
    // print the month name as string
    print_month(d);
    // print the day as decimal
    printf(" %d\n\n", d.d);
}

// Function next_date
date nextdate(date d){
    date next_day; // define next_day as struct

    // condition for 28 of february
    if (d.d == 28 && d.m == feb){
        next_day.d = 1;
        next_day.m = mar;
    // months with 30 days
    } else if (d.d == 30 && (d.m == apr || d.m == jun || d.m == sep || d.m == nov)){
        next_day.d = 1;
        next_day.m = (d.m + 1) % 12;
    // months with 31 days
    } else if (d.d == 31 && (d.m == jan || d.m == mar || d.m == may || d.m == jul ||
                             d.m == aug || d.m == oct || d.m == dec)) {
        next_day.d = 1;
        next_day.m = (d.m + 1) % 12;
    // default condition
    } else {
        next_day.d = d.d + 1;
        next_day.m = d.m;
    }
    return next_day;
}

int main() {
    // creating dates to test as assignment instruction
    date d1, d2, d3, d4, d5;

    puts("date 1: 1 of January\nnext date: ");
    // next date: 2 of January
    d1.d = 1;
    d1.m = jan;
    printdate(nextdate(d1));

    puts("date 2: 28 of February\nnext date: ");

    // next date: 1 of March
    d2.d = 28;
    d2.m = feb;
    printdate(nextdate(d2));

    puts("date 3: 14 of March\nnext date: ");
    // next date: 15 of March
    d3.d = 14;
    d3.m = mar;
    printdate(nextdate(d3));

    puts("date 4: 31 of October\nnext date: ");
    // next date: 1 of November
    d4.d = 31;
    d4.m = oct;
    printdate(nextdate(d4));

    puts("date 5: 31 of December\nnext date: ");
    // next date: 1 of January
    d5.d = 31;
    d5.m = dec;
    printdate(nextdate(d5));

    return 0;
}
