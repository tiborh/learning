/*
Peer-graded Assignment: Honors Assignment 1
Coursera C for Everyone: Structured Programming (week 1)

wjs - 2019/11/28 - project started, code as per assignment
wjs - 2019/12/16 - added straights, flushes and straight flushes
wjs - 2019/12/17 - cleanup, code refactoring and more commenting

Straights, flushes and straight flushes have to be evaluated
anyhow in order to get correct statistics, otherwise the
probabilities for the required hands are skewed. Hence, the
code also prints the non-required hand probabilities. 

Change "#define HANDSIZE 7" to "#define HANDSIZE 5" for a 5-card evaluation

Here two different 1e9 runs (compiled with gcc -Ofast):


Monte Carlo 1000.0 million run on 7-card poker hands:

Hand                 Prob. %     Calc. %      Delta
----------------     -------     -------     -------
Royal Flush           0.0032      0.0032      0.0000
Straight Flush        0.0279      0.0279      0.0000
Four of a Kind        0.1681      0.1683     -0.0002
Full House            2.5961      2.5966     -0.0005
Flush                 3.0255      3.0249      0.0006
Straight              4.6194      4.6199     -0.0005
Three of a Kind       4.8299      4.8306     -0.0007
Two Pairs            23.4955     23.4949      0.0006
One Pair             43.8225     43.8222      0.0003
Ace High or less     17.4119     17.4115      0.0004

real    3m35,360s
user    3m35,247s
sys     0m0,000s


Monte Carlo 1000.0 million run on 5-card poker hands:

Hand                 Prob. %     Calc. %      Delta
----------------     -------     -------     -------
Royal Flush           0.0002      0.0002      0.0000
Straight Flush        0.0014      0.0014      0.0000
Four of a Kind        0.0240      0.0241     -0.0001
Full House            0.1441      0.1441     -0.0000
Flush                 0.1965      0.1965      0.0000
Straight              0.3925      0.3923      0.0002
Three of a Kind       2.1128      2.1124      0.0004
Two Pairs             4.7539      4.7536      0.0003
One Pair             42.2569     42.2568      0.0001
Ace High or less     50.1177     50.1186     -0.0009

real    2m12,261s
user    2m12,248s
sys     0m0,000s
*/

#include <stdio.h>      // for printf()
#include <time.h>       // for random seeding
#include <stdlib.h>     // for random


#define HANDSIZE 7      // number of cards in a hand - allows simulating 5 and 7 card poker hands only


enum statsIdx {royalFlush, straightFlush, fourKind, fullHouse, flush, straight, threeKind, twoPair, onePair, noPair}; // 0 to 9
enum cardSuit {club, diamond, heart, spade, blank}; // 0 to 4 - blank used for rank only searches

typedef enum cardSuit cardSuit;

typedef struct
{
    cardSuit suit; // 0 to 3 - club, diamond, heart, spade
    unsigned short rank; // 1 (ace) to 13 (king)
} card;


// function prototypes (declarations)
void initDeck(card*); // assign card deck with valid cards 
void shuffleDeck(card*, unsigned short); // shuffle card deck
void dealHand(card*, card*, unsigned short); // deal hand by copying nbr random cards from deck to hand
void sortHand(card*, unsigned short); // bubble sort hand by rank - fast enough as hand has few cards
unsigned short cardInHand(card*, unsigned short, unsigned short); // return 1 if card {suit, rank} in hand, else 0
unsigned short eleInArray(unsigned short, unsigned short*, unsigned short); // return 1 if element in array, else 0
unsigned short evaluateHand(card*); // evaluate hand and return highest possible score


int main(void)
{
    card deck[52]; // holds full 52-card deck
    card hand[HANDSIZE]; // holds HANDSIZE-card hand (5 or 7)
    unsigned int stats[10] = {0}; // buckets for counting evaluated hands - 0: royal flush, ..., 9: no pair
    unsigned int mcLoops = 2500000; // monte carlo loop amount - start per assignment with at least 1e6 loops
    
    char lbl[10][20] = {"Royal Flush", "Straight Flush", "Four of a Kind", "Full House", "Flush",
                        "Straight", "Three of a Kind", "Two Pairs", "One Pair", "Ace High or less"};

    float prob[3][10] = {{0}, {0.0032, 0.0279, 0.1681, 2.5961, 3.0255, 4.6194, 4.8299, 23.4955, 43.8225, 17.4119},  // 7 card probs (wiki)
                              {0.0002, 0.0014, 0.0240, 0.1441, 0.1965, 0.3925, 2.1128,  4.7539, 42.2569, 50.1177}}; // 5 card probs (wiki)

    srand(time(NULL)); // seed random generator once
    initDeck(deck); // populate deck with cards
    shuffleDeck(deck, 52);  // not needed but required by assignment

    for (unsigned int i = 0; i < mcLoops; i++)  // monte carlo stats/probabilities loop
    {
        shuffleDeck(deck, HANDSIZE); // randomize HANDSIZE first cards of deck
        dealHand(hand, deck, HANDSIZE); // deal hand by copying HANDSIZE random cards from deck
        stats[evaluateHand(hand)]++;
    }

    for (short i = royalFlush; i <= noPair; i++) // precalculate monte carlo results
        prob[0][i] = 100.0 * stats[i] / mcLoops;

    // print results table
    printf("\nMonte Carlo %.1f million run on %d-card poker hands:\n\n", mcLoops / 1000000.0, HANDSIZE);
    printf("Hand                 Prob. %%     Calc. %%      Delta\n");
    printf("----------------     -------     -------     -------\n");
    
    for (short i = royalFlush; i <= noPair; i++)
        printf("%-16s     %7.4lf     %7.4lf     %7.4lf\n", lbl[i], prob[HANDSIZE == 7 ? 1 : 2][i], prob[0][i], prob[HANDSIZE == 7 ? 1 : 2][i] - prob[0][i]);
    
    printf("\n"); // end of table cosmetics

    return 0; // all OK
}


// assign card deck with valid cards 
void initDeck(card *deck)
{
    unsigned int idx = 0; // index to card deck

    for (cardSuit suit = club; suit <= spade; suit++)
        for (unsigned short rank = 1; rank <= 13; rank++)
            *(deck + idx++) = (card) {suit, rank};
}


// shuffle card deck  
void shuffleDeck(card *deck, unsigned short nbr)
{
    unsigned short rdp; // random deck pointer

    for (unsigned short idx = 0; idx < nbr; idx++)
    {
        rdp = rand() % 52; // get new random deck pointer
        card tmp = *(deck + idx); // swap card
        *(deck + idx) = *(deck + rdp);
        *(deck + rdp) = tmp;
    }
}


// deal hand by copying nbr random cards from deck to hand
void dealHand(card *hand, card *deck, unsigned short nbr)
{
    for (unsigned short idx = 0; idx < nbr; idx++)
        *(hand + idx) = *(deck + idx);
}


// bubble sort hand by rank - fast enough as hand has few cards
void sortHand(card *hand, unsigned short nbr)
{
    unsigned short sorted; // flag to check if hand is sorted
    card tmp; // temporary storage used for card swapping

    while (nbr > 1) // nbr holds number of possibly unsorted cards
    {
        sorted = 1; // presume hand is sorted 

        // loop through the remaining unsorted cards
        for (unsigned int idx = 0; idx < nbr - 1; idx++)
        {
            // swap data if card rank > next card rank
            if ((hand + idx)->rank > (hand + idx + 1)->rank)
            {
                tmp = hand[idx];
                hand[idx] = hand[idx + 1];
                hand[idx + 1] = tmp;
                sorted = 0; // flag hand as modified (unsorted)
            }
        }
        if (sorted) break; // early loop exit if hand is sorted
        nbr--; // decrement unsorted number of cards
    }
}


// return 1 if card with rank and suit in hand, else 0 - blank means any suit
unsigned short cardInHand(card *hand, unsigned short rank, unsigned short clr)
{
    for (unsigned short idx = 0; idx < HANDSIZE; idx++)
        if ((hand + idx)->rank == rank && (clr == blank || (hand + idx)->suit == clr)) return 1;
    return 0;
}


// return 1 if unsigned short in unsigned short array, else 0
unsigned short eleInArray(unsigned short ele, unsigned short *array, unsigned short size)
{
    for (unsigned short idx = 0; idx < size; idx++)
        if (array[idx] == ele) return 1;
    return 0;
}


// evaluate hand and return highest score
unsigned short evaluateHand(card *hand)
{
    unsigned short rankBuckets[14] = {0}; // dummy, 1, 2, ..., 13
    cardSuit suitBuckets[4] = {0}; //  club, diamond, heart, spade
    unsigned short resultRanks[5] = {0}; // x: number of x rank cards in hand (0 to 4)
    unsigned short resultFlSt[3] = {15, 15, 15}; // idx 0: straightFlush, 1: flush, 2: straight
    unsigned short uniqueRanks[HANDSIZE + 1] = {0}; // used for straight verification
    unsigned short straights[5] = {0}; // holds lowest straight cards (several straights possible) 
    unsigned short j; // for various temporary uses


    // ----- Preprocess hand -----

    sortHand(hand, HANDSIZE); // sort hand by rank

    for (unsigned short idx = 0; idx < HANDSIZE; idx++) // populate ranks and suits buckets
    {
        rankBuckets[(hand + idx)->rank]++;
        suitBuckets[(hand + idx)->suit]++;
    }

    for (unsigned short idx = 0; idx < 14; idx++) // populate rank results (pair, full house, ...)
        resultRanks[rankBuckets[idx]]++;

    j = 0; // serves as index to uniqueRanks
    for (unsigned short idx = 0; idx < HANDSIZE; idx++) // populate uniqueRanks array
    {
        if (eleInArray((hand + idx)->rank, uniqueRanks, HANDSIZE + 1)) continue;
        uniqueRanks[j++] = (hand + idx)->rank;
    }

    if (uniqueRanks[0] == 1) uniqueRanks[j] = 14; // add ace at end of list if ace present


    // ----- Evaluate straights, flushes and straight flushes

    for (unsigned short idx = 0; idx < 4; idx++) // check for flush
        if (suitBuckets[idx] > 4)
        {
            resultFlSt[1] = idx; // holds suit of flush, else 15
            break;
        }

    j = 0; // serves as index to straights
    for (short idx = HANDSIZE - 4; idx > -1; idx--) // check for straights from highest to lowest
    {
        unsigned short flag = 1; // 1 indicates we have a straight
        for (unsigned short i = 1; i < 5; i++)
        {
            if (uniqueRanks[idx] + i != uniqueRanks[idx + i])
            {
                flag = 0; // no straight found
                break; // check next possible straight
            }
        }
        if (flag) // bingo - store lowest card and continue search
            straights[j++] = resultFlSt[2] = uniqueRanks[idx]; // lowest card of straight - max 10 -> 10, j, q, k, a
    }

    if (resultFlSt[1] < 15 && resultFlSt[2] < 15) // check straight flush, only if flush and straight in hand
    {
        unsigned short clr = resultFlSt[1]; // holds suit - can only be one in hands of 5 and 7 cards
        
        for (unsigned short idx = 0; idx < 5; idx++)
        {
            j = 1; // serves as flag - 1 indicates we have a straight flush

            if (straights[idx] == 0) break; // no straights left to check, exit

            // check all straights found for same suit
            for (unsigned short rnk = straights[idx]; rnk < straights[idx] + 5; rnk++)
            {
                if (!cardInHand(hand, rnk == 14 ? 1 : rnk, clr))  // cope with possible ace as 14
                {
                    j = 0; // no straight flush found
                    break; // check next possible straight
                }
            }
            if (j) // bingo, and it will be the highest straight flush, so stop further search
            {
                resultFlSt[0] = straights[idx];
                break;
            }
        }
    }

    // do NOT change order
    if (resultFlSt[0] == 10) return royalFlush;
    if (resultFlSt[0] < 10) return straightFlush;
    if (resultRanks[4]) return fourKind;
    if ((resultRanks[3] && resultRanks[2]) || resultRanks[3] > 1) return fullHouse;
    if (resultFlSt[1] < 15) return flush;
    if (resultFlSt[2] < 15) return straight;
    if (resultRanks[3]) return threeKind;
    if (resultRanks[2] > 1) return twoPair;
    if (resultRanks[2]) return onePair;
    return noPair;
}
