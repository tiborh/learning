#include<stdio.h>

typedef enum month{ jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec} month;
typedef struct date{ month m; int d;} date;

const int daysOfMonths[] = {31,28,31,30,31,30,31,31,30,31,30,31};
const char months[][10] = {  "January\0",
                             "February\0",
                             "March\0",
                             "April\0",
                             "May\0",
                             "June\0",
                             "July\0",
                             "August\0",
                             "September\0", 
                             "October\0",
                             "November\0",
                             "December\0",};
 
// Funtion to check if date is valid
int validate_date(date mydate)
{
    if(mydate.d > daysOfMonths[mydate.m])
    {
        printf("Invalid Date. Day month for %s can only be between 1-%d",months[mydate.m],daysOfMonths[mydate.m]);
        return -1;
    }
    else
    {
        return 1;
    }
}

// Funtion to print the date
void print_date(date mydate)
{
    if(validate_date(mydate) != -1)
        printf("%s %d\n",months[mydate.m],mydate.d);
}
  
// Function to return the next day
date next_date(date mydate)
{ 
    if(mydate.d >= daysOfMonths[mydate.m])
    {
        mydate.d = 1;
        mydate.m = (mydate.m + 1) % 12;
    }
    else
    {
        mydate.d++;
    }
    return mydate;
}

date mydate;

int main()
{
    // ---------------------------    For February 28
    mydate.m = feb;
    mydate.d = 28;
    
    printf("\n\nToday - ");
    print_date(mydate);

    mydate = next_date(mydate);

    printf("Tomorrow - ");
    print_date(mydate);

    // ---------------------------    For March 14
    mydate.m = mar;
    mydate.d = 14;

    printf("\n\nToday - ");
    print_date(mydate);

    mydate = next_date(mydate);

    printf("Tomorrow - ");
    print_date(mydate);


    // ---------------------------    For October 31
    mydate.m = oct;
    mydate.d = 31;

    printf("\n\nToday - ");
    print_date(mydate);

    mydate = next_date(mydate);

    printf("Tomorrow - ");
    print_date(mydate);


    // ---------------------------    For December 31.
    mydate.m = dec;
    mydate.d = 31;

    printf("\n\nToday - ");
    print_date(mydate);

    mydate = next_date(mydate);

    printf("Tomorrow - ");
    print_date(mydate);
    
    return 0;
}
