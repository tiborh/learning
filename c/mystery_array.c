#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void myst1(void) {
  int a[10] = {1,2,3,4,5,6,7,8,9,10}, i = 6 ;
  int* p = &a[0];
  printf("*(p+i): %d\n", *(p + i));
}

void myst2(void) {
  char a[5] = "abcd";
  char* str = &a[0];
  printf("*str: %c\n",*str);
}


int main (int argc, char** argv) {
  myst1();
  myst2();

  return 0;
}
