#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  {
    char a = 'a';
    char b = 'b';
    char* pch = &a;
    printf("%c\n",*pch);
    *pch = 'c';			/* you can change value */
    printf("%c\n",*pch);
    a = 'a';			/* value of pointed changes */
    printf("%c\n",*pch);
    pch = &b;			/* you can change where it points */
    printf("%c\n",*pch);
    puts("---");
  }
  {
    char a = 'a';
    char b = 'b';
    char const* pch = &a;       /* recommended in Modern C */
    printf("%c\n",*pch);
    //*pch = 'c';			/* you cannot change value */
    /* error: assignment of read-only location ‘*pch’ */
    // printf("%c\n",*pch);
    a = 'c';			/* value of pointed changes */
    printf("%c\n",*pch);
    pch = &b;			/* you can change where it points */
    printf("%c\n",*pch);
    puts("---");
  }
  {
    char a = 'a';
    char b = 'b';
    const char* pch = &a;        /* not recommended in Modern C */
    printf("%c\n",*pch);
    //*pch = 'c';			/* you cannot change value */
    /* error: assignment of read-only location ‘*pch’ */
    // printf("%c\n",*pch);
    a = 'c';			/* value of pointed changes */
    printf("%c\n",*pch);
    pch = &b;			/* you can change where it points */
    printf("%c\n",*pch);
    puts("---");
  }
  {
    char a = 'a';
    // char b = 'b';
    char* const pch = &a;	/* recommended in Modern C */
    printf("%c\n",*pch);
    *pch = 'c';			/* you can change value */
    printf("%c\n",*pch);
    a = 'a';			/* value of pointed changes */
    printf("%c\n",*pch);
    // pch = &b;			/* you cannot change where it points */
    /* error: assignment of read-only variable ‘pch’ */
    // printf("%c\n",*pch);
    puts("---");
  }
  {
    char a = 'a';
    // char b = 'b';
    char const* const pch = &a;	/* recommended in Modern C, also: const*const  */
    printf("%c\n",*pch);
    // *pch = 'c';			/* you cannot change value */
    // printf("%c\n",*pch);
    a = 'c';			/* value of pointed changes */
    printf("%c\n",*pch);
    // pch = &b;			/* you cannot change where it points */
    // printf("%c\n",*pch);
    puts("---");
  }
  puts("Look into the source, the output is not especially enlightening.");
  
  return 0;
}
