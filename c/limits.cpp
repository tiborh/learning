#include <iostream>
using namespace std;

template <class T>
void print_limits(string type_txt) {
  T n0 = 0;
  T n1 = 1;
  for (; n0 < n1; n0++, n1++);
  cout << "The maxval of " << type_txt << " is: " << static_cast<long long int>(n0) << endl;
  n0 = 1;
  n1 = 0;
  for (; n0 > n1; n0--, n1--);
  cout << "The minval of " << type_txt << " is: " << static_cast<long long int>(n0) << endl;
}

int main (int argc, char** argv) {
  print_limits<char> ("char");
  print_limits<signed char> ("signed char");
  print_limits<short int> ("short int");
  print_limits<int> ("int");
  print_limits<long int> ("long int"); /* takes too long */
  // print_limits<long long int> ("long long int"); /* takes too long */
}
