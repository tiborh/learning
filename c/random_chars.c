#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

#define NUM_OF_CHARS 1000
#define DEFAULT_UNIT 'b'
#define BYTE_MULTIP 1024

void char_printer(long long unsigned n) {
  srand(time(NULL));
  for (long long unsigned i = 0; i < n; ++i)
    printf("%c",rand() % UCHAR_MAX + 1); /* using the max value of unsigned chars */
  //putchar('\n');
}

int main (int argc, char** argv) {

  char* endptr;
  int base = 10;
  long long unsigned n = (argc > 1) ? strtoull(argv[1],&endptr,base) : NUM_OF_CHARS;
  long long unsigned m = 1;
  char u = (argc > 2) ? argv[2][0] : DEFAULT_UNIT;
  switch (u) {
  case 'b':
  case 'B':
    break;
  case 'g':
  case 'G':
    m *= BYTE_MULTIP;
  case 'm':
  case 'M':
    m *= BYTE_MULTIP;
  case 'k':
  case 'K':
    m *= BYTE_MULTIP;
    break;
  default:
    printf("unimplemented unit: '%c'\n",u);
  }

  char_printer(n*m);
  
  return 0;
}
