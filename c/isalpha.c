#include <stdio.h>
#include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  for (int i = 1; i < argc; ++i) {
    int length = strlen(argv[i]);
    for (int j = 0; j < length; ++j) {
      if(isalpha(argv[i][j]))
	printf("alphabetc:\t");
      else
	printf("not alphabetic:\t0x%x\t",argv[i][j]);
      putchar(argv[i][j]);
      putchar('\n');
    }
    putchar('\n');
  }
 
  return 0;
}
