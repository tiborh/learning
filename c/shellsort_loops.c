#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX

int* gennums(unsigned num_of_nums, unsigned upper_bound) {
  int* outarr = calloc(num_of_nums,sizeof(int));
  for(unsigned i = 0; i < num_of_nums; ++i)
    *(outarr+i) = rand() % upper_bound;
  return outarr;
}

void printnums(unsigned len, int* nums) {
  putchar('\t');
  for(unsigned i = 0; i < len; ++i)
    printf("%d ",*(nums+i));
  putchar('\n');
}

void shellsort(unsigned alen, int arr[]) {
  for (int gap = alen/2; gap > 0; gap /= 2)
    for (int i = gap; i < alen; ++i)
      //      for (int j = i-gap; j >= 0 && arr[j] > arr[j+gap]; j-=gap) {
      for (int j = i-gap; j >= 0; j-=gap) {
	printf("gap: %d, i: %d, j: %d\n",gap,i,j);
	if (arr[j] > arr[j+gap]) {
	  printf("\tswapping %d (elem %d) and %d (elem %d)\n",arr[j],j,arr[j+gap],j+gap);
	  int temp = arr[j];
	  arr[j] = arr[j+gap];
	  arr[j+gap] = temp;
	  printnums(alen,arr);
	} else
	  printf("\tno change\n");
      }
}

int main (int argc, char** argv) {
  unsigned num_of_nums = (argc > 1) ? atoi(argv[1]) : NUM_OF_NUMS;
  unsigned upper_bound = (argc > 2) ? atoi(argv[2]) : UPPER_BOUND;
  srand(time(NULL));
  int* nums = gennums(num_of_nums,upper_bound);
  printnums(num_of_nums,nums);
  shellsort(num_of_nums,nums);
  //printnums(num_of_nums,nums);
  free(nums);
  return 0;
}
