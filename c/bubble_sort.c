#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void swap(int* n1, int* n2) {
  int temp = *n1;
  *n1 = *n2;
  *n2 = temp;
  return;
}

void fill_array(int alength, int* a, int rand_ceiling) {
  srand(time(NULL));
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

void bubble_sort(int alength, int* a) {
  bool go_on = true;
  bool swap_happened = false;
  int sort_width = alength;
  int inner_loop_counter = 0;
  int outer_loop_counter = 0;
  printf("Sorting:\n");
  while(go_on) {
    ++outer_loop_counter;
    for(int i = 1; i < sort_width; ++i) {
      ++inner_loop_counter;
      if (*(a + i - 1) > *(a + i)) {
	swap((a + i - 1),(a + i));
	swap_happened = true;
      }
    }
    --sort_width;
    putchar('\t');
    print_array(alength,a);
    if (swap_happened)
      swap_happened = false;
    else
      go_on = false;
  }
  printf("outer loop counter: %d\n",outer_loop_counter);
  printf("inner loop counter: %d\n",inner_loop_counter);
}

int main (int argc, char** argv) {
  int array_length = 10;
  int value_ceiling = 100;
  if (argc == 1)
    printf("%s [length (%d)] [ceiling (%d)]\n",basename(argv[0]),array_length,value_ceiling);
  if (argc > 1)
    sscanf(argv[1],"%d",&array_length);
  assert(array_length > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&value_ceiling);
  assert(value_ceiling > 1);
  int* arr = calloc(array_length,sizeof(int));

  fill_array(array_length,arr,value_ceiling);
  printf("Generated:\n\t");
  print_array(array_length,arr);
  bubble_sort(array_length,arr);
  printf("Sorted:\n\t");
  print_array(array_length,arr);
  
  free(arr);
  return 0;
}
