#include <stdio.h>
#include <ctype.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {

  int blanks = 0, digits = 0, letters = 0, punctuation=0, control=0, others = 0, graph = 0, other_printable=0, all=0;
  int newlines = 0;
  int c; 			/* for storing characters */
  while((c = getchar()) != EOF) {
    ++all;
    if (c == '\n')
      ++newlines;
    if (isalpha(c))
      ++letters;
    else if (isspace(c))
      ++blanks;
    else if (isdigit(c))
      ++digits;
    else if (ispunct(c))
      ++punctuation;
    else if (iscntrl(c))
      ++control;
    else if (isgraph(c))
      ++graph;
    else if (isprint(c))
      ++other_printable;
    else {
      ++others;
      putchar(c);
      printf("(%x)",c);
    }
  };
  putchar(10);

  printf("space chars == %d\n",blanks);
  printf("letters == %d\n",letters);
  printf("digits == %d\n",digits);
  printf("punctuation == %d\n",punctuation);
  printf("control == %d\n",control);
  printf("graphical == %d\n",graph);
  printf("other printable == %d\n",other_printable);
  printf("other == %d\n",others);
  printf("all == %d\n", all);
  printf("newlines == %d\n",newlines);
  return 0;
}
