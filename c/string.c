#include <stdio.h>
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void array_printer(int size, char* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",(int)a[i]);
  putchar('\n');
}

int main (int argc, char** argv) {
  char str[] = "a b c";
  printf("str: %s\n",str);
  printf("length: %lu\n",strlen(str));
  printf("in array printer:\n\t");
  array_printer(strlen(str)+1,str);
  return 0;
}
