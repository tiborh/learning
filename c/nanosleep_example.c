#include "msleep.h"

#define BASE 10
#define SDIV 1000
#define LONGTIME 5 * SDIV

int main(int argc, char** argv) {
  /* unsigned long strtoul(const char *nptr, char **endptr, int base) */
  const unsigned long default_time = 2500;
  char** endptr = argv;
  unsigned sleeptime = argc < 2 ? default_time : strtoul(argv[1],endptr,BASE);
  if (sleeptime > LONGTIME)
    printf("Sleeping for %3.1f seconds...\n",(double)sleeptime/SDIV);
  int ret = msleep(sleeptime);
  printf("sleep result: %d\n",ret);
  printf("sleep time:   %u ms\n",sleeptime);
  return EXIT_SUCCESS;
}
