#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h> 		/* atol */
#include <unistd.h>		/* for usleep */

#define BASE 10
#define SDIV 1000000
#define LONGTIME 5 * SDIV


int main(int argc, char** argv) {
  /* unsigned long strtoul(const char *nptr, char **endptr, int base) */
  const unsigned long default_time = 2500;
  char** endptr = argv;
  unsigned sleeptime = argc < 2 ? default_time : strtoul(argv[1],endptr,BASE);
  if (sleeptime > LONGTIME)
    printf("Sleeping for %u seconds...\n",sleeptime/SDIV);
  int ret = usleep(sleeptime);
  printf("sleep result: %d\n",ret);
  printf("sleep time:   %u µs (%f second(s))\n",sleeptime,(double)sleeptime/SDIV);
  return EXIT_SUCCESS;
}
