#ifndef BITREE_H_INCLUDED
#define BITREE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

// data
typedef void (*destr_fun) (void* data);
typedef long (*comp_fun)(const void* k0, const void* k1);
typedef enum Errors_ {NoProblem=0,
                      NoTree=-1,
                      NoNode=-2,
                      NoData=-3,
                      RootOccupied=-4,
                      LeftOccupied=-5,
                      RightOccupied=-6,
                      ChildrenOccupied=-7,
                      NoMemory=-8,
                      NoRoot=-9,
                      DuplData=-10
} Errors;
typedef void(*DataPrint)(FILE*,const void*);

typedef struct BiTreeNode_ {
    void* data;
    struct BiTreeNode_* left;
    struct BiTreeNode_* right;
} BiTreeNode;

typedef struct BiTree_ {
    unsigned size;

    destr_fun destr;
    comp_fun comp;

    BiTreeNode* root;
} BiTree;

// setters
Errors init_bitree(BiTree*, destr_fun, comp_fun);
Errors destr_subtree(BiTree*, BiTreeNode*);
Errors destr_bitree(BiTree*);
BiTreeNode* create_new_node(void*);
Errors ins_left(BiTree*, BiTreeNode*, void*);
Errors ins_right(BiTree*, BiTreeNode*, void*);
Errors rem_left(BiTree*,BiTreeNode*);
Errors rem_right(BiTree*,BiTreeNode*);
int merge_trees(BiTree*,BiTree*,BiTree*,void*);

// getter
void* get_node_data(const BiTreeNode*);
BiTreeNode* get_left_node(const BiTreeNode*);
BiTreeNode* get_right_node(const BiTreeNode*);
bool is_leaf_node(const BiTreeNode*);
bool is_end_of_branch(const BiTreeNode*);
BiTreeNode* get_tree_root(const BiTree*);
unsigned get_tree_size(const BiTree*);
bool is_empty_tree(const BiTree*);

// utility
void error_report (char*, Errors);
void print_tree(FILE*,const BiTreeNode*,DataPrint);
void print_tree_to_dot_file(FILE*,const BiTree*,DataPrint);


#endif // BITREE_H_INCLUDED
