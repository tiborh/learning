#include "bitree.h"

void error_report (char* funcname, Errors errorcode) {
    fprintf(stderr,"%s: ",funcname);
    switch(errorcode) {
    case NoTree:
        fprintf(stderr,"tree is NULL.");
        break;
    case NoNode:
        fprintf(stderr,"node is NULL.");
        break;
    case NoData:
        fprintf(stderr,"data is NULL.");
        break;
    case RootOccupied:
        fprintf(stderr,"Root node space is already taken.");
        break;
    case LeftOccupied:
        fprintf(stderr,"left node space is already taken.");
        break;
    case RightOccupied:
        fprintf(stderr,"right node space is already taken.");
        break;
    case ChildrenOccupied:
        fprintf(stderr,"both child node's space is already taken.");
        break;
    case NoMemory:
        fprintf(stderr,"cannot allocate space for new node.");
        break;
    case NoRoot:
        fprintf(stderr,"root node is NULL.");
        break;
    case DuplData:
        fprintf(stderr,"data is already in the tree, duplicates are not allowed.");
        break;
    case NoProblem:
        fprintf(stderr,"No Problem.");
        break;

    default:
        fprintf(stderr,"unknown error: %d.",errorcode);
        break;
    }
    fputc('\n',stderr);
}

Errors init_bitree(BiTree* tree, destr_fun dfun, comp_fun cfun) {
    if (!tree)
        return NoTree;
    tree->size = 0;
    tree->destr = dfun;
    tree->comp = cfun;
    tree->root = NULL;
    return NoProblem;
}

void* get_node_data(const BiTreeNode* node) {
    return node->data;
}
BiTreeNode* get_left_node(const BiTreeNode* node) {
    return node->left;
}
BiTreeNode* get_right_node(const BiTreeNode* node) {
    return node->right;
}
bool is_leaf_node(const BiTreeNode* node) {
    return(!node->left && !node->right);
}
bool is_end_of_branch(const BiTreeNode* node) {
    return !node;
}
BiTreeNode* get_tree_root(const BiTree* tree) {
    return tree->root;
}
unsigned get_tree_size(const BiTree* tree) {
    return tree->size;
}

bool is_empty_tree(const BiTree* tree) {
    return !tree->root;
}

Errors destr_subtree(BiTree* tree, BiTreeNode* node) {
    if (!tree)
        return NoTree;
    if (!node)
        return NoNode;
    if (node->left) {
        destr_subtree(tree,node->left);
        node->left = NULL;
    }
    if (node->right) {
        destr_subtree(tree,node->right);
        node->right = NULL;
    }
    if (tree->destr && node->data)
        tree->destr(node->data);
    --tree->size;
    free(node);
    return NoProblem;
}

Errors rem_left(BiTree* tree,BiTreeNode* node) {
    if (!tree)
        return NoTree;
    if (!node)
        return NoNode;
    Errors retval = NoProblem;
    if (node->left) {
        retval = destr_subtree(tree,node->left);
        node->left = NULL;
    }
    return retval;
}

Errors rem_right(BiTree* tree,BiTreeNode* node) {
    if (!tree)
        return NoTree;
    if (!node)
        return NoNode;
    Errors retval = NoProblem;
    if (node->right) {
        retval = destr_subtree(tree,node->right);
        node->right = NULL;
    }
    return retval;
}

Errors destr_bitree(BiTree* tree) {
    if (!tree)
        return NoTree;
    Errors retval = NoProblem;
    if (tree->root) {
        retval = destr_subtree(tree,tree->root);
        tree->root = NULL;
    }
    return retval;
}

BiTreeNode* create_new_node(void* data) {
    BiTreeNode* newnode = malloc(sizeof(BiTreeNode));
    if (!newnode)
        return NULL;
    newnode->data = data;
    newnode->left = NULL;
    newnode->right = NULL;
    return newnode;
}

Errors ins_left(BiTree* tree, BiTreeNode* node, void* data) {
    if(!tree)
        return NoTree;
    if(!data)
        return NoData;
    if(!node && tree->root)
        return NoNode;
    if(node && node->left)
        return LeftOccupied;

    BiTreeNode* newnode = create_new_node(data);
    if (!newnode)
        return NoMemory;

    if (!node)
        tree->root = newnode;
    else
        node->left = newnode;

    ++tree->size;

    return NoProblem;
}

Errors ins_right(BiTree* tree, BiTreeNode* node, void* data) {
    if(!tree)
        return NoTree;
    if(!data)
        return NoData;
    if(!node)
        return NoNode;
    if(node->right)
        return RightOccupied;

    BiTreeNode* newnode = create_new_node(data);
    if (!newnode)
        return NoMemory;

    node->right = newnode;

    ++tree->size;

    return NoProblem;
}

Errors merge_trees(BiTree* merged,BiTree* left, BiTree* right,void* merged_data) {
    if (!merged || !left || !right)
        return NoTree;
    if (!merged_data)
        return NoData;
    if (merged->root)
        return RootOccupied;

    BiTreeNode* newroot = create_new_node(merged_data);
    if (!newroot)
        return NoMemory;
    merged->root = newroot;
    merged->root->left = left->root;
    merged->root->right = right->root;
    merged->size = left->size + right->size + 1;

    left->root = NULL;
    left->size = 0;
    Errors res0 = destr_bitree(left);
    right->root = NULL;
    right->size = 0;
    Errors res1 = destr_bitree(right);

    if (res0 != res1) {
            fputs("merge_trees discrepancy:",stderr);
        if (res0 < 0) {
            fputs("left tree destruction:",stderr);
            error_report("merge_trees",res0);
        } else if (res1 < 0) {
            fputs("right tree destruction:",stderr);
            error_report("merge_trees",res1);
        }
        assert(res0 == res1);
    }


    return res0;
}

void print_tree(FILE* fh, const BiTreeNode* node, DataPrint dp) {
    if (!node)
        return;
    if (node->left) {
        fputc('\t',fh);
        dp(fh,node->data);
        fprintf(fh," -> ");
        dp(fh,node->left->data);
        fprintf(fh," [ label=\"L\" ];");
        fputc('\n',fh);
        print_tree(fh,node->left,dp);
    }
    if (node->right) {
        fputc('\t',fh);
        dp(fh,node->data);
        fprintf(fh," -> ");
        dp(fh,node->right->data);
        fprintf(fh," [ label=\"R\" ];");
        fputc('\n',fh);
        print_tree(fh,node->right,dp);
    }
}

void print_tree_to_dot_file(FILE* fh, const BiTree* bt, DataPrint dp) {
    fprintf(fh,"digraph g {\n");
    if (!bt->root->left && !bt->root->right) {
        fputc('\t',fh);
        dp(fh,bt->root->data);
        fputc('\n',fh);
    } else
    print_tree(fh,bt->root,dp);
    fprintf(fh,"}\n");
}
