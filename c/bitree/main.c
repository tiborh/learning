#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include "bitree.h"

#define TEST_SIZE 5
#define UPPER_BOUND UCHAR_MAX
#define PRNSTR "%u"
#define DATAMAX UINT_MAX
typedef unsigned DATA;


void print_data(FILE* fh, const void* data) {
    fprintf(fh,PRNSTR,*((DATA*)data));
}
unsigned conv_data(const void* data) {
    if (!data)
        return DATAMAX;
    return *((DATA*)data);
}

void destr_data(void* data) {
    free((DATA*)data);
}

long comp_data(const void* data0, const void* data1) {
    return((long)(*((DATA*)data0)) - (long)(*((DATA*)data1)));
}

long comp_list_elems(BiTreeNode* node0, BiTreeNode* node1) {
    return(comp_data(node0->data,node1->data));
}

void tree_writer(BiTree* bt, char* fn) {
    FILE* fh;
    if (!(fh = fopen(fn,"w"))) {
        fprintf(stderr,"Cannot open file: %s for writing\n",fn);
    } else {
        print_tree_to_dot_file(fh,bt,print_data);
        fclose(fh);
        printf("Tree has been written into: %s\n",fn);
    }
}

void simple_test(void) {
    puts("Simple test:");
    BiTree* bt0 = malloc(sizeof(BiTree));
    init_bitree(bt0,destr_data,comp_data);
    DATA* d0 = malloc(sizeof(DATA));
    *d0 = 0;
    ins_left(bt0,NULL,d0);
    DATA* d1 = malloc(sizeof(DATA));
    *d1 = 1;
    ins_left(bt0,bt0->root,d1);
    DATA* d2 = malloc(sizeof(DATA));
    *d2 = 2;
    ins_right(bt0,bt0->root,d2);
    DATA* d3 = malloc(sizeof(DATA));
    *d3 = 3;
    ins_left(bt0,bt0->root->left,d3);
    DATA* d4 = malloc(sizeof(DATA));
    *d4 = 4;
    ins_right(bt0,bt0->root->left,d4);
    print_tree_to_dot_file(stdout,bt0,print_data);
    tree_writer(bt0,"bitree_simple_test.dot");
    destr_bitree(bt0);
    free(bt0);
}

Errors find_place(BiTree* bt, BiTreeNode* treenode, BiTreeNode* newnode) {
    long comparison = bt->comp(treenode->data,newnode->data);
    if (comparison > 0) {
        if (treenode->left)
            return find_place(bt,treenode->left,newnode);
        else {
            treenode->left = newnode;
//            printf("Comparison result: %ld\n",comparison);
//            printf("connected ");
//            print_data(stdout,treenode->data);
//            printf(" -> ");
//            print_data(stdout,newnode->data);
//            puts(" as left node");
            return NoProblem;
        }
    } else if (comparison < 0) {
        if (treenode->right)
            return find_place(bt,treenode->right,newnode);
        else {
            treenode->right = newnode;
//            printf("Comparison result: %ld\n",comparison);
//            printf("connected ");
//            print_data(stdout,treenode->data);
//            printf(" -> ");
//            print_data(stdout,newnode->data);
//            puts(" as right node");
            return NoProblem;
        }
    } else {
        printf("Ignoring duplicate: ");
        print_data(stdout,newnode->data);
        putchar('\n');
        return DuplData;
    }
    return NoProblem;
}

Errors add_data(BiTree* bt, void* data) {
    if (!bt)
        return NoTree;
    if (!data)
        return NoData;
    BiTreeNode* newnode = create_new_node(data);
    if (!newnode)
        return NoMemory;
    Errors retval = NoProblem;
    if (!bt->root) {
        bt->root = newnode;
    } else {
        retval = find_place(bt,bt->root,newnode);
        if (retval == DuplData)
            free(newnode);
    }

    if (retval == NoProblem)
        ++bt->size;
    return retval;
}

Errors add_node(BiTree* bt, BiTreeNode* newnode) {
    if (!bt)
        return NoTree;
    if (!newnode)
        return NoNode;
    Errors retval = NoProblem;
    if (!bt->root) {
        bt->root = newnode;
    } else {
        retval = find_place(bt,bt->root,newnode);
        if (retval == DuplData)
            free(newnode);
    }

    if (retval == NoProblem)
        ++bt->size;
    return retval;
}

BiTree* create_new_tree(void) {
    BiTree* bt = malloc(sizeof(BiTree));
    init_bitree(bt,destr_data,comp_data);
    return bt;
}

BiTree* generate_tree(DATA size, DATA upper_bound, DATA shiftval) {
    BiTree* bt = create_new_tree();
    assert(bt->size == 0);
    unsigned tree_size = 0;
    while (bt->size < size) {
        DATA* d0;
        if (!(d0 = malloc(sizeof(DATA))))
            break;
        *d0 = shiftval + (rand() % upper_bound);
        Errors res = add_data(bt,d0);
        if (res == NoProblem) {
            assert(tree_size + 1 == bt->size);
            ++tree_size;
        }
    }

    return bt;
}

void merge_test(DATA size, DATA upper_bound) {
    char* fns[] = {"bitree_left.dot", "bitree_right.dot", "bitree_merged.dot"};
    BiTree* bt0 = generate_tree(size,upper_bound,0);
    puts("Left tree:");
    print_tree_to_dot_file(stdout,bt0,print_data);
    tree_writer(bt0,fns[0]);
    BiTree* bt1 = generate_tree(size,upper_bound,upper_bound);
    puts("Right tree:");
    print_tree_to_dot_file(stdout,bt1,print_data);
    tree_writer(bt1,fns[1]);
    BiTree* bt2 = create_new_tree();
    DATA* d0 = malloc(sizeof(DATA));
    *d0 = 2 * upper_bound;
    merge_trees(bt2,bt0,bt1,d0);
    assert(!bt0->root && !bt1->root);
    assert(!bt0->size && !bt1->size);
    puts("Merged trees:");
    print_tree_to_dot_file(stdout,bt2,print_data);
    tree_writer(bt2,fns[2]);
    free(bt0);
    free(bt1);
    destr_bitree(bt2);
    free(bt2);
}

void bst_merge(BiTreeNode* fromroot, BiTree* to) {
    if (!fromroot)
        return;
    BiTreeNode* newnode = create_new_node(fromroot->data);
    if (!newnode)
        return;
    fromroot->data = NULL;
    add_node(to,newnode);
    if (fromroot->left)
        bst_merge(fromroot->left,to);
    if (fromroot->right)
        bst_merge(fromroot->right,to);
}

Errors bst_merge_trees(BiTree* merged,BiTree* from0,BiTree* from1) {
    if (!merged || !from0 || !from1)
        return NoNode;
    bst_merge(from0->root,merged);
    bst_merge(from1->root,merged);
    destr_bitree(from0);
    destr_bitree(from1);
    return NoProblem;
}

void bst_merge_test(DATA size, DATA upper_bound) {
    char* fns[] = {"bitree1.dot", "bitree2.dot", "bitree_bst_merged.dot"};
    BiTree* bt0 = generate_tree(size,upper_bound,0);
    puts("First tree:");
    print_tree_to_dot_file(stdout,bt0,print_data);
    tree_writer(bt0,fns[0]);
    BiTree* bt1 = generate_tree(size,upper_bound,0);
    puts("Second tree:");
    print_tree_to_dot_file(stdout,bt1,print_data);
    tree_writer(bt1,fns[1]);
    BiTree* bt2 = create_new_tree();
    bst_merge_trees(bt2,bt0,bt1);
    assert(!bt0->root && !bt1->root);
    assert(!bt0->size && !bt1->size);
    free(bt0);
    free(bt1);
    puts("Merged trees:");
    print_tree_to_dot_file(stdout,bt2,print_data);
    tree_writer(bt2,fns[2]);
    destr_bitree(bt2);
    free(bt2);
}

int main(int argc, char** argv) {
    DATA size = (argc < 2) ? TEST_SIZE : atoi(argv[1]);
    DATA upper_bound = (argc < 3) ? UPPER_BOUND : atoi(argv[2]);
    srand(time(NULL));
    simple_test();
    merge_test(size, upper_bound);
    bst_merge_test(size, upper_bound);
    //puts("to be continued...");
    return 0;
}
