#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int find_first(char* str1, char* str2) {
  int len1 = strlen(str1);
  int len2 = strlen(str2);

  for(int j = 0; j < len1; ++j) {
    for(int i = 0; i < len2; ++i) {
      if (*(str1+j) == *(str2+i)) {
	return(j);
      }
    }
  }
  
  return(-1);
}

int main (int argc, char** argv) {
  assert(find_first("","") == -1);
  assert(find_first("alpha","z") == -1);
  assert(find_first("alpha","a") == 0);
  assert(find_first("alpha","h") == 3);
  assert(find_first("alpha","hpl") == 1);
  
  if (argc <  3) {
    printf("%s <string1> <string2>\n\treturns with a string1 index\n\t\twhere any characters of string2 is first found\n\tor with -1 if none such exists.\n",basename(argv[0]));
    exit(0);
  }

  int the_index = find_first(argv[1],argv[2]);
  printf("%d\n",the_index);
  if (the_index >= 0)
    printf("%c\n",*strpbrk(argv[1],argv[2]));

  return 0;
}
