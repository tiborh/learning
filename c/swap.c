#include <stdio.h>
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void swap(int* n1, int* n2) {
  int temp = *n1;
  *n1 = *n2;
  *n2 = temp;
  return;
}

void array_printer(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",a[i]);
  putchar('\n');
}

int main (int argc, char** argv) {
  int a0[] = {1,2};
  printf("Original array:\n\t");
  array_printer(2,a0);
  swap(a0,a0+1);
  printf("Swapped array:\n\t");
  array_printer(2,a0);
  return 0;
}
