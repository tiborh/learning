#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX

typedef int INDEX;
typedef unsigned DATA;

int* gennums(unsigned num_of_nums, unsigned upper_bound) {
  int* outarr = calloc(num_of_nums,sizeof(int));
  for(unsigned i = 0; i < num_of_nums; ++i)
    *(outarr+i) = rand() % upper_bound;
  return outarr;
}

void printnums(unsigned len, int* nums) {
  putchar('\t');
  for(unsigned i = 0; i < len; ++i)
    printf("%d ",*(nums+i));
  putchar('\n');
}

void swap(int* first, int* second) {
  int tmp = *first;
  *first = *second;
  *second = tmp;
}

int cmp(const int* first, const int* second) {
  return *first - *second;
}

bool is_sorted(INDEX alen, int* nums) {
  for (INDEX i = 1; i < alen; ++i)
    if (cmp(nums+(i-1),nums+i) > 0)
      return false;
  return true;
}

/* algorithm:
   First, the middle element of the list is compared with the first element.
   Then, this frame (zero to middle) is shifted right one by one.
   When the end has been reached, half the distance is used, and the loop is
     restarted
   Whenever a swap is done, and there is enough space from the first of the 
     frame to the first of the list to shift the frame left so that its right
     element becomes the left element of the original, a shift-left is made
     and a comparison, and if needed, a swap (and so on)
   After these left shifts, the frame should go on from the location of the
     first swap
   1. begin with first and middle elements (frame)
   2. if first is greater,
        a. swap
        b. shift left the frame by middle increment as long as further swaps are needed
   3. shift frame right by one
 */

void shellsort(unsigned alen, int arr[]) {
  for (int gap = alen/2; gap > 0; gap /= 2)
    for (int end = gap; end < alen; ++end)
      for (int begin = end-gap; begin >= 0 && 0 < cmp(arr+begin,arr+begin+gap); begin-=gap)
	swap(arr+begin,arr+begin+gap);
}

int main (int argc, char** argv) {
  INDEX num_of_nums = (argc > 1) ? atoi(argv[1]) : NUM_OF_NUMS;
  DATA upper_bound = (argc > 2) ? atoi(argv[2]) : UPPER_BOUND;
  srand(time(NULL));
  int* nums = gennums(num_of_nums,upper_bound);
  printnums(num_of_nums,nums);
  shellsort(num_of_nums,nums);
  printnums(num_of_nums,nums);
  assert(is_sorted(num_of_nums,nums));
  free(nums);
  return 0;
}
