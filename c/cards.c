/********************************************************************************
 * Name: cards.c
 * Author: Tibor
 * Version: 0.2
 * Date: 2019-12-23
 * 
 * Instructions:
 * -------------
 * 1. Use a struct to define a card as 
 *    a. an enumerated member that is its suit value and 
 *    b. a short that is its pips value (note: char is enough; note2: not used)
 * 2. Write a function that randomly shuffles the deck.
 * 3. Submit your work as a text file.
 * 4. Then deal out 7 card hands and evaluate the probability that a hand has 
 *    a. no pair, 
 *    b. one pair, 
 *    c. two pair, 
 *    d. three of a kind, 
 *    e. full house and 
 *    f. 4 of a kind. 
 *    This is a Monte Carlo method to get an approximation to these 
 *    probabilities.
 * 5. Use _at least 1 million_ randomly generated hands.
 * 
 * You can check against probabilities found in a standard table.
 * Seven-Card Stud
 * ==================================================
 * Hand			Combinations	Probabilities
 * --------------------------------------------------
 * Royal flush		     4324	0.00003232
 * Straight flush	    37260	0.00027851
 * Four of a kind	   224848	0.00168067
 * Full house		  3473184	0.02596102
 * Flush		  4047644	0.03025494
 * Straight		  6180020	0.04619382
 * Three of a kind	  6461620	0.04829870
 * Two pair		 31433400	0.23495536
 * Pair			 58627800	0.43822546
 * Ace high or less	 23294460	0.17411920
 * -----------------------------------------------
 * Total		133784560	1.00000000
 * ==================================================
 * 
 * Implementation Notes:
 * ---------------------
 * 0. the suit names require unicode. if unicode is not available, garbage may be
 *    displayed. For normal run, they are not used. the hands are printed out in
 *    DEBUG mode only.
 * 1. pip values are not used. they are there only to fulfil the task requirements
 *    a.) ranks are used instead, which makes it easier (for me) to visualise real 
 *        cards.
 * 2. the command line argument specifies the minimum number of hands dealt
 *    a.) the program guarantees the minimum, but the precise number is determined
 *        by the deck / hand-size ratio:
 *        for example, from a 52-card-deck, seven 7-card hands are dealt, and
 *        when you specify 10, 14 hands will be tested.
 *        the same way, the default minimum 1 million hands is 1 million and 6.
 *    b.) non-numeric value in the command line results in 7 hands dealt
 * 3. according to my tests, the 10 million hands range gives a pretty good
 *    approximation of the table above:
 *
 *    Patterns collected:
 *    -------------------
 *           pairs: 4919289 (0.491929)
 *       two pairs: 2408447 (0.240845)
 *          threes:  509327 (0.050933)
 *           fours:   16912 (0.001691)
 *      full house:  245177 (0.024518)
 *         no pair: 1900852 (0.190085)
 *    (hands dealt: 10000004)
 *
 ********************************************************************************/
#include <stdio.h>
#include <stdlib.h>	/* for calloc and free */
#include <time.h>      	/* for time(NULL),srand(), and rand() */

#define MAX_NAME_LENGTH 5
#define NUM_OF_SUITS 4
#define NUM_OF_RANKS 13
/* deck size is determined by the number suits and ranks */
#define DECK_SIZE (NUM_OF_SUITS * NUM_OF_RANKS)
/* it needs be made sure that the deck is properly shuffled */
#define NUM_OF_SHUFFLE_SWAPS (DECK_SIZE * 10)
#define PRINT_SEP "  "
#define HAND_SIZE 7
#define DEBUG 0			/* extra info can be printed out */
#define MIN_REQ_TEST_SIZE 1000000 /* actual number of hands drawn will be 
				     greater or equal */

/* suits and ranks are enumerated types */
typedef enum suits{Hearts, Diamonds, Clubs, Spades}suit;
typedef enum ranks{Ace,Two,Three,Four,Five,Six,Seven,Eight,Nine,Ten,
		   Jack,Queen,King}rank;
/* card: suit and rank identify the cards in the deck, pips is not used, name 
   is used in DEBUG mode only */
typedef struct card{
  suit s;
  rank r;
  char pips;
  char name[MAX_NAME_LENGTH + 1];
}card;
typedef struct pattern_collector{
  unsigned int pairs;
  unsigned int twopairs;
  unsigned int threes;
  unsigned int fours;
  unsigned int fhouse;
  unsigned int hands_dealt;
}pcollector;
/* deck and hand are array types */
typedef card* deck[DECK_SIZE];
typedef card* hand[HAND_SIZE];
/* rank counter is used to collect the number of cards with that rank */
typedef short unsigned int rank_counter[NUM_OF_RANKS];

/* rank and suit names help construct the card name */
const char* suit_names[] = {"♥","♦","♣","♠"};
const char* rank_names[] = {"A","2","3","4","5","6","7",
			    "8","9","10","J","Q","K"};
/* HANDS_IN_DECK determines how many full hands can be drawn from a deck */
const int HANDS_IN_DECK = (int)DECK_SIZE / (int)HAND_SIZE;

/* all card info can be inferred from the suit and rank */
void fill_card(card* c, suit s, rank r) {
  c->s = s;
  c->r = r;
  c->pips = r + 1;
  sprintf(c->name,"%2s%s",rank_names[r],suit_names[s]);
}

/* a deck is an array of card pointers */
void fill_deck(deck d) {
  int place_in_deck = 0;
  for(suit i = 0; i < NUM_OF_SUITS; ++i) {
    suit s = i;
    for (rank j = 0; j < NUM_OF_RANKS; ++j) {
      rank r = j;
      d[place_in_deck] = calloc(1,sizeof(card));
      fill_card(d[place_in_deck++],s,r);
    }
  }
}

/* the cards of a deck are dynamically allocated, so the allocated memory
   needs to be freed up.
*/
void free_deck(deck d) {
  for (int i = 0; i < DECK_SIZE; ++i)
    if (d[i] != NULL)
      free(d[i]);
}

/* the cards of a hand are dynamically allocated, so the allocated memory
   needs to be freed up.
*/
void free_hand(hand h) {
  for (int i = 0; i < HAND_SIZE; ++i)
    free(h[i]);
}

/* when a hand is dealt, the corresponding card pointers will point at
   null, so they need be dealt with differently when they are printed.
*/
void print_deck(deck d) {
  for(int i = 0; i < DECK_SIZE; ++i) {
    if (!d[i])
      printf("nil%s",PRINT_SEP);
    else
      printf("%s%s",d[i]->name,PRINT_SEP);
    
    if ((i+1) % NUM_OF_RANKS == 0)
      putchar('\n');
  }
}

/* shuffling a deck is swapping cards pair by pair at random */
void shuffle_deck(deck d) {
  card* ct;
  for(int i = 0; i < NUM_OF_SHUFFLE_SWAPS; ++i) {
    int index1 = rand() % DECK_SIZE;
    int index2 = rand() % DECK_SIZE;
    if (index1 != index2) {
      ct = d[index1];
      d[index1] = d[index2];
      d[index2] = ct;
    }
  }
}

/* an alternative way to pick hands (without shuffle).
   Not used. */
void pick_rand_hand(deck d, hand h) {
  short int deck_index = rand() % DECK_SIZE;
  for( short int i = 0; i < HAND_SIZE; ++i) {
    while(!d[deck_index])
      deck_index = rand() % DECK_SIZE;
    h[i] = d[deck_index];
    d[deck_index] = NULL;
  }
}

/* pick a hand of cards when the deck is shuffled */
void pick_hand(deck d, hand h) {
  short int deck_index = 0;
  while(!d[deck_index])
    ++deck_index;
  for(short int i = 0; i < HAND_SIZE && i < DECK_SIZE; ++i) {
    h[i] = d[deck_index];
    d[deck_index] = NULL;
    ++deck_index;
  }
}

/* the rank and suit of each card are printed */
void print_hand(hand h) {
  putchar('\t');
  for(int i = 0; i < HAND_SIZE; ++i)
    if (!h[i])
      printf("nil%s",PRINT_SEP);
    else
      printf("%s%s",h[i]->name,PRINT_SEP);
  putchar('\n');
}

/* for debug purposes only */
void print_rank_counter(rank_counter rc) {
 for(short unsigned int i = 0; i < NUM_OF_RANKS; ++i)
    printf("%10s: %d\n",rank_names[i],rc[i]);
}


/* count the occurrence of each rank */
void examine_hand(hand h, rank_counter rc) {
  for (int i = 0; i < HAND_SIZE; ++i)
    ++rc[h[i]->r];
}

/* count the occurrence of each examined pattern:
 * - pairs
 * - two pairs
 * - three of a kind
 * - full house
 * - four of a kind
 * - no pairs (interpreted as no examined pattern)
 */
pcollector* examine_results(rank_counter rc) {
  pcollector* pc = calloc(1,sizeof(pcollector));
  for (int i = 0; i < NUM_OF_RANKS; ++i) {
    switch(rc[i]) {
    case 2:
      if (DEBUG)
	puts("\tpair found");
      ++pc->pairs;
      break;
    case 3:
      if (DEBUG)
	puts("\ttriple found");
      ++pc->threes;
      break;
    case 4:
      if (DEBUG)
	puts("\tquad found");
      ++pc->fours;
      break;
    }
  }
  if(pc->pairs >= 2) {
    if (DEBUG)
      puts("\ttwo pairs found");
    pc->twopairs = 1;
    pc->pairs -= 2;
  } else if (pc->pairs >= 1 && pc->threes >= 1) {
    if (DEBUG)
      puts("\tfull house found");
    pc->fhouse = 1;
    pc->pairs -= 1;
    pc->threes -= 1;
  }
  ++pc->hands_dealt;

  return pc;
}

/* add each pattern collector content to the sum */
void add_results(pcollector* pc, pcollector* pc_sum) {
  pc_sum->pairs += pc->pairs;
  pc->pairs = 0;
  pc_sum->twopairs += pc->twopairs;
  pc->twopairs = 0;
  pc_sum->threes += pc->threes;
  pc->threes = 0;
  pc_sum->fours += pc->fours;
  pc->fours = 0;
  pc_sum->fhouse += pc->fhouse;
  pc->fhouse = 0;
  pc_sum->hands_dealt += pc->hands_dealt;
  pc->hands_dealt = 0;
}

/* get all full hands from the deck */
void play_a_deck(deck d, pcollector* pc_sum) {
    for (int i = 0; i < HANDS_IN_DECK; ++i) {
      hand h = {};
      pick_hand(d,h);
      if (DEBUG)
	print_hand(h);
      rank_counter rc = {};
      examine_hand(h,rc);
      free_hand(h);
      pcollector* pc = examine_results(rc);
      add_results(pc,pc_sum);
      free(pc);
    }
}

/* run a full test */
void do_the_test(pcollector* pc_sum, const unsigned int min_test_size) {
  srand(time(NULL));
  while(pc_sum->hands_dealt <= min_test_size) {
    deck d = {};
    fill_deck(d);
    shuffle_deck(d);
    if (DEBUG)
      puts("New deck.");
    play_a_deck(d,pc_sum);
    free_deck(d);
  }
}

/* print the end results */
void print_pcollector (pcollector* pc) {
  unsigned int sum_of_no_patterns = pc->hands_dealt - (pc->pairs + pc->twopairs + pc->threes + pc->fours + pc->fhouse);
  putchar('\n');
  puts("Patterns collected:");
  puts("-------------------");
  printf("       pairs: %7u (%f)\n",pc->pairs,(double)pc->pairs/pc->hands_dealt);
  printf("   two pairs: %7u (%f)\n",pc->twopairs,(double)pc->twopairs/pc->hands_dealt);
  printf("      threes: %7u (%f)\n",pc->threes,(double)pc->threes/pc->hands_dealt);
  printf("       fours: %7u (%f)\n",pc->fours,(double)pc->fours/pc->hands_dealt);
  printf("  full house: %7u (%f)\n",pc->fhouse,(double)pc->fhouse/pc->hands_dealt);
  printf("     no pair: %7u (%f)\n",sum_of_no_patterns,(double)sum_of_no_patterns/pc->hands_dealt); 
  printf("(hands dealt: %7u)\n",pc->hands_dealt);
}

int main (int argc, char** argv) {
  const unsigned int min_test_size = (argc == 1) ? MIN_REQ_TEST_SIZE : atoi(argv[1]);
  pcollector* pc_sum =  calloc(1,sizeof(pcollector));

  do_the_test(pc_sum,min_test_size);

  print_pcollector(pc_sum);
  free(pc_sum);
  
  return 0;
}
