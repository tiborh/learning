#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */

int main (int argc, char** argv) {
  int counter = 0;
  
  if (argc > 1) {
    sscanf(argv[1],"%d",&counter);
    assert(counter >= 0);
  } else {
    puts("No arg.");
    exit(1);
  }

  printf ("counter: %3d\n",counter);
  
  if (counter > 0) {
    char counter_str[12];
    sprintf(counter_str,"%d",counter-1);
    char* newargv[] = {argv[0],counter_str};
    main(2,newargv);
  } else
    puts("Blast off...!");
  
  return 0;
}
