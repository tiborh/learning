### To use:

    make

    ./prog

### Try:

    make

    ./prog | tee prog_next.c
    make prog_next
    ./prog_next | tee prog_next.c
    make prog_next
    ./prog_next | tee prog_next.c

    make python

    make python3

### Selected Judges Remarks:

And now for something completely different: A program whose metabolic
processes are a matter of interest only to historians. It has kicked the bucket
and departed to that mysterious country from whose bourne no traveller returns!
This is an EX-tremely obfuscated program!

