#include <stdio.h>
#include <math.h>

const int pi = acos(-1);
char* ask_radius = "radius == ";
char* circ_txt = "circumference == %lf\n";
char* area_txt = "area == %lf\n";

int main (int argc, char** argv) {
  double radius = 0;
  printf(ask_radius);
  if(argc > 1) {
    sscanf(argv[1],"%lf",&radius);
    printf("%lf\n",radius);
  } else
    scanf("%lf",&radius);

  printf(circ_txt,2 * radius * pi);
  printf(area_txt, radius * radius * pi);
  return 0;
}
