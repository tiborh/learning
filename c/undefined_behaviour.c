#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  puts("For example, value overflow.");
  unsigned char c1 = 128, c2 = 2*c1, c3 = -1;
  printf("sizeof(c1): %lu\n",sizeof(c1));
  printf("c == %u\n",c1);
  printf("2*c == %u\n",c2);
  printf("(c3 = - 1) == %u\n",c3);
  
  return 0;
}
