#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>

typedef int(*Search_func)(int,int,int*);

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  srand(time(NULL));
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

static int cmp_ints(const void *p1, const void *p2) {
  return(*((int*)p1)-*((int*)p2));
}

int bsearch_(int sought, int* arr, int mini, int maxi) {
  int foundi = -1;
  int midi = (maxi + mini) / 2;
  int midval = *(arr + midi);

  if (midval == sought)
    return midi;
  if (mini >= maxi)
    return foundi;
  if (midval > sought)
    return(bsearch_(sought,arr,mini,midi-1));
  if (midval < sought)
    return(bsearch_(sought,arr,midi+1,maxi));
  
  return foundi;
}

int binary_search(int sought, int len, int* arr) {
  return(bsearch_(sought,arr,0,len-1));
}

int binsearch(int sought, int len, int* arr) {
  int low, high, mid;
  low = 0;
  high = len - 1;
  while (low <= high) {
    mid = (low+high) / 2;
    if (sought < arr[mid])
      high = mid - 1;
    else if (sought > arr[mid])
      low = mid + 1;
    else
      return mid;
  }
  return -1;
}

int binsearch_mod(int x,  int n, int v[]) {
    int low, high, mid;

    low = 0;
    high = n - 1;
    while (low < high) {
        mid = (low + high) / 2;
        if (x <= v[mid])
            high = mid;
        else
            low = mid + 1;
    }
    return (x == v[low]) ? low : -1;
}

int* test_data_preparation(int num_of_nums,int upper_bound) {
  int* arr = calloc(num_of_nums,sizeof(int));
  if (!arr) {
    printf("Could not allocate enough memory for %d test numbers.\n",num_of_nums);
    exit(EXIT_FAILURE);
  }

  fill_array(num_of_nums,arr,upper_bound);
  //puts("unsorted:");
  //print_array(num_of_nums,arr);
  qsort(arr,num_of_nums,sizeof(int),cmp_ints);
  //puts("The array to search in:");
  //print_array(num_of_nums,arr);
  puts("=================================");
  puts("New test data has been generated.");
  puts("=================================");
  return arr;
}

double get_average(int len, long *nums) {
    double avg = 0.0;
    for (int i = 0; i < len; ++i)
        avg += ((double)*(nums + i) - avg) / (double)(i+1);

    return avg;
}

double* test_engine(Search_func* sf,int num_of_funcs, int num_of_nums, int upper_bound,int num_of_tests) {
  int* arr = test_data_preparation(num_of_nums,upper_bound);
  long** search_times = malloc(num_of_funcs*sizeof(long*));
  for (int i = 0; i < num_of_funcs; ++i)
    search_times[i] = calloc(num_of_tests,sizeof(long));
  double* res = calloc(num_of_funcs,sizeof(double));
  clock_t begin = 0, end = 0;
  int re_init_countdown = num_of_nums / 10;
  for(int i = num_of_tests-1; i >= 0; --i) {
    for (int j = 0; j < num_of_funcs; ++j) {
      int foundi = -1;
      int sought = rand() % upper_bound;
      begin = clock();
      foundi = (*sf)(sought,num_of_nums,arr);
      end = clock();
      search_times[j][i] = end - begin;
      printf("with search func nu %d:\n",j);
      if (foundi < 0)
	printf("%d has not been found in the array.\n",sought);
      else
	printf("The index where the value %d was found: %d (proof: %d)\n",sought,foundi,*(arr+foundi));
      printf("search time: %ld ms\n",search_times[j][i]);
    }
    --re_init_countdown;
    if (!re_init_countdown && i) {
      free(arr);
      arr = test_data_preparation(num_of_nums,upper_bound);
      re_init_countdown = num_of_nums / 10;
    }
  }
  
  for (int i = 0; i < num_of_funcs; ++i) {
    res[i] = get_average(num_of_tests,search_times[i]);
    free(search_times[i]);
  }
  free(search_times);
  free(arr);
  return res;
}

int main (int argc, char** argv) {
  int num_of_nums = UCHAR_MAX;
  int upper_bound = UCHAR_MAX;
  int num_of_tests = 100;
  if (argc == 1)
    printf("%s [length (%d)] [ceiling (%d)] [num of tests %d]\n",basename(argv[0]),num_of_nums,upper_bound,num_of_tests);
  if (argc > 1)
    sscanf(argv[1],"%d",&num_of_nums);
  assert(num_of_nums > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&upper_bound);
  if (argc > 3)
    sscanf(argv[3],"%d",&num_of_tests);
  assert(upper_bound > 1);
  assert(num_of_tests > 0);

  int num_of_funcs = 3;
  Search_func* sf = calloc(num_of_funcs,sizeof(Search_func));
  sf[0] = binary_search;
  sf[1] = binsearch;
  sf[2] = binsearch_mod;
  double* res = test_engine(sf,num_of_funcs,num_of_nums,upper_bound,num_of_tests);
  printf("Average time for binary_search: %f\n",res[0]);
  printf("Average time for binsearch: %f\n",res[1]);
  printf("Average time for binsearch_mod: %f\n",res[2]);

  free(sf);
  free(res);
  return 0;
}
