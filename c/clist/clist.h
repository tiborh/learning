#ifndef CLIST_H_INCLUDED
#define CLIST_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct CListElem_ {
    void* data;
    struct CListElem_ *next;
    struct CListElem_ *prev;
} CListElem;

typedef struct CList_ {
    int size;
    CListElem* head;
    long (*cmp)(const void*, const void*);
    void (*destroy)(void*);
} CList;

void initialise_list(CList*, void (*destroy)(void*), long (*cmp)(const void*, const void*));
void destroy_list(CList*);
int insert_as_next_elem(CList* list, CListElem* listelem, CListElem* newelem);
int insert_next(CList*, CListElem*, void*);
int insert_as_prev_elem(CList* list, CListElem* listelem, CListElem* newelem);
int insert_prev(CList*, CListElem*, void*);
int pop_head(CList*,void**);
CListElem* pop_head_elem(CList*);
int push_as_head_elem(CList*, CListElem*);
int push_as_head(CList*, void*);
int push_back(CList*,void*);
int pop_back(CList*,void**);
int remove_elem(CList*,CListElem*,void**);
int get_list_size(const CList*);
CListElem* get_list_head(const CList*);
void* get_elem_data(const CListElem*);
CListElem* get_next_elem(const CListElem*);
CListElem* get_prev_elem(const CListElem*);

#endif // CLIST_H_INCLUDED
