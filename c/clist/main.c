#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <assert.h>
#include "clist.h"

void print_data(const void* data) {
    printf("%u ",*((unsigned *)data));
}
unsigned conv_data(const void* data) {
    if (!data)
        return UINT_MAX;
    return *((unsigned *)data);
}

void destroy_num(void* data) {
    free((unsigned*)data);
}

long int cmp_data(const void* data0, const void* data1) {
    return((long)*((unsigned*)data1) - (long)*((unsigned*)data0));
}

long int cmp_list_elems(CListElem* elem0, CListElem* elem1) {
    return(cmp_data(elem0->data,elem1->data));
}

void print_clist(const CList* cl) {
    if (!cl || !cl->head) {
        fputs("print_clist: nothing to print.\n",stderr);
        return;
    }

    CListElem* tempelem = cl->head;
    do {
        print_data(tempelem->data);
        tempelem = tempelem->next;
    } while(tempelem!=cl->head);
    putchar('\n');
}

void reverse_print_clist(const CList* cl) {
    if (!cl || !cl->head) {
        fputs("reverse_print_clist: nothing to print.\n",stderr);
        return;
    }

    CListElem* tempelem = cl->head->prev;
    do {
        print_data(tempelem->data);
        tempelem = tempelem->prev;
    } while(tempelem!=cl->head->prev);
    putchar('\n');
}

CList* setup_test(void) {
    CList* cl = malloc(sizeof(CList));
    initialise_list(cl,destroy_num,cmp_data);
    return cl;
}

void simple_test(void) {
    CList* cl = setup_test();
    assert(0 == get_list_size(cl));
    assert(!cl->head);
    assert(cl->cmp);
    unsigned* d0 = malloc(sizeof(unsigned));
    *d0 = 123;
    int ret0 = push_as_head(cl,d0);
    assert(1 == get_list_size(cl));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->data)));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->prev->data)));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->next->data)));
    assert(!ret0);
    unsigned* d1 = malloc(sizeof(unsigned));
    *d1 = 321;
    int ret1 = push_as_head(cl,d1);
    assert(2 == get_list_size(cl));
    assert(*((unsigned*)d1) == *((unsigned*)(cl->head->data)));
    assert(!ret1);
    destroy_list(cl);
    assert(0 == get_list_size(cl));
    assert(!cl->head);
    free(cl);
}

void insert_test(void) {
    CList* cl = setup_test();
    unsigned* d0 = malloc(sizeof(unsigned));
    unsigned* d1 = malloc(sizeof(unsigned));
    unsigned* d2 = malloc(sizeof(unsigned));
    *d0 = 0;
    *d1 = 1;
    *d2 = 2;
    int ret0 = insert_next(cl,cl->head,d0);
    assert(!ret0);
    assert(1 == get_list_size(cl));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->data)));
    remove_elem(cl,cl->head,(void**)&d0);
    assert(*((unsigned*)d0) == 0);
    assert(0 == get_list_size(cl));
    assert(!cl->head);
    ret0 = insert_prev(cl,cl->head,d0);
    assert(!ret0);
    assert(1 == get_list_size(cl));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->data)));
    remove_elem(cl,cl->head,(void**)&d0);
    assert(*((unsigned*)d0) == 0);
    assert(0 == get_list_size(cl));
    assert(!cl->head);
    push_as_head(cl,d0);
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->data)));
    insert_next(cl,cl->head,d1);
    assert(2 == get_list_size(cl));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->data)));
    assert(*((unsigned*)d1) == *((unsigned*)(cl->head->next->data)));
    assert(*((unsigned*)d1) == *((unsigned*)(cl->head->prev->data)));
    insert_prev(cl,cl->head,d2);
    assert(3 == get_list_size(cl));
    assert(*((unsigned*)d2) == *((unsigned*)(cl->head->data)));
    assert(*((unsigned*)d0) == *((unsigned*)(cl->head->next->data)));
    assert(*((unsigned*)d1) == *((unsigned*)(cl->head->prev->data)));
    destroy_list(cl);
    free(cl);
}

CList* create_random_list(unsigned length, unsigned upper_bound) {
    CList* cl = setup_test();
    for(unsigned i = 0; i < upper_bound; ++i) {
        unsigned* datum = malloc(sizeof(unsigned));
        *datum = rand() % upper_bound;
        push_as_head(cl,datum);
    }

    return cl;
}

void print_n_elem_list(unsigned num) {
    CList* cl = create_random_list(num,num);
    print_clist(cl);
    reverse_print_clist(cl);
    destroy_list(cl);
    free(cl);
}

void print_test(void) {
    puts("print tests:");
    puts("print empty list:");
    print_n_elem_list(0);
    puts("one-elem list");
    print_n_elem_list(1);
    puts("two-elem list");
    print_n_elem_list(2);
    puts("ten-elem list");
    print_n_elem_list(10);
}

void test_funcs(void) {
    simple_test();
    insert_test();
    print_test();
}

int main() {
    srand(time(NULL));
    test_funcs();
    puts("Tests have passed.");
    puts("Some more tests are needed. Not all functions are properly tested.");
    return 0;
}
