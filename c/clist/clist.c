#include "clist.h"

void initialise_list(CList *list, void (*destroy)(void* data), long (*cmp)(const void* d0, const void* d1)) {
    list->cmp = cmp;
    list->destroy = destroy;
    list->size = 0;
    list->head = NULL;
}

void destroy_list(CList* list) {
    void* data = NULL;
    while(list->head)
        if(!pop_head(list,&data) && list->destroy)
            list->destroy(data);
    memset(list,0,sizeof(CList));
}

int get_list_size(const CList* list) { return list->size; }
CListElem* get_list_head(const CList* list) { return list->head; }
void* get_elem_data(const CListElem* elem) { return elem->data; }
CListElem* get_next_elem(const CListElem* elem) { return elem->next; }
CListElem* get_prev_elem(const CListElem* elem) { return elem->prev; }

int push_as_head_elem(CList* list, CListElem* newelem) {
    if (!list) {
        fputs("push_as_head_elem: list is NULL\n",stderr);
        return -1;
    }
    if (!newelem) {
        fputs("push_as_head_elem: new elem is NULL\n",stderr);
        return -2;
    }

    if (list->head) {
        newelem->next = list->head;
        newelem->prev = list->head->prev;
        list->head->prev->next = newelem;
        list->head->prev = newelem;
    } else {
        newelem->next = newelem;
        newelem->prev = newelem;
    }
    list->head = newelem;

    ++list->size;

    return 0;
}

int insert_as_next_elem(CList* list, CListElem* listelem, CListElem* newelem) {
    if (!list) {
        fputs("insert_as_next_elem: list is NULL\n",stderr);
        return -1;
    }
    if (!listelem) {
        fputs("insert_as_next_elem: list elem is NULL\n",stderr);
        return -2;
    }
    if (!newelem) {
        fputs("insert_as_next_elem: new elem is NULL\n",stderr);
        return -3;
    }

    newelem->prev = listelem;
    newelem->next = listelem->next;
    listelem->next->prev = newelem;
    listelem->next = newelem;

    ++list->size;

    return 0;
}

int insert_next(CList* list, CListElem* listelem, void* data) {
    CListElem* newelem = malloc(sizeof(CListElem));
    if (newelem)
        newelem->data = data;
    else {
        fputs("insert next: could not reserve memory for new elem.\n",stderr);
        return -4;
    }

    if (listelem)
        return insert_as_next_elem(list,listelem,newelem);
    else
        return push_as_head_elem(list,newelem);
}

int insert_as_prev_elem(CList* list, CListElem* listelem, CListElem* newelem) {
    if (!list) {
        fputs("insert_as_prev_elem: list is NULL\n",stderr);
        return -1;
    }
    if (!listelem) {
        fputs("insert_as_prev_elem: list elem is NULL\n",stderr);
        return -2;
    }
    if (!newelem) {
        fputs("insert_as_prev_elem: new elem is NULL\n",stderr);
        return -3;
    }

    if (list->head == listelem)
        list->head = newelem;

    if (listelem->prev)
        listelem->prev->next = newelem;
    newelem->next = listelem;
    newelem->prev = listelem->prev;
    listelem->prev = newelem;

    ++list->size;

    return 0;
}

int insert_prev(CList* list, CListElem* listelem, void* data) {
    CListElem* newelem = malloc(sizeof(CListElem));
    if (newelem)
        newelem->data = data;
    else {
        fputs("insert prev: could not reserve memory for new elem.\n",stderr);
        return -4;
    }

    if (listelem)
        return insert_as_prev_elem(list,list->head,newelem);
    else
        return push_as_head_elem(list,newelem);
}

int push_as_head(CList* list, void* data) {
    return insert_prev(list,list->head,data);
}

int push_back(CList* list,void* data) {
    return insert_prev(list,list->head,data);
}

CListElem* pop_head_elem(CList* list) {
    if (!list) {
        fputs("pop_head_elem: list is NULL.\n",stderr);
        return NULL;
    }
    if (!list->head) {
        fputs("pop_head_elem: list->head is NULL.\n",stderr);
        return NULL;
    }

    CListElem* retelem = list->head;
    if (list->size == 1) {
        retelem = list->head;
        list->head = NULL;
    } else {
        list->head->next->prev = list->head->prev;
        list->head->prev->next = list->head->next;
        list->head = list->head->next;
    }
    retelem->prev = NULL;
    retelem->next = NULL;
    --list->size;

    return retelem;
}

int remove_elem(CList* list,CListElem* listelem, void** data) {
    if (!list) {
        fputs("remove_elem: list is NULL\n",stderr);
        return -1;
    }
    if (!listelem) {
        fputs("remove_elem: list elem is NULL\n",stderr);
        return -2;
    }

    if(list->size == 1) {
        list->head = NULL;
    } else {
        listelem->next->prev = listelem->prev;
        listelem->prev->next = listelem->next;
        if (list->head == listelem)
            list->head = listelem->next;
    }
    *data = listelem->data;
    free(listelem);
    --list->size;

    return 0;
}

int pop_head(CList* list,void** data) {
    return remove_elem(list,list->head,data);
}

int pop_back(CList* list,void** data) {
    if (!list) {
        fputs("pop_back: list is NULL\n",stderr);
        return -1;
    }
    if (!list->head) {
        fputs("pop_back: list is empty.\n",stderr);
        return -2;
    }

    return remove_elem(list,list->head->prev,data);
}
