#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

enum day { Sun, Mon, Tue, Wed, Thu, Fri, Sat };
typedef enum day day;

char* get_day_str(day d) {
  switch (d) {
  case Sun:
    return "Sunday";
  case Mon:
    return "Monday";
  case Tue:
    return "Tuesday";
  case Wed:
    return "Wednesday";
  case Thu:
    return "Thursday";
  case Fri:
    return "Friday";
  case Sat:
    return "Saturday";
  default:
    return NULL;
  }
}

char* get_next_day(day d) {
  return(get_day_str((d + 1) % 7));
}

char* get_prev_day(day d) {
  return(get_day_str(d == Sun ? Sat : d - 1));
}

void print_days(day d) {
  printf("Today is %s. (%d)\n", get_day_str(d),d);
  printf("Tomorrow will be %s.\n", get_next_day(d));
  printf("Yesterday was %s.\n", get_prev_day(d));
}

int main (int argc, char** argv) {
  time_t t = time(NULL);
  struct tm* lt = localtime(&t);
  day week_day = lt->tm_wday;

  print_days(week_day);
  
  return 0;
}
