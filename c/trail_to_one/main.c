#include "trail_to_one.h"

int main (int argc, char** argv) {
  if (argc == 1) {
    help(argv[0]);
    exit(1);
  }
  
  char* end;
  int inp = strtoimax(argv[1],&end,base);

  printf("read:\n\t");
  print_result(inp);
  printf("transformed:\n\t");
  print_result((inp - 1) | inp);
  return 0;
}
