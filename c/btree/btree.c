#include "btree.h"
#include "queue.h"
#include "array.h"

BTREE new_node(DATA data) {
    BTREE bt = malloc(sizeof(NODE));
    bt->data = data;
    bt->left = bt->right = NULL;
    return bt;
}

void insert_node(BTREE* bt,DATA data) {
    if(!*bt) {
        *bt = new_node(data);
    } else if (data < (*bt)->data) {
        insert_node(&(*bt)->left,data);
    } else if (data > (*bt)->data) {
        insert_node(&(*bt)->right,data);
    } else
        fprintf(stderr,"omitted: %u\n",data);
}

static void arr2btree(ARRAY ar, INDEX num, BTREE* bt) {
    if (num < ar->length) {
        insert_node(bt,view_array_item(ar,num));
        arr2btree(ar,num+1,bt);
    }
}

BTREE array_to_btree(ARRAY ar) {
    BTREE bt = NULL;
    if (ar->length > 0)
        arr2btree(ar,0,&bt);
    return bt;
}

BTREE delete_node(BTREE bt,DATA data) {
    if (!bt) {
        bt = NULL;
    } else if (data < bt->data) {
        bt->left = delete_node(bt->left,data);
    } else if (data > bt->data) {
        bt->right = delete_node(bt->right,data);
    } else  {//if (bt->data == data) {
        if (!bt->left && !bt->right) {
            free(bt);
            bt = NULL;
        } else if (!bt->left) {
            BTREE temp = bt;
            bt = bt->right;
            temp->right = NULL;
            free(temp);
        } else if (!bt->right) {
            BTREE temp = bt;
            bt = bt->left;
            temp->left = NULL;
            free(temp);
        } else {
            BTREE temp = find_min(bt->right);
            bt->data = temp->data;
            bt->right = delete_node(bt->right,temp->data);
        }
    }

    return bt;
}

BTREE search_tree(BTREE bt, DATA data) {
    if (!bt)
        return NULL;
    if (bt->data == data)
        return bt;
    if(data > bt->data)
        return search_tree(bt->right,data);
    if(data < bt->data)
        return search_tree(bt->left,data);
    return NULL;
}

BTREE find_successor(BTREE bt,DATA data) {
    BTREE data_node = search_tree(bt,data);
    BTREE successor_node = NULL;
    if (!data_node || find_max(bt)->data == data)
        return successor_node;
    if (data_node->right)
        return find_min(data_node->right); // if there is a right child, the successor is there
    BTREE ancestor_node = bt;
    while(ancestor_node != data_node) {
        if (data_node->data < ancestor_node->data) { // so the ancestor node has a left tree
            successor_node = ancestor_node; // this is surely not yet the data_node
            ancestor_node = ancestor_node->left; // get closer to the data_node
        } else
            ancestor_node = ancestor_node->right; // if greater, the ancestor node must have a right child
    }
    return successor_node;
}

BTREE find_predecessor(BTREE bt,DATA data) {
    BTREE data_node = search_tree(bt,data);
    BTREE predecessor_node = NULL;
    if (!data_node || find_min(bt)->data == data)
        return predecessor_node;
    if (data_node->left)
        return find_max(data_node->left); // if there is a left child, the successor is there
    BTREE ancestor_node = bt;
    while(ancestor_node != data_node) {
        if (data_node->data > ancestor_node->data) { // so the ancestor node has a right tree
            predecessor_node = ancestor_node; // this is surely not yet the data_node
            ancestor_node = ancestor_node->right; // get closer to the data_node
        } else
            ancestor_node = ancestor_node->left; // if less, the ancestor node must have a left child
    }
    return predecessor_node;
}

BTREE find_min(BTREE bt) {
    if (!bt)
        return NULL;
    if (!bt->left)
        return bt;
    return find_min(bt->left);
}

BTREE find_max(BTREE bt) {
    if (!bt)
        return NULL;
    if (!bt->right)
        return bt;
    return find_max(bt->right);
}

void destroy_tree(BTREE* bt) {
    if((*bt)->right)
        destroy_tree(&(*bt)->right);
    if((*bt)->left)
        destroy_tree(&(*bt)->left);
    free(*bt);
}

INDEX tree_height(BTREE bt) {
    if (!bt)
        return 0;
    INDEX left_height = 0;
    INDEX right_height = 0;
    if (bt->left)
        left_height = 1 + tree_height(bt->left);
    if (bt->right)
        right_height = 1 + tree_height((bt->right));
    if (left_height > right_height)
        return left_height;
    return right_height;
}

static INDEX depth_finder(BTREE bt, DATA data, INDEX depth) {
    if (!bt)
        return 0;
    if (bt->data == data)
        return depth;
    if(data > bt->data)
        return depth_finder(bt->right,data,depth+1);
    if(data < bt->data)
        return depth_finder(bt->left,data,depth+1);
    return depth;
}

INDEX node_depth(BTREE bt, DATA data) {
    INDEX depth = 0;
    return depth_finder(bt,data,depth);
}

void preorder_traversal(BTREE bt) {
    if (!bt)
        return;
    printf("%u ",bt->data);
    preorder_traversal(bt->left);
    preorder_traversal(bt->right);
}

void inorder_traversal(BTREE bt) {
    if (!bt)
        return;
    inorder_traversal(bt->left);
    printf("%u ",bt->data);
    inorder_traversal(bt->right);
}

static void inorder_to_queue(BTREE bt) {
    if (!bt)
        return;
    inorder_to_queue(bt->left);
    push_front(bt);
    inorder_to_queue(bt->right);
}

ARRAY tree_to_ordered_array(BTREE bt) {
    inorder_to_queue(bt);
    return flush_queue_to_array();
}
void postorder_traversal(BTREE bt) {
    if (!bt)
        return;
    postorder_traversal(bt->left);
    postorder_traversal(bt->right);
    printf("%u ",bt->data);
}

void levelorder_traversal(BTREE bt) {
    if (!bt && queue_is_empty())
        return;
    if (bt->left)
        push_front(bt->left);
    if (bt->right)
        push_front(bt->right);
    printf("%u ",bt->data);
    if (!queue_is_empty())
        levelorder_traversal(pop_back());
}

bool is_search_tree(BTREE bt) {
    ARRAY ar = tree_to_ordered_array(bt);
    bool retval = true;
    for (INDEX i = 1; i < ar->length; ++i)
        if (view_array_item(ar,i) < view_array_item(ar,i-1))
            retval = false;

    destroy_array(&ar);
    return retval;
}

static bool isBinSearchTree(BTREE bt, DATA minval, DATA maxval) {
    if (!bt)
        return true;
    if (bt->data < minval || bt->data > maxval)
        return false;
    return isBinSearchTree(bt->left,minval,bt->data);
    return isBinSearchTree(bt->right,bt->data,maxval);
}

bool is_binary_search_tree(BTREE bt) {
    return isBinSearchTree(bt,DATAMIN,DATAMAX);
}

INDEX tree_size(BTREE bt) {
    INDEX size = 0;
    if (bt)
        size = 1;
    else
        return size;
    size += tree_size(bt->left);
    size += tree_size(bt->right);
    return size;
}

void print_tree(FILE* fh,BTREE bt) {
    if (!bt)
        return;
    if (bt->left) {
        fprintf(fh,"\t%u -> %u;\n",bt->data,bt->left->data);
        print_tree(fh,bt->left);
    }
    if (bt->right) {
        fprintf(fh,"\t%u -> %u;\n",bt->data,bt->right->data);
        print_tree(fh,bt->right);
    }
}

void print_tree_to_dot_file(FILE* fh, BTREE bt) {
    fprintf(fh,"digraph g {\n");
    if (!bt->left && !bt->right)
        fprintf(fh,"\t%u;\n",bt->data);
    else
        print_tree(fh,bt);
    fprintf(fh,"}\n");
}
