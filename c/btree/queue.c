#include "queue.h"
#include "array.h"

typedef struct list_item {BTREE btree; struct list_item* next; struct list_item* prev; } li;
static li* head = NULL;
static li* tail = NULL;
static INDEX list_length = 0;

void push_front(BTREE bt) {
    li* an_item = calloc(1,sizeof(li));
    an_item->btree = bt;
    if (head)
        head->prev = an_item;
    an_item->next = head;
    an_item->prev = NULL;
    if (!tail)
        tail = an_item;
    head = an_item;
    ++list_length;
}

BTREE pop_back(void) {
    if(tail == NULL)
        return NULL;
    BTREE ret_data = tail->btree;
    li* saved_tail = tail;
    if (head == tail)
        head = NULL;
    tail = tail->prev;
    if (tail)
        tail->next = NULL;
    free(saved_tail);
    --list_length;
    return ret_data;
}

ARRAY queue_data_to_array() {
    if (!tail)
        return NULL;
    ARRAY out_array = create_array(list_length);
    for(li* temp = tail; temp->prev; temp = temp->prev) {
        add_value_to_array(&out_array,temp->btree->data);
    }

    return out_array;
}

ARRAY flush_queue_to_array() {
    ARRAY out_array = create_array(list_length);
    while(tail)
        add_value_to_array(&out_array,pop_back()->data);
    return out_array;
}

void print_numeric_queue(void) {
    li* tmp_ptr = tail;
    for(INDEX i = 1; i <= list_length && tmp_ptr; ++i) {
        printf("%u%s",tmp_ptr->btree->data,SEP);
        tmp_ptr = tmp_ptr->prev;
        if (!(i % BREAK_AFTER))
            putchar('\n');
    }
    putchar('\n');
}

INDEX get_queue_length(void) {
    return list_length;
}

bool queue_is_empty(void) {
    return(head == NULL);
}
