#ifndef ARRAY_H_INCLUDED
#define ARRAY_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "common.h"

ARRAY create_array(INDEX);
void destroy_array(ARRAY*);
void print_array(ARRAY);
void add_value_to_array(ARRAY*,DATA);
void write_array_to_file(ARRAY,FILE*);
DATA view_array_item(ARRAY,INDEX);

#endif // ARRAY_H_INCLUDED
