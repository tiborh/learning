#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include "btree.h"
#include "array.h"

#define NUM_OF_DOTS 5
#define DEFAULT_TYPE 0

void init_balanced_tree(BTREE* bt, DATA n0, DATA n1) {
    DATA diff = abs(n1 - n0);
    if (diff < 2) {
        insert_node(bt,n0);
        if (diff > 0)
            insert_node(bt,n1);
    } else {
        INDEX mid = diff / 2 + n0;
        insert_node(bt,mid);
        init_balanced_tree(bt,n0,mid-1);
        init_balanced_tree(bt,mid+1,n1);
    }
}

void init_tree(BTREE* bt, DATA n) {
    for(DATA i = 0; i < n; ++i)
        insert_node(bt,rand() % DATAMAX);
}

void print_tree_as_array(BTREE bt) {
    ARRAY ar = tree_to_ordered_array(bt);
    puts("Tree dumped into array:");
    print_array(ar);
    destroy_array(&ar);
}

ARRAY generate_numbers(DATA upper_boundary_for_num_of_nums, DATA min_num_of_nums, DATA upper_boundary_for_num_vals, DATA min_num) {
  puts("--------------------------------------------------------------------------------");
  printf("upper boundary for number of numbers: %u\n",upper_boundary_for_num_of_nums);
  printf("minimum number of numbers: %u\n",min_num_of_nums);
  printf("upper boundary for number values: %u\n",upper_boundary_for_num_vals);
  printf("minimum number: %u\n",min_num);
  puts("--------------------------------------------------------------------------------");
  DATA num_of_nums = (rand() % (upper_boundary_for_num_of_nums - min_num_of_nums)) + min_num_of_nums;
  ARRAY ar = create_array(num_of_nums);
  for (INDEX i = 0; i < num_of_nums; ++i) {
    DATA num = (rand() % (upper_boundary_for_num_vals - min_num)) + min_num;
    add_value_to_array(&ar,num);
  }

  return ar;
}

void write_array_data_to_file(ARRAY ar, char* fn) {
  FILE* fh;
  if ((FILE *)NULL == (fh = fopen(fn,"w"))) {
    fprintf(stderr,"Cannot open file for writing: %s\n",fn);
    exit(EXIT_FAILURE);
  }
  write_array_to_file(ar,fh);
  fclose(fh);
  printf("Array data have been written into file %s\n",fn);
}

ARRAY read_data_from_file(char* fn) {
  FILE* fh;
  if ((FILE *)NULL == (fh = fopen(fn,"r"))) {
    fprintf(stderr,"Cannot open file for writing: %s\n",fn);
    exit(EXIT_FAILURE);
  }

  INDEX num_of_data;
  fscanf(fh,"%u ",&num_of_data);
  ARRAY ar = create_array(num_of_data);
  for(INDEX i = 0; i < num_of_data; ++i) {
    DATA datum = 0;
    fscanf(fh,"%hhu ",&datum);
    add_value_to_array(&ar,datum);
  }

  fclose(fh);
  return ar;
}

void write_dot_file(BTREE bt,char* fn) {
    FILE* fh = NULL;
    if (NULL == (fh = fopen(fn,"w"))) {
        printf("Cannot open for writing; %s",fn);
        exit(EXIT_FAILURE);
    }
    print_tree_to_dot_file(fh,bt);
    fclose(fh);
    printf("Tree has been written into: %s\n",fn);
}

int main(int argc, char** argv) {
    srand(time(NULL));
    INDEX num_of_dots = argc > 1 ? atoi(argv[1]) : NUM_OF_DOTS;
    if (!num_of_dots)
        exit(EXIT_FAILURE);
    DATA type = argc > 2 ? atoi(argv[2]) : DEFAULT_TYPE;
    if (type > 1) {
        printf("unknown type: %u\n",type);
        exit(EXIT_FAILURE);
    }
    DATA search_n = argc > 3 ? atoi(argv[3]) : rand() % DATAMAX;

    BTREE bt = NULL;
    BTREE temp = NULL;
    switch (type) {
    case 0:
        init_tree(&bt,num_of_dots);
        break;
    case 1:
        init_balanced_tree(&bt,1,num_of_dots);
        break;
    default:
        printf("unknown type: %d",type);
    }
    temp = search_tree(bt,search_n);
    if(!temp) {
        printf("Not found in tree: %u\n",search_n);
        puts("Delete experiment: it should not crash.");
        bt = delete_node(bt,search_n);
    } else {
        printf("Found in tree: %u\n",search_n);
        DATA nd_depth = node_depth(bt,temp->data);
        printf("node depth: %u\n",nd_depth);
        DATA subtree_height = tree_height(temp);
        if (subtree_height > 0) {
            printf("Subtree:\n");
            printf("\theight: %u\n",subtree_height);
            print_tree(stdout,temp);
        } else
            puts("(No children.)");
        puts("Does it have a preceding number?");
        BTREE predecessor = find_predecessor(bt,search_n);
        if (!predecessor)
            puts("No it doesn't.");
        else
            printf("Yes, it does: %u\n",predecessor->data);
        puts("Does it have a successive number?");
        BTREE successor = find_successor(bt,search_n);
        if (!successor)
            puts("No it doesn't.");
        else
            printf("Yes, it does: %u\n",successor->data);
        puts("Delete experiment:");
        puts("before delete:");
        print_tree_as_array(bt);
        bt = delete_node(bt,search_n);
        puts("after delete:");
        print_tree_as_array(bt);
        if (!bt)
            exit(EXIT_SUCCESS);
    }
    DATA min_val = find_min(bt)->data;
    if (min_val)
        printf("Minimum: %u\n",min_val);
    DATA max_val = find_max(bt)->data;
    if (max_val)
        printf("Maximum: %u\n",max_val);
    printf("Tree height: %u\n",tree_height(bt));
    printf("Tree size: %u\n",tree_size(bt));
    if (is_binary_search_tree(bt))
        puts("A binary search tree.");
    else
        puts("Not a binary search tree.");
    puts("With an alternative method:");
    if (is_search_tree(bt))
        puts("A binary search tree.");
    else
        puts("Not a binary search tree.");
    print_tree_as_array(bt);
    write_dot_file(bt,"btree.dot");
    puts("Depth-first traversals:");
    puts("PreOrder Traversal:");
    preorder_traversal(bt);
    putchar('\n');
    puts("InOrder Traversal:");
    inorder_traversal(bt);
    putchar('\n');
    puts("PostOrder Traversal:");
    postorder_traversal(bt);
    putchar('\n');
    puts("Breadth-First Traversal:");
    levelorder_traversal(bt);
    putchar('\n');
    destroy_tree(&bt);
    puts("File Operations");
    puts("1. Generate data.");
    ARRAY data_ar = generate_numbers(DATAMAX,10,DATAMAX,10);
    printf("Number of data items generated: %u\n",data_ar->length);
    puts("the generated data:");
    print_array(data_ar);
    puts("2. Write data to file.");
    char* data_fn = "btree.dat";
    write_array_data_to_file(data_ar,data_fn);
    destroy_array(&data_ar);
    puts("3. Read data from file to ARRAY");
    data_ar = read_data_from_file(data_fn);
    puts("Data read back:");
    print_array(data_ar);
    puts("4. Create BTREE from ARRAY.");
    BTREE data_bt = array_to_btree(data_ar);
    //print_tree(stdout,data_bt);
    destroy_array(&data_ar);
    puts("5. Write out BTREE in order.");
    inorder_traversal(data_bt);
    putchar('\n');
    puts("5. Write out BTREE into dot file.");
    write_dot_file(data_bt,"data_tree.dot");
    destroy_tree(&data_bt);

    return 0;
}
