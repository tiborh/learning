#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "common.h"

#define SEP "\t"
#define BREAK_AFTER 10

void push_front(BTREE);
BTREE pop_back(void);
void print_numeric_queue(void);
INDEX get_queue_length(void);
ARRAY queue_data_to_array();
ARRAY flush_queue_to_array();
bool queue_is_empty(void);

#endif // QUEUE_H_INCLUDED
