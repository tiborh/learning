#ifndef BTREE_H_INCLUDED
#define BTREE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "common.h"

void insert_node(BTREE*,DATA);
BTREE array_to_btree(ARRAY);
BTREE search_tree(BTREE,DATA);
BTREE find_successor(BTREE,DATA);
BTREE find_predecessor(BTREE,DATA);
BTREE find_min(BTREE);
BTREE find_max(BTREE);
INDEX tree_size(BTREE);
INDEX tree_height(BTREE);
INDEX node_depth(BTREE,DATA);
BTREE delete_node(BTREE,DATA);
void destroy_tree(BTREE*);

bool is_binary_search_tree(BTREE);
bool is_search_tree(BTREE);

void preorder_traversal(BTREE);
void inorder_traversal(BTREE);
ARRAY tree_to_ordered_array(BTREE);
void postorder_traversal(BTREE);
void levelorder_traversal(BTREE);

void print_tree(FILE*,BTREE);
void print_tree_to_dot_file(FILE*,BTREE);

#endif // BTREE_H_INCLUDED
