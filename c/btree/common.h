#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <limits.h>

#define DATAMAX ((DATA)UCHAR_MAX)
#define DATAMIN ((DATA)(DATAMAX+1))
#define PRINTDATASTR "%u"
#define PRINTSEP " "

typedef unsigned char DATA;
typedef unsigned int INDEX;
typedef struct btree_node { DATA data; struct btree_node* left; struct btree_node* right; } NODE;
typedef struct array {INDEX length; INDEX maxlength; DATA* values; } Array;
typedef Array* ARRAY;
typedef NODE* BTREE;

#endif // COMMON_H_INCLUDED
