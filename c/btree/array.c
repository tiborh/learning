#include "array.h"

ARRAY create_array(INDEX length) {
    ARRAY out_array = malloc(sizeof(Array));
    out_array->values = calloc(length,sizeof(DATA));
    out_array->length = 0;
    out_array->maxlength = length;
    return out_array;
}

void add_value_to_array(ARRAY* ar,DATA val) {
    if((*ar)->length != (*ar)->maxlength) {
        (*ar)->values[(*ar)->length] = val;
        (*ar)->length++;
    } else
    fprintf(stderr,"Array is full.\n");
}

DATA view_array_item(ARRAY ar,INDEX i) {
    if (i >= ar->maxlength)
        return DATAMAX;
    return(ar->values[i]);
}

void destroy_array(ARRAY* ar) {
  free((*ar)->values);
  free(*ar);
  *ar = NULL;
}

void write_array_to_file(ARRAY ar,FILE* fh) {
  fprintf(fh,PRINTDATASTR PRINTSEP,ar->length);
  for(INDEX i = 0; i < ar->length; ++i)
    fprintf(fh,PRINTDATASTR PRINTSEP,ar->values[i]);
  putc('\n',fh);
}

void print_array(ARRAY ar) {
    write_array_to_file(ar,stdout);
}
