#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

char *true_or_false(int val) {
  return((val == 0) ? "false" : "true");
}

int main (int argc, char** argv) {
  int i = 1, j = 2;
  int n = (i < j);
  int m = (i > j);
  printf("n == %d (%s)\n",n,true_or_false(n));
  printf("m == %d (%s)\n",m,true_or_false(m));
  return 0;
}
