#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <libgen.h>		/* for basename and dirname */

int main(int argc, char** argv) {
  //long int r = 0;
  int num_of_r = 1;
  long rceil = 10;
  long rmin = 0;
  srand(time(NULL));   // Initialization, should only be called once.
  // rand() Returns a pseudo-random integer between 0 and RAND_MAX.
  if (argc == 1)
    printf("usage: %s [random min (default: %ld)] [random ceiling (default: %ld)] [number of random values (default %d)]\n",basename(argv[0]),rmin,rceil,num_of_r);
  if (argc > 1)
    sscanf(argv[1],"%ld",&rmin);
  if (argc > 2)
    sscanf(argv[2],"%ld",&rceil);
  if (argc > 3)
    sscanf(argv[3],"%d",&num_of_r);

  for(int i = 0; i < num_of_r; ++i)
    printf("%ld ",rmin + rand() % (rceil - rmin));
  putchar('\n');
}
