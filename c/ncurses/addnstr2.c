#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>		/* includes <stdio.h>,  <termios.h>, <termio.h>, or <sgtty.h> */
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#define DELAY 300000

static void finish(int sig);

int main(int argc, char** argv) {

  char* samplestr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
  unsigned samplelen = strlen(samplestr);
  
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  (void) initscr();      /* initialize the curses library */
  keypad(stdscr, TRUE);  /* enable keyboard mapping */
  (void) nonl();         /* tell curses not to do NL->CR/NL on output */
  (void) cbreak();       /* take input chars one at a time, no wait for \n */
  (void) noecho();       /* no not echo input */
  //(void) echo();
  curs_set(FALSE);	 /* no cursor */

  int i = COLS < samplelen ? COLS : samplelen;
  for (int j = 0; i > 0 && j < LINES; ++j,--i) {
    if(samplestr[i-1] == ' ') {
      --j;
      continue;
    }
    addnstr(samplestr,i);
    addch('\n');
    refresh();
    usleep(DELAY);
  }
  getch();
  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(0);
}
