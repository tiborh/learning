#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>		/* includes <stdio.h>,  <termios.h>, <termio.h>, or <sgtty.h> */
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define DELAY 10000

// source:
// https://www.linuxjournal.com/content/getting-started-ncurses

/* How it works:
    1. Set three points that define a triangle.
    2. Randomly select a point anywhere (x,y).

Then:

    1. Randomly select one of the triangle's points.
    2. Set the new x,y to be the midpoint between the previous x,y and the triangle point.
    3. Repeat.
 */

#define ITERMAX 10000
int main(int argc, char** argv) {
    long iter;
    int yi, xi;
    int y[3], x[3];
    int index;
    int maxlines, maxcols;
    srand(time(NULL));

    /* initialize curses */

    initscr();
    cbreak();
    noecho();
    curs_set(FALSE);	 /* no cursor */

    clear();

    /* initialize triangle */

    maxlines = LINES - 1;
    maxcols = COLS - 1;

    y[0] = 0;
    x[0] = 0;

    y[1] = maxlines;
    x[1] = maxcols / 2;

    y[2] = 0;
    x[2] = maxcols;

    mvaddch(y[0], x[0], '0');
    mvaddch(y[1], x[1], '1');
    mvaddch(y[2], x[2], '2');

    /* initialize yi,xi with random values */

    yi = rand() % maxlines;
    xi = rand() % maxcols;

    //mvaddch(yi, xi, '.');

    /* iterate the triangle */
    clock_t begin = clock();
    for (iter = 0; iter < ITERMAX; iter++) {
        index = rand() % 3;

        yi = (yi + y[index]) / 2;
        xi = (xi + x[index]) / 2;

        mvaddch(yi, xi, '*');
        refresh();
	usleep(DELAY);
    }
    clock_t end = clock();

    /* done */
    mvprintw(maxlines-2,0,"Running time: %lu ms",end-begin);
    mvprintw(maxlines-1,0,"Running time: %5.1f seconds",(double)(end-begin)/CLOCKS_PER_SEC);
    mvaddstr(maxlines, 0, "Press any key to quit");

    refresh();

    getch();
    endwin();

    exit(0);

  return 0;
}
