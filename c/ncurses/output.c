#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>		/* includes <stdio.h>,  <termios.h>, <termio.h>, or <sgtty.h> */
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define DELAY 300000

static void finish(int sig);

int main(int argc, char** argv) {

  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  (void) initscr();      /* initialize the curses library */
  keypad(stdscr, TRUE);  /* enable keyboard mapping */
  (void) nonl();         /* tell curses not to do NL->CR/NL on output */
  (void) cbreak();       /* take input chars one at a time, no wait for \n */
  (void) noecho();       /* no not echo input */
  //(void) echo();
  curs_set(FALSE);	 /* no cursor */

  printw("printw: just like normal printf\n");
  printw("addch: for a single char.\n");
  addch('c');
  addch('\n');
  printw("addch with attributes, e.g. addch('c' | A_BOLD | A_ITALIC | A_UNDERLINE | A_BLINK )\n");
  addch('c' | A_BOLD | A_ITALIC | A_UNDERLINE | A_BLINK);
  addch('\n');
  addstr("addstr: similar to puts (but no line break at the end)\n");
  addstr("(press a key to exit)\n");
  getch();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(0);
}
