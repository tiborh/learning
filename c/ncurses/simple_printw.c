#define _DEFAULT_SOURCE		/* for usleep */x
#include <ncurses.h>                   /* ncurses.h includes stdio.h */  
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define DELAY 300000

static void finish(int sig);

int main()
{
 char mesg[]="Just a string";           /* message to be appeared on the screen */
 int row,col;                           /* to store the number of rows and *
                                         * the number of colums of the screen */
 initscr();                             /* start the curses mode */
 while(1) {
   clear();
   getmaxyx(stdscr,row,col);              /* get the number of rows and columns */
   mvprintw(row/2,(col-strlen(mesg))/2,"%s",mesg);
                                        /* print the message at the center of the screen */
   mvprintw(row-2,0,"This screen has %d rows and %d columns\n",row,col);
   printw("Try resizing your window(if possible).");
   refresh();
   usleep(DELAY);
 }
 endwin();

 finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(0);
}
