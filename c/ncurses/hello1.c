#include <ncurses.h>
#include <unistd.h>

void help_line() {
  attron(A_BLINK);
  mvprintw(LINES-1,0,"Press a key to exit.");
  attroff(A_BLINK);
}

int main(int argc, char *argv[]) {

 initscr();
 noecho();
 curs_set(FALSE);

 mvprintw(0, 0, "Hello, world!");
 help_line();
 refresh();

 getch();

 endwin();
}
