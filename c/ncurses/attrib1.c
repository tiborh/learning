#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

//based on programming howto

const attr_t amodes[] = {A_INVIS, A_NORMAL, A_STANDOUT, A_UNDERLINE, A_REVERSE, A_BLINK, A_DIM, A_BOLD, A_ITALIC};

const int nmodes = sizeof(amodes) / sizeof(amodes[1]);

const char* verse[] = {"Why are you incensed?",
		       "  And why is your face fallen?",
		       "For whether you offer well,",
		       "  or whether you do not,",
		       "at the tent flap sin crouches",
		       "  and for you is its longing",
		       "    but you will rule over it!"};
const int nvlines = sizeof(verse) / sizeof(*(verse));

int get_maxline(void) {
  static int len = 0;
  for (int i = 0; i < nvlines; ++i) {
    int lilen = strlen(verse[i]);
    if (lilen > len)
      len = lilen;
  }
  return len;
}

void w_verse_writer(WINDOW* w) {
  for (int i = 0; i < nvlines; ++i)
    mvwprintw(w,i,0,"%s\n",*(verse + i));
  wrefresh(w);
}

int main() {
  initscr();                      /* Start curses mode            */
  raw();                          /* Line buffering disabled      */
  keypad(stdscr, TRUE);           /* We get F1, F2 etc..          */
  noecho();                       /* Don't echo() while we do getch */
  curs_set(FALSE);

  printw("q == exit, another char == reprint verse\n");
  int nlines = nvlines+1;
  int ncols = get_maxline() + 1;
  int starty = 2;
  int startx = 0;
  WINDOW* versewin = newwin(nlines,ncols,starty,startx);
  int ch = 0;
  int i = 0;
  while((ch = getch())) {
    int a_mode = i % nmodes;
    mvwprintw(versewin,nvlines,0,"%d",a_mode);
    wattron(versewin,amodes[a_mode]);
    w_verse_writer(versewin);
    wattroff(versewin,amodes[a_mode]);
    if(ch == 'q' || ch == 'Q') {         /* Without keypad enabled this will */
      wclear(versewin);
      wprintw(versewin,"\n%c key pressed\nExiting...\n",ch);/*  not get to us either       */
      wrefresh(versewin);
      sleep(2);
      break;
    }
    ++i;
  }
  refresh();                      /* Print it on to the real screen */
  //getch();                        /* Wait for user input */
  endwin();                       /* End curses mode                */

  
  return 0;
}





/* full attrib list: 
  (not all work on all systems)
    A_NORMAL        Normal display (no highlight)
    A_STANDOUT      Best highlighting mode of the terminal.
    A_UNDERLINE     Underlining
    A_REVERSE       Reverse video
    A_BLINK         Blinking
    A_DIM           Half bright
    A_BOLD          Extra bright or bold
    A_INVIS         Invisible or blank mode
    A_ITALIC        (experimental)
    up to here
    A_PROTECT       Protected mode
    A_ALTCHARSET    Alternate character set
    A_CHARTEXT      Bit-mask to extract a character
    COLOR_PAIR(n)   Color-pair number n 
*/
