#include <stdlib.h>
#include <curses.h>
#include <signal.h>
#include <time.h>

typedef struct {
  int x;
  int y;
} POINT;

void create_random_point(POINT* pt) {
  pt->x = rand() % COLS;
  pt->y = rand() % LINES;
}

static void finish(int sig);

int main(int argc, char *argv[]) {
  srand(time(NULL));
  int num = 0;

  /* initialize your non-curses data structures here */

  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */

  (void) initscr();      /* initialize the curses library */
  keypad(stdscr, TRUE);  /* enable keyboard mapping */
  (void) nonl();         /* tell curses not to do NL->CR/NL on output */
  (void) cbreak();       /* take input chars one at a time, no wait for \n */
  (void) echo();         /* echo input - in color */

  if (has_colors()) {
    start_color();

    /*
     * Simple color assignment, often all we need.  Color pair 0 cannot
     * be redefined.  This example uses the same value for the color
     * pair as for the foreground color, though of course that is not
     * necessary:
     */
    init_pair(1, COLOR_RED,     COLOR_BLACK);
    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
    init_pair(5, COLOR_CYAN,    COLOR_BLACK);
    init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(7, COLOR_WHITE,   COLOR_BLACK);
  }

  noecho();
  for (;;) {
    int c = getch();     /* refresh, accept single keystroke of input */
    attrset(COLOR_PAIR(num % 8));
    num++;
    POINT* pt = calloc(1,sizeof(POINT));
    create_random_point(pt);
    mvaddch(pt->y,pt->x,c);
    refresh();
    /* process the command keystroke */
    free(pt);
  }

  finish(0);               /* we're done */
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(0);
}
