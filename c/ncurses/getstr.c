#define _DEFAULT_SOURCE		/* for usleep */
#include <ncurses.h>                   /* ncurses.h includes stdio.h */  
#include <string.h>
#include <unistd.h>

int main()
{
 char* mesg="Enter a string: ";                /* message to be appeared on the screen */
 char* msg2="Your string is: %s";
 char str[80];
 int row,col;                           /* to store the number of rows and *
                                         * the number of colums of the screen */
 initscr();                             /* start the curses mode */
 getmaxyx(stdscr,row,col);              /* get the number of rows and columns */
 int ypos = row/2;
 int xpos = (col-strlen(mesg))/2;
 mvprintw(ypos,xpos,"%s",mesg);
                                /* print the message at the center of the screen */
 getstr(str);
 clear();
 refresh();
 usleep(100000);
 mvprintw(ypos, xpos, msg2, str);
 getch();
 endwin();

 return 0;
}
