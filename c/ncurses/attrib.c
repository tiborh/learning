#include <ncurses.h>
#include <unistd.h>

//based on programming howto

int main() {
  int ch;

  initscr();                      /* Start curses mode            */
  raw();                          /* Line buffering disabled      */
  keypad(stdscr, TRUE);           /* We get F1, F2 etc..          */
  noecho();                       /* Don't echo() while we do getch */

  printw("Type any character to see it in bold (F1 == exit)\n");
  while((ch = getch())) { /* If raw() hadn't been called
				   * we have to press enter before it
				   * gets to the program          */

    /* Without noecho() some ugly escape
     * charachters might have been printed
     * on screen                    */
    printw("The pressed key is ");
    attron(A_BOLD);
    printw("%c (%d, 0x%x)\n", ch, ch, ch);
    attroff(A_BOLD);
    if(ch == KEY_F(1)) {         /* Without keypad enabled this will */
      printw("F1 Key pressed\nExiting...\n");/*  not get to us either       */
      refresh();
      sleep(2);
      break;
    }
  }
  refresh();                      /* Print it on to the real screen */
  //getch();                        /* Wait for user input */
  endwin();                       /* End curses mode                */

  return 0;
}

/* full attrib list: 
  (not all work on all systems)
    A_NORMAL        Normal display (no highlight)
    A_STANDOUT      Best highlighting mode of the terminal.
    A_UNDERLINE     Underlining
    A_REVERSE       Reverse video
    A_BLINK         Blinking
    A_DIM           Half bright
    A_BOLD          Extra bright or bold
    A_PROTECT       Protected mode
    A_INVIS         Invisible or blank mode
    A_ALTCHARSET    Alternate character set
    A_CHARTEXT      Bit-mask to extract a character
    COLOR_PAIR(n)   Color-pair number n 
*/
