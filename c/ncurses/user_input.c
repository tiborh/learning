#include <ncurses.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

static void finish(int sig);

int main(int argc, char** argv) {
  (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */
  
  // 1.a. initialisation (of screen and memory):
  initscr();
  // 1.b. configuration of ncurses behaviour:
  cbreak();    // make typed characters immediately available
  // nocbreak();  // turns back on line buffering (after cbreak() turned it off)
  // raw();       // almost like cbreak but the interrupt, quit, suspend, and
                  // flow control characters are all passed through
                  // uninterpreted, instead of generating  a  signal.
                  // it gets overriden by cbreak();
  // noraw();     // places the terminal out of raw mode
  noecho();    // typed characters (in getch()) are not echoed
  // echo();      // turns back echoing chars after noecho() turned it off
  curs_set(FALSE); // to hide the cursor
  
  // 2. write something on the screen:
  int nlines = 3;
  int ncols = COLS-12;
  int starty = LINES-5;
  int startx = 5;
  
  WINDOW* inputwin = newwin(nlines, ncols, starty, startx);
  keypad(inputwin, TRUE);	// for function keys
  box(inputwin, 0, 0);
  mvprintw(starty-1,startx,"Give me a character (and press ENTER):");
  refresh();
  wrefresh(inputwin);
  
  int c = wgetch(inputwin);
  switch(c) {
  case KEY_UP:
    mvwprintw(inputwin,1,1,"Up arrow has been pressed.");
    break;
  case KEY_DOWN:
    mvwprintw(inputwin,1,1,"Down arrow has been pressed.");
    break;
  case KEY_LEFT:
    mvwprintw(inputwin,1,1,"Left arrow has been pressed.");
    break;
  case KEY_RIGHT:
    mvwprintw(inputwin,1,1,"Right arrow has been pressed.");
    break;
  default:
    mvwprintw(inputwin,1,1,"Character input: %c (0x%x)",c,c);
    break;
  }
  mvprintw(LINES-1,0,"Exiting...\n");
  
  // 3. make it appear on the current screen:
  refresh();
  wrefresh(inputwin);
  //wrefresh(outputwin);

  // 4. give time for the user to see it:
  //    (a loop may also come handy)
  //getch();

  // 5. clean up:
  sleep(3);
  endwin();

  finish(0);
}

static void finish(int sig) {
  endwin();
  printf("Exiting...\n");
  /* do your non-curses wrapup here */

  exit(EXIT_SUCCESS);
}
