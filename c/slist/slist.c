#include "slist.h"

void initialise_list(slist *list, void (*destroy)(void *data),long (*cmp)(const void* data0, const void* data1)) {
    list->destroy = destroy;
    list->cmp = cmp;
    list->size = 0;
    list->head = list->tail = NULL;
    return;
}

void destroy_list(slist* list) {
    void* data = NULL;
    while (list->size)
        if (!remove_next_elem(list,NULL,(void **)&data) && list->destroy)
            list->destroy(data);

    /* clear the list: */
    memset(list,0,sizeof(slist));
    return;
}

int insert_next_elem (slist* list, slist_elem* elem, const void* data) {
    slist_elem* new_elem = NULL;

    if(!(new_elem = (slist_elem *)malloc(sizeof(slist_elem))))
        return -1;

    new_elem->data = (void *)data;

    if (!get_list_size(list))
        list->tail = new_elem;

    if (!elem) {
        new_elem->next = list->head;
        list->head = new_elem;
    } else {
        new_elem->next = elem->next;
        elem->next = new_elem;
    }

    ++list->size;

    return 0;
}

int connect_as_next_elem(slist* list,slist_elem* elem,slist_elem* new_elem) {
    if(!new_elem || !new_elem->data)
        return -1;

    if (!get_list_size(list))
        list->tail = new_elem;

    if (!elem) {
        new_elem->next = list->head;
        list->head = new_elem;
    } else {
        new_elem->next = elem->next;
        elem->next = new_elem;
    }

    ++list->size;

    return 0;
}

int push_front(slist* list, const void* data) {
    return(insert_next_elem(list,NULL,data));
}
int push_front_elem(slist* list, slist_elem* new_elem) {
    return(connect_as_next_elem(list,NULL,new_elem));
}

int remove_next_elem (slist* list, slist_elem* elem, void** data) {
    if (!get_list_size(list))
        return -1;

    slist_elem* temp_elem = NULL;

    if (!elem) {
        *data = list->head->data;
        temp_elem = list->head;
        list->head = list->head->next;
    } else {
        if(!elem->next)
            return -2;
        *data = elem->next->data;
        temp_elem = elem->next;
        elem->next = elem->next->next;
        if (!elem->next)
            list->tail = elem;
    }

    free(temp_elem);
    --list->size;

    return 0;
}

slist_elem* disconnect_next_elem(slist* list, slist_elem* elem) {
    if (!get_list_size(list))
        return NULL;

    slist_elem* temp_elem = NULL;

    if (!elem) {
        temp_elem = list->head;
        list->head = list->head->next;
    } else {
        if(!elem->next)
            return NULL;
        temp_elem = elem->next;
        elem->next = elem->next->next;
        if (!elem->next)
            list->tail = elem;
    }

    --list->size;

    return temp_elem;
}

int pop_front(slist* list, void** data) {
    return remove_next_elem(list,NULL,data);
}
slist_elem* pop_front_elem(slist* list) {
    return disconnect_next_elem(list,NULL);
}

void swap_list_elem_data(slist_elem* elem0,slist_elem* elem1) {
    if (elem0 == elem1)
        return;

    void* tmp_data = elem0->data;
    elem0->data = elem1->data;
    elem1->data = tmp_data;
}

int get_list_size(const slist* list) { return list->size; }
slist_elem* get_list_head(const slist* list) { return list->head; }
slist_elem* get_list_tail(const slist* list) { return list->tail; }
bool is_head_elem(const slist* list, const slist_elem* elem) { return elem == list->head; }
bool is_tail_elem(const slist* list, const slist_elem* elem) { return elem == list->tail; }
void* get_elem_data(const slist_elem* elem) { return elem->data; }
slist_elem* get_next_elem(const slist_elem *elem) { return elem->next; }

void print_list(const slist* sl,void(*print_data)(const void* data)) {
    slist_elem* le = sl->head;
    while(le) {
        print_data(le->data);
        le = le->next;
    }
}
