#ifndef SLIST_H_INCLUDED
#define SLIST_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/* Primary source: O'Reilly's Mastering Algorithms with C by Kyle London */

typedef struct sListElem_ {
    void* data;
    struct sListElem_* next;
}slist_elem;

typedef struct sList_ {
    int size;
    long (*cmp)(const void* key1, const void* key2);
    void (*destroy)(void* data);
    slist_elem* head;
    slist_elem* tail;
}slist;

void initialise_list(slist*, void (*destroy)(void*),long (*cmp)(const void*, const void*));
void destroy_list(slist*);
int insert_next_elem(slist*, slist_elem*, const void*);
int connect_as_next_elem(slist*,slist_elem*,slist_elem*);
int push_front(slist*, const void*);
int push_front_elem(slist*, slist_elem*);
int remove_next_elem (slist*, slist_elem*, void**);
//slist_elem* disconnect_next_elem(slist*, slist_elem*);
int pop_front(slist*, void**);
slist_elem* pop_front_elem(slist*);
int get_list_size(const slist*);
slist_elem* get_list_head(const slist*);
slist_elem* get_list_tail(const slist*);
bool is_head_elem(const slist*, const slist_elem*);
bool is_tail_elem(const slist*, const slist_elem*);
void* get_elem_data(const slist_elem*);
void swap_list_elem_data(slist_elem*,slist_elem*);
void print_list(const slist*,void(*print_data)(const void*));

#endif // SLIST_H_INCLUDED
