#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <libgen.h>
#include "slist.h"

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX
#define NUM_OF_TESTS 100

void help(char* argv0) {
    puts("Usage:");
    printf("\t%s [number of numbers] [upper bound] [number of tests]\n",argv0);
}

void print_data(const void* data) {
    printf("%u ",*((unsigned *)data));
}

void destroy_num(void* data) {
    free((unsigned*)data);
}

void fill_list(slist* sl,unsigned num_of_nums, unsigned upper_bound) {
    for(unsigned i = 0; i < num_of_nums; ++i) {
        unsigned* num = malloc(sizeof(unsigned));
        *num = rand() % upper_bound;
        push_front(sl,(void *)num);
    }
}

long int cmp_data(const void* data0, const void* data1) {
    return((long)*((unsigned*)data0) - (long)*((unsigned*)data1));
}

long int cmp_list_elems(slist_elem* elem0, slist_elem* elem1) {
    return(cmp_data(elem0->data,elem1->data));
}

int merge_into(slist* sl, void* data) {
    if (!data || !sl)
        return -1;
    slist_elem* temp_elem = sl->head;
    slist_elem* prev_elem = NULL;
    while(temp_elem) {
        if (cmp_data(data,temp_elem->data) <= 0)
            break;
        prev_elem = temp_elem;
        temp_elem = temp_elem->next;
    }
    insert_next_elem(sl,prev_elem,data);
    return 0;
}

slist* mergesort(slist* sl) {
    slist* sorted_list = malloc(sizeof(slist));
    initialise_list(sorted_list,destroy_num,cmp_data);
    while(sl->head) {
        unsigned* num = NULL;
        pop_front(sl,(void **)&num);
        merge_into(sorted_list,num);
    }
    destroy_list(sl);
    free(sl);
    return sorted_list;
}

slist* bubble_sort(slist* sl) {
    if (!sl)
        return NULL;

    bool change_watch = true;
    for(int upper_bound = get_list_size(sl); upper_bound > 1 && change_watch; --upper_bound) {
        slist_elem* tmp_ptr = sl->head->next;
        slist_elem* prev_ptr = sl->head;
        change_watch = false;
        for(int i = 0; i < upper_bound; ++i) {
            if (cmp_data(prev_ptr->data,tmp_ptr->data) < 0) {
                swap_list_elem_data(prev_ptr,tmp_ptr);
                change_watch = true;
            }
            prev_ptr = tmp_ptr;
            if (tmp_ptr->next)
                tmp_ptr = tmp_ptr->next;
        }
    }

    return sl;
}

unsigned sort_test(slist*(*sort_func)(slist*),unsigned num_of_nums, unsigned upper_bound) {
    slist* sl = malloc(sizeof(slist));
    initialise_list(sl,destroy_num,cmp_data);
    fill_list(sl,num_of_nums,upper_bound);
    puts("unsorted:");
    print_list(sl,print_data);
    putchar('\n');
    clock_t begin = clock();
    sl = sort_func(sl);
    clock_t end = clock();
    puts("sorted:");
    print_list(sl,print_data);
    putchar('\n');
    unsigned result = end - begin;
    printf("time needed: %u ms\n",result);
    destroy_list(sl);
    free(sl);
    return result;
}

void print_array(unsigned len,unsigned* nums) {
    for(unsigned i = 0; i < len; ++i)
        printf("%u ",*(nums + i));
    putchar('\n');
}

double get_average(unsigned len, unsigned *nums) {
    double avg = 0.0;
    for (unsigned i = 0; i < len; ++i)
        avg += ((double)*(nums + i) - avg) / (double)(i+1);

    return avg;
}

double timing_tests(unsigned (*sorting_test_func)(slist*(*)(slist*),unsigned,unsigned),slist* (*sort_func)(slist*),unsigned num_of_nums, unsigned upper_bound, unsigned num_of_tests) {
    unsigned* nums = calloc(num_of_tests,sizeof(unsigned));
    for(unsigned i = 0; i < num_of_tests; ++i) {
        *(nums + i) = sorting_test_func(sort_func,num_of_nums,upper_bound);
    }
    double average = get_average(num_of_tests,nums);
    puts("timings:");
    print_array(num_of_tests,nums);
    free(nums);
    return(average);
}

int main(int argc, char** argv) {
    if (argc == 2 && !strcmp("help",argv[1])) {
        help(basename(argv[0]));
        exit(EXIT_SUCCESS);
    }
    unsigned num_of_nums = argc > 1 ? atoi(argv[1]) : NUM_OF_NUMS;
    unsigned upper_bound = argc > 2 ? atoi(argv[2]) : UPPER_BOUND;
    unsigned num_of_tests = argc > 3 ? atoi(argv[3]) : NUM_OF_TESTS;
    srand(time(NULL));
    puts("mergesort test:");
    double average_merge = timing_tests(sort_test,mergesort,num_of_nums,upper_bound,num_of_tests);
    puts("bubblesort test:");
    double average_bubble = timing_tests(sort_test,bubble_sort,num_of_nums,upper_bound,num_of_tests);
    printf("Average running time for merge sort: %f\n",average_merge);
    printf("Average running time for bubble sort: %f\n",average_bubble);
    return 0;
}
