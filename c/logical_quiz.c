#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {

   int a = 2, b = 5, c = 8;

   printf("  a && b  = %d \n ",  a && b);
   printf(" b %% a   = %d \n ", b % a );  
   printf(" a / b    = %d \n " , a/b); 
   printf(" 2 * a + 1 < b = %d \n ", 2*a + 1 < b);  
   printf(" b <= c = %d \n "  , b<= c);
   printf(" !!c = %d \n " , !!c); 
   printf(" c > 2 && a > 6 = %d \n " , c> 2 && a > 6); 
   printf(" b == 2||a != 4 = %d \n ", b == 2 || a != 4 );
   printf(" (a + b) * c-- = %d \n ",  (a + b) * c-- );
   printf(" c = ++b = %d \n ",c = ++b);  
   return 0;

}
