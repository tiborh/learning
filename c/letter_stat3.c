#include <stdio.h>
#include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

#define BAR_CHAR '#'
#define MAX_BAR_LENGTH 80

void print_bar(int length) {
  for(int i = 0; i < length; ++i)
    putchar(BAR_CHAR);
}

int get_maxval(int max_length,int* stats) {
  int max_val = 0;
  for(int i = 0; i < max_length; ++i)
    if (*(stats+i) > max_val)
      max_val = *(stats+i);
  return max_val;
}

void print_graph(int max_length,int* stats, long sum) {
  int maxval = get_maxval(max_length,stats);
  double sumf = (double)sum;
  for(int i = 0; i < max_length; ++i)
    if (*(stats+i)) {
      int bar_length = maxval < MAX_BAR_LENGTH ? *(stats+i) : (int)(((double)*(stats+i) / maxval) * MAX_BAR_LENGTH) + 1;
      printf("%c\t",i+'a');
      //printf("bar_length: %d\n",bar_length);
      print_bar(bar_length);
      printf(" (%d, %5.2f%%)\n",*(stats+i),100 * *(stats+i)/sumf);
    }
}

int main (int argc, char** argv) {

  int c = 0;			/* if char, needs to look out for 0xff to avoid infinite loop */
  const int Alphabet_Size = 'z' - 'a' + 1;
  int* letter_numbers = calloc(Alphabet_Size,sizeof(int));
  long c_sum = 0;

  while((c = getchar()) != EOF) {
    int c_low = tolower(c);
    if (c_low >= 'a' && c_low <= 'z') {
      ++(*(letter_numbers + (c_low - 'a')));
      ++c_sum;
    }
    /* else */
    /*   printf("unhandled: %c (0x%x)\n",c,c); */
  }

  print_graph(Alphabet_Size,letter_numbers,c_sum);

  free(letter_numbers);
  return 0;
}
