#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#define SEP "\t"
#define BREAK_AFTER 5

typedef struct list_item {unsigned char c; struct list_item* n; } li;
void push_front(unsigned char);
unsigned char pop_front(void);
unsigned char view_front(void);
unsigned char view_nth(unsigned char);
void set_nth(unsigned char, unsigned char);
unsigned int get_length(void);
unsigned int check_length(void);
void print_list(void);
void print_numeric_list(void);
int insert_after(unsigned char, unsigned int);
int insert_before(unsigned char, unsigned int);
unsigned char remove_item(unsigned int);
void reverse(void);
void sort_list(void);
void empty_list(void);
bool is_empty(void);

#endif // LIST_H_INCLUDED
