#include "list.h"

static li* head = NULL;
static unsigned int list_length = 0;

void push_front(unsigned char c) {
    li* an_item = calloc(1,sizeof(li));
    an_item->c = c;
    an_item->n = head;
    head = an_item;
    ++list_length;
}
unsigned char view_front(void) {
    if(!head)
        return((unsigned char) -1);
    return head->c;
}
unsigned char view_nth(unsigned char n) {
    assert(n < list_length);
    unsigned char i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; i < n && tmp_ptr; ++i,tmp_ptr = tmp_ptr->n);
    return tmp_ptr->c;
}
void set_nth(unsigned char n, unsigned char new_val) {
    assert(n < list_length);
    unsigned char i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; i < n && tmp_ptr; ++i,tmp_ptr = tmp_ptr->n);
    tmp_ptr->c = new_val;
}
unsigned int get_length(void) {
    return list_length;
}
unsigned int check_length(void) {
    unsigned int i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; tmp_ptr; ++i,tmp_ptr = tmp_ptr->n);
    return i;
}
unsigned char pop_front(void) {
    if(head == NULL)
        return -1;
    unsigned char ret_c = head->c;
    li* saved_head = head;
    head = head->n;
    free(saved_head);
    --list_length;
    return ret_c;
}
void print_list(void) {
    li* tmp_ptr = head;
    while(tmp_ptr) {
        printf("%c > ",tmp_ptr->c);
        tmp_ptr = tmp_ptr->n;
    }
    puts("NULL");
}
void print_numeric_list(void) {
    li* tmp_ptr = head;
    for(int i = 1; i <= list_length && tmp_ptr; ++i) {
        printf("%d%s",tmp_ptr->c,SEP);
        tmp_ptr = tmp_ptr->n;
        if (!(i % BREAK_AFTER))
            putchar('\n');
    }
    putchar('\n');
}
int insert_after(unsigned char c, unsigned int place) {
    li* tmp_ptr = head;
    int list_counter = 0;
    while(list_counter < place && tmp_ptr) {
        tmp_ptr = tmp_ptr->n;
        ++list_counter;
    }
    if (!tmp_ptr)
        return -1;
    else {
        li* new_li = calloc(1,sizeof(li));
        new_li->c = c;
        new_li->n = tmp_ptr->n;
        tmp_ptr->n = new_li;
        ++list_length;
    }
    return list_counter;
}
int insert_before(unsigned char c, unsigned int place) {
    if (!place) {
        push_front(c);
        return 0;
    }
    else
        return(insert_after(c,place-1));
}
unsigned char remove_item(unsigned int index) {
    li* tmp_ptr = head;
    li* prev_ptr = head;
    int list_counter = 0;
    unsigned char ret_char = 0;
    while(list_counter < index && tmp_ptr) {
        prev_ptr = tmp_ptr;
        tmp_ptr = tmp_ptr->n;
        ++list_counter;
    }
    if (!tmp_ptr)
        return -1;
    else {
        ret_char = tmp_ptr->c;
        if (head == tmp_ptr)
            head = tmp_ptr->n;
        else
            prev_ptr->n = tmp_ptr->n;
        free(tmp_ptr);
        --list_length;
    }
    return ret_char;
}
void reverse(void) {
    li* tmp_ptr = head;
    li* saved_ptr = tmp_ptr->n;
    tmp_ptr->n = NULL;
    while (saved_ptr) {
        li* second_saved_ptr = saved_ptr->n;
        saved_ptr->n = tmp_ptr;
        tmp_ptr = saved_ptr;
        saved_ptr = second_saved_ptr;
    }
    head = tmp_ptr;
}
static void swap(li* li0, li* li1) {
    unsigned char tmp = li0->c;
    li0->c = li1->c;
    li1->c = tmp;
}
static int li_cmp(li* li0, li* li1) {
    return(li1->c - li0->c);
}
void sort_list(void) {
    bool change_watch = true;
    for(unsigned int upper_bound = list_length; upper_bound > 1 && change_watch; --upper_bound) {
        li* tmp_ptr = head->n;
        li* prev_ptr = head;
        change_watch = false;
        for(unsigned int i = 0; i < upper_bound; ++i) {
            if (li_cmp(prev_ptr,tmp_ptr) < 0) {
                swap(prev_ptr,tmp_ptr);
                change_watch = true;
            }
            prev_ptr = tmp_ptr;
            if (tmp_ptr->n)
                tmp_ptr = tmp_ptr->n;
        }
    }
}
void empty_list(void) {
    while(head)
        pop_front();
}
bool is_empty(void) {
    return(head == NULL);
}
