#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "list.h"
#include <limits.h>
#include <string.h>
#include <assert.h>

#define DEBUG 1

void load_stack(char* instr) {
    int slen = strlen(instr);
    for (int i = 0; i < slen; ++i)
        push_front(instr[i]);
}

char* unload_stack() {
    int slen = get_length();
    char* outstr = calloc(slen+1,sizeof(char));
    for(int i = 0; i < slen; ++i)
        outstr[i] = pop_front();
    outstr[slen] = '\0';
    return outstr;
}

char* string_reverse(char *instr) {
    unsigned int saved_length;
    if (DEBUG)
        saved_length = get_length();
    load_stack(instr);
    if (DEBUG) {
        print_list();
        assert(strlen(instr) == get_length());
    }
    char* outstr = unload_stack();
    if (DEBUG)
        assert(saved_length == get_length());
    return(outstr);
}

char* test_reverse(char* instr) {
    load_stack(instr);
    puts("Loaded to stack:");
    print_list();
    reverse();
    puts("After reversal:");
    print_list();
    char* outstr = unload_stack();
    return outstr;
}

char* insertion_before(char* instr, char c, unsigned int place) {
        load_stack(instr);
        int chk = insert_before(c,place);
        if (chk < 0)
            puts("no success");
        else
            print_list();
    return unload_stack();
}

char* insertion_after(char* instr, char c, unsigned int place) {
        load_stack(instr);
        int chk = insert_after(c,place);
        if (chk < 0)
            puts("no success");
        else
            print_list();
    return unload_stack();
}

char* removal(char* instr, unsigned int place) {
    load_stack(instr);
    char out_char = remove_item(place);
    if (out_char == UCHAR_MAX)
        puts("no success");
    else
        printf("returned char: %c\n",out_char);
    return unload_stack();
}

void fill_stack_with_random_chars(unsigned char n) {
    assert(is_empty());
    for (unsigned char i = 0; i < n; ++i)
        push_front(rand() % UCHAR_MAX + 1);
    assert(!is_empty());
}

void nth_element(char* instr, unsigned char n, char new_char) {
    load_stack(instr);
    print_list();
    printf("The nth %u: %c\n",n,view_nth(n));
    set_nth(n,new_char);
    puts("After set:");
    print_list();
    unload_stack();
}

int main(int argc, char** argv)
{
    srand(time(NULL));
//    char viewed = view_front();
//    if (viewed == UCHAR_MAX)
//        puts("Empty list");
//    else
//        printf("view: %d",viewed);

//    push_front('a');
//    printf("view: %c\n",view_front());
//    char c = pop_front();
//    printf("popped: %c\n",c);
//    printf("view: %c\n",view_front());

    if(argc == 2) {
        puts(argv[1]);
        puts("Part 1: Reverse by push and pop");
        puts("-------------------------------");
        char* revstr = string_reverse(argv[1]);
        puts(revstr);
        free(revstr);
        puts("Part 2: Reverse with reverse()");
        puts("------------------------------");
        char* copied_str = test_reverse(argv[1]);
        puts("and the copied str:");
        puts(copied_str);
        free(copied_str);
    } else if (argc == 3) {
        unsigned char num_of_nums = atoi(argv[1]);
        fill_stack_with_random_chars(num_of_nums);
        puts("Unsorted:");
        print_numeric_list();
        sort_list();
        puts("Sorted:");
        print_numeric_list();
        printf("check_length: %u\n",check_length());
        empty_list();
        assert(is_empty());
    } else if(argc == 4) {
        unsigned int place = atoi(argv[2]);
        puts("Part 1: Insertion");
        puts("-----------------");
        puts("insert_before:");
        char* outstr = insertion_before(argv[1],argv[3][0],place);
        puts(outstr);
        free(outstr);
        puts("insert_after:");
        outstr = insertion_after(argv[1],argv[3][0],place);
        puts(outstr);
        free(outstr);
        putchar('\n');
        puts("Part 2: removal");
        puts("-----------------");
        puts("removal test:");
        printf("Before removal: %s\n",argv[1]);
        outstr = removal(argv[1],place);
        printf("After: ");
        puts(outstr);
        free(outstr);
        putchar('\n');
        puts("Part 3: Get/Set");
        puts("---------------");
        nth_element(argv[1],place,argv[3][0]);
    }
    return 0;
}
