#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
     char c = 'c';
     printf("c == %c\n",c);
     printf("&c == %p\n",&c);
     char* pc = &c;
     printf("pc == %p\n",pc);
     printf("*pc == %c\n",*pc);
     const char* pc1;
     printf("pc1 == %p\n",pc1);
     printf("*pc1 == %c\n",*pc1);
     pc1 = pc; 			// legal
     printf("pc1 == %p\n",pc1);
     printf("*pc1 == %c\n",*pc1);
     pc = pc1;			// warning is produced
/* 
  gcc:
const2.c: In function ‘main’:
const2.c:20:9: warning: assignment discards ‘const’ qualifier from pointer target type [-Wdiscarded-qualifiers]
   20 |      pc = pc1;   // warning is produced
      |         ^
  clang:
const2.c:20:9: warning: assigning to 'char *' from 'const char *' discards qualifiers [-Wincompatible-pointer-types-discards-qualifiers]
     pc = pc1;                  // warning is produced
        ^ ~~~
*/

     printf("&pc == %p\n",&pc);
     const char** ppc = &pc;
     printf("ppc == %p\n",ppc);
     printf("*ppc == %p\n",*ppc);
/* 
const2.c:30:19: warning: initializing 'const char **' with an expression of type 'char **' discards qualifiers in nested pointer types [-Wincompatible-pointer-types-discards-qualifiers]
     const char** ppc = &pc;
                  ^     ~~~
 */
  return 0;
}
