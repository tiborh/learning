#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

long long unsigned fib_iter(int num) {
  long long unsigned fib0 = 0;
  long long unsigned fib1 = 1;

  if (num <= 1)
    return num;
  for(int i = 2; i < num; ++i) {
    long long unsigned old_fib0 = fib0;
    fib0 = fib1;
    fib1 = old_fib0 + fib0;
  }
  return(fib0+fib1);
}

long long unsigned fib_recur(int num) {
  if (num <= 1)
    return num;
  else
    return(fib_recur(num-2) + fib_recur(num-1));
}

int main (int argc, char** argv) {
  int fib_num = 0;
  short unsigned recur = 1;

  if (argc > 2)
    sscanf(argv[2],"%hu",&recur);
  if (argc < 2)
    puts("No arg.");
  else
    sscanf(argv[1],"%d",&fib_num);

  assert(fib_num < 100);   /* long long unsigned is good up to 99 */

  for(int i = 0; i <= fib_num; ++i) {
    printf("fibonacci (%d):\n",i);
    printf("\titerative %llu\n",fib_iter(i));
    if (recur)
      printf("\trecursive %llu\n",fib_recur(i));
  }
  
  return 0;
}
