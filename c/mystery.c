#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int mystery(int p, int q) {
    int r;
    if ((r = p % q) == 0)
        return q; 
    else return mystery(q, r);
}

int main (int argc, char** argv) {

  printf("mystery(2,6): %d\n",mystery(2,6));
  printf("mystery(7,91): %d\n",mystery(7,91));

  return 0;
}
