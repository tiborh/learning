#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void reverse_array(int size, void** in) {
  for (int i = 0, j = size-1; i < j; ++i, --j) {
    void* tmp = *(in + i);
    *(in + i) = *(in + j);
    *(in + j) = tmp;
  }
}

void reverse_chars(int size, char* in) {
  for (int i = 0, j = size-1; i < j; ++i, --j)
    if (i != j) {
      int tmp = *(in + i);
      *(in + i) = *(in + j);
      *(in + j) = tmp;
    }
}

int main (int argc, char** argv) {
  reverse_array(argc,(void **)argv);
  for (int i = 0; i < argc; ++i) {
    reverse_chars(strlen(argv[i]),argv[i]);
    printf("%s ",argv[i]);
  }
  putchar('\n');

  return 0;
}
