#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

typedef int INDEX;

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

int cmp(const int* first, const int* second) {
  return *first - *second;
}

bool is_sorted(INDEX alen, int* nums) {
  for (INDEX i = 1; i < alen; ++i)
    if (cmp(nums+(i-1),nums+i) > 0)
      return false;
  return true;
}

int *merge_arrays(int len1, int* arr1, int len2, int* arr2) {
  int lenm = len1 + len2;
  int* arrm = calloc(lenm,sizeof(int));
  for(int i1 = 0, i2 = 0, im = 0; im < lenm; ++im) {
    if (i1 == len1) {
      *(arrm + im) = *(arr2 + i2++);
    } else if (i2 == len2) {
      *(arrm + im) = *(arr1 + i1++);
    } else if (*(arr2 + i2) < *(arr1 +i1)) {
      *(arrm + im) = *(arr2 + i2++);
    } else if (*(arr1 + i1) <= *(arr2 +i2)) {
      *(arrm + im) = *(arr1 + i1++);
    }
  }
  return arrm;
}

int* clone_array(int len, int* arr) {
  int* clone = calloc(len,sizeof(int));
  for(int i = 0; i < len; ++i)
    *(clone + i) = *(arr + i);
  return clone;
}

int* merge_sort(int len, int* arr) {
  if (len == 0)
    return NULL;
  if (len == 1)
    return clone_array(len,arr);
  return merge_arrays(len/2,merge_sort(len/2,arr),len-len/2,merge_sort(len-len/2,arr+len/2));
}

int main (int argc, char** argv) {
  int length_ceiling = 10;
  int value_ceiling = 100;
  if (argc == 1)
    printf("%s [length (%d)] [ceiling (%d)]\n",basename(argv[0]),length_ceiling,value_ceiling);
  if (argc > 1)
    sscanf(argv[1],"%d",&length_ceiling);
  assert(length_ceiling > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&value_ceiling);
  assert(value_ceiling > 1);

  srand(time(NULL));
  int arr_len = (rand() % length_ceiling);
  int* arr = calloc(arr_len,sizeof(int));
  int* arrm = calloc(arr_len,sizeof(int));

  fill_array(arr_len,arr,value_ceiling);
  
  puts("The array:");
  print_array(arr_len,arr);
  arrm = merge_sort(arr_len,arr);
  puts("Merge-sorted:");
  print_array(arr_len,arrm);
  assert(is_sorted(arr_len,arrm));
  
  free(arr);
  free(arrm);
  return 0;
}
