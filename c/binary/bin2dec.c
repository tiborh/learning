#include <stdio.h>
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
#include <stdlib.h>		/* for exit,abs */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <string of binary digits>\n",basename(argv0));
  exit(EXIT_FAILURE);
}

int main (int argc, char** argv) {
  if (argc < 2) {
    help(argv[0]);
  }
  
  int decnum = bin2dec(argv[1]);
  printf("%d\n",decnum);
  
  return 0;
}

/* int bin2dec(char* binstr) { */
/*   int decnum = 0; */
/*   int len = strlen(binstr); */
/*   int bin_multiplier = 1; */

/*   for (int i = len-1; i >= 0; --i) { */
/*     decnum += (binstr[i]-'0') * bin_multiplier; */
/*     bin_multiplier *= 2; */
/*   } */
    
/*   return decnum; */
/* } */

