#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */
#include <libgen.h>		/* for basename and dirname */
#include "binary.h"

void help(char* argv0) {
  puts("Counts the number of 1 bits.");
  puts("Usage:");
  printf("\t%s x\n",argv0);
  printf("\t\tx: a natural number\n");
}

unsigned bitcount(unsigned x) {
  unsigned bitnu = 0;
  for(int i = 0; x != 0; x>>=1)
    if (x & 01)
      ++bitnu;

  return bitnu;
}

unsigned bitcount2(unsigned x) {
  unsigned bitnu = x > 0 ? 1 : 0;
  while((x &= x-1))
      ++bitnu;

  return bitnu;
}

int main (int argc, char** argv) {
  if (argc < 2) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }

  unsigned int xdec = atoi(argv[1]);
  char* xbin = decnum2bin(xdec);
  unsigned bits = bitcount(xdec);
  
  printf("in binary: %s\n",xbin);
  printf("number of bits:  %u\n",bits);
  printf("number of bits2: %u\n",bitcount2(xdec));
  free(xbin);
  
  return 0;
}
