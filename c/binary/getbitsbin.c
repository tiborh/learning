#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */
#include <libgen.h>		/* for basename and dirname */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s x p n\n",argv0);
  printf("\t\tx: a binary number\n");
  printf("\t\tp: a decimal index starting at the small end of x\n");
  printf("\t\ty: a decimal number which says how many bits to copy\n");
}

/* from The C Programming Language, 2nd Edition: */
/* getbits: get n bits of x from position p */
unsigned getbits(unsigned x, int p, int n) {
  return((x >> (p+1-n)) & ~(~0 << n));
}

int main (int argc, char** argv) {
  if (argc < 4) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }
  char* xbin = argv[1];
  int p = atoi(argv[2]);
  int n = atoi(argv[3]);
  unsigned xdec = bin2dec(xbin);
  unsigned res = getbits(xdec,p,n);
  unsigned char* resbin = decnum2bin(res);
  assert(p < strlen(xbin));
  assert(n <= p + 1);
  //printf("getbits(%u,%d,%d) == %u\n",xdec,p,n,res);
  printf("getbits(%s,%d,%d) == %s\n",xbin,p,n,resbin);

  free(resbin);
  
  return 0;
}
