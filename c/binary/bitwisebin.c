#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */
#include "binary.h"

#define MINUS_INF INT_MIN

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <op> <n1> [n2]|<n2>\n",basename(argv0));
  printf("\t\top:        a binary operator\n");
  printf("\t\tn1 and n2: binary numbers\n");
  printf("\tOperators:\n");
  printf("\t\t~  one's complement (unary)\n");
  printf("\t\t&  bitwise AND\n");
  printf("\t\t|  bitwise inclusive OR\n");
  printf("\t\t^  bitwise exclusive OR\n");
  printf("\t\t<  left shift (<<)\n");
  printf("\t\t>  right shift (>>)\n");
}

void print_result(int res) {
  char* binstr = decnum2bin(res);
  printf("  result: %8s (dec: %4d)\n",binstr,res);
  free(binstr);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  char operator = *argv[1];
  char* nu0bin = argv[2];
  int nu0dec = bin2dec(nu0bin);
  if (argc == 3 && *argv[1] != '~')
    exit(EXIT_FAILURE);
  char* nu1bin = "-Inf";
  int nu1dec = MINUS_INF;
  int result = MINUS_INF;
  printf(" number0: %8s (dec: %4d)\n",nu0bin,nu0dec);
  if (argc> 3) {
    nu1bin = argv[3];
    nu1dec = bin2dec(nu1bin);
    printf(" number1: %8s (dec: %4d)\n",nu1bin,nu1dec);
  }

  printf("operator: ");
  switch (*argv[1]) {
  case '~':
    putchar(operator);
    putchar('\n');
    result = ~nu0dec;
    print_result(result);
    break;
  case '&':
    putchar(operator);
    putchar('\n');
    result = nu0dec & nu1dec;
    print_result(result);
    break;
  case '|':
    putchar(operator);
    putchar('\n');
    result = nu0dec | nu1dec;
    print_result(result);
    break;
  case '^':
    putchar(operator);
    putchar('\n');
    result = nu0dec ^ nu1dec;
    print_result(result);
    break;
  case '>':
    puts(">>");
    result = nu0dec >> nu1dec;
    print_result(result);
    break;
  case '<':
    puts("<<");
    result = nu0dec << nu1dec;
    print_result(result);
    break;
  default:
    printf("unknown operator: %c\n",operator);
    exit(EXIT_FAILURE);
  }

  return 0;
}
