#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */
#include <libgen.h>		/* for basename and dirname */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s x n\n",argv0);
  printf("\t\tx: a binary number\n");
  printf("\t\tn: a decimal that says how many bits to rotate right\n");
}

/* from: 
 * https://github.com/ccpalettes/the-c-programming-language-second-edition-solutions/tree/master/Chapter2/Exercise%202-08 */
char* rotright(unsigned x, int n) {
  while (n-- > 0)
    if(x & 1)
      x = (x >> 1) | ~(~0U >> 1);
    else
      x = x >> 1;
  return decnum2bin(x);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }
  char* xbin = argv[1];
  unsigned xdec = bin2dec(xbin);
  int n = atoi(argv[2]);
  //assert(n < strlen(xbin));
  char* resbin = rotright(xdec,n);
  printf("rotright(%s,%d) == %s\n",xbin,n,resbin);
  free(resbin);
  
  return 0;
}
