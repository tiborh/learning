#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */
#include <libgen.h>		/* for basename and dirname */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s x p n\n",argv0);
  printf("\t\tx: a binary number\n");
  printf("\t\tp: a decimal index starting at the small end of x (zero index)\n");
  printf("\t\tn: a decimal that says how many bits to invert (starting with p)\n");
}

/* from: 
 * https://github.com/ccpalettes/the-c-programming-language-second-edition-solutions/tree/master/Chapter2/Exercise%202-07 */
char* invert(unsigned x, int p, int n) {   
    // 000001111100000
    unsigned mask = ~(~0U << n) << (p + 1 - n);
     
    // xxxxx^^^^^xxxxx
    return decnum2bin(x ^ mask);
}

int main (int argc, char** argv) {
  if (argc < 4) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }
  char* xbin = argv[1];
  int xdec = bin2dec(xbin);
  int pos = atoi(argv[2]);
  int n = atoi(argv[3]);
  assert(pos < strlen(xbin));
  assert(n <= pos + 1);
  char* resbin = invert(xdec,pos,n);
  unsigned len = strlen(resbin);
  printf("length of result: %u\n",len);
  printf("invert(%s,%d,%d) == %s\n",xbin,pos,n,resbin);
  free(resbin);
  
  return 0;
}
