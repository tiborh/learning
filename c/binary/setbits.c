#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdlib.h>		/* for exit,abs */
#include <libgen.h>		/* for basename and dirname */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s x p y\n",argv0);
  printf("\t\tx: a binary number\n");
  printf("\t\tp: a decimal index starting at the small end of x\n");
  printf("\t\ty: a binary number whose bits are copied over into x\n");
  printf("\t\t   starting at position p\n");
}

unsigned char* setbits(unsigned x, unsigned int p, char* ybin) {
  int ydec = bin2dec(ybin);
  unsigned ylen = strlen(ybin);
  unsigned x_front = (x >> p+1) << p+1;
  unsigned x_back = 0;
  unsigned backshift = p - ylen + 1;
  unsigned y_shifted = ydec;
  if ((backshift) > 0) {
    x_back = x & ~(~0 << backshift);
    y_shifted <<= backshift;
  }
  return decnum2bin(x_front | y_shifted | x_back);
}

int main (int argc, char** argv) {
  if (argc < 4) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }
  char* xbin = argv[1];
  unsigned xdec = bin2dec(xbin);
  unsigned pos = atoi(argv[2]);
  char* ybin = argv[3];
  unsigned ydec = bin2dec(ybin);
  assert(pos < strlen(xbin));
  assert(strlen(ybin) <= strlen(xbin));
  assert(strlen(ybin) <= pos + 1);
  unsigned char* resbin = setbits(xdec,pos,ybin);
  printf("setbits(%s,%d,%s) == %s\n",xbin,pos,ybin,resbin);
  free(resbin);
  
  return 0;
}
