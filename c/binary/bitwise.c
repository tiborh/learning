#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */
#include "binary.h"

#define MINUS_INF INT_MIN

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <operator> <operand1> [operand2]|<operand2>\n",basename(argv0));
  printf("\tOperators:\n");
  printf("\t\t~  one's complement (unary)\n");
  printf("\t\t&  bitwise AND\n");
  printf("\t\t|  bitwise inclusive OR\n");
  printf("\t\t^  bitwise exclusive OR\n");
  printf("\t\t<  left shift (<<)\n");
  printf("\t\t>  right shift (>>)\n");
}

void print_result(int res) {
  char* binstr = decnum2bin(res);
  printf("  result: %4d (binary: %8s)\n",res,binstr);
  free(binstr);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  char operator = *argv[1];
  int operand0 = atoi(argv[2]);
  char* binstr = decnum2bin(operand0);
  if (argc == 3 && *argv[1] != '~')
    exit(EXIT_FAILURE);
  int operand1 = MINUS_INF;
  int result = MINUS_INF;
  printf("operand0: %4d (binary: %8s)\n",operand0,binstr);
  free(binstr);
  if (argc> 3) {
    operand1 = atoi(argv[3]);
    binstr = decnum2bin(operand1);
    printf("operand1: %4d (binary: %8s)\n",operand1,binstr);
    free(binstr);
  }

  printf("operator: ");
  switch (*argv[1]) {
  case '~':
    putchar(operator);
    putchar('\n');
    result = ~operand0;
    print_result(result);
    break;
  case '&':
    putchar(operator);
    putchar('\n');
    result = operand0 & operand1;
    print_result(result);
    break;
  case '|':
    putchar(operator);
    putchar('\n');
    result = operand0 | operand1;
    print_result(result);
    break;
  case '^':
    putchar(operator);
    putchar('\n');
    result = operand0 ^ operand1;
    print_result(result);
    break;
  case '>':
    puts(">>");
    result = operand0 >> operand1;
    print_result(result);
    break;
  case '<':
    puts("<<");
    result = operand0 << operand1;
    print_result(result);
    break;
  default:
    printf("unknown operator: %c\n",operator);
    exit(EXIT_FAILURE);
  }

  return 0;
}
