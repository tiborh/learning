#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */
#include "binary.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <number> <from back position> <number of bits>\n",argv0);
}

/* from The C Programming Language, 2nd Edition: */
/* getbits: get n bits of x from position p */
unsigned getbits(unsigned x, int p, int n) {
  return((x >> (p+1-n)) & ~(~0 << n));
}

int main (int argc, char** argv) {
  if (argc < 4) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  unsigned x = strtoul(argv[1],NULL,10);
  int p = atoi(argv[2]);
  int n = atoi(argv[3]);
  unsigned char* xbinstr = decnum2bin(x);
  unsigned res = getbits(x,p,n);
  unsigned char* resbinstr = decnum2bin(res);
  assert(p < strlen(xbinstr));
  assert(n <= p + 1);
  printf("getbits(%u,%d,%d) == %u\n",x,p,n,res);
  printf("getbits(%s,%d,%d) == %s\n",xbinstr,p,n,resbinstr);

  free(xbinstr);
  free(resbinstr);
  
  return 0;
}
