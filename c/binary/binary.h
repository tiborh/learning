#ifndef BINARY_H_INCLUDED
#define BINARY_H_INCLUDED
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

unsigned char* decstr2bin(char *);
unsigned char* decnum2bin(int);
int bin2dec(char*);

#endif // BINARY_H_INCLUDED
