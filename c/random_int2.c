#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define ARRLENGTH 10
#define ARRAYCEILING 100

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  srand(time(NULL));
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

int main(int argc, char** argv) {
  int arr[ARRLENGTH] = {0};
  fill_array(ARRLENGTH,arr,ARRAYCEILING);
  print_array(ARRLENGTH,arr);
}
