#include <stdio.h>
#include <math.h>

int counter() {
  static int i = 0;
  return ++i;
}

int main (int argc, char** argv) {
  for(int i = 0; i < 10; ++i)
    printf("counter: %2d\n",counter());

  return 0;
}
