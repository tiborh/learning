#include <stdio.h>
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

#define ASIZE 5			/* int or even const int leads to
				   error: variable-sized object may not be initialized */

void array_printer(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("\t%d (at %p)\n",*(a+i),a+i);
  putchar('\n');
}

int main (int argc, char** argv) {
  int a0[ASIZE];      		/* uninitialised */
  int a1[ASIZE] = {0};		/* zero initialised */
  int a2[ASIZE] = {1,2,3,4,5};	/* all slots initialised with individual items */
  int a3[] = {1,2,3,4,5,6};	/* size not given */

  printf("uninitialised:\n");
  array_printer(ASIZE,a0);
  printf("zero initialised:\n");
  array_printer(ASIZE,a1);
  printf("all initialised individually:\n");
  array_printer(ASIZE,a2);
  printf("like above but size not given when initialised:\n");
  array_printer(6,a3);
  
  return 0;
}
