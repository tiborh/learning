/*******************************************************************************
 * Title: Sort Numbers in Single-Linked List
 * Author: Tibor
 * Date: 2019-12-21
 * Version 0.1
 *
 * Task Description:
 * -----------------
 * Option 1 (General)
 *
 * 1. Use the linear linked list code to store a randomly generated set of 100 
 *    integers.
 * 2. Write a routine that will rearrange the list in sorted order of these
 *    values.
 *    Note you might want to use bubble sort to do this.
 * 3. Print these values in rows of 5 on the screen.
 *
 * How will this work?
 * -------------------
 * 1. Compare two adjacent list elements and if they are out of order swap them.
 * 2. After a first pass the largest element will have bubbled to the end of the
 *    list.
 * 3. Each pass can look at one less element as the end of a list stays sorted.
 *
 * Implementation notes:
 * ---------------------
 * 1. Minimum design is used:
 *    a. as there are only 100 numbers, the storage type is not int but
 *       unsigned char, which can hold 256 different numbers
 *    b. as only one list is needed, the lecture example is simplified so that
 *       fewer arguments are enough when calling the list functions
 * 2. The default number of random numbers is 100 but you can go up to 255 with
 *    a command line argument
 ******************************************************************************/
#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for calloc and random numbers */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for UCHAR_MAX */

#define REQUIRED_NUM 100
#define SEP "\t"
#define BREAK_AFTER 5

typedef struct list_item {unsigned char c; struct list_item* n; } li;
void push_front(unsigned char);
unsigned char pop_front(void);
unsigned int get_length(void);
unsigned int check_length(void);
void print_numeric_list(void);
void sort_list(void);
void empty_list(void);
bool is_empty(void);
void fill_stack_with_random_chars(unsigned char); /* do not ask more than 255 numbers */

/* generate 100 random numbers by default, or 0 to 255 with a command line argument */
int main (int argc, char** argv) {
  srand(time(NULL));
  /* leave it open to experiments: */
  unsigned char num_of_nums = argc > 1 ? atoi(argv[1]) : REQUIRED_NUM;
  fill_stack_with_random_chars(num_of_nums);
  puts("Unsorted:");
  print_numeric_list();
  sort_list();			/* bubble sort */
  puts("Sorted:");
  print_numeric_list();
  empty_list();			/* to make valgrind happy */
  
  return 0;
}

/* static was useful when they were local parameters in a separate file */
static li* head = NULL;
static unsigned int list_length = 0;

/* traditional naming from C++ */
void push_front(unsigned char c) {
    li* an_item = calloc(1,sizeof(li));
    an_item->c = c;
    an_item->n = head;
    head = an_item;
    ++list_length;
}

/* used for emptying the list */
unsigned char pop_front(void) {
    if(head == NULL)
        return -1;
    unsigned char ret_c = head->c;
    li* saved_head = head;
    head = head->n;
    free(saved_head);
    --list_length;
    return ret_c;
}

/* if list_length is properly maintained */
unsigned int get_length(void) {
    return list_length;
}

/* this was useful for debugging purposes to check list_length */
unsigned int check_length(void) {
    unsigned int i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; tmp_ptr; ++i,tmp_ptr = tmp_ptr->n);
    return i;
}

/* chars printed as numbers 
 * break after the required number of columns
 * separate them with the defined separation string
*/
void print_numeric_list(void) {
    li* tmp_ptr = head;
    for(int i = 1; i <= list_length && tmp_ptr; ++i) {
        printf("%d%s",tmp_ptr->c,SEP);
        tmp_ptr = tmp_ptr->n;
        if (!(i % BREAK_AFTER))
            putchar('\n');
    }
    putchar('\n');
}

/* to swap the chars in two list items */
static void li_swap(li* li0, li* li1) {
    unsigned char tmp = li0->c;
    li0->c = li1->c;
    li1->c = tmp;
}

/* to compare the chars (as numbers) in two list items 
   less than zero: li0 is greater than li1
   zero: they are equal
   greater than zero: li0 is less than li1
*/
static int li_cmp(li* li0, li* li1) {
    return(li1->c - li0->c);
}

/* bubble sort in single linked list */
void sort_list(void) {
    bool change_watch = true;	/* to watch if there are any swaps in a loop,
				   to be able to stop cycling in an already sorted list */
    
    /* with each iteration of the outer loop, the inner loop getw shorter */
    for(unsigned int upper_bound = list_length; upper_bound > 1 && change_watch; --upper_bound) {
        li* tmp_ptr = head->n;
        li* prev_ptr = head;
        change_watch = false;
        for(unsigned int i = 0; i < upper_bound; ++i) {
            if (li_cmp(prev_ptr,tmp_ptr) < 0) {
                li_swap(prev_ptr,tmp_ptr);
                change_watch = true;
            }
            prev_ptr = tmp_ptr;
            if (tmp_ptr->n)	/* useful in the last iteration, to avoid segmentation fault */
                tmp_ptr = tmp_ptr->n;
        }
    }
}

/* to help clean up */
void empty_list(void) {
    while(head)
        pop_front();
}

/* to meet conventions */
bool is_empty(void) {
    return(head == NULL);
}

/* chars used as small numbers */
void fill_stack_with_random_chars(unsigned char n) {
  assert(is_empty());		/* for simplicity let's assume it's empty */
  for (unsigned char i = 0; i < n; ++i)
    push_front(rand() % UCHAR_MAX + 1); /* using the max value of unsigned chars */
}
