#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

char* create_alphabet(int lowc,int upc) {
  int lowi = (int)(lowc);
  int upi = (int)(upc);
  int alph_sz = (upi - lowi) + 2;
  char* alph_str = (char*) malloc(alph_sz);
  int i = 0;
  if (alph_str != NULL) {
    for(int j = lowi; j <=upi; ++j,++i)
      alph_str[i] = (char)(j);
    alph_str[i] = 0;
  }
  return(alph_str);
}

int main (int argc, char** argv) {

  int lower_a = (int)('a');
  int lower_z = (int)('z');
  int upper_a = (int)('A');
  int upper_z = (int)('Z');

  char* low_alpha = create_alphabet(lower_a,lower_z);
  puts(low_alpha);
  char* up_alpha = create_alphabet(upper_a,upper_z);
  puts(up_alpha);

  free(low_alpha);
  free(up_alpha);
  
  return 0;
}
