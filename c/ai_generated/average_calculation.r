#!/usr/bin/env Rscript

# Function to read numbers from input file
readNumbers <- function(inputFile) {
  numbers <- scan(inputFile, what = numeric(), quiet = TRUE)
  return(numbers)
}

# Function to calculate average of numbers
calculateAverage <- function(numbers) {
  avg <- mean(numbers)
  return(avg)
}

# Main function
main <- function(inputFile, outputFile) {
  # Step 2: Read numbers from input file
  numbers <- readNumbers(inputFile)
  
  # Step 3: Calculate average of numbers
  avg <- calculateAverage(numbers)
  
  # Step 4: Store input numbers and result in output file
  write(paste("Input numbers:", paste(numbers, collapse = ", ")), file = outputFile)
  write(paste("Average:", avg), file = outputFile, append = TRUE)
}

# Call main function with input and output file paths
inputFile <- "input.txt"
outputFile <- "output.txt"
main(inputFile, outputFile)
