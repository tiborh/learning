#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

bool isPrime(int n) {
    if (n <= 1) {
        return false;
    }
    for (int i = 2; i * i <= n; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int* findPrimes(int start, int end, int *numPrimes) {
    int count = 0;
    for (int i = start; i <= end; i++) {
        if (isPrime(i)) {
            count++;
        }
    }

    int* primes = (int*)malloc(count * sizeof(int));
    if (primes == NULL) {
        *numPrimes = 0;
        return NULL;
    }

    int index = 0;
    for (int i = start; i <= end; i++) {
        if (isPrime(i)) {
            primes[index] = i;
            index++;
        }
    }

    *numPrimes = count;
    return primes;
}

int extractInteger(const char *str) {
  int retval;
  if (sscanf(str, "%d", &retval) != 1)
    retval = 0;
  return retval;
}

void writeResultsToFile(int start, int end, int* primes, int numPrimes) {
    FILE *file = fopen("prime_results.txt", "w");
    if (file == NULL) {
        printf("Error opening file for writing\n");
        return;
    }

    fprintf(file, "Input range: %d to %d\n", start, end);
    fprintf(file, "Prime numbers:\n");
    for (int i = 0; i < numPrimes; i++) {
        fprintf(file, "%d\n", primes[i]);
    }

    fclose(file);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <arg1> <arg2>\n", argv[0]);
        return 1;
    }

    int int1, int2;
    int1 = extractInteger(argv[1]);
    int2 = extractInteger(argv[2]);
    printf("First integer: %d\n", int1);
    printf("Second integer: %d\n", int2);

    int numPrimes;
    int* primes = findPrimes(int1, int2, &numPrimes);
    if (primes != NULL) {
        printf("Prime numbers between %d and %d are:\n", int1, int2);
        for (int i = 0; i < numPrimes; i++) {
            printf("%d\n", primes[i]);
        }
	writeResultsToFile(int1, int2, primes, numPrimes);
        free(primes);
    } else {
        printf("Memory allocation failed\n");
    }

    return 0;
}

