#include <stdio.h>
#include <stdlib.h>

int extractInteger(const char *input) {
    return atoi(input);
}

void readDecimalNumbers(int count, float *numbers) {
    printf("Enter %d decimal numbers:\n", count);
    for (int i = 0; i < count; i++) {
        scanf("%f", &numbers[i]);
    }
}

float calculateAverage(int count, const float *numbers) {
    float sum = 0.0;
    for (int i = 0; i < count; i++) {
        sum += numbers[i];
    }
    return sum / count;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: %s <integer>\n", argv[0]);
        return 1;
    }

    int inputNumber = extractInteger(argv[1]);

    /* int count; */
    /* printf("Enter the count of decimal numbers: "); */
    /* scanf("%d", &count); */

    float *numbers = (float*)malloc(inputNumber * sizeof(float));
    readDecimalNumbers(inputNumber, numbers);

    float average = calculateAverage(inputNumber, numbers);

    FILE *file = fopen("output.txt", "w");
    if (file != NULL) {
        fprintf(file, "Commandline argument: %d\n", inputNumber);
	fprintf(file, "Numbers read on terminal: ");
	for (int i = 0; i < inputNumber; i++) {
	  fprintf(file, "%f ", numbers[i]);
	}
	fputc('\n',file);
        fprintf(file, "Average: %f\n", average);
        fclose(file);
    } else {
        printf("Error opening file for writing.\n");
    }

    free(numbers);

    return 0;
}
