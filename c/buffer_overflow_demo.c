/* source: https://youtu.be/7YRyFMv-tY8 */
#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

void function2() {
  puts("malicious");
}

void function1() {
  char name[8];
  /*      buf   src       EBP               New return value */
  strncpy(name,"0123456" "\x01\x02\x03\x04",12);
  puts("intended");
}

int main (int argc, char** argv) {
  puts("Starting...");
  function1();

  return 0;
}

/* it does not work:
$ make buffer_overflow_demo
gcc -g -std=c18 -pthread -Wall -Werror    buffer_overflow_demo.c  -lm  -o buffer_overflow_demo
buffer_overflow_demo.c: In function ‘function1’:
buffer_overflow_demo.c:23:3: error: ‘__builtin_memcpy’ writing 12 bytes into a region of size 8 overflows the destination [-Werror=stringop-overflow=]
   23 |   strncpy(name,"0123456" "\x01\x02\x03\x04",12);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cc1: all warnings being treated as errors
make: *** [<builtin>: buffer_overflow_demo] Error 1
[tibor@pi4tvroom c]$ gcc -o buffer_overflow_demo buffer_overflow_demo.c
buffer_overflow_demo.c: In function ‘function1’:
buffer_overflow_demo.c:23:3: warning: ‘__builtin_memcpy’ writing 12 bytes into a region of size 8 overflows the destination [-Wstringop-overflow=]
   23 |   strncpy(name,"0123456" "\x01\x02\x03\x04",12);
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[tibor@pi4tvroom c]$ ./buffer_overflow_demo 
Starting...
intended
*** stack smashing detected ***: terminated
Aborted (core dumped)
 */
