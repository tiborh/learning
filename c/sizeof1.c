#include <stdio.h>
#include <wchar.h>
#include <time.h>

char forced_conversions() {
    char n0 = 127;
    wchar_t n8 = 128;
    int n1 = 12;
    long n7 = 123;
    long long n5 = 21;
    size_t n6 = 1000;
    double n2 = 13.5;
    float n3 = 12.1;
    long double n4 = 15.7;
    time_t t1 = time(NULL);

    printf("size of char:                 %2lu\n",sizeof(n0));
    printf("size of wchar:                %2lu\n",sizeof(n8));    
    printf("size of int:                  %2lu\n",sizeof(n1));
    printf("size of long int:             %2lu\n",sizeof(n7));
    printf("size of long long:            %2lu\n",sizeof(n5));
    printf("size of size_t:               %2lu\n",sizeof(n6));
    printf("size of time_t:               %2lu\n",sizeof(t1));
    printf("size of float:                %2lu\n",sizeof(n3));
    printf("size of double:               %2lu\n",sizeof(n2));
    printf("size of long double:          %2lu\n",sizeof(n4));
    printf("size of char + int:           %2lu\n",sizeof(n0+n1));
    printf("size of char / int:           %2lu\n",sizeof(n0/n1));
    printf("size of int + double:         %2lu\n",sizeof(n1+n2));
    printf("size of double + float:       %2lu\n",sizeof(n2+n3));
    printf("size of double + long double: %2lu\n",sizeof(n2+n4));

    return n5;
}

int main(void) {
  puts("For the illustration of forced (or implicit) conversions:");
  forced_conversions();
  printf("sizeof(forced_conversions()): %2lu\n",sizeof(forced_conversions()));
  
    return 0;
}
