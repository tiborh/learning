#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {

  enum fuzzy{false,true,maybe};

  enum fuzzy a = false;

  printf("fuzzy false: %d\n",a);

  enum month{jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec};
  
  enum month next_month(enum month m) {
    return((m + 1) % 12);
  }
    
  printf("next to dec: %u\n", next_month(dec));

  enum month get_month(enum month m) {
    return(m);
  }
    
  printf("april printed: %u\n", get_month(apr));
  printf("next to dec + 1: %u\n", next_month(dec + 1));

  typedef enum choices {left, left_center = 2, right_center, right = 5} choices;
  choices cho = left;
  choices rice = right_center;
  printf("left: %u\n",cho);
  printf("right_center: %u\n",rice);
  
  return 0;
}
