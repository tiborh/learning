#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

/* in lbs */
#define MAX_EL_SEAL_WT_M 8800
#define MIN_EL_SEAL_WT_M 4400
#define MAX_EL_SEAL_WT_F 2000
#define MIN_EL_SEAL_WT_F  800

#define RANGE MIN_EL_SEAL_WT_M
#define POPULAT 1000
#define WT_OVER rand() % RANGE
#define WT WT_OVER + MIN_EL_SEAL_WT_M
#define FILL for (int i = 0; i < POPULAT; ++i) \
    data[i] = WT

void print_data(int d[], int size) {
  for(int i = 0; i < size; ++i) {
    printf("%d\t",d[i]);
    if((i+1) % 10 == 0)
      putchar('\n');
  }
}

int main (int argc, char** argv) {
  int data[POPULAT];
  srand(time(NULL));
  FILL;
  print_data(data, POPULAT);
  printf("\n\n");

  return 0;
}
