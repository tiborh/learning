#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */
#include "dec2bin.h"

#define MINUS_INF INT_MIN

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <operator> <operand1> [operand2]|<operand2>\n",basename(argv0));
  printf("\tOperators:\n");
  printf("\t\t~: one's complement (unary)\n");
  printf("\t\t&: bitwise AND\n");
  printf("\t\t|: bitwise inclusive OR\n");
  printf("\t\t^: bitwise exclusive OR\n");
  printf("\t\t<: left shift (<<)\n");
  printf("\t\t>: right shift (>>)\n");
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  char operator = *argv[1];
  int operand0 = atoi(argv[2]);
  if (argc == 3 && *argv[1] != '~')
    exit(EXIT_FAILURE);
  int operand1 = MINUS_INF;
  int result = MINUS_INF;
  printf("%c %d ",operator,operand0);
  if (argc> 3) {
    operand1 = atoi(argv[3]);
    printf("%d",operand1);
  }
  putchar('\n');

  char* binstr = dec2bin(operand0);
  printf("%c %s ",operator,binstr);
  free(binstr);
  if (argc > 3) {
    binstr = dec2bin(operand1);
    printf("%s",binstr);
    free(binstr);
  }
  putchar('\n');
  
  switch (*argv[1]) {
  case '~':
    result = ~operand0;
    //printf("result: %d\n",result);
    print_result(result);
    break;
  case '&':
    result = operand0 & operand1;
    print_result(result);
    break;
  case '|':
    result = operand0 | operand1;
    print_result(result);
    break;
  case '^':
    result = operand0 ^ operand1;
    print_result(result);
    break;
  case '>':
    result = operand0 >> operand1;
    print_result(result);
    break;
  case '<':
    result = operand0 << operand1;
    print_result(result);
    break;
  default:
    printf("unknown operator: %c\n",operator);
    exit(EXIT_FAILURE);
  }

  return 0;
}
