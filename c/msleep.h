#ifndef MSLEEP_H_INCLUDED
#define MSLEEP_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <time.h>   		/* struct timespec */
#include <errno.h>

#define SECOMULTIP 1000
#define NANOMULTIP 1000000
#define MINMS 0
#define MAXMS 999999999
#define ERRORTHRESHOLD MINMS

void print_tm(struct timespec tm) {
  printf("\t sec: %ld\n",tm.tv_sec);
  printf("\tnsec: %ld\n",tm.tv_nsec);
}

int msleep(unsigned ms) {
   struct timespec tm0, tm1;

   if (ms > MAXMS) {
     printf("msleep error:\n\tvalue cannot be greater than %u\n",(unsigned)MAXMS);
     printf("\treceived value: %u\n",ms);
     errno = EINVAL;
     exit(EXIT_FAILURE);
   }
   
   if(ms > SECOMULTIP-1) {   
     tm0.tv_sec = (int)(ms / SECOMULTIP);                            /* Must be Non-Negative */
     tm0.tv_nsec = (ms - ((long)tm0.tv_sec * SECOMULTIP)) * NANOMULTIP; /* Must be in range of MINMS to MAXMS */
   } else {   
     tm0.tv_sec = MINMS;                     /* Must be Non-Negative */
     tm0.tv_nsec = (long)ms * NANOMULTIP;    /* Must be in range of MINMS to MAXMS */
   }   

   long result = nanosleep(&tm0 , &tm1);

   if (result < ERRORTHRESHOLD) {
     puts("Nanosleep error.");
     printf("with msleep input: %u\n",ms);
     print_tm(tm0);
     exit(EXIT_FAILURE);
   }
   
   return result;
}

#endif // MSLEEP_H_INCLUDED
