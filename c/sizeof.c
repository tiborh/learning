#include <stdio.h>
#include <wchar.h>

char forced_conversions() {
    char n0 = 127;
    wchar_t n8 = 257;
    int n1 = 12;
    long n6 = 1000;
    size_t n7 = 1001;
    long long n5 = 21;
    double n2 = 13.5;
    float n3 = 12.1;
    long double n4 = 15.7;

    printf("sizeof(%d): %lu\n",n0,sizeof(n0));
    printf("sizeof(%d): %lu\n",n8,sizeof(n8));
    printf("sizeof(%d): %lu\n",n1,sizeof(n1));
    printf("sizeof(%f): %lu\n",n3,sizeof(n3));
    printf("sizeof(%f): %lu\n",n2,sizeof(n2));
    printf("sizeof(%Lf): %lu\n",n4,sizeof(n4));
    printf("sizeof(%Ld): %lu\n",n5,sizeof(n5));
    printf("sizeof(%ld): %lu\n",n6,sizeof(n6));
    printf("sizeof(%lu): %lu\n",n7,sizeof(n7));
    printf("sizeof(%d + %d): %lu\n",n0,n1,sizeof(n0+n1));
    printf("sizeof(%d / %d): %lu\n",n0,n1,sizeof(n0/n1));
    printf("sizeof(%d + %f): %lu\n",n1,n2,sizeof(n1+n2));
    printf("sizeof(%f + %f): %lu\n",n2,n3,sizeof(n2+n3));
    printf("sizeof(%f + %Lf): %lu\n",n2,n4,sizeof(n2+n4));

    return n5;
}

int main(void) {
  puts("For the illustration of forced (or implicit) conversions:");
  forced_conversions();
  printf("sizeof(forced_conversions()): %lu\n",sizeof(forced_conversions()));
  
    return 0;
}
