/*******************************************************************************
 * Title: File to Array to Binary Tree to In-Order Printout
 * Author: Tibor
 * Date: 2019-12-29
 * Version: 0.2
 *
 * Assignment Description:
 * -----------------------
 * 1. Open and read a file of integers into an array that is created with
 *    -> the first integer telling you how many to read.
 *    -> So  4 9  11  4  5 would mean create an integer array size 4 and read
 *       into data[].  
 * 2. Write a routine that places these values into a binary tree structure.
 * 3. Then walk the tree “inorder” and print these values to the screen.
 *
 * Implementation Notes:
 * ---------------------
 * 1. As array can only dynamically be constructed from a variable with the
 *    C compiler on my computer, I'll use an ARRAY data structure which also
 *    contains the length and maxlength of the array. (plus some functions
 *    to help its usage.
 * 2. For the experimentation, I found it useful to randomly generate the input
 *    size and values, so these ones are also included.
 *    -> command line arguments can modify the default parameters.
 * 3. As there is not large number of values, and the values in the example are
 *    not too high, unsigned chars will be used in place of integers.
 * 4. The original first sentence makes a mention of "ineger pairs". As the
 *    "pair" part has no significance later in the exercise, the word is omitted.
 * 5. For simplicity, only those functions will be copied here (from my
 *    playground project) that are 
 *    -> either used for the completion of the assignment
 *    -> or helped in troubleshooting (e.g. writing the tree out in graphviz
 *       format for better visualisation (in xdot, or other viewers)
 *******************************************************************************/
#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <libgen.h>		/* for basename */
#include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,malloc,calloc, and free */
#include <time.h>      	        /* for time(NULL) */
#include <limits.h>		/* for UCHAR_MAX */

/* some common definitions */
#define DATAMAX ((DATA)UCHAR_MAX)
#define DATAMIN ((DATA)(DATAMAX+1))
#define PRINTDATASTR "%u"
#define PRINTSEP " "

/* for the current program: */
#define DATA_FN "array.dat"
#define RESULT_FN "btree_inorder.dat"
#define BINARY_TREE_DOT_FN "binary_tree_edges.dot"
#define MIN_NUM 10
#define MIN_NUM_OF_NUMS MIN_NUM

/* type definitions: */
typedef unsigned char DATA;
typedef unsigned int INDEX;
typedef struct btree_node {
  DATA data;
  struct btree_node* left;
  struct btree_node* right;
} NODE;
typedef struct array {
  INDEX length;
  INDEX maxlength;
  DATA* values;
} Array;
typedef Array* ARRAY;
typedef NODE* BTREE;
/* for queue which is used internally by binary tree: */
typedef struct list_item {
  BTREE btree;
  struct list_item* next;
  struct list_item* prev;
} li;

/* functions for data file preparation: */
ARRAY generate_numbers(INDEX, INDEX, DATA, DATA);
void write_array_data_to_file(ARRAY, char*);

/* ARRAY funcitons: */
ARRAY create_array(INDEX);
void destroy_array(ARRAY*);
void print_array(ARRAY);
void add_value_to_array(ARRAY*,DATA);
void write_array_to_file(ARRAY,FILE*);
DATA view_array_item(ARRAY,INDEX);
ARRAY read_data_from_file(char*); /* not strictly */

/* Binary Tree (BTREE) Functions */
void insert_node(BTREE*,DATA);
BTREE array_to_btree(ARRAY);
void inorder_traversal(BTREE);
ARRAY tree_to_ordered_array(BTREE);
void destroy_tree(BTREE*);
void print_tree_to_dot_file(FILE*,BTREE);

/* nice to have: */
void write_dot_file(BTREE, char*); /* the edges are written into the file */

/* used by tree only */
void push_front(BTREE);
BTREE pop_back(void);
INDEX get_queue_length(void);
ARRAY queue_data_to_array();
ARRAY flush_queue_to_array();
bool queue_is_empty(void);

/* this tries to help make the code self-documenting */
void help(char* fn) {
  puts("Usage:");
  printf("    %s [arg 1] [arg 2] [arg 3] [arg 4]\n",fn);
  printf("    Defaults: %u, %u, %u, %u\n",DATAMAX,MIN_NUM_OF_NUMS,DATAMAX,MIN_NUM);
  printf("\targ 1 (optional): upper boundary for the generated number of numbers,\n");
  printf("\t                  (non-inclusive)\n");
  printf("\targ 2 (optional): the minimum number of random numbers generated.\n");
  printf("\t                  (inclusive)\n");
  printf("\targ 3 (optional): upper boundary for the values of the generated\n");
  printf("\t                  numbers, (non-inclusive)\n");
  printf("\targ 3 (optional): the minimum value generated. (inclusive)\n");
}

int main (int argc, char** argv) {
  /* for a simple help: */
  if (argc == 2 && !strcmp(argv[1],"help")) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }

  /* optional parameters helped some experimentation */
  INDEX upper_boundary_for_num_of_nums = argc > 1 ? atoi(argv[1]) : DATAMAX;
  INDEX min_num_of_nums = argc > 2 ? atoi(argv[2]) : MIN_NUM_OF_NUMS;
  DATA upper_boundary_for_num_vals = argc > 3 ? atoi(argv[3]) : DATAMAX;
  DATA min_num = argc > 4 ? atoi(argv[4]) : MIN_NUM;
  
  srand(time(NULL));

  /* data file preparation */
  puts("1. Generate data.");
  ARRAY data_array = generate_numbers(upper_boundary_for_num_of_nums,
				      min_num_of_nums,
				      upper_boundary_for_num_vals,
				      min_num);
  printf("Number of data items generated: %u\n",data_array->length);
  puts("the generated data:");
  print_array(data_array);

  puts("2. Write data to file.");
  write_array_data_to_file(data_array,DATA_FN);
  destroy_array(&data_array);
  assert(!data_array); 		/* should be NULL */

  /* start of the assignment */
  puts("3. Read data from file to ARRAY");
  data_array = read_data_from_file(DATA_FN);
  puts("Data read back:");
  print_array(data_array);

  puts("4. Create binary tree from ARRAY.");
  BTREE data_bt = array_to_btree(data_array);
  destroy_array(&data_array);
  puts("the binary tree edges:");
  print_tree_to_dot_file(stdout,data_bt);

  puts("5. Write out BTREE in order.");
  /* the assignment required printing the tree in order: */
  inorder_traversal(data_bt);
  putchar('\n');
  /* alternatively, you can save the data to file: */
  ARRAY ordered_data = tree_to_ordered_array(data_bt);
  write_array_data_to_file(ordered_data,RESULT_FN);
  destroy_array(&ordered_data);

  /* the nice to have: */
  puts("5. Write out BTREE into dot file.");
  write_dot_file(data_bt,BINARY_TREE_DOT_FN);
  destroy_tree(&data_bt);

  return 0;
}

ARRAY generate_numbers(INDEX upper_boundary_for_num_of_nums, INDEX min_num_of_nums, DATA upper_boundary_for_num_vals, DATA min_num) {
  puts("--------------------------------------------------------------------------------");
  printf("upper boundary for number of numbers: %u\n",upper_boundary_for_num_of_nums);
  printf("minimum number of numbers: %u\n",min_num_of_nums);
  printf("upper boundary for number values: %u\n",upper_boundary_for_num_vals);
  printf("minimum number: %u\n",min_num);
  puts("--------------------------------------------------------------------------------");
  INDEX num_of_nums = (rand() % (upper_boundary_for_num_of_nums - min_num_of_nums)) + min_num_of_nums;
  ARRAY ar = create_array(num_of_nums);
  for (INDEX i = 0; i < num_of_nums; ++i) {
    DATA num = (rand() % (upper_boundary_for_num_vals - min_num)) + min_num;
    add_value_to_array(&ar,num);
  }

  return ar;
}

void write_array_data_to_file(ARRAY ar, char* fn) {
  FILE* fh;
  if ((FILE *)NULL == (fh = fopen(fn,"w"))) {
    fprintf(stderr,"Cannot open file for writing: %s\n",fn);
    exit(EXIT_FAILURE);
  }
  write_array_to_file(ar,fh);
  fclose(fh);
  printf("Array data have been written into file %s\n",fn);
}

ARRAY read_data_from_file(char* fn) {
  FILE* fh;
  if ((FILE *)NULL == (fh = fopen(fn,"r"))) {
    fprintf(stderr,"Cannot open file for writing: %s\n",fn);
    exit(EXIT_FAILURE);
  }

  INDEX num_of_data;
  fscanf(fh,"%u ",&num_of_data);
  ARRAY ar = create_array(num_of_data);
  for(INDEX i = 0; i < num_of_data; ++i) {
    DATA datum = 0;
    fscanf(fh,"%hhu ",&datum);
    add_value_to_array(&ar,datum);
  }

  fclose(fh);
  return ar;
}

/* nice to have:
 * a simple way to create a dot file for graphviz viewer.
 * a better way could be the usage of the proper graphviz
 *   library functions.
 * this is the wrapper file to the tree function.
 */
void write_dot_file(BTREE bt,char* fn) {
    FILE* fh = NULL;
    if (NULL == (fh = fopen(fn,"w"))) {
        printf("Cannot open for writing; %s",fn);
        exit(EXIT_FAILURE);
    }
    print_tree_to_dot_file(fh,bt);
    fclose(fh);
    printf("Tree has been written into: %s\n",fn);
}

/* tree functions */
/* create a new (unconnected) node with given data */
BTREE new_node(DATA data) {
    BTREE bt = malloc(sizeof(NODE));
    bt->data = data;
    bt->left = bt->right = NULL;
    return bt;
}

/* if you already have a tree defined, you can use this
 * to add a new node 
 */
void insert_node(BTREE* bt,DATA data) {
    if(!*bt) {
        *bt = new_node(data);
    } else if (data < (*bt)->data) {
        insert_node(&(*bt)->left,data);
    } else if (data > (*bt)->data) {
        insert_node(&(*bt)->right,data);
    } else
        fprintf(stderr,"omitted: %u\n",data); /* identical numbers are discarded */
}

static void arr2btree(ARRAY ar, INDEX num, BTREE* bt) {
    if (num < ar->length) {
        insert_node(bt,view_array_item(ar,num));
        arr2btree(ar,num+1,bt);
    }
}

/* read in all the elements of an ARRAY into a binary tree */
BTREE array_to_btree(ARRAY ar) {
    BTREE bt = NULL;
    if (ar->length > 0)
        arr2btree(ar,0,&bt);
    return bt;
}

/* print out the nodes in order */
void inorder_traversal(BTREE bt) {
    if (!bt)
        return;
    inorder_traversal(bt->left);
    printf("%u ",bt->data);
    inorder_traversal(bt->right);
}

/* internal for binary tree only */
static void inorder_to_queue(BTREE bt) {
    if (!bt)
        return;
    inorder_to_queue(bt->left);
    push_front(bt);
    inorder_to_queue(bt->right);
}

/* uses an internal queue */
ARRAY tree_to_ordered_array(BTREE bt) {
    inorder_to_queue(bt);
    return flush_queue_to_array();
}

/* prints the edges with node pairs */
void print_tree(FILE* fh,BTREE bt) {
    if (!bt)
        return;
    if (bt->left) {
        fprintf(fh,"\t%u -> %u;\n",bt->data,bt->left->data);
        print_tree(fh,bt->left);
    }
    if (bt->right) {
        fprintf(fh,"\t%u -> %u;\n",bt->data,bt->right->data);
        print_tree(fh,bt->right);
    }
}

/* print out the dot file to the given filehandler
 * stdout will print it to terminal.
 */
void print_tree_to_dot_file(FILE* fh, BTREE bt) {
    fprintf(fh,"digraph g {\n");
    if (!bt->left && !bt->right)
        fprintf(fh,"\t%u;\n",bt->data);
    else
        print_tree(fh,bt);
    fprintf(fh,"}\n");
}

void destroy_tree(BTREE* bt) {
    if((*bt)->right)
        destroy_tree(&(*bt)->right);
    if((*bt)->left)
        destroy_tree(&(*bt)->left);
    free(*bt);
}

/* array functions */
ARRAY create_array(INDEX length) {
    ARRAY out_array = malloc(sizeof(Array));
    out_array->values = calloc(length,sizeof(DATA));
    out_array->length = 0;
    out_array->maxlength = length;
    return out_array;
}

/* value is only added if the array is not full */
void add_value_to_array(ARRAY* ar,DATA val) {
    if((*ar)->length != (*ar)->maxlength) {
        (*ar)->values[(*ar)->length] = val;
        (*ar)->length++;
    } else
      fprintf(stderr,"Array is full.\n");
}

DATA view_array_item(ARRAY ar,INDEX i) {
    if (i >= ar->maxlength)
        return DATAMAX;
    return(ar->values[i]);
}

void destroy_array(ARRAY* ar) {
  free((*ar)->values);
  free(*ar);
  *ar = NULL;
}

/* given a file handler, the array is printed out. */
void write_array_to_file(ARRAY ar,FILE* fh) {
  fprintf(fh,PRINTDATASTR PRINTSEP,ar->length);
  for(INDEX i = 0; i < ar->length; ++i)
    fprintf(fh,PRINTDATASTR PRINTSEP,ar->values[i]);
  putc('\n',fh);
}

/* a wrapper for terminal printing */
void print_array(ARRAY ar) {
    write_array_to_file(ar,stdout);
}

/* queue functions: used by binary tree only */
static li* head = NULL;
static li* tail = NULL;
static INDEX list_length = 0;

void push_front(BTREE bt) {
    li* an_item = calloc(1,sizeof(li));
    an_item->btree = bt;
    if (head)
        head->prev = an_item;
    an_item->next = head;
    an_item->prev = NULL;
    if (!tail)
        tail = an_item;
    head = an_item;
    ++list_length;
}

BTREE pop_back(void) {
    if(tail == NULL)
        return NULL;
    BTREE ret_data = tail->btree;
    li* saved_tail = tail;
    if (head == tail)
        head = NULL;
    tail = tail->prev;
    if (tail)
        tail->next = NULL;
    free(saved_tail);
    --list_length;
    return ret_data;
}

ARRAY queue_data_to_array() {
    if (!tail)
        return NULL;
    ARRAY out_array = create_array(list_length);
    for(li* temp = tail; temp->prev; temp = temp->prev) {
        add_value_to_array(&out_array,temp->btree->data);
    }

    return out_array;
}

/* all items to ARRAY, and this empties the queue */
ARRAY flush_queue_to_array() {
    ARRAY out_array = create_array(list_length);
    while(tail)
        add_value_to_array(&out_array,pop_back()->data);
    return out_array;
}

INDEX get_queue_length(void) {
    return list_length;
}

bool queue_is_empty(void) {
    return(head == NULL);
}
