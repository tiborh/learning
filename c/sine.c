/*******************************************************************************
   Author: tiberius
   Version: 0.1
   Original file name: sine.c
   Date: 2019-11-03
   Compiler used for testing: gcc version 8.3.0 (Raspbian 8.3.0-6+rpi1)

   Task Description
   ----------------
   Write a program that can give the sine of a value between 0 and 1 (non
   inclusive). You will be graded based on whether the program can output a
   value in the correct range and whether your code is well-formatted and 
   logically correct.

   Extra functionality
   -------------------
   If a command line argument is given, the program interprets it as the
   input value and does not ask input from user.
*******************************************************************************/
#include <stdio.h>
#include <math.h>		/* do not forget -lm at compilation time */
#include <assert.h>

int main (int argc, char** argv) {              /* argc: length of argv array
						   argv: the argument array */
  double input_value = 0;
  printf("Input value (in radians) (0 < input < 1): ");
  if (argc > 1) {	                        /* there is at least one 
						   command line argument */
    sscanf(argv[1],"%lf",&input_value);         /* argv[0]: program name, 
						   argv[1]: first argument */
    printf("%f\n", input_value);		/* for a uniform look of the
						   output */
  } else {
    scanf("%lf",&input_value);
  }
  assert(input_value > 0 && input_value < 1);

  printf("The sine is: %g\n",sin(input_value)); /* if the number is too small
						   it's best if scientific 
						   notation is used */

  return 0;
}
