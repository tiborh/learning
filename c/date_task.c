/*******************************************************************************
 * Author: Tibor
 * Version: 0.1
 * Date: 2019-12-16
 * 
 * Assignment (Option 1):
 * ======================
 * 0. Write enumerated types that supports dates—such as december 12.
 *    Use a struct with two members; one is the month and the second  is the day 
 *    of the month—an int (or short).
 *    typedef enum month{ jan, feb, …} month;
 *    typedef struct date{ month m; int d} date;
 * 1. Then add a function that produces a next day.
 *    So _nextday(date)_ of december 12 is december 13. 
 *    The function can assume that February has 28 days and it most know how 
 *    many days are in each month. Use a struct with two members; one is the 
 *    month and the second  is the day of the month—an int (or short).
 * 2. Also write a function _printdate(date)_ that prints a date legibly.
 * 3. Then print out the date 
 *    * January 1 (dt0) and
 *    * print the next day Jan 2.
 * 4. Do this for the following dates:
 *    * February 28, (dt1)
 *    * March 14, (dt2)
 *    * October 31, (dt3) and 
 *    * December 31 (dt4).
 *******************************************************************************/

#include <stdio.h>
#include <assert.h>		/* for simple checks */
#include <string.h>		/* for str operations */
#include <stdlib.h>		/* for calloc */

#define MAX_DAY_LENGTH 2
#define NUM_OF_MONTHS 12
#define NUM_OF_TEST_DATA 5
#define MONTH_NAMES month_names_long_en

/* the two typedefs as recommended in the assignment text: */
typedef enum month{ Jan, Feb, Mar, Apr, May, Jun,
		    Jul, Aug, Sep, Oct, Nov, Dec } month;
typedef struct date{ month m; short d; } date;

/* month lengths stored in an array of structures */
const date year[NUM_OF_MONTHS] = {{ Jan, 31 }, { Feb, 28 }, {Mar, 31}, {Apr, 30},
				  { May, 31 }, { Jun, 30 }, {Jul, 31}, {Aug, 31},
				  { Sep, 30 }, { Oct, 31 }, {Nov, 30}, {Dec, 31},
};

/* to help multilingualism, the month names are in separate string array: */
char* month_names_long_en[NUM_OF_MONTHS] = {"January","February","March","April",
					    "May","June","July","August","September",
					    "October","November","December",
};

/* Test data from assignment: */
const date d0 = {Jan, 1};
const date d1 = {Feb, 28};
const date d2 = {Mar, 14};
const date d3 = {Oct, 31};
const date d4 = {Dec, 31};

void printdate(date);		/* good for debugging */

/* need to watch for:
 * end of month: set next date to first day of month
 * end of year: skip to first (index zero in array) month of year
 */
date nextday(date d) {
  int days_in_month = year[d.m].d;
  date next_date = { d.m, d.d }; /* we would not like garbage in case of a mishap */
  if(d.d == days_in_month) {
    next_date.d = 1;
    if(d.m == NUM_OF_MONTHS - 1)
      next_date.m = 0;
    else
      next_date.m = d.m + 1;
  } else
    next_date.d = d.d + 1;
  return(next_date);
}

/* convert enum names to strings from month names table */
/* (char *) cast is needed to avoid the warning:
   "return discards ‘const’ qualifier from pointer target type"
 */
char* get_month_str(month m) {
  return(MONTH_NAMES[m]);
}

/* for the test function, it is easier if string can be compared */
/* char* get_date_str(date d) { */
/*   char* day_str = calloc(MAX_DAY_LENGTH + 1, sizeof(char)); */
/*   char* mth_str = get_month_str(d.m); */
/*   char* sep = " "; */
/*   sprintf(day_str,"%d",d.d);	/\* convert number to string *\/ */
/*   int out_len = strlen(day_str) + strlen(mth_str) + strlen(sep); */
  
/*   /\* allocate memory for the output string: *\/ */
/*   char* out_str = calloc(out_len + 1,sizeof(char)); */
/*   sprintf(out_str,"%s%s%s",mth_str,sep,day_str); /\* concatenation *\/ */
  
/*   return out_str; */
/* } */

char* get_date_str(date d) {
  static char out_str[20];
  sprintf(out_str, "%s %d", MONTH_NAMES[d.m], d.d);
  return out_str;
} 

/* prints out the string prepared in another function */
void printdate(date d) {
  puts(get_date_str(d));
}

void test_assignment(void) {
  /* this one could be more open to multilinguality too */
  assert(!strcmp(get_date_str(nextday(d0)),"January 2"));
  assert(!strcmp(get_date_str(nextday(d1)),"March 1"));
  assert(!strcmp(get_date_str(nextday(d2)),"March 15"));
  assert(!strcmp(get_date_str(nextday(d3)),"November 1"));
  assert(!strcmp(get_date_str(nextday(d4)),"January 1"));
}

int main (int argc, char** argv) {

  test_assignment();
  /* make it less repetitive: */
  date test_dates[NUM_OF_TEST_DATA] = {d0,d1,d2,d3,d4};
  for (int i = 0; i < NUM_OF_TEST_DATA; ++i) {
    printdate(test_dates[i]);
    printdate(nextday(test_dates[i]));
  }
  
  return 0;
}

