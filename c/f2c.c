#include <stdio.h>
#include <math.h>
#include <libgen.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

int main (int argc, char** argv) {
  char* help_txt = "an argument is needed: a temperature value\n";
  if (argc < 2) {
    puts(help_txt);
    exit(1);
  }
  char* fahr2cels = "f2c";
  char* cels2fahr = "c2f";
  char* unknown = "Unknown operation.\n";
  const double temp_coeff = 9 / 5.0;
  const int temp_const = 32;
  char* fn = basename(argv[0]);
  int is_f2c = strcmp(fahr2cels,fn);
  int is_c2f = strcmp(cels2fahr,fn);
  double temp_to_convert;
  sscanf(argv[1],"%lf",&temp_to_convert);
  if (0 == is_f2c)
    printf("%lf\n",(temp_to_convert - temp_const) / temp_coeff);
  else if (0 == is_c2f)
    printf("%lf\n",temp_to_convert * temp_coeff + temp_const);
  else
    puts(unknown);

  return 0;
}
