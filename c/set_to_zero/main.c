#include "set_to_zero.h"

int main (int argc, char** argv) {
  if (argc == 1) {
    help(argv[0]);
    exit(1);
  }
  
  char* end;
  int inp = strtoimax(argv[1],&end,base);
  int shwth = argc > 2 ? strtoimax(argv[2],&end,base) : shift_with;

  printf("read:\n\t");
  print_result(inp);
  printf("transformed:\n\t");
  print_result(~(shift_num << shwth) & inp);
  return 0;
}
