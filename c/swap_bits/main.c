#include "swap_bits.h"

// copy a to b where m == 1
// take all from a that correspond to the mask
// take all from b that do not correspond to the mask

int main (int argc, char** argv) {
  if (argc < 5) {
    help(argv[0]);
    exit(1);
  }
  
  char* end;
  int base = strtoimax(argv[1],&end,10);
  int num = strtoimax(argv[2],&end,base);
  int ind1 = strtoimax(argv[3],&end,10);
  int ind2 = strtoimax(argv[4],&end,10);

  printf("input:\n\t");
  print_result(num);
  unsigned frst_bit = (num >> ind1) & 1;
  unsigned scnd_bit = (num >> ind2) & 1;
  printf("first bit:\n\t");
  print_result(frst_bit);
  printf("second bit:\n\t");
  print_result(scnd_bit);
  
  unsigned xor_bit = frst_bit ^ scnd_bit;
  printf("xor bit:\n\t");
  print_result(xor_bit);
  printf("result:\n\t");
  unsigned res = num ^ ((xor_bit<<ind1) | (xor_bit<<ind2));
  print_result(res);
  puts("for some, it works, for others it does not do not use!");
  return 0;
}
