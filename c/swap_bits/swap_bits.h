#ifndef SWAP_BITS_H_INCLUDED
#define SWAP_BITS_H_INCLUDED

#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <inttypes.h>		/* strtoimax */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */
#include "dec2bin.h"

//static const int base = 10;
void help(char*);
void print_result(int);

#endif //SWAP_BITS_H_INCLUDED
