#include "swap_bits.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <base (2,8,10,16)> <int> <index 1> <index 2>\n",basename(argv0));
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}
