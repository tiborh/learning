#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int is_even(int num) {
  return(num == (num/2)*2);
}

void reverse(char* inch) {
  for(int i = 0, j = strlen(inch)-1; i < j; ++i,--j) {
    char t = *(inch + i);
    *(inch + i) = *(inch + j);
    *(inch + j) = t;
  }
}

char* int_to_bin(int num) {
  int length = sizeof(int) * 8;
  //printf("length: %d\n",length);
  char* binstr = calloc(length+1,sizeof(char));
  int i;
  for(i = 0; i < length && num != 0; ++i,num>>=1) {
    //printf("num == %d\n",num);
    *(binstr + i) = is_even(num) ? '0' : '1';
  }
  *(binstr + i) = '\0';
  reverse(binstr);
  
  return binstr;
}

int main (int argc, char** argv) {
  if (argc < 2)
    puts("First arg is converted from deimal to binary.");
  else {
    char* result;
    result = int_to_bin(atoi(argv[1]));
    printf("%s\n",result);
  }

  return 0;
}
