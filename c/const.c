#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  const int a;	/* if you do not do it here, you cannot do it later */
  a = 333;	/* const.c:14:5: error: assignment of read-only variable ‘a’ */
  printf("%d\n",a);
  return 0;
}
