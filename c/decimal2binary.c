#include <stdio.h>
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
#include <stdlib.h>		/* for exit,abs */
#include "list.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <string of decimal digits>\n",basename(argv0));
  exit(EXIT_FAILURE);
}

unsigned char* stack2str(void) {
  unsigned int outlen = get_length();
  unsigned char* outstr = calloc(outlen+1,sizeof(unsigned char));
  unsigned int i = 0;
  for(; i < outlen; ++i)
    outstr[i] = pop_front() + '0';
  outstr[i] = '\0';
  return outstr;
}

unsigned char* dec2bin(char* decstr) {
  int dec = atoi(decstr);
  while(dec) {
    push_front(dec % 2);
    dec >>= 1;
  }
  return stack2str();
}

int main (int argc, char** argv) {
  if (argc < 2) {
    help(argv[0]);
  }
  int prefix = argv[1][0] == '-' ? -1 : 1;
  char* decstr = prefix == 1 ? argv[1] : argv[1] + 1;
  unsigned char* binstr = dec2bin(decstr);
  printf("%s\n",binstr);
  free(binstr);
  
  return 0;
}
