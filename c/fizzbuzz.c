#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

const int fizznum = 3;
const int buzznum = 5;
const char* fizzstr = "fizz";
const char* buzzstr = "buzz";
const int defnum = 0;

void fizzbuzz(char* fbstr, int n) {
  if (n % fizznum == 0)
    strcat(fbstr,fizzstr);
  if (n % buzznum == 0)
    strcat(fbstr,buzzstr);
  if (fbstr[0] == '\0')
    sprintf(fbstr,"%d",n);

  return;
}

int main (int argc, char** argv) {
  const int STARTNUM = 1;
  const int MAXNUM = 100;

  int max_num = argc < 2 ? MAXNUM : atoi(argv[1]);
  int start_num = argc < 3 ? STARTNUM : atoi(argv[2]);

  int fizzbuzzlen = strlen(fizzstr) + strlen(buzzstr) + 1;
  char* fizzbuzzstr = calloc(fizzbuzzlen,sizeof(char));

  for(int i = start_num; i <= max_num; ++i) {
    fizzbuzzstr[0] = '\0';
    fizzbuzz(fizzbuzzstr,i);
    puts(fizzbuzzstr);
  }

  free(fizzbuzzstr);
  
  return 0;
}
