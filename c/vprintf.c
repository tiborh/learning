#include <stdio.h>
#include <stdarg.h>

// source:
// https://www.tutorialspoint.com/c_standard_library/c_function_vprintf.htm

void WriteFrmtd(char *format, ...) {
   va_list args;
   
   va_start(args, format);
   vprintf(format, args);
   va_end(args);
}

int main () {
   WriteFrmtd("%d variable argument\n", 1);
   WriteFrmtd("%d variable %s\n", 2, "arguments");
   
   return(0);
}
