#include <stdio.h>
#include <stdlib.h>		/* for exit,abs */
#include <ctype.h>
#include <string.h>

#define DEBUG 1

const size_t MAX_LINE_LENGTH = 80;

void copy_array(char* from, char* to, size_t size) {
  for (size_t i = 0; i < size; ++i)
    *(to + i) = *(from + i);
}

size_t trim_end(size_t slen, char* a_str) {
  for(int i = slen-1; i >= 0; --i)
    if(isspace(*(a_str + i)))
      *(a_str + i) = '\0';
    else
      break;
  
  return strlen(a_str);
}

char* getline() {
  size_t maxlinelength = MAX_LINE_LENGTH;
  char* the_line = calloc(maxlinelength + 1, sizeof(char));
  size_t i = 0;
  int c = getchar();
  if (c == EOF)
    return NULL;
  for (; c != '\n' && c != EOF && i < maxlinelength; c = getchar(), ++i) {

    *(the_line + i) = c;

    if (i + 1 == maxlinelength && c != EOF) {
      maxlinelength *= 2;
      char* new_line = calloc(maxlinelength + 1, sizeof(char));
      copy_array(the_line,new_line,maxlinelength/2);
      free(the_line);
      the_line = new_line;
    }
  }

  if (c > 0 && c != '\n')
    ungetc(c,stdin);
  
  if (*(the_line + i - 1) == '\n')
    *(the_line + i - 1) = '\0';
  
  int slen = strlen(the_line);
  trim_end(slen,the_line);
  
  return the_line;
}

int main (int argc, char** argv) {

  char* a_line;
  while(NULL != (a_line = getline())) {
    printf("%s",a_line);
    if (DEBUG)
      putchar('|');
    putchar('\n');
  }

  return 0;
}
