#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  srand(time(NULL));
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

static int cmp_ints(const void *p1, const void *p2) {
  return(*((int*)p1)-*((int*)p2));
}

int main (int argc, char** argv) {
  int array_length = 10;
  int value_ceiling = 100;
  if (argc == 1)
    printf("%s [length (%d)] [ceiling (%d)]\n",basename(argv[0]),array_length,value_ceiling);
  if (argc > 1)
    sscanf(argv[1],"%d",&array_length);
  assert(array_length > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&value_ceiling);
  assert(value_ceiling > 1);
  int* arr = calloc(array_length,sizeof(int));

  fill_array(array_length,arr,value_ceiling);
  print_array(array_length,arr);
  qsort(arr,array_length,sizeof(int),cmp_ints);
  puts("sorted:");
  print_array(array_length,arr);
  
  free(arr);
  return 0;
}
