#include "dec2bin.h"

static unsigned char* stack2str(void) {
  unsigned int outlen = get_length();
  unsigned char* outstr = calloc(outlen+1,sizeof(unsigned char));
  unsigned int i = 0;
  for(; i < outlen; ++i) {
    unsigned char data = pop_front();
    if (data != '-')
      data += '0';
    outstr[i] = data;
  }
  outstr[i] = '\0';
  return outstr;
}

unsigned char* dec2bin(int dec) {
  if (dec < 0) {
    push_front('-');
    dec *= -1;
  }
  while(dec) {
    push_front(dec % 2);
    dec >>= 1;
  }
  return stack2str();
}

int bin2dec(char* binstr) {
  int decnum = 0;
  int len = strlen(binstr);
  int bin_multiplier = 1;

  for (int i = len-1; i >= 0; --i) {
    decnum += (binstr[i]-'0') * bin_multiplier;
    bin_multiplier *= 2;
  }
    
  return decnum;
}
