#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  char teststr[7] = "abcdefg";	/* one off if handled as string */
  printf("length: %lu\n",strlen(teststr)); /* 7, so where is the /0 char? */
  /* valgrind --track-origins=yes ./strlen
     ==11394== Conditional jump or move depends on uninitialised value(s)
     ==11394==    at 0x484E730: strlen (vg_replace_strmem.c:459)
     ==11394==    by 0x1088BB: main (strlen.c:17)
     ==11394==  Uninitialised value was created by a stack allocation
     ==11394==    at 0x108874: main (strlen.c:15)
   */
    
  return 0;
}
