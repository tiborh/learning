#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
#include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX

int main (int argc, char** argv) {
  unsigned num_of_nums = (argc < 2) ? NUM_OF_NUMS : atoi(argv[1]);
  unsigned upper_bound = (argc < 3) ? UPPER_BOUND : atoi(argv[2]);
  srand(time(NULL));
  unsigned numwidth = log10(upper_bound) + 1;
  //printf("%u\n",numwidth);
  for(unsigned i = 0; i < num_of_nums; ++i)
    printf("%*u\n",numwidth,rand() % upper_bound);
  //putchar('\n');
  return 0;
}
