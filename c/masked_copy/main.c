#include "masked_copy.h"

// copy a to b where m == 1
// take all from a that correspond to the mask
// take all from b that do not correspond to the mask

int main (int argc, char** argv) {
  if (argc < 5) {
    help(argv[0]);
    exit(1);
  }
  
  char* end;
  int base = strtoimax(argv[1],&end,10);
  int a = strtoimax(argv[2],&end,base);
  int b = strtoimax(argv[3],&end,base);
  int m = strtoimax(argv[4],&end,base);

  printf("read:\n");
  printf("\ta: ");
  print_result(a);
  printf("\tb: ");
  print_result(b);
  printf("\tm: ");
  print_result(m);
  printf("transformed:\n\t");
  print_result((b & m) | (a & ~m));
  return 0;
}
