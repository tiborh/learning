#include "masked_copy.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <base (2,8,10,16)> <a> <b> <mask>\n",basename(argv0));
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}
