#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
#include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  clock_t begin,end,diff;
  int max = 1000;
  begin = clock();
  end = clock();
  diff = end - begin;
  printf("Time spent (between two assignments) %ld ms\n",diff);
  begin = clock();
  for (int i = 0; i < 1000; ++i);
  end = clock();
  diff = end - begin;
  begin = clock();
  printf("Time spent (for looping %d) %ld ms\n",max,diff);
  end = clock();
  diff = end - begin;
  printf("Time spent (during previous printf) %ld ms\n",diff);
  printf("Same in second: %lf\n",(double)diff/CLOCKS_PER_SEC);
  printf("CLOCKS_PER_SEC: %lu\n",CLOCKS_PER_SEC);

  return 0;
}
