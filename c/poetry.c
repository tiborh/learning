#include <stdio.h>

/* the poem in an array of character strings */
char* the_poem[] = {
		   "It's as if you'd woken in a locked cell and found",
		   "in your pocket a slip of paper, and on it a single sentence",
		   "in a language you don't know.",
		   "",
		   "And you'd be sure this sentence was the key to your",
		   "life. Also to this cell.",
		   "",
		   "And you'd spend years trying to decipher the sentence,",
		   "until finally you'd understand it. But after a while",
		   "you'd realize you got it wrong, and the sentence meant",
		   "something else entirely. And so you'd have two sentences.",
		   "",
		   "Then three, and four, and ten, until you'd created a new language.",
		   "",
		   "And in that language you'd write the novel of your life.",
		   "And once you'd reached old age you'd notice the door of the cell",
		   "was open. You'd go out into the world. You'd walk the length and breadth of it,",
		   "",
		   "until in the shade of a massive tree you'd yearn",
		   "for that one single sentence in a language you don't know.",
		   "",
		   "Sentence",
		   "By Tadeusz Dąbrowski",
		   "(Translated, from the Polish, by Antonia Lloyd-Jones.)",
		   "from:",
		   "https://www.newyorker.com/magazine/2019/07/22/sentence"
};

int poem_length = sizeof(the_poem)/sizeof(the_poem[0]);

int main (int argc, char** argv) {
  /* lines one by one */
  for(int i = 0; i < poem_length; ++i) {
    printf("%s\n",the_poem[i]);
  }
  return 0;
}
