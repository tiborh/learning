#include <stdio.h>
#include <ctype.h>
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  int c = 0;			/* if char, needs to look out for 0xff to avoid infinite loop */
  // int prevc = 0;
  int chars = 0, lines = 0, words = 0;
  bool in_word = false;
  while((c = getchar()) != EOF) {
    if(isalpha(c)) {
      if(!in_word) {
	//putchar(c);
	++words;
	in_word = true;
      }
    } else if (isspace(c))
      if (in_word) {
	// putchar('-');
	// putchar(prevc);
	// putchar(',');
	in_word = false;
      }
    ++chars;
    if (c == '\n')
      ++lines;
    // putchar(c);
    // prevc = c;
  }
  //putchar('\n');
  printf("linebreaks: %d\n",lines);
  printf("words: %d\n",words);
  printf("characters: %d\n",chars);
  
  return 0;
}
