#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <libgen.h>		/* for basename and dirname */

size_t* make_tests(size_t num, size_t out) {
  size_t *results = calloc(out,sizeof(size_t));
  for (size_t i = 0; i < num; ++i)
    results[rand() % out]++;
  return results;
}

void print_results(size_t num, size_t* results, size_t min, size_t ssize) {
  for (size_t i = 0; i < num; ++i) {
    printf("%lu: %lu (~%5.2f%%)\n",i+min,results[i],100.0*results[i]/ssize);
  }
}

int main(int argc, char** argv) {
  size_t outcomes = 2;
  size_t sample_size = 1024;
  size_t start_val = 0;
  srand(time(NULL));   // Initialization, should only be called once.
  // rand() Returns a pseudo-random integer between 0 and RAND_MAX.
  if (argc == 1)
    printf("usage: %s [sample size (default: %lu)] [num of outcomes (default: %lu)] [starting value (default %lu)]\n",basename(argv[0]),sample_size,outcomes,start_val);
  if (argc > 1)
    sscanf(argv[1],"%lu",&sample_size);
  if (argc > 2)
    sscanf(argv[2],"%lu",&outcomes);
  if (argc > 3)
    sscanf(argv[3],"%lu",&start_val);

  printf("sample size: %lu\n",sample_size);
  size_t* res = make_tests(sample_size,outcomes);
  print_results(outcomes,res,start_val,sample_size);

  free(res);
  return 0;
}
