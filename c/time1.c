#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  time_t t = time(NULL);
  printf("time(NULL): %ld\n",t);
  printf("ctime(time(NULL)): %s\n",ctime(&t)); /* ctime == convert time */
  time_t t1 = (time_t)0;
  printf("t1: %ld\n",t1);
  printf("ctime(0): %s\n",ctime(&t1)); /* ctime == convert time */
  
  time_t tmax = LONG_MAX;
  printf("maxtime: %ld\n",tmax);
  printf("ctime(&maxtime): %s\n",ctime(&tmax));
  time_t changer = 2;
  time_t differ = tmax/changer;
  char* a_time = ctime(&tmax);
  //  for(int i = 0; i < 10; ++i) {
  while(differ) {
    for (; !a_time && tmax && differ && changer; tmax-=differ,a_time=ctime(&tmax),differ/=changer) {
    }
    printf("maxtime: %ld\n",tmax);
    printf("ctime(&maxtime): %s\n",ctime(&tmax));
    printf("changer: %ld\n",changer);
    printf("differ: %ld\n",differ);
    changer*=2;
    differ/=changer;
    for (; a_time && tmax && differ && changer; tmax+=differ,a_time=ctime(&tmax)) {
    }
  }
  printf("maxtime: %ld\n",tmax);
  printf("ctime(&maxtime): %s\n",ctime(&tmax));
  double time_to_wait = difftime(tmax,time(NULL));
  printf("difftime(maxtime,time(NULL)): %lf\n",time_to_wait);
  time_t mins = 60;
  time_t hours = mins * 60;
  time_t days = hours * 24;
  time_t years = days * 365.25;
  time_t wyears = time_to_wait / years;
  time_t wdays = (time_to_wait - (wyears * years)) / days;
  printf("time to wait: %ld years and %ld days\n",wyears,wdays);
  return 0;
}
