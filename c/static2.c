#include <stdio.h>
#include <time.h>
#include <unistd.h>		/* for sleep */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */

time_t time_since() {
  static time_t previous = 0;
  if (previous == 0)
    previous = time(NULL);
  time_t now = time(NULL);
  time_t elapsed = now - previous;
  now = previous;
  return elapsed;
}

int main (int argc, char** argv) {
  const char* time_since_txt = "time since last called: %lds\n";
  for (int i = 0; i < 10; ++i) {
    printf(time_since_txt,time_since());
    sleep(1);
  }
  return 0;
}
