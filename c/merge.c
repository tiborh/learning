#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

static int cmp_ints(const void *p1, const void *p2) {
  int nu1 = *((int*)p1);
  int nu2 = *((int*)p2);
  return(nu1-nu2);
}

void merge_arrays(int len1, int* arr1, int len2, int* arr2, int lenm, int* arrm) {
  /* printf("len1: %d\n",len1); */
  /* printf("len2: %d\n",len2); */
  for(int i1 = 0, i2 = 0, im = 0; im < lenm; ++im) {
    if (i1 == len1) {
      *(arrm + im) = *(arr2 + i2++);
      /* printf("(1) from arr2[%d] : arrm[%d] == %d\n",i2-1,im,*(arr2 + i2 - 1)); */
    } else if (i2 == len2) {
      *(arrm + im) = *(arr1 + i1++);
      /* printf("(2) from arr1[%d]: arrm[%d] == %d\n",i1-1,im,*(arr1 + i1 - 1)); */
    } else if (*(arr2 + i2) < *(arr1 +i1)) {
      *(arrm + im) = *(arr2 + i2++);
      /* printf("(3) from arr2[%d] : arrm[%d] == %d\n",i2-1,im,*(arr2 + i2 - 1)); */
    } else if (*(arr1 + i1) <= *(arr2 +i2)) {
      *(arrm + im) = *(arr1 + i1++);
      /* printf("(4) from arr1[%d]: arrm[%d] == %d\n",i1-1,im,*(arr1 + i1 - 1)); */
    }
  }
}

int main (int argc, char** argv) {
  int length_ceiling = 10;
  int value_ceiling = 100;
  if (argc == 1)
    printf("%s [length (%d)] [ceiling (%d)]\n",basename(argv[0]),length_ceiling,value_ceiling);
  if (argc > 1)
    sscanf(argv[1],"%d",&length_ceiling);
  assert(length_ceiling > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&value_ceiling);
  assert(value_ceiling > 1);

  srand(time(NULL));
  int arr_len1 = (rand() % length_ceiling);
  int arr_len2 = (rand() % length_ceiling);
  int arr_lenm = arr_len1 + arr_len2;
  int* arr1 = calloc(arr_len1,sizeof(int));
  int* arr2 = calloc(arr_len2,sizeof(int));
  int* arrm = calloc(arr_lenm,sizeof(int));

  fill_array(arr_len1,arr1,value_ceiling);
  fill_array(arr_len2,arr2,value_ceiling);
  qsort(arr1,arr_len1,sizeof(int),cmp_ints);
  qsort(arr2,arr_len2,sizeof(int),cmp_ints);
  puts("The arrays:");
  print_array(arr_len1,arr1);
  print_array(arr_len2,arr2);
  merge_arrays(arr_len1,arr1,arr_len2,arr2,arr_lenm,arrm);
  puts("Merged:");
  print_array(arr_lenm,arrm);
  
  free(arr1);
  free(arr2);
  free(arrm);
  return 0;
}
