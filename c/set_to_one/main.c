#include "set_to_one.h"

int main (int argc, char** argv) {
  if (argc == 1) {
    help(argv[0]);
    exit(1);
  }
  
  char* end;
  int inp = strtoimax(argv[1],&end,base);
  int shnu = argc > 2 ? strtoimax(argv[2],&end,base) : shift_num;

  printf("read:\n\t");
  print_result(inp);
  printf("transformed:\n\t");
  print_result(set_to_nu << shnu | inp);
  return 0;
}
