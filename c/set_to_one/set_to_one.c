#include "set_to_one.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <integer> [place to set (default: %d)]\n",basename(argv0),shift_num);
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}
