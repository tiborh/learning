/********************************************************************************
 Author:  tiberius
 Version: 0.1
 Date:    2019-11-04

 Task description:
 -----------------
 Write a C program that has a function that prints a table of values for sine and
 cosine between (0, 1). You will upload your program as a text file.

 Missing from description:
 -------------------------
 + does (0,1) mean inclusive or non-inclusive?
 + is 0 and 1 radian or degree?
 + what increment?
 + number of decimals to display?

 Implementation workaround of missing specification:
 ---------------------------------------------------
 + others said that (0,1) means non-exclusive, so this is how it is interpreted
 + it is assumed that the input is radian because of the narrow range
 + by default, increment will be 0.1
   first command line argument, if given is assumed to be the increment value
   user input increment value must be between 0 (non-inclusive) and 0.5 
   (non-inclusive)
 + by default, six decimals are displayed but a second command line argument can
   modify that. number of decimals is assumed to be greater than 2.

 ********************************************************************************/

#include <stdio.h>
#include <assert.h>	   /* for assert() statements */
#include <math.h>	   /* for sin() and cos()
			      do not forget -lm at compilation time */
#include <libgen.h>	   /* for basename() */
#include <string.h>	   /* for strlen() */

/* constants and parameters */
const double lower_limit = 0.0;	/* radian */
const double upper_limit = 1.0;	/* radian */
const char* table_gap = "    ";
const char tline_char = '-';
double increment = 0.1;
int number_of_decimals = 6;
int col_sep_width;
int table_width;
int column_width;

/* brief overview of possible command line parameters */
void usage(char* fullpath) {
  printf("%s [increment of radians] [number of decimals]\n",basename(fullpath));
}

int main (int argc, char** argv) {
  col_sep_width = strlen(table_gap);

  /* with first argument you can either ask for help or give the increment size */
  if (argc > 1) {
    if (0 == strcmp(argv[1],"--help")) {
      usage(argv[0]);
      return 0;
    } else {
      sscanf(argv[1],"%lf",&increment);
      assert(increment > 0 && increment < 0.5);
    }
  }

  /* with the second command line argument you can give the number of decimals to display */
  if (argc > 2)
    sscanf(argv[2],"%d",&number_of_decimals);
  assert(number_of_decimals > 2);

  /* certain parameters depend on the number of decimals */
  table_width = 2 * (col_sep_width + 2) + 3 * (number_of_decimals + 2);
  column_width = number_of_decimals + 2;

  /* table head: */
  printf("%*s %*s %*s\n",column_width,"radian",col_sep_width+column_width+1,"sine",col_sep_width+column_width+1,"cosine");
  for(int i = 0; i < table_width; ++i)
    putchar(tline_char);
  putchar('\n');
  
  /* table body: */
  for(double i = lower_limit + increment; i < upper_limit; i += increment) /* 0 and 1 are non-inclusive */
    printf("%.*f %s %.*f %s %.*f\n",number_of_decimals, i, table_gap, number_of_decimals, sin(i), table_gap, number_of_decimals, cos(i));

  return 0;
}
