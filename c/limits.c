#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {
  char c,c0;
  for (c = 1, c0 = 0; c0 < c; c0=c++);
  printf("maxval of char: %d\n",c0);
  for (c = 0, c0 = 1; c0 > c; c0=c--);
  printf("minval of char: %d\n",c0);

  signed char sc,sc0;
  for (sc = 1, sc0 = 0; sc0 < sc; sc0=sc++);
  printf("maxval of signed char: %d\n",sc0);
  for (sc = 0, sc0 = 1; sc0 > sc; sc0=sc--);
  printf("minval of signed char: %d\n",sc0);

  unsigned int u,u0;
  for (u = 1, u0 = 0; u0 < u; u0=u++);
  printf("maxval of unsigned int: %u\n",u0);
  for (u = 4, u0 = 5; u0 > u; u0=u--);
  printf("minval of unsigned int: %u\n",u0);
  
  return 0;
}
