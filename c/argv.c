#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

/* 
in gcc:
argv.c: In function ‘main’:
argv.c:23:12: warning: passing argument 2 of ‘foo’ from incompatible pointer type [-Wincompatible-pointer-types]
   23 |   foo(argc,argv);
      |            ^~~~
      |            |
      |            char **
argv.c:15:29: note: expected ‘const char **’ but argument is of type ‘char **’
   15 | void foo(int s,const char **p) {
      |                ~~~~~~~~~~~~~^

in clang:
argv.c:34:12: warning: passing 'char **' to parameter of type 'const char **' discards qualifiers in nested pointer types [-Wincompatible-pointer-types-discards-qualifiers]
  foo(argc,argv);
           ^~~~
argv.c:26:29: note: passing argument to parameter 'p' here
void foo(int s,const char **p) {
                            ^
1 warning generated.
 */
void foo(int s,const char **p) { /* pointer to pointer to const char */
  printf("size of p: %d\n",s);
  if (s > 1)
    printf("first: %s\n",*(p+1));
  return;
}

int main (int argc, char** argv) { /* pointer to pointer to char */
  foo(argc,argv);

  /* explanation:
     if:
     char c = 'c';
     char* pc = &c;
     const char* pc1;
     pc1 = pc; 			// legal
     pc = pc1;			// warning is produced
 */
  
  return 0;
}
