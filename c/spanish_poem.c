#include <stdio.h>
int main(void)
{   
    printf("\n Lagrima de preta, by Antonio Gedeao\n \n");
    printf("Encontrei uma preta\n");
    printf("que estava a chorar,\n");
    printf("pedi-lhe uma lagrima\n");
    printf("para a analisar.\n\n");
    printf("Recolhi a lagrima\n");
    printf("com todo o cuidado\n");
    printf("num tubo de ensaio\n");
    printf("bem esterilizado.\n\n");
    printf("Olhei-a de um lado,\n");
    printf("do outro e de frente:\n");
    printf("tinha um ar de gota\n");
    printf("muito transparente.\n\n");
    printf("Mandei vir os acidos,\n");
    printf("as bases e os sais,\n");
    printf("as drogas usadas\n");
    printf("em casos que tais.\n\n");
    printf("Ensaiei a frio,\n");
    printf("experimentei ao lume,\n");
    printf("de todas as vezes\n");
    printf("deu-me o que de costume:\n\n");
    printf("Nem sinais de negro,\n");
    printf("nem vestigios de odio.\n");
    printf("Agua (quase tudo)\n");
    printf("e cloreto de sodio.\n\n");

    return 0;
}
