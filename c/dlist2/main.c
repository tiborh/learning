#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <libgen.h>
#include <assert.h>
#include "dlist2.h"

#define DEBUG 0

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX
#define NUM_OF_TESTS 100

typedef DList*(*SortFunc)(DList*);
typedef unsigned (*SortingTestFunc)(SortFunc,unsigned,unsigned);
typedef enum Tests_ {nosort,insertsort,insertsort2,bubblesort} Tests;
const char* TestNames[] = {"empty function","insertion sort (two lists)","insertion sort (one list)", "bubble sort"};
const unsigned NUM_OF_TEST_FUNCS = 4;
typedef struct TestParameters_ {
    SortFunc* sortfuncs;
    unsigned num_of_sortfuncs;
    SortingTestFunc sorting_test_func;
    unsigned num_of_nums;
    unsigned upper_bound;
    unsigned num_of_tests;
    double* test_results;
} TestParameters;


void help(char* argv0) {
    puts("Usage:");
    printf("\t%s [number of numbers] [upper bound] [number of tests]\n",argv0);
}

void print_data(const void* data) {
    printf("%u ",*((unsigned *)data));
}
unsigned conv_data(const void* data) {
    if (!data)
        return UINT_MAX;
    return *((unsigned *)data);
}

void destroy_num(void* data) {
    free((unsigned*)data);
}

void fill_list(DList* dl,unsigned num_of_nums, unsigned upper_bound) {
    for(unsigned i = 0; i < num_of_nums; ++i) {
        unsigned* num = malloc(sizeof(unsigned));
        *num = rand() % upper_bound;
        if (0 > push_front(dl,(void *)num))
            exit(EXIT_FAILURE);
    }
}

long int cmp_data(const void* data0, const void* data1) {
    return((long)*((unsigned*)data1) - (long)*((unsigned*)data0));
}

long int cmp_list_elems(DListElem* elem0, DListElem* elem1) {
    return(cmp_data(elem0->data,elem1->data));
}

int merge_into(DList* dl, void* data) {
    DListElem* temp_elem = dl->head;
    while(temp_elem && cmp_data(data,temp_elem->data) < 0)
        temp_elem = temp_elem->next;
    if (!temp_elem)
        push_back(dl,data);
    else
        insert_as_prev(dl,temp_elem,data);

    return 0;
}

DList* nosortfunc(DList* dl){
    return dl;
}

DList* insertion_sort(DList* dl) {
    if (!dl)
        return NULL;
    if (dl->size < 2)
        return dl;
    DList* sorted_list = malloc(sizeof(DList));
    initialise_list(sorted_list,destroy_num,cmp_data);
    while(dl->head) {
        unsigned* num = NULL;
        pop_front(dl,(void **)&num);
        merge_into(sorted_list,num);
    }
    destroy_list(dl);
    free(dl);
    return sorted_list;
}

DList* insertion_sort2(DList* dl) {
    if (!dl)
        return NULL;
    if (dl->size < 2)
        return dl;

    DListElem* last_sorted = dl->head;
    for(DListElem* temp_elem = last_sorted->next; temp_elem; temp_elem = last_sorted->next) {
        disconnect_elem(dl,temp_elem);
            DListElem* place_seeker = last_sorted;
            while(place_seeker && cmp_data(place_seeker->data,temp_elem->data) <= 0) {
                place_seeker = place_seeker->prev;
            }
            if (!place_seeker) {
                connect_as_prev(dl,dl->head,temp_elem);
            } else {
                connect_as_next(dl,place_seeker,temp_elem);
                if(place_seeker == last_sorted)
                    last_sorted = temp_elem;
            }
    }

    return dl;
}

DList* bubble_sort(DList* dl) {
    if (!dl)
        return NULL;
    if (get_list_size(dl) <= 1)
        return dl;
    bool change_watch = true;
    for(int upper_bound = get_list_size(dl)-1; upper_bound > 1 && change_watch; --upper_bound) {
        change_watch = false;
        //printf("upper_bound: %d\n",upper_bound);
        DListElem* tmp_ptr = dl->head;
        for(int i = 0; i < upper_bound; ++i, tmp_ptr = tmp_ptr->next) {
            if (cmp_data(tmp_ptr->data,tmp_ptr->next->data) <= 0) {
                //printf("i: %d, data: ",i);
                //print_data(tmp_ptr->data);
                //printf(", next data: ");
                //print_data(tmp_ptr->next->data);
                //putchar('\n');
                swap_list_elem_data(tmp_ptr,tmp_ptr->next);
                change_watch = true;
            }
        }
    }

    return dl;
}

void print_array(long len,long* nums) {
    for(long i = 0; i < len; ++i)
        printf("%ld ",*(nums + i));
    putchar('\n');
}

double get_average(long len, long *nums) {
    double avg = 0.0;
    for (unsigned i = 0; i < len; ++i)
        avg += ((double)*(nums + i) - avg) / (double)(i+1);

    return avg;
}

DList* test_data_preparation(unsigned num_of_nums,unsigned upper_bound) {
    printf("\t(Preparing test data.)\n");
    DList* dl = malloc(sizeof(DList));
    initialise_list(dl,destroy_num,cmp_data);
    fill_list(dl,num_of_nums,upper_bound);

    return dl;
}

void* copy_data(const void* dtin) {
    unsigned* dtout = calloc(1,sizeof(unsigned));
    if (!dtout) {
        puts("copy_data: Cannot reserve memory for data.\n");
        return NULL;
    }
    *dtout = *((unsigned *)dtin);
    return((void*)dtout);
}

DList* copy_list(const DList* dl_in) {
    if (!dl_in) {
        puts("copy_list: input list is NULL.");
        return NULL;
    }
    if (!dl_in->size)
        puts("copy_list: input list is empty.");

    DList* dl_out = malloc(sizeof(DList));
    initialise_list(dl_out,destroy_num,cmp_data);
    for(DListElem* temp_elem = dl_in->head; temp_elem; temp_elem = temp_elem->next)
        push_back(dl_out,copy_data(temp_elem->data));
    return dl_out;
}

void test_engine(TestParameters* testpars) {
    /* space for test results */
    puts("Creating space for results.");
    long** search_times = malloc(testpars->num_of_sortfuncs*sizeof(long*));
    for (unsigned i = 0; i < testpars->num_of_sortfuncs; ++i)
        search_times[i] = calloc(testpars->num_of_tests,sizeof(long));

    /* the tests */
    for (unsigned testnu = 0; testnu < testpars->num_of_tests; ++testnu) {
        printf("Test %u/%u:\n",testnu+1,testpars->num_of_tests);
        /* get test data ready */
        DList* dl = test_data_preparation(testpars->num_of_nums,testpars->upper_bound);
        if (DEBUG) {
            puts("Test data:");
            print_list(dl,print_data);
            putchar('\n');
        }
        for (unsigned funcnu = 0; funcnu < testpars->num_of_sortfuncs; ++funcnu) {
            printf("\t%s:\n",TestNames[funcnu]);
            clock_t begin = 0, end = 0;
            DList* dl_sorted;
            begin = clock();
            dl_sorted = testpars->sortfuncs[funcnu](copy_list(dl));
            end = clock();
            search_times[funcnu][testnu] = end - begin;
            if (DEBUG) {
                printf("Sorted list for sort func %s:\n",TestNames[funcnu]);
                print_list(dl_sorted,print_data);
                putchar('\n');
            }
           destroy_list(dl_sorted);
           free(dl_sorted);
           if (DEBUG) {
                puts("original list (after temp destruction):");
                print_list(dl,print_data);
                putchar('\n');
           }
            printf("\t\t%ld ms\n",search_times[funcnu][testnu]);
        }
        destroy_list(dl);
        free(dl);
    }

    /* getting averages and freeing up memory */
    for (unsigned funcnu = 0; funcnu < testpars->num_of_sortfuncs; ++funcnu) {
        testpars->test_results[funcnu] = get_average(testpars->num_of_tests,search_times[funcnu]);
        printf("Timings for sort func %s:\n",TestNames[funcnu]);
        print_array(testpars->num_of_tests,search_times[funcnu]);
        free(search_times[funcnu]);
    }
    free(search_times);
    return;
}

void destroy_TestParameters(TestParameters* tp) {
    free(tp->sortfuncs);
    free(tp->test_results);
    free(tp);
}

int main(int argc, char** argv) {
    if (argc == 2 && !strcmp("help",argv[1])) {
        help(basename(argv[0]));
        exit(EXIT_SUCCESS);
    }
    /* Initiate Test Parameters */
    TestParameters* testpars = calloc(1,sizeof(TestParameters));
    testpars->num_of_nums = argc > 1 ? atoi(argv[1]) : NUM_OF_NUMS;
    testpars->upper_bound = argc > 2 ? atoi(argv[2]) : UPPER_BOUND;
    testpars->num_of_tests = argc > 3 ? atoi(argv[3]) : NUM_OF_TESTS;
    testpars->num_of_sortfuncs = NUM_OF_TEST_FUNCS;
    testpars->sortfuncs = calloc(testpars->num_of_sortfuncs,sizeof(SortFunc));
    testpars->sortfuncs[nosort] = nosortfunc;
    testpars->sortfuncs[insertsort] = insertion_sort;
    testpars->sortfuncs[insertsort2] = insertion_sort2;
    testpars->sortfuncs[bubblesort] = bubble_sort;
    testpars->test_results = calloc(testpars->num_of_sortfuncs,sizeof(double));

    srand(time(NULL));

    test_engine(testpars);
    printf("Average running time for blank function: %f\n",testpars->test_results[nosort]);
    printf("Average running time for insert sort: %f\n",testpars->test_results[insertsort]);
    printf("Average running time for insert sort2: %f\n",testpars->test_results[insertsort2]);
    printf("Average running time for bubble sort: %f\n",testpars->test_results[bubblesort]);

    destroy_TestParameters(testpars);
    puts("to be continued in test_engine, based on binary_search.c");

    return 0;
}
