#include "dlist2.h"

void initialise_list(DList *list, void (*destroy)(void *data),long (*cmp)(const void* d0,const void* d1)) {
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;
    list->cmp = cmp;
    list->destroy = destroy;
    return;
}

void destroy_list(DList* list) {
    void* data = NULL;
    while (list->size)
        if (!pop_back(list,(void **)&data) && list->destroy)
            list->destroy(data);

    /* clear the list: */
    memset(list,0,sizeof(DList));
    return;
}

int insert_as_next (DList* list, DListElem* elem, const void* data) {
    if (!elem && list->size) {
        fprintf(stderr,"insert_as_next: elem cannot be NULL for non-empty list. (-1)\n");
        return -1;
    }

    DListElem* new_elem = NULL;

    if(!(new_elem = (DListElem *)malloc(sizeof(DListElem)))) {
        fprintf(stderr,"insert_as_next: cannot allocate memory for new list element. (-2)\n");
        return -2;
    }


    new_elem->data = (void *)data;

    if (!get_list_size(list)) {
        new_elem->prev = NULL;
        new_elem->next = NULL;
        list->head = new_elem;
        list->tail = new_elem;
    } else {
        new_elem->prev = elem;
        new_elem->next = elem->next;
        if (elem->next)
            elem->next->prev = new_elem;
        else
            list->tail = new_elem;
        elem->next = new_elem;
    }

    ++list->size;

    return 0;
}

int connect_as_next(DList* list, DListElem* elem, DListElem* new_elem) {
    if (!elem && list->size) {
        fprintf(stderr,"connect_as_next: elem cannot be NULL for non-empty list. (-1)\n");
        return -1;
    }

    if (!get_list_size(list)) {
        new_elem->prev = NULL;
        new_elem->next = NULL;
        list->head = new_elem;
        list->tail = new_elem;
    } else {
        new_elem->prev = elem;
        new_elem->next = elem->next;
        if (elem->next)
            elem->next->prev = new_elem;
        else
            list->tail = new_elem;
        elem->next = new_elem;
    }

    ++list->size;

    return 0;
}

int insert_as_prev(DList* list, DListElem* elem, const void* data) {
    if (!elem && list->size) {
        fprintf(stderr,"insert_as_prev: elem cannot be NULL for non-empty list. (-1)\n");
        return -1;
    }


    DListElem* new_elem = NULL;

    if(!(new_elem = (DListElem *)malloc(sizeof(DListElem)))) {
        fprintf(stderr,"insert_as_prev: Cannot allocate memory for new list element.) (-2)\n");
        return -3;
    }


    new_elem->data = (void *)data;

    if (!get_list_size(list)) {
        new_elem->prev = NULL;
        new_elem->next = NULL;
        list->head = new_elem;
        list->tail = new_elem;
    } else {
        new_elem->next = elem;
        new_elem->prev = elem->prev;
        if (elem->prev)
            elem->prev->next = new_elem;
        else
            list->head = new_elem;
        elem->prev = new_elem;
    }

    ++list->size;

    return 0;
}

int connect_as_prev(DList* list, DListElem* elem, DListElem* new_elem) {
    if (!elem && list->size) {
        fprintf(stderr,"connect_as_prev: elem cannot be NULL for non-empty list. (-1)\n");
        return -1;
    }

    if (!get_list_size(list)) {
        new_elem->prev = NULL;
        new_elem->next = NULL;
        list->head = new_elem;
        list->tail = new_elem;
    } else {
        new_elem->next = elem;
        new_elem->prev = elem->prev;
        if (elem->prev)
            elem->prev->next = new_elem;
        else
            list->head = new_elem;
        elem->prev = new_elem;
    }

    ++list->size;

    return 0;
}

int push_front(DList* list, const void* data) {
    return insert_as_prev(list,list->head,data);
}
int push_back(DList* list, const void* data) {
    return insert_as_next(list,list->tail,data);
}

int remove_elem (DList* list, DListElem* elem, void** data) {
    if (!elem || !list->size)
        return -1;

    *data = elem->data;
    if (elem == list->head) {
        list->head = elem->next;
        if (list->head)
            list->head->prev = NULL;
        else
            list->tail = elem;
    } else {
        elem->prev->next = elem->next;
        if (elem->next)
            elem->next->prev = elem->prev;
        else
            list->tail = elem->prev;
    }
    free(elem);

    --list->size;

    return 0;
}

int disconnect_elem (DList* list, DListElem* elem) {
    if (!elem || !list->size)
        return -1;

    if (elem == list->head) {
        list->head = elem->next;
        if (list->head)
            list->head->prev = NULL;
        else
            list->tail = elem;
    } else {
        elem->prev->next = elem->next;
        if (elem->next)
            elem->next->prev = elem->prev;
        else
            list->tail = elem->prev;
    }

    --list->size;

    return 0;
}

int pop_front(DList* list, void** data) {
    return remove_elem(list,list->head,data);
}
int pop_back(DList* list, void** data) {
    return remove_elem(list,list->tail,data);
}

int get_list_size(const DList* list) { return list->size; }
DListElem* get_list_head(const DList* list) { return list->head; }
DListElem* get_list_tail(const DList* list) { return list->tail; }
bool is_head_elem(const DList* list, const DListElem* elem) { return elem == list->head; }
bool is_tail_elem(const DList* list, const DListElem* elem) { return elem == list->tail; }
bool is_empty(const DList* dl) { return !dl->size; }
void* get_elem_data(const DListElem* elem) { return elem->data; }
DListElem* get_next_elem(const DListElem *elem) { return elem->next; }

void swap_list_elem_data(DListElem* elem0,DListElem* elem1) {
    if (elem0 == elem1)
        return;

    void* tmp_data = elem0->data;
    elem0->data = elem1->data;
    elem1->data = tmp_data;
}

void print_list(const DList* dl,void(*print_data)(const void* data)) {
    if (!dl) {
        puts("print_list: NULL list.");
        return;
    }
    if (!dl->size) {
        puts("print_list: empty list.");
        return;
    }

    DListElem* le = dl->head;
    while(le) {
        print_data(le->data);
        le = le->next;
    }
}
void print_list_backward(const DList* dl,void(*print_data)(const void* data)) {
    DListElem* le = dl->tail;
    while(le) {
        print_data(le->data);
        le = le->prev;
    }
}
