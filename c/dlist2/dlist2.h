#ifndef DLIST2_H_INCLUDED
#define DLIST2_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct DListElem_ {
    void *data;
    struct DListElem_* prev;
    struct DListElem_* next;
} DListElem;

typedef struct DList_ {
    int size;
    long (*cmp)(const void* data0, const void* data1);
    void (*destroy)(void* data);
    DListElem *head;
    DListElem *tail;
} DList;

void initialise_list(DList*, void (*destroy)(void *data),long (*cmp)(const void* d0,const void* d1));
void destroy_list(DList*);
int insert_as_next(DList*, DListElem*, const void*);
int insert_as_prev(DList*, DListElem*, const void*);
int connect_as_next(DList*, DListElem*, DListElem*);
int connect_as_prev(DList*, DListElem*, DListElem*);
int push_front(DList*, const void*);
int push_back(DList*, const void*);
int remove_elem (DList*, DListElem*, void**);
int disconnect_elem (DList*, DListElem*);
int pop_front(DList*, void**);
int pop_back(DList*, void**);

int get_list_size(const DList*);
DListElem* get_list_head(const DList*);
DListElem* get_list_tail(const DList*);
bool is_head_elem(const DList*, const DListElem*);
bool is_tail_elem(const DList*, const DListElem*);
bool is_empty(const DList*);
void* get_elem_data(const DListElem*);
void swap_list_elem_data(DListElem*,DListElem*);
void print_list(const DList*,void(*print_data)(const void*));
void print_list_backward(const DList*,void(*print_data)(const void*));

#endif // DLIST2_H_INCLUDED
