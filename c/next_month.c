#include <stdio.h>

int main(int argc, char** argv) {
  enum month { jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec };

  enum month get_month(enum month m) {
    return(m);
  }
  
  enum month next_month(enum month m) {
    return((m + 1) % 12);
  }
  
  printf("%u\n", next_month(dec));
  printf("%u\n", get_month(apr));
  printf("%u\n", next_month(dec + 1));
  
  return 0;
}
