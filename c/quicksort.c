#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
#include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX
#define NUM_OF_TESTS 10

bool is_sorted(const int len, const int* arr) {
  bool sorted = true;
  for(int i = 1; i < len; ++i)
    if (*(arr + i - 1) > *(arr + i))
      sorted = false;
  return sorted;
}

void print_array(const int len, const int* arr) {
  putchar('\t');
  for(int i = 0; i < len; ++i)
    printf("%d ",*(arr + i));
  putchar('\n');
}

int* gen_nums(const int num_of_nums, const int upper_bound) {
  int* nums = calloc(num_of_nums,sizeof(int));
  for(int i = 0; i < num_of_nums; ++i)
    *(nums + i) = rand() % upper_bound;
  return nums;
}

void swap(int* nu0, int* nu1) {
  int temp = *nu0;
  *nu0 = *nu1;
  *nu1 = temp;
}

// https://en.wikipedia.org/wiki/Quicksort
// Lomuto:
/* algorithm partition(A, lo, hi) is */
/*     pivot := A[hi] */
/*     i := lo */
/*     for j := lo to hi do */
/*         if A[j] < pivot then */
/*             swap A[i] with A[j] */
/*             i := i + 1 */
/*     swap A[i] with A[hi] */
/*     return i */

int divide(int* nums,int starti, int endi) {
  int pivot = nums[endi];
  int i,j;
  for (i = j = starti; j <= endi; ++j) {
    if (nums[j] < pivot) {
      swap(nums+i,nums+j);
      ++i;
    }
  }
  swap(nums+i,nums+endi);
  
  return i;
}

void quicksort(int* nums,int starti, int endi) {
  if(starti < endi) {
    int pivoti = divide(nums,starti,endi);
    quicksort(nums,starti,pivoti-1);
    quicksort(nums,pivoti+1,endi);
  }
}

int main (int argc, char** argv) {
  const unsigned num_of_nums = (argc < 2) ? NUM_OF_NUMS : strtoul(argv[1],NULL,10);
  const unsigned upper_bound = (argc < 3) ? UPPER_BOUND : strtoul(argv[2],NULL,10);
  const unsigned num_of_tests = (argc < 4) ? NUM_OF_TESTS : strtoul(argv[3],NULL,10);
  const unsigned test_num_width = log10(num_of_tests) + 1;
  srand(time(NULL));
  for (unsigned i = 0; i < num_of_tests; ++i) {
    printf("Test %*d:\n",test_num_width,i+1);
    int* numbers = gen_nums(num_of_nums,upper_bound);
    puts("\tunsorted:");
    print_array(num_of_nums,numbers);
    quicksort(numbers,0,num_of_nums-1);
    puts("\tsorted:");
    print_array(num_of_nums,numbers);
    assert(is_sorted(num_of_nums,numbers));
  }

  return 0;
}
