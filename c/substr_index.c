#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  char* longstr = "ardabcdefghijklmnopqrstuv";
  char* shrtstr = "abc";
  char* strtsub = strstr(longstr,shrtstr);
  int strindex = strtsub - longstr;
  int endindex = strindex + strlen(shrtstr) - 1;
  printf("long string: %s\n",longstr);
  printf("short string: %s\n",shrtstr);
  printf("index of the first substr char: %d\n",strindex);
  printf("index of the last char of sub: %d\n",endindex);

  return 0;
}
