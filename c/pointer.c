#include <stdio.h>

int main (int argc, char** argv) {
  float n = 5;
  float *pn = &n;

  printf("Pointer as pointer address: %p\n",pn);
  printf("Pointer as long unsigned: %lu (or in hexa: %lx)\n",(long unsigned)pn,(long unsigned)pn);
  printf("The value pointed at by the pointer: %f\n",*pn); /* dereferencing */

  return 0;
}
