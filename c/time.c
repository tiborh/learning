#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  time_t t = time(NULL);
  printf("time(NULL): %ld\n",t);
  printf("ctime(time(NULL)): %s\n",ctime(&t)); /* ctime == convert time */
  time_t t1 = (time_t)0;
  printf("t1: %ld\n",t1);
  printf("ctime(0): %s\n",ctime(&t1)); /* ctime == convert time */
  printf("asctime(gmtime(&t)): %s\n",asctime(gmtime(&t)));
  return 0;
}
