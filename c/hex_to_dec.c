#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs,atoi,strtol */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int hex_char_to_int(char inch) {
  switch (inch) {
  case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
    return inch - '0';
  case 'a': case 'A':
    return 10;
  case 'b': case 'B':
    return 11;
  case 'c': case 'C':
    return 12;
  case 'd': case 'D':
    return 13;
  case 'e': case 'E':
    return 14;
  case 'f': case 'F':
    return 15;
  default:
    fprintf(stderr, "invalid hexadecimal char: %c\n", inch);
    return -1;
  }
}

long long int hex_to_dec(char* instr) {
  long long int outint = 0;
  int i = 0;
  int width = strlen(instr);
  bool is_minus = false;

  if (*(instr + i) == '-') {
    is_minus = true;
    ++i;
  }
  if (*(instr + i) == '0') {
    ++i;
    if (*(instr + i) == 'x' || *(instr + 1) == 'X')
      ++i;
  }

  for(; i < width; ++i) {
    int res = hex_char_to_int(*(instr + i));
    if (res < 0)
      break;
    outint *= 16;
    outint += res;
  }

  if (is_minus)
    outint *= -1;
  return outint;
}

/*   return outint; */
/* } */

int main (int argc, char** argv) {
  if (argc < 2)
    puts("First arg is converted from hexadecimal to deimal.");
  else {
    long long int result;
    result = hex_to_dec(argv[1]);
    printf("%Ld\n",result);
    //printf("%Ld\n",strtoll(argv[1],NULL,16));
    assert(strtoll(argv[1],NULL,16) == result);
  }

  return 0;
}
