#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define NUMOFNUMS 10
#define MAXNUM 100

void usage(char* progname) {
  printf("Usage:\n\t %s [num of nums (default: %d)] [max num (default: %d)]\n",
	 progname,(int)NUMOFNUMS,(int)MAXNUM);
  exit(1);
}

void print_factors(int num) {
  int halfnum = (int)((float)num / 2);
  printf("Num: %d, factors: ",num);
  for (int i = 1; i <= halfnum; ++i) {
    if (num % i == 0)
      printf("%d ",i);
  }
  printf("%d",num);
  putchar('\n');
  return;
}

int main(int argc, char** argv) {
  if (argc > 1 && (strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"--help") == 0))
    usage(argv[0]);

  int numofnums = (argc < 2) ? (int)NUMOFNUMS : atoi(argv[1]) + 1;
  int maxnum = (argc < 3) ? (int)MAXNUM : atoi(argv[2]) + 1;
  srand((unsigned)time(NULL));

  for (int i = 1; i < numofnums; ++i) {
    int num = rand() % maxnum + 1;
    print_factors(num);
  }
  
  return 0;
}
