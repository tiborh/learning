#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */

int main (int argc, char** argv) {
  printf("three bells: %c%c%c\n",'\a','\a','\a'); /* does not seem to work on pi */
  printf("same with character codes: %c%c%c\n",7,7,7); /* neither does this */
  return 0;
}
