#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  long int r = 0;
  srand(time(NULL));   // Initialization, should only be called once.
  r = rand(); // Returns a pseudo-random integer between 0 and RAND_MAX.
  printf("%ld\n",r);
}
