#include <stdio.h>
#include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

#define BAR_CHAR '#'

void print_bar(int length) {
  for(int i = 0; i < length; ++i)
    putchar(BAR_CHAR);
}

int get_maxval(int max_length,int* stats) {
  int max_val = 0;
  for(int i = 0; i < max_length; ++i)
    if (*(stats+i) > max_val)
      max_val = *(stats+i);
  return max_val;
}

void print_graph(int max_length,int* stats) {
  int maxval = get_maxval(max_length,stats);
  for(int i = 0; i < max_length; ++i)
    if (*(stats+i)) {
      int bar_length = maxval < 100 ? *(stats+i) : (int)(((double)*(stats+i) / maxval) * 100) + 1;
      printf("%c\t",i+'a');
      //printf("bar_length: %d\n",bar_length);
      print_bar(bar_length);
      printf(" (%d)\n",*(stats+i));
    }
}

int main (int argc, char** argv) {

  int c = 0;			/* if char, needs to look out for 0xff to avoid infinite loop */
  const int Alphabet_Size = 'z' - 'a' + 1;
  int* letter_numbers = calloc(Alphabet_Size,sizeof(int));

  while((c = getchar()) != EOF) {
    int c_low = tolower(c);
    if (c_low >= 'a' && c_low <= 'z')
      ++(*(letter_numbers + (c_low - 'a')));
    /* else */
    /*   printf("unhandled: %c (0x%x)\n",c,c); */
  }

  print_graph(Alphabet_Size,letter_numbers);

  free(letter_numbers);
  return 0;
}
