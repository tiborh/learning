#include "bitwise.h"

int main (int argc, char** argv) {
  if (argc < 3) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  char operator = *argv[1];
  int operand0 = atoi(argv[2]);
  if (argc == 3 && *argv[1] != '~')
    exit(EXIT_FAILURE);
  int operand1 = MINUS_INF;
  int result = MINUS_INF;
  printf("%c %d ",operator,operand0);
  if (argc> 3) {
    operand1 = atoi(argv[3]);
    printf("%d",operand1);
  }
  putchar('\n');

  char* binstr = dec2bin(operand0);
  printf("%c %s ",operator,binstr);
  free(binstr);
  if (argc > 3) {
    binstr = dec2bin(operand1);
    printf("%s",binstr);
    free(binstr);
  }
  putchar('\n');
  
  switch (*argv[1]) {
  case '~':
    result = ~operand0;
    //printf("result: %d\n",result);
    print_result(result);
    break;
  case '&':
    result = operand0 & operand1;
    print_result(result);
    break;
  case '|':
    result = operand0 | operand1;
    print_result(result);
    break;
  case '^':
    result = operand0 ^ operand1;
    print_result(result);
    break;
  case '>':
    result = operand0 >> operand1;
    print_result(result);
    break;
  case '<':
    result = operand0 << operand1;
    print_result(result);
    break;
  default:
    printf("unknown operator: %c\n",operator);
    exit(EXIT_FAILURE);
  }

  return 0;
}
