#include "bitwise.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <operator> <operand1> [operand2]|<operand2>\n",basename(argv0));
  printf("\tOperators:\n");
  printf("\t\t~: one's complement (unary)\n");
  printf("\t\t&: bitwise AND\n");
  printf("\t\t|: bitwise inclusive OR\n");
  printf("\t\t^: bitwise exclusive OR\n");
  printf("\t\t<: left shift (<<)\n");
  printf("\t\t>: right shift (>>)\n");
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}

