#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAXNUM 10
#define STARTNUM 1
#define INCREMENT 1

void usage(char* progname) {
  printf("Usage:\n\t %s [max num (default: %d)] [start num (default: %d)] [increment (default: %d)]\n",
	 progname,(int)MAXNUM,(int)STARTNUM,(int)INCREMENT);
  exit(1);
}

void print_factors(int num) {
  int halfnum = (int)((float)num / 2);
  printf("Num: %d, factors: ",num);
  for (int i = 1; i <= halfnum; ++i) {
    if (num % i == 0)
      printf("%d ",i);
  }
  printf("%d",num);
  putchar('\n');
  return;
}

int main(int argc, char** argv) {
  if (argc > 1 && (strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"--help") == 0))
    usage(argv[0]);

  int maxnum = (argc < 2) ? (int)MAXNUM : atoi(argv[1]);
  int startnum = (argc < 3) ? (int)STARTNUM : atoi(argv[2]);
  int incr = (argc < 4) ? (int)INCREMENT : atoi(argv[3]);

  for (int i = startnum; i < maxnum; i += incr) {
    print_factors(i);
  }
  
  return 0;
}
