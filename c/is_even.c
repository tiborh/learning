#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs,atoi */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int is_even(int num) {
  return(num == (num/2)*2);
}

int main (int argc, char** argv) {
    if (argc < 2)
    puts("First arg should be an int.");
  else {
    char* result = is_even(atoi(argv[1])) ? "even" : "odd";
    printf("%s\n",result);
  }


  return 0;
}
