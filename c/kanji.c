#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  char* kanji = argc < 2 ? "葉" : argv[1];
  size_t len = strlen(kanji);

  printf("%s (length: %lu)\n",kanji,len);
  for(size_t i = 0; i < len; ++i)
    printf("%x ",kanji[i]);
  putchar('\n');
  
  return 0;
}
