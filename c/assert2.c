#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  /* try to  compile this with -DNDEBUG too. */

  assert(argc >= 3);
  char* end_ptr;
  double dividend = strtod(argv[1], &end_ptr);
  double divisor  = strtod(argv[2], &end_ptr);
  double quotient = dividend / divisor;
  assert(divisor != 0);
  printf("%f / %f == %f\n", dividend, divisor, quotient);
  
  return 0;
}
