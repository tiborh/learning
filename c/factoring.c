#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int main (int argc, char** argv) {
  if (argc < 2) {
    printf("First parameter is the number to fatorise.\n");
    exit(EXIT_SUCCESS);
  }

  long long num = 0;
  if (1 != sscanf(argv[1], "%Ld", &num)) {
    printf("Cannot read first argument.\n");
    exit(EXIT_FAILURE);
  }
  num = abs(num);
  long long halfnum = num / 2;
  long long lastquotient = 0;
  for(long long int i = 1; i <= halfnum; ++i)
    if (num % i == 0) {
      if (i == lastquotient)
	exit(EXIT_SUCCESS);
      long long quotient = num/i;
      printf("%Ld * %Ld == %Ld\n",i,quotient,num);
      if (quotient == i)
	exit(EXIT_SUCCESS);
      lastquotient = quotient;
    }

  return(EXIT_SUCCESS);
}
