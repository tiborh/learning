#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */

int int_div(int a, int b) {
  int result = a / b;
  return result;
}

int main (int argc, char** argv) {
  if(argc < 3){
    printf("Usage: %s <dividend> <divisor>\n",basename(argv[0]));
    exit(1);
  }
  int a,b;
  sscanf(argv[1],"%d",&a);
  sscanf(argv[2],"%d",&b);
  printf("%d / %d == %d\n",a,b,int_div(a,b));
  
  return 0;
}
