#include <stdio.h>
#include <math.h>

const double yards_in_mile = 1760.0;
const double km_in_mile = 1.609;
char* kilometers_txt = "There are %lf kilometers in a marathon.\n";
const double miles = 26;
const double yards = 385;

int main (int argc, char** argv) {
  printf(kilometers_txt,km_in_mile * (miles + yards / yards_in_mile));

  return 0;
}
