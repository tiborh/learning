#include <cstdio>

int main (int argc, char** argv) {
     char c = 'c';
     printf("c == %c\n",c);
     printf("&c == %p\n",&c);
     char* pc = &c;
     printf("pc == %p\n",pc);
     printf("*pc == %c\n",*pc);
     const char* pc1;
     printf("pc1 == %p\n",pc1);
     printf("*pc1 == %c\n",*pc1);
     pc1 = pc; 			// legal
     printf("pc1 == %p\n",pc1);
     printf("*pc1 == %c\n",*pc1);
     pc = pc1;			// warning is produced
     printf("&pc == %p\n",&pc);
     const char** ppc = &pc;
     printf("ppc == %p\n",ppc);
     printf("*ppc == %p\n",*ppc);
     // errors as opposed to warnings in C:
/*
const2.cpp: In function ‘int main(int, char**)’:
const2.cpp:16:11: error: invalid conversion from ‘const char*’ to ‘char*’ [-fpermissive]
   16 |      pc = pc1;   // warning is produced
      |           ^~~
      |           |
      |           const char*
const2.cpp:18:25: error: invalid conversion from ‘char**’ to ‘const char**’ [-fpermissive]
   18 |      const char** ppc = &pc;
      |                         ^~~
      |                         |
      |                         char**
*/
     
     return 0;
}
