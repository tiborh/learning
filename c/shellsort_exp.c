#include <stdio.h>
// #include <ctype.h>		/* for character functions */
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>		/* for integer type max/min/size */

// reference:
// https://www.codingeek.com/algorithms/shell-sort-algorithm-explanation-implementation-and-complexity/

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX

typedef int INDEX;

int* gennums(INDEX num_of_nums, INDEX upper_bound) {
  int* outarr = calloc(num_of_nums,sizeof(int));
  for(INDEX i = 0; i < num_of_nums; ++i)
    *(outarr+i) = rand() % upper_bound;
  return outarr;
}

void printnums(INDEX len, int* nums) {
  putchar('\t');
  for(INDEX i = 0; i < len; ++i)
    printf("%d ",*(nums+i));
  putchar('\n');
}

int cmp(const int* first, const int* second) {
  return *first - *second;
}

bool is_sorted(INDEX alen, int* nums) {
  for (INDEX i = 1; i < alen; ++i)
    if (cmp(nums+(i-1),nums+i) > 0)
      return false;
  return true;
}

void swap(int* first, int* second) {
  int tmp = *first;
  *first = *second;
  *second = tmp;
}

void shellsort(INDEX alen, int arr[]) {
  for(INDEX halb = alen / 2; halb > 0; halb /= 2) {
    for (INDEX end = halb; end < alen; ++end) {
      for (INDEX begin = end - halb; begin >= 0; begin -= halb) {
	//printf("begin: %d, end %d, half: %d\n",begin,end,halb);
	if (cmp((arr+begin),(arr+end)) > 0) {
	  swap(arr+begin,arr+end);
	  //puts("\tswapped");
	  if (begin - halb >= 0) {
	    begin -= halb + 1;
	    end -= halb + 1;
	  }
	} else
	  break;
      }
    }
  }
}

void shellsort_exp(INDEX alen, int arr[]) {
  for(INDEX halb = alen / 2; halb > 0; halb /= 2) {
    for (INDEX end = halb; end < alen; ++end) {
      for (INDEX begin = end - halb; begin >= 0 && 0 < cmp((arr+begin),(arr+begin+halb)); begin -= halb) {
	//if (cmp((arr+begin),(arr+end)) > 0) {
	  swap(arr+begin,arr+begin+halb);
	  //begin -= halb + 1;
	  //end -= halb + 1;
	  //} //else
	  //break;
      }
    }
  }
}

int main (int argc, char** argv) {
  INDEX num_of_nums = (argc > 1) ? atoi(argv[1]) : NUM_OF_NUMS;
  INDEX upper_bound = (argc > 2) ? atoi(argv[2]) : UPPER_BOUND;
  srand(time(NULL));
  int* nums = gennums(num_of_nums,upper_bound);
  printnums(num_of_nums,nums);
  shellsort_exp(num_of_nums,nums);
  printnums(num_of_nums,nums);
  assert(is_sorted(num_of_nums,nums));
  free(nums);
  return 0;
}
