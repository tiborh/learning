#ifndef DEC2BIN_H_INCLUDED
#define DEC2BIN_H_INCLUDED
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

unsigned char* dec2bin(int);
int bin2dec(char*);

#endif // DEC2BIN_H_INCLUDED
