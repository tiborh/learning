#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

#define ALLOC_SZ 1024

int main (int argc, char** argv) {
  printf("= A malloc example =\n");

  printf("1. Allocate memory.\n");
  char* ptr = (char *) malloc(ALLOC_SZ);

  if (ptr != NULL) {
    printf("2. write into buffer\n");
    ptr[0] = 'a';
    ptr[1] = 0;
    printf("print buffer: |%s|\n",ptr);
    // printf("size of argv: %d\n",argc);
  }

  printf("3. Free memory.\n");
  free(ptr);
  return 0;
}
