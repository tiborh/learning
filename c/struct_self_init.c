#include <stdio.h>

int main (int argc, char** argv) {
  struct point{
    double x;
    double y;
  } p1 = {2.5, p1.x};
  struct point* ptr = &p1;

  printf("p1->x == %f\n",ptr->x);
  printf("p1->y == %f\n",ptr->y);

  return 0;
}
