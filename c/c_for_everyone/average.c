#include <stdio.h>

int main(int argc, char** argv) {
  if (argc == 1) {
    puts("arguments are numbers the average of which you would like to calculate");
    return 1;
  }
  double sum = 0.0;

  for (int i = 1; i < argc; ++i) {
    double num = 0.0;
    sscanf(argv[i],"%lf",&num);
    sum += num;
  }
  double avg = sum / (argc-1);

  printf("Average: %lf\n",avg);
  
  return 0;
}
