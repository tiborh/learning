#include <stdio.h>

int counter(void) {
  static int i = 0;
  return(i++);
}

int countdown(void) {
  static int i = 9;
  return(i--);
}

int main(int argc, char** argv) {

  for (int i = counter(); i < 10; i = counter()) {
    printf("%d ",i);
  }
  putchar('\n');

  for (int i = countdown(); i >= 0; i = countdown()) {
    printf("%d ",i);
  }
  putchar('\n');

  
  return 0;
}
