#include <stdio.h>

int proc_arg_int(char* arg) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    return num;
  return 0;
}

int add_numbers(int num_of_nums, char** num_strings) {
  int minus_one = num_of_nums-1;
  if (num_of_nums == 0)
    return 0;
  return (proc_arg_int(num_strings[minus_one]) + add_numbers(minus_one,num_strings));
}

int main(int argc, char** argv) {
  printf("%d\n",add_numbers(argc,argv));

  return 0;
}
