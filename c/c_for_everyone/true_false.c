#include <stdio.h>
#include <unistd.h>

int main(void) {
  for(bool i = true; ; i = !i) {
    printf("%s\n",i ? "true" : "false");
    sleep(1);
  }
  return 0;
}
