#include <stdio.h>

const double cf_coeff = 9 / 5.0;
const double cf_diff = 32;

double fahr_to_cels(double fahr) {
  return((fahr - cf_diff) / cf_coeff);
}

double cels_to_fahr(double cels) {
  return(cels * cf_coeff + cf_diff);
}

int main (int argc, char** argv) {
  if (argc < 3) {
    puts("First arg: temperature value.\nsecond arg: temp unit (C or F)\n");
    return 1;
  }

  double temp_in = 0.0;
  double temp_out = 0.0;
  char temp_unit = '\0';
  char unit_out = '\0';
  
  sscanf(argv[1], "%lf",&temp_in);
  sscanf(argv[2], "%c", &temp_unit);

  switch(temp_unit) {
  case 'f':
  case 'F':
    temp_out = fahr_to_cels(temp_in);
    unit_out = 'C';
    break;
  case 'c':
  case 'C':
    temp_out = cels_to_fahr(temp_in);
    unit_out = 'F';
    break;
  default:
    printf("Unknown unit.\n");
    return 2;
  }

  printf("%5.2lf %c\n",temp_out,unit_out);
  
  return 0;
}
