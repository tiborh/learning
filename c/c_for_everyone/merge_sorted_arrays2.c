#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> // sleep

#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

int give_number(int ceil) { 
  return(rand() % ceil);
}

void fill_arr(int sz, int ceil, int* arr) {
  for (int i = 0; i < sz; ++i)
    arr[i] = give_number(ceil);
  return;
}

void print_arr(int sz, int* arr) {
  for (int i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int comp_elems_p(const void* pn1, const void* pn2) {
  int n1 = *((int*)pn1);
  int n2 = *((int*)pn2);
  if (n1 < n2)
    return -1;
  if (n1 == n2)
    return 0;
  if (n1 > n2)
    return 1;
  return 10;
}

int *merge_arrays(int sz1, int sz2, int* arr1,int* arr2) {
  int merge_size = sz1 + sz2;
  int* arr_merged = calloc(merge_size,sizeof(int));
  for (int i = 0, count1 = 0, count2 = 0; i < merge_size && (count1 < sz1 || count2 < sz2); ++i) {
    if (count1 < sz1 && count2 < sz2)
      if (arr1[count1] < arr2[count2])
	arr_merged[i] = arr1[count1++];
      else
	arr_merged[i] = arr2[count2++];
    else if (count1 >= sz1)
      arr_merged[i] = arr2[count2++];
    else if (count2 >= sz2)
      arr_merged[i] = arr1[count1++];
  }
  return arr_merged;
}

int main(int argc, char** argv) {
  int arr_sz_ceil = (int)DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg_int(argv[1],&arr_sz_ceil);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  srand(time(NULL));
  
  int arr1_sz = give_number(arr_sz_ceil);
  int arr2_sz = give_number(arr_sz_ceil);
  if (arr1_sz + arr2_sz <= 0) {
    printf("size is too great (%d+%d)\n",arr1_sz,arr2_sz);
    return 1;
  }
  int* arr1 = calloc(arr1_sz,sizeof(int));
  int* arr2 = calloc(arr2_sz,sizeof(int));

  printf("Size of array 1: %d\n",arr1_sz);
  puts("filling array 1");
  fill_arr(arr1_sz,num_ceil,arr1);
  printf("Size of array 2: %d\n",arr2_sz);
  puts("filling array 2");
  fill_arr(arr2_sz,num_ceil,arr2);
  puts("sorting array 1");
  qsort(arr1,arr1_sz,sizeof(int),comp_elems_p);
  puts("sorting array 2");
  qsort(arr2,arr2_sz,sizeof(int),comp_elems_p);

  time_t merge_start_time = time(NULL);
  printf("merge_start_time: %ld\n",merge_start_time);
  int* arr_merged = merge_arrays(arr1_sz,arr2_sz,arr1,arr2);
  time_t merge_end_time = time(NULL);
  printf("merge_end_time:   %ld\n",merge_end_time);

  if (arr1_sz < DEF_PRINT_CEIL || arr2_sz < DEF_PRINT_CEIL) {
    puts("array 1:");
    print_arr(arr1_sz,arr1);
    puts("array 2:");
    print_arr(arr2_sz,arr2);
    puts("merged array:");
    print_arr(arr1_sz+arr2_sz,arr_merged);
  }

  printf("Time needed for merge: %ld(s)\n",merge_end_time - merge_start_time);

  free(arr1);
  free(arr2);
  free(arr_merged);
  
  return 0;
}
