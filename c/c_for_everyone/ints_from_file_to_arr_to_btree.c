/*
 * Task description:
 * 1. Open and read a file of integers into an array that is created
 *    * the first integer telling you how many to read
 *     * as an example: 4 9  11  4  5 would mean
 *       a.) create an integer array size 4 and
 *       b.) read into data[].   
 * 2. Write a routine that places these values into a binary tree structure.
 * 3. Then walk the tree “inorder” and print these values to the screen.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* default filename for input numbers
 * which can be overriden with first command line argument
 * sample input can be generated with gen_ints_file(.c)
 */
#define DEF_FN       "ints.txt"

typedef int DATA;
typedef struct node{
  DATA  d;
  struct node* left;
  struct node* right;
} Node;

/* for printing the an int array of size sz */
void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

/* see point 1 in Task description above */
int* get_file_contents(FILE* fh) {
  size_t num_of_ints = 0;
  fscanf(fh,"%lu",&num_of_ints);
  int* int_arr = calloc(num_of_ints+1,sizeof(int));
  int_arr[0] = (int)num_of_ints; /* make conversion explicit */
  
  for(size_t i = 1; i <= num_of_ints; ++i) {
    fscanf(fh,"%d",&int_arr[i]);
  }

  return int_arr;
}

/* binary tree node functions */
void init_node(Node* n, DATA d) {
  n->d = d;
  n->left = NULL;
  n->right = NULL;
}

Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

void add_to_tree(Node* n, Node* tr) {
  if (n->d < tr->d) {
    if (tr->left == NULL)
      tr->left = n;
    else
      add_to_tree(n,tr->left);
  } else {
    if (tr->right == NULL)
      tr->right = n;
    else
      add_to_tree(n,tr->right);
  }
}

/* tree generation from array of size sz */
Node* gen_tree(size_t sz, int* arr) {
  Node* n = create_node(arr[0]);
  for (size_t i = 1; i < sz; ++i) {
    Node* nx = create_node(arr[i]);
    add_to_tree(nx,n);
  }
  return n;
}

/* this is basically a sorted list of the integer values */
void print_tree_inorder(Node* tr) {
  if (tr != NULL) {
    print_tree_inorder(tr->left);
    printf("%d ",tr->d);
    print_tree_inorder(tr->right);
  }
}

/* free up memory taken up by the binary tree nodes */
void destroy_tree(Node** tr) {
  if ((*tr)->left != NULL)
    destroy_tree((&(*tr)->left));
  if ((*tr)->right != NULL)
    destroy_tree(&((*tr)->right));
  free(*tr);
}

int main(int argc, char** argv) {
  char* in_fn = (argc > 1) ? argv[1] : DEF_FN; /* you can provide new valid input file as command line arg*/
  FILE* fh = fopen(in_fn,"r");
  if (fh == NULL) {
    fprintf(stderr,"Error at opening file for reading: %s\n",in_fn);
    exit(1);
  }

  int* arr_from_file = get_file_contents(fh);
  fclose(fh);

  size_t num_of_ints = arr_from_file[0];
  printf("Number of integers: %lu\n",num_of_ints);
  puts("the integers:");
  print_arr(num_of_ints,arr_from_file+1); /* to avoid printing the first element too */

  Node* btr = gen_tree(num_of_ints,arr_from_file+1);
  free(arr_from_file);		/* not needed any longer */
  puts("Numbers from tree:");
  print_tree_inorder(btr);
  puts("");			/* to add a line break */

  /* cleanup */
  destroy_tree(&btr);
  
  return 0;
}


/***************************************************************************************************
 * input file generator                                                                            *
 ***************************************************************************************************/

/*
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_FN       "ints.txt"
#define DEF_NUM_INTS ((size_t)10)
#define DEF_NUM_CEIL ((int)100)
#define DEF_MAX_PRNT ((size_t)100)

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(size_t sz, int ceil, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

void arr_to_file(size_t sz,int* arr, FILE* fh) {
  fprintf(fh,"%lu\n",sz);
  for (size_t i = 0; i < sz; ++i)
    fprintf(fh,"%d ",arr[i]);
  fputc('\n',fh);
}

void print_file_contents(FILE* fh) {
  rewind(fh);
  size_t num_of_ints = 0;
  fscanf(fh,"%lu",&num_of_ints);
  puts("file contents:");
  printf("%lu\n",num_of_ints);
  int* int_arr = calloc(num_of_ints,sizeof(int));
  for(size_t i = 0; i < num_of_ints; ++i) {
    fscanf(fh,"%d",&int_arr[i]);
  }
  print_arr(num_of_ints,int_arr);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  char* out_fn = (argc > 1) ? argv[1] : DEF_FN;
  size_t num_of_ints = DEF_NUM_INTS;
  if (argc > 2)
    proc_arg_long_unsigned(argv[2],&num_of_ints);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 3)
    proc_arg_int(argv[3],&num_ceil);

  int* int_arr = calloc(num_of_ints,sizeof(int));
  fill_arr(num_of_ints,num_ceil,int_arr);
  
  puts("array to file:");
  printf("%lu\n",num_of_ints);
  print_arr(num_of_ints,int_arr);

  FILE* fh = fopen(out_fn,"w+");
  if (fh == NULL) {
    fprintf(stderr,"Error at opening file for writing: %s\n",out_fn);
    exit(1);
  }
  arr_to_file(num_of_ints,int_arr,fh);

  puts("File read back from file:");
  print_file_contents(fh);

  printf("Data has been written to: %s\n",out_fn);
  
  fclose(fh);
  
  free(int_arr);
  
  return 0;
}
*/
