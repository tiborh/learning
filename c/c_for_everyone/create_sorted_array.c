#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int give_number(int ceil) { 
  return(rand() % ((RAND_MAX < ceil) ? RAND_MAX : ceil));
}

int* merge_value(size_t arr1_sz, int* arr1, int val) {
  bool val_added = false;
  size_t msz = arr1_sz + 1;
  int* marr = calloc(msz,sizeof(int));

  for (size_t i = 0, c1 = 0; (c1 < arr1_sz || !val_added) && i < msz; ++i) {
    if (!val_added && (c1 == arr1_sz || val < arr1[c1])) {
      marr[i] = val;
      val_added = true;
    } else {
      marr[i] = arr1[c1++];
    }
  }
  free(arr1);
  return marr;
}

int* create_arr_ordered(size_t sz, int ceil) {
  srand(time(NULL));
  int* arr1 = calloc((size_t)1,sizeof(int));
  arr1[0] = (long unsigned)give_number(ceil);
  for (size_t i = 1; i < sz; ++i)
    arr1 = merge_value(i,arr1,give_number(ceil));
  return arr1;
}

int main(int argc, char** argv) {
  size_t arr_sz = (size_t)DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&arr_sz);
  int num_ceil = (int)DEF_NUM_CEIL;
  if (argc > 2) {
    proc_arg_int(argv[2],&num_ceil);
    if (num_ceil <= 0) {
      printf("Ceiling error: %d\n",(int)num_ceil);
      return 1;
    }
  }
  
  time_t fill_start_time = time(NULL);
  int* arr = create_arr_ordered(arr_sz,num_ceil);
  time_t fill_end_time = time(NULL);
  if (arr_sz < (size_t)DEF_PRINT_CEIL)
      print_arr(arr_sz,arr);

  printf("fill start_time: %ld\n",fill_start_time);
  printf("fill end_time:   %ld\n",fill_end_time);
  printf("Fill time:       %ld(s)\n",fill_end_time - fill_start_time);

  free(arr);
  
  return 0;
}
