#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> // sleep

#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

int give_number(int ceil) { 
  return(rand() % ceil);
}

void fill_arr(size_t sz, int ceil, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    arr[i] = give_number(ceil);
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int *merge_arrays(size_t sz1, size_t sz2, int* arr1,int* arr2) {
  size_t merge_size = sz1 + sz2;
  int* arr_merged = calloc(merge_size,sizeof(int));
  for (size_t i = 0, count1 = 0, count2 = 0; i < merge_size && (count1 < sz1 || count2 < sz2); ++i) {
    if (count1 < sz1 && count2 < sz2)
      if (arr1[count1] < arr2[count2])
	arr_merged[i] = arr1[count1++];
      else
	arr_merged[i] = arr2[count2++];
    else if (count1 >= sz1)
      arr_merged[i] = arr2[count2++];
    else if (count2 >= sz2)
      arr_merged[i] = arr1[count1++];
  }
  return arr_merged;
}

void swap(int* ptr1, int* ptr2) {
  int tmp = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = tmp;
  return;
}

int* merge_sort(int* arr, size_t sz) {
  if (sz == 2) {
    if (*(arr+1) < *(arr))
      swap(arr,arr+1);
    return arr;
  }
  //size_t half = sz/2;
  int* merged_arr = merge_arrays(sz/2,sz-sz/2,merge_sort(arr,sz/2),merge_sort(arr+sz/2,sz-sz/2));
  free(arr);
  return merged_arr;
}

int main(int argc, char** argv) {
  puts("not yet implemented");
  sleep(1);
  exit(1);
  size_t arr_sz = DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&arr_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);
  int* arr = calloc(arr_sz,sizeof(int));

  time_t fill_start_time = time(NULL);
  printf("fill_start_time: %ld\n",fill_start_time);
  fill_arr(arr_sz,num_ceil,arr);
  time_t fill_end_time = time(NULL);
  printf("fill_end_time:   %ld\n",fill_end_time);
  if (arr_sz < DEF_PRINT_CEIL)
      print_arr(arr_sz,arr);
  time_t sort_start_time = time(NULL);
  printf("sort_start_time: %ld\n",sort_start_time);
  arr = merge_sort(arr,arr_sz);
  time_t sort_end_time = time(NULL);
  printf("sort_end_time:   %ld\n",sort_end_time);
  if (arr_sz < DEF_PRINT_CEIL)
    print_arr(arr_sz,arr);
  printf("Time needed for filling: %ld(s)\n",fill_end_time - fill_start_time);
  printf("Time needed for sorting: %ld(s)\n",sort_end_time - sort_start_time);

  free(arr);

  return 0;
}
