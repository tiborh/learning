#include <stdio.h>
#include <ctype.h>

int main(int argc, char** argv) {
  int blanks = 0, digits = 0, letters = 0, puncts = 0, nascii = 0, other = 0;
  char c = '\0';

  while((c = getchar()) != EOF) {
    if (isalpha(c))
      ++letters;
    else if (isspace(c))
      ++blanks;
    else if (isdigit(c))
      ++digits;
    else if (ispunct(c))
      ++puncts;
    else if (!__isascii(c))
      ++nascii;
    else {
      //putchar(c);
      ++other;
    }
  }
  printf("blanks:            %d\n"
	 "digits:            %d\n"
	 "letters:           %d\n"
	 "punctuation:       %d\n"
	 "other non ascii:   %d\n"
	 "other chars:       %d\n"
	 "sum:               %d\n",blanks,digits,letters,puncts,nascii,other,
	 blanks+digits+letters+puncts+nascii+other);
  
  return 0;
}
