#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define DEF_FMT "%d"
#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
  return;
}

int give_number(int ceil) { 
  return(rand() % ((RAND_MAX < ceil) ? RAND_MAX : ceil));
}

void fill_arr(int sz, int ceil, int* arr) {
  srand(time(NULL));
  for (int i = 0; i < sz; ++i)
    arr[i] = give_number(ceil);
  return;
}

void print_arr(int sz, int* arr) {
  for (int i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int comp_elems_p(const void* pn1, const void* pn2) {
  int n1 = *((int*)pn1);
  int n2 = *((int*)pn2);
  if (n1 < n2)
    return -1;
  if (n1 == n2)
    return 0;
  if (n1 > n2)
    return 1;
  return 10;
}

int main(int argc, char** argv) {
  int arr_sz = (int)DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg(argv[1],&arr_sz);
  int num_ceil = (int)DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg(argv[2],&num_ceil);
  int* arr = calloc(arr_sz,sizeof(int));

  fill_arr(arr_sz,num_ceil,arr);
  if (arr_sz < DEF_PRINT_CEIL)
      print_arr(arr_sz,arr);
  time_t sort_start_time = time(NULL);
  printf("start_time: %ld\n",sort_start_time);
  qsort(arr,arr_sz,sizeof(int),comp_elems_p);
  time_t sort_end_time = time(NULL);
  printf("end_time:   %ld\n",sort_end_time);
  if (arr_sz < DEF_PRINT_CEIL)
    print_arr(arr_sz,arr);

  int* pfinding = NULL;

  time_t search_start_time = time(NULL);
  printf("search start_time: %ld\n",search_start_time);
  long int search_counter;
  for(search_counter = 0; pfinding == NULL; ++search_counter) {
    int num = give_number(num_ceil);
    pfinding = bsearch(&num,arr,arr_sz,sizeof(int),comp_elems_p);
    printf("%d: %s\n",num,(pfinding == NULL) ? "NOT IN ARRAY" : "FOUND");
  }
  time_t search_end_time = time(NULL);
  printf("search end_time:   %ld\n",search_end_time);

  printf("Sort time:   %ld(s)\n",sort_end_time - sort_start_time);
  long int total_search_time = search_end_time - search_start_time;
  printf("Search time: %ld(s) (%ld/attempt) (search counter: %ld)\n", total_search_time,total_search_time/search_counter,search_counter);
  
  free(arr);

  return 0;
}
