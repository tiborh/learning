#include <stdio.h>

#define KMS_IN_MILE 1.609344
#define YDS_IN_MILE 1760

int main(int argc, char** argv) {
  if (argc == 1) {
    printf("First arg: miles\nSecond arg: yards\n");
    return 1;
  }

  double miles=0.0, yards=0.0, kms=0.0;
  sscanf(argv[1],"%lf",&miles);
  if (argc > 2)
    sscanf(argv[2],"%lf",&yards);
  double in_miles = miles + yards / YDS_IN_MILE;
  double in_kms = in_miles * KMS_IN_MILE;
  
  printf("%5.2lf kms (%5.2lf miles).\n",in_kms,in_miles);
  
  return 0;
}
