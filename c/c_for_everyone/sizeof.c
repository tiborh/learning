#include <stdio.h>

int main(int argc, char** argv) {
  printf("Size of char: %lu\n",sizeof(char));
  printf("Size of short: %lu\n",sizeof(short));
  printf("Size of int: %lu\n",sizeof(int));
  printf("Size of float: %lu\n",sizeof(float));
  printf("Size of double: %lu\n",sizeof(double));
  printf("Size of long: %lu\n",sizeof(long));
  printf("Size of long long: %lu\n",sizeof(long long));
  printf("Size of long double: %lu\n",sizeof(long double));
  puts("----");
  printf("Size of 3:   %lu\n",sizeof(3));
  printf("Size of 3L:  %lu\n",sizeof(3L));
  printf("Size of 0.3: %lu\n",sizeof(0.3));
  
  return 0;
}
