#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_FMT "%d"
#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(int sz, int ceil, unsigned* arr) {
  srand(time(NULL));
  for (int i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(int sz, unsigned* arr) {
  for (int i = 0; i < sz; ++i)
    printf("%u ",arr[i]);
  putchar('\n');
  return;
}

void swap(unsigned* ptr1, unsigned* ptr2) {
  unsigned tmp = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = tmp;
  return;
}

void sort_arr(int sz,unsigned* arr) {
  bool change_flag = true;
  for (int i = 0, end_ind = sz; i < sz; ++i, --end_ind) {
    if (change_flag == false)
      break;
    change_flag = false;
    for (int j = 1; j < end_ind; ++j)
      if (arr[j-1] > arr[j]) {
	swap(arr+(j-1),arr+j);
	change_flag = true;
      }
  }
}

int main(int argc, char** argv) {
  int arr_sz = DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg(argv[1],&arr_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg(argv[2],&num_ceil);
  unsigned* arr = calloc(arr_sz,sizeof(unsigned));

  fill_arr(arr_sz,num_ceil,arr);
  if (arr_sz < DEF_PRINT_CEIL) {
    puts("unsorted");
    print_arr(arr_sz,arr);
  }
  time_t start_time = time(NULL);
  printf("start_time: %ld\n",start_time);
  sort_arr(arr_sz,arr);
  time_t end_time = time(NULL);
  printf("end_time: %ld\n",end_time);
  if (arr_sz < DEF_PRINT_CEIL) {
    puts("sorted");
    print_arr(arr_sz,arr);
  }
  printf("Time needed: %ld(s)\n",end_time - start_time);
  
  free(arr);
  
  return 0;
}
