#include <stdio.h>

#define DEF_FMT "%hu"

void proc_arg(char* arg, short unsigned* nu) {
  short unsigned num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
}

long unsigned fibr(short unsigned n) {
  if (n <= 1)
    return n;
  return(fibr(n - 2) + fibr(n - 1));
}

int main(int argc, char** argv) {
  short unsigned num = 0;
  if (argc > 1)
    proc_arg(argv[1],&num);
  else {
    printf("fibonacci number <- %s <num>\n",argv[0]);
    return 1;
  }
  long unsigned fib_res = fibr(num);
  printf("fib(%hu) == %lu\n",num,fib_res);
  return 0;
}
