/*******************************************************************************
 * Title: Sine Values Between Two Numbers (non-inclusive)
 * Date: 2024-01-24
 * Author: Tibor
 * Comment: due to math.h library -lm switch might be needed by the compiler
 ******************************************************************************/
#include <stdio.h>
#include <math.h>

#define DEFAULT_INCREMENT 0.1

int main(int argc, char** argv) {
  const float START_X = 0;
  const float END_X = 1;
  float incr = (float)DEFAULT_INCREMENT;

  /* first command line arg can modify the default increment */
  if (argc > 1) {
    float new_incr = 0;
    int num_of_scanned = sscanf(argv[1],"%f",&new_incr);
    if (num_of_scanned > 0) { // use only when a value has been read and
      if (new_incr > START_X && new_incr < END_X) // the value is within sensible range
	incr = new_incr;
      else
	puts("Command line argument value is out of range.");
    } else
      puts("Command line argument cannot be parsed as number.");
  }

  /* datapoint calculation and printing */
  for(float curr_x = START_X + incr; curr_x < END_X; curr_x += incr)
    printf("x = %f, y = %f\n",curr_x,sinf(curr_x));
  
  return 0;
}
