#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  srand(time(NULL));
  
  int b = rand() % 100;
  char* a = (b < 25) ? "low" : (b < 75) ? "mid" : "high";
  printf("%d (%s)\n",b,a);
  
  return 0;
}
