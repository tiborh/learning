#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(size_t sz, int ceil, int* arr) {
  srand(time(NULL));
  for (size_t i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int comp_elems_p(const void* pn1, const void* pn2) {
  int n1 = *((unsigned*)pn1);
  int n2 = *((unsigned*)pn2);
  if (n1 < n2)
    return -1;
  if (n1 == n2)
    return 0;
  if (n1 > n2)
    return 1;
  return 10;
}

int main(int argc, char** argv) {
  size_t arr_sz = DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&arr_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);
  int* arr = calloc(arr_sz,sizeof(int));

  time_t fill_start_time = time(NULL);
  printf("fill_start_time: %ld\n",fill_start_time);
  fill_arr(arr_sz,num_ceil,arr);
  time_t fill_end_time = time(NULL);
  printf("fill_end_time:   %ld\n",fill_end_time);
  if (arr_sz < DEF_PRINT_CEIL)
      print_arr(arr_sz,arr);
  time_t sort_start_time = time(NULL);
  printf("sort_start_time: %ld\n",sort_start_time);
  qsort(arr,arr_sz,sizeof(unsigned),comp_elems_p);
  time_t sort_end_time = time(NULL);
  printf("sort_end_time:   %ld\n",sort_end_time);
  if (arr_sz < DEF_PRINT_CEIL)
    print_arr(arr_sz,arr);
  printf("Time needed for filling: %ld(s)\n",fill_end_time - fill_start_time);
  printf("Time needed for sorting: %ld(s)\n",sort_end_time - sort_start_time);

  free(arr);

  return 0;
}
