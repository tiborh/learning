/*
* Title:  Give the Next Date (based on month and day)
* Author: Tibor
* Date:   2024-01-30
*/
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>		/* calloc, free */

#define NUM_OF_TEST_CASES 5	/* number of test cases provided in task */

/* using full names */
typedef enum{ january, february, march, april, may, june,
	      july, august, september, october, november, december} Month;

/* fixed end of year */
const Month LAST_MONTH = december;

/* a date ADT */
typedef struct{
  Month m;
  short d;
} Date;

/* to help get number of days each month */
short get_max_days(Month);

/* help get month name from month enum */
const char* get_month_str(Month);

/* print month and day */
void printdate(Date* d) {
  printf("%9s %2d",get_month_str(d->m),d->d);
}

/* getting the next date */
Date *nextdate(Date d) {
  Date* out_date = calloc(1,sizeof(Date)); /* allocate memory for a date */
  if (d.d < get_max_days(d.m)) {
    out_date->m = d.m;
    out_date->d = d.d + 1;
  } else if (d.m != LAST_MONTH) {
    out_date->m = d.m + 1;
    out_date->d = 1;
  } else {
    out_date->m = (Month)january;
    out_date->d = 1;
  }
  return out_date;
}

int main(int argc, char** argv) {
  /* how many test dates/cases */
  const short int num_of_test_cases = (short)NUM_OF_TEST_CASES;
  /* test cases */
  Date input_dates[NUM_OF_TEST_CASES] = {
    {january, 1},
    {february, 28},
    {march, 14},
    {october, 31},
    {december, 31}
  };
  /* accepted results */
  Date expected_results[NUM_OF_TEST_CASES] = {
    {january, 2},
    {march, 1},
    {march, 15},
    {november, 1},
    {january, 1}
  };

  /* running the test cases */
  puts("Test cases:");
  for (short i = 0; i < num_of_test_cases; ++i) {
    printf("  %d: ",i+1);	/* start the line */
    printdate(&input_dates[i]);
    printf(" --> ");
    Date* res = nextdate(input_dates[i]);
    printdate(res);
    puts("");			/* close the line */
    assert(res->m == expected_results[i].m);
    assert(res->d == expected_results[i].d);
    free(res);			/* free up memory allocated in function nextdate */
  }
  
  return 0;
}

short get_max_days(Month m) {
  switch(m) {
  case january:
  case march:
  case may:
  case july:
  case august:
  case october:
  case december:
    return 31;
  case april:
  case june:
  case september:
  case november:
    return 30;
  case february:			/* simplification: no year, best guess: not a leap year */
    return 28;
  default:
    fprintf(stderr,"wrong month: %d\n",m);
    return 0;
  };
}

const char* get_month_str(Month m) {
  switch(m) {
  case january:
    return "January";
  case march:
    return "March";
  case may:
    return "May";
  case july:
    return "July";
  case august:
    return "August";
  case october:
    return "October";
  case december:
    return "December";
  case april:
    return "April";
  case june:
    return "June";
  case september:
    return "September";
  case november:
    return "November";
  case february:
    return "February";
  default:
    fprintf(stderr,"wrong month");
    return 0;
  };
}
