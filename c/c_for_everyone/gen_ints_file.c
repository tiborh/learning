#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_FN       "ints.txt"
#define DEF_NUM_INTS ((size_t)10)
#define DEF_NUM_CEIL ((int)100)
#define DEF_MAX_PRNT ((size_t)100)

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(size_t sz, int ceil, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

void arr_to_file(size_t sz,int* arr, FILE* fh) {
  fprintf(fh,"%lu\n",sz);
  for (size_t i = 0; i < sz; ++i)
    fprintf(fh,"%d ",arr[i]);
  fputc('\n',fh);
}

void print_file_contents(FILE* fh) {
  rewind(fh);
  size_t num_of_ints = 0;
  fscanf(fh,"%lu",&num_of_ints);
  puts("file contents:");
  printf("%lu\n",num_of_ints);
  int* int_arr = calloc(num_of_ints,sizeof(int));
  for(size_t i = 0; i < num_of_ints; ++i) {
    fscanf(fh,"%d",&int_arr[i]);
  }
  print_arr(num_of_ints,int_arr);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  char* out_fn = (argc > 1) ? argv[1] : DEF_FN;
  size_t num_of_ints = DEF_NUM_INTS;
  if (argc > 2)
    proc_arg_long_unsigned(argv[2],&num_of_ints);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 3)
    proc_arg_int(argv[3],&num_ceil);

  int* int_arr = calloc(num_of_ints,sizeof(int));
  fill_arr(num_of_ints,num_ceil,int_arr);
  
  puts("array to file:");
  printf("%lu\n",num_of_ints);
  print_arr(num_of_ints,int_arr);

  FILE* fh = fopen(out_fn,"w+");
  if (fh == NULL) {
    fprintf(stderr,"Error at opening file for writing: %s\n",out_fn);
    exit(1);
  }
  arr_to_file(num_of_ints,int_arr,fh);

  puts("File read back from file:");
  print_file_contents(fh);

  printf("Data has been written to: %s\n",out_fn);
  
  fclose(fh);
  
  free(int_arr);
  
  return 0;
}
