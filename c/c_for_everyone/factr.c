#include <stdio.h>

#define DEF_FMT "%hu"

void proc_arg(char* arg, short unsigned* nu) {
  short unsigned num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
}

long unsigned factr(short unsigned n) {
  if (n == 0)
    return 1;
  return((long unsigned)n * factr(n - 1));
}

int main(int argc, char** argv) {
  short unsigned num = 0;
  if (argc > 1)
    proc_arg(argv[1],&num);
  else {
    printf("n! <- %s <num>\n",argv[0]);
    return 1;
  }
  long unsigned fact_res = factr(num);
  if (fact_res == 0)
    puts("number is greater than can be processed");
  else
    printf("%hu! == %lu\n",num,fact_res);
  return 0;
}
