#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  srand(time(NULL));

  for (int i = 0; i < 10; ++i) {
    int a = rand() % 5, b = 0;
    switch(a) {
    case 4:
      ++b;
    case 3:
      ++b;
    case 2:
      ++b;
    case 1:
      ++b;
      break;
    default:
      puts("zero");
    }
    printf("b == %d (a == %d)\n",b,a);
  }
    
  return 0;
}
