#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_LST_SIZ  ((size_t)10)
#define DEF_NUM_CEIL ((int)100)
#define DEF_MAX_PRNT ((size_t)100)
#define DEF_SRT_ALG  'i'

typedef int DATA;

typedef struct node{
  DATA d;
  struct node* next;
} Node;

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_char(char* arg, char* c) {
  char ch = 0;
  int res = sscanf(arg,"%c",&ch);
  if (res > 0)
    *c = ch;
  return;
}

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void init_node(Node* n, DATA d) {
  n->d = d;
  n->next = NULL;
}

Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

void push_d(DATA d, Node** li) {
  Node* newn = create_node(d);
  newn->next = *li;
  *li = newn;
}

DATA view(Node* li) {		/* untested */
  return(li->d);
}

void concat_lists(Node* l1, Node** l2) { /* untested */
  Node* lipo = l1;
  while(lipo->next != NULL)
    lipo = lipo->next;
  lipo->next = *l2;
}

void print_list(Node* li) {
  Node* lipo = li;
  while (lipo != NULL) {
    printf("%d" " -> ",lipo->d);
    lipo = lipo->next;
  } 
  puts("NULL");
}

Node* gen_list(size_t sz, int ceil) {
  Node* outp = NULL;
  for (size_t i = 0; i < sz; ++i) {
    DATA d = rand() % ceil;
    push_d(d,&outp);
  }
  return outp;
}

DATA pop_d(Node** lst) {		/* untested */
  DATA out = (*lst)->d;
  Node* tmp = *lst;
  *lst = (*lst)->next;
  free(tmp);
  return(out);
}

void destroy_list(Node** lst) {
  while (*lst != NULL) {
    Node* tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
}

//no good because node previous to ptr1 will still point at ptr2
/* void swap_n(Node** ptr1, Node** ptr2) { */
/*   Node* tmp = *ptr1; */
/*   tmp->next = (*ptr2)->next; */
/*   (*ptr2)->next = tmp; */
/*   *ptr1 = *ptr2; */
/*   *ptr2 = tmp; */
/*   return; */
/* } */

void swap_d(Node* ptr1, Node* ptr2) {
  DATA tmp = ptr1->d;
  ptr1->d = ptr2->d;
  ptr2->d = tmp;
  return;
}

Node* pop_n(Node **lst) {
  Node* popped = *lst;
  *lst = (*lst)->next;
  popped->next = NULL;
  return popped;
}

void push_n(Node* n, Node** li) {
  //puts("push");
  n->next = *li;
  *li = n;
}

void ins_n(Node* n, Node** lst) {
  //puts("ins");
  n->next = (*lst)->next;
  (*lst)->next = n;
}

void sort_list_insert(Node** lst) {
  puts("insertion sort");
  if (lst == NULL)
    return;
  Node* merged = pop_n(lst);
  while(*lst != NULL) {
    Node* n = pop_n(lst);
    if (n->d < merged->d) {
      push_n(n,&merged);
    } else {
      Node* mrgp = merged;
      while (mrgp->next != NULL && n->d > mrgp->next->d)
	mrgp = mrgp->next;	/* wind forward */
      ins_n(n,&mrgp);
    }
  }
  *lst = merged;
}

void sort_list_bubble(size_t sz, Node* lst) {
  puts("bubble sort");
  bool change_flag = true;
  Node* lipo = lst;
  for (int i = 0, end_ind = sz; i < sz; ++i, --end_ind, lipo=lst) {
    if (change_flag == false)
      break;
    change_flag = false;
    for (int j = 0; j < end_ind+1 && lipo->next != NULL; ++j, lipo = lipo->next)
      if (lipo->d > lipo->next->d) {
	swap_d(lipo,lipo->next);
	//swap_n(&lipo,&(lipo->next));
	change_flag = true;
      }
  }
}

int main(int argc, char** argv) {
  srand(time(NULL));
  size_t list_sz = DEF_LST_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&list_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);
  char sort_alg = DEF_SRT_ALG;
  if (argc > 3)
    proc_arg_char(argv[3],&sort_alg);
  
  Node* lst = gen_list(list_sz,num_ceil);
  if (list_sz <= DEF_MAX_PRNT) {
    puts("Unsorted:");
    print_list(lst);
  }

  time_t start_time = time(NULL);
  switch(sort_alg) {
  case 'b':
  case 'B':
    sort_list_bubble(list_sz,lst);
    break;
  case 'i':
  case 'I':
    sort_list_insert(&lst);
    break;
  default:
    puts("invalid char for sorting algorithm");
    exit(1);
  }
  time_t end_time = time(NULL);
  if (list_sz <= DEF_MAX_PRNT) {
    puts("Sorted:");
    print_list(lst);
  }
  printf("sorting time: %lu(s)\n",end_time-start_time);
  
  destroy_list(&lst);
  
  return 0;
}
