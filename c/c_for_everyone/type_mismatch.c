#include <stdio.h>

double implicit_conversion(int a) {
  return a;
}

int main(int argc, char** argv) {

  int a = 25;
  printf("a: %d, implicit_conversion(a): %f\n",a,implicit_conversion(a));
  
  return 0;
}
