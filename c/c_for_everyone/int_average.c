#include <stdio.h>

int main(int argc, char** argv) {
  if (argc == 1) {
    puts("arguments are numbers the int-average of which you would like to calculate");
    return 1;
  }
  long sum = 0;

  for (int i = 1; i < argc; ++i) {
    int num = 0;
    sscanf(argv[i],"%d",&num);
    sum += num;
  }
  int avg = sum / (argc-1);

  printf("Integer average: %d\n",avg);
  
  return 0;
}
