#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* some defaults that can be overriden with command line args */
#define DEF_TREE_SIZ ((size_t)10)
#define DEF_NUM_CEIL ((int)100)
/* this can only be overriden here, in the code: */
#define DEF_MAX_PRNT ((size_t)200)

typedef int DATA;
typedef struct node{
  DATA  d;
  struct node* left;
  struct node* right;
} Node;

/* used for command line arg procession */
void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

/* used for command line arg procession */
void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void init_node(Node* n, DATA d) {
  n->d = d;
  n->left = NULL;
  n->right = NULL;
}

Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

DATA gen_num(int ceil) {
  return(rand() % ceil);
}

void add_to_tree(Node* n, Node* tr) {
  if (n->d < tr->d) {
    if (tr->left == NULL)
      tr->left = n;
    else
      add_to_tree(n,tr->left);
  } else {
    if (tr->right == NULL)
      tr->right = n;
    else
      add_to_tree(n,tr->right);
  }
}

Node* gen_tree(size_t sz, int ceil) {
  Node* n = create_node(gen_num(ceil));
  for (size_t i = 1; i < sz; ++i) {
    Node* nx = create_node(gen_num(ceil));
    add_to_tree(nx,n);
  }
  return n;
}

void print_tree_inorder(Node* tr) {
  if (tr != NULL) {
    print_tree_inorder(tr->left);
    printf("%d ",tr->d);
    print_tree_inorder(tr->right);
  }
}

void destroy_tree(Node** tr) {
  if ((*tr)->left != NULL)
    destroy_tree((&(*tr)->left));
  if ((*tr)->right != NULL)
    destroy_tree(&((*tr)->right));
  free(*tr);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  /* default tree size can be overridden with first command line arg */
  size_t tree_sz = DEF_TREE_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&tree_sz);

  /* default ceiling to random number can be overridden with second command line arg */
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  time_t start_time = time(NULL);
  Node* btr = gen_tree(tree_sz,num_ceil);
  time_t end_time = time(NULL);
  if (tree_sz <= DEF_MAX_PRNT) {
    puts("Numbers from tree:");
    print_tree_inorder(btr);
    puts("");
  }
  printf("time needed for tree generation: %lu\n",end_time-start_time);
      
  destroy_tree(&btr);
  
  return 0;
}
