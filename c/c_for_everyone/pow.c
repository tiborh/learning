#include <stdio.h>

#define DEF_FMT "%d"

long my_pow(int base, int power) {
  if (power <= 0)
    return 1;
  return((long)base * my_pow(base,--power));
}

void proc_arg(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
}

int main(int argc, char** argv) {
  if (argc < 3) {
    printf("command line arguments: 1. base 2. power\n"
	   "(power is for positive integers only)\n");
    return 1;
  }
  int base=0,power=0;
  proc_arg(argv[1],&base);
  proc_arg(argv[2],&power);
  if (power < 0)
    return 2;

  printf("%d on the power of %d is %ld.\n",base,power,my_pow(base,power));
  
  return 0;
}
