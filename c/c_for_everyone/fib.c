#include <stdio.h>
#include <limits.h>

#define DEF_FMT "%hu"

void proc_arg(char* arg, short unsigned* nu) {
  short unsigned num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num;
}

void next_fib(long unsigned* n1, long unsigned* n2) {
  long unsigned temp = *n2;
  *n2 += *n1;
  *n1 = temp;
  return;
}

long unsigned fib(short unsigned n) {
  long unsigned n0=0,n1=1;
  if (n == 0)
    return n0;
  if (n == 1)
    return n1;
  for (short unsigned counter = 2; counter <= n; ++counter) {
    next_fib(&n0,&n1);
    if (n0 > n1)
      return ULONG_MAX;
  }
  return(n1);
}

int main(int argc, char** argv) {
  short unsigned num = 0;
  if (argc > 1)
    proc_arg(argv[1],&num);
  else {
    printf("fibonacci number <- %s <num>\n",argv[0]);
    return 1;
  }
  long unsigned fib_res = fib(num);
  if (fib_res == ULONG_MAX)
    puts("number is greater than can be processed");    
  else
    printf("fib(%hu) == %lu\n",num,fib_res);
  return 0;
}
