/*
 * Title: integers from file, calculation results into another file
 * Author: Tibor
 * Date: 2024-02-04
 *
 * Task Description:
 * 1. Open file for reading
 * 2. Read file content of integers into an array
 *    * the first integer tells you how many to read
 *    * the rest are the integer array
 *    So, for example,  4  9  11  12  15  would mean:
 *    a.) create an int array size 4 and
 *    b.) read in the remaining 4 values into data[].
 * 3. Then compute their
 *    a.) average as a double and
 *    b.) their max as an int.
 * 4. Print all this out neatly
 *    a.) to the screen and
 *    b.) to an output file named answer-hw3.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* default filename for input numbers
 * which can be overriden with first command line argument
 */
#define DEF_FN       "ints.txt"

typedef struct{
  double avg;
  int max;
} RESULTS;

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int* get_file_contents(FILE* fh) {
  size_t num_of_ints = 0;
  fscanf(fh,"%lu",&num_of_ints);
  int* int_arr = calloc(num_of_ints+1,sizeof(int));
  int_arr[0] = (int)num_of_ints; /* make conversion explicit */
  
  for(size_t i = 1; i <= num_of_ints; ++i) {
    fscanf(fh,"%d",&int_arr[i]);
  }

  return int_arr;
}

void print_results(RESULTS* res) {
  printf("Average: %lf\n",res->avg);
  printf("Max:     %d\n",res->max);
}

void calc_results(int* nums, RESULTS* res) {
  int num_of_nums = nums[0];	/* length is stored in index zero */
  long sum = 0;
  int max = nums[1];
  for (int i = 1; i <= num_of_nums; ++i) { /* starting from first data after length value */
    sum += nums[i];
    if (nums[i] > max)
      max = nums[i];
  }
  res->avg = (double)sum / num_of_nums;
  res->max = max;
  print_results(res);
}

/* two records on two different lines, label and value separated with tab '\t' */
void write_results_to_file(char* fn, RESULTS res) {
  FILE* fh = fopen(fn,"w");
  if (fh == NULL) { 		/* to help protect against non-existing file */
    fprintf(stderr,"Error at opening file for writing: %s\n",fn);
    exit(1);
  }
  fprintf(fh,"Average:\t%f\n",res.avg);
  fprintf(fh,"Max:\t%d\n",res.max);
  printf("Results have been written to: %s\n",fn);
  fclose(fh);
}

int main(int argc, char** argv) {
  char* in_fn = (argc > 1) ? argv[1] : DEF_FN;
  FILE* fh = fopen(in_fn,"r");
  if (fh == NULL) {
    fprintf(stderr,"Error at opening file for reading: %s\n",in_fn);
    exit(1);
  }

  int* arr_from_file = get_file_contents(fh);
  fclose(fh);

  size_t num_of_ints = arr_from_file[0];
  printf("Number of integers: %lu\n",num_of_ints);
  puts("the integers:");
  print_arr(num_of_ints,arr_from_file+1); /* to avoid printing the first element too */

  RESULTS res = {0};
  calc_results(arr_from_file,&res);
  free(arr_from_file);
  char* out_fn = "answer-hw3.txt";
  write_results_to_file(out_fn,res);
  
  return 0;
}

/***************************************************************************************************
 * input file generator                                                                            *
 ***************************************************************************************************/

/*
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_FN       "ints.txt"
#define DEF_NUM_INTS ((size_t)10)
#define DEF_NUM_CEIL ((int)100)
#define DEF_MAX_PRNT ((size_t)100)

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(size_t sz, int ceil, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

void arr_to_file(size_t sz,int* arr, FILE* fh) {
  fprintf(fh,"%lu\n",sz);
  for (size_t i = 0; i < sz; ++i)
    fprintf(fh,"%d ",arr[i]);
  fputc('\n',fh);
}

void print_file_contents(FILE* fh) {
  rewind(fh);
  size_t num_of_ints = 0;
  fscanf(fh,"%lu",&num_of_ints);
  puts("file contents:");
  printf("%lu\n",num_of_ints);
  int* int_arr = calloc(num_of_ints,sizeof(int));
  for(size_t i = 0; i < num_of_ints; ++i) {
    fscanf(fh,"%d",&int_arr[i]);
  }
  print_arr(num_of_ints,int_arr);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  char* out_fn = (argc > 1) ? argv[1] : DEF_FN;
  size_t num_of_ints = DEF_NUM_INTS;
  if (argc > 2)
    proc_arg_long_unsigned(argv[2],&num_of_ints);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 3)
    proc_arg_int(argv[3],&num_ceil);

  int* int_arr = calloc(num_of_ints,sizeof(int));
  fill_arr(num_of_ints,num_ceil,int_arr);
  
  puts("array to file:");
  printf("%lu\n",num_of_ints);
  print_arr(num_of_ints,int_arr);

  FILE* fh = fopen(out_fn,"w+");
  if (fh == NULL) {
    fprintf(stderr,"Error at opening file for writing: %s\n",out_fn);
    exit(1);
  }
  arr_to_file(num_of_ints,int_arr,fh);

  puts("File read back from file:");
  print_file_contents(fh);

  printf("Data has been written to: %s\n",out_fn);
  
  fclose(fh);
  
  free(int_arr);
  
  return 0;
}
*/
