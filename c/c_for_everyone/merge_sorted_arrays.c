#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> // sleep

#define DEF_ARR_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 50

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

void fill_arr(size_t sz, int ceil, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    arr[i] = rand() % ceil;
  return;
}

void print_arr(size_t sz, int* arr) {
  for (size_t i = 0; i < sz; ++i)
    printf("%d ",arr[i]);
  putchar('\n');
  return;
}

int comp_elems_p(const void* pn1, const void* pn2) {
  int n1 = *((unsigned*)pn1);
  int n2 = *((unsigned*)pn2);
  if (n1 < n2)
    return -1;
  if (n1 == n2)
    return 0;
  if (n1 > n2)
    return 1;
  return 10;
}

int *merge_arrays(size_t sz1, size_t sz2, int* arr1,int* arr2) {
  size_t merge_size = sz1 + sz2;
  int* arr_merged = calloc(merge_size,sizeof(int));
  // sleep(1);
  for (size_t i = 0, count1 = 0, count2 = 0; i < merge_size && (count1 < sz1 || count2 < sz2); ++i) {
    if (count1 < sz1 && count2 < sz2)
      if (arr1[count1] < arr2[count2])
	arr_merged[i] = arr1[count1++];
      else
	arr_merged[i] = arr2[count2++];
    else if (count1 >= sz1)
      arr_merged[i] = arr2[count2++];
    else if (count2 >= sz2)
      arr_merged[i] = arr1[count1++];
  }
  return arr_merged;
}

int main(int argc, char** argv) {
  size_t arr_sz = DEF_ARR_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&arr_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  if ((int)(arr_sz * 2) <= 0) {
    printf("size is too great (%lu)\n",arr_sz);
    return 1;
  }
  
  int* arr1 = calloc(arr_sz,sizeof(int));
  int* arr2 = calloc(arr_sz,sizeof(int));

  srand(time(NULL));
  puts("filling array 1");
  fill_arr(arr_sz,num_ceil,arr1);
  puts("filling array 2");
  fill_arr(arr_sz,num_ceil,arr2);
  puts("sorting array 1");
  qsort(arr1,arr_sz,sizeof(unsigned),comp_elems_p);
  puts("sorting array 2");
  qsort(arr2,arr_sz,sizeof(unsigned),comp_elems_p);

  time_t merge_start_time = time(NULL);
  printf("merge_start_time: %ld\n",merge_start_time);
  int* arr_merged = merge_arrays(arr_sz,arr_sz,arr1,arr2);
  time_t merge_end_time = time(NULL);
  printf("merge_end_time:   %ld\n",merge_end_time);

  if (arr_sz < DEF_PRINT_CEIL) {
    puts("array 1:");
    print_arr(arr_sz,arr1);
    puts("array 2:");
    print_arr(arr_sz,arr2);
    puts("merged array:");
    print_arr(arr_sz*2,arr_merged);
  }
  
  printf("Time needed for merge: %ld(s)\n",merge_end_time - merge_start_time);

  free(arr1);
  free(arr2);
  free(arr_merged);
  
  return 0;
}
