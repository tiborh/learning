/*******************************************************************************
 * Title: Sine Values Between Two Numbers (non-inclusive) (a simplified version)
 * Date: 2024-01-24
 * Author: Tibor
 * Usage: use it with command line argument (e.g. ./sine_simplified 0.1)
 * Comment: due to math.h library -lm switch might be needed by the compiler
 *          e.g. gcc sine_simplified.c -lm -o sine_simplified
 ******************************************************************************/
#include <stdio.h>
#include <math.h>

int main(int argc, char** argv) {
  const float START_X = 0;
  const float END_X = 1;
  float input_number = -1;

  /* first command line arg can serve as input */
  if (argc > 1) {
    int num_of_scanned = sscanf(argv[1],"%f",&input_number);
    if (num_of_scanned > 0) { // use only when a value has been read and
      if (input_number > START_X && input_number < END_X) // the value is within sensible range
	printf("sine of %f == %f\n",input_number,sinh(input_number));
      else
	puts("Command line argument value is out of range.");
    } else
      puts("Command line argument cannot be parsed as number.");
  } else
    printf("Give a number as a command line argument (greater than %f and less than %f)\n",START_X,END_X);
  
  return 0;
}
