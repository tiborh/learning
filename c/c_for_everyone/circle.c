#include <stdio.h>
#include <math.h>

double c_circ (double r) {
  return(2.0 * r * M_PI);
}
double c_area (double r) {
  return(r * r * M_PI);
}

int main(int argc, char** argv) {
  if (argc == 1) {
    puts("Give radius as command line argument.");
    return 1;
  }
  double r = 0.0;
  sscanf(argv[1],"%lf",&r);
  printf("Circumference: %7.2lf\n",c_circ(r));
  printf("Area:          %7.2lf\n",c_area(r));
  
  return 0;
}
