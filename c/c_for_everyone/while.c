#include <stdio.h>

#define DEF_FMT "%d"
const int START = 1;
const int END = 5;

void proc_arg(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,DEF_FMT,&num);
  if (res > 0)
    *nu = num - 1;
}

int main(int argc, char** argv) {
  int start = START - 1;
  int end = END - 1;
  if (argc > 1)
    proc_arg(argv[1],&start);
  if (argc > 2)
    proc_arg(argv[2],&end);

  int i = start;
  while (i++ < end)
    printf("%d ",i);
  printf("%d\n",i);
  
  return 0;
}
