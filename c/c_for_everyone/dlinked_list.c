/*
 * Title:  Double linked list with duplicate elimination
 * Author: Tibor
 * Date:   2024-02-03
 *
 * Task description:
 * 1. Modify the singly linked list to be a doubly linked list.
 * 2. Write a routine that removes all duplicate data in the doubly linked list.
 *    a.) The data will be integers generated at random from [0,49].
 *    b.) Initially have a list of 200 elements.  
 * Notes to possible implementations:
 * 1. Do this in one of two ways:
 *  I.
 *    a.) Sort the list by its data field.
 *    b.) Remove adjacent elements of the sorted list with the same value.
 *  II. Or,
 *    a.) take the first element
 *    b.) search the remainder of the list for elements with the same data and
 *    c.) remove them.
 *    d.) Then take the second element and do the same.
 *    e.) Repeat until only one element is left.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* some defaults that can be overriden with command line args */
#define DEF_LST_SIZ  ((size_t)200)
#define DEF_NUM_CEIL ((int)50)
/* this can only be overriden here, in the code: */
#define DEF_MAX_PRNT ((size_t)200)

/* data definition */
typedef int DATA;

typedef struct node{
  DATA d;
  struct node* next;
  struct node* prev;
} Node;

/* used for command line arg procession */
void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,"%d",&num);
  if (res > 0)
    *nu = num;
  return;
}

/* used for command line arg procession */
void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void init_node(Node* n, DATA d) {
  n->d = d;
  n->next = NULL;
  n->prev = NULL;
}

Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

/* push data */
void push_d(DATA d, Node** li) {
  Node* newn = create_node(d);
  newn->next = *li;
  if (*li != NULL)
    (*li)->prev = newn;
  *li = newn;
}

/* pop node */
Node* pop_n(Node **lst) {
  if (*lst == NULL)
    return NULL;
  Node* popped = *lst;
  *lst = (*lst)->next;
  if (*lst != NULL)
    (*lst)->prev = NULL;
  popped->next = NULL;
  return popped;
}

/* generate list filled with random numbers */
Node* gen_list(size_t sz, int ceil) {
  Node* outp = NULL;
  for (size_t i = 0; i < sz; ++i) {
    DATA d = rand() % ceil;
    push_d(d,&outp);
  }
  return outp;
}

/* pudh new node into list */
void push_n(Node* n, Node** li) {
  n->next = *li;
  (*li)->prev = n;
  *li = n;
}

/* insert node after lst */
void ins_n(Node* n, Node** lst) {
  n->prev = (*lst)->prev;
  (*lst)->prev = n;
  n->next = (*lst)->next;
  (*lst)->next = n;
}

/* insertion sort is marginally more efficient than bubble sort
 * and is easier to use for getting rid of duplicates
 */
void sort_list_insert(Node** lst) {
  puts("insertion sort");
  if (lst == NULL)
    return;
  Node* merged = pop_n(lst);
  while(*lst != NULL) {
    Node* n = pop_n(lst);
    if (n->d < merged->d) {
      push_n(n,&merged);
    } else if(n->d == merged->d) { /* omit duplicate data */
      free(n);
      continue;
    } else {
      Node* mrgp = merged;
      while (mrgp->next != NULL && n->d > mrgp->next->d)
	mrgp = mrgp->next;	/* wind forward */
      if (mrgp->next != NULL && mrgp->next->d == n->d) {
	free(n);
	continue;
      }
      ins_n(n,&mrgp);
    }
  }
  *lst = merged;
}

void print_list(Node* li) {
  Node* lipo = li;
  while (lipo != NULL) {
    printf("%d" " -> ",lipo->d);
    lipo = lipo->next;
  } 
  puts("NULL");
}

/* cleanup */
void destroy_list(Node** lst) {
  while (*lst != NULL) {
    Node* tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
}

int main(int argc, char** argv) {
  srand(time(NULL));
  /* default list size can be modified with first command line arg */
  size_t list_sz = DEF_LST_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&list_sz);
  int num_ceil = DEF_NUM_CEIL;
  /* default ceiling to random number can be modified with second command line arg */
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  Node* lst = gen_list(list_sz,num_ceil);
  if (list_sz <= DEF_MAX_PRNT) {
    puts("Unsorted:");
    print_list(lst);
  }
  
  time_t start_time = time(NULL);
  sort_list_insert(&lst);
  time_t end_time = time(NULL);
  
  if (list_sz <= DEF_MAX_PRNT) {
    puts("Sorted and duplicates elmininated:");
    print_list(lst);
  }
  /* starts to be interesting after n == 20000 ceiling == 100000 */
  printf("sorting time: %lu(s)\n",end_time-start_time);
  
  destroy_list(&lst);
  
  return 0;
}
