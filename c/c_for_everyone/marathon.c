#include <stdio.h>

#define KMS_IN_MILE 1.609344
#define YDS_IN_MILE 1760
#define MILES_IN_MARATHON 26
#define YARDS_IN_MARATHON 385

const double marathon_in_miles = (double)MILES_IN_MARATHON + ((double)YARDS_IN_MARATHON / (double)YDS_IN_MILE);
const double marathon_in_kms = (marathon_in_miles * (double)KMS_IN_MILE);

int main(void) {
  printf("A marathon is %5.2lf kilometers (%5.2lf miles).\n",marathon_in_kms,marathon_in_miles);
  
  return 0;
}
