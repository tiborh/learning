#include <stdio.h>
#include <string.h>

#define PRINT_FORMAT_LENGTH 16

int length_of_longest_line(int nu_of_lines,char** lines) {
  int length_of_longest = 0;
  for (int i = 0; i < nu_of_lines; ++i)
    if (strlen(lines[i]) > length_of_longest)
      length_of_longest = strlen(lines[i]);

  return length_of_longest;
}

int main(int argc, char** argv) {
  char* wandrers_nachtlied[] = {
    "Über allen Gipfeln",
    "Ist Ruh,",
    "In allen Wipfeln",
    "Spürest du",
    "Kaum einen Hauch;",
    "Die Vögelein schweigen im Walde.",
    "Warte nur, balde",
    "Ruhest du auch.",
    "(Johann Wolfgang von Goethe)"
  };
  char* wanderers_nightsong[] = {
    "O'er all the hilltops",
    "Is quiet now,",
    "In all the treetops",
    "Hearest thou",
    "Hardly a breath;",
    "The birds are asleep in the trees:",
    "Wait, soon like these",
    "Thou too shalt rest.",
    "(Henry Wadsworth Longfellow)"
};
  long num_of_lines = sizeof(wandrers_nachtlied) / sizeof(wandrers_nachtlied[0]);
  int len_longest = length_of_longest_line(num_of_lines,wandrers_nachtlied);
  char print_format_str[PRINT_FORMAT_LENGTH] = {0};

  sprintf(print_format_str,"%%-%ds\t%%s\n",len_longest);

  for(int i = 0; i < num_of_lines; ++i) {
    printf(print_format_str,wandrers_nachtlied[i],wanderers_nightsong[i]);
  }
  
  return 0;
}

