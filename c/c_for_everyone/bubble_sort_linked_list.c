#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_LST_SIZ 10
#define DEF_NUM_CEIL 100
#define DEF_PRINT_CEIL 101
#define FMT "%d"

typedef int DATA;

typedef struct node{
  DATA d;
  struct node* next;
} Node;

void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,FMT,&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void init_node(Node* n, DATA d) {
  n->d = d;
  n->next = NULL;
}

Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

void push(DATA d, Node** li) {
  Node* newn = create_node(d);
  newn->next = *li;
  *li = newn;
}

void print_list(Node* li) {
  Node* lipo = li;
  while (lipo != NULL) {
    printf(FMT " -> ",lipo->d);
    lipo = lipo->next;
  } 
  puts("NULL");
}

Node* gen_list(size_t sz, int ceil) {
  Node* outp = NULL;
  for (size_t i = 0; i < sz; ++i) {
    DATA d = rand() % ceil;
    push(d,&outp);
  }
  return outp;
}

void destroy_list(Node** lst) {
  while (*lst != NULL) {
    Node* tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
}

void swap_d(Node* ptr1, Node* ptr2) {
  //printf("swapping " FMT " and " FMT "\n",ptr1->d,ptr2->d);
  DATA tmp = ptr1->d;
  ptr1->d = ptr2->d;
  ptr2->d = tmp;
  return;
}

void sort_list(size_t sz, Node* lst) {
  bool change_flag = true;
  Node* lipo = lst;
  for (int i = 0, end_ind = sz; i < sz; ++i, --end_ind, lipo=lst) {
    if (change_flag == false)
      break;
    change_flag = false;
    for (int j = 0; j < end_ind+1 && lipo->next != NULL; ++j, lipo = lipo->next)
      if (lipo->d > lipo->next->d) {
	swap_d(lipo,lipo->next);
	change_flag = true;
      }
  }
}

int main(int argc, char** argv) {
  srand(time(NULL));
  size_t list_sz = DEF_LST_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&list_sz);
  int num_ceil = DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  Node* lst = gen_list(list_sz,num_ceil);

  if (list_sz < DEF_PRINT_CEIL) {
    puts("unsorted");
    print_list(lst);
  }
  time_t start_time = time(NULL);
  printf("start_time: %ld\n",start_time);
  sort_list(list_sz,lst);
  time_t end_time = time(NULL);
  printf("end_time: %ld\n",end_time);
  if (list_sz < DEF_PRINT_CEIL) {
    puts("sorted");
    print_list(lst);
  }
  printf("Time needed: %ld(s)\n",end_time - start_time);
  
  destroy_list(&lst);
  
  return 0;
}
