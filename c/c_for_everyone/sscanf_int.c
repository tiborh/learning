#include <stdio.h>

#define MAXLEN 100

int main(int argc, char** argv) {
  int val = 0;
  const int maxlen = (int)MAXLEN;
  char inp[(int)MAXLEN+1] = "";
  fgets(inp,maxlen,stdin);
  sscanf(inp,"%d",&val);
  printf("%d\n",val);
  
  return 0;
}
