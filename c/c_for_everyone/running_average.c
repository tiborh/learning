#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define DEF_FMT "%d"
const int DEF_NUM = 10;
const int DEF_CEIL = 100;

void proc_arg(char* arg, int* nu, char* fmt) {
  int num = 0;
  int res = sscanf(arg,fmt,&num);
  if (res > 0 && num > 0)
    *nu = num;
}

int main(int argc, char** argv) {
  int num_of_nums = DEF_NUM;
  int rand_ceil = DEF_CEIL;
  if (argc > 1)
    proc_arg(argv[1],&num_of_nums,DEF_FMT);
  if (argc > 2)
    proc_arg(argv[2],&rand_ceil,DEF_FMT);
  
  srand(time(NULL));
  double avg = 0.0;
  long int sum = 0;
  for (int i = 1; i <= num_of_nums; ++i) {
    int num = rand() % rand_ceil;
    sum += num;
    avg += ((double)num - avg) / i;
    printf("num: %3i, avg: %5.2f (naive avg: %5.2f)\n",num,avg,sum/(double)i);
  }
  
  return 0;
}
