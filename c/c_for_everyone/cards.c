/*
 * Title:  Card pattern match
 * Author: Tibor
 * Date:   2024-01-31
 * Description:
 * 1. Deal out 7 card hands and
 * 2. evaluate the probability that a hand has
 *    a. no pair,
 *    b. one pair,
 *    c. two pairs,
 *    d. three of a kind,
 *    e. full house and
 *    f. 4 of a kind.
 * 3. Use at least 1 million randomly generated hands.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SHORT_NAMES 1		/* a relatively simple way to swap between short names (1) and long numes (0) */
//#define DEBUG                 /* a clumsy way but I did not need to use too much, so OK for the purpose */
#define SUIT_SZ ((size_t)4)
#define PIPS_SZ ((size_t)13)
#define DECK_SZ ((size_t)(SUIT_SZ * PIPS_SZ))
#define HAND_SZ ((size_t)7)
#define MAX_DECK_DEAL ((size_t)(DECK_SZ/HAND_SZ)) /* after how many hands a reshuffle is needed */
#define DEF_NUM_OF_HANDS ((size_t)10)

typedef enum{clubs, diamonds, hearts, spades } Suit;
const char* suit_names[] = {"clubs", "diamonds", "hearts", "spades"}; /* in case the short, unicode symbols do not work */
const char* suit_symbols[] = {"♣","♦","♥","♠"}; /* unicode chars, which may or may not work well on various systems */
typedef enum{ace, two, three, four, five, six, seven,
	     eight, nine, ten, jack, queen, king} Pips;
const char* pips_names[] = {"ace", "two", "three", "four", "five", "six", "seven",
			    "eight", "nine", "ten", "jack", "queen", "king"};
const char* pips_symbols[] = {"A","2","3","4","5","6","7",
			      "8","9","10","J","Q","K"};

typedef struct{
  Suit suit;
  Pips pips;
} Card;

typedef struct{
  Card deck[DECK_SZ];
  size_t deal_count;		/* inportant for checking against MAX_DECK_HANDS */
  size_t next_index;
} Deck;

typedef struct{
  long unsigned nopa;		/* no pair */
  long unsigned onpa;		/* one pair */
  long unsigned twpa;		/* two pairs */
  long unsigned drei;		/* three pairs */
  long unsigned quad;		/* four pairs */
  long unsigned fuho;		/* full house */
  long unsigned alle;		/* the number of hands dealt */
} HandStat;

long unsigned add_all_hs(HandStat hs) {
  return(hs.nopa + hs.onpa + hs.twpa + hs.drei + hs.quad + hs.fuho);
}

double rat(long unsigned val, long unsigned all) {
  return(((double)val/all));
}

void print_hstat(HandStat hs) {
  printf("No  pair:        %10lu (%f)\n",hs.nopa,rat(hs.nopa,hs.alle));
  printf("One pair:        %10lu (%f)\n",hs.onpa,rat(hs.onpa,hs.alle));
  printf("Two pairs:       %10lu (%f)\n",hs.twpa,rat(hs.twpa,hs.alle));
  printf("Three of a kind: %10lu (%f)\n",hs.drei,rat(hs.drei,hs.alle));
  printf("Four  of a kind: %10lu (%f)\n",hs.quad,rat(hs.quad,hs.alle));
  printf("Full house:      %10lu (%f)\n",hs.fuho,rat(hs.fuho,hs.alle));
  printf("Check Sum:       %10lu\n",add_all_hs(hs)); /* a check if all hands are covered and none counted more than once */
}

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

void print_card(Card c) {
  printf("(%s of %s) ",pips_names[c.pips],suit_names[c.suit]);
}

void print_card_short(Card c) {
  printf("%s%s ",pips_symbols[c.pips],suit_symbols[c.suit]);
}

void print_deck_status(Deck d) {
  printf("deal count: %lu\n",d.deal_count);
  printf("next index: %lu\n",d.next_index);
  puts("next deal preview: ");
  for (size_t i = d.next_index; i < DECK_SZ && i < d.next_index + HAND_SZ; ++i)
    print_card_short(d.deck[i]);
  puts("_");
}

void print_cards(size_t sz, Card* d) {
  for (size_t i = 0; i < sz; ++i) {
    if (SHORT_NAMES)
      print_card_short(d[i]);
    else
      print_card(d[i]);
  }
  puts("");
}

void fill_deck(Card* d) {
  for (size_t s = 0, i = 0; s < SUIT_SZ; ++s)
    for (size_t p = 0; p < PIPS_SZ; ++p,++i) {
      d[i].suit = (Suit)s;
      d[i].pips = (Pips)p;
      //print_card(d[i]);
    }
}

void init_deck(Deck* d) {
  fill_deck(d->deck);
  d->deal_count = 0;
  d->next_index = 0;
}

void swap_cards(Card* c1, Card* c2) {
  Card tmp = *c1;
  *c1 = *c2;
  *c2 = tmp;
}

void shuffle_cards(size_t sz, Card* d) {
  for (size_t i = sz-1; i > 0; --i) {
    size_t new_place = rand() % i;
    swap_cards(&d[i],&d[new_place]);
  }
}

void reset_deck(Deck* d) {
  shuffle_cards(DECK_SZ,d->deck);
  d->deal_count = 0;
  d->next_index = 0;
  //puts("deck has been reset");
}

void deal_hand(Deck* d, Card* hand) {
  if (d->deal_count == MAX_DECK_DEAL)
    reset_deck(d);
  size_t stop_index = d->next_index + HAND_SZ;
  for (size_t i = 0, j = d->next_index; j < stop_index && j < DECK_SZ; ++i, ++j, ++d->next_index) {
    hand[i].suit = d->deck[j].suit;
    hand[i].pips = d->deck[j].pips;
  }
  ++d->deal_count;
}

void print_array(size_t sz, long unsigned* arr) {
  for(size_t i = 0; i < sz; ++i)
    if (arr[i] > 1)
      printf("%lu: %lu\n",i,arr[i]);
}

/*
 * Intended logic of evaluation:
 * 1. the same card is not counted in two different patterns at the same time
 *    (e.g. both inside a full house and in a pair, a seven-card hand gives an further difficulty
 *     by the two remaining cards's ability to also form an additional pair)
 * 2. an order of evaluation is important:
 *      i. no pair is checked, which means every pips can appear max once
 *         (no need to go further if this is the case)
 *     ii. look for four of a kind (a pair or a three of a kind outside it is also possible) (as one
 *         deck is used, five, six, and seven of a kind are all impossible)
 *    iii. look for full house (all the "triple and pair cases" can branch out from this examination
 *         -- full house + pair	// commented out
 *         -- no triple but pair(s) only (two pairs, possibly with a third pair (commented out), or one pair)
 *         -- triple but no pair
 */
/* fill up the collector array */
void collect_data(long unsigned* coll, Card* h, HandStat* hs) {
  for(size_t i = 0; i < HAND_SZ; ++i)
    ++coll[h[i].pips];
}

bool no_pair(long unsigned* coll) {
  bool nopa = true;
  for (size_t i = 0; i < PIPS_SZ; ++i)
    if (coll[i] > 1)
      nopa =  false;
  return nopa;
}

bool check_four(long unsigned* coll) {
  bool quad = false;
  for (size_t i = 0; i < PIPS_SZ; ++i)
    if (coll[i] == 4)
      quad = true;
  return quad;
}

short unsigned num_of_pairs(long unsigned* coll) {
  short unsigned n_pa = 0;
  for (size_t i = 0; i < PIPS_SZ; ++i)
    if (coll[i] == 2)
      ++n_pa;
  return n_pa;
}

short unsigned num_of_triple(long unsigned* coll) {
  short unsigned ntri = 0;
  for (size_t i = 0; i < PIPS_SZ; ++i)
    if (coll[i] == 3)
      ++ntri;
  return ntri;
}

void eval_quatripar(HandStat* hs, long unsigned* coll) {
  bool is_qua = check_four(coll);
  short unsigned n_pa = num_of_pairs(coll);
  short unsigned ntri = num_of_triple(coll);
  if (is_qua)
    hs->quad++;
  else if (n_pa > 0 && ntri > 0) {	/* full house */
    hs->fuho++;
    //    if (n_pa > 1)		/* a pair over a full house */
    //      hs->onpa++;
  } else if (n_pa > 0) {
    if (n_pa > 1) {		/* two pairs */
      hs->twpa++ ;
      //      if (n_pa > 2)		/* a pair over two pairs */
      //	hs->onpa++;
    } else			/* a pair (and nothing else,
				   or over four of a kind) */
      hs->onpa++;
  } else if (ntri > 0)		/* three of a kind */
    hs->drei++;
}

void eval_hand(Card* h, HandStat* hs) {
  long unsigned collector[PIPS_SZ] = {0};
  collect_data(collector,h, hs);
  if (no_pair(collector)) {
    hs->nopa++;
    return;
  }
  eval_quatripar(hs,collector);
  #ifdef DEBUG
  print_cards(HAND_SZ,h);
  print_array(PIPS_SZ,collector);
  #endif
}

int main(int argc, char** argv) {
  srand(time(NULL));
  size_t num_of_hands = DEF_NUM_OF_HANDS;
  if (argc > 1)			/* command line arg can overwride the default number */
    proc_arg_long_unsigned(argv[1],&num_of_hands);
  Deck d = {0};
  Card h[HAND_SZ] = {0};
  init_deck(&d);
  #ifdef DEBUG
  puts("Original deck:");
  print_cards(DECK_SZ,d.deck);
  #endif
  reset_deck(&d);
  #ifdef DEBUG
  puts("Shuffled deck:");
  print_cards(DECK_SZ,d.deck);
  puts("Hands dealt:");
  #endif
  
  HandStat hs = {0};
  hs.alle = num_of_hands;
  for (size_t i = 0; i < num_of_hands; ++i) {
    deal_hand(&d,h);
    eval_hand(h,&hs);
  }

  print_hstat(hs);
  
  return 0;
}
