/*
 * Title:  Bubble Sort in (Single) Linked Link List
 * Author: Tibor
 * Date:   2024-02-02
 * Task Description:
 * 1. Use the linear linked list code to store a randomly generated set of 100 integers.
 * 2. Write a routine that will rearrange the list in sorted order of these values.
 *    Note: you might want to use bubble sort to do this.
 * 3. Print these values in rows of 5 on the screen.
 * Comments:
 * How will this work?
 * 1. Compare two adjacent list elements and
 * 2. if they are out of order swap them.
 * 3. After a first pass the largest element will have bubbled to the end of the list.
 *    Each pass can look at one less element as the end of a list stays sorted.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEF_LST_SIZ 100
#define DEF_NUM_CEIL 1000
#define DEF_PRINT_CEIL 101
#define DEF_NU_IN_ROW 5
#define FMT "%3d"

/* definition of data */
typedef int DATA;
typedef struct node{
  DATA d;
  struct node* next;
} Node;

/* command line arg procession */
void proc_arg_int(char* arg, int* nu) {
  int num = 0;
  int res = sscanf(arg,FMT,&num);
  if (res > 0)
    *nu = num;
  return;
}

void proc_arg_long_unsigned(char* arg, long unsigned* nu) {
  long unsigned num = 0;
  int res = sscanf(arg,"%lu",&num);
  if (res > 0)
    *nu = num;
  return;
}

/* adding values to members */
void init_node(Node* n, DATA d) {
  n->d = d;
  n->next = NULL;
}

/* allocating memory */
Node* create_node(DATA d) {
  Node* n = calloc(1,sizeof(Node));
  init_node(n,d);
  return n;
}

/* adding a node on top */
void push(DATA d, Node** li) {
  Node* newn = create_node(d);
  newn->next = *li;
  *li = newn;
}

/* printing full list */
void print_list(Node* li) {
  const size_t nu_in_row = (size_t) DEF_NU_IN_ROW;
  Node* lipo = li;
  for (size_t i = 0; lipo != NULL; ++i, lipo = lipo->next) {
    if (i % nu_in_row == 0)
      puts("");
    printf(FMT " -> ",lipo->d);
  } 
  puts("NULL");
}

/* generating a list of given size filled with random integers */
Node* gen_list(size_t sz, int ceil) {
  Node* outp = NULL;
  for (size_t i = 0; i < sz; ++i) {
    DATA d = rand() % ceil;
    push(d,&outp);
  }
  return outp;
}

/* some housekeeping so that valgrind does not complain */
void destroy_list(Node** lst) {
  while (*lst != NULL) {
    Node* tmp = *lst;
    *lst = (*lst)->next;
    free(tmp);
  }
}

/* swapping values between two nodes */
void swap_d(Node* ptr1, Node* ptr2) {
  //printf("swapping " FMT " and " FMT "\n",ptr1->d,ptr2->d);
  DATA tmp = ptr1->d;
  ptr1->d = ptr2->d;
  ptr2->d = tmp;
  return;
}

/* sortint list with bubble sort */
void sort_list(size_t sz, Node* lst) {
  bool change_flag = true;
  Node* lipo = lst;
  for (int i = 0, end_ind = sz; i < sz; ++i, --end_ind, lipo=lst) {
    if (change_flag == false)
      break;
    change_flag = false;
    for (int j = 0; j < end_ind+1 && lipo->next != NULL; ++j, lipo = lipo->next)
      if (lipo->d > lipo->next->d) {
	swap_d(lipo,lipo->next);
	change_flag = true;
      }
  }
}

int main(int argc, char** argv) {
  srand(time(NULL));
  /* see if any command line args have been given */
  size_t list_sz = (size_t)DEF_LST_SIZ;
  if (argc > 1)
    proc_arg_long_unsigned(argv[1],&list_sz);
  int num_ceil = (size_t)DEF_NUM_CEIL;
  if (argc > 2)
    proc_arg_int(argv[2],&num_ceil);

  Node* lst = gen_list(list_sz,num_ceil);

  if (list_sz < DEF_PRINT_CEIL) {
    puts("unsorted");
    print_list(lst);
  }
  time_t start_time = time(NULL);
  printf("start_time: %ld\n",start_time);
  sort_list(list_sz,lst);
  time_t end_time = time(NULL);
  printf("end_time: %ld\n",end_time);
  if (list_sz < DEF_PRINT_CEIL) {
    puts("sorted");
    print_list(lst);
  }
  printf("Time needed: %ld(s)\n",end_time - start_time);
  
  destroy_list(&lst);
  
  return 0;
}
