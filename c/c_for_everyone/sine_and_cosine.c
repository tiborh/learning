/*******************************************************************************
 * Title: Sine Values Between Two Numbers (non-inclusive)
 * Date: 2024-01-24
 * Author: Tibor
 * Comments: 1. due to math.h library -lm switch might be needed by the compiler
 *              e.g. gcc sine_and_cosine.c -lm -o sine_and_cosine
 *           2. you can use command line args to change the increment
 *              e.g. ./singe_and_cosine 0.01
 ******************************************************************************/
#include <stdio.h>
#include <math.h>

#define DEFAULT_INCREMENT 0.1

int main(int argc, char** argv) {
  const double START_X = 0.0;
  const double END_X = 1.0;
  const int INCR_FLOOR = 0;
  const int INCR_CEIL = 1;
  float incr = (float)DEFAULT_INCREMENT;

  /* first command line arg can modify the default increment */
  if (argc > 1) {		/* no arg: argc == 1 */
    double new_incr = 0;
    int num_of_scanned = sscanf(argv[1],"%lf",&new_incr); /* argv[0]: program name
							     argv[1]: first arg */
    if (num_of_scanned > 0) { // use only when a value has been read and
      if (new_incr > (double)INCR_FLOOR && new_incr < (double)INCR_CEIL) // the value is within sensible range
	incr = new_incr;
      else
	puts("Command line argument value is out of range.");
    } else
      puts("Command line argument cannot be parsed as number.");
  }

  /* datapoint calculation and printing */
  for(double curr_x = START_X; curr_x <= END_X; curr_x += incr)
    printf("x = %f\tsin(x) = %f\tcos(x) = %f\n",curr_x,sin(curr_x),cos(curr_x));
  
  return 0;
}
