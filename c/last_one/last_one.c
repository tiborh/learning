#include "last_one.h"

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <integer>\n",basename(argv0));
}

void print_result(int res) {
  char* binstr = dec2bin(res);
  printf("%d %s\n",res,binstr);
  free(binstr);
}
