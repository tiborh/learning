#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void print_array(int size, int* a) {
  for(int i = 0; i < size; ++i)
    printf("%d ",*(a + i));
  putchar('\n');
}

void fill_array(int alength, int* a, int rand_ceiling) {
  srand(time(NULL));
  for(int i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

int main (int argc, char** argv) {
  int length_ceiling = 10;
  int value_ceiling = 100;

  if (argc == 1)
    printf("%s [length ceiling (%d)] [number ceiling (%d)]\n",basename(argv[0]),length_ceiling,value_ceiling);
  if (argc > 1)
    sscanf(argv[1],"%d",&length_ceiling);
  assert(length_ceiling > 0);
  if (argc > 2)
    sscanf(argv[2],"%d",&value_ceiling);
  assert(value_ceiling > 1);

  srand(time(NULL)/7);
  int array_length = (rand() % length_ceiling);
  printf("Selected length: %d\n",array_length);
  int* arr = calloc(array_length,sizeof(int));

  if (array_length == 0)
    puts("Zero length array.");
  else {
    fill_array(array_length,arr,value_ceiling);
    print_array(array_length,arr);
  }
  
  free(arr);
  return 0;
}
