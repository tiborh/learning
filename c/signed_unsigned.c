#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

int cmp(unsigned char c0, unsigned char c1) {
  return(c1 - c0);
}

int main (int argc, char** argv) {
  unsigned char c0 = 12;
  unsigned char c1 = 10;
  printf("%u - %u == %d\n", c1, c0, c1 - c0);
  printf("cmp(%u,%u) == %d\n",c0,c1,cmp(c0,c1));
  printf("cmp(%u,%u) == %d\n",c1,c0,cmp(c1,c0));
  
  return 0;
}
