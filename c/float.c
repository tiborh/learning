#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */

int main (int argc, char** argv) {
    printf("size of float: %d\n", sizeof(float));
    printf("size of double: %d\n", sizeof(double));
    printf("size of long double: %d\n", sizeof(long double));
  return 0;
}
