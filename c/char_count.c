#include <stdio.h>
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
// #include <stdlib.h>		/* for exit */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

int main (int argc, char** argv) {

  int blanks = 0, digits = 0, letters = 0, punctuation=0, mathematical=0, others = 0, symbols = 0;
  //  int newlines = 0;
  int c; 			/* for storing characters */
  while((c = getchar()) != EOF) {
    if (c == ' ' || c == '\n')
      ++blanks;
    //    else if (c == '\n') // does not work
    //      ++newlines;
    else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
      ++letters;
    else if (c >= '0' && c <= '9')
      ++digits;
    else if (c == '.' || c == ',' || c == '-' || c == ':' || c == '(' || c == ')' || c == '"' || c == ';' || c == '\'' || c == '!' || c == '?')
      ++punctuation;
    else if (c == '/' || c == '*' || c == '=' || c == '%')
      ++mathematical;
    else if (c == '$' || c == '@' || c == '_')
      ++symbols;
    else {
      ++others;
      putchar(c);
      printf("(%x)",c);
    }
  };
  putchar(10);

  printf("blanks = %d\n",blanks);
  //  printf("newlines = %d\n",newlines);
  printf("letters = %d\n",letters);
  printf("digits = %d\n",digits);
  printf("punctuation = %d\n",punctuation);
  printf("mathematical = %d\n",mathematical);
  printf("symbols = %d\n",symbols);
  printf("other = %d\n",others);
  printf("all = %d\n", blanks + letters + digits + punctuation + mathematical + others);
  
  return 0;
}
