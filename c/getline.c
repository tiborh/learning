#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

#define MAX_LINE_LENGTH 80

char* getline() {
  char* the_line = calloc(MAX_LINE_LENGTH,sizeof(char)) + 1;
  int i = 0, c = getchar();
  if (c == EOF)
    return NULL;
  for (; c != '\n' && c != EOF && i < MAX_LINE_LENGTH; c = getchar(), ++i)
    *(the_line + i) = c;

  if (c > 0 && c != '\n')
    ungetc(c,stdin);
  
  if (*(the_line + i - 1) == '\n')
    *(the_line + i - 1) = '\0';

  return the_line;
}

int main (int argc, char** argv) {

  char* a_line;
  while(NULL != (a_line = getline())) {
    puts(a_line);
  }

  return 0;
}
