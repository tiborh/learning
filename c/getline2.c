#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

const size_t MAX_LINE_LENGTH = 80;

void print_array(size_t size, char* array) {
  for (int i = 0; i < size; ++i)
    putchar(*(array+i));
  putchar('\n');
}

char* getline() {
  size_t maxlinelength = MAX_LINE_LENGTH;
  char* the_line = malloc(maxlinelength + 1);
  size_t i = 0;
  int c = getchar();
  if (c == EOF)
    return NULL;
  for (; c != '\n' && c != EOF && i < maxlinelength; c = getchar(), ++i) {
    *(the_line + i) = c;
    printf("i: %lu, c: %c (0x%x)\n",i,c,c);
    if (i + 1 == maxlinelength && c != EOF) {
      printf("current size: %lu\n", maxlinelength);
      puts("content so far:");
      print_array(maxlinelength,the_line);
      maxlinelength *= 2;
      printf("new size: %lu\n", maxlinelength);
      if (!realloc(the_line, maxlinelength + 1)) {
	maxlinelength /= 2;
	printf("Unable to increase linelength over %lu.\n",maxlinelength);
      }
    }
  }

  if (c > 0 && c != '\n')
    ungetc(c,stdin);
  
  if (*(the_line + i - 1) == '\n')
    *(the_line + i - 1) = '\0';

  return the_line;
}

int main (int argc, char** argv) {

  puts("Faulty when realloc is used.");
  exit(-1);

  char* a_line;
  while(NULL != (a_line = getline())) {
    puts(a_line);
  }

  return 0;
}
