/*******************************************************************************
 * Title: Eliminating Duplicates in Double Linked List
 * Author: Tibor
 * Date: 2019-12-22
 * Version: 0.1
 *
 * Task Description:
 * -----------------
 * 1. Modify the singly linked list to be a doubly linked list. 
 * 2. Write a routine that removes all duplicate data in the doubly linked list. 
 * 3. The data will be integers generated at random from [0,49].
 * 4. Initially have a list of 200 elements.
 *
 * Implementation Options:
 * -----------------------
 * Do this in one of two ways. 
 * 1. 
 *   a. Sort the list by its data field. 
 *   b. Remove adjacent elements of the sorted list with the same value. 
 * 2. Or, 
 *   a. take the first element
 *   b. search the remainder of the list for elements with the same data
 *   c. remove them. 
 *   d. Then take the second element and do the same
 *   e. Repeat until only one element is left.
 *
 * Implementation choice: 
 * Option 1
 *
 * Implementation Notes:
 * ---------------------
 * 1. The task could be implemented using single linked list too.
 *    Where double-linked list helps is the removal of a list item.
 * 2. A minimalist approach is maintained:
 *    a. as only 50 different numbers are needed, unsigned char is used for
 *       storing the numbers.
 *    b. as only one list is needed, the head and tail pointers will be common
 *       and for this reason need not appear as function arguments
 *    c. only those functions are copied here (from the playground used
 *       during experimenting with double-linked lists) that are needed for the
 *       task implementation and testing of the solution.
 *******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>

#define UPPER_BOUND 50
#define NUM_OF_NUMS (UPPER_BOUND * 4)
#define SEP "\t"
#define BREAK_AFTER 5

typedef struct list_item {unsigned char ch; struct list_item* next; struct list_item* prev; } li;
/* to load and to unload the stack: */
void push_front(unsigned char);
unsigned char pop_front(void);
unsigned int get_length(void);	/* the efficient way */
unsigned int check_length(void); /* the option used for debugging */
void print_list(void);		 /* it helps visualise the double-linked list */
void print_numeric_list(void);	 /* numeric representation of the chars */
void remove_neighbouring_duplicates(void); /* effective only in sorted lists */
void sort_list(void);
void empty_list(void);
bool is_empty(void);

int main (int argc, char** argv) {
  /* to help experimentation: 
   * first arg can give the upper bound of rand()
   * second arg can give how many numbers to stuff into the stack. */
  unsigned char upper_bound = argc > 1 ? atoi(argv[1]) : UPPER_BOUND;
  unsigned char num_of_nums = argc > 2 ? atoi(argv[2]) : NUM_OF_NUMS;

  printf("upper_bound: %u\n",upper_bound);
  printf("num_of_nums: %u\n",num_of_nums);

  /* fill up the stack */
  for(unsigned char i = 0; i < num_of_nums; ++i) {
    unsigned char c = rand() % upper_bound;
    push_front(c);
  }
  puts("Unsorted list:");
  print_numeric_list();
  sort_list();
  puts("Sorted list:");
  print_numeric_list();
  puts("After removal of duplicates:");
  remove_neighbouring_duplicates();
  print_numeric_list();
  empty_list();			/* used when checking for memory leaks 
				   with valgrind */
  
  return 0;
}

static li* head = NULL;
static li* tail = NULL;
static unsigned int list_length = 0;

void push_front(unsigned char c) {
    li* an_item = calloc(1,sizeof(li));
    an_item->ch = c;
    if (head)
        head->prev = an_item;
    an_item->next = head;
    an_item->prev = NULL;
    if (!tail)
        tail = an_item;
    head = an_item;
    ++list_length;
}

unsigned char pop_front(void) {
    if(head == NULL)
        return 0;
    unsigned char ret_ch = head->ch;
    li* saved_head = head;
    if (head == tail)
        tail = NULL;
    head = head->next;
    if (head)
        head->prev = NULL;
    free(saved_head);
    --list_length;
    return ret_ch;
}

unsigned int get_length(void) {
    return list_length;
}

unsigned int check_length(void) {
    unsigned int i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; tmp_ptr; ++i,tmp_ptr = tmp_ptr->next);
    return i;
}

void print_list(void) {
    if (!list_length) {
        puts("NULL");
        return;
    }
    li* tmp_ptr = head;
    printf("NULL < ");
    while(tmp_ptr) {
        printf("%c ",tmp_ptr->ch);
        tmp_ptr = tmp_ptr->next;
        if (tmp_ptr)
            printf("<-> ");
    }
    puts("> NULL");
}

void print_numeric_list(void) {
    li* tmp_ptr = head;
    for(int i = 1; i <= list_length && tmp_ptr; ++i) {
        printf("%d%s",tmp_ptr->ch,SEP);
        tmp_ptr = tmp_ptr->next;
        if (!(i % BREAK_AFTER))
            putchar('\n');
    }
    putchar('\n');
}

/* used by sort */
static void swap(li* li0, li* li1) {
    unsigned char tmp = li0->ch;
    li0->ch = li1->ch;
    li1->ch = tmp;
}

/* used by sort:
 * less than zero: li0->ch is greater
 * zero: the two chars are equal
 * more than zero: li1-ch is greater
 */
static int li_cmp(li* li0, li* li1) {
    return(li1->ch - li0->ch);
}

/* bubble sort */
void sort_list(void) {
    bool change_watch = true;	/* helps stop going through an already sorted list */
    for(unsigned int upper_bound = list_length; upper_bound > 1 && change_watch; --upper_bound) {
        li* tmp_ptr = head->next;
        li* prev_ptr = head;
        change_watch = false;
        for(unsigned int i = 0; i < upper_bound; ++i) {
            if (li_cmp(prev_ptr,tmp_ptr) < 0) {
                swap(prev_ptr,tmp_ptr);
                change_watch = true;
            }
            prev_ptr = tmp_ptr;
            if (tmp_ptr->next)
                tmp_ptr = tmp_ptr->next;
        }
    }
}

/* works for sorted lists only */
void remove_neighbouring_duplicates(void) {
    if (list_length < 2)
        return;
    li* tmp_ptr = head->next;
    while(tmp_ptr) {
        if (tmp_ptr->prev->ch == tmp_ptr->ch) {
            tmp_ptr->prev->next = tmp_ptr->next;
            if (tmp_ptr->next)
                tmp_ptr->next->prev = tmp_ptr->prev;
            li* saved_ptr = tmp_ptr->next;
            free(tmp_ptr);
            --list_length;
            tmp_ptr = saved_ptr;
        } else
            tmp_ptr = tmp_ptr->next;
    }
}

/* remove all elements */
void empty_list(void) {
    while(head)
        pop_front();
}

/* check if empty */
bool is_empty(void) {
    return(head == NULL);
}
