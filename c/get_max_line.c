#include <stdio.h>
#include <stdlib.h>		/* for exit,abs */
#include <string.h>

const size_t MAX_LINE_LENGTH = 80;

void copy_array(char* from, char* to, size_t size) {
  for (size_t i = 0; i < size; ++i)
    *(to + i) = *(from + i);
}

char* getline() {
  size_t maxlinelength = MAX_LINE_LENGTH;
  char* the_line = calloc(maxlinelength + 1, sizeof(char));
  size_t i = 0;
  int c = getchar();
  if (c == EOF)
    return NULL;
  for (; c != '\n' && c != EOF && i < maxlinelength; c = getchar(), ++i) {

    *(the_line + i) = c;

    if (i + 1 == maxlinelength && c != EOF) {
      maxlinelength *= 2;
      char* new_line = calloc(maxlinelength + 1, sizeof(char));
      copy_array(the_line,new_line,maxlinelength/2);
      free(the_line);
      the_line = new_line;
    }
  }

  if (c > 0 && c != '\n')
    ungetc(c,stdin);
  
  if (*(the_line + i - 1) == '\n')
    *(the_line + i - 1) = '\0';

  return the_line;
}

int main (int argc, char** argv) {

  char* a_line;
  char* saved_line = calloc(1,sizeof(char));
  size_t maxlength = 0, num_of_lines = 0;
  while(NULL != (a_line = getline())) {
    //puts(a_line);
    size_t line_length = strlen(a_line);
    ++num_of_lines;
    if (line_length > maxlength) {
      maxlength = line_length;
      free(saved_line);
      saved_line = calloc(line_length+1,sizeof(char));
      copy_array(a_line,saved_line,line_length);
    }
  }
  printf("number of lines: %lu\n",num_of_lines);
  printf("max line length: %lu\n",maxlength);
  puts("The longest line:");
  puts(saved_line);
  
  return 0;
}
