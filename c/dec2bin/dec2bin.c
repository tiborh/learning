#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFWDTH 16
#define ASCII_SHIFT 48

/* https://www.programmingsimplified.com/c-program-reverse-string */
void rev_str(char *x, int begin, int end) {
   char c;

   if (begin >= end)
      return;

   c          = *(x+begin);
   *(x+begin) = *(x+end);
   *(x+end)   = c;

   rev_str(x, ++begin, --end);
}

char* dec2bin(int inpn, size_t wdth) {
  char* bin_val = calloc(wdth,sizeof(char));
  if (inpn == 0) {
    bin_val[0] = '0';
    bin_val[1] = '\0';
  } else {
    int i = 0;
    int prev_inpn;
    while(inpn != 0) {
      printf("inpn: %d, i: %d\n",inpn,i);
      char c = (char)(abs(inpn % 2)+(int)ASCII_SHIFT);
      bin_val[i++] = c;
      // inpn /= 2;
      prev_inpn = inpn;
      inpn >>= 1;
      if (i > wdth-1) {
	wdth <<= 1;
	bin_val = realloc(bin_val,wdth*sizeof(char));
      }
      if (inpn == prev_inpn)
	break;
    }
    rev_str(bin_val,0,i-1);
    bin_val[i] = '\0';
  }
  
  return bin_val;
}

int bin2dec(char* binstr) {
  int outnum = 0;
  int i,multip;
  for(i = strlen(binstr) - 1, multip = 1; i >= 0; --i, multip <<= 1)
    outnum += ((int)(binstr[i])-(int)ASCII_SHIFT)*multip;
  
  return outnum;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    puts("no value to convert");
    return 1;
  }
  int in_val = atoi(argv[1]);
  printf("input value: %d\n",in_val);
  char* bin_str = dec2bin(in_val,(size_t)DEFWDTH);
  printf("binary string: \"%s\"\n",bin_str);
  int check_val = bin2dec(bin_str);
  printf("back conversion check: %d\n",check_val);
  free(bin_str);
  
  return 0;
}
