#include <stdio.h>

int main() {

  char buffer[32];

  printf("Type something: ");
  scanf("%[abcABC]", buffer);	/* anything not in [abcABC] halts the input
				   even if later an accepted char is present */
  printf("chars read: |%s|\n", buffer);

  return(0);
}
