#include <stdio.h>

int main() {
  char nonstring[] = {
    'g', 'r', 'e', 'e', 't',
    'i', 'n', 'g', 's', ',',
    ' ', 'h', 'u', 'm', 'a', 'n'
  };

  char data[] = { 127, 129, 255 };

  printf("|%s|\n", nonstring);

  return(0);
}
