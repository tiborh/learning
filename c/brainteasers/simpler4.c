#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUMOFNUMS 10
#define MAXNUM 100

typedef struct {
  int n;
  int d;
} Pair ;

int gcd(int n,int d) {
  int larger, smaller, diff;

  larger = n>d ? n : d;
  smaller = n<d ? n : d;
  diff = larger-smaller;

  while( diff!=larger) {
    larger = smaller>diff ? smaller : diff;
    smaller = smaller==larger ? diff : smaller;
    diff = larger-smaller;
  }

  return(diff);
}

Pair simplifier(int n,int d) {
  int diff = gcd(n,d);
  
  Pair ret_vals;
  ret_vals.n = diff > 1 ? n/diff : n;
  ret_vals.d = diff > 1 ? d/diff : d;
  
  return(ret_vals);
}

int main(int argc, char** argv) {
  int num = (argc < 2) ? (int)NUMOFNUMS : atoi(argv[1]);
  int maxnum = (argc < 3) ? (int)MAXNUM : atoi(argv[2]);
  
  for (int i = 0; i < num; ++i) {
    int n, d;
    n = (rand() % maxnum) + 1;
    d = (rand() % maxnum) + 1;

    printf("%d/%d = ", n, d);

    Pair nd = simplifier(n,d);

    printf("%d/%d\n",nd.n,nd.d);
  }

  return(0);
}
