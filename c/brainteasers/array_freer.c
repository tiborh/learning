#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#define DEFSIZE 12
#define MAXNUM 100
#define FMTSIZE 32

void usage(char* progname) {
  printf("Usage:\n\t %s [array size (default: %d)] [max num (default: %d)]\n",
	 progname,(int)DEFSIZE,(int)MAXNUM);
}

int get_num_width(int num) {
  return((int)(log(num)/log(10))+1);
}

int main(int argc, char** argv) {
  if (argc > 1 && (strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"--help") == 0))
    usage(argv[0]);
  srand( (unsigned)time(NULL) );
  int asize = argc > 1 ? atoi(argv[1]) : (int)DEFSIZE;
  int maxnu = argc > 2 ? atoi(argv[2]) : (int)MAXNUM;
  int *array;
  int x;
  char fmt[FMTSIZE];
  
  sprintf(fmt," %%%dd",get_num_width(maxnu));
  //puts(fmt);
  array = calloc((size_t)asize,sizeof(int));
   
  for( x=0; x < asize; x++ ) {
    //printf("i: %d ",x);
    array[x] = (rand() % maxnu)+1;
    printf(fmt, array[x]);
    if ((x + 1) % 10 == 0)
      putchar('\n');
  }
  
  putchar('\n');

  free(array);
  
  return(0);
}
