#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ASIZE 12

int main() {
  const int asize = (int)ASIZE;
  int array[asize];
  int x;
  
  srand( (unsigned)time(NULL) );
  
  for( x=0; x < asize; x++ ) {
    array[x] = rand() % 100;
    printf(" %d", array[x]);
  }
  
  putchar('\n');
  
  return(0);
}
