#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int n;
  int d;
} Pair ;

Pair simplifier(int n,int d) {
  int larger, smaller, diff;

  larger = n>d ? n : d;
  smaller = n<d ? n : d;
  diff = larger-smaller;

  while( diff!=larger) {
    larger = smaller>diff ? smaller : diff;
    smaller = smaller==larger ? diff : smaller;
    diff = larger-smaller;
  }

  Pair ret_vals;
  ret_vals.n = diff > 1 ? n/diff : n;
  ret_vals.d = diff > 1 ? d/diff : d;
  
  return(ret_vals);
}

void usage(char* progname) {
  printf("Usage:\n\t%s <numerator> <denominator>\n",progname);
  return;
}

int main(int argc, char** argv) {
  if (argc < 3) {
    usage(argv[0]);
    return(1);
  }
  
  int n, d;
  n = atoi(argv[1]);
  d = atoi(argv[2]);

  printf("%d/%d = ", n, d);

  Pair nd = simplifier(n,d);

  printf("%d/%d\n",nd.n,nd.d);

  return(0);
}
