#include <stdio.h>

#define BUFFSIZE 32

int yes_no (char* prompt) {
  char buffer[(int)BUFFSIZE] = {};

  printf("%s (y/n): ",prompt);
  int scan_status = EOF;
  do {
  scan_status = scanf("%[yYnN]", buffer);
  printf("scan status: %d, read: |%s|\n",scan_status,buffer);
  if (scan_status != 1) {
    puts("(y/n)?");
    fgets(buffer,BUFFSIZE-1,stdin);
    buffer[0] = '\0';
  }
  } while (scan_status != 1);

  switch(buffer[0]) {
  case 'y':
  case 'Y':
    return 1;
  case 'n':
  case 'N':
    return 0;
  default:
    puts("unhandled case");
  }
  return -1;
}

int main() {

  int answer = yes_no("Do you agree?");
  
  printf("'%s' has been selected.\n",answer == 1 ? "yes" : "no");
  
  return(0);
}
