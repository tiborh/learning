#include <stdio.h>

int main() {
  char buffer[BUFSIZ]; /* BUFSIZ is a constant defined in stdio.h */
  
  setbuf(stdout, buffer);
  
  puts("first line before");
  puts("second line before");
  puts("third line before");
  fflush(stdout);
  char a_char = getchar();
  printf("Char read: '0x%x'\n",a_char);
  puts("first line after getchar()");

  return(0);
}
