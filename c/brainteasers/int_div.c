#include <stdio.h>

int main() {

  int a, b;
  a = 5; b = 4;
  printf("%d/%d = %f\n", a, b, a/b);
  printf("%d/%d = %f\n", a, b, a/(float)b);
  printf("%d/%d = %f\n", b, a, (float)b/a);
  
  return(0);
}
