#include <stdio.h>

int main(int argc, char** argv) {

  struct c_struct {
    char nonstring[17];
    char data[4];
  } c1 = {
    .nonstring = {
      'G', 'r', 'e', 'e', 't',
      'i', 'n', 'g', 's', ',',
      ' ', 'h', 'u', 'm', 'a',
      'n', '!'
    },
    .data = { 79, 85, 84, 33 }
  };
 
  printf("|%s|\n", c1.nonstring);

  return(0);
}
