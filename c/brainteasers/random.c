#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char** argv) {
  printf("%d\n",rand());	/* always the same */
  srand((unsigned)time(NULL));
  printf("%d\n",rand());	/* unique */

  return(0);
}
