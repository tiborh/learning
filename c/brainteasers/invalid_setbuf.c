#include <stdio.h>

int main(void) {
  char buf[BUFSIZ];

  setbuf(stdout, buf);
  printf("Hello, world!\n");

  return 0;
}
