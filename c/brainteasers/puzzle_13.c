#include <stdio.h>

int main() {
  int a, b;

  printf("Enter a positive integer: ");
  scanf("%d", &a);
  printf("(read as: %d)\n",a);
  b = ~a + 1;
  printf("Result: %d\n", b);

  return(0);
}
