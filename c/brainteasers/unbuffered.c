#include <stdio.h>

int main() {
  puts("first line before");
  puts("second line before");
  puts("third line before");
  char a_char = getchar();
  printf("Char read: '0x%x'\n",a_char);
  puts("first line after getchar()");

  return(0);
}
