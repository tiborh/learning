#include <stdio.h>
#include <stdlib.h>

#define DEFN -0.0
#define DEFP +0.0

void usage(char* pname) {
  printf("Usage: %s <n (default: %f)> <p (default: %f)>\n",
	 pname,
	 (float)(DEFN),
	 (float)(DEFP));
}


int main(int argc, char** argv) {
  float n = ((argc > 1) ? atof(argv[1]) : (float)(DEFN));
  float p = ((argc > 2) ? atof(argv[2]) : (float)(DEFP));

  if( n==p )
    printf("%f == %f\n",n,p);
  else
    printf("%f != %f\n",n,p);

  return(0);
}
