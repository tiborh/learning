#include <stdio.h>

int main() {
  char buffer[BUFSIZ]; /* BUFSIZ is a constant defined in stdio.h */
  
  setvbuf(stdout, buffer, _IONBF, BUFSIZ); /* _ options:
					      _IOFBF full buffering
					      _IOLBF line buffering
					      _IONBF no buffering size - size of the buffer
					    */
  
  puts("first line before");
  puts("second line before");
  puts("third line before");
  char a_char = getchar();
  printf("Char read: '0x%x'\n",a_char);
  puts("first line after getchar()");

  return(0);
}
