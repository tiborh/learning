#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GT_VAL 50
#define UP_LIM 100

void usage(char* pname) {
  printf("Usage: %s <val (default: %d)> <upper limit (default: %d)>\n",
	 pname,
	 (int)(GT_VAL),
	 (int)(UP_LIM));
}

int main(int argc, char** argv) {
  srand((unsigned)time(NULL));
  int gt_val = ((argc > 1) ? atoi(argv[1]) : (int)(GT_VAL));
  int up_lim = ((argc > 2) ? atoi(argv[2]) : (int)(UP_LIM));

  printf("gt_val: %d, up_lim: %d\n",gt_val,up_lim);
  int val = up_lim;
  do {
    val = rand() % up_lim;
    printf("val: %d\n", val);
  } while(val < gt_val);

  return(0);
}
