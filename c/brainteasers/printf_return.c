#include <stdio.h>
#include <math.h>

int printit(char* fmt, double flt) {
  printf("|");
  int n = printf(fmt,flt);
  printf("|\n");
  printf("format: %s\n",fmt);
  printf("(That's %d characters)\n", n);
  putchar('\n');
  return(n);
}

int main() {
    int n;
    printit("%2.4f", M_PI);
    printit("%.4f", M_PI);
    printit("%4f", M_PI);
    printit("%4.f", M_PI);
    printit("%04.f", M_PI);
    printit("%08.04f", 5.1);

    return(0);
}
