#include <stdio.h>

#define LEN 16

int main() {
  char filename[] = "update_the_file_as_much_as_you_would_like_to.txt";
  char name[LEN+1];
  int x;
  
  /* initialize the name[] buffer with dots */
  for( x=0; x<16; x++ )
    name[x] = '\0';
  
  /* extract first part of the filename */
  x = 0;
  while( filename[x] != '.' && x < LEN) {
    name[x] = filename[x];
    x++;
  }
  
  /* output result */
  printf("Extracted name '%s' from '%s'\n",
	 name,
	 filename
	 );
  
  return(0);
}
