#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define NUMOFNUMS 10
#define MAXNUM 100

void usage(char* progname) {
  printf("Usage:\n\t %s [num of nums (default: %d)] [max num (default: %d)]\n",
	 progname,(int)NUMOFNUMS,(int)MAXNUM);
}

int gcd(int n,int d) {
  int larger, smaller, diff;

  larger = n>d ? n : d;
  smaller = n<d ? n : d;
  diff = larger-smaller;

  while( diff!=larger) {
    larger = smaller>diff ? smaller : diff;
    smaller = smaller==larger ? diff : smaller;
    diff = larger-smaller;
  }

  return(diff);
}

int main(int argc, char** argv) {
  if (argc > 1 && (strcmp(argv[1],"-h") == 0 || strcmp(argv[1],"--help") == 0))
    usage(argv[0]);
  int num = (argc < 2) ? (int)NUMOFNUMS : atoi(argv[1]);
  int maxnum = (argc < 3) ? (int)MAXNUM : atoi(argv[2]);
  srand((unsigned)time(NULL));
  
  for (int i = 0; i < num; ++i) {
    int n, d;
    n = (rand() % maxnum) + 1;
    do {
      d = (rand() % maxnum) + 1;
    } while (n == d);

    printf("(%d,%d) --> ", n, d);

    int gcd_num = gcd(n,d);

    printf("GCD == %d\n",gcd_num);
  }

  return(0);
}
