#include <stdio.h>

typedef struct {
  int n;
  int d;
} Pair ;

Pair simplifier(int n,int d) {
  int larger, smaller, diff;

  larger = n>d ? n : d;
  smaller = n<d ? n : d;
  diff = larger-smaller;

  while( diff!=larger) {
    larger = smaller>diff ? smaller : diff;
    smaller = smaller==larger ? diff : smaller;
    diff = larger-smaller;
  }

  Pair ret_vals;
  ret_vals.n = diff > 1 ? n/diff : n;
  ret_vals.d = diff > 1 ? d/diff : d;
  
  return(ret_vals);
}

int main() {
  int n, d;

  printf("Enter a fraction (nn/nn): ");
  scanf("%d/%d", &n, &d);
  printf("%d/%d = ", n, d);

  Pair nd = simplifier(n,d);

  printf("%d/%d\n",nd.n,nd.d);

  return(0);
}
