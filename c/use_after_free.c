#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

char* fill_buffer(char *s) {
  sprintf(s, "This is a string\n");
  free(s);
  return s;
}

int main (int argc, char** argv) {
    void *ptr;
  if ((ptr = malloc(1024)) == NULL) {
    puts("could not reserve enough memory");
    exit(EXIT_FAILURE);
  }
  printf("fill_buffer returned: %s\n", fill_buffer(ptr));

  return 0;
}
