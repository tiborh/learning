#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "dlist.h"

#define UPPER_BOUND 50
#define NUM_OF_NUMS (UPPER_BOUND * 4)

unsigned char* unload_stack() {
    int slen = get_length();
    unsigned char* outstr = calloc(slen+1,sizeof(char));
    for(int i = 0; i < slen; ++i)
        outstr[i] = pop_front();
    outstr[slen] = '\0';
    return outstr;
}
unsigned char* unload_queue() {
    int slen = get_length();
    unsigned char* outstr = calloc(slen+1,sizeof(char));
    for(int i = 0; i < slen; ++i)
        outstr[i] = pop_back();
    outstr[slen] = '\0';
    return outstr;
}
void load_queue(char* instr) {
    unsigned short int slen = strlen(instr);
    printf("slen: %u\n", slen);
    for (unsigned short int i = 0; i < slen; ++i)
        push_front(instr[i]);
}

void back_load(char* instr) {
    unsigned short int slen = strlen(instr);
    for (unsigned short int i = 0; i < slen; ++i)
        push_back(instr[i]);
}

int main(int argc, char** argv)
{
    srand(time(NULL));
    print_list();
    push_front('c');
    print_list();
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    assert(get_length() == check_length());
    printf("unloaded: %c\n",pop_front());
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    assert(get_length() == check_length());
    push_back('b');
    puts("back-loaded:");
    print_list();
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    printf("back unloaded: %c\n",pop_back());
    //printf("unloaded: %c\n",pop_front());
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    assert(get_length() == check_length());
    push_front('a');
    push_front('b');
    print_list();
    assert(get_length() == check_length());
    unsigned char* outstr = unload_stack();
    printf("unloaded: %s\n",outstr);
    free(outstr);
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    assert(get_length() == check_length());
    load_queue("asahi");
    print_list();
    puts("View test:");
    INDEX len = get_length();
    for(INDEX i = 0; i < len; ++i)
        printf("%c ",view_nth(i));
    putchar('\n');
    puts("View from back:");
    for(INDEX i = 0; i < len; ++i)
        printf("%c ",view_nth_from_back(i));
    putchar('\n');
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    assert(get_length() == check_length());
    outstr = unload_stack();
    printf("unload: %s\n",outstr);
    free(outstr);
    assert(get_length() == check_length());
    back_load("isanagi");
    print_list();
    assert(get_length() == check_length());
    remove_item(0);
    puts("first char removed:");
    print_list();
    assert(get_length() == check_length());
    remove_item(get_length()-1);
    puts("last char removed:");
    print_list();
    assert(get_length() == check_length());
    remove_item(2);
    puts("third item removed:");
    print_list();
    remove_item_from_back(2);
    puts("third from back removed:");
    print_list();
    printf("check_length_from_back: %u\n",check_length_from_back());
    assert(get_length() == check_length());
    puts("insert tests:");
    puts("after 2nd from front:");
    insert_after_from_front(2,'i');
    print_list();
    printf("check_length_from_back: %u\n",check_length_from_back());
    assert(get_length() == check_length_from_back());
    puts("before 0th from front:");
    insert_before_from_front(0,'i');
    print_list();
    printf("check_length_from_back: %u\n",check_length_from_back());
    assert(get_length() == check_length_from_back());
    puts("after 2nd from front:");
    insert_after_from_front(2,'n');
    print_list();
    printf("check_length_from_back: %u\n",check_length_from_back());
    assert(get_length() == check_length_from_back());
    puts("before 4th from front:");
    insert_before_from_front(4,'a');
    print_list();
    printf("check_length_from_back: %u\n",check_length_from_back());
    assert(get_length() == check_length_from_back());
    outstr = unload_queue();
    printf("back unload: %s\n",outstr);
    free(outstr);
    puts("insert from back tests:");
    push_back('z');
    print_list();
    puts("insert_before_from_back 0 s");
    insert_before_from_back(0,'s');
    print_list();
    puts("insert_after_from_back 0 s");
    insert_after_from_back(0,'a');
    print_list();
    puts("insert_before_from_back 1 u");
    insert_before_from_back(1,'u');
    puts("insert_after_from_back 1 k");
    insert_after_from_back(1,'k');
    print_list();
    puts("insert_before_from_back 1 u");
    insert_before_from_back(1,'u');
    print_list();
    empty_list();
    puts("push array test:");
    puts("preparation");
    push_front('a');
    push_back('b');
    print_list();
    puts("front:");
    push_front_array(2,(DATA*)"ha");
    print_list();
    puts("back:");
    push_back_array(3,(DATA*)"ite");
    print_list();
    assert(get_length() == check_length());
    assert(get_length() == check_length_from_back());
    empty_list();
    //printf("get_length == %u\n",get_length());
    //printf("check_Length == %u\n", check_length());
    //assert(get_length() == check_length());
    assert(is_empty());
    load_queue("aabbcc");
    print_list();
    remove_neighbouring_duplicates();
    print_list();
    assert(get_length() == check_length());
    empty_list();
    unsigned char upper_bound = argc > 1 ? atoi(argv[1]) : UPPER_BOUND;
    unsigned char num_of_nums = argc > 2 ? atoi(argv[2]) : NUM_OF_NUMS;
    printf("upper_bound: %u\n",upper_bound);
    printf("num_of_nums: %u\n",num_of_nums);
    assert(is_empty());
    for(unsigned char i = 0; i < num_of_nums; ++i) {
        unsigned char c = rand() % upper_bound;
        //printf("c == %u\n",c);
        push_front(c);
    }
    puts("Unsorted list:");
    print_numeric_list();
    sort_list();
    puts("Sorted list:");
    print_numeric_list();
    puts("After removal of duplicates:");
    remove_neighbouring_duplicates();
    print_numeric_list();
    empty_list();
    return 0;
}
