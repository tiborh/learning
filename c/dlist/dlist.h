#ifndef DLIST_H_INCLUDED
#define DLIST_H_INCLUDED
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#define SEP "\t"
#define BREAK_AFTER 5
#define LIST_PRINT_STRING "%c "

typedef unsigned int INDEX;
typedef unsigned char DATA;
typedef struct list_item {DATA data; struct list_item* next; struct list_item* prev; } li;
void push_front(DATA);
void push_front_array(INDEX,DATA*);
void push_back(DATA);
void push_back_array(INDEX,DATA*);
DATA pop_front(void);
DATA pop_back(void);
DATA view_front(void);
DATA view_back(void);
DATA view_nth(INDEX);
DATA view_nth_from_back(INDEX);
void set_nth_from_front(INDEX, DATA);
INDEX get_length(void);
INDEX check_length(void);
INDEX check_length_from_back(void);
void print_list(void);
void print_numeric_list(void);
void insert_after_from_front(INDEX, DATA);
void insert_before_from_front(INDEX, DATA);
void insert_after_from_back(INDEX, DATA);
void insert_before_from_back(INDEX, DATA);
DATA remove_item(INDEX);
DATA remove_item_from_back(INDEX);
void remove_neighbouring_duplicates(void);
void sort_list(void);
void empty_list(void);
bool is_empty(void);

#endif // DLIST_H_INCLUDED
