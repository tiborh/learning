#include "dlist.h"

static li* head = NULL;
static li* tail = NULL;
static INDEX list_length = 0;

void push_front(DATA c) {
    li* an_item = calloc(1,sizeof(li));
    an_item->data = c;
    if (head)
        head->prev = an_item;
    an_item->next = head;
    an_item->prev = NULL;
    if (!tail)
        tail = an_item;
    head = an_item;
    ++list_length;
}
void push_front_array(INDEX size,DATA* array) {
    for(INDEX i = 0; i < size; ++i) {
        push_front(*(array + i));
    }
}
void push_back(DATA c) {
    li* an_item = calloc(1,sizeof(li));
    an_item->data = c;
    an_item->prev = tail;
    if (tail)
        tail->next = an_item;
    an_item->next = NULL;
    if (!head)
        head = an_item;
    tail = an_item;
    ++list_length;
}
void push_back_array(INDEX size,DATA* array) {
    for(INDEX i = 0; i < size; ++i) {
        push_back(*(array + i));
    }
}
DATA view_front(void) {
    if(!head)
        return((DATA) 0);
    return head->data;
}
DATA view_back(void) {
    if(!tail)
        return((DATA) 0);
    return tail->data;
}
DATA view_nth_from_front(INDEX n) {
    if (n == 0)
        return view_front();
    INDEX i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; i < n && tmp_ptr; ++i,tmp_ptr = tmp_ptr->next);
    return tmp_ptr->data;
}
DATA view_nth_from_back(INDEX n) {
    assert(n < list_length);
    if (n == 0)
        return view_back();
    INDEX i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = tail; i < n && tmp_ptr; ++i,tmp_ptr = tmp_ptr->prev);
    return tmp_ptr->data;
}
DATA view_nth(INDEX n) {
    assert(n < list_length);
    return(view_nth_from_front(n));
}
void set_nth_from_front(INDEX n, DATA new_val) {
    assert(n < list_length);
    INDEX i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; i < n && tmp_ptr; ++i,tmp_ptr = tmp_ptr->next);
    tmp_ptr->data = new_val;
}
INDEX get_length(void) {
    return list_length;
}
INDEX check_length(void) {
    INDEX i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = head; tmp_ptr; ++i,tmp_ptr = tmp_ptr->next);
    return i;
}
INDEX check_length_from_back(void) {
    INDEX i;
    li* tmp_ptr;
    for(i = 0, tmp_ptr = tail; tmp_ptr; ++i,tmp_ptr = tmp_ptr->prev);
    return i;
}
DATA pop_front(void) {
    if(head == NULL)
        return 0;
    DATA ret_data = head->data;
    li* saved_head = head;
    if (head == tail)
        tail = NULL;
    head = head->next;
    if (head)
        head->prev = NULL;
    free(saved_head);
    --list_length;
    return ret_data;
}
DATA pop_back(void) {
    if(tail == NULL)
        return 0;
    DATA ret_data = tail->data;
    li* saved_tail = tail;
    if (head == tail)
        head = NULL;
    tail = tail->prev;
    if (tail)
        tail->next = NULL;
    free(saved_tail);
    --list_length;
    return ret_data;
}
void insert_after_from_front(INDEX place, DATA data) {
    assert(place < list_length);
    if (place == list_length - 1) {
        push_back(data);
        return;
    }
    li* tmp_ptr = head;
    int list_counter = 0;
    while(list_counter < place && tmp_ptr) {
        tmp_ptr = tmp_ptr->next;
        ++list_counter;
    }
    if (!tmp_ptr)
        return;
    else {
        li* new_li = calloc(1,sizeof(li));
        new_li->data = data;
        new_li->next = tmp_ptr->next;
        tmp_ptr->next->prev = new_li;
        tmp_ptr->next = new_li;
        new_li->prev = tmp_ptr;
        ++list_length;
    }
    return;
}
void insert_before_from_front(INDEX place, DATA data) {
    assert(place < list_length);
    if (!place)
        push_front(data);
    else
        insert_after_from_front(place-1,data);
    return;
}
void insert_before_from_back(INDEX place, DATA data) {
    assert(place < list_length);
    if (place == list_length - 1) {
        push_front(data);
        return;
    }
    li* tmp_ptr = tail;
    int list_counter = 0;
    while(list_counter < place && tmp_ptr) {
        tmp_ptr = tmp_ptr->prev;
        ++list_counter;
    }
    if (!tmp_ptr)
        return;
    else {
        li* new_li = calloc(1,sizeof(li));
        new_li->data = data;
        new_li->prev = tmp_ptr->prev;
        tmp_ptr->prev->next = new_li;
        tmp_ptr->prev = new_li;
        new_li->next = tmp_ptr;
        ++list_length;
    }
    return;
}
void insert_after_from_back(INDEX place, DATA data) {
    assert(place < list_length);
    if (!place)
        push_back(data);
    else
        insert_before_from_back(place-1,data);
    return;
}
DATA remove_item(INDEX index) {
    assert(index < list_length);
    if (index == 0)
        return pop_front();
    if (index == list_length - 1)
        return pop_back();
    li* tmp_ptr = head;
    int list_counter = 0;
    DATA ret_data = 0;
    while(list_counter < index && tmp_ptr) {
        tmp_ptr = tmp_ptr->next;
        ++list_counter;
    }
    if (!tmp_ptr)
        return 0;
    else {
        ret_data = tmp_ptr->data;
        tmp_ptr->prev->next = tmp_ptr->next;
        if (tmp_ptr->next)
            tmp_ptr->next->prev = tmp_ptr->prev;
        free(tmp_ptr);
        --list_length;
    }
    return ret_data;
}
DATA remove_item_from_back(INDEX index) {
    assert(index < list_length);
    if (index == 0)
        return pop_back();
    if (index == list_length - 1)
        return pop_front();
    li* tmp_ptr = tail;
    int list_counter = list_length-1;
    DATA ret_data = 0;
    while(list_counter > index && tmp_ptr) {
        tmp_ptr = tmp_ptr->prev;
        --list_counter;
    }
    if (!tmp_ptr)
        return 0;
    else {
        ret_data = tmp_ptr->data;
        if (tmp_ptr->prev)
            tmp_ptr->prev->next = tmp_ptr->next;
        tmp_ptr->next->prev = tmp_ptr->prev;
        free(tmp_ptr);
        --list_length;
    }
    return ret_data;
}
void print_list(void) {
    if (!list_length) {
        puts("NULL");
        return;
    }
    li* tmp_ptr = head;
    printf("NULL < ");
    while(tmp_ptr) {
        printf(LIST_PRINT_STRING,tmp_ptr->data);
        tmp_ptr = tmp_ptr->next;
        if (tmp_ptr)
            printf("<-> ");
    }
    puts("> NULL");
}
void print_numeric_list(void) {
    li* tmp_ptr = head;
    for(INDEX i = 1; i <= list_length && tmp_ptr; ++i) {
        printf("%u%s",tmp_ptr->data,SEP);
        tmp_ptr = tmp_ptr->next;
        if (!(i % BREAK_AFTER))
            putchar('\n');
    }
    putchar('\n');
}
static void swap(li* li0, li* li1) {
    DATA tmp = li0->data;
    li0->data = li1->data;
    li1->data = tmp;
}
static int li_cmp(li* li0, li* li1) {
    return(li1->data - li0->data);
}
void sort_list(void) {
    bool change_watch = true;
    for(INDEX upper_bound = list_length; upper_bound > 1 && change_watch; --upper_bound) {
        li* tmp_ptr = head->next;
        li* prev_ptr = head;
        change_watch = false;
        for(INDEX i = 0; i < upper_bound; ++i) {
            if (li_cmp(prev_ptr,tmp_ptr) < 0) {
                swap(prev_ptr,tmp_ptr);
                change_watch = true;
            }
            prev_ptr = tmp_ptr;
            if (tmp_ptr->next)
                tmp_ptr = tmp_ptr->next;
        }
    }
}
void remove_neighbouring_duplicates(void) {
    if (list_length < 2)
        return;
    li* tmp_ptr = head->next;
    while(tmp_ptr) {
        if (tmp_ptr->prev->data == tmp_ptr->data) {
            tmp_ptr->prev->next = tmp_ptr->next;
            if (tmp_ptr->next)
                tmp_ptr->next->prev = tmp_ptr->prev;
            li* saved_ptr = tmp_ptr->next;
            free(tmp_ptr);
            --list_length;
            tmp_ptr = saved_ptr;
        } else
            tmp_ptr = tmp_ptr->next;
    }
}
void empty_list(void) {
    while(head)
        pop_front();
}
bool is_empty(void) {
    return(head == NULL);
}
