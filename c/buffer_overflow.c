#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

#define FILL_SIZE 32

int main (int argc, char** argv) {
  char* ptr;
  if ((ptr = malloc(FILL_SIZE)) == NULL) {
    puts("could not reserve enough memory");
    exit(EXIT_FAILURE);
  }
  int i;
  for(i = 0; i < FILL_SIZE; ++i)
    *(ptr + i) = 'a';
  *(ptr + i) = 0;
  printf("ptr prints as: %s\n", ptr);
  free(ptr);

  return 0;
}
