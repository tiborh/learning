#include <stdio.h>
#include <ctype.h>
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
#include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs,calloc */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

#define MAX_WORD_LENGTH 32
#define BAR_CHAR '#'

void print_stats(int max_length,int* stats) {
  int sum = 0;
  for(int i = 1; i <= max_length; ++i)
    if (*(stats+i)) {
      sum += *(stats + i);
      printf("%d:\t%d\n",i,*(stats + i));
    }
  printf("sum:\t%d\n",sum);
}

void reset_array(int fill_length, char* arr) {
  for(int i = 0; i < fill_length; ++i)
    *(arr + i) = '\0';
}

int main (int argc, char** argv) {
  int c = 0;			/* if char, needs to look out for 0xff to avoid infinite loop */
  int words = 0, word_index = 0, max_word_length = 0;
  bool in_word = false;
  char word_collector[MAX_WORD_LENGTH+1] = {};
  int word_stat[MAX_WORD_LENGTH+1] = {};
  while((c = getchar()) != EOF) {
    if(isalpha(c) || c == '\'') {
      if(!in_word) {
	++words;
	in_word = true;
      }
      if (word_index <= MAX_WORD_LENGTH)
	*(word_collector + word_index++) = c;
      else {
	*(word_collector + MAX_WORD_LENGTH + 1) = '\0';
	printf(">>>WORD OVERFLOW<<< in %s at %c\n",word_collector,c);
      }
    } else {
      if (in_word)
	in_word = false;
      *(word_collector + word_index) = '\0';
    }
    if(!in_word) {
      int word_length = strlen(word_collector);
      if(word_length > 0) {
	if (word_length > max_word_length)
	  max_word_length = word_length;
	(*(word_stat + word_length))++;
	if(word_length > 19)
	  puts(word_collector);
	reset_array(word_length,word_collector);
	word_index = 0;
      }
    }
  }
  printf("words: %d\n",words);
  print_stats(max_word_length,word_stat);

  return 0;
}
