#include <stdio.h>
#include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
#include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
#include <limits.h>

#define NUM_OF_NUMS 10
#define UPPER_BOUND UCHAR_MAX
#define NUM_OF_TESTS 100

void help(char* argv0) {
    puts("Usage:");
    printf("%s [number of numbers] [upper bound] [number of tests]\n",argv0);
}

void print_array(unsigned size, unsigned* a) {
  for(unsigned i = 0; i < size; ++i)
    printf("%u ",*(a + i));
  putchar('\n');
}

void fill_array(unsigned alength, unsigned* a, unsigned rand_ceiling) {
  for(unsigned i = 0; i < alength; ++i)
    *(a + i) = rand() % rand_ceiling;
}

static int cmp_ints(const void *p1, const void *p2) {
  return(*((unsigned*)p1)-*((unsigned*)p2));
}

unsigned qsort_test(unsigned num_of_nums,unsigned upper_bound) {
  unsigned* arr = calloc(num_of_nums,sizeof(unsigned));
  clock_t begin = 0, end = 0;
  
  fill_array(num_of_nums,arr,upper_bound);
  puts("unsorted:");
  print_array(num_of_nums,arr);

  begin = clock();
  qsort(arr,num_of_nums,sizeof(unsigned),cmp_ints);
  end = clock();

  unsigned diff = end - begin;
  puts("sorted:");
  print_array(num_of_nums,arr);
  free(arr);
  
  return diff;
}

double get_average(unsigned len, unsigned *nums) {
    double avg = 0.0;
    for (unsigned i = 0; i < len; ++i)
        avg += ((double)*(nums + i) - avg) / (double)(i+1);

    return avg;
}

double timing_tests(unsigned (*sorting_test)(unsigned,unsigned),unsigned num_of_nums, unsigned upper_bound, unsigned num_of_tests) {
    unsigned* nums = calloc(num_of_tests,sizeof(unsigned));
    for(unsigned i = 0; i < num_of_tests; ++i) {
        *(nums + i) = sorting_test(num_of_nums,upper_bound);
    }
    double average = get_average(num_of_tests,nums);
    puts("timings:");
    print_array(num_of_tests,nums);
    free(nums);
    return(average);
}

int main (int argc, char** argv) {
  if (argc == 2 && !strcmp("help",argv[1])) {
    help(basename(argv[0]));
    exit(EXIT_SUCCESS);
  }
  unsigned num_of_nums = argc > 1 ? atoi(argv[1]) : NUM_OF_NUMS;
  unsigned upper_bound = argc > 2 ? atoi(argv[2]) : UPPER_BOUND;
  unsigned num_of_tests = argc > 3 ? atoi(argv[3]) : NUM_OF_TESTS;
  srand(time(NULL));
  //unsigned sorting_time = qsort_test(num_of_nums,upper_bound);
  //printf("Sorting time: %u\n",sorting_time);  

  double average = timing_tests(qsort_test,num_of_nums,upper_bound,num_of_tests);
  printf("Average sorting time: %f\n",average);
  
  return 0;
}
