#include <stdio.h>
#include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
// #include <libgen.h>		/* for basename and dirname */
// #include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions

void print_stats(int max_length,int* stats) {
  int sum = 0;
  for(int i = 0; i < max_length; ++i)
    if (*(stats+i)) {
      sum += *(stats + i);
      printf("%c:\t%d\n",'a'+i,*(stats + i));
    }
  printf("sum:\t%d\n",sum);
}

int main (int argc, char** argv) {

  int c = 0;			/* if char, needs to look out for 0xff to avoid infinite loop */
  const int Alphabet_Size = 'z' - 'a' + 1;
  int* letter_numbers = calloc(Alphabet_Size,sizeof(int));

  while((c = getchar()) != EOF) {
    int c_low = tolower(c);
    if (c_low >= 'a' && c_low <= 'z')
      ++(*(letter_numbers + (c_low - 'a')));
    /* else */
    /*   printf("unhandled: %c (0x%x)\n",c,c); */
  }

  print_stats(Alphabet_Size,letter_numbers);

  free(letter_numbers);
  return 0;
}
