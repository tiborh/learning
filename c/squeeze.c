#include <stdio.h>
// #include <ctype.h>		/* for character functions */
// #include <assert.h>		/* for simple checks */
// #include <math.h>		/* do not forget -lm at compilation time */
// #include <tgmath.h>		/* for fabs */
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
// #include <stdbool.h>		/* for bool */
#include <stdlib.h>		/* for exit,abs */
// #include <time.h>      	/* for time(NULL) */
// #include <unistd.h>		/* for sleep */
// #include <stdarg.h>		/* for variable argument functions
// #include <limits.h>		/* for integer type max/min/size */

char* squeeze_nulls(int len, int nunulls, char* instr) {
  int lenout = len - nunulls + 1;
  char* outstr = calloc(lenout,sizeof(char));
  int i = 0, j = 0;
  while(i < lenout && j < len) {
    if(*(instr+j) != '\0')
      *(outstr + i++) = *(instr + j++);
    else
      ++j;
  }
  *(outstr+i) = '\0';

  return(outstr);
}

char* squeeze(char* str1, char* str2) {
  int len1 = strlen(str1);
  int len2 = strlen(str2);
  int numnulls = 0;
  for(int i = 0; i < len2; ++i) {
    for(int j = 0; j < len1; ++j) {
      if (*(str1+j) == *(str2+i)) {
	*(str1+j) = '\0';
	++numnulls;
      }
    }
  }
  return(squeeze_nulls(len1,numnulls,str1));
}

int main (int argc, char** argv) {

  if (argc <  3) {
    printf("%s <string1> <string2>\n\tstring2 characters are deleted from string1\n",basename(argv[0]));
    exit(0);
  }
  char* squeezed = squeeze(argv[1],argv[2]);
  printf("%s\n",squeezed);
  printf("%lu\n",strlen(squeezed));

  return 0;
}
