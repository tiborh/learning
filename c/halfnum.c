#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int num_halfer(int num) {
  return((int)((double)num / 2));
}

int main(int argc, char** argv) {
  srand((unsigned)time(NULL));
  int num = (rand() % 100) + 1;

  printf("num: %d, double halfnum: %lf\n",num,(double)num/2);
  printf("num: %d,    int halfnum: %d\n",num,num_halfer(num));
  
  return 0;
}
