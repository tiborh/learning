#include <stdio.h>
#include <libgen.h>		/* for basename and dirname */
#include <string.h>		/* str operations */
#include <stdlib.h>		/* for exit,abs */

void help(char* argv0) {
  puts("Usage:");
  printf("\t%s <string of binary digits>\n",basename(argv0));
  exit(EXIT_FAILURE);
}

int bin2dec(char* binstr) {
  int decnum = 0;
  int len = strlen(binstr);
  int bin_multiplier = 1;

  for (int i = len-1; i >= 0; --i) {
    decnum += (binstr[i]-'0') * bin_multiplier;
    bin_multiplier *= 2;
  }
    
  return decnum;
}

int main (int argc, char** argv) {
  if (argc < 2) {
    help(argv[0]);
  }
  int prefix = argv[1][0] == '-' ? -1 : 1;
  char* binstr = prefix == 1 ? argv[1] : argv[1] + 1;
  int decnum = prefix * bin2dec(binstr);
  printf("%d\n",decnum);
  
  return 0;
}
