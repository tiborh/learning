#include <stdio.h>
#include <math.h> /* has  sin() */
#include <tgmath.h> // has fabs() for floating point numbers (Raspbian OS)
#include <stdlib.h> // has abs(), which is for integers only (Raspbian OS)
int main(void)
{ 
    double interval;
    int i;
    for(i = 0; i <40; i++) // it turns into negative above pi, so a higher range is needed
    {
        interval = i/10.0;
        //printf("sin( %lf ) = %lf\n", interval, sin(interval)); // turns negative at 3.2
        printf("sin( %lf ) = %lf\n", interval, fabs(sin(interval))); // tabbed result was a mess on my terminal
    }

    printf("\n+++++++\n");
    return 0;
}
