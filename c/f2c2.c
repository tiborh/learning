#include <stdio.h>
#include <math.h>
#include <libgen.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

const double temp_coeff = 9 / 5.0;
const int temp_const = 32;

double f2c(double fahr) {
  return((fahr - temp_const) / temp_coeff);
}

double c2f(double cels) {
  return(cels * temp_coeff + temp_const);
}

double proc_input(char* called, char* temp_string) {
  char* fahr2cels = "f2c2";
  char* cels2fahr = "c2f2";
  char* fn = basename(called);
  int is_f2c = strcmp(fahr2cels,fn);
  int is_c2f = strcmp(cels2fahr,fn);
  double temp_to_convert;
  sscanf(temp_string,"%lf",&temp_to_convert);
  
  if (0 == is_f2c)
    return(f2c(temp_to_convert));
  else if (0 == is_c2f)
    return(c2f(temp_to_convert));
  else {
    printf("unknown basename: %s\n",called);
    exit(2);
  }
}

int main (int argc, char** argv) {
  char* help_txt = "an argument is needed: a temperature value\n";
  if (argc < 2) {
    puts(help_txt);
    exit(1);
  }

  printf("%lf\n",proc_input(argv[0],argv[1]));
  
  return 0;
}
