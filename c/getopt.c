#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void usage(char* prog_name) {
      fprintf(stderr, "Usage:\n\t%s [-t nsecs] [-n] name\n",
	      prog_name);
      exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
  int flags, opt;
  int nsecs, tfnd;

  nsecs = 0;
  tfnd = 0;
  flags = 0;
  while ((opt = getopt(argc, argv, "nt:h?")) != -1) {
    switch (opt) {
    case 'n':
      flags = 1;
      break;
    case 't':
      nsecs = atoi(optarg);
      tfnd = 1;
      break;
    case 'h':
    default: /* '?' */
      usage(argv[0]);
    }
  }

  printf("flags=%d; tfnd=%d; nsecs=%d; optind=%d\n",
	 flags, tfnd, nsecs, optind);

  if (optind >= argc) {
    fprintf(stderr, "Expected argument after options\n");
    exit(EXIT_FAILURE);
  }

  printf("name argument = %s\n", argv[optind]);

  /* Other code omitted */

  exit(EXIT_SUCCESS);
}
