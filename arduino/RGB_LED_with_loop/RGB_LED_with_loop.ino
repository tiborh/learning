//RGB LED
//The RGB LED will appear red, green, and blue first, then red, orange, yellow, green, blue, indigo, and purple.
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
/*************************************************************************/
const int redPin = 11;  // R petal on RGB LED module connected to digital pin 11 
const int greenPin = 10;  // G petal on RGB LED module connected to digital pin 10 
const int bluePin = 9;  // B petal on RGB LED module connected to digital pin 9 
const int sDT = 50; // short delay time
const int lDT = 1000; // long delay time
/**************************************************************************/
void setup()
{ 
  pinMode(redPin, OUTPUT); // sets the redPin to be an output 
  pinMode(greenPin, OUTPUT); // sets the greenPin to be an output 
  pinMode(bluePin, OUTPUT); // sets the bluePin to be an output 
}    
/***************************************************************************/
void loop()  // run over and over again  
{ 
  /*
  colour(255,255,255);
  delay(lDT);
  colour(255,0,0);
  delay(lDT);
  colour(0,255,0);
  delay(lDT);
  colour(0,0,255);
  delay(lDT);
  */

  for (int i = 0; i <= 255; ++i) {
    colour(i,i,i);
    delay(sDT);
  }
  
  for (int r = 255; r >= 0; --r) {
     colour(r,255,255);
     delay(sDT);
  }
  for (int g = 255; g >= 0; --g) {
     colour(0,g,255);
     delay(sDT);
  }
  for (int r = 0,b = 255; b >= 0; ++r,--b) {
     colour(r,0,b);
     delay(sDT);
  }
  for (int r = 255,g = 0; r >= 0; --r,++g) {
     colour(r,g,0);
     delay(sDT);
  }
  for (int r = 0; r <= 255; ++r) {
     colour(r,255,0);
     delay(sDT);
  }
  for (int b = 0; b <= 255; ++b) {
     colour(255,255,b);
     delay(sDT);
  }
  for (int g = 255; g >= 0; --g) {
     colour(255,g,255);
     delay(sDT);
  }


  three_blinks();


/*  for (int r = 255; r >= 0; r--) {
    for (int g = 0; g <= 255; g++) {
       for (int b = 0; b <= 255; b++) {
         colour(r, g, b);
         //delay(1);
       }  
     } 
  }
  */
}     
/******************************************************/
void colour (unsigned char red, unsigned char green, unsigned char blue)     // the colour generating function  
{    
  analogWrite(redPin, red);   
  analogWrite(greenPin, green); 
  analogWrite(bluePin, blue); 
}
void three_blinks (void) {
  for (int j = 0; j < 3; ++j) {
    colour(0,0,0);
    delay(lDT/4);
    colour(64,64,64);
    delay(lDT/4);
  }   
}
/******************************************************/


