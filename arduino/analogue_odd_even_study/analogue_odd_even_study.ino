/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

size_t samp_size = 0-1;
//size_t samp_size = 100;
int delms = 10;
struct Result {
  size_t even;
  size_t odd;
};
Result test_res(size_t ssize, int delms) {
  Result res = {};
  size_t an0;
  for(size_t i = 0; i < ssize; ++i) {
     an0 = analogRead(A0);
     (an0 % 2 == 0) ? res.even++ : res.odd++;
     delay(delms);
  }
  return res;
}
void print_res(Result res) {
  Serial.print("odd: ");
  Serial.print(res.odd);
  Serial.print(", even: ");
  Serial.println(res.even);
}
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("starting...(sample size: ");
  Serial.print(samp_size);
  Serial.println(")");
}
// the loop routine runs over and over again forever:
void loop() {
  print_res(test_res(samp_size,delms));
  delay(10);        // delay in between reads for stability
}
