//RGB LED
//The RGB LED will appear red, green, and blue first, then red, orange, yellow, green, blue, indigo, and purple.
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
/*************************************************************************/
const int redPin = 4;  // R petal on RGB LED module connected to digital pin 11 
const int greenPin = 3;  // G petal on RGB LED module connected to digital pin 10 
const int bluePin = 2;  // B petal on RGB LED module connected to digital pin 9 
const int sDT = 50; // short delay time
const int lDT = 1000; // long delay time
/**************************************************************************/
void setup()
{ 
  pinMode(redPin, OUTPUT); // sets the redPin to be an output 
  pinMode(greenPin, OUTPUT); // sets the greenPin to be an output 
  pinMode(bluePin, OUTPUT); // sets the bluePin to be an output 
}    
/***************************************************************************/
void loop()  // run over and over again  
{ 
  three_blinks();

  int r = 0;
  int g = 0;
  int b = 0;
  colour(r,g,b);
  delay(lDT/2);
  int colours[3] = {0,0,0};

  for(;;) {
   change_colours(colours,random(256),random(256),random(256));
  }
}     
/******************************************************/
void colour (unsigned char red, unsigned char green, unsigned char blue)     // the colour generating function  
{    
  analogWrite(redPin, red);   
  analogWrite(greenPin, green); 
  analogWrite(bluePin, blue); 
}

void three_blinks (void) {
    colour(255,0,0);
    delay(lDT/2);
    colour(0,255,0);
    delay(lDT/2);
    colour(0,0,255);
    delay(lDT/2);
}

void change_colours(int* colours, int r, int g, int b) {

  int rd = r < colours[0] ? -1 : 1;
  int gd = g < colours[1] ? -1 : 1;
  int bd = b < colours[2] ? -1 : 1;
  
  while (r != colours[0] || g != colours[1] || b != colours[2]) {
    if (r != colours[0])
      colours[0] += rd;
    if (g != colours[1])
      colours[1] += gd;
    if (b != colours[2])
      colours[2] += bd;

    colour(colours[0],colours[1],colours[2]);
    delay(sDT);
  }
}
/******************************************************/
