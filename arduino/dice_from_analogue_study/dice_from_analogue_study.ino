/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

size_t samp_size = 0-1;
//size_t samp_size = 1000;
size_t num_of_outcomes = 6;
int delms = 10;

void zero_out(size_t size, size_t* arr, size_t zeroval) {
  for (size_t i = 0; i < size; ++i) {
    arr[i] = 0;
  }
}

size_t* make_tests(size_t num, size_t out, int delms) {
  size_t *results = new size_t[out];
  zero_out(out,results,0);

  for (size_t i = 0; i < num; ++i) {
    results[analogRead(A0) % out]++;
    delay(delms);
  }
    
  return results;
}

void print_results(size_t num, size_t* results) {
  for (size_t i = 0; i < num; ++i) {
    Serial.print(i);
    Serial.print(": ");
    Serial.print(results[i]);
    Serial.print("; ");
  }
  Serial.println("");
  delete[] results;
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("starting...(sample size: ");
  Serial.print(samp_size);
  Serial.println(")");
}

// the loop routine runs over and over again forever:
void loop() {
  print_results(num_of_outcomes,make_tests(samp_size,num_of_outcomes,delms));
  delay(delms);        // delay in between reads for stability
}
