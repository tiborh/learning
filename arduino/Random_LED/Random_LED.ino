//Flowing LED Lights
/* Eight LEDs will light up one by one from left to right, and then go out one by one from right to left.
After that, the LEDs will light up one by one from right to left, and then go out one by one from left to right.
This process will repeat indefinitely.*/
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
/**************************************/
const int lowestPin = 5;//the lowest one attach to
const int highestPin = 12;//the highest one attach to 
/**************************************/
void setup()
{
  //set pins 1 through 6 as output  
  for(int thisPin = lowestPin;thisPin <= highestPin;thisPin++)
  {
     pinMode(thisPin,OUTPUT); //initialize thisPin as an output
  }
} 
/****************************************/
void loop()
{
  bool thePins[highestPin];
  for (int i = 0; i < highestPin; i++) {
    thePins[i] = false;
  }
  for (;;) {
    int num = random(lowestPin,highestPin+1);
    if (thePins[num]) {
       thePins[num] = false;
       digitalWrite(num, LOW);
       delay(100);
    } else {
       thePins[num] = true;
       digitalWrite(num, HIGH);
       delay(100);
    }
  }
}
