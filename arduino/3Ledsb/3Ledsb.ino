/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade
*/

int led2 = 2;           // the PWM pin the LED is attached to
int led3 = 3;
int led4 = 4;
int five = led2 + led3;
bool hi2 = false;
bool hi3 = false;
bool hi4 = false;
//int brightness = 0;    // how bright the LED is
//int fadeAmount = 5;    // how many points to fade the LED by

int lowhi(bool* hi) {
  if (*hi) {
    *hi = false;
    return LOW;
  } else {
    *hi = true;
    return HIGH;
  }
}

// the setup routine runs once when you press reset:
void setup() {
  // declare pins to be an output:
  pinMode(led2, OUTPUT);  
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  int an0 = analogRead(A0);
  // set the brightness of pin 9:
  if ( an0 % led2 == 0 )
    digitalWrite(led2, lowhi(&hi2));
  if ( an0 % led3 == 0 )
    digitalWrite(led3, lowhi(&hi3));
  if ( an0 % five == 0 )
    digitalWrite(led4, lowhi(&hi4));
  // change the brightness for next time through the loop:
//  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
//  if (brightness <= 0 || brightness >= 255) {
//    fadeAmount = -fadeAmount;
//  }
  // wait for 1000 milliseconds
  delay(250);
}
