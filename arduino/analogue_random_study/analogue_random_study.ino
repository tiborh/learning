/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

//int counter = 0;
size_t samp_size = 10;
int delms = 10;
struct Result {
  int even;
  int odd;
};
struct MinMax {
  int min;
  int max;
};
MinMax check_min_max(int num, int delms) {
  int an0 = analogRead(A0);
  MinMax mm = {};
  mm.min = an0;
  for (int i = 0; i < num; ++i) {
    an0 = analogRead(A0);
    if (an0 < mm.min)
      mm.min = an0;
    if (an0 > mm.max)
      mm.max = an0;
    delay(delms);
  }
  return mm;
}
void print_min_max(size_t samp_size, MinMax in) {
  Serial.print("min: ");
  Serial.print(in.min);
  Serial.print(", max: ");
  Serial.print(in.max);
  Serial.print(" (sample size: ");
  Serial.print(samp_size);
  Serial.println(")");
}
//Result res = {};

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  print_min_max(samp_size,check_min_max(samp_size,delms));
  samp_size *= 2;
//  int an0 = analogRead(A0);
//  int an1 = analogRead(A1);
//  int an2 = analogRead(A2);
//  int an3 = analogRead(A3);
//  int ans[] = {an0,an1,an2,an3};
//  int nu = sizeof(ans) / sizeof(ans[0]);

//  if (an0 % 2 == 0)
//    res.even++;
//  else
//    res.odd++;
    
//  for (int i = 0; i < nu; ++i) {
//    Serial.print(counter);
//    Serial.print(", ");
//    Serial.print(i);
//    Serial.print(": ");
//    Serial.println(ans[i]);
//  }
//  counter++;
//  Serial.print("odd: ");
//  Serial.print(res.odd);
//  Serial.print(", even: ");
//  Serial.println(res.even);
  
  delay(10);        // delay in between reads for stability
}
