/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

//int counter = 0;
size_t samp_size = 0-1;
size_t min_num = samp_size;
size_t max_num = 0;
int delms = 10;
struct MinMax {
  size_t min;
  size_t max;
};
MinMax check_min_max(size_t num, size_t delms) {
  size_t an0 = analogRead(A0);
  MinMax mm = {};
  mm.min = min_num;
  mm.max = max_num;
  for (size_t i = 0; i < num; ++i) {
    an0 = analogRead(A0);
    if (an0 < mm.min)
      mm.min = an0;
    if (an0 > mm.max)
      mm.max = an0;
    delay(delms);
  }
  return mm;
}
void print_min_max(size_t samp_size, MinMax in) {
  Serial.print("min: ");
  Serial.print(in.min);
  Serial.print(", max: ");
  Serial.print(in.max);
  Serial.println(";");
}
//Result res = {};

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("starting...(sample size: ");
  Serial.print(samp_size);
  Serial.println(")");
}

// the loop routine runs over and over again forever:
void loop() {
  print_min_max(samp_size,check_min_max(samp_size,delms));

  delay(10);        // delay in between reads for stability
}
