//RGB LED
//The RGB LED will appear red, green, and blue first, then red, orange, yellow, green, blue, indigo, and purple.
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
/*************************************************************************/
const int redPin = 11;  // R petal on RGB LED module connected to digital pin 11 
const int greenPin = 10;  // G petal on RGB LED module connected to digital pin 10 
const int bluePin = 9;  // B petal on RGB LED module connected to digital pin 9 
const int sDT = 50; // short delay time
const int lDT = 1000; // long delay time
/**************************************************************************/
void setup()
{ 
  pinMode(redPin, OUTPUT); // sets the redPin to be an output 
  pinMode(greenPin, OUTPUT); // sets the greenPin to be an output 
  pinMode(bluePin, OUTPUT); // sets the bluePin to be an output 
}    
/***************************************************************************/
void loop()  // run over and over again  
{ 
  three_blinks();

  for(;;) {
   colour(random(256),random(256),random(256));
   delay(sDT); 
  }
}     
/******************************************************/
void colour (unsigned char red, unsigned char green, unsigned char blue)     // the colour generating function  
{    
  analogWrite(redPin, red);   
  analogWrite(greenPin, green); 
  analogWrite(bluePin, blue); 
}
void three_blinks (void) {
  for (int j = 0; j < 3; ++j) {
    colour(0,0,0);
    delay(lDT/4);
    colour(64,64,64);
    delay(lDT/4);
  }   
}
/******************************************************/


