/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

static const uint8_t analog_pins[] = {A0,A1,A2,A3,A4,A5,A6,A7};
static const size_t samp_size = 0-1;
//static size_t samp_size = 1000;
// UNO
//static const size_t vfrom = 275;
//static const size_t vto = 400;
// NANO
static const size_t vfrom = 190;
static const size_t vto = 310;
//
static const size_t vstep = 5;
//static size_t num_of_outcomes = 6;
static const int delms = 10;

struct Arr {
  size_t sz;
  size_t* arr;
  Arr(size_t sz) {
    Arr::sz = sz;
    Arr::arr = new size_t[Arr::sz];
  }
  ~Arr() { delete[] Arr::arr; }
};


void zero_out(size_t size, size_t* arr, size_t zeroval) {
  for (size_t i = 0; i < size; ++i)
    arr[i] = 0;
}

Arr get_hist_data(size_t from, size_t to, size_t step, size_t ssize, size_t delms) {
  Arr out(lround((to - from) / step) + 2);
  zero_out(out.sz,out.arr,0);
  size_t walker = from + 1;
  for(size_t i = 0; i < ssize; ++i) {
    delay(delms);
    size_t val = analogRead(A0);
    if (val < walker) {
      ++out.arr[0];
      continue;
    }
    if (val > to) {
      ++out.arr[out.sz-1];
      continue;
    }
    ++out.arr[lround((val - from) / step) + 1];
  }
  return out;
}

void print_results(Arr res) {
  for (size_t i = 0; i < res.sz; ++i) {
    Serial.print(i);
    Serial.print(": ");
    Serial.print(res.arr[i]);
    Serial.print("; ");
  }
  Serial.println("");
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.print("starting...(sample size: ");
  Serial.print(samp_size);
  Serial.println(")");
  Serial.print("from: ");
  Serial.print(vfrom);
  Serial.print(" (exclusive), to: ");
  Serial.print(vto);
  Serial.print(" (inclusive), stepping: ");
  Serial.print(vstep);
  Serial.println("");
  Serial.println("first column <= from; last column > to");
}

// the loop routine runs over and over again forever:
void loop() {
  print_results(get_hist_data(vfrom,vto,vstep,samp_size,delms));
  delay(delms);        // delay in between reads for stability
}
