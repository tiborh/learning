#!/usr/bin/env ruby

a = "aleph"
b = "beth"
c = "gimel"
abc = [a,b,c]

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # reference is changed:
  a_id0 = a.object_id
  a = "alpha"
  a_id1 = a.object_id
  assert_not_equal(a_id0,a_id1)
  assert_equal("aleph",abc[0])

  # value is changed
  a.sub!("aleph","alpha")
  a_id2 = a.object_id
  assert_equal(a_id0,a_id2)
  assert_equal("alpha",abc[0])
end
