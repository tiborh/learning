#!/usr/bin/env ruby

def meth(arg1,arg2)
  return arg1 + arg2
end

def meth2(arg1,*args)
  return arg1 + args.first
end

def meth4(arg1,*args)
  return args.inject(arg1) { |sum,num| sum + num }
end

class BigMeth
  def meth3(arg1,arg2=3,*args,&arg3)
    return arg1+arg2+args
  end
end

metho = self.method(:meth)
metho2 = self.method(:meth2)
metho3 = BigMeth.new.method(:meth3)

puts "metho to str: ",metho
puts "nu of args it takes: #{metho.arity}"
puts "parameters it takes: #{metho.parameters}"
puts "metho2 to str: ",metho2
puts "nu of args it takes: #{metho2.arity}"
puts "parameters it takes: #{metho2.parameters}"
puts "metho3 to str: ",metho3
puts "parameters it takse: #{metho3.parameters}"
puts "receiver: #{metho3.receiver}"
puts "Owner: #{metho3.owner}"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_equal("onetwo",metho.call("one","two"))  
  assert_equal(3,metho.call(1,2))
  assert_equal((1..6).to_a,metho.call([1,2,3],[4,5,6]))
  assert_equal(String.methods,String.public_methods)
  assert_equal(10,meth4(1,2,3,4))
end
