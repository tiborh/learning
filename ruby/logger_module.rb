#!/usr/bin/env ruby

require 'logger'

# source:
# https://stackoverflow.com/questions/917566/ruby-share-logger-instance-among-module-classes?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

module Logging
  def logger
    @logger ||= Logging.logger_for(self.class.name)
  end

  # Use a hash class-ivar to cache a unique Logger per class:
  @loggers = {}

  class << self
    def logger_for(classname)
      @loggers[classname] ||= configure_logger_for(classname)
    end

    def configure_logger_for(classname)
      logger = Logger.new(STDOUT)
      logger.progname = classname
      logger
    end
  end
end

class Widget
  # Mix in the ability to log stuff ...
  include Logging

  # ... and proceed to log with impunity:
  def discombobulate(whizbang)
    logger.warn "About to combobulate #{whizbang}"
    # commence discombobulation
  end
end

w1 = Widget.new
w1.discombobulate("something")

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
