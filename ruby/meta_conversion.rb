#!/usr/bin/env ruby

def relay(array, data_type)
  array.map {|it| it.send("to_" + data_type)}
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_equal(["1","2","3"],relay([1,2,3],"s"))
  assert_equal([1,2,3],relay([1.1,2.2,3.3],"i"))
end
