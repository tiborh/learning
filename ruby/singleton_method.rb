#!/usr/bin/env ruby

class Object
  # def singleton_method?(method)
  #   puts method
  #   puts method.class
  #   puts singleton_class
  #   puts "self: #{self}"
  #   puts "self.signleton_class: #{self.singleton_class}"
  #   puts "self.signleton_class.singleton_methods: #{self.singleton_class.singleton_methods}"
  #   puts "self.class.instance_methods: #{self.class.instance_methods.sort}"
  #   #puts "#{singleton_class.methods.sort}"
  #   #puts "#{singleton_class.singleton_methods.sort}"
  #   #puts singleton_class.respond_to?(method)
  # end
  def singleton_method?(method)
    singleton_methods = 
      self.singleton_class.instance_methods - self.class.instance_methods
        
    singleton_methods.include? method
  end
end

foo = "A string"
def foo.shout
  puts foo.upcase
end

# shout is a singleton method.
p foo.singleton_method?(:shout)


if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
