#!/usr/bin/env ruby

require "readline"

if ARGV.length < 1
  puts "Filename is needed as an argument."
  raise "NoArgumentError"
end

if (File.exists?(ARGV[0]))
  if (File.stat(ARGV[0]).readable?)
    if (File.stat(ARGV[0]).size > 0)
      fh = File.open(ARGV[0])
    else
      puts "Zero length file."
      raise "EmptyFileError"
    end
  else
    puts "Unable to read file."
    raise "UnreadableFileError"
  end
else
  puts "File (#{ARGV[0]}) does not exist."
  raise "FileNotFoundError"
end

def find_frequency(str,hsh = Hash.new(0))
  str.downcase.split(/\W+/).inject(hsh) { |a,i| a[i] += 1; a }#.sort_by { |k,v| v}.reverse.to_h
end

word_hash = Hash.new(0)
fh.each do |ln|
  find_frequency(ln,word_hash)
end

puts word_hash.select {|k,v| v > (ARGV[1].to_i || 1)}.sort_by { |k,v| v}.reverse.to_h

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
