#!/usr/bin/env ruby

start_timer = Time.now
x = 0
(1..10000000).each { x+=1 }
end_timer = Time.now
puts "Measured time lapse: #{end_timer - start_timer}."

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
