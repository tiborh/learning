module Flexmod
  class Flex
    raise Exception.new("NotAnArray") unless $arr.class == Array
    $arr.each do |act|
      define_method("#{act}") do |arg|
        "Name of method: #{act}; arg: #{arg}"
      end
    end
  end
end
