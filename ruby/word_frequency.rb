#!/usr/bin/env ruby

def find_frequency(sentence, word)
  sentence.downcase.split.count(word)
end

if __FILE__ == $0
  puts find_frequency("The Earth goes round the Sun.","the")
end
