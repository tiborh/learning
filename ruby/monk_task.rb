#!/usr/bin/env ruby

class Monk
  @arr = ["life","the_universe","everything"]
  @arr.each do |act|
    define_method("meditate_on_#{act}") do
      "I know the meaning of #{act.gsub('_', ' ')}"
    end
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  m = Monk.new
  assert_equal("I know the meaning of life",m.meditate_on_life) 
  assert_equal("I know the meaning of the universe",m.meditate_on_the_universe) 
end
