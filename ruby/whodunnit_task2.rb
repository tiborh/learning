#!/usr/bin/env ruby

require_relative "superclass2.rb"

def location_in_hierarchy(object, method)
  # object = object.class
  # Your code here
  # puts "object: #{object}"
  # puts "method: #{method}"
  # puts "object.respond_to?(method): #{object.respond_to?(method)}"
  return nil if !object.respond_to?(method)
  ancs = get_par_ancs(object).reverse
  puts "All the ancestors: #{ancs}"
  ancs.each do |anc|
    # puts "object examined: #{anc}"
    if (anc == Proc)
      return anc if anc.new {}.respond_to?(method)
    end
    return anc if anc.new.respond_to?(method)
  end
end



if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_nil(location_in_hierarchy(1,"foo"))
  assert_equal(Object,location_in_hierarchy("","eql?"))
  assert_equal(String,location_in_hierarchy("","encode"))
  assert_equal(Numeric,location_in_hierarchy(1,"between?"))
  assert_equal(Proc,location_in_hierarchy(lambda {},"call"))
end
