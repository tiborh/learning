#!/usr/bin/env ruby

a = (1..5).to_a
b = a
c = a.clone

puts "a: #{a}"
puts "b: #{b}"
puts "c: #{c}"

b << 6

puts "a: #{a}"
puts "b: #{b}"
puts "c: #{c}"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
