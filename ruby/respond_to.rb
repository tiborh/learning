#!/usr/bin/env ruby

class Echo
  def respond
    "Bah"
  end
  def method_missing(method_name, *args, &block)
    puts "Method called:    #{method_name}"
    puts "Arguments pushed: #{args}" if args.length > 0
    puts "Block defined:    #{block}" if block
  end
end

a = Echo.new
a.aleph
a.beth(1,2,3)
a.gimel(3,2,1) {|x| x + 3 }
puts "does it respond to 'dalet'? #{a.respond_to?("dalet")}" 
puts "does it respond to 'respond'? #{a.respond_to?("respond")}" 

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
