#!/usr/bin/env ruby

class FibonacciNumbers
  include Enumerable            # if "each" is implemented, the rest is done
  NUMBERS = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
  def each
    i = 0
    while (i < NUMBERS.length)
      yield NUMBERS[i]
      i += 1
    end
  end
  def to_s
    NUMBERS.to_s
  end
end

f=FibonacciNumbers.new
# f.each do |fibonacci_number|
#   puts "f * 10 == #{fibonacci_number*10}"
# end

puts "Printing it: #{f}"

f.each_with_index do |fibonacci_number,index|
  puts "f[#{index}] * 10 == #{fibonacci_number*10}"
end

puts "Same with map: #{f.map {|x| x * 10 }}"
puts "Selecting even elements: #{f.select {|x| x.even?}}"
puts "Rejecting odd elements: #{f.reject {|x| !x.even?}}"
puts "Sum with inject: #{f.inject {|sum,elem| sum + elem } }"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
