#!/usr/bin/env ruby

class SecretiveMonk
  private
  def meaning_of_life
    "You cannot save the effort."
  end
end
  
class Initiate
  def initialize(secretive_monk)
    @secretive_monk = secretive_monk
  end
  
  def	meaning_of_life
    @secretive_monk.send(:meaning_of_life)
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  sm = SecretiveMonk.new
  assert_raise NoMethodError do
    sm.meaning_of_life
  end
  smi = Initiate.new(sm)
  assert_nothing_raised NoMethodError do
    puts smi.meaning_of_life
  end
end
