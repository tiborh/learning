#!/usr/bin/env ruby

class Object
  def superclasses
    retval = self.ancestors.select {|it| !/\w+::\w+/.match(it.to_s) and it.to_s != "Kernel"}
    retval.shift
    retval
  end
end

class Bar
end

class Foo < Bar
end

p Foo.superclasses  # should be [Bar, Object, BasicObject]

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
