#!/usr/bin/env ruby

# source: 
# https://rubymonk.com/learning/books/4-ruby-primer-ascent/chapters/50-debugging/lessons/124-benchmarking_ruby_code
# but logic had to be modified

require 'benchmark'

n=4000
Benchmark.bm do |benchmark|
  benchmark.report do
    a=[]
    m = 0
    n.times { a += [n]; m += 1 }
  end
  benchmark.report do
    a=[]
    m = 0
    n.times { a << m; m += 1 }
  end
  benchmark.report do
    a=[]; a = (1..n).to_a
  end
end

puts "1. times and a += n"
puts "2. times and a << n"
puts "3. Range and map"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
