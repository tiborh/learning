#!/usr/bin/env ruby

class Spy
  def initialize(enemy_agent)
    @enemy_agent = enemy_agent
  end

  # Write your method_missing here
  def method_missing(name,*args,&block)
    puts ""
    puts "method called: #{name}"
    puts "args: #{args}" if args.length > 0
    puts "args: #{block}" if block
    return "formatted result: #{args.join(" : ")}" if args.length > 0
    return "result of #{name}"
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  s = Spy.new("joe")
  assert_equal("result of hello",s.send("hello"))
  assert_equal("formatted result: 1 : 2 : 3",s.send("name",[1,2,3]))
end
