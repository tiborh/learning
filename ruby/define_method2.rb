#!/usr/bin/env ruby

$arr = ["a","b","c"]

require_relative("Flexmod.rb")

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  # assert_raise(Exception) do 
  #   Flex.new("")
  # end
  assert_equal("global-variable",defined?($arr))
  assert_nothing_raised Exception do 
    Flexmod::Flex.new
  end
  a = Flexmod::Flex.new
  assert_equal([:a, :b, :c],(a.methods - Object.new.methods).sort)
  $arr = ["d","e","f"]
  b = Flexmod::Flex.new
  assert_equal([:a, :b, :c],(b.methods - Object.new.methods).sort) # serious limitation of define_method
end
