#!/usr/bin/env ruby

require 'logger'

# source: https://ruby-doc.org/stdlib-2.1.0/libdoc/logger/rdoc/Logger.html

# The levels are:
# (from highest to lowest):

# ANY (unknown)

# UNKNOWN:
#   An unknown message that should always be logged.

# FATAL:
#   An unhandleable error that results in a program crash.

# ERROR:
#   A handleable error condition.

# WARN:
#   A warning.

# INFO:
#   Generic (useful) information about system operation.

# DEBUG:
#   Low-level information for developers.


logs_dir = "logs"

if !File.exists?(logs_dir)
  Dir.mkdir logs_dir
end
if !File.directory?(logs_dir)
  abort "#{logs_dir} must be a directory"
end

logfilename = logs_dir + "/logger.log"
# initialise:
logfile = File.open(logfilename, File::WRONLY | File::APPEND)
logger = Logger.new(logfile)
logger.level = Logger::DEBUG

# this is optional:
original_formatter = Logger::Formatter.new
# lambda would also be good:
logger.formatter = proc { |severity, datetime, progname, msg|
  original_formatter.call(severity, datetime, progname, msg.dump)
}

# to show how easy it is to use
logger.debug("Program starts")
logger.info("Hello logger!")
logger.warn("you are warned")
logger.error("an error has occurred")
logger.fatal("a fatal error has occurred")
logger.unknown("an unknown error has occurred")
logger.info("Bye logger!")
puts "log has been written to #{logfile}"
logger.debug("Program ends")
