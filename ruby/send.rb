#!/usr/bin/env ruby

# source:
# https://rubymonk.com/learning/books/2-metaprogramming-ruby/chapters/25-dynamic-methods/lessons/65-send

class Glider
  def lift
    puts "Rising"
  end
  
  def bank
    puts "Turning"
  end
end

class Nomad
  def initialize(glider)
    @glider = glider
  end

  def do(action)
    # Write your commands here
    @glider.send(action)
  end
end

nomad = Nomad.new(Glider.new)
nomad.do("lift")
nomad.do("bank")

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
