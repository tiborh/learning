#!/usr/bin/env ruby

def a
  puts "a: only called, never a caller"
  puts caller
end

def b
  puts "b: calling a"
  a
end

def c
  puts "c: calling b"
  b
end

puts "in <main>: calling c"
c

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
