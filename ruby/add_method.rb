#!/usr/bin/env ruby

class A
  def initialize(one)
    @one = one
  end
  def give_one
    @one
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  ai = "one"
  a = A.new(ai)
  assert_equal(ai,a.give_one)
  class A
    def give_two
      @one + @one
    end
  end
  assert_true(a.respond_to?("give_one"))
  assert_true(a.respond_to?("give_two"))
  assert_equal("oneone",a.give_two)
end
