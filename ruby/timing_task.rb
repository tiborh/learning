#!/usr/bin/env ruby

def benchmark(&block)
  start_timer = Time.now
  block.call
  end_timer = Time.now
  return end_timer - start_timer
end

time_taken = benchmark do
  sleep 0.1
end
puts "Time taken #{time_taken}"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
