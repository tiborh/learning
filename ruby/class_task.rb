#!/usr/bin/env ruby

# Roby's Object Model
# 02. What is an object?

class Dish
end

class Soup < Dish
end
class IceCream < Dish
end
class ChineseGreenBeans < Dish
end

class DeliveryTray
  DISH_BOWL_MAPPING = { 
    Soup => "soup bowl",
    IceCream => "ice cream bowl",
    ChineseGreenBeans => "serving plate"
  }
    
  def initialize 
    @dishes = Hash.new(0)
  end
  
  def add(dish)
    @dishes[dish.class] += 1
  end

  def to_s
    outarr = []
    @dishes.each do |k,v|
      outarr << "#{v.to_s} #{DISH_BOWL_MAPPING[k]}"
    end
    outarr.join(", ")
  end
  
  def dishes_needed
    self.to_s
  end

  def ==(other_object)
    self.to_s == other_object.to_s
  end
end  

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  d = DeliveryTray.new
  d.add Soup.new; d.add Soup.new
  d.add IceCream.new
  puts d.dishes_needed # should be "2 soup bowl, 1 ice cream bowl"
  
  # tests should come here:
  assert_equal("2 soup bowl, 1 ice cream bowl",d.dishes_needed)
end
