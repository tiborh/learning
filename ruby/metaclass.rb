#!/usr/bin/env ruby

# source: https://rubymonk.com/learning/books/4-ruby-primer-ascent/chapters/39-ruby-s-object-model/lessons/131-singleton-methods-and-metaclasses

# making visible the metaclass (that Ruby also creates automatically) of an object containing a mixin:
class Object
  def metaclass
    class << self
      self
    end
  end
end

foo = "I'm a string object"

# Let us define a singleton method on foo
def foo.shout
  puts self.upcase
end

foo.shout

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  # Lets look at the class of foo's metaclass.
  assert_equal(Class,foo.metaclass.class)
  # The singleton method 'shout' obviously doesn't exist in the actual class.
  assert_false(foo.class.instance_methods.include? :shout)
  # But does it exist in the metaclass?
  assert_true(foo.metaclass.instance_methods.include? :shout)
  a=Object.new
  assert_raise TypeError do     # did not work with {}
    a.metaclass.new
  end
end
