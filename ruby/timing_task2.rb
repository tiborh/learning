#!/usr/bin/env ruby

require 'benchmark'

time_taken = Benchmark.measure { sleep 0.1 }
puts "Time taken #{time_taken}"
puts "first num:     'user cpu time'"
puts "               command executing in user space"
puts "second num:    'system cpu time'"
puts "               sytem-level functions to execute"
puts "third num:     sum of the two above"
puts "(num in pars): the above with cpu waiting time also included (e.g. I/O)"


if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
