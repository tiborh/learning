#!/usr/bin/env ruby


if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  class Foo
  end
  # tests should come here:
  assert_true(Foo.is_a?(Object))
  assert_true(Foo.new.is_a?(Object))
end
