#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

class Aleph
  attr_accessor :aleph
  def initialize(alpha)
    @aleph = alpha
  end
  def to_s
    @aleph.to_s
  end
end

puts "simple string change creates a new object, thus ID too"
s = "一"
puts "s == #{s} (oid: #{s.object_id})"
s = "二"
puts "s == #{s} (oid: #{s.object_id})"

puts "\na mutable object leaves the id unchanged:"
a = Aleph.new("一")
puts "a == #{a} (oid: #{a.object_id})"
a.aleph = "二"
puts "a == #{a} (oid: #{a.object_id})"

puts "\n but string also has its mutators:"
puts "#{"".methods.sort.select {|x| x.to_s.scan(/\w+!/) != []}}"
puts "so, returning to the first example:"
puts "s == #{s} (oid: #{s.object_id})"
s.sub!("二","三")
puts "s == #{s} (oid: #{s.object_id})"

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
