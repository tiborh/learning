#!/usr/bin/env ruby

# source:
# https://rubymonk.com/learning/books/2-metaprogramming-ruby/chapters/32-introduction-to-metaprogramming/lessons/75-being-meta

class Array
  def foldl(method)
    inject {|result, i| result ? result.send(method, i) : i }
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_equal(0.1,[1000.0, 200.0, 50.0].foldl("/"))
  assert_equal(6,[1, 2, 3].foldl('+'))
  assert_equal(-4,[1, 2, 3].foldl('-'))
  assert_equal("hello",['h', 'e', 'l', 'l', 'o'].foldl('concat'))
  assert_equal("hello",['h', 'e', 'l', 'l', 'o'].foldl('+'))
end
