#!/usr/bin/env ruby

# source:
# https://rubymonk.com/learning/books/4-ruby-primer-ascent/chapters/50-debugging/lessons/123-ruby_logging

class Order
  require "logger"
  
  def initialize(order_items, customer)
    @order_items = order_items
    @customer = customer
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
    @logger.info(customer)
    @state = :new
  end

  def procure(vendor)
    if @state == :new
	  @vendor = vendor
	  @state = :procured
      @logger.info(vendor + @state.to_s)
    end
  end

  def pack
    if @state == :procured
	    @state = :packed
      @logger.info(@state.to_s)
    else
      @logger.error("Cannot pack unprocured")
    end
    
  end

  def ship(address)
    if @state == :packed
	    @state = :shipped
	    @shipping_address = address
      @logger.info(address + @state.to_s)
    else
      @logger.error("Cannot ship unpacked")
    end
  end
end

order = Order.new(["mouse", "keyboard"], "Asta")
order.pack
order.procure("Awesome Supplier")
order.ship("Somewhere else")
order.pack
order.ship("The Restaurant, End of the Universe")

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
