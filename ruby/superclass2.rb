#!/usr/bin/env ruby

def get_par_ancs(object)
  parancs = []
  parancs << object.class
  object = object.class
  while object != Object do
    object = object.superclass
    parancs << object
  end
  return parancs
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions
  # tests should come here:
  assert_equal([String, Object],get_par_ancs(""))
  assert_equal([Fixnum, Integer, Numeric, Object],get_par_ancs(1))
  assert_equal([Float, Numeric, Object],get_par_ancs(1.0))
  assert_equal([Array, Object],get_par_ancs([]))
  assert_equal([Hash, Object],get_par_ancs({}))
  assert_equal([Symbol, Object],get_par_ancs(:sym))
  assert_equal([Proc, Object],get_par_ancs(lambda {}))
end
