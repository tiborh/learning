#!/usr/bin/env ruby

puts "#ancestors displays all the ancestors."
puts "Ancestors of Integer: #{Integer.ancestors}"

class A
end

module MyMixin
end

class B < A
  include MyMixin
end

puts ".ancestors lists mixins too."
puts "Ancestors of B: #{B.ancestors}"
puts "BTW, 'Kernel' is also a mixin, providing a set of functions."

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
