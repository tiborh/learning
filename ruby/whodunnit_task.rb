#!/usr/bin/env ruby

def location_in_hierarchy(object, method)
  # Your code here
  # puts "object: #{object}"
  # puts "method: #{method}"
  # puts "object.respond_to?(method): #{object.respond_to?(method)}"
  return nil if !object.respond_to?(method)
  ancs = object.ancestors.reverse
  ancs.each do |anc|
    return anc if anc.respond_to?(method)
  end
end



if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_nil(location_in_hierarchy(Object,"foo"))
  assert_equal(BasicObject,location_in_hierarchy(String,"ancestors"))
  assert_equal(String,location_in_hierarchy(String,"try_convert"))
end
