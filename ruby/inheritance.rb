#!/usr/bin/env ruby

class A
  def speak
    "Speaking as defined in A: #{self}"
  end
end

a = A.new
puts a.speak

class B < A
end

b = B.new
puts b.speak

class C < B
  def speak
    super + ", addition in C"
  end
end

c = C.new
puts c.speak

c1 = C.new
def c1.speak
  super + ", singleton addition in c1"
end

puts c1.speak

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
