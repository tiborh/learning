#!/usr/bin/env ruby

def sum_of_cubes(a, b)
  # Write your code here
  (a..b).to_a.inject(0) {|sum,elem| sum + elem**3}
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_equal(36,sum_of_cubes(1,3))
  assert_equal(216,sum_of_cubes(3,5))
end
