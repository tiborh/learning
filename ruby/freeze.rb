#!/usr/bin/env ruby

a = "first"
a.freeze

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_raise RuntimeError do
    a << "change"
  end
  assert_true(a.frozen?)
  assert_nothing_raised RuntimeError do
    a = "change"
  end
  assert_false(a.frozen?)
  
end
