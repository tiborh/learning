#!/usr/bin/env ruby

class Flex
  attr_reader :arr
  @arr = ["a","b","c"]
  def initialize(arr)
    raise Exception.new("NotAnArray") unless arr.class == Array
    @arr = arr
  end
  @arr.each do |act|
    define_method("#{act}") do |arg|
      "Name of method: #{act}; arg: #{arg}"
    end
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  assert_raise(Exception) do 
    Flex.new("")
  end
  assert_nothing_raised Exception do 
    Flex.new([])
  end
  a = Flex.new(("d".."f").to_a)
  assert_equal([:a, :arr, :b, :c],(a.methods - Object.new.methods).sort)
  assert_equal(["d","e","f"],a.arr)
  assert_equal("Name of method: a; arg: argument",a.a("argument"))
end
