#!/usr/bin/env ruby

class FibonacciNumbers
  
  NUMBERS = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

  def select(&filtering_condition_block)
    filtered_result = []
    NUMBERS.each do |number|
      filtered_result << number if filtering_condition_block.call(number)
    end
    filtered_result
  end

end

# print only the even Fibonacci numbers
nums = FibonacciNumbers.new
nums.select {|num| num % 2 == 0}.each {|num| puts num}

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions

  # tests should come here:
  
end
