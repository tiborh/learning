#!/usr/bin/env ruby

class MethodCall
  def initialize(sym, args)
    @sym = sym
    @args = args
  end
  
  def sym
    @sym
  end
  
  def args
    @args
  end
  
  def ==(other_call)
    @sym == other_call.sym && @args == other_call.args
  end
end

class Spy
  def initialize
    @method_calls = []
  end
  
  # Write your method_missing here
  def method_missing(name,*args,&block)
    puts ""
    puts "method called: #{name}"
    @method_calls << name
    puts "args: #{args}" if args.length > 0
    puts "args: #{block}" if block
  end


  def method_called?(sym, *args)
    # Your superiors will call this method to ask you if you've seen
    # a particular method call. Simply answer them by returning true
    # or false.
    puts "\tmethod_called?"
    puts "\t\tsyn: #{sym}"
    puts "\t\targs: #{args}" if args.length > 0
    return false if @method_calls.length == 0
    return false if @method_calls.find_index(sym) == nil
    return true
  end
end

if __FILE__ == $0
  require 'test/unit'
  extend Test::Unit::Assertions
  # tests should come here:
  spi = Spy.new
  assert_false(spi.method_called?(:hello))
  spi.hello
  assert_true(spi.method_called?(:hello))
  assert_false(spi.method_called?(:bye))
end
