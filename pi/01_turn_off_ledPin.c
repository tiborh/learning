#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

//#define LedPin		0

int main(int argc, char **argv)
{
  int LedPin = 0;
  printf("argc: %d\n",argc);
  for (int i = 0; i < argc; ++i)
    printf("argv[%d]: %s",i,argv[i]);
  printf("\n");
  if (argc > 1)
    LedPin = atoi(argv[1]);
    
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, HIGH);
  
  return 0;
}
