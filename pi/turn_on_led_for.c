#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

//#define LedPin		0
#define DEBUG 0

int main(int argc, char **argv)
{
  int LedPin = 0;
  int onFor = 500;
  if (argc == 1)
    printf("%s [num1] [num2]: turn on LED on pin [num1] for [num2] microseconds.\n",argv[0]);

  if (DEBUG) {
    printf("argc: %d\n",argc);
    for (int i = 0; i < argc; ++i)
      printf("argv[%d]: %s ",i,argv[i]);
    printf("\n");
  }
  if (argc > 1)
    LedPin = atoi(argv[1]);
  if (argc > 2)
    onFor = atoi(argv[2]);
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  pinMode(LedPin, OUTPUT);
  printf("Turning on LED %d for %d microseconds.\n",LedPin,onFor);
  digitalWrite(LedPin, HIGH);
  delay(onFor);
  digitalWrite(LedPin, LOW);
  
  return 0;
}
