/*
  from:
  https://stackoverflow.com/questions/27503904/read-tab-delimited-file-to-structure-in-c
  a. Read a complete line from the file using fgets().
  b. tokenize the input buffer based on the required delimiter [tab in your case] using strtok().
  c. allocate memory (malloc()/ realloc()) to a pointer variable of your structure.
  d. copy the tokenized inputs into the member variables.
  e. Notes:
     1. fgets() reads and stores the trailing \n.
     2. Please check carefully how to use strtok(). The input string should be mutable.
     3. Allocate memory to pointers before using them. IMO, use statically allocated array as 
        struct colour variables.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiringPi.h>
#include <softPwm.h>

#define FILE_COLUMN_NUM 3
#define HEXLENGTH 8
#define DECLENGTH HEXLENGTH * 2
#define MAX_COLOUR_NAME DECLENGTH * 2
#define MAXLINE MAX_COLOUR_NAME * 2

#define uchar unsigned char

#define LedPinRed0    0
#define LedPinGreen0  1
#define LedPinBlue0   2
#define LedPinRed1    3
#define LedPinGreen1  4
#define LedPinBlue1   5

typedef struct{
  char name[MAX_COLOUR_NAME];
  int  red;
  int  green;
  int  blue;
} Colour;

const char *const FN = "rgb.csv";
const char *const LineDelim = "\t";
const char *const ColourDelim = ",";

void ledInit(int,int,int);
void ledColorSet(uchar,uchar,uchar,int,int,int);

int count_lines(void);
void get_data(Colour*,int);
//void parse_line(char*);
//void parse_rgb(char*);

int main (int argc, char** argv) {
  
  if(wiringPiSetup() == -1){ //when initialize wiring failed, printf messageto screen
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  ledInit(LedPinRed0,LedPinGreen0,LedPinBlue0);
  ledInit(LedPinRed1,LedPinGreen1,LedPinBlue1);
  
  int NuOfColours = count_lines();
  printf("Number of lines in %s: %d\n",FN,NuOfColours);
  Colour rgb_colours[NuOfColours];

  get_data(rgb_colours,NuOfColours);
  printf("Colour: %s, Code: %3d,%3d,%3d\n",rgb_colours[0].name,rgb_colours[0].red,rgb_colours[0].green,rgb_colours[0].blue);
  printf("Colour: %s, Code: %3d,%3d,%3d\n",rgb_colours[NuOfColours-1].name,rgb_colours[NuOfColours-1].red,rgb_colours[NuOfColours-1].green,rgb_colours[NuOfColours-1].blue);

  for (int i = 0; i < NuOfColours; ++i) {
    printf("%s (%3d,%3d,%3d)\n",rgb_colours[i].name,rgb_colours[i].red,rgb_colours[i].green,rgb_colours[i].blue);
    ledColorSet(rgb_colours[i].red,rgb_colours[i].green,rgb_colours[i].blue,LedPinRed0,LedPinGreen0,LedPinBlue0);
    ledColorSet(rgb_colours[i].red,rgb_colours[i].green,rgb_colours[i].blue,LedPinRed1,LedPinGreen1,LedPinBlue1);
    delay(500);
  }
  
  return 0;
}

void ledInit(int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmCreate(LedPinRed,  0, 100);
	softPwmCreate(LedPinGreen,0, 100);
	softPwmCreate(LedPinBlue, 0, 100);
}

void ledColorSet(uchar r_val, uchar g_val, uchar b_val,int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmWrite(LedPinRed,   r_val);
	softPwmWrite(LedPinGreen, g_val);
	softPwmWrite(LedPinBlue,  b_val);
}

int count_lines(void) {
  FILE* fh;
  char* fm = "r";
  int nu_of_lines = 0;

  if((fh = fopen(FN,fm)) == NULL)
    {
      printf("Cannot read file: '%s'.\n",FN);
      exit(100);
    }

  char inputline[MAXLINE] = {};

  while ((fgets(inputline,MAXLINE,fh)) != NULL)
    nu_of_lines++;

  fclose(fh);

  return(nu_of_lines);
}

void get_data(Colour* the_colours,int numlines) {
  FILE* fh;
  char* fm = "r";
  int lineNu = 0;

  if((fh = fopen(FN,fm)) == NULL)
    {
      printf("Cannot read file: '%s'.\n",FN);
      exit(100);
    }

  char inputline[MAXLINE] = {};

  while ((fgets(inputline,MAXLINE,fh)) != NULL && lineNu < numlines)
    {
      char *str1,*str2,*token,*subtoken;
      char *saveptr1, *saveptr2;
      
      inputline[strlen(inputline)-1] = '\0';

      int j;
      for (j = 1, str1 = inputline; ; j++, str1 = NULL) {
	token = strtok_r(str1, LineDelim, &saveptr1);
	if (j == 1)
	  strcpy(the_colours[lineNu].name,token);
	if (token == NULL)
	  break;
	//printf("%d: %s\n", j, token);

	if (j == 3) {
	  subtoken = strtok_r(token, ColourDelim, &saveptr2);
	  the_colours[lineNu].red = atoi(subtoken);
	  //printf(" --> %d\n", atoi(subtoken));
	  
	  subtoken = strtok_r(NULL, ColourDelim, &saveptr2);
	  the_colours[lineNu].green = atoi(subtoken);
	  //printf(" --> %d\n", atoi(subtoken));
	  
	  subtoken = strtok_r(NULL, ColourDelim, &saveptr2);
	  the_colours[lineNu].blue = atoi(subtoken);
	  //printf(" --> %d\n", atoi(subtoken));
	}
      }
      //puts(inputline);
      //printf("%s",inputline);
      inputline[0] = '\0';
      ++lineNu;
    }

  if(fclose(fh) == EOF)
    {
      printf("Cannot close file: '%s'.\n",FN);
      exit(102);
    }

  return;
}
