/**********************************************************************
* Filename    : blinkLed.c
* Description : Make an led blinking.
* Author      : Robot
* E-mail      : support@sunfounder.com
* website     : www.sunfounder.com
* Update      : Cavon    2016/07/01
**********************************************************************/
#include <wiringPi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define LedPin		0

int main(void)
{
  srandom(time(NULL));
	// When initialize wiring failed, print messageto screen
	if(wiringPiSetup() == -1){
		printf("setup wiringPi failed !");
		return 1; 
	}
	
	pinMode(LedPin, OUTPUT);

	printf("\n");
	printf("\n");
	printf("========================================\n");
	printf("|              Blink LED               |\n");
	printf("|    ------------------------------    |\n");
	printf("|         LED connect to GPIO0         |\n");
	printf("|                                      |\n");
	printf("|        LED will Blink at 500ms       |\n");
	printf("|                                      |\n");
	printf("|                            SunFounder|\n");
	printf("========================================");
	printf("\n");
	printf("\n");
	
	while(1){
	  int blink_on_time = random() % 500;
	  int blink_off_time = random() % 500;
		// LED on
		digitalWrite(LedPin, LOW);
		printf("...LED on (for %d)\n",blink_on_time);
		delay(blink_on_time);
		// LED off
		digitalWrite(LedPin, HIGH);
		printf("LED off (for %d)...\n",blink_off_time);
		delay(blink_off_time);
	}

	return 0;
}

