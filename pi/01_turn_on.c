#include <wiringPi.h>
#include <stdio.h>

#define LedPin		0

int main(void)
{
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, LOW);
  
  return 0;
}
