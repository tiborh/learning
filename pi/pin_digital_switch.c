#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  int thePin = 0;
  int lowHigh = 0;
  printf("%s <0|1> <num>: turn <low|high> pin[num].\n",argv[0]);

  printf("argc: %d\n",argc);
  for (int i = 0; i < argc; ++i)
    printf("argv[%d]: %s ",i,argv[i]);
  printf("\n");
  if (argc > 1)
    thePin = atoi(argv[1]);
  if (argc > 2)
    lowHigh = atoi(argv[2]); 
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  pinMode(thePin, OUTPUT);
  digitalWrite(thePin, lowHigh ? LOW : HIGH);
  
  return 0;
}
