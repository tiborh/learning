#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

// Turn LED(channel) on
void led_on(int channel){
	digitalWrite(channel, LOW);
}

// Turn LED(channel) off
void led_off(int channel){
	digitalWrite(channel, HIGH);
}

int main(int argc, char **argv)
{
  int LedFrom = 0;
  int LedTo = 7;
  int OnOff = 0;
  printf("%s [num0] [num1] [01]: turn [off/on] LEDS from pin[num0] to pin[num1].\n",argv[0]);

  printf("argc: %d\n",argc);
  for (int i = 0; i < argc; ++i)
    printf("argv[%d]: %s ",i,argv[i]);
  printf("\n");
  if (argc > 1)
    LedFrom = atoi(argv[1]);
  if (argc > 2)
    LedTo = atoi(argv[2]);
  if (argc > 3)
    OnOff = atoi(argv[3]);

  if(LedFrom > LedTo) {
    int tmp = LedFrom;
    LedFrom = LedTo;
    LedTo = tmp;
  }
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  for(int i = LedFrom; i <= LedTo; ++i) {
    pinMode(i, OUTPUT);
    OnOff ? led_on(i) : led_off(i);
    delay(100);
  }
  
  return 0;
}
