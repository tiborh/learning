#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

//#define LedPin		0
#define DEBUG 0

int main(int argc, char **argv)
{
  int LedPin = 0;
  int offOn = 0;
  if (argc == 1)
    printf("%s [01] [num]: turn [off/on] led on pin[num].\n",argv[0]);

  if (DEBUG) {
    printf("argc: %d\n",argc);
    for (int i = 0; i < argc; ++i)
      printf("argv[%d]: %s ",i,argv[i]);
    printf("\n");
  }
  if (argc > 1)
    offOn = atoi(argv[1]);
  if (argc > 2)
    LedPin = atoi(argv[2]); 
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, offOn ? LOW : HIGH);
  
  return 0;
}
