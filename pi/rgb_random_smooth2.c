#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#define uchar unsigned char

const int LedPinRed0=0;
const int LedPinGreen0=1;
const int LedPinBlue0=2;
const int LedPinRed1=3;
const int LedPinGreen1=4;
const int LedPinBlue1=5;

const int sDT = 50; // short delay time
const int lDT = 1000; // long delay time
const int COLOURWIDTH = 5;
const char* const FN = "rgb_saved.txt";

void ledInit();
void ledColourSet(uchar,uchar,uchar);
void change_colours_smoothly(int*,int,int,int);
void load_colours(int *);
void save_colours(int *);

static volatile int shouldRun = 1;

void sighandler(int dummy) {
    shouldRun = 0;
}

int main(int argc, char** argv){
  signal(SIGINT, sighandler);
  
  printf("RGB pins: %d, %d, %d\n",LedPinRed0,LedPinGreen0,LedPinBlue0);
  
  if(wiringPiSetup() == -1){ //when initialize wiring failed, printf messageto screen
    printf("setup wiringPi failed !");
    return 1; 
  }
  
  ledInit();
  
  int colours[3] = {0,0,0};
  load_colours(colours);
  printf("colours loaded from file: %d, %d, %d\n",colours[0],colours[1],colours[2]);
  ledColourSet(colours[0],colours[1],colours[2]);
  srandom(time(NULL));
  while(shouldRun) {
    int redCode = random() % 255;
    int greenCode = random() % 255;
    int blueCode = random() % 255;
    printf("Target colour: %d, %d, %d\n",redCode,greenCode,blueCode);
    change_colours_smoothly(colours,redCode,greenCode,blueCode);
  }
  save_colours(colours);
  change_colours_smoothly(colours,0,0,0);

  return 0;
}

void ledInit(){
	softPwmCreate(LedPinRed0,  0, 100);
	softPwmCreate(LedPinGreen0,0, 100);
	softPwmCreate(LedPinBlue0, 0, 100);
	softPwmCreate(LedPinRed1,  0, 100);
	softPwmCreate(LedPinGreen1,0, 100);
	softPwmCreate(LedPinBlue1, 0, 100);
}

void ledColourSet(uchar r_val, uchar g_val, uchar b_val){
	softPwmWrite(LedPinRed0,   r_val);
	softPwmWrite(LedPinGreen0, g_val);
	softPwmWrite(LedPinBlue0,  b_val);
	softPwmWrite(LedPinRed1,   r_val);
	softPwmWrite(LedPinGreen1, g_val);
	softPwmWrite(LedPinBlue1,  b_val);
}

void change_colours_smoothly(int* colours, int r, int g, int b) {

  int rd = r < colours[0] ? -1 : 1;
  int gd = g < colours[1] ? -1 : 1;
  int bd = b < colours[2] ? -1 : 1;
  
  while (r != colours[0] || g != colours[1] || b != colours[2]) {
    if (r != colours[0])
      colours[0] += rd;
    if (g != colours[1])
      colours[1] += gd;
    if (b != colours[2])
      colours[2] += bd;

    ledColourSet(colours[0],colours[1],colours[2]);
    delay(sDT);
  }
}

void save_colours(int* colours) {
  FILE* fh;
  char* fm = "w";
  if((fh = fopen(FN,fm)) == NULL) {
    printf("Cannot load colours.");
  } else {
    fprintf(fh,"%d\n",colours[0]);
    fprintf(fh,"%d\n",colours[1]);
    fprintf(fh,"%d\n",colours[2]);
    fclose(fh);
  }
}

void load_colours(int* colours) {
  FILE* fh;
  char* fm = "r";
  if((fh = fopen(FN,fm)) == NULL) {
    printf("Cannot save colours.");
  } else {
    char colour[COLOURWIDTH];
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Red code cannot be loaded\n.");
      colours[0] = 0;
    } else
      colours[0] = atoi(colour);
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Green code cannot be loaded\n.");
      colours[1] = 0;
    } else
      colours[1] = atoi(colour);
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Blue code cannot be loaded\n.");
      colours[2] = 0;
    } else
      colours[2] = atoi(colour);
    fclose(fh);
  }
}

