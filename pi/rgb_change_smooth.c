#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define uchar unsigned char

//#define LedPinRed    0
//#define LedPinGreen  1
//#define LedPinBlue   2

const int sDT = 50; // short delay time
const int lDT = 1000; // long delay time
const int COLOURWIDTH = 5;
const char* const FN = "rgb_saved.txt";

void ledInit(int,int,int);
void ledColourSet(uchar,uchar,uchar,int,int,int);
void change_colours_smoothly(int*,int,int,int,int,int,int);
void load_colours(int *);
void save_colours(int *);

int main(int argc, char** argv){

  	if (argc < 4) {
	  printf("Usage: %s <red-code> <green-code> <blue-code> [red-pin green-pin blue-pin]\n",argv[0]);
	  exit(EXIT_FAILURE);
	}

	int LedPinRed = argc > 4 ? atoi(argv[4]) : 0;
	int LedPinGreen = argc > 5 ? atoi(argv[5]) : 1;
	int LedPinBlue = argc > 6 ? atoi(argv[6]) : 2;

	printf("RGB pins: %d, %d, %d\n",LedPinRed,LedPinGreen,LedPinBlue);

	int redCode = atoi(argv[1]);
	int greenCode = atoi(argv[2]);
	int blueCode = atoi(argv[3]);
	printf("Target colour: %d, %d, %d\n",redCode,greenCode,blueCode);
	
  	if(wiringPiSetup() == -1){ //when initialize wiring failed, printf messageto screen
		printf("setup wiringPi failed !");
		return 1; 
	}

	ledInit(LedPinRed,LedPinGreen,LedPinBlue);
	
	//get_saved_colours(
	int colours[3] = {0,0,0};
	load_colours(colours);
	printf("colours loaded from file: %d, %d, %d\n",colours[0],colours[1],colours[2]);
	ledColourSet(colours[0],colours[1],colours[2],LedPinRed,LedPinGreen,LedPinBlue);
        change_colours_smoothly(colours,redCode,greenCode,blueCode,LedPinRed,LedPinGreen,LedPinBlue);
	save_colours(colours);
	return 0;
}

void ledInit(int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmCreate(LedPinRed,  0, 100);
	softPwmCreate(LedPinGreen,0, 100);
	softPwmCreate(LedPinBlue, 0, 100);
}

void ledColourSet(uchar r_val, uchar g_val, uchar b_val,int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmWrite(LedPinRed,   r_val);
	softPwmWrite(LedPinGreen, g_val);
	softPwmWrite(LedPinBlue,  b_val);
}

void change_colours_smoothly(int* colours, int r, int g, int b,int LedPinRed,int LedPinGreen,int LedPinBlue) {

  int rd = r < colours[0] ? -1 : 1;
  int gd = g < colours[1] ? -1 : 1;
  int bd = b < colours[2] ? -1 : 1;
  
  while (r != colours[0] || g != colours[1] || b != colours[2]) {
    if (r != colours[0])
      colours[0] += rd;
    if (g != colours[1])
      colours[1] += gd;
    if (b != colours[2])
      colours[2] += bd;

    ledColourSet(colours[0],colours[1],colours[2],LedPinRed,LedPinGreen,LedPinBlue);
    delay(sDT);
  }
}

void save_colours(int* colours) {
  FILE* fh;
  char* fm = "w";
  if((fh = fopen(FN,fm)) == NULL) {
    printf("Cannot load colours.");
  } else {
    fprintf(fh,"%d\n",colours[0]);
    fprintf(fh,"%d\n",colours[1]);
    fprintf(fh,"%d\n",colours[2]);
    fclose(fh);
  }
}

void load_colours(int* colours) {
  FILE* fh;
  char* fm = "r";
  if((fh = fopen(FN,fm)) == NULL) {
    printf("Cannot save colours.");
  } else {
    char colour[COLOURWIDTH];
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Red code cannot be loaded\n.");
      colours[0] = 0;
    } else
      colours[0] = atoi(colour);
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Green code cannot be loaded\n.");
      colours[1] = 0;
    } else
      colours[1] = atoi(colour);
    if (fgets(colour,COLOURWIDTH,fh) == NULL) {
      printf("Blue code cannot be loaded\n.");
      colours[2] = 0;
    } else
      colours[2] = atoi(colour);
    fclose(fh);
  }
}

