/**********************************************************************
* Filename    : breathLed.c
* Description : Make LED breath.
* Author      : Robot
* E-mail      : support@sunfounder.com
* website     : www.sunfounder.com
* Update      : Cavon    2016/07/01
**********************************************************************/

#include <wiringPi.h>
#include <stdio.h>
#include <softPwm.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h>

#define LEDELAY 100
#define STARTLED 0
#define LEDNU 8
#define LEDONELESS LEDNU-1
#define SWITCHON 1
#define SWITCHOFF 0

static volatile int shouldRun = 1;

void sighandler(int dummy) {
    shouldRun = 0;
}

// Turn LED(channel) on
void led_on(int channel){
	digitalWrite(channel, LOW);
}

// Turn LED(channel) off
void led_off(int channel){
	digitalWrite(channel, HIGH);
}

void left_to_right(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from left to right.\n",onOff ? "on" : "off");
  for(int i = from; i < to; ++i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

void right_to_left(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from right to left.\n",onOff ? "on" : "off");
  for(int i = from; i >= to; --i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

int main(void)
{
  signal(SIGINT, sighandler);

  if(wiringPiSetup() == -1){ //when initialize wiring failed, print messageto screen
    printf("setup wiringPi failed !");
    return 1; 
  }

  printf("\n");
  printf("\n");
  printf("========================================\n");
  printf("|              Breath LED              |\n");
  printf("|    ------------------------------    |\n");
  printf("|         LED connect to GPIO0         |\n");
  printf("|                                      |\n");
  printf("|            Make LED breath           |\n");
  printf("|                                      |\n");
  printf("|                            SunFounder|\n");
  printf("========================================\n");
  printf("\n");
  printf("\n");

  while(shouldRun){
    for(int j = STARTLED; j < LEDNU && shouldRun; ++j) {
      softPwmCreate(j, 0, 100);
      printf("Breath on\n");
      for(int i=0;i<=100;i++){
	softPwmWrite(j, i);
	delay(LEDELAY/5);
      }
      delay(LEDELAY*10);
      /* printf("Breath off\n"); */
      /* for(int i=100;i>=0;i--){ */
      /* 	softPwmWrite(j, i); */
      /* 	delay(LEDELAY/5); */
      /* } */
    }
  }
	
  return EXIT_SUCCESS;
}

