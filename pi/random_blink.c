#include <wiringPi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h>

#define LEDELAY 100
#define STARTLED 0
#define LEDNU 8
#define LEDONELESS LEDNU-1
#define SWITCHON 1
#define SWITCHOFF 0

static volatile int shouldRun = 1;

void sighandler(int dummy) {
    shouldRun = 0;
}

// Turn LED(channel) on
void led_on(int channel){
	digitalWrite(channel, LOW);
}

// Turn LED(channel) off
void led_off(int channel){
	digitalWrite(channel, HIGH);
}

void left_to_right(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from left to right.\n",onOff ? "on" : "off");
  for(int i = from; i < to; ++i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

void right_to_left(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from right to left.\n",onOff ? "on" : "off");
  for(int i = from; i >= to; --i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

int main(void){
  signal(SIGINT, sighandler);
  
  // When initialize wiring failed, print messageto screen
  if(wiringPiSetup() == -1){
    printf("setup wiringPi failed !");
    return 1; 
  }
  // Set LEDNU pins' modes to output
  for(int i=STARTLED;i<LEDNU;i++){       
    pinMode(i, OUTPUT);
  }
  
  int leds[LEDNU] = {0};
  srandom(time(NULL));
  
  while(shouldRun){
    int rled = random() % LEDNU;
    int waitime = random() % (LEDELAY * 2);
    if (leds[rled]) {
      led_off(rled);
      leds[rled] = 0;
    } else {
      led_on(rled);
      leds[rled] = 1;
    }
    delay(waitime);
  }
  time(NULL) % 2 ? left_to_right(STARTLED,LEDNU,SWITCHOFF) : right_to_left(LEDONELESS,STARTLED,SWITCHOFF);
  
  return EXIT_SUCCESS;
}

