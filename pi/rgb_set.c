#include <wiringPi.h>
#include <softPwm.h>
#include <stdio.h>
#include <stdlib.h>

#define uchar unsigned char

//#define LedPinRed    0
//#define LedPinGreen  1
//#define LedPinBlue   2

void ledInit(int,int,int);
void ledColorSet(uchar,uchar,uchar,int,int,int);

int main(int argc, char** argv){

  	if (argc < 4) {
	  printf("Usage: %s <red-code> <green-code> <blue-code> [red-pin green-pin blue-pin]\n",argv[0]);
	  exit(EXIT_FAILURE);
	}

	int LedPinRed = argc > 4 ? atoi(argv[4]) : 0;
	int LedPinGreen = argc > 5 ? atoi(argv[5]) : 1;
	int LedPinBlue = argc > 6 ? atoi(argv[6]) : 2;

	printf("Red pin: %d\nGreen pin: %d\nBlue pin: %d\n",LedPinRed,LedPinGreen,LedPinBlue);

	int redCode = atoi(argv[1]);
	int greenCode = atoi(argv[2]);
	int blueCode = atoi(argv[3]);
	printf("Red code: %d\nGreen code: %d\nBlue code: %d\n",redCode,greenCode,blueCode);
	
  	if(wiringPiSetup() == -1){ //when initialize wiring failed, printf messageto screen
		printf("setup wiringPi failed !");
		return 1; 
	}

	ledInit(LedPinRed,LedPinGreen,LedPinBlue);

	ledColorSet(redCode,greenCode,blueCode,LedPinRed,LedPinGreen,LedPinBlue);
	
	return 0;
}

void ledInit(int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmCreate(LedPinRed,  0, 100);
	softPwmCreate(LedPinGreen,0, 100);
	softPwmCreate(LedPinBlue, 0, 100);
}

void ledColorSet(uchar r_val, uchar g_val, uchar b_val,int LedPinRed,int LedPinGreen,int LedPinBlue){
	softPwmWrite(LedPinRed,   r_val);
	softPwmWrite(LedPinGreen, g_val);
	softPwmWrite(LedPinBlue,  b_val);
}
