/**********************************************************************
* Filename    : 8Led.c
* Description : Make a breathing led.
* Author      : Robot
* E-mail      : support@sunfounder.com
* website     : www.sunfounder.com
* Update      : Cavon    2016/07/01
**********************************************************************/
#include <wiringPi.h>
#include <stdio.h>

#define LEDELAY 100
#define STARTLED 0
#define LEDNU 8
#define LEDONELESS LEDNU-1
#define SWITCHON 1
#define SWITCHOFF 0

// Turn LED(channel) on
void led_on(int channel){
	digitalWrite(channel, LOW);
}

// Turn LED(channel) off
void led_off(int channel){
	digitalWrite(channel, HIGH);
}

void left_to_right(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from left to right.\n",onOff ? "on" : "off");
  for(int i = from; i < to; ++i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

void right_to_left(int from, int to, int onOff) {
  void (*ptrLedAct)(int) = onOff ? &led_on : led_off;
  printf("All %s from right to left.\n",onOff ? "on" : "off");
  for(int i = from; i >= to; --i) {
    (*ptrLedAct)(i);
    delay(LEDELAY);
  }
}

int main(void){
	// When initialize wiring failed, print messageto screen
	if(wiringPiSetup() == -1){
		printf("setup wiringPi failed !");
		return 1; 
	}
	// Set LEDNU pins' modes to output
	for(int i=0;i<LEDNU;i++){       
		pinMode(i, OUTPUT);
	}

	printf("\n");
	printf("\n");
	printf("========================================\n");
	printf("|                8 LEDs                |\n");
	printf("|    ------------------------------    |\n");
	printf("|         LED0 connect to GPIO0        |\n");
	printf("|         LED1 connect to GPIO1        |\n");
	printf("|         LED2 connect to GPIO2        |\n");
	printf("|         LED3 connect to GPIO3        |\n");
	printf("|         LED4 connect to GPIO4        |\n");
	printf("|         LED5 connect to GPIO5        |\n");
	printf("|         LED6 connect to GPIO6        |\n");
	printf("|         LED7 connect to GPIO7        |\n");
	printf("|                                      |\n");
	printf("|            Flow LED effect           |\n");
	printf("|                                      |\n");
	printf("|                            SunFounder|\n");
	printf("========================================\n");
	printf("\n");
	printf("\n");

	while(1){
		// Turn LED on from left to right
		printf("From left to right.\n");
		for(int i=STARTLED;i<LEDNU;i++){
			led_on(i);
			delay(LEDELAY);
			led_off(i);
		}
		// Turn LED off from right to left
		printf("From right to left.\n");
		for(int i=LEDONELESS;i>=STARTLED;i--){
			led_on(i);
			delay(LEDELAY);
			led_off(i);
		}
		left_to_right(STARTLED,LEDNU,SWITCHON);
		right_to_left(LEDONELESS,STARTLED,SWITCHOFF);
		left_to_right(STARTLED,LEDNU,SWITCHON);
		left_to_right(STARTLED,LEDNU,SWITCHOFF);
		right_to_left(LEDONELESS,STARTLED,SWITCHON);
		left_to_right(STARTLED,LEDNU,SWITCHOFF);
		right_to_left(LEDONELESS,STARTLED,SWITCHON);
		right_to_left(LEDONELESS,STARTLED,SWITCHOFF);
	}

	return 0;
}

