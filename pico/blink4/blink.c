/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"

//#define SHORT 250
//#define LONG 500
#define BLIP 250

void init_pins(uint n, const uint* pins) {
  for(uint i = 0; i < n; ++i) {
    gpio_init(pins[i]);
    gpio_set_dir(pins[i], GPIO_OUT);
  }
}

void make_onoff(uint n, const uint* pins, uint onoff) {
  for(uint i = 0; i < n; ++i) {
    printf("pin %u: %s\n",pins[i],onoff ? "on" : "off");
    gpio_put(pins[i],onoff);
  }
  sleep_ms(BLIP);
}

void make_blip(uint n, const uint* pins) {
  make_onoff(n,pins,1);
  make_onoff(n,pins,0);
}

int main() {
  const uint NU_PINS = 5;
  const uint LED_PINS[] = {25, 14, 15, 16, 17};

  stdio_init_all();
  init_pins(NU_PINS,LED_PINS);

  for (uint i = 0; ; ++i) {
    const uint PINS[] = {LED_PINS[0],LED_PINS[(i % (NU_PINS-1))+1]};
    make_blip(2,PINS);
  }
}
