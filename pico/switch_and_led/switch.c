/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"

int main() {
  const uint LED_PIN = 25;
  const uint LED_PIN2 = 15;
  const uint IN_PIN = 14;

  stdio_init_all();

  gpio_init(LED_PIN);
  gpio_init(LED_PIN2);
  gpio_init(IN_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);
  gpio_set_dir(LED_PIN2, GPIO_OUT);
  gpio_set_dir(IN_PIN, GPIO_IN);
  //gpio_pull_up(IN_PIN);

  while(true) {
    if(gpio_get(IN_PIN)){
      gpio_put(LED_PIN, 0);
      gpio_put(LED_PIN2, 1);
    } else {
      gpio_put(LED_PIN, 1);
      gpio_put(LED_PIN2, 0);
    }
  }
}
