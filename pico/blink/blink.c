/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"

#define SHORT 250
#define LONG 500

int main() {
  const uint LED_PIN = 0;

  stdio_init_all();

  gpio_init(LED_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);
  for (unsigned i = 0; ; ++i) {
    printf("i: %d\n",i);
    gpio_put(LED_PIN, 1);
    if (i % 2) {
      puts("LED:  on, sleep: short");
      sleep_ms(SHORT);
    } else {
      printf("LED:  on, sleep: %s\n", i % 3 ? "short" : "long");
      sleep_ms(i % 3 ? SHORT : LONG);
    }
    gpio_put(LED_PIN, 0);
    if (i % 2) {
      printf("LED: off, sleep: %s\n", i % 3 ? "long" : "short");
      sleep_ms(i % 3 ? LONG : SHORT);
    } else {
      puts("LED: off, sleep: long");
      sleep_ms(LONG);
    }
  }
}
