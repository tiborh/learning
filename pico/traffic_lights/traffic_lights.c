/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdbool.h>
#include "pico/stdlib.h"

#define SHORT 500
#define LONG 750
//#define BLIP SHORT
#define VERYLONG 5000
#define BUZZ_SLEEP 10
#define BUZZ_NUM ((VERYLONG / BUZZ_SLEEP) / 4)
#define BUZZ_SILENCE (2 * BUZZ_NUM)
#define BUZZ_SHORT (BUZZ_SILENCE / (2*BUZZ_SLEEP))
#define NU_SHORT BUZZ_SLEEP

bool button_pressed = false;

void *button_check(void *vargp, const uint IN_PIN) {
  while(1) {
    button_pressed = gpio_get(IN_PIN) ? true : false;
    button_pressed=false;
    sleep_ms(200);
  }
} 

void init_pins(uint n, const uint* pins) {
  for(uint i = 0; i < n; ++i) {
    gpio_init(pins[i]);
    gpio_set_dir(pins[i], GPIO_OUT);
  }
}

/* void make_onoff(uint n, const uint* pins, uint onoff) { */
/*   for(uint i = 0; i < n; ++i) { */
/*     printf("pin %u: %s\n",pins[i],onoff ? "on" : "off"); */
/*     gpio_put(pins[i],onoff); */
/*   } */
/*   sleep_ms(BLIP); */
/* } */

/* void make_blip(uint n, const uint* pins) { */
/*   make_onoff(n,pins,1); */
/*   make_onoff(n,pins,0); */
/* } */

void blink_warn(uint r, uint a, uint g) {
  gpio_put(g,0);
  gpio_put(r,0);
  while(1) {
    gpio_put(a,1);
    sleep_ms(LONG);
    gpio_put(a,0);
    sleep_ms(LONG);
  }
}

void green_to_red(uint r, uint a, uint g) {
  gpio_put(g,1);
  sleep_ms(SHORT);
  gpio_put(g,0);
  gpio_put(a,1);
  sleep_ms(LONG);
  gpio_put(a,0);
  gpio_put(r,1);
}

void red_to_green(uint r, uint a, uint g) {
  gpio_put(r,1);
  sleep_ms(SHORT);
  gpio_put(a,1);
  sleep_ms(LONG);
  gpio_put(r,0);
  gpio_put(a,0);
  gpio_put(g,1);
}

void call_buzz(uint bz, uint num) {
  for(int i = 0; i < num; ++i) {
    gpio_put(bz,1);
    sleep_ms(BUZZ_SLEEP);
    gpio_put(bz,0);
    sleep_ms(BUZZ_SLEEP);
  }
}

void call_buzz2(uint bz, uint num) {
  for(int i = 0; i < num; ++i) {
    call_buzz(bz,BUZZ_SHORT);
    sleep_ms(BUZZ_SILENCE);
  }
}

int main() {
  const uint NU_PINS = 4;
  const uint LED_PINS[] = {25, 16, 17, 18};
  const uint BUZZ_PIN = 19;
  const uint red = LED_PINS[1];
  const uint amber = LED_PINS[2];
  const uint green = LED_PINS[3];
  const uint IN_PIN = 14;

  stdio_init_all();
  init_pins(NU_PINS,LED_PINS);
  gpio_init(IN_PIN);
  gpio_init(BUZZ_PIN);
  gpio_set_dir(IN_PIN, GPIO_IN);
  gpio_set_dir(BUZZ_PIN, GPIO_OUT);
  for (uint i = 0; ; ++i) {
    gpio_put(red,1);
    if (gpio_get(IN_PIN)) {
      red_to_green(red,amber,green);
      call_buzz(BUZZ_PIN,BUZZ_NUM);
      call_buzz2(BUZZ_PIN,NU_SHORT);
      //sleep_ms(VERYLONG);
      green_to_red(red,amber,green);
    }
  }

  return 0;
}
