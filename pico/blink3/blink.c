/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdbool.h>
#include "pico/stdlib.h"
//#include "pico/multicore.h"

//#define SHORT 250
//#define LONG 500
#define BLIP 250

bool button_pressed = false;

/* void core1_entry() { */
/*     while (1) { */
/*         // Function pointer is passed to us via the FIFO */
/*         // We have one incoming int32_t as a parameter, and will provide an */
/*         // int32_t return value by simply pushing it back on the FIFO */
/*         // which also indicates the result is ready. */
/*         int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking(); */
/*         int32_t p = multicore_fifo_pop_blocking(); */
/*         int32_t result = (*func)(p); */
/*         multicore_fifo_push_blocking(result); */
/*     } */
/* } */


void *button_check(void *vargp, const uint IN_PIN) {
  while(1) {
    button_pressed = gpio_get(IN_PIN) ? true : false;
    button_pressed=false;
    sleep_ms(200);
  }
} 

void init_pins(uint n, const uint* pins) {
  for(uint i = 0; i < n; ++i) {
    gpio_init(pins[i]);
    gpio_set_dir(pins[i], GPIO_OUT);
  }
}

void make_onoff(uint n, const uint* pins, uint onoff) {
  for(uint i = 0; i < n; ++i) {
    printf("pin %u: %s\n",pins[i],onoff ? "on" : "off");
    gpio_put(pins[i],onoff);
  }
  sleep_ms(BLIP);
}

void make_blip(uint n, const uint* pins) {
  make_onoff(n,pins,1);
  make_onoff(n,pins,0);
}

int main() {
  const uint NU_PINS = 4;
  const uint LED_PINS[] = {25, 16, 17, 18};
  const uint IN_PIN = 14;

  stdio_init_all();
  init_pins(NU_PINS,LED_PINS);
  gpio_init(IN_PIN);
  pthread_t thread_id;
  for (uint i = 0; ; ++i) {
    if (!gpio_get(IN_PIN)) {
      const uint PINS[] = {LED_PINS[0],LED_PINS[(i % (NU_PINS-1))+1]};
      make_blip(2,PINS);
    } else {
      const uint PINS[] = {LED_PINS[0]};
      make_blip(1,PINS);
    }
  }

  return 0;
}
