/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdbool.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"

#define VERYSHORT 250
#define SHORT (2 * VERYSHORT)
#define LONG (3 * VERYSHORT)
//#define BLIP SHORT
#define VERYLONG (10 * SHORT)
#define BUZZ_SLEEP 10
#define BUZZ_NUM ((VERYLONG / BUZZ_SLEEP) / 4)
#define BUZZ_SILENCE (2 * BUZZ_NUM)
#define BUZZ_SHORT (BUZZ_SILENCE / (2*BUZZ_SLEEP))
#define NU_SHORT BUZZ_SLEEP

bool button_pressed = false;

void button_check() {
  const uint IN_PIN = 14;
  const uint OUT_PIN = 25;
  gpio_init(IN_PIN);
  gpio_init(OUT_PIN);
  gpio_set_dir(IN_PIN, GPIO_IN);
  gpio_set_dir(OUT_PIN, GPIO_OUT);
  while(1) {
    if (gpio_get(IN_PIN)) {
	button_pressed = true;
	gpio_put(OUT_PIN,1);
	sleep_ms(VERYSHORT);
	gpio_put(OUT_PIN,0);
    } else
	button_pressed = false;
    //printf("button pressed? %s\n",button_pressed ? "true" : "false");
  }
} 

void init_pins(uint n, const uint* pins) {
  for(uint i = 0; i < n; ++i) {
    gpio_init(pins[i]);
    gpio_set_dir(pins[i], GPIO_OUT);
  }
}

void blink_warn(uint r, uint a, uint g) {
  gpio_put(g,0);
  gpio_put(r,0);
  while(1) {
    gpio_put(a,1);
    sleep_ms(LONG);
    gpio_put(a,0);
    sleep_ms(LONG);
  }
}

void green_to_red(uint r, uint a, uint g) {
  gpio_put(g,1);
  sleep_ms(SHORT);
  gpio_put(g,0);
  gpio_put(a,1);
  sleep_ms(LONG);
  gpio_put(a,0);
  gpio_put(r,1);
}

void red_to_green(uint r, uint a, uint g) {
  gpio_put(r,1);
  sleep_ms(SHORT);
  gpio_put(a,1);
  sleep_ms(LONG);
  gpio_put(r,0);
  gpio_put(a,0);
  gpio_put(g,1);
}

void call_buzz(uint bz, uint num) {
  for(int i = 0; i < num; ++i) {
    gpio_put(bz,1);
    sleep_ms(BUZZ_SLEEP);
    gpio_put(bz,0);
    sleep_ms(BUZZ_SLEEP);
  }
}

void call_buzz2(uint bz, uint num) {
  for(int i = 0; i < num; ++i) {
    call_buzz(bz,BUZZ_SHORT);
    sleep_ms(BUZZ_SILENCE);
  }
}

int main() {
  const uint NU_PINS = 3;
  const uint LED_PINS[] = {16, 17, 18};
  const uint BUZZ_PIN = 19;
  const uint red = LED_PINS[0];
  const uint amber = LED_PINS[1];
  const uint green = LED_PINS[2];

  stdio_init_all();
  init_pins(NU_PINS,LED_PINS);
  gpio_init(BUZZ_PIN);
  gpio_set_dir(BUZZ_PIN, GPIO_OUT);
  multicore_launch_core1(button_check);
  for (uint i = 0; ; ++i) {
    gpio_put(red,1);
    printf("button pressed? %s\n",button_pressed ? "true" : "false");
    if (button_pressed) {
      sleep_ms(LONG);
      red_to_green(red,amber,green);
      call_buzz(BUZZ_PIN,BUZZ_NUM);
      call_buzz2(BUZZ_PIN,NU_SHORT);
      button_pressed = false;
      green_to_red(red,amber,green);
    }
  }

  return 0;
}
