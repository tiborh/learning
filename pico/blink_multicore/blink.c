/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"

#define SHORT 250
#define LONG 500

const uint LED_PIN = 16;
const uint IN_PIN = 14;

void blink_led(uint lednu) {
    gpio_put(lednu, 1);
    sleep_ms(LONG);
    gpio_put(lednu, 0);
    sleep_ms(LONG);
}

void core1_entry() {
  while(1) {
    if(gpio_get(IN_PIN))
      blink_led(LED_PIN);
  }  
}

int main() {
  const uint STATUS_PIN = 25;

  stdio_init_all();
  gpio_init(LED_PIN);
  gpio_init(STATUS_PIN);
  gpio_init(IN_PIN);
  gpio_set_dir(STATUS_PIN, GPIO_OUT);
  gpio_set_dir(LED_PIN, GPIO_OUT);
  gpio_set_dir(IN_PIN, GPIO_IN);
  multicore_launch_core1(core1_entry);
  while(1) {
    blink_led(STATUS_PIN);
  }

  return 0;
}
