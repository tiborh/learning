/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"

#define VERYSHORT 125
#define SHORT (2 * VERYSHORT)
#define LONG (2 * SHORT)

void blink_led(uint lednu) {
    gpio_put(lednu, 1);
    sleep_ms(LONG);
    gpio_put(lednu, 0);
    sleep_ms(LONG);
}

void core1_entry() {
  const uint IN_PIN = 14;
  const int32_t PRESSED = 1;
  const int32_t OK = 1;
  const int32_t NOK = 0;
  int32_t LISTENING = OK;
  gpio_init(IN_PIN);
  gpio_set_dir(IN_PIN, GPIO_IN);
  while(1) {
    int32_t fifo_status = multicore_fifo_get_status();
    int32_t received = 0;
    received = multicore_fifo_pop_blocking();
    if(received)
      LISTENING = OK;
    printf("LISTENING on core1? %d\n",LISTENING);
    if(LISTENING) {
      puts("LISTENING signal received on core1.");
      if(gpio_get(IN_PIN)) {
	multicore_fifo_push_blocking(PRESSED);
	LISTENING = NOK;
	puts("LISTENING switched off on core1.");
      }
    }
    sleep_ms(VERYSHORT);
  }
}

int main() {
  const uint LED_PIN = 16;
  const uint STATUS_PIN = 25;
  const int32_t OK = 1;
  int32_t pressed = 0;
  stdio_init_all();
  gpio_init(LED_PIN);
  gpio_init(STATUS_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);
  gpio_set_dir(STATUS_PIN, GPIO_OUT);
  multicore_launch_core1(core1_entry);
  gpio_put(STATUS_PIN, 1);
  while(1) {
    int32_t res = 0;
    res = multicore_fifo_pop_blocking();
    if (res)
      pressed = OK;
    int32_t fifo_status = 0;
    fifo_status = multicore_fifo_get_status();
    printf("pressed on core0? %d\n",pressed);
    if(pressed) {
      puts("PRESSED signal received on core0.");
      gpio_put(STATUS_PIN, 0);
      blink_led(LED_PIN);
      gpio_put(STATUS_PIN, 1);
      multicore_fifo_push_blocking(OK);
      puts("LISTENING signal sent on core0.");
    } else {
      blink_led(STATUS_PIN);
    }
  }

  return 0;
}
