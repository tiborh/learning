#!/usr/bin/env Rscript

## source:
## https://httr2.r-lib.org/

source("common.r")

check.packages(c("xml2","httr2"))

url <- "https://r-project.org"

req <- httr2::request(url)

rlog::log_info("Plain request:")
rlog::log_info(str(req))
## List of 7
##  $ url     : chr "https://r-project.org"
##  $ method  : NULL
##  $ headers : list()
##  $ body    : NULL
##  $ fields  : list()
##  $ options : list()
##  $ policies: list()
##  - attr(*, "class")= chr "httr2_request"

rlog::log_info("add header:")
res1 <- req %>% httr2::req_headers("Accept" = "application/json")
rlog::log_info(str(res1))
## List of 7
##  $ url     : chr "https://r-project.org"
##  $ method  : NULL
##  $ headers :List of 1
##   ..$ Accept: chr "application/json"
##  $ body    : NULL
##  $ fields  : list()
##  $ options : list()
##  $ policies: list()
##  - attr(*, "class")= chr "httr2_request"

rlog::log_info("add body:")
res2 <- req %>% httr2::req_body_json(list(x = 1, y = 2))
rlog::log_info(str(res2))
## List of 7
##  $ url     : chr "https://r-project.org"
##  $ method  : NULL
##  $ headers : list()
##  $ body    :List of 4
##   ..$ data        :List of 2
##   .. ..$ x: num 1
##   .. ..$ y: num 2
##   ..$ type        : chr "json"
##   ..$ content_type: NULL
##   ..$ params      :List of 3
##   .. ..$ auto_unbox: logi TRUE
##   .. ..$ digits    : num 22
##   .. ..$ null      : chr "null"
##  $ fields  : list()
##  $ options : list()
##  $ policies: list()
##  - attr(*, "class")= chr "httr2_request"

rlog::log_info("set retry:")
res3 <- req %>% httr2::req_retry(max_tries = 5)
rlog::log_info(str(res3))
## List of 7
##  $ url     : chr "https://r-project.org"
##  $ method  : NULL
##  $ headers : list()
##  $ body    : NULL
##  $ fields  : list()
##  $ options : list()
##  $ policies:List of 1
##   ..$ retry_max_tries: num 5
##  - attr(*, "class")= chr "httr2_request"

rlog::log_info("set method:")
res4 <- req %>% httr2::req_method("PATCH")
rlog::log_info(str(res4))
## List of 7
##  $ url     : chr "https://r-project.org"
##  $ method  : chr "PATCH"
##  $ headers : list()
##  $ body    : NULL
##  $ fields  : list()
##  $ options : list()
##  $ policies: list()
##  - attr(*, "class")= chr "httr2_request"

rlog::log_info("see request with dry run:")
res5 <- req %>% httr2::req_dry_run()
rlog::log_info(str(res5))
## Host: r-project.org
## User-Agent: httr2/0.2.3 r-curl/5.0.1 libcurl/8.1.2
## Accept: */*
## Accept-Encoding: deflate, gzip, br, zstd

## List of 3
##  $ method : chr "GET"
##  $ path   : chr "/"
##  $ headers:List of 4
##   ..$ accept         : chr "*/*"
##   ..$ accept-encoding: chr "deflate, gzip, br, zstd"
##   ..$ host           : chr "r-project.org"
##   ..$ user-agent     : chr "httr2/0.2.3 r-curl/5.0.1 libcurl/8.1.2"

rlog::log_info("perform the request:")
res6 <- resp <- httr2::req_perform(req)
rlog::log_info(str(res6))

if (res6$status_code == 200) {
    rlog::log_info(paste("status description:",res6 %>% httr2::resp_status_desc()))
    rlog::log_info(paste("content type:",res6 %>% httr2::resp_content_type()))
    body <- rawToChar(res6$body)
    rlog::log_info("Body:")
    rlog::log_info(str(body))
    print(res6 %>% httr2::resp_body_html())
}
