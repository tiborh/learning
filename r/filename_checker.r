#!/usr/bin/env Rscript

source("common.r")

usage <- function() {
    rlog::log_info(paste(get.script.fn(),"<filename>"))
}

if (NARGS < 1) {
    usage()
    q()
}

res <- NULL
if (NARGS > 1) {
    res <- f.checker(ARGS[1],F)
} else {
    res <- f.checker(ARGS[1])
}
    
rlog::log_info(res)
