#!/usr/bin/env Rscript

source("common.r")

a <- factor(as.character(sample(1:5,12,replace=T)))
printv("a",a)
printv("levels(a)",levels(a))
print.obj("summary:",summary(a))
a[13] <- "6"
cat("after attempting to add '6':\n")
printv("a",a)
print.obj("summary:",summary(a))
cat("corect way:\n\t1. add new level,\n\t2. then add new value\n")
levels(a) <- append(levels(a),"6")
a[13] <- "6"
printv("a",a)
printv("levels(a)",levels(a))
print.obj("summary:",summary(a))

