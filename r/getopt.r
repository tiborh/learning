#!/usr/bin/env Rscript

source("common.r")

check.packages(c("getopt"))

## get options, using the spec as defined by the enclosed list.
## we read the options from the default: commandArgs(TRUE).
##    1      2     3             4
##   long  short  arg flag    data type
##      name      0==no arg   * logical
##                1==needed   * integer
##                x==optional * double (numeric --> double)
##                            * complex
##                            * charcter

spec = matrix(c(
    'verbose', 'v', 2, "integer", "get more on the screen",
    'help' , 'h', 0, "logical", "spit out this help",
    'count' , 'c', 1, "integer", "how many values to generate",
    'mean' , 'm', 1, "double", "the mean value of the generated numbers",
    'sd' , 's', 1, "double", "the standard deviance of the generated numbers"
), byrow=TRUE, ncol=5)
opt = getopt(spec)

## if help was asked for print a friendly message
## and exit with a non-zero error code
if ( !is.null(opt$help) ) {
    cat(getopt(spec, usage=TRUE))
    q(status=1)
}

## set some reasonable defaults for the options that are needed,
## but were not specified.
if ( is.null(opt$mean ) ) { opt$mean = 0 }
if ( is.null(opt$sd ) ) { opt$sd = 1 }
if ( is.null(opt$count ) ) { opt$count = 10 }
if ( is.null(opt$verbose ) ) { opt$verbose = FALSE }

## print some progress messages to stderr, if requested.
if ( opt$verbose ) { write("writing...",stderr()) }

## do some operation based on user input.
cat(paste(rnorm(opt$count,mean=opt$mean,sd=opt$sd),collapse="\n"))
cat("\n")
## signal success and exit.
## q(status=0)
