#!/usr/bin/env Rscript

## source("common.r")

expr <- expression(function(who){cat("\thello,",who,"\n")})
cat("The expression:\n")
print(expr)
cat("as.character:\n",as.character(expr),"\n")
cat("evaluated into function:\n")
print(eval(expr))
cat("executed:\n")
eval(expr)(Sys.getenv("USER"))
