#!/usr/bin/env Rscript

source("common.r")

cat("assign(str,value)\n")
assign("a",123)
printv("a",a)
cat("(it can be used to create a series of objects, but usually better store those in a vector)\n")

cat("expression() and eval() can be used for frequent use of the same expression:\n")
x <- 3
y <- 4
printv("x",x)
printv("y",y)
expr1 <- expression(x ^ y)
print.obj("expr1",expr1)
printv("expression eval-ed",eval(expr1))
y <- 2
printv("y",y)
printv("expression eval-ed",eval(expr1))

cat("do.call can be used to give parameters to an anonymous function, which can also be created from a string:\n")
printv("x^x in a fancy way",do.call(function(x){x^x},list(3)))
f1 <- function(x){x^x}
printv("same from string",do.call("f1",list(3)))

cat("see also parse.r\n")
