#!/usr/bin/env Rscript

source("common.r")
source("error_check.r")

usage <- function() {
    rlog::log_info(paste(get.script.fn(),"<filename>"))
}

if (NARGS < 1) {
    usage()
    q()
}

res <- NULL
if (NARGS > 1) {
    res <- test(f.checker(ARGS[1],F))
} else {
    res <- test(f.checker(ARGS[1]))
}

if (is.null(res))
    res <- "NULL"
rlog::log_info(res)
