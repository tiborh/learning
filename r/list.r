#!/usr/bin/env Rscript

source("common.r")

cat("- with vectors, matrices, and arrays, there is only one 'mode', one data type can be used\n")
cat("- with data.frames, you can use different data types for the variables, but they need to be of equal length\n")
cat("+ with lists, list items can be of different lengths\n")

l1 <- list(fruit="apple",colour=c("red","yellow"),weights=c(1,2,5,10))
print.obj("list example",l1)
print(str(l1))
