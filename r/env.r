#!/usr/bin/env Rscript

## source("common.r")

cat("show current environment:\n")
print(environment())

e1 = new.env(parent = baseenv())
e2 = new.env(parent = e1)

assign("a",3,envir=e1)
assign("b",4,envir=e2)

cat("What is in e1 env?\n")
print(ls(e1))

cat("What is in e2 env?\n")
print(ls(e2))

cat("does 'a' exist in e2 (by inheritance)",exists("a",envir=e2),"\n")
cat("does 'a' exist in e2 (without inheritance)",exists("a",envir=e2,inherits=F),"\n")
cat("The value of 'a' in 'e1':",substitute(a,e1),"\n")
cat("substitution as character:",as.character(substitute(a,e1)),"\n")
