#!/usr/bin/env Rscript

source("common.r")

file.head <- c("#",
               "# Data file",
               paste("# Written by",get.script.fn()),
               "# Use read.table.r to read the data.",
               "#")

fn <- file.path(RESULTS.DIR,"sample_table.txt")
write.table(file.head,fn,quote=F,row.names=F,col.names=F)
num.of.records <- ifelse(length(ARGS)>0,as.integer(ARGS[1]),12)
words <- scan(file=file.path("","usr","share","dict","words"),what=character())
a.df <- data.frame(id=1:num.of.records,word=sample(words,num.of.records,replace=T))
print(str(a.df))
options(warn=-1) # suppress warnings
write.table(a.df,fn,row.names=F,append=T)
options(warn=0)  # turn warnings back on
cat("Data has been written to:",fn,"\n")
cat("You can use 'read.table.r' to read the data from the file.\n")
