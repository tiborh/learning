#!/usr/bin/env Rscript

## source("common.r")
cat("at script startup:\n")
print(ls())
a <- "a"
cat("after defining a variable:\n")
print(ls())
require(readxl)
cat("after requiring a library:\n")
print(ls())
rm(list=ls())
cat("after removing everything:\n")
print(ls())
