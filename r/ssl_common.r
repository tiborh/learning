#!/usr/bin/env Rscript

source("common.r")

check.packages(c("rlang","openssl"))

ssl <- new_environment()
ssl$key.dir.mode <- "700"
ssl$key.file.mode <- "600"
ssl$key.dir <- file.path(HOME.DIR,".ssh","r")
check.dir(ssl$key.dir)
Sys.chmod(ssl$key.dir,ssl$key.dir.mode)
ssl$key.fn <- file.path(ssl$key.dir,"ssl_key.rds")
ssl$pubkey.fn <- file.path(ssl$key.dir,"ssl_pubkey.rds")
ssl$key <- NULL
ssl$pubkey <- NULL

ssl$load.keys <- function() {
    if (!is.null(ssl$key))
        rlog::log_info("Reloading key.")
    if (!file.exists(ssl$key.fn)) {
        ssl$gen.keys()
        ssl$save.keys()
        return("new")
    }
    ssl$key <- readRDS(ssl$key.fn)
    if (!file.exists(ssl$pubkey.fn))
        ssl$gen.pubkey()
    ssl$pubkey <- readRDS(ssl$pubkey.fn)
    return("old")
}

ssl$save.keys <- function() {
    saveRDS(ssl$key,ssl$key.fn)
    Sys.chmod(ssl$key.fn,ssl$key.file.mode)
    ssl$save.pubkey()
}

ssl$backup.key <- function() {
    backup.key.fn <- paste0(ssl$key.fn,"_",time.stamp())
    saveRDS(ssl$key,backup.key.fn)
    Sys.chmod(backup.key.fn,ssl$key.file.mode)
    rlog::log_info(paste("Key has been backed up to",backup.key.fn))
}

ssl$gen.keys <- function() {
    key <- openssl::rsa_keygen()
    if (file.exists(ssl$key.fn)) {
        rlog::log_warn("Overwriting key file. Previously encrypted files cannot be decrypted later.")
        ssl$backup.key()
    }
    ssl$key <- rsa_keygen()
    ssl$gen.pubkey()
    ssl$save.keys()
    rlog::log_info("New keys have been generated.")
}

ssl$gen.pubkey <- function() {
    ssl$pubkey <- ssl$key$pubkey
    ssl$save.pubkey()
}

ssl$save.pubkey <- function() {
    saveRDS(ssl$pubkey,ssl$pubkey.fn)
}

ssl$encrypt <- function(obj) {
    if (is.null(ssl$pubkey)) {
        res <- ssl$load.keys()
    }
    return(openssl::encrypt_envelope(serialize(obj,NULL),ssl$pubkey))
}

ssl$decrypt <- function(crp) {
    if (is.null(ssl$pubkey)) {
        res <- ssl$load.keys()
        if (res == "new")
            stop("Sorry. Cannot decrypt with newly generated key.")
    }
    return(unserialize(openssl::decrypt_envelope(crp$data,crp$iv, crp$session, ssl$key)))
}
