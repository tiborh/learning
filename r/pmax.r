#!/usr/bin/env Rscript

source("common.r")

v1 <- sample(1:5,5)
v2 <- sample(2:6,5)

printv("v1",v1)
printv("v2",v2)

cat("max of v1,v2:",max(v1,v2),"\n")

cat("parallel maximum:\n")
printv("pmax(v1,v2)",pmax(v1,v2))
