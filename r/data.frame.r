#!/usr/bin/env Rscript

source("common.r")

make.data.frame <- function(n,m=n) {
    ## printv("n",n)
    ## printv("m",m)
    e1 <- expression(sample(letters,m))
    df <- data.frame(eval(e1))
    for (n in 2:n) {
        df <- cbind(df,eval(e1))
    }
    names(df) <- LETTERS[1:n]
    return(df)
}

df <- make.data.frame(5)
print.obj("df",df)
printv("nrow",nrow(df))
printv("ncol",ncol(df))
print(str(df))


## make.data.frame <- function(n,m=n) {
##     printv("n",n)
##     printv("m",m)
##     e1 <- expression(sample(letters,m))
##     v1 <- vector(mode="character")
##     for (n in 1:n) {
##         v1 <- append(v1,eval(e1))
##     }
##     df <- as.data.frame(matrix(v1,nrow=m,ncol=n,byrow=F))
##     names(df) <- LETTERS[1:n]
##     return(df)
## }
