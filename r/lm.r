#!/usr/bin/env Rscript

source("common.r")

cat("Linear regression example:\n")
cat("dataset:\n")
print(str(mtcars))
lmfit <- lm(mpg~wt, data=mtcars)
plotindices <- c(1,2,3,5)
print(summary(lmfit))
cat("Correlation between mpg and weight:",cor(mtcars$mpg,mtcars$wt),"\n")
for (i in plotindices) {
    fn.lm <- file.path("img",paste0(get.script.fn(),"_plot",i,".svg"))
    svg(file=fn.lm)
    plot(lmfit,which=i)
    dev.off()
    cat("Plot has been written to:",fn.lm,"\n")
}
cook <- cooks.distance(lmfit)
fn.cook <- file.path("img",paste0(get.script.fn(),"_cook_plot.svg"))
svg(file=fn.cook)
plot(cook)
dev.off()
cat("Plot has been written to:",fn.cook,"\n")
pred.wt <- 6
cat("Predict",pred.wt,":",predict(lmfit,list(wt=pred.wt)),"\n") # far from spectacular
