#!/usr/bin/env Rscript

source("common.r")

md <- if (NUARGS > 1) ARGS[2] else "any"
    

if (NUARGS > 0)
    apropos(ARGS[1],mode=md) # function can also be useful
