#!/usr/bin/env Rscript

source("common.r")

if (NUARGS > 0) {
    vignette(ARGS[1])
} else {
    vignette()
}

