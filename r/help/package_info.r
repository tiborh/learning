#!/usr/bin/env Rscript

source("common.r")

if (NUARGS > 0)
    help(package=ARGS[1])
