#!/usr/bin/env Rscript

source("common.r")

f2c <- function(f) {
    return(((f-32)*5)/9)
}
c2f <- function(c) {
    return(c * (9/5) + 32)
}

usage <- function() {
    cat("Usage:\n\t")
    cat(get.script.fn(),"<fahren|celsius>","[<F|C>]\n")
    q()
}

if (NUARGS < 1)
    usage()

temp.unit <- "F"

if (NUARGS > 1) {
    temp.unit <- toupper(ARGS[2])
}

res <- NULL

temp.val <- as.numeric(ARGS[1])

if (temp.unit == "F") {
    res <- f2c(temp.val)
} else if (temp.unit == "C") {
    res <- c2f(temp.val)
}

if (!is.null(res))
    cat(res,"\n")
