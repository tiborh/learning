#!/usr/bin/env Rscript

source("common.r")

a <- data.frame(a=sample(0:99,10,replace=T),
                b=sample(letters,10,replace=T),
                c=sample(LETTERS,10,replace=T))
print.obj("a",a)
printv("a$b",a$b)
attach(a)
cat("inside attach:\n")
print.obj("a",a)
printv("b",b)
printv("c",c)
detach(a)
