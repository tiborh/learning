#!/usr/bin/env Rscript

source("common.r")

write.script <- "write.table.r"
fn <- file.path(RESULTS.DIR,"sample_table.txt")
if (!file.exists(fn)) {
    cat("File does not exist:",fn,"\n")
    cat("Use",write.script,"to create a sample file.\n")
}
a.df <- read.table(fn,header=T,skip=5)
print(str(a.df))
