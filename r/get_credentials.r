#!/usr/bin/env Rscript

source("common.r")

check.packages(c("testit"))

ask.user <- function(prompt = "Username: ") {
    cat(prompt)
    user <- system('read ff && echo $ff && ff=""', intern=TRUE)
    cat('\n')
    return(user)
}

ask.pass <- function(prompt = "Password:"){
    cat(prompt)
    pass <- system('stty -echo && read ff && stty echo && echo $ff && ff=""',
                   intern=TRUE)
    cat('\n')
    invisible(pass)
}

get.creds <- function() {
    creds.list <- list(user=Sys.info()[["user"]],pass="")
    prompt <- paste0("username (default: ",creds.list$user,"): ")
    name.input <- ask.user(prompt)
    if (nchar(name.input) != 0)
        creds.list$user <- name.input
    creds.list$pass <- ask.pass("Enter your password: ")
    assert(nchar(creds.list$pass) > 0)
    return(creds.list)
}

res <- get.creds()
print(res)
