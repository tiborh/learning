#!/usr/bin/env Rscript

source("common.r")

a <- data.frame(a=sample(0:99,10,replace=T),
                b=sample(letters,10,replace=T),
                c=sample(LETTERS,10,replace=T))
print.obj("a",a)
printv("a$b",a$b)
with(a, {
    cat("inside 'with':\n")
    printv("a",a)
    printv("b",b)
    printv("c",c)
    savedb <<-b
})
cat("outside 'with':\n")
printv("savedb",savedb)
