#!/usr/bin/env Rscript

source("common.r")

v1 <- 1:length(letters)
printv("v1",v1)
v2 <- letters
printv("v2",v2)
v3 <- v1 %% 3 == 0
printv("v3",v3)
v4 <- jitter(v1)
printv("v4",v4)
v5 <- append(v1[1:12],v4[13:26])
printv("v5",v5)
printv("v2[v3]",v2[v3])
printv("odds",v1[v1%%2==1])
printv("evens",v1[v1%%2==0])
printv("odds",v2[v1%%2==1])
printv("evens",v2[v1%%2==0])
printv("type of v1",typeof(v1))
printv("type of v2",typeof(v2))
printv("type of v3",typeof(v3))
printv("type of v4",typeof(v4))
printv("type of v5",typeof(v5))
printv("cars$speed[1:12",cars$speed[1:12])
printv("type of cars speed",typeof(cars$speed))
