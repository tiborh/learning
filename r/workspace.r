#!/usr/bin/env Rscript

## source("common.r")

print.cwd <- function() {
    cat("Current working directory (getwd):",getwd(),"\n")
}

print.cwd()
img.sav <- file.path(getwd(),"workspace.saved")
tmp.dir <- file.path(getwd(),"tmp")
## cat("tmp path:",tmp.dir,"\n")
tmp.status <- dir.exists(tmp.dir)
if (!tmp.status) {
    dir.create(tmp.dir)
    cat("created:",tmp.dir,"\n")
}
setwd(tmp.dir)
print.cwd()
if (!tmp.status) {
    unlink(tmp.dir,recursive=T) # default F: directories are not deleted
    cat("unlinked:",tmp.dir,"\n")
}
setwd("..")
print.cwd()
cat("objects in current workspace (ls()):",ls(),"\n")
save.image(img.sav)
cat("workspace saved to:",img.sav,"\n")
rm(list=ls())
cat("objects in current workspace (after rm(list=ls())):",ls(),"\n")
## cat("command history:\n")
## print(history()) # does not work in script mode
## savehistory("filename") # .Rhistory is the default
## loadhistory("myfile") # .Rhistory is the default
