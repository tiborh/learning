#!/usr/bin/env Rscript

source("common.r")

check.packages(c("ggplot2"))

get.clean.mtcars <- function() {
    mtcars$am <- factor(mtcars$am)
    levels(mtcars$am) = c("manual","automatic")
    return(mtcars)
}

plot.mtcars <- function(data,...) {
    args <- lapply(list(...), function(x) if (!is.null(x)) sym(x))

    ggplot(data,aes(mpg,wt)) +
        geom_point(mapping = aes(!!!args))
}

fn <- file.path(IMG.DIR,"triple_exclamation.png")
png(file=fn)
plot.mtcars(get.clean.mtcars(),colour="am",size="cyl")
dev.off()
cat("plot has been written to:",fn,"\n")
