#!/usr/bin/env Rscript

source("common.r")

check.packages(c("rlog","tidyverse")) # also includes dplyr, lubridate, stringr, and ggplot2

rlog::log_info("Start here:")
rlog::log_info("https://tidyr.tidyverse.org/")

