#!/usr/bin/env Rscript

source("common.r")

usage.info <- function() {
    cat("Usage:\n\t")
    cat(get.script.fn(),"[number of numbers] [number of digits]\n")
    quit()
}

num.of.nums <- if (NUARGS > 0) as.numeric(ARGS[1]) else 20
if (is.na(num.of.nums) || num.of.nums <= 0)
    usage.info()
num.of.digits <- if (NUARGS > 1) as.numeric(ARGS[2]) else 4
if (is.na(num.of.nums) || num.of.nums < 0)
    usage.info()

options(digits=num.of.digits)
x <- runif(num.of.nums)

cat("x:",x,"\n")
cat("summary x:\n")
print(summary(x))

img.fn <- file.path(IMG.DIR,paste0(get.script.fn(),"_hist.svg"))
svg(file = img.fn)
hist(x)
dev.off()

cat("Histogram written to:",img.fn,"\n")
