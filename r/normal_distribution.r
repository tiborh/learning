#!/usr/bin/env Rscript

source("common.r")

default.num <- 100
num <- ifelse(NUARGS > 0,as.integer(ARGS[1]),default.num)
nums <- rnorm(num)
nums2 <- rnorm(num)
norm.fn <- file.path(IMG.DIR,"normal_distrib.svg")
norm2.fn <- file.path(IMG.DIR,"normal_distrib2.svg")
sorted.norm.fn <- file.path(IMG.DIR,"normal_distrib_sorted.svg")
sorted.norm2.fn <- file.path(IMG.DIR,"normal_distrib2_sorted.svg")

svg(file=norm.fn)
plot(nums,main="Normal Distribution")
dev.off()
cat("plot has been written to",norm.fn,"\n")

svg(file=sorted.norm.fn)
plot(sort(nums),main="Normal Distribution (sorted)")
dev.off()
cat("plot has been written to",sorted.norm.fn,"\n")

svg(file=norm2.fn)
plot(nums,nums2,main="Normal Distribution 2")
dev.off()
cat("plot has been written to",norm2.fn,"\n")
svg(file=sorted.norm2.fn)
plot(sort(nums),sort(nums2),main="Normal Distribution (sorted) 2")
dev.off()
cat("plot has been written to",sorted.norm2.fn,"\n")
