#!/usr/bin/env Rscript

## source("common.r")

fn <- "save.saved"
if (file.exists(fn)) {
    load(fn)
    unlink(fn)
    cat("file deleted:",fn,"\n")
} else {
    a <- data.frame(n = 0:25, l = letters, L = LETTERS)
    save(a,file=fn)
    cat("data saved to:",fn,"\n")
}
print(str(a))
rm(a)
