#!/usr/bin/env Rscript

## source("common.r")

class.mode.type <- function(lst) {
    print(str(lst))
    df <- data.frame(name=character(0),class=character(0),mode=character(0),storage.mode=character(0),typeof=character(0))
    ##cat("df:\n")
    ##print(df)
    for(name in names(lst)) {
        it <- eval(lst[[name]])
        data.line = data.frame(name=name,class=class(it),mode=mode(it),storage.mode=storage.mode(it),typeof=typeof(it))
        ##print(lst[[name]])
        ##print(data.line)
        df <- rbind(df,data.line)
    }
    return(df)
}

print(class.mode.type(list(long=quote(1L),
                           number=quote(1),
                           float=quote(1.1),
                           vect=quote(letters),
                           logical=quote(TRUE),
                           dataframe=quote(cars),
                           dataframe.row=quote(cars[1]),
                           dataframe.col=quote(cars[[1]]),
                           matrix=quote(matrix(cars)),
                           env=quote(new.env()),
                           expr=quote(expression(1+1)),
                           quote=quote(quote(y <- 1 + 1)),
                           func=quote(ls))))
