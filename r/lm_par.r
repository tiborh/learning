#!/usr/bin/env Rscript

source("common.r")

cat("Linear regression example:\n")
cat("dataset:\n")
print(str(mtcars))
lmfit <- lm(mpg~wt,data=mtcars)
print(summary(lmfit))
cat("Correlation between mpg and weight:",cor(mtcars$mpg,mtcars$wt),"\n")
fn.lm <- file.path("img",paste0(get.script.fn(),"_plot.svg"))
svg(file=fn.lm)
par(mfrow=c(2,2))
plot(lmfit,which=c(1,2,3,5))
dev.off()
cat("Plot has been written to:",fn.lm,"\n")

cook <- cooks.distance(lmfit)
fn.cook <- file.path("img",paste0(get.script.fn(),"_cook_plot.svg"))
svg(file=fn.cook)
plot(cook)
dev.off()
cat("Plot has been written to:",fn.cook,"\n")
pred.wt <- 6
cat("Predict",pred.wt,":",predict(lmfit,list(wt=pred.wt)),"\n") # far from spectacular
