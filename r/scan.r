#!/usr/bin/env Rscript

## source("common.r")

enter.record <- function() {
    cat("id:\n")
    id <- scan(file = stdin(), what = character())
    cat("name:\n")
    nm <- scan(file = "", what = character())
    cat("age (year):\n")
    ag <- scan(file = "", what = integer())
    cat("weight (kg, float):\n")
    wt <- scan(file = "", what = numeric())
    cat("feeling good? (T/F)\n")
    fg <- scan(file = "", what = logical())
    return(data.frame(id=id,name=nm,age=ag,weight=wt,feel.good=fg))
}

cat("works only in interactive mode, and even there, it is of dubious usefulness for stdin,\n better off with readLines()\n")
a <- enter.record()
print(str(a))
print(a)
