#!/usr/bin/env Rscript

## source("common.r")

require(DBI)
require(RPostgres)

con <- dbConnect(RPostgres::Postgres()) # connect to default database
dbDisconnect(con)
cat("Success.\n")
