#!/usr/bin/env Rscript

source("common.r")

usage.info <- function() {
    cat("Usage:\n\t")
    cat(get.script.fn(),"[number of numbers]  [jitter factor]\n")
    quit()
}

num.of.nums <- if (NUARGS > 0) as.numeric(ARGS[1]) else 12
if (is.na(num.of.nums) || num.of.nums <= 0)
    usage.info()
factr <- if (NUARGS > 1) as.numeric(ARGS[2]) else floor(num.of.nums / 3)
if (is.na(factr) || factr < 0)
    usage.info()

min.x <- 1
max.x <- min.x + num.of.nums - 1
min.y <- 3
max.y <- min.y + num.of.nums - 1

x <- min.x:max.x
y <- jitter(min.y:max.y,factor=factr)

cat("x:",x,"\n")
cat("y:",y,"\n")
cat("mean y:",mean(y),"\n")
cat("standard deviation of y:",sd(y),"\n")
cat("correlation between x and y:",cor(x,y),"\n")

img.fn <- file.path(IMG.DIR,paste0(get.script.fn(),"_x_y_plot.svg"))
svg(file = img.fn)
plot(x,y)
lines(predict(lm(y~x)),col='green')
lines(predict(lm(y~log(x))),col='red')[1][1]
legend(min.x,
       floor(max(y)),
       title="Trends",
       legend=c("linear", "logarithmic"),
       fill = c("green","red")
       )
dev.off()

cat("Plot written to:",img.fn,"\n")

## library(help = "datasets") # to check what is available

## weight.age.data <- read.csv("https://www.cdc.gov/growthcharts/data/zscore/wtageinf.csv")
## str(weight.age.data)
## length.age.data <- read.csv("https://www.cdc.gov/growthcharts/data/zscore/lenageinf.csv")
## str(length.age.data)
## weight.length.data <- read.csv("https://www.cdc.gov/growthcharts/data/zscore/wtleninf.csv")
## str(weight.length.data)
