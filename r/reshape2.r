#!/usr/bin/env Rscript

source("common.r")

check.packages(c("reshape2"))

## source:
## https://seananderson.ca/2013/10/19/reshape/

print.data <- function(d) {
    cat("structure:\n")
    print(str(d))

    cat("head:\n")
    print(head(d))

    cat("tail:\n")
    print(tail(d))
}

cat("original data.frame:\n")
print.data(airquality)

m.airq <- melt(airquality)
cat("melting with defaults:\n")
print.data(m.airq)

cat("Molten with month and day as ID vars:\n")
m.airq.md <- melt(airquality,id.vars=c("Month","Day"))
print.data(m.airq.md)

cat("After specifying variable and value names:\n")
m.airq.md.rn <- melt(airquality,id.vars=c("Month","Day"),value.name="climate.value",variable.name="climate.variable")
print.data(m.airq.md.rn)

cat("Restoring the original:\n")
airq <- dcast(m.airq.md, Month + Day ~ variable)
print.data(airq)

