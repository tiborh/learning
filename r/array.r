#!/usr/bin/env Rscript

source("common.r")

v1 <- as.vector(1:12,mode="integer")
a1 <- array(1:12,12)

cat("one dimensional array is like a vector:\n")
print.obj("v1",v1)
print.obj("a1",a1)
printv("type of v1",typeof(v1))
printv("type of a1",typeof(a1))
printv("is v1 vector?",is.vector(v1))
printv("is v1 array?",is.array(v1))
printv("is a1 vector?",is.vector(a1))
printv("is a1 array?",is.array(a1))
printv("is 1:12 a vector?",is.vector(1:12))

m1 <- matrix(1:12,nrow=3,ncol=4)
a2 <- array(1:12,c(3,4))

cat("\ntwo dimensional arrays are like matrices:\n")
print.obj("m1",m1)
print.obj("a2",a2)
printv("type of m1",typeof(m1))
printv("type of a2",typeof(a2))
printv("is m1 vector?",is.vector(m1))
printv("is m1 array?",is.array(m1))
printv("is a2 vector?",is.vector(a2))
printv("is a2 array?",is.array(a2))

cat("\nif there are more than two dimensions, array is the natural choice:\n")
a3 <- array(1:24,c(3,4,2))
print.obj("a3",a3)
