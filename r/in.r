#!/usr/bin/env Rscript

## source("common.r")

a <- data.frame(n=0:25,l=letters,L=LETTERS)
cat("the whole (str):\n")
print(str(a))
cat("a sample of the whole (sorted sample):\n")
print(a[sort(sample(a$n,5)),])

cat("same with %in% (a[a$n %in% sample(a$n,5),]):\n")
print(a[a$n %in% sample(a$n,5),])
