#!/usr/bin/env Rscript

source("ssl_common.r")

check.package("getopt")

spec = matrix(c(
    'help' ,  'h', 0, "logical",   "spit out this help",
    'ifn' ,   'i', 1, "character", "input filename (MANDATORY)"
), byrow=TRUE, ncol=5)
opt = getopt(spec)

if ( !is.null(opt$help) || is.null(opt$ifn) ) {
    cat(getopt(spec, usage=TRUE))
    q(status=1)
}

encr.str <- readRDS(opt$ifn)
rlog::log_debug(str(encr.str))
str <- ssl$decrypt(encr.str)
cat(str,"\n")
