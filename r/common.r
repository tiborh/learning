HOME.DIR <- Sys.getenv("HOME")
BASE.PATH <- file.path(HOME.DIR,"repos","learning","r")
source(file.path(BASE.PATH,"shiny/common.r"))
NUARGS <- length(ARGS)
IMG.DIR <- "img"
DATA.DIR <- "data"
RESULTS.DIR <- "results"

check.dir <- function(dn) {
    if (!dir.exists(dn)) {
        dir.create(dn)
        cat("created:",dn,"\n")
    }
}
check.dirs <- function(dns) {
    for(dn in dns) {
        ## cat("checking dir:",dn,"\n")
        check.dir(dn)
    }
}

check.dirs(c(IMG.DIR,DATA.DIR,RESULTS.DIR))

printv <- function(vname,v) {
    cat(vname,"==",v,"\n")
}
print.obj <- function(str,obj) {
    cat(str,"\n")
    print(obj)
}
0
