#!/usr/bin/env Rscript

source("common.r")
source("error_check.r")

if (NUARGS > 0) {
    retval <- test(parse(text=ARGS[1]))
    if (!is.null(retval))
        rlog::log_info(paste("Returned value(s):",paste(retval,collapse=", ")))
}

