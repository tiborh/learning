#!/usr/bin/env Rscript

source("error_check.r")

divide <- function(a,b) {
    return(a/b)
}

## source:
## https://www.geeksforgeeks.org/handling-errors-in-r-programming/

check(10/2)
check(divide(10,2))
check(eval(parse(text="10/2")))
check({10/0})
check({10/'noe'})
check({"a" == tolower("A")})
