#!/usr/bin/env Rscript

source("ssl_common.r")

check.package("getopt")

spec = matrix(c(
    'help' ,  'h', 0, "logical",   "spit out this help",
    'str' ,   's', 1, "character", "string to encrypt (MANDATORY)",
    'ofn' ,   'o', 1, "character", "output filename (MANDATORY)"
), byrow=TRUE, ncol=5)
opt = getopt(spec)

if ( !is.null(opt$help) || is.null(opt$str) || is.null(opt$ofn) ) {
    cat(getopt(spec, usage=TRUE))
    q(status=1)
}

encr.str <- ssl$encrypt(opt$str)
rlog::log_info(str(encr.str))
rlog::log_info(paste("File to save to:",opt$ofn))
saveRDS(encr.str,opt$ofn)
rlog::log_info("encrypted data has been saved")
