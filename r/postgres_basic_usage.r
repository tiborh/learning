#!/usr/bin/env Rscript

## source: https://github.com/r-dbi/RPostgres/blob/main/README.md

library(DBI)
cat("Connecting to the default postgres database.\n")
con <- dbConnect(RPostgres::Postgres())

cat("Listing tables:\n")
dbListTables(con)
cat("Writing mtcars into db:\n")
dbWriteTable(con, "mtcars", mtcars)
cat("Listing tables again:\n")
dbListTables(con)

cat("Listing fields:\n")
dbListFields(con, "mtcars")
cat("Reading mtcars table:\n")
dbReadTable(con, "mtcars")

cat("querying 4 cylinder cars:\n")
res <- dbSendQuery(con, "SELECT * FROM mtcars WHERE cyl = 4")
dbFetch(res)
dbClearResult(res)

cat("fetching chunks from the query result (and printing number of rows):\n")
res <- dbSendQuery(con, "SELECT * FROM mtcars WHERE cyl = 4")
while(!dbHasCompleted(res)){
  chunk <- dbFetch(res, n = 5)
  print(nrow(chunk))
}
# Clear the result
dbClearResult(res)

cat("Removing the table:\n")
dbRemoveTable(con,"mtcars")
cat("Listing tables again:\n")
dbListTables(con)

cat("Disconnecting from the database.\n")
dbDisconnect(con)
