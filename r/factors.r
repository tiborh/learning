#!/usr/bin/env Rscript

## source("common.r")
args <- commandArgs(trailingOnly=T)
num.of.patients <- ifelse(length(args)>0,as.integer(args[1]),120)
age.bracket <- 18:85
age.probs <- c(rep(3,(45-18)),rep(12,(65-45)),rep(21,(75-65)),rep(22,(86-75)))
status <- c("poor","improved","excellent")
status.probs <- c(1,2,3)
types <- c(1,2)
type.probs <- c(2,1)
type.labels <- c("type.1","type.2")

diabetes <- data.frame(patient.id=as.character(1:num.of.patients),
                       age=sample(age.bracket,num.of.patients,replace=T,prob=age.probs),
                       type=sample(types,num.of.patients,replace=T,prob=type.probs),
                       status=sample(status,num.of.patients,replace=T,prob=status.probs)
                       )
diabetes$status <- factor(diabetes$status,order=T,levels=status)
diabetes$type <- factor(diabetes$type,levels=types,labels=type.labels)

cat("Data structure:\n")
print(str(diabetes))
cat("Summary:\n")
print(summary(diabetes))
