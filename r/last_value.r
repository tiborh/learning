#!/usr/bin/env Rscript

## source("common.r")

cat("a vector without storing it in a variable:\n")
c(a="one",1,c=NA)
print(names(.Last.value))
cat("same with a list:\n")
list(a="one",1,c=NA)
print(names(.Last.value))
