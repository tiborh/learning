#!/usr/bin/env Rscript

source("common.r")

check.package("readxl")
check.package("writexl")

xlsx.fn <- file.path(RESULTS.DIR,"mtcars.xlsx")

if (file.exists(xlsx.fn)) {
    cat("file exists:",xlsx.fn,"\n")
    mtcars.read <- readxl::read_excel(xlsx.fn,range = readxl::cell_limits(c(1,1),c(NA,NA)))
    cat("file read:",xlsx.fn,"\n")
    cat("data from file:\n")
    print(str(mtcars.read))
    unlink(xlsx.fn)
    cat("file has been deleted:",xlsx.fn,"\n")
} else {
    writexl::write_xlsx(list(mtcars=mtcars),xlsx.fn)
    cat("file has been witten:",xlsx.fn,"\n")
    cat("listing",RESULTS.DIR,"\n")
    print(dir(path=RESULTS.DIR,pattern=".xlsx"))
}


