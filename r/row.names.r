#!/usr/bin/env Rscript

source("common.r")

a <- data.frame(a=sample(0:99,10),
                b=sample(letters,10,replace=T),
                c=sample(LETTERS,10,replace=T))
row.names(a) <- a$a
print.obj("a",a)
print.obj("a (sorted)",a[order(a$a),])
printv("a$a (sorted)",sort(a$a))
