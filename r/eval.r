#!/usr/bin/env Rscript

source("common.r")

if (NUARGS > 0)
    eval(parse(text=ARGS[1])) # the parse part creates an expression,
                              # and the expression gets evaluated into an R command
