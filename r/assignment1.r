#!/usr/bin/env Rscript

## source("common.r")

x <- 1
y <- 2

my.fun <- function() {
    cat("x:",x,"\n")
    cat("y:",y,"\n")
    x <- x * 2
    y <<- y * 3
    cat("x:",x,"\n")
    cat("y:",y,"\n")
}

my.fun.rev <- function() {
    cat("x:",x,"\n")
    cat("y:",y,"\n")
    x * 2 -> x
    y * 3 ->> y
    cat("x:",x,"\n")
    cat("y:",y,"\n")
}


my.fun()
cat("x:",x,"\n")
cat("y:",y,"\n")

my.fun.rev()
cat("x:",x,"\n")
cat("y:",y,"\n")
