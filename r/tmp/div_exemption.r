#!/usr/bin/env Rscript

source("../common.r")

usage <- function() {
    rlog::log_info(paste(get.script.fn(),"<dividend> <divisor>"))
}

if (NARGS < 1) {
    usage()
    q()
}

# Create a function that divides two numbers
divide_numbers <- function(x, y) {
  result <- tryCatch(
    expr = x / y,
    error = function(e) {
      # Error handling code
      message("An error occurred: ", conditionMessage(e))
      return(NA)
    },
    finally = {
      # Code to execute regardless of whether an error occurred or not
      message("Division operation completed.")
    }
  )
  
  return(result)
}

rlog::log_info(divide_numbers(as.numeric(ARGS[1]),as.numeric(ARGS[2])))
