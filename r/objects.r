#!/usr/bin/env Rscript

## source("common.r")
cat("at script startup:\n")
print(objects())
a <- "a"
cat("after defining a variable:\n")
print(objects())
require(readxl)
cat("after requiring a library:\n")
print(objects())
