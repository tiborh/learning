#!/usr/bin/env Rscript

source("common.r")

check.packages(c("rlang","txtplot"))

rlog::log_info(paste("number of args:",NARGS))

if (NARGS > 1) {
    ss <- rlang::syms(ARGS)
    txtplot(eval(ss[[1]],mtcars), eval(ss[[2]],mtcars), xlab = rlang::as_string(ss[[1]]), ylab = rlang::as_string(ss[[2]]))
} else if (NARGS > 0) {
    print(eval(rlang::sym(ARGS[1]),mtcars))
} else {
    rlog::log_info(paste("vars to select from:",paste(names(mtcars),collapse=", ")))
}
