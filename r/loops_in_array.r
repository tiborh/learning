#!/usr/bin/env Rscript

source("common.r")

check.packages(c("rlog"))

gen.vector <- function(s) {
    return(sample(seq(1,s),s))
}

find.loop <- function(v,i) {
    rlog::log_debug(paste("index:",i))
    out.inds <- c(i)
    first.num <- v[i]
    out.vect <- c(first.num)
    num <- v[first.num]
    out.inds <- c(out.inds,num)
    while(num != first.num) {
        out.vect <- c(out.vect,num)
        num <- v[num]
        out.inds <- c(out.inds,num)
    }
    return(list(indices=out.inds,numbers=out.vect))
}

find.index <- function(vect,visited) {
    not.visited <- vect[!vect %in% visited]
    if (length(not.visited) > 0)
        return(not.visited[1])
    return(0)
}

find.loops <- function(vect) {
    visited <- integer(0)
    ind <- find.index(vect,visited)
    loop.counter <- 1
    loops <- list()
    while(ind != 0) {
        res <- find.loop(vect,ind)
        loops[[loop.counter]] <- res[["numbers"]]
        loop.counter <- loop.counter + 1
        visited <- c(visited,res[["indices"]])
        rlog::log_debug("Visited:")
        rlog::log_debug(paste(visited,collapse=" "))
        ind <- find.index(vect,visited)
    }
    return(loops)
}

if (NUARGS > 1)
    set.seed(as.integer(ARGS[2]))
v <- gen.vector(ifelse(NUARGS==0,100,as.integer(ARGS[1])))

cat("Original vector:\n")
print(v)
all.loops <- find.loops(v)
cat("Loops in vector:\n")
print(all.loops)
