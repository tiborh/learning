#!/usr/bin/env Rscript

## an example from DigitalOcean.com

source("common.r")

check.packages(c("txtplot"))

txtplot(cars[,1], cars[,2], xlab = 'speed', ylab = 'distance')
