#!/usr/bin/env Rscript

source("common.r")

v <- sample(1:100,12,replace=T)
printv("v",v)
printv("sort(v)",sort(v))
cat("(gives the elements sorted)\n")
printv("rank(v)",rank(v))
cat("(gives the rank of each element)\n")
printv("order(v)",order(v))
cat("(gives the indices sorted)\n")
words <- scan(file=file.path("","usr","share","dict","words"),what=character())
df <- data.frame(words=sample(words,12),numbers=v)
print.obj("order can be used when sorting a data.frame:",df[order(df[,1]),])
print.obj("original list:",df)
