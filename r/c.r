#!/usr/bin/env Rscript

## source("common.r")

cat("vector inside vector:\n")
print(c(1,c(2,3)))
cat("list inside vector:\n")
print(c(1,list(2,3)))
cat("list inside vector (recursive=T):\n")
print(c(1,list(2,3),recursive=T))
