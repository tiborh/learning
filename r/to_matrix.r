#!/usr/bin/env Rscript

source("common.r")

gen.vector <- function(s) {
    return(sample(seq(1,s),s))
}

to.matrix <- function(v) {
    len <- length(v)
    ncol <- as.integer(sqrt(len))
    if(sqrt(len) != ncol) {
        cat("Try a square number as argument.\n")
        return(NULL)
    }
    return(matrix(v,ncol=ncol))
}

v <- gen.vector(ifelse(NUARGS==0,100,as.integer(ARGS[1])))

cat("Original vector:\n")
print(v)
res <- to.matrix(v)
if (!is.null(res)) {
    cat("As matrix:\n")
    print(res)
}
