#!/usr/bin/env Rscript

## source("common.r")

ltrs <- quote(letters)
cat("quoted:",ltrs,"\n")
cat("quote evaluated:\n")
print(eval(ltrs))
