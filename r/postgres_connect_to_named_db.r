#!/usr/bin/env Rscript

## source: https://github.com/r-dbi/RPostgres/blob/main/README.md

library(DBI)
cat("Connecting to database 'R'.\n")
con <- dbConnect(RPostgres::Postgres(),dbname = 'R', 
                 host = 'localhost',
                 port = 5432#, # or any other port specified by your DBA
                 # user = 'USERNAME',
                 # password = 'PASSWORD'
                 )

cat("Listing tables:\n")
dbListTables(con)

cat("Disconnecting from the database.\n")
dbDisconnect(con)
