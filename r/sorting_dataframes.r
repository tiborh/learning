#!/usr/bin/env Rscript

source("common.r")

top.5.of.each <- function(df,col.num) {
    out.df <- df[0,]
    factors <- levels(df[,col.num])
    for (f in factors) {
        out.df <- rbind(out.df,head(df[df[,col.num]==f,],n=5))
    }
    return(out.df)
}



## source: https://www.programmingr.com/examples/r-dataframe/sort-r-data-frame/
cat("Original data set:\n")
print(str(ChickWeight))
printv("Dimensions",dim(ChickWeight))
birds.21 <- ChickWeight[ChickWeight$Time ==21,]
printv("Filtered data dimensions",dim(birds.21))
print.obj("Using 'order()' to sort by weight:",head(birds.21[order(birds.21$weight),]))
cat("...\n")
print(tail(birds.21[order(birds.21$weight),]))
print.obj("Going for top 5:",birds.21[order(-birds.21$weight),][1:5,])
birds.21.diet.weight.sorted <- birds.21[order(birds.21$Diet, -birds.21$weight),]
birds.21.diet.weight.sorted2 <- birds.21[order(birds.21$Diet, birds.21$weight),]
##print.obj("By multiple variables:",birds.21.diet.weight.sorted)
top.fives <- top.5.of.each(birds.21.diet.weight.sorted,4)
bottom.fives <- top.5.of.each(birds.21.diet.weight.sorted2,4)
print(top.fives)
top.5.matrix <- matrix(top.fives$weight,nrow=5,ncol=4)
print(top.5.matrix)
fn <- file.path(RESULTS.DIR,"weight_for_each_diet.svg")
svg(file=fn)
plot(birds.21[,c("weight","Diet")],main="Weights for each diet (Time=21)")
dev.off()
cat("Plot (for all) has been written to:",fn,"\n")
fn <- file.path(RESULTS.DIR,"top_weights_for_each_diet.svg")
svg(file=fn)
plot(top.fives[,c("weight","Diet")],main="Top 5 weights for each diet")
dev.off()
cat("Plot (for top 5) has been written to:",fn,"\n")
fn <- file.path(RESULTS.DIR,"bottom_weights_for_each_diet.svg")
svg(file=fn)
plot(bottom.fives[,c("weight","Diet")],main="Bottom 5 weights for each diet")
dev.off()
cat("Plot (for bottom 5) has been written to:",fn,"\n")
