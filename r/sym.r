#!/usr/bin/env Rscript

source("common.r")

check.packages(c("rlang"))

if (NARGS > 0) {
    print(eval(rlang::sym(ARGS[1]),mtcars))
} else {
    rlog::log_info(paste("vars to select from:",paste(names(mtcars),collapse=", ")))
}
