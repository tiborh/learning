#!/usr/bin/env Rscript

## source("common.r")

f <- function(...) {
    dots <- list(...)
    cat("length of input:",length(dots),"\n")
    print(dots)
}

f1 <- function(long.param.name=NULL,...) {
    if (!is.null(long.param.name))
        cat("long param value:",long.param.name,"\n")
    f(...)
}

f2 <- function(...,long.param.name=NULL) {
    if (!is.null(long.param.name))
        cat("long param value:",long.param.name,"\n")
    f(...)
}

cat("empty:\n")
f()

cat("int and string:\n")
f(1,"one")

cat("int and list:\n")
f(1,list(1,2,3))

cat("int and vector:\n")
f(1,c("one","two","three"))

cat("abbreviation:\n")
f1(long="long param value",list(1:5))

cat("no abbreviation after the ...\n")
f2(long="long param value",list(1:5))
