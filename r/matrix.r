#!/usr/bin/env Rscript

source("common.r")

cat("matrix is folding a vector into rows and columns\n")

v1 <- 1:12
printv("v1",v1)
m1 <- matrix(v1,nrow=3,ncol=4,byrow=T)
m2 <- matrix(v1,nrow=3,ncol=4,byrow=F)
print.obj("m1 is by row:",m1)
print.obj("m2 is by col:",m2)
print.obj("the two added:",m1+m2)
print.obj("the two multiplied:",m1*m2)
print.obj("multiplication on the fly",do.call(function(v){v*v},list((matrix(1:16,nrow=4,ncol=4,byrow=F)))))
