#!/usr/bin/env Rscript

source("common.r")

usage <- function() {
    cat("Usage:\n\t")
    cat(ARGV[1],"<RData filename>\n")
    q(status=1)
}

if (length(ARGS) < 1)
    usage()

fn <- ARGS[1]
f.checker(fn)

var.name <- load(fn)
rlog::log_info(paste("Original variable name:",var.name))

assign("var",get(var.name))
rlog::log_info("Structure of loaded data:")
rlog::log_info(str(var))
