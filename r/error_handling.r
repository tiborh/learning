#!/usr/bin/env Rscript

source("common.r")

usage <- function() {
    cat("Usage:\n\t")
    cat(get.script.fn(),"<dividend>","<divisor>\n")
    q()
}

num.valid <- function(inp) {
    return(ifelse(is.numeric(inp),inp,NA))
}

divide <- function(a,b) {
    res <- NULL
    tryCatch (
        expr = { res <- a / b
            rlog::log_info("so far so good (expr)")
        },
        error = function(e) {
            rlog::log_error("Something went wrong. (error)")
            rlog::log_error(e)
        },
        warning = function(w) {
            rlog::log_warn("Something is not fully good. (warning)")
            rlog::log_warn(w)
        },
        finally = {
            rlog::log_info("Finally has been reached.")
            rlog::log_debug(paste("input:",num.valid(a),num.valid(b),"result:",ifelse(!is.null(res),res,"NULL")))
        }
    )
    return(res)
}

if (NUARGS < 2)
    usage()

res <- divide(as.numeric(ARGS[1]),as.numeric(ARGS[2]))
if (!is.null(res) && !is.nan(res)) {
    rlog::log_info(paste("Result:",res))
} else {
    rlog::log_warn("NULL was returned.")
}

## example for warning: a 2
