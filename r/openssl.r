#!/usr/bin/env Rscript

source("common.r")

check.package("openssl")
                                        # Requires RSA key
key <- rsa_keygen()
rlog::log_info("(private) Key:")
rlog::log_info(str(key))

pubkey <- key$pubkey
rlog::log_info("Public key:")
rlog::log_info(str(pubkey))

msg <- serialize(iris, NULL)
rlog::log_info("Message:")
rlog::log_info(str(msg))
                                        # Encrypt
out <- encrypt_envelope(msg, pubkey)
rlog::log_inf0(str(out))
                                        # Decrypt
orig <- decrypt_envelope(out$data, out$iv, out$session, key)
stopifnot(identical(msg, orig))
