#!/usr/bin/env Rscript

source("common.r")

DEF_NUM_OF_WORDS <- 12
WORD_FILE_PATH <- file.path("/","usr","share","dict","words")
resp <- f.checker(WORD_FILE_PATH) # resp: catches returned value but is not used

num_of_words <- DEF_NUM_OF_WORDS

if (NUARGS < 1) {
    rlog::log_info(paste(ARGV[1],"<nu of words (default:",num_of_words,")>"))
} else {
    num_of_words <- ARGS[1]
}

dict.words <- read.csv(WORD_FILE_PATH)
common.words <- dict.words$A[!grepl("[A-Z]+[a-z]*",dict.words$A)]
common.words <- common.words[!grepl("[a-z]+'s",common.words)]
print(paste(sample(common.words,num_of_words),collapse=" "))
