#!/usr/bin/env Rscript

source("common.r")

ts <- function() {
    timestamp();
    q();
}

pa <- function() {
    print(ARGS)
    ts()
}


## cat("NUARGS:",NUARGS,"\n")
sink("sink.dump",append=T,split=T)
ifelse(NUARGS == 0, ts(), pa())
