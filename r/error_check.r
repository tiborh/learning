## test seems to be working better
test <- function(expr) {
    res <- NULL
    tryCatch (
        expr = { res <- eval(expr)
            rlog::log_info("(looks good)")
            rlog::log_debug(res)
        },
        error = function(e) {
            rlog::log_error("Something went wrong.")
            rlog::log_error(e)
        },
        warning = function(w) {
            rlog::log_warn("Something is not fully good.")
            rlog::log_warn(w)
        },
        finally = {
            rlog::log_info("Finally has been reached.")
        }
    )
    return(res)
}

check <- function(expression){
    withCallingHandlers(expression,
                        warning = function(w){
                            message("warning:\n", w)
                        },
                        error = function(e){
                            message("error:\n", e)
                        },
                        finally = {
                            message("Completed")
                        })
}
