#!/usr/bin/env Rscript

source("common.r")

default.num <- 100
num <- ifelse(NUARGS > 0,as.integer(ARGS[1]),default.num)
mean.val <- ifelse(NUARGS > 1,as.numeric(ARGS[2]),num/2)
stand.dev <- ifelse(NUARGS > 2,as.numeric(ARGS[3]),15)
nums <- 1:num
y <- dnorm(nums, mean = mean.val, sd=stand.dev)
cat("number of data points:",num,"\n")
cat("mean value:",mean.val,"\n")
cat("standard deviation:",stand.dev,"\n")
plot.fn <- file.path(IMG.DIR,"bell_curve.svg")
svg(file=plot.fn)
plot(nums,y)
dev.off()
cat("Plot has been written to:",plot.fn,"\n")
