#!/usr/bin/env Rscript

source("common.r")

check.packages(c("getopt"))

MIN.LEN <- 1
MAX.LEN <- 12
DEF.NUM <- 5

gen.str <- function(max,min) {
    return(paste(sample(letters,sample(min:max,1),replace=T),collapse=""))
}

gen.strings <- function(num,max,min) {
    out.vect <- character(0)
    for (n in 1:num) {
        out.vect <- c(out.vect,gen.str(max,min))
    }
    return(out.vect)
}

spec = matrix(c(
    'help'     , 'h', 0, "logical", "spit out this help",
    'collapse' , 'c', 0, "logical", "collapse to text",
    'maxlen'   , 'a', 1, "integer", "maximum length of a generated string",
    'minlen'   , 'i', 1, "integer", "minimum length of a generated string",
    'number'   , 'n', 1, "integer", "number of strings to be generated",
    "seed"     , 's', 1, "integer", "set seed (an integer)"
), byrow=TRUE, ncol=5)
opt = getopt(spec)

if (!is.null(opt$seed))
    set.seed(opt$seed)

if (!is.null(opt$help) ) {
    cat(getopt(spec, usage=TRUE))
    q(status=1)
}

num <- ifelse(is.null(opt$number),DEF.NUM,opt$number)
max <- ifelse(is.null(opt$maxlen),MAX.LEN,opt$maxlen)
min <- ifelse(is.null(opt$minlen),MIN.LEN,opt$minlen)

res <- gen.strings(num,max,min)

if (!is.null(opt$collapse)) {
    txt <- paste(res,collapse=" ")
    cat(txt,"\n")
} else {
    print(res)
}

