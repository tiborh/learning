#!/usr/bin/env Rscript

## source("common.r")

n.r <- 5
n.c <- 3
n.e <- n.r * n.c

my.mat <- matrix(rnorm(n.e),nrow=n.r,ncol=n.c)

cat("The original:\n")
print(my.mat)

cat("row means:\n")
print(apply(my.mat,2,mean))

na.ind <- sample(1:n.e,1)

my.mat[na.ind] <- NA

cat("with NA:\n")
print(my.mat)

cat("row means:\n")
print(apply(my.mat,2,mean))

cat("row means when ignoring NA vals:\n")
print(apply(my.mat,2,mean,na.rm=T))

cat("colMeans also exists:\n")
print(colMeans(my.mat,na.rm=T))
