source("../common.r")
load(file.path(Sys.getenv("HOME"),"Dropbox","documents","r","movies.RData"))
n.total <- nrow(movies)
## original download link is from:
## https://rstudio-education.github.io/shiny-course/
check.packages(c("ggplot2","dplyr","DT"))
y.opts <- c("imdb_rating", "imdb_num_votes", "critics_score", "audience_score", "runtime")
y.names <- c("IMDB rating","Number of IMDB votes","Critics score","Audience score","Runtime")
names(y.opts) <- y.names
x.opts <- y.opts
x.names <- names(y.opts)

ui <- fluidPage(
  br(),

  sidebarLayout(
    sidebarPanel(
      selectInput(
        inputId = "y", label = "Y-axis:",
        choices = y.opts,
        selected = "audience_score"
      ),

      selectInput(
        inputId = "x", label = "X-axis:",
        choices = x.opts,
        selected = "critics_score"
      )
    ),

    mainPanel(
      plotOutput(outputId = "scatterplot", hover = "plot_hover"),
      DT::dataTableOutput(outputId = "moviestable"),
      verbatimTextOutput(outputId = "debugPrint")
    )
  )
)

# Define server ----------------------------------------------------------------

server <- function(input, output, session) {

    output$debugPrint <- renderPrint({
        coll <- list()
        for (a.name in names(input)) {
            coll[[a.name]] <- input[[a.name]]
        }
        coll
    })
    
    output$scatterplot <- renderPlot({
        ggplot(data = movies, aes_string(x = input$x, y = input$y)) +
        ## ggplot(data = movies, aes(x = .data[[input$x]], y = .data[[input$y]])) + # Warning: Error in nearPoints: nearPoints: `xvar` ('.data[["critics_score"]]')  not in names of input
            geom_point()
    })

    output$moviestable <- renderDataTable({
        nearPoints(movies, input$plot_hover) %>% # Warning: Error in : Can't subset `.data` outside of a data mask context.
            select(title, audience_score, critics_score)
    })
}

# Create the Shiny app object --------------------------------------------------

shinyApp(ui = ui, server = server)
