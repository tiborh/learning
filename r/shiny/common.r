dir.maker <- function(dirpath) {
    if (!dir.exists(dirpath)) {
        dir.create(dirpath,recursive=T)
        cat("Directory has been created:",dirpath,"\n")
    }
    return(dirpath)
}

DEFAULT.PACKAGE.LOCATION <- "https://cloud.r-project.org"
DEFAULT.USER <- Sys.info()[["user"]]
LIBDIR <- Sys.getenv("R_LIBS_USER")

check.package <- function(pkg.name) {
    libdir <- dir.maker(LIBDIR)
    if (!require(pkg.name, character.only = T)) {
        install.packages(pkg.name,lib=libdir,repos="https://cloud.r-project.org")
        require(pkg.name, character.only = T)
        return(1)
    }
    return(0)
}

check.package <- function(pkg.name,ndebug=T) {
    if (!require(pkg.name,quietly=ndebug,character.only=T)) {
        install.packages(pkg.name,repos=DEFAULT.PACKAGE.LOCATION)
        if (!require(pkg.name,quietly=ndebug,character.only=T)) {
            stop("package not found: ",pkg.name)
        }
    }
}

check.packages <- function(pkg.names,ndebug=T) {
    for (it in pkg.names) {
        check.package(it,ndebug)
    }
}

check.package("rlog")

f.checker <- function(fn,stop.on.false=T) {
    if (!file.exists(fn)) {
        msg.txt <- paste("File does not exist:",fn)
        if (stop.on.false) {
            rlog::log_fatal(msg.txt)
            stop("Make sure the filename is correct.")
        } else {
            rlog::log_warn(msg.txt)
        }
        return(F)
    }
    return(T)
}

get.script.fn <- function() {
    args <- commandArgs()
    rlog::log_debug(str(args))
    if (2 < length(args))  {
        file.split <- strsplit(args[grepl("^--file=",args)],"=")
        if(length(file.split) > 0)
            return(file.split[[1]][2])
    }
    return(NULL)
}

time.stamp <- function() {
    ts <- gsub(" ","_",Sys.time())
    ts <- gsub("[-:]","",ts)
    return(ts)
}

ARGS <- commandArgs(trailingOnly=T)
ARGV <- c(get.script.fn(),ARGS)
NARGS <- length(ARGS)

## obsoleted stuff that can later be deleted

## f.checker <- function(fn) {
##     if (!file.exists(fn)) {
##         rlog::log_fatal(paste("File does not exist:",fn))
##         stop("Make sure the filename is correct.")
##     }
## }
