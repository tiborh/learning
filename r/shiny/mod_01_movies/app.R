source("../common.r")
load(file.path(Sys.getenv("HOME"),"Dropbox","documents","r","movies.RData"))
## original download link is from:
## https://rstudio-education.github.io/shiny-course/
check.packages(c("ggplot2"))
y.opts <- c("imdb_rating", "imdb_num_votes", "critics_score", "audience_score", "runtime")
y.names <- c("IMDB rating","Number of IMDB votes","Critics score","Audience score","Runtime")
names(y.opts) <- y.names
x.opts <- y.opts
x.names <- names(y.opts)
col.opts <- c("title_type", "genre", "mpaa_rating", "critics_rating", "audience_rating")
col.names <- c("Title type","Genre","MPAA rating","Critics rating","Audience rating")
names(col.opts) <- col.names

size.opts <- c("runtime","imdb_rating","imdb_num_votes","critics_score","audience_score")
size.names <- c("Runtime","IMDB Rating","IMDB Vote Num","Critics score","Audience score")
names(size.opts) <- size.names

## Define UI ----
ui <- fluidPage(
    titlePanel("Movies Data"),

    sidebarLayout(
        sidebarPanel(
            h2("Axis Data"),
            selectInput(
                inputId = "y",
                label = "Y-axis:",
                choices = y.opts,
                selected = y.opts[4]
            ),
      ## Select variable for x-axis
            selectInput(
                inputId = "x",
                label = "X-axis:",
                choices = x.opts,
                selected = x.opts[3]
            ),
            selectInput(
                inputId = "c",
                label = "Colour by:",
                choices = col.opts,
                selected = col.opts[3]
            ),
            selectInput(
                inputId = "s",
                label = "Size by:",
                choices = size.opts,
                selected = size.opts[3]
            ),
            sliderInput(inputId = "a",
                        label = "Opacity",
                        min = 0, max = 1, step = 0.1, value = 0.5),
            sliderInput(inputId = "w",
                        label = "Plot width (px)",
                        min = 500, max = 1900, step = 100, value = 1000),
            sliderInput(inputId = "h",
                        label = "Plot height (px)",
                        min = 300, max = 1200, step = 100, value = 700)
        ),
        mainPanel(
            h1(textOutput("main.panel.title")),
            plotOutput(outputId = "scatterplot")
	)	
    )
)

## Define server logic ----
server <- function(input, output, session) {
    output$scatterplot <- renderPlot({
        ggplot(data = movies, aes(x = .data[[input$x]], y = .data[[input$y]])) +
            geom_point(aes(alpha = input$a, colour = .data[[input$c]], size = .data[[input$s]])) +
            guides(alpha="none")
    },reactive(width <- input$w),reactive(height <- input$h))
    output$main.panel.title <- renderText(paste(x.names[grep(input$x,x.opts)],"vs",y.names[grep(input$y,y.opts)]))
}

## Run the app ----
shinyApp(ui = ui, server = server)

## References:
## Tutorial: https://shiny.rstudio.com/tutorial/
## How to use HTML tags: https://shiny.rstudio.com/articles/html-tags.html
## HTML tags: https://shiny.rstudio.com/articles/tag-glossary.html
## Chart Colours: https://r-charts.com/colors/
