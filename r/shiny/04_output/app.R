library(shiny)

## Source:
## https://shiny.rstudio.com/tutorial/written-tutorial/lesson4/
##
## Functions that can be used in main panel:
## 
## Output function 	Creates
## ===============      =======
## dataTableOutput 	DataTable
## htmlOutput 		raw HTML
## imageOutput 		image
## plotOutput 		plot
## tableOutput 		table
## textOutput 		text
## uiOutput 		raw HTML
## verbatimTextOutput 	text
##
##
## Functions that can be used in server to pass to Main Panel output:
## 
## render function 	creates
## ===============      =======
## renderDataTable 	DataTable
## renderImage 		images (saved as a link to a source file)
## renderPlot 		plots
## renderPrint 		any printed output
## renderTable 		data frame, matrix, other table like structures
## renderText 		character strings
## renderUI 		a Shiny tag object or HTML

pt <- "censusVis"

make.colours <- function() {
    colours <- colours()
    out.list <- list()
    for (a.colour in colours)
        out.list[[a.colour]] <- a.colour
    return(out.list)
}

## Define UI ----
ui <- fluidPage(
    titlePanel(h1(pt),windowTitle=pt),
    sidebarLayout(
        sidebarPanel(
            helpText("Create demographic maps with information from the 2010 US Census."),
            selectInput("select", "Choose a variable to display", 
                        choices = list("Percent Asian" = "pas",
                                       "Percent Black" = "pbl",
                                       "Percent Hispanic" = "phi",
                                       "Percent White" = "pwh"),
                        selected = "pwh"),
            sliderInput("range.of.interest","Range of Interest",
                        min = 0, max = 100, value = c(0, 100)),
            selectInput("select.colour", label = h3("Select Colour"), 
                        choices = make.colours(), 
                        selected = "darkgreen"),
            numericInput("plot.width", label = h3("Plot Width"), value = 640),
            numericInput("plot.height", label = h3("Plot Height"), value = 480),
            width=2),
        mainPanel(width=10,
                  h1("Main Panel"),
                  htmlOutput("selected_var")
          )
    )  
)

## Further info:
## http://shiny.rstudio.com/gallery/widget-gallery.html

values.to.df <- function(input) {
    out.df <- data.frame(name=character(0),value=character(0))
    for (a.name in names(input)) {
        a.row <- data.frame(name=a.name,value=input[[a.name]])
        out.df <- rbind(out.df,a.row)
    }
    return(out.df)
}

# Define server logic ----
server <- function(input, output) {
    output$selected_var <- renderTable({
        values.to.df(input)
    })
}

# Run the app ----
shinyApp(ui = ui, server = server)
