#!/usr/bin/env Rscript

source("common.r")

check.packages(c("shiny"))

print(names(shiny::tags))
