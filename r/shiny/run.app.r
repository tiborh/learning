#!/usr/bin/env Rscript

source("common.r")

check.packages(c("shiny","getopt","rlog"))

DM <- "normal"

spec = matrix(c(
    'help'    , 'h', 0, "logical", "spit out this help",
    'app'     , 'a', 1, "character", "application name (or dir path) (mandatory)",
    'display' , 'd', 1, "character", paste0("display mode (default: ",DM,", the other option is 'showcase') (optional)"),
    'port'    , 'p', 1, "integer", "port number where the app should listen (optional)",
    'test'    , 't', 0, "logical", "dry run"
), byrow=TRUE, ncol=5)
opt = getopt(spec)

## if help was asked for print a friendly message
## and exit with a non-zero error code
if ( !is.null(opt$help) || is.null(opt$app)) {
    cat(getopt(spec, usage=TRUE))
    q(status=1)
}

command.collect <- "runApp(launch.browser=F"
if (!is.null(opt$app))
    command.collect <- paste0(command.collect,",appDir=\"",opt$app,"\"")
if (!is.null(opt$display))
    command.collect <- paste0(command.collect,",display=\"",opt$display,"\"")
if (!is.null(opt$port))
    command.collect <- paste0(command.collect,",port=",opt$port)
command.collect <- paste0(command.collect,")")

if (is.null(opt$test)) {
    eval(parse(text=command.collect))
} else {
    rlog::log_info(paste("Command line:",command.collect))
}
