#!/usr/bin/env Rscript

source("../common.r")

check.packages(c("shiny"))

runExample("01_hello", launch.browser=F)
