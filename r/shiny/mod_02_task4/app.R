source("../common.r")
load(file.path(Sys.getenv("HOME"),"Dropbox","documents","r","movies.RData"))
n.total <- nrow(movies)
movies$score_ratio <- movies$audience_score / movies$critics_score
## original download link is from:
## https://rstudio-education.github.io/shiny-course/
check.packages(c("ggplot2","dplyr"))
y.opts <- c("imdb_rating", "imdb_num_votes", "critics_score", "audience_score", "runtime")
y.names <- c("IMDB rating","Number of IMDB votes","Critics score","Audience score","Runtime")
names(y.opts) <- y.names
x.opts <- y.opts
x.names <- names(y.opts)
col.opts <- c("title_type", "genre", "mpaa_rating", "critics_rating", "audience_rating")
col.names <- c("Title type","Genre","MPAA rating","Critics rating","Audience rating")
names(col.opts) <- col.names

## Define UI ----
ui <- fluidPage(
    titlePanel("Movies Data"),
    sidebarLayout(
        sidebarPanel(
            h2("Axis Data"),
            selectInput(
                inputId = "y",
                label = "Y-axis:",
                choices = y.opts,
                selected = y.opts[4]
            ),
      ## Select variable for x-axis
            selectInput(
                inputId = "x",
                label = "X-axis:",
                choices = x.opts,
                selected = x.opts[3]
            ),
            checkboxGroupInput(inputId = "sel.ttype", 
                   label = "Select title type:", 
                   choices = levels(movies$title_type),
                   selected = levels(movies$title_type)),
            p("To be completed...")
        ),
        mainPanel(
            h1(textOutput("main.panel.title")),
            p(paste0("The dataset has ", n.total, "observations.")),
            textOutput(outputId = "sel.num"),
            plotOutput(outputId = "scatterplot"),
            tableOutput(outputId = "sumtab")
	)	
    )
)

## Define server logic ----
server <- function(input, output, session) {
    output$scatterplot <- renderPlot({
        mov.sel <- movies[movies$title_type %in% input$sel.ttype,]
        output$sel.num <- renderText(paste("Selected:",nrow(mov.sel)))
        output$sumtab <- renderTable(mov.sel %>% group_by(mpaa_rating) %>% summarise(mean_score_ratio=mean(score_ratio), SD = sd(score_ratio), n = n()),
                                     striped = TRUE, spacing = "l", align = "lccr", digits = 4, width = "90%",
                                     caption = "Score ratio (audience / critics' scores) summary statistics by MPAA rating.")
        ggplot(data = mov.sel, aes(x = .data[[input$x]], y = .data[[input$y]])) +
            geom_point(aes(colour = title_type))
    })
    output$main.panel.title <- renderText(paste(x.names[grep(input$x,x.opts)],"vs",y.names[grep(input$y,y.opts)]))
}

## Run the app ----
shinyApp(ui = ui, server = server)

## References:
## Tutorial: https://shiny.rstudio.com/tutorial/
## How to use HTML tags: https://shiny.rstudio.com/articles/html-tags.html
## HTML tags: https://shiny.rstudio.com/articles/tag-glossary.html
## Chart Colours: https://r-charts.com/colors/
