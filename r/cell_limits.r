#!/usr/bin/env Rscript

require(rlog)
require(readxl)

llevel <- Sys.getenv("LOG_LEVEL")
if (llevel == "")
    llevel = "INFO"
rlog::log_info(paste("Log level:",llevel))

cl <- readxl::cell_limits(c(8,1),c(NA,NA))
rlog::log_info(cl)
rlog::log_info(str(cl))
