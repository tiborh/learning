#!/usr/bin/env Rscript

source("common.r")

attach(mtcars)

linear <- lm(mpg~wt)
print(linear)
fn <- file.path(RESULTS.DIR,"abline.svg")

svg(file=fn)
## plot(mpg~wt,main="mtcars",sub="mpg vs weight") # title hits main
plot(mpg~wt,sub="Subtitle: mpg vs weight")
abline(linear)
title("Title: Regression of MPG on Weight")

detach(mtcars)

dev.off()

cat("Plot has been written to:",fn,"\n")
