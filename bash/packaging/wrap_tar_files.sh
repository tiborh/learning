#!/usr/bin/env bash

readonly CYAN='\033[0;36m'
readonly RED='\033[0;31m'
readonly LCYAN='\033[1;36m'
readonly NC='\033[0m' # No Color
readonly OUTDIR="uploadables"

if [ -d ${OUTDIR} ]; then
    printf "${LCYAN}'${OUTDIR}' exists${NC}\n"
else
    mkdir ${OUTDIR}
    printf "${CYAN}'${OUTDIR}' has been created${NC}\n"
fi

for f in *.tar.gz
do
    basefn=$(echo "$f" | sed 's/.tar.gz//')
    fns="${basefn}*"
    ofn="${OUTDIR}/19010-${basefn}_Ux_.tar.gz"
    printf "${CYAN}$f\n\t--> $ofn\n\t--> "
    if [ -e ${ofn} ]; then
	printf "${LCYAN}File already exists (skipped)${NC}\n"
    elif tar -czf ${ofn} ${fns} 2> out.log; then
	printf "Created${NC}\n"
    else
	printf "${RED}NOK (see out.log)${NC}\n"
    fi
done
