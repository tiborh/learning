#!/usr/bin/env bash

readonly CYAN='\033[0;36m'
readonly LCYAN='\033[1;36m'
readonly NC='\033[0m' # No Color

for f in *.tar.gz
do
    mfn=${f}.md5
    printf "${CYAN}$f$\n\t--> "
    if [ -e ${mfn} ]; then
	printf "${LCYAN}md5 file alredy exists${NC}\n"
    else
	md5sum "$f" > ${mfn}
	printf "${mfn}${NC}\n"
    fi
done
