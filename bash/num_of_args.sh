#!/usr/bin/env bash

echo "number of command line args: " $#

usage () {
    echo "Usage:"
    echo -e '\t' `basename "$0"` "<some args>"
    echo ""
}

if [ $# -lt 1 ]
then
    usage
    exit 1
fi

if [ $# -gt 0 ]
then
    echo "args as string: " $*
    echo "args as array: " $@
    echo "script name is \$0: " `basename "$0"`
    echo "first arg is \$1:" $1
fi

echo "iterating through the args"
for i in $@
do
    echo -e '\t' $i
done
