#!/usr/bin/env bash

usage() {
    printf "Usage:\n\t`basename ${0}` <file_list_fn>\n"
}

# https://gist.github.com/mohanpedala/1e2ff5661761d3abd0385e8223e16425
set -euo pipefail

if (("${#}" < "1")); then # or sth like if [ ${#} -lt 1 ]; then
    usage
    exit 1
else
    fn="${1}"
fi

if [ -e "${fn}" ]; then
    echo "Username: "
    read user
    echo "Password: "
    stty_orig=$(stty -g) # save original terminal setting.
    stty -echo           # turn-off echoing.
    IFS= read -r passwd  # read the password
    stty "${stty_orig}"    # restore terminal setting.
    while read -r line; do
	name="${line}"
	echo "Read from file: ${name}"
	curl --user "${user}:${passwd}" "${name}" --output `basename ${name}`
    done < "${fn}"
else
    echo "File does not exist: ${fn}"
fi
