install:

1. explore:
   pacman -Ss docker
2. install what seems useful:
   container-diff
   docker
   docker-buildx
   docker-compose
   docker-machine
   docker-scan
   drone-runner-docker
3. check if group got created:
   grep docker /etc/group
   (if not create)
4. check install:
   docker version
5. add your user to group:
   sudo usermod -aG docker <username>
6. log out and log in
7. check if you are a member:
   groups
8. start docker server:
   check:
   systemctl status docker
   start:
   systemctl start docker
   (if you want it to be available all the time: "enable")
9. check docker:
   docker info

first steps:
   1. docker run hello-world
      (read output for tips)
   2. docker image list
      (it will list the image above)
      a. alternatively:
      	 docker images
      b. or to list ids only:
      	 docker images -q
   3. docker run -it alpine
      (the most minimal distro available)
      (apk update to be able to obtain packages)
   4. docker run -it debian bash
      (interactive run)
      (apt update is recommended before looking for missing packages)
      (all changes are lost after exiting the container)
   5. docker run -p 8080:8080 -p 50000:50000 --restart=on-failure jenkins/jenkins:lts-jdk11
      (for further help: https://github.com/jenkinsci/docker/blob/master/README.md)
   6. docker ps
      (check what is running)
      a. docker ps -a
         (all containers get listed: with status)
   7. docker container stop <container id>
      (stops a running container)
   8. docker rmi ImageID
      (remove image by id)
      if it says:
      Error response from daemon: conflict: unable to delete <id> (must be forced) - image is being used by stopped container <id>
      try:
      docker rmi -f ImageID
   9. docker inspect alpine
      gives info on the image
  10. docker history <image>
      all commands run against the image
  11. docker top <container id>
      "top" for the running container
  12. docker stop <container id>
      (stops the running image)
  13. docker stats <container id>
      realtime resource monitoring
  14. docker attach <container id>
      to see from a second console what is going on inside
  15. docker pause <container id>
      all processes are paused
  16. docker unpause <container id>
      all processes are resumed
  17. docker kill <container id>
      all processes are killed (usually means exiting image)
      
Frequent use cases:
I. run -> exit (leaver running) -> re-enter (running container)
   1. docker run -it <image>
   2. Ctrl+P+Q
   3. docker attach <container id>
II. execute command in above
    0. prerequisite: docker run --rm -v /usr/local/bin:/target jpetazzo/nsenter
    1. I. 1.
    2. I. 2.
    3. docker ps
    4. docker-enter <container id> <command>
III. tag and push
    0. prerequisite: dockerhub account
    1. docker tag <image id> <username/reponame:version>
    2. docker login
    3. docker push <username/reponame:version>
    note: a tag can be removed as:
    	  docker rmi <tagname:version>

references:
   1. https://www.tutorialspoint.com/docker/docker_hub.htm
   2. https://hub.docker.com/
   3. https://www.r-bloggers.com/2019/02/running-your-r-script-in-docker/
   
