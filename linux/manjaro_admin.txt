image write:
------------

sudo dd if=manjaro.iso of=/dev/XdA bs=1M oflag=sync status=progress

user  admin:
------------

add user to group:
sudo gpasswd -a user group

package manager
===============

if error: error: failed to synchronize all databases (unable to lock database)
try deleting:  /var/lib/pacman/db.lck

update mirror list:
-------------------

pacman-mirrors -f

update/upgrade:
---------------

if pacman has problems accessing repo mirrors:
sudo pacman-mirrors -f
(reconfigures list by access time)

1. update:
	sudo pacman -Syu
	* if not enough space due to /var full (/var/cache/pacman/pkg/ more than 10GB)
	  sudo pacman -Sc
	  for more information:
	  https://gist.github.com/rumansaleem/083187292632f5a7cbb4beee82fa5031 (rumansaleem/clean-up-arch-linux.md)
2. if there is a problem with upgrade:
	sudo pacman -Syyuu
3. if local files conflict:
	sudo pacman -Syyuu --overwrite <glob>
	(see man pages for more info)
4. conflicting packages:
	one may need to be deleted before update (usually some search & read
	on the web may come useful)
5. info about installed package:
	pacman -Si <package-name>
	pacman -Qi <package-name>
6. list files in an installed package:
	pacman -Ql <package-name>
7. check which package owns a file:
	pacman -Qo <filename>

further info:
https://linuxhint.com/manjaro-package-manager-pacman/

search:
-------

1. search:
	pacman -Ss <something>
2. search for open ports:
	ss -tulnp
	(then, /etc/services can help identify port number)
	-t tcp
	-u udp
	-l listening
	-n do not resolve service names (good to have port numbers)
	-p show process using socket 

install:
--------

* install
	sudo pacman -S <packagename>
* install offline package:
	sudo pacman -U <package filename>

remove:
------

1. remove recursively (to get rid of orphan packages as well):
	sudo pacman -Rs
	(removes packages that are not needed by other packages and are not
	installed by user)


system info:
-----------

 1. uname -a (terse, useful, it has other options to show specific info, e.g.
 	--m for hw architecture)
 2. lsb_release -a (essential OS info)
 3. neofetch (needs installation, nice & colourful) (archived project)
    or forked alternative:
    hyfetch (if you launch it with neowofetch, same OS determined outlook as neofetch)
    or even better (more detailed):
    fastfetch
 4. hwinfo (very detailed, usu. needs options to focus, e.g. --short or --cpu)
 5. lshw (needs to be run as root for full info, -html output is an option)
 6. lscpu (also lists known vulnerabilities)
 7. lsblk (block devices)
 8. lsusb (usb devices and hubs, -v for verbose info)
 9. lspci
10. lsscsi (SATA)
11. hdparm (extra info on a /dev/<something>)
12. disk read speed: sudo hdparm -Tt /dev/sda
13. glances: an enhanced form of htop
14. btop: another enhanced top

Arch Linux packagebuild
-----------------------

1. What do you see?
	PKGBUILD (file in the folder)
2. What to do with it?
	makepkg
3. What will you see after successful build?
	*.pkg.tar.zst
4. What to do with that?
	See Install -> install offline package

Cleanup
-------

after years of use, some bloat may accummulate: some commands to help:

sudo pacman -Sc
sudo pacman -Qdt
sudo pacman -Rns $(pacman -Qtdq)
sudo journalctl --vacuum-size=50M
