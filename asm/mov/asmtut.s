@ one-line comment in assembly
/* Multi=line comments are 
   done c-style */
.text @ where the instructions are

.global _start @ a label globally available in the program

_start: @ the starting point in the program

	@ moving numbers to register:
	MOV R0, #65
	@ exit program to terminal
	MOV r7, #1
SWI 0 @ SWI: software interrupt, just like "return 0" in C

.data @ where the data are

