.global _start

_start:
	mov r1, #5
	mov r2, #10
	cmp r1, r2
	@ branch if equal:
	beq _vals_eq
	@ branch if first is greater:
	bgt _r1_gt

_r1_lt:
	mov r0, #2
	b _end

_r1_gt:
	mov r0, #1
	b _end

_vals_eq:
	mov r0, #0

_end:
	mov r7, #1
	swi 0

