/* A general structure to an
   assembly program */
.text @ where the instructions are

.global _start @ a label globally available in the program

_start: @ the starting point in the program
	@ some intructions here	
	swi 0 @ swi: software interrupt

.data @ where the data are

