.global _start

_start:
	@ want to have keyboard input:
	mov r7, #3
	@ input stream is coming
	mov r0, #0
	@ want to read in this amount of characters:
	mov r2, #10
	@ load into register what
	ldr r1, =message
	swi 0

_write:
	mov r7, #4
	mov r0, #1
	mov r2, #5
	ldr r1, =message
	swi 0

end:
	mov r7, #1
	swi 0

.data
message:
	.ascii " "

