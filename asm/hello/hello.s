.global _start

_start:
	@ want to write on screen:
	mov r7, #4
	@ output stream is coming:
	mov r0, #1
	@ link the string you want to output this number of chars:
	mov r2, #13
	@ load into register what:
	ldr r1, =message
	swi 0

end:
	mov r7, #1
	swi 0

.data
message:
	.ascii "Hello World!\n"

