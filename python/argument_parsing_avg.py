#!/usr/bin/env python3

import argparse

# from Gemini search for: how to process command line arguments in python?

def avg(nums):
    return sum(nums) / len(nums)

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                        help='an integer for the accumulator')
    parser.add_argument('--sum',  
                        dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='sum the integers (default: find the max)')
    parser.add_argument('--avg',  
                        dest='accumulate', action='store_const',
                        const=avg, default=max,
                        help='the average of the integers (default: find the max)')
    args = parser.parse_args()

    print(args.integers)
    print(args.accumulate(args.integers))

if __name__ == '__main__':
    main()   
