#!/usr/bin/env python3

def my_add(x, y):
    """Calculates the sum of x and y."""
    return x + y

def my_sum(ar):
    assert(isinstance(ar,list))
    sum = 0
    for an_elem in ar:
        sum = my_add(sum,an_elem)
    return sum

def test_my_add():
    assert my_add(2, 3) == 5
    assert my_add(-1, 1) == 0
    print("my_add tests passed!")

def test_my_sum():
    ar0 = []
    ar1 = [1]
    ar3 = [1,2,3]
    assert my_sum(ar0) == 0
    assert my_sum(ar1) == 1
    assert my_sum(ar3) == 6
    print("my_sum tests passed!")
    
if __name__ == "__main__":
    test_my_add()
    test_my_sum()
