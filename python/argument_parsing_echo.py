#!/usr/bin/env python3

# source: https://docs.python.org/3/howto/argparse.html
# accepts exactly one mandatory argument, which it writes back
# more than one arg results in error

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo")
args = parser.parse_args()

print(args.echo)
