#!/usr/bin/env python3

import self_test
import argparse

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                        help='an integer or list of integers for the accumulator')
    args = parser.parse_args()

    print(args.integers)
    print(self_test.my_sum(args.integers))

if __name__ == '__main__':
    main()   
