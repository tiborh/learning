#!/usr/bin/env python3

import csv

with open("tab-separated-values.txt") as tsv:
    for line in csv.reader(tsv, delimiter="\t"): #You can also use dialect="excel-tab" rather than giving a delimiter.
        print(line)
