#!/usr/bin/env python3

import urllib3
from html.parser import HTMLParser
from json import loads

http = urllib3.PoolManager()
raw_html = http.request('GET',"https://api.icndb.com/jokes/random")
print(raw_html);
print(raw_html.data);
print(loads(raw_html.data))
print(loads(raw_html.data).keys())
print(loads(raw_html.data)['value'])
print(loads(raw_html.data)['value']['joke'])
