#!/usr/bin/env python3

import signal
import sys

def signal_handler(sig, frame):
    print('\nYou pressed Ctrl+C!')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
print('Press Ctrl+C')
while 1:
        continue
