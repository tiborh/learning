#!/usr/bin/env python3

# original source:
# https://youtu.be/eN1eZtjLEnU

import signal
import sys
import time
import curses
import urllib3
import html
from json import loads

def end_program():
    ## in older versions:
    # curses.nocbreak()
    # curses.echo()
    # curses.curs_set(1)
    ## end curses
    curses.endwin()
    print("Bye")
    sys.exit(0)

def signal_handler(sig, frame):
    print("Ctrl-C has been pressed.")
    time.sleep(2)
    end_program()

def get_new_joke():
    http = urllib3.PoolManager()
    return(html.unescape(loads(http.request('GET',"https://api.icndb.com/jokes/random").data)['value']['joke']))

signal.signal(signal.SIGINT, signal_handler)

help_str = "Press 'R' to request a new quote, or 'Q' to quit"
r_index = help_str.find("'R'")+1
q_index = help_str.find("'Q'")+1

# start curses
stdscr = curses.initscr()

# initialise screen
curses.noecho()
curses.cbreak()
#curses.nonl()
curses.curs_set(0)
#stdscr.keypad(1) # to enable F keys

if curses.has_colors():
    curses.start_color()

# initialise colour combinations
curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)

# begin program
stdscr.addstr("Random Quotes", curses.A_REVERSE)
stdscr.chgat(-1, curses.A_REVERSE) # fills the rest of the line

stdscr.addstr(curses.LINES-1, 0, help_str)
# R to Green
stdscr.chgat(curses.LINES-1, r_index, 1, curses.A_BOLD | curses.color_pair(2))
# Q to Red
stdscr.chgat(curses.LINES-1, q_index, 1, curses.A_BOLD | curses.color_pair(1))

#                            height         width       y x (start at)
quote_window = curses.newwin(curses.LINES-2,curses.COLS,1,0)
quote_window.box()
quote_text_window = quote_window.subwin(curses.LINES-6,curses.COLS-4, 3, 2)
quote_text_window.addstr(help_str)

stdscr.refresh()
quote_window.refresh()
# stdscr.getkey()
# curses.doupdate() ## refresh screen

while True:
    c = quote_window.getch()

    if c == ord('r') or c == ord('R'):
        quote_text_window.clear()
        quote_text_window.addstr("Getting quote...",curses.color_pair(3))
        quote_text_window.refresh()
        quote_text_window.clear()
        quote_text_window.addstr(get_new_joke())
        #quote_text_window.refresh()
    elif c == ord('q') or c == ord('Q'):
        break

    stdscr.noutrefresh()
    quote_window.noutrefresh()
    quote_text_window.noutrefresh()
    curses.doupdate()
    
end_program()
