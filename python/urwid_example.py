#!/usr/bin/env python3

# original source:
# https://youtu.be/eN1eZtjLEnU?t=1005

import urwid
import signal
import sys
import time
import urllib3
import html
from json import loads

def end_program():
    # curses.endwin() ## replace this one
    print("Bye")
    sys.exit(0)

def signal_handler(sig, frame):
    print("Ctrl-C has been pressed.")
    time.sleep(2)
    end_program()

signal.signal(signal.SIGINT, signal_handler)
    
def get_new_joke():
    http = urllib3.PoolManager()
    return(html.unescape(loads(http.request('GET',"https://api.icndb.com/jokes/random").data)['value']['joke']))

# Handle key presses
def handle_input(key):
    if key == 'R' or key == 'r':
        quote_box.base_widget.set_text(('getting quote', 'Getting new quote...'))
        main_loop.draw_screen()
        quote_box.base_widget.set_text(get_new_joke())
    elif key == 'Q' or key == 'q':
        raise urwid.ExitMainLoop()

# color scheme:
palette = [('titlebar','black','white'),
           ('refresh button', 'dark green,bold', 'black'),
           ('quit button', 'dark red,bold', 'black'),
           ('getting quote', 'dark blue', 'black')]

# create header
header_text = urwid.Text(u'Random Quotes')
header = urwid.AttrMap(header_text, 'titlebar')

# create menu
menu = urwid.Text([u'Press (', ('refresh button', u'R'),
                   u') to get a new quote. Press (',
                   ('quit button', u'Q'),
                   u') to quit.'])

# Create quote box
quote_text = urwid.Text(u'Press (R) to get your first quote!')
quote_filler = urwid.Filler(quote_text, valign='top', top=1, bottom=1)
v_padding = urwid.Padding(quote_filler, left=1, right=1)
quote_box = urwid.LineBox(v_padding)

# assemble the widgets
layout = urwid.Frame(header=header, body=quote_box, footer=menu)

# create the event loop
main_loop = urwid.MainLoop(layout, palette, unhandled_input=handle_input)

# start the program:
main_loop.run()

# architecture:
#                                 Main Loop
#                 /                   |                 \
#          Display Modules      Widget Layout      Event Loops
#            /        \          /         \         /      \
#         Screen    Canvas    ListBox     Text      I/O      \
#        Keyboard   Cache     Contents   Layout    Events  Timers
#         Mouse
#
# Another view:
# ______________________________________________________________________
# |             |  |                 |  |             |  |             |
# |             |  |                 |  |             |  |             |
# | raw_display |  | curses_display  |  | web_display |  |html_fragment|
# |             |  |                 |  |             |  |             |
# |             |  |_________________|  |_____________|  |             |
# |             |  |                 |  |             |  |             |
# |             |  | ncurses_library |  |  apacheCGI  |  |             |
# |             |  |                 |  |             |  |             |
# |_____________|__|_________________|  |_____________|  |_____________|
# |                                  |  |             |  |             |
# |      console or terminal         |  | web browser |  |  HTML file  |
# |                                  |  |             |  |             |
# |__________________________________|  |_____________|  |_____________|
#
# Widget Layout
#_______________________________________________________________________
#| a Topmost Widget                                                    |
#| ___________________________________________________________________ |
#| | b Inner Widget (depth 1)                                        | |
#| | _________________________  ______  ____________________________ | |
#| | | c Inner Widget        |  | d  |  |  e                       | | |
#| | |   (depth 2)           |  |    |  | _______________________  | | |
#| | |_______________________|  |    |  | | f Inner Widget      |  | | |
#| |                            |    |  | |   (depth 3)         |  | | |
#| |                            |    |  | |_____________________|  | | |
#| |                            |    |  |                          | | |
#| |                            |    |  | _______________________  | | |
#| |                            |    |  | | g                   |  | | |
#| |                            |    |  | |                     |  | | |
#| |                            |    |  | |_____________________|  | | |
#| |                            |____|  |__________________________| | |
#| |_________________________________________________________________| |
#|                                                                     |
#|_____________________________________________________________________|

