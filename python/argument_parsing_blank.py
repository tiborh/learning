#!/usr/bin/env python3

import argparse

# source: https://docs.python.org/3/howto/argparse.html
# gives error to any arg

parser = argparse.ArgumentParser()
parser.parse_args()
