<?php
function get_creds($fn) {
    $fn = $fn;
    $fh = fopen($fn, "r") or die("Unable to open file: " . $fn . "\n");
    $creds;

    while(!feof($fh)) {
        $tempArr = explode(":",rtrim(fgets($fh)));
        if (sizeof($tempArr) == 2) {
            $creds[$tempArr[0]] = $tempArr[1];
        }
    }
    fclose($fh);
    
    return $creds;
}

$creds = get_creds("creds.php");
print_r($creds);
?>
