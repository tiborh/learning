<!DOCTYPE html>
<html>
<body>

<?php
$a_str = "you reap what you sow";
echo "original: ".$a_str."\n";
echo "length: ".strlen($a_str)."\n";
echo "nu of words: ".str_word_count($a_str)."\n";
echo "reversed: ".strrev($a_str)."\n";
echo "position of 'what': ".strpos($a_str,"what")."\n";
echo "replace 'you': ".str_replace("you","they",$a_str)."\n";
echo "substring till from pos 'what': ".substr($a_str,strpos($a_str,"what"))."\n";
echo "last two chars: ".substr($a_str,-2)."\n";
echo "before 'what': |".substr($a_str,0,strpos($a_str,"what")-1)."|\n";
echo "char at 'what' position: ".substr($a_str,strpos($a_str,"what"),1)."\n";
echo "three chars omitted from end: ".substr($a_str,strpos($a_str,"what"),-3)."\n";
?>

</body>
</html>
