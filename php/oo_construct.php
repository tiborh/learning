/* Object Oriented PHP */
<?php
class HelloWorld {
    public $world;
    
    function __construct($world) {
        $this->world = $world;
    }

    function getHtml() {
        return "<html><body>\n".
            "<p>Hello, ".$this->world."!</p>\n".
            "</body></html>\n";
    }
}

$greetings = new HelloWorld("All");
echo $greetings->getHtml();
?>
