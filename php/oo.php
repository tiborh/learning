/* Object Oriented PHP */
<?php
class HelloWorld {
    public $world = 'World';
    function getHtml() {
        return "<html><body>\n".
            "<p>Hello, ".$this->world."!</p>\n".
            "</body></html>\n";
    } /* function */
}     /* class */

$greetings = new HelloWorld;
echo $greetings->getHtml();
?>
