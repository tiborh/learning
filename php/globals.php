<!DOCTYPE html>
<html>
<body>

<?php
$x = 5;
$y = 10;

echo "x == " . $x . "<br />";

function myTest($z) {
    global $x;                  /* needed to be able to access global variable */

    echo "z == " . $z . "<br />";
    echo "y = " . $x . " + " . $GLOBALS['y'] . " + " . $z . "<br />"; /* $GLOBALS array has all the global variables */
    $GLOBALS['y'] = $x + $GLOBALS['y'] + $z;

}

echo "y == " . $y . "<br />";

myTest(7);

echo "y == " . $y . "<br />";

?>

</body>
</html>
