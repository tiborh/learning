<?php

class HtmlDocument {
    function getHtml() {
        return "<html><body>\n".$this->getContent().
            "</body></html>\n";
    }
    function getContent() { return ''; }
}

class HelloWorld extends HtmlDocument {
    public $world;
    function __construct($world ) {
        $this->name = $world ;
    }
    function getContent() {
        return "<p>Hello, ".$this->name."!</p>\n";
    }
}

?>