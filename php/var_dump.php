<!DOCTYPE html>
<html>
<body>
<?php
$x = 5985;
echo var_dump($x)."<br />\n";
$y = pi();
echo var_dump($y)."<br />\n";
$b = true;
echo var_dump($b)."<br />\n";
$ar = array("one","two","three");
echo var_dump($ar)."<br />\n";
class Thingy {
    function __construct($model) {
        $this->model = $model;
    }
}
$th = new Thingy("th");
echo var_dump($th)."<br />\n";
$n = null;
echo " (dumping null-ed var) ".var_dump($n)."<br />\n";
$fn = "test.txt";
$fh = fopen($fn,"r") or die("unable to open file: ".$fn."\n");
echo var_dump($fh)."<br />\n";
fclose($fh);
?>
</body>
</html>
