<!DOCTYPE html>
<html>
<body>

<?php
define("SAYING","十人十色",true);
echo "defined as case insensitive: ".saying."\n";
define("COLOURS",["白","青","黒","緑","赤","黄色","葉色","茶色"],true);
echo "constant array: ";
print_r(colours);
echo "\n";
function check_globality() {
    echo "inside a function: ".saying."\n";
}
check_globality();
?>

</body>
</html>
