#!/usr/bin/env racket
#lang racket

(define (incr-byte0 tb)
  (bytes-append (make-bytes 1 (add1 (bytes-ref tb 0))) (make-bytes 1 0))
  )
(define (incr-byte1 tb)
  (bytes-append (make-bytes 1 (bytes-ref tb 0)) (make-bytes 1 (add1 (bytes-ref tb 1))))
  )
(define (incr-byte tb)
  (if (= (bytes-ref tb 1) 255)
      (incr-byte0 tb)
      (incr-byte1 tb)
      )
  )
(define (char-lister tb)
  (cond [(and (= (bytes-ref tb 0) 255)
              (= (bytes-ref tb 1) 255)
              )
         empty]
        [else (cons tb (char-lister (incr-byte tb)))]
        )
  )
(define (print-chars) (let ([two-bytes (make-bytes 2 0)])
                        ;;                     (displayln (char-lister two-bytes))
                        (with-output-to-file "byte_print.txt" #:exists 'replace
                          (lambda () (printf "~a~n" (char-lister two-bytes)))
                          )
                        
                        )
  )
(print-chars)
;; three bytes are needed to utf-8, Japanese chars
;; see: https://sites.psu.edu/symbolcodes/languages/asia/japanese/hiraganachart/
