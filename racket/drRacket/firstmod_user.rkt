#lang slideshow
(require "firstmod.rkt")
(define sz 30)
(rainbow (sq sz))
(rainbow (fl-sq sz))
(apply vc-append (rainbow (fl-sq sz)))