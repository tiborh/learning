#!/usr/bin/env racket
#lang racket
(require "mod_die.rkt")
(define args (vector->list (current-command-line-arguments)))
(define argslen (length args))
(define sides (if (> argslen 0) (string->number (first args)) 6))
(writeln (rolln sides))

