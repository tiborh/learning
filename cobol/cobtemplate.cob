       identification division.
       program-id. cobtemplate.
       author. Tibor.
       date-written. 2020.
       environment division.
       data division.
       file section.
       working-storage section. *> for variables
       *> zero, zeros, zeroes are all acceptable as figurative constants
       *> space or spaces to assign a space
       *> high-value or high-values for the largest value
       *> low-value or low-values for the lowest 
       procedure division.
           stop run.
           
