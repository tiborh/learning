       *> first tutorial program
       identification division.
       program-id. coboltut.
       author. Tibor.
       date-written. 27th April 2020.
       environment division.
       data division.
       file section.
       working-storage section. *> for variables
       01 UserName pic x(30) value "You". *> alpha numberic, (30) max size
       01 Num1     pic  9 value zeros.     *> value can be from zero to 9
       01 Num2     pic  9 value zeros.
       01 Total    pic 99 value 0.
       01 SSNum.                *> hierarchical variable: composed of:
           02 SSArea   pic 999.
           02 SSGroup  pic 99.
           02 SSSerial pic 9999.
       01 PIValue constant 3.141592653589793.
       *> zero, zeros, zeroes are all acceptable as figurative constants
       *> space or spaces to assign a space
       *> high-value or high-values for the largest value
       *> low-value or low-values for the lowest 
       procedure division.
           display "What is your name? " with no advancing
           accept UserName
           display "Hello " UserName "!"
           move zero to UserName
           display "UserName zeroed: " UserName
           display "Adding two numbers."
           display "Enter num1 (0-9): " with no advancing
           accept Num1
           display "Enter num2 (0-9): " with no advancing
           accept Num2
           compute Total = Num1 + Num2
           display Num1 " + " Num2 " = " Total
           display "Enter your social security number: "
           accept SSNum
           display "Social Security Num: " SSNum
           display "Area num:   " SSArea
           display "Group ID:   " SSGroup
           display "Serial num: " SSSerial
           display "PI: " PIValue
           stop run.
           
