       identification division.
       program-id. cobtut2.
       author. Tibor.
       date-written. 2020-04-27.
       environment division.
       data division.
       file section.
       working-storage section. *> for variables
       01 SampleData pic x(10) value "Stuff".
       01 JustLetters pic AAA value "ABC".
       01 JustNums pic 9(4) value 1234.
       01 SignedInt pic s9(4) value -1234.
       01 FloatNum pic 9(4)V99 value zeros.
       01 Customer.
           02 CustID pic 9(3).
           02 CustName pic x(20).
           02 CustBirthDate.
               03 YoB pic 9999.
               03 MoB pic 99.
               03 DoB pic 99.
       01 Num1 pic 9 value 5.
       01 Num2 pic 9 value 4.
       01 Num3 pic 9 value 3.
       01 Ans pic s99v99 value 0.
       01 IntAns pic s999 value 0.
       01 Rem pic 9v99 value zero.
       *> zero, zeros, zeroes are all acceptable as figurative constants
       *> space or spaces to assign a space
       *> high-value or high-values for the largest value
       *> low-value or low-values for the lowest 
       procedure division.
           display "Sample Data: " SampleData
           move "more stuff" to SampleData
           display "Sample Data: " SampleData
           move "123" to SampleData
           display "Sample Data: " SampleData
           move 123 to SampleData
           display "Sample Data: " SampleData
           move zero to SampleData
           display "Zeroed out: " SampleData
           move space to SampleData
           display "Spaced out: " SampleData "|end|"
           move high-value to SampleData
           display "high-value-d: " SampleData
           move low-value to SampleData
           display "low-value-d: " SampleData "|end|"
           move quote to SampleData
           display "quoted out: " SampleData
           move all "2" to SampleData
           display "filled with 2s: " SampleData
           display "Floating Num: " FloatNum
           move "123Bob Smith           19741221" to Customer
           display "Customer:"
           display "ID: " CustID
           display "Name: " CustName
           display "Date of Birth: " YoB "-" MoB "-" DoB
           display "Math:"
           add Num1 to Num2 giving Ans
           display Num1 " + " Num2 " = " Ans
           add Num1,Num2 to Num3 giving Ans
           *> alternatively:
           *> add Num1,Num2,Num3 giving Ans
           *> compute also works, of course for all of the examples
           display Num1 " + " Num2 " + " Num3 " = " Ans
           subtract Num1 from Num2 giving Ans
           display Num2 " - " Num1 " = " Ans
           multiply Num1 by Num2 giving Ans
           display Num1 " * " Num2 " = " Ans
           divide Num1 by Num2 giving Ans
           display Num1 " / " Num2 " = " Ans
           compute IntAns = Num2 / Num1
           display Num2 " / " Num1 " = " IntAns
           compute IntAns rounded = Num2 / Num1
           display Num2 " / " Num1 " rounded: " IntAns
           divide Num1 by Num2 giving IntAns remainder Rem
           display Num1 " / " Num2 " = " IntAns " (rem: " Rem ")"
           compute IntAns = Num1 ** Num3
           display Num1 "^" Num3 " = " IntAns
           stop run.
           
