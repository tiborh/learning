#!/usr/bin/env clisp

(load "my_funcs.lisp")

(defun my-set-difference (l1 l2)
  (cond ((null l1) nil)
	((not (my-member (car l1) l2))
	 (cons (car l1) (my-set-difference (cdr l1) l2))
	 )
	(t (my-set-difference (cdr l1) l2))
	)
  )
	      
;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(my-set-difference '(1 2 3 4 5) '(4 5 6 7 8)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
