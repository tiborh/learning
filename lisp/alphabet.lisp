#!/usr/bin/env -S sbcl --script
;; (defconstant +args+ (cdr *posix-argv*))
(load "my-utils.lisp") ; alternatively

(format t "~&~a~%" (my-utils::alphabet))
(format t "~&~a~%" (my-utils::capitals))

(format t "~&~a~%" (my-utils::alphabet :start "b" :end "m"))
(format t "~&~a~%" (reverse (my-utils::capitals :start "D" :end "S")))

(format t "~&終~%")
