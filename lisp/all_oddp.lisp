#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun all-oddp (li-o-nu)
  (cond ((null li-o-nu) nil)
	((not (oddp (car li-o-nu))) nil)
	((and (null (cdr li-o-nu))
	      (oddp (car li-o-nu))) t)
	(t (all-oddp (cdr li-o-nu)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (all-oddp nil)))
(assert (all-oddp '(1)))
(assert (null (all-oddp '(2))))
(assert (null (all-oddp '(1 3 5 6 7))))
(assert (all-oddp '(1 3 5 7 9)))
