#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defconstant *maxnum* 10)
(defparameter *validator* (let ((num (random *maxnum* (make-random-state t)))) #'(lambda (guess) (= guess num))))

;; (load "dtrace.generic")
;; (dtrace func)

(loop for i from 0 to (1- *maxnum*)
   do (princ i)(princ ": ")(princ (funcall *validator* i))(terpri)
     )
