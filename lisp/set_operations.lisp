#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun add-members(st li)
  (dolist (it li)
    (pushnew it st)
    )
  ;;  (format t "~&~s~%" st)
  (return-from add-members st)
  )

(let ((a-set (add-members () '(0 1 2 3 4 5)))
      (b-set (add-members () '(3 4 5 6 7 8)))
      (popped nil)
      )
  (format t "~&a: ~s~%" a-set)
  (format t "~&b: ~s~%" b-set)
  (setf popped (pop a-set))
  (format t "~&popped: ~s~%" popped)
  (format t "~&a: ~s~%" a-set)
  (pushnew popped a-set)
  (format t "~&restored a: ~s~%" a-set)
  (format t "~&Union: ~s~%" (union a-set b-set)) ; nunion: in-place
  (format t "~&Instersection: ~s~%" (intersection a-set b-set))
  (format t "~&set-difference (a-b): ~s~%" (set-difference a-set b-set)) ; nset-difference: in-place
  (format t "~&set-difference (b-a): ~s~%" (set-difference b-set a-set))
  )

(format t "~&終~%")
