#!/usr/bin/env clisp

(defun my-find-if (fu li)
  (cond ((null li) nil)
  	((funcall fu (car li)) t)
  	(t (my-find-if fu (cdr li)))
  	)
  )

;; (load "dtrace.generic")
;; (dtrace my-find-if)

(let ((a (list 1 3 nil 7 9))
      (b (list 0 2 4 6 8))
      )
  (format t "~&list a: ~s~%" a)
  (format t "~&list b: ~s~%" b)
  (format t "Find-if oddp in a: ~s~%" (find-if #'oddp a))
  (format t "Find-if oddp in b: ~s~%" (find-if #'oddp b))
  (format t "Find-if null in a: ~s~%" (find-if #'null a))
  (format t "Find-if null in b: ~s~%" (find-if #'null b))
  (format t "trying the four above with own function: ~s ~s ~s ~s~%" (my-find-if #'oddp a) (my-find-if #'oddp b) (my-find-if #'null a) (my-find-if #'null b))
  )
