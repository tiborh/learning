#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a '(small (1 2) (3 4)))
       (b '(middle (5 6) (7 8)))
       (c '(large (9 10) (11 12)))
       (all (list a b c))
       )
  (format t "~&structure: ~a~%" all)
  (format t "~&find middle: ~a~%" (assoc 'middle all))
  (format t "~&cdr of the above: ~a~%" (cdr (assoc 'middle all)))
  (format t "~&find 6 in middle: ~a~%" (find 6 (cadr (assoc 'middle all))))
  (format t "~&find the whole list where 6 is found ~a:~%" (find 6 (cdr (assoc 'middle all)) :key #'cadr))
  )
       
