#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun proper-listp (li)
  (cond ((null li) t)
	((atom li) nil)
	(t (proper-listp (cdr li)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (proper-listp nil))
(assert (proper-listp '(1)))
(assert (proper-listp '(1 2 3)))
(assert (not (proper-listp 1)))
(assert (not (proper-listp '(1 . 2))))
