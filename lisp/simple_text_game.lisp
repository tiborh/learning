#!/usr/bin/env clisp

(load "simple_text_game.params")

(defun describe-location (location nodes)
  (cadr (assoc location nodes))
  )

(defun describe-path (edge)
	       `(there is a ,(caddr edge) going ,(cadr edge) from here.)
	       )

(defun describe-paths (location edges)
		(apply #'append (mapcar #'describe-path (cdr (assoc location edges))
					)
		       )
		)

(defun objects-at (loc objs obj-locs)
    (remove-if-not #'(lambda (obj) (eq (cadr (assoc obj obj-locs)) loc)) objs)
  )

(defun describe-objects (loc objs obj-loc)
    (apply #'append (mapcar #'(lambda (obj) `(you see a ,obj on the floor.)) (objects-at loc objs obj-loc)))
  )

;; Simplification:

(defun look ()
  (append (describe-location *location* *nodes*)
	  (describe-paths *location* *edges*)
	  (describe-objects *location* *objects* *object-locations*)
	  )
  )

(defun walk (direction)
  (let ((next (find direction (cdr (assoc *location* *edges*)) :key #'cadr))
	)
    (if next (progn (setf *location* (car next))
		    (look)
		    )
	'(you cannot go that way.)
	)
    )
  )

(defun pickup (object)
  (cond ((member object (objects-at *location* *objects* *object-locations*))
	 (push (list object 'body) *object-locations*)
	 `(you are now carrying the ,object)
	 )
	(t '(you cannot get that.))
	)
  )

(defun inventory ()
  (cons 'items- (objects-at 'body *objects* *object-locations*))
  )

(defun tweak-text (lst caps lit)
  ;; processes list of chars, capitalises if caps is true, leaves untoched if lit is true
  (when lst
    (let ((item (car lst))
	  (rest (cdr lst))
	  )
      (cond ((eql item #\space) (cons item (tweak-text rest caps lit)))
	    ((member item *end-of-sentence*) (cons item (tweak-text rest t lit)))
	    ((eql item #\") (tweak-text rest caps (not lit)))
	    (lit (cons item (tweak-text rest nil lit)))
	    (caps (cons (char-upcase item) (tweak-text rest nil lit)))
	    (t (cons (char-downcase item) (tweak-text rest nil nil)))
	    )
      )
    )
  )
  
(defun game-print (list)
  (princ (coerce (tweak-text (coerce (string-trim "() " (prin1-to-string list))
				     'list)
			     t nil)
		 'string)
	 )
  (fresh-line)
  )

(defun help ()
  (cons "allowed-commands:" *allowed-commands*)
  )

(defun game-eval (form)
  (if (member (car form) *allowed-commands*)
      (eval form)
      '(Invalid command)
      )
  )

(defun game-read ()
  (let ((cmd (read-from-string (concatenate 'string "(" (read-line) ")")
			       )
	  )
	)
      (cons (car cmd) (mapcar #'(lambda (x) (list 'quote x)) (cdr cmd)))
    )
  )

(defun game-repl ()
  (let ((cmd (game-read)))
    (unless (eq (car cmd) 'quit)
      (game-print (game-eval cmd))
      (game-repl)
      )
    )
)

;; (load "dtrace.generic")
;; (dtrace func)

(game-print (help))
(game-repl)
