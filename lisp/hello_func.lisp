(defun hello (&optional (x "Joe"))
  (format t "~&Hello, ~a!~%" x)
  )

(defun hellos (li)
  (cond ((null li) nil)
	(t (progn
	     (hello (car li))
	     (hellos (cdr li))
	     )
	   )
	)
  )
