#!/usr/bin/env clisp

(load "countdown_funcs.lisp")

(defun factorial (n)
  (reduce #'* (countdown n))
  )

(defun fact (n)
  (if (or (not (typep n 'integer)) (minusp n))
      (error "~S is not a natural number." n)
      (factorial n)
      )
  )

(when (zerop (length *args*))
    (format t "<cmd> <natural number1> [<natural number2> ...]")
    )

(dolist (i *args*) 
  do (format t "~&~a! ~3d~%" i (fact (parse-integer i)))
  )
