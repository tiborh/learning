#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(write-line "with 'map' you need to give the output type")
(let ((a "sample string"))
  (map 'string #'(lambda (c) (print c)) a)
  )
(let ((a '(1 2 3 4))
      (b '(10 9 8 7))
      )
  ;; from http://clhs.lisp.se/Body/f_map.htm
  (format t "~&~s~%" (map 'string #'(lambda (x y)
			       (char "01234567890ABCDEF" (mod (+ x y) 16)))
		   a b
		   )
	      )
  )

(format t "~&mapcar for doing the same on each element of the list (preserving their type)~%")
(let* ((a '(0 1 2 3 4))
       (b (mapcar '1+ a))
       )
  (format t "~&Before: ~a~%" a)
  (format t "~&After mapcar: ~a~%" b)
  )

(format t "~&mapc does not return a changed list~%")
(let* ((a '(0 1 2 3 4))
       (b (mapc (lambda (an) (fresh-line) (prin1 (1+ an))) a))
       )
  (format t "~&Before: ~a~%" a)
  (format t "~&After:  ~a~%" b)
  )

(format t "~&maplist has the remaining list, not only a single element.~%")
(let* ((a '(0 1 2 3 4))
       (b (maplist (lambda (an) (fresh-line) (prin1 an)) a))
       )
  (format t "~&Before: ~a~%" a)
  (format t "~&After:  ~a~%" b)
  )
