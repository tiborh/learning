#!/usr/bin/env clisp

(defparameter hexchars "0123456789ABCDEF")

(defun dec-to-hex-digit (n)
  (string (char hexchars n))
  )

(defun dec-to-hex (n)
  ;; (format t "~&n == ~a~%" n)
  (cond ((< n 16) (dec-to-hex-digit n))
	(t (concatenate 'string (dec-to-hex (floor (/ n 16))) (dec-to-hex-digit (mod n 16))))
	)
)
  
;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (write-line "No args. A natural number is expected.")
    (write-line (dec-to-hex (parse-integer (car *args*))))
    )
    
