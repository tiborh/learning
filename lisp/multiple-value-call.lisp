#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(let ((li (multiple-value-call #'list (floor 5 2) (floor 7 3))))
  (assert (equal li '(2 1 2 1)))
  (princ li)(terpri)
  )
