#!/usr/bin/env clisp

(defun fct (n)
  (cond ((zerop n) 1)
	(t (* n (fct (1- n))))
	)
  )

;;ex 8.59 in Touretzky's book
(defun fact (n)
  (if (zerop n)
      1
      ;; (/ (fact (1+ n)) (1+ n))       ;this would lead to an infinite loop
      (/ (fct (1+ n)) (1+ n))		;the original formula should not call itself
      )
  )
  
(defun factorial (str)
  (let ((n (parse-integer str)))
    (when (<= 0 n)
      (format t "~&~a! == ~a~%" n (fact n))
      )
    )
  T
  )

;; (load "dtrace.generic")
;; (dtrace fact)

(every #'factorial *args*)
