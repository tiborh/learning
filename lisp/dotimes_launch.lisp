#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun launch (n)
  (let ((nu (1+ n)))
    (dotimes (it nu)
      (format t "~a..." (- n it))
      )
    )
    (format t "liftoff, it's a liftoff!")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(when *args*
  (launch (parse-integer (car *args*)))
  )
