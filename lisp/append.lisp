#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a (list 1 2 3 4))
       (b (list 5 6 7 8))
       (c (list a b))
       (d '(9 10))
      )
  (format t "~&a:            ~a~%" a)
  (format t "~&b:            ~a~%" b)
  (format t "~&(append a b): ~a~%" (append a b))
  (format t "~&c:            ~a~%" c)
  (format t "~&d:            ~a~%" d)
  (format t "~&(append c d): ~a~%" (append c d))
  (format t "~&append c and d with apply: ~a~%" (append (apply #'append c) d))
  )
