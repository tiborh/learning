#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun print-vals (x y)
  (write-line "let x be:")
  (prin1 x)(terpri)
  (write-line "and let y be:")
  (prin1 y)(terpri)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(write-line "both setf and psetf can add values to multiple variables")
(write-line "difference is setf works sequentially, psetf works in parallel.")
(let* ((x1 (cons 'a 'b))
       (y1 (list 1 2 3))
       (x2 (cons 'a 'b))
       (y2 (list 1 2 3))
       (chg-str "(car x) 'x (cadr y) (car x) (cdr x) y)")
       (the-change1 `(setf (car ',x1) 'x (cadr ',y1) (car ',x1) (cdr ',x1) ',y1))
       (the-change2 `(psetf (car ',x2) 'x (cadr ',y2) (car ',x2) (cdr ',x2) ',y2))
      )
  (print-vals x1 y1)
  (write-line "The first change:")
  (format t "~&(setf ~a)~%" chg-str)
  (eval the-change1)
  (print-vals x1 y1)
  (write-line "The second change:")
  (format t "~&(psetf ~a)~%" chg-str)
  (eval the-change2)
  (print-vals x2 y2)
  )
