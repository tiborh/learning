#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

;; from http://www.lispworks.com/documentation/HyperSpec/Body/f_comple.htm

;; returns the complement of what the function would return

(print (funcall (complement #'zerop) 1))
(print (funcall (complement #'characterp) #\A))
(print (funcall (complement #'member) 'a '(a b c)))
(print (funcall (complement #'member) 'd '(a b c)))

