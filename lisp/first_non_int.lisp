#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun first-non-int (li)
  (dolist (it li)
    (when (not (integerp it))
      (return it)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (first-non-int nil)))
(assert (null (first-non-int '(1 2 3))))
(assert (equal #\a (first-non-int '(1 2 3 #\a))))
(assert (equal #\a (first-non-int '(#\a 1 2 3))))
(assert (equal #\a (first-non-int '(1 2 #\a 3))))
(assert (null (first-non-int '(1 2 nil 3))))
(assert (equal #\a (first-non-int '(1 2 #\a 3 #\b))))
