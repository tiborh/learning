#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(when (null *args*)
  (format t "~&No args.~%")
  (EXT:EXIT)
  )

(loop for i in *args*
   do (format t "~&~a~%" i)
     )
