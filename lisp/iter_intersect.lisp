#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "iter_funcs.lisp")

(defun iter-intersect (l0 l1)
  (let ((li nil))
    (dolist (it l0 (nreverse li))
      (when (iter-member it l1)
	(push it li)
	)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (iter-intersect nil nil)))
(assert (null (iter-intersect nil '(1 2 3))))
(assert (null (iter-intersect '(1 2 3) nil)))
(assert (null (iter-intersect '(a b c) '(d e f))))
(assert (equal '(c d) (iter-intersect '(a b c d) '(c d e f))))
(assert (equal '(a b) (iter-intersect '(a b c d) '(e f b a))))
