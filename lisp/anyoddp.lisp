#!/usr/bin/env clisp

(defun anyoddp (li)
  (cond ((null li) nil)
	((oddp (parse-integer (car li))) t)
	(t (anyoddp (cdr li)))
	)
  )

(defun test-anyoddp ()
  (assert (null (anyoddp '("2"))))
  (assert (anyoddp '("1")))
  (assert (anyoddp '("2" "4" "5" "6" "7")))
  (assert (anyoddp '("1" "two" "three")))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace anyoddp)

(if (null *args*)
    (test-anyoddp)
    (format t "~&~a~%" (anyoddp *args*))
  )
