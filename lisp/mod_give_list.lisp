(defun give-reverse-list (n min-num)
  (cond ((< n min-num) nil)
	(t (cons n (give-reverse-list (- n 1) min-num)))
	)
  )

(defun give-list (n &optional start-num)
  (reverse (give-reverse-list n (if start-num start-num 0)))
  )
