#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defparameter max-n 100)
(defparameter min-n -100)

;; (load "dtrace.generic")
;; (dtrace func)

(if (<= min-n (parse-integer (car *args*)) max-n)
    (write-line "in range")
    (write-line "out of range")
    )
