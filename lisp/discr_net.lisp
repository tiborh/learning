#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defparameter *node-list* nil)
(defconstant FN "discr_net.txt")

(defstruct node
  (name nil)
  (quest nil)
  (y-case nil)
  (n-case nil)
  )

(defun add-node (n q y no)
  (push (make-node :name n :quest q :y-case y :n-case no) *node-list*)
  (car *node-list*)
  )

(defun read-node-list-from-file ()
  (with-open-file (fh FN)
    (setf *node-list* (read fh))
    )
  )

(defun print-node-list ()
  (format t "~&node list: ~a~%" *node-list*)
  )

(defun add-new-node-interactively (name)
  (let ((n-quest nil)
	(n-y nil)
	(n-n nil)
	)
    (princ "Node question: ")
    (setf n-quest (read))
    (princ "Yes case: ")
    (setf n-y (read))
    (princ "No case: ")
    (setf n-n (read))
    (add-node name n-quest n-y n-n)
    )
  )

(defun would-you-like-to-add (name)
  (when (null name)
    (return-from would-you-like-to-add nil)
    )
  (princ name)
  (if (y-or-n-p "Would you like to add this node?")
      (add-new-node-interactively name)
      nil
      )
  )

(defun find-node (node-name node-list)
  (cond ((null node-list) (would-you-like-to-add node-name))
	((eq node-name (node-name (car node-list))) (car node-list))
	(t (find-node node-name (cdr node-list)))
	)
  )

(defun process-node (name)
  (let ((a-node (find-node name *node-list*)))
    (when (null a-node)
      (format t "~&There is no node with name ~a in the list.~%" name)
      (return-from process-node nil)
      )
    (if (y-or-n-p (node-quest a-node))
	(node-y-case a-node)
	(node-n-case a-node)
	)
    )
  )

(defun run ()
  (do ((current-node 'start (process-node current-node)))
      ((null current-node) 'open-case)
    (when (stringp current-node)
      (write-line current-node)
      (return 'solved-case)
      )
    )
  )

(defun save-node-list (node-list filename)
  (with-open-file (fh filename
			    :direction :output
			    :if-exists :supersede
		   ;; :if-exists :append
			    :if-does-not-exist :create
			    )
    (format fh "~&~s~%" node-list fh)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

;; (format t "~&~a~%" #s(node))
(read-node-list-from-file)
;;(init)
;;(print-node-list)
(format t "~&~a~%" (run))
(save-node-list *node-list* FN)
