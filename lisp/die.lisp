#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun cast-die (&optional (n_sides 6))
  (+ 1 (random n_sides (make-random-state t)))
  )

(if (null *args*)
    (format t "~&Usage tip: an argument gives the number of sides.~%")
    (let ((n (parse-integer (first *args*))))
      (format t "~&Number of sides: ~a~%" n)
      (format t "~&Die cast: ~a~%" (cast-die n))
      )
)
