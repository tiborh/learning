#!/usr/bin/env clisp

(format t "~&Give me a number: ")
(let ((x (read)))
  (format t "~&Value read: ~s~%" x)
  (format t "~&Times two:  ~s~%" (* 2 x))
  )
