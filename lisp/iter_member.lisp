#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "iter_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (iter-member 1 nil)))
(assert (null (iter-member 1 '(2))))
(assert (iter-member 1 '(1)))
(assert (iter-member 1 '(2 3 4 5 6 2 1)))
(assert (null (iter-member 1 '(2 3 4 5 6 2))))
