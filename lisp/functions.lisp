#!/usr/bin/env clisp

(defconstant MAXN 10)

(defun give-num (maxnum)
  "Gives a(n integer) number less than maxnum" ; documentation
  (random maxnum (make-random-state t))
  )

(defun give-num2 (&optional maxnum)
  "Shows how optional argument works."
  (unless maxnum
    (setq maxnum MAXN)
    )
  (give-num maxnum)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(when (null *args*)
  (format t "~&No args.~%")
  (format t "~&Demo value 1: ~d~%" (give-num2))
  (format t "~&Demo value 2: ~d~%" (give-num2 MAXN))
  (EXT:EXIT)
  )

(let ((maxnum (parse-integer (first *args*))))
  (format t "~&Your number: ~d~%Generated number: ~d~%" maxnum (give-num maxnum))
  )
