#!/usr/bin/env clisp

(defun factorial (n)
  (cond ((zerop n) 1)
	(t (* n (factorial (- n 1)))
	   )
	)
  )

(defun fact (n)
  (if (or (not (typep n 'integer)) (minusp n))
      (error "~S is not a natural number." n)
      (factorial n)
      )
  )

(when (zerop (length *args*))
    (format t "<cmd> <natural number1> [<natural number2> ...]")
    )

(dolist (i *args*) 
  do (format t "~&~a! ~3d~%" i (fact (parse-integer i)))
  )
