#!/usr/bin/env clisp

(load "mod_give_list.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(when (null *args*)
  (format t "~&No args.~%")
  (EXT:EXIT)
  )

(let ((n (parse-integer (first *args*))))
  (when (> n -1)
    (format t "~&~s~%" (give-reverse-list n 0))
    (format t "~&~s~%" (give-list n))
    )
  )
