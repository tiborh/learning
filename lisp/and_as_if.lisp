#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(and *args*
     (numberp (read-from-string (first *args*)))
     (format t "~&The integer read: ~a~%" (parse-integer (first *args*) :junk-allowed t))
     )
