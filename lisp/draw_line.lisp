#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "draw_tools.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (draw-lines *args*)
  )
