#!/usr/bin/env clisp

(when (null *args*)
    (format t "~&No args.~%(An integer is needed.)~%")
    (EXT:EXIT)
    )

(load "digit_funcs.lisp")

(defparameter input-num (parse-integer (first *args*)))

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&Input reads:              ~d~%" input-num)
(format t "~&Number of digits:         ~d~%" (num-of-digits input-num))
(format t "~&First digit:              ~d~%" (first-digit input-num))
(format t "~&Rest of the digits:       ~d~%" (rest-digits input-num))
(format t "~&Last digit:               ~d~%" (last-digit input-num))
(format t "~&Front rest of the digits: ~d~%" (frest-digits input-num))
