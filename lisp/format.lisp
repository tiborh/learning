#!/usr/bin/env clisp

(defun a_printer (a_list)
  (dolist (n a_list)
    (format t "~&~a~&" n))
  )

(defun s_printer (a_list)
  (dolist (n a_list)
    (format t "~&~s~&" n))
  )

(print "Most useful is ~a and ~s, both use auto formatting.")
(print "The next two is ~% for newline, and ~& for fresh line (only new if not on newline)")
(defvar a "alpha")
(defvar b 3)
(defvar c 3.14)
(defvar d pi)
(defvar e (list "one" "two" "three"))
(defvar f '(1 2 3))
(defvar the_vars (list a b c d e f))

(format t "~&simple lists with a_printer:~&")
(a_printer e)
(a_printer f)
(format t "~&simple lists with s_printer:~&")
(s_printer e)
(s_printer f)
(format t "~&complex list with a_printer:~&")
(a_printer the_vars)
(format t "~&complex list with s_printer:~&")
(s_printer the_vars)
(format t "~&left padding with @: ~10@a~%" a)
(format t "~&right padding with number only: ~10a(text after)~%" a)

(defvar a_char #\Space)
(format t "#~cSpace character representation with: ~~c ~~:c ~~@c" #\\)
(format t "~&~~c simple print: two~cwords~%" a_char)
(format t "~&~~:c to make the invisible visible: two~:cwords~%" a_char)
(format t "~~@c with Lisp's syntax: two~@cwords~%" a_char)

;; for more: http://www.gigamonkeys.com/book/a-few-format-recipes.html
