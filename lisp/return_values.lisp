#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun ret-vals ()
    (values 1 2 3)
  )

(let ((vals (ret-vals)))
  (format t "~&type of values: ~s~%" (type-of vals))
  (format t "~&values printed: ~s~%" vals)
  )
(multiple-value-bind
      (one two three)
    (ret-vals)
  (format t "~&multiple bound: ~s ~s ~s~%" one two three)
  )


(format t "~&終~%")
