#!/usr/bin/env -S sbcl --script

(defconstant *args* (cdr *posix-argv*))

(if (null *args*)
    (format t "~&Give day number (0-6).~%")
    (case (parse-integer (car *args*))
      (0 (format t "~&  Monday~&"))
      (1 (format t "~&  Tuesday~&"))
      (2 (format t "~&  Wednesday~&"))
      (3 (format t "~&  Thursday~&"))
      (4 (format t "~&  Friday~&"))
      (5 (format t "~&  Saturday~&"))
      (6 (format t "~&  Sunday~&"))
      (t (format t "~&  Out of bounds.~%"))
      )
    )
