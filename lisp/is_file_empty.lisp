#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "file_io_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (if (is-file-empty? (car *args*))
      (progn (princ "yes")(terpri))
      (progn (princ "no")(terpri))
      )
  )
