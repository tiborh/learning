#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "ltk/ltk")
(load "cgn/cgn")

(in-package :cgn)

;; (start-gnuplot) ; good only in interactive mode
(with-gnuplot ('linux)
  (plot-function "cos(x)")
)

;; (load "dtrace.generic")
;; (dtrace func)

