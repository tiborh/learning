#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun mat-max (mat)
  "Find the max value in a two-dimensional matrix."
  (let (max)
    (dotimes (i (array-dimension mat 0))
      (dotimes (j (array-dimension mat 1))
	(when (or (null max)
		  (> (aref mat i j) max))
	  (setf max (aref mat i j))
	  )
	)
      )
    max
    )
  )

(let (mat)
  (setf mat (make-array '(2 3)
			:initial-contents '((0 1 2) (3 4 5))
			)
	)
  (format t "~&Matrix: ~s~%" mat)
  (format t "~&   Max: ~s~%" (mat-max mat))
  )
