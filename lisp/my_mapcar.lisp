#!/usr/bin/env clisp

(defun my-mapcar (func li)
  (cond ((null li) nil)
	(t (cons (funcall func (car li)) (my-mapcar func (cdr li))))
	)
  )


;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(my-mapcar #'oddp '(1 2 3 4 5 6 7)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
