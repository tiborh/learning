#!/usr/bin/env clisp

(defun eo (li n)
  (cond ((null li) nil)
	((oddp n) (cons (car li) (eo (cdr li) (1+ n))))
	(t (eo (cdr li) (1+ n)))
	)
  )

(defun every-other (li)
  (eo li 1)
  )

(defun test-every-other ()
  (assert (equal (every-other '(A B C D E F G)) '(A C E G)))
  (assert (equal (every-other '(i came i saw i conquered)) '(i i i)))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-every-other)
    (format t "~&~a~%" (every-other *args*))
    )
