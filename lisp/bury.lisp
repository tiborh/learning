#!/usr/bin/env clisp

(defun bury (it &optional (n 0))
  (cond ((<= n 0) it)
      (t (list (bury it (1- n))))
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (if (= (length *args*) 2)
        (format t "~&~a~%" (bury (car *args*) (parse-integer (cadr *args*))))
	(format t "~&~a~%" (bury (car *args*)))
	)
  )
