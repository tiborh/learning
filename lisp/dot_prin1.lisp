#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun dot-prin1 (li)
  (cond ((null li) (princ "nil"))
	((atom li) (princ li))
	(t (progn
	     (princ "(")
	     (and (dot-prin1 (car li))
		  (princ " . ")
		 (dot-prin1 (cdr li))
		 )
	     (princ ")")
	     )
	   )
	)
  )

(defun tester (li)
  (format t "~&~s: " li)
  (dot-prin1 li)
  (terpri)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(write-line "testing (dot-prin1)")
(tester nil)
(tester 'a)
(tester '(a))
(tester '(((a))))
(tester '(a b))
(tester '(a b c))
(tester '(a (b) c))
(tester '(((a) b) c))
(tester '(a . b))
