(defun factorial (n)
  (if (= n 0) 1
      (* n (factorial (- n 1)))))

(defun main ()
  (loop for i from 0 to 5
    do (format t "~&~d! ~3d~%" i (factorial i))
    )
    (EXT:EXIT)
)

(main)
