#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

;;(defvar args (format nil "~&~a~%" *args*))
(write-line "format + t means stream output")
(write-line "format + nil means string output")
(defvar args (format nil  "~&~a~%" (cdr *posix-argv*)))
(format t "~&Command line arguments: ~a~%" args)
