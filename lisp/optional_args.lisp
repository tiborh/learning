#!/usr/bin/env clisp

(defun f (one two &optional (three 3) (four 4))
  (format t "~&~4tone: ~s,~%~4ttwo: ~s,~%~4tthree: ~s,~%~4tfour: ~s;~%" one two three four)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&f Called with two args:~%")
(f 5 6)
(format t "~&f Called with four args:~%")
(f 5 6 7 8)
