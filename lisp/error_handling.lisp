#!/usr/bin/env -S sbcl --script
;; from
;; https://www.tutorialspoint.com/lisp/lisp_error_handling.htm
;; Second and third cases had to be commented out due to warnings:
;; WARNING: RESTART-CASE: restart cannot be invoked interactively because it is
;;          missing a :INTERACTIVE option: (RETURN-VALUE (R) R)
;; WARNING: RESTART-CASE: restart cannot be invoked interactively because it is
;;          missing a :INTERACTIVE option:
;;          (RECALC-USING (D) (DIVISION-FUNCTION VALUE1 D))
;; with cl-launch the situation is more catastrophic (after long-long error message:
;; compilation unit aborted
;;   caught 2 fatal ERROR conditions
;;   caught 1 ERROR condition

(define-condition on-division-by-zero (error)
   ((message :initarg :message :reader message))
)
   
(defun handle-infinity ()
   (restart-case
      (let ((result 0))
         (setf result (division-function 10 0))
         (format t "Value: ~a~%" result)
      )
      (just-continue () nil)
   )
)
     
(defun division-function (value1 value2)
  ;;(format t "~&value1: ~a; value2: ~a~%" value1 value2)
   (restart-case
      (if (/= value2 0)
         (/ value1 value2)
         (error 'on-division-by-zero :message "denominator is zero")
      )

      (return-zero () 0)
      (return-value (r) r)
      (recalc-using (d) (division-function value1 d))
   )
)

(defun high-level-code ()
   (handler-bind
      (
         (on-division-by-zero
            #'(lambda (c)
               (format t "error signaled: ~a~%" (message c))
               (invoke-restart 'return-zero)
            )
         )
         (handle-infinity)
      )
   )
)

(handler-bind
   (
      (on-division-by-zero
         #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'return-value 0)
         )
      )
   )
   (handle-infinity)
)

(handler-bind
   (
      (on-division-by-zero
         #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'recalc-using 1d-307)
         )
      )
   )
   (handle-infinity)
)

(handler-bind
   (
      (on-division-by-zero
         #'(lambda (c)
            (format t "error signaled: ~a~%" (message c))
            (invoke-restart 'just-continue)
         )
      )
   )
   (handle-infinity)
)

(format t "~&Done.~%")
