#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun iter-nth (n li)
  (do ((i 0 (1+ i))
       (inli li (cdr inli)))
    ((= i n)(car inli))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (iter-nth 0 nil)))
(assert (= 0 (iter-nth 0 '(0))))
(assert (null (iter-nth 1 '(0))))
(assert (= 5 (iter-nth 5 '(0 1 2 3 4 5))))
