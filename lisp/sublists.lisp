#!/usr/bin/env clisp

(defun sublists (li)
  (cond ((null li) nil)
      (t (cons li (sublists (cdr li))))
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~a~%" (sublists *args*))
  )
