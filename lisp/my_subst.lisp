#!/usr/bin/env clisp

(load "subst_common.lisp")

(defun my-subst (to-x from-y in-z)
  (cond ((equal in-z from-y) to-x)
	((atom in-z) in-z)
	(t (cons (my-subst to-x from-y (car in-z))
		 (my-subst to-x from-y (cdr in-z))
	       )
	   )
	)
  )

(defun my-subst-if  (to-x func in-z)
  (cond ((funcall func in-z) to-x)
	((atom in-z) in-z)
	(t (cons (my-subst-if to-x func (car in-z))
		 (my-subst-if to-x func (cdr in-z))
	       )
	   )
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&original data~%~a~%" *a-tree*)
(format t "~&subst 1 with 0:~%~a~%" (my-subst 0 1 *a-tree*))
(format t "~&substitute numbers less than 5 with nil:~%~a~%"
	(my-subst-if nil #'helper-func *a-tree*))
