#!/usr/bin/env clisp

(defun odds-of (a-list)
  (remove-if-not #'oddp a-list)
  )

(defun evens-of (a-list)
  (remove-if-not #'evenp a-list)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a '(0 1 2 3 4 5 6 7 8 9))
       (b (odds-of a))
       (c (evens-of a))
       )
  (format t "~&original list:      ~a~%" a)
  (format t "~&filtered for odds:  ~a~%" b)
  (format t "~&filtered for evens: ~a~%" c)
  )
