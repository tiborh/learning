#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun binary-rand ()
  (random 2 (make-random-state t))
)

;; (load "dtrace.generic")
;; (dtrace func)

(when (= 2 (length *args*))
  (let ((cols (parse-integer (car *args*)))
	(rows (parse-integer (cadr *args*)))
	)
    (dotimes (i rows)
      (dotimes (j cols)
	(if (= 0 (binary-rand))
	    (princ "/")
	    (princ "\\")
	    )
	)
      (terpri)
      )
    )
    (princ "Not a very good one, but is said to work on old Commodore.")(terpri)
  )

