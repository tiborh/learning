#!/usr/bin/env clisp

(defun add-nums (n)
  (cond ((= 0 n) 0)
	(t (+ n (add-nums (1- n))))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~a~%" (add-nums (parse-integer (car *args*))))
  )
