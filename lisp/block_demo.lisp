#!/usr/bin/env clisp

;; source: https://www.tutorialspoint.com/lisp/lisp_loops.htm

(defun demo-function (flag)
   (print 'entering-outer-block)
   
   (block outer-block
      (print 'entering-inner-block)
      (print (block inner-block

         (if flag
            (return-from outer-block 3)
            (return-from inner-block 5)
         )

         (print 'This-wil--not-be-printed))
      )

      (print 'left-inner-block)
      (print 'leaving-outer-block)
   t)
)

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&Going with t~%")
(demo-function t)
(terpri)
(format t "~&Going with nil~%")
(demo-function nil)
