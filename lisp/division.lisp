#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun divide (dividend divisor)
  (if (= 0 divisor)
      (error "Error: divisor cannot be zero")
      (/ dividend divisor)
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

 (if (null *args*)
     (write-line "usage: <prog> dividend divisor")
     (format t "~&~a / ~a == ~a" (car *args*) (cadr *args*)
	     (divide (parse-integer (car *args*))
		     (parse-integer (cadr *args*))))
     )
		       
