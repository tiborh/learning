#!/usr/bin/env clisp

(load "list_merge_fun.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((base 'merge-lists)
       (a '(list 1 2 6 8 10 12))
       (b '(list 2 3 5 9 13))
       (c (cons base (cons a (cons b nil))))
       (d (cons base '(nil nil)))
       (e (cons base '('(1 2 3) nil)))
       (f (cons base '(nil '(1 2 3))))
       (g (cons base '('(1 2 3) '(1 2 3))))
       )
    (format t "~&~a => ~a~%" d (eval d))
    (format t "~&~a => ~a~%" e (eval e))
    (format t "~&~a => ~a~%" f (eval f))
    (format t "~&~a => ~a~%" g (eval g))
    (format t "~&~a => ~a~%" c (eval c))
    )
