#!/usr/bin/env -S sbcl --script

;; based on:
;; https://www.tutorialspoint.com/lisp/lisp_clos.htm

;; this way: there is no way to access members:
;; (defclass Box () 
;;    (length 
;;    breadth 
;;    height)
;; )

(defclass Box ()
  ((length :initarg :length
	   :initform 1
	   :accessor box-length)
   (breadth :initarg :breadth
	    :initform 2
	    :accessor box-breadth)
   (height :initarg :height
	   :initform 3
	   :accessor box-height)
   (volume :reader volume)
   (to-string  :reader to-string)
   )
  )

(defmethod volume ((object box))
   (* (box-length object) (box-breadth object)(box-height object))
)

(defmethod to-string ((object box))
  (format nil "L: ~d, B: ~d, H: ~d (V: ~d)" (box-length object) (box-breadth object)(box-height object)(volume object))
  )

(defclass Wooden-box (Box)
  ((price :initarg :price
	  :accessor box-price)))
(defmethod to-string ((object wooden-box))
  (format nil "L: ~d, B: ~d, H: ~d (V: ~d) (price: ~d)" (box-length object) (box-breadth object)(box-height object)(volume object)(box-price object))
  )


(setf a_box (make-instance 'Box
			   :length 5
			   :breadth 10
			   :height 7))
(format t "~&a_box: ~s~%" (to-string a_box))

(setf b_box (make-instance 'box))
(format t "~&b_box: ~s~%" (to-string b_box))

(defvar w_box)
(setf w_box (make-instance 'Wooden-box
			   :length 5
			   :breadth 10
			   :height 7
			   :price 100))
(format t "~&w_box: ~s~%" (to-string w_box))
(setf (box-price w_box) 200)
(format t "~&price: ~s~%" (box-price w_box))

(format t "~&終~%")
