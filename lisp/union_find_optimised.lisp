#!/usr/bin/env -S sbcl --script
(defconstant +args+ (cdr *posix-argv*))
;; (load "my-utils.lisp") ; alternatively

(defstruct node
  parent
  )

(defun uf-union (node1 node2)
  "add node 1 to node2 as sibling or child"
  (setf (node-parent node1) (or (node-parent point2) point2))
  )

(defun uf-find (node)
  "top node of tree 'node' belongs to"
  (let ((parent (node-parent node)))
    (if parent
	(setf (node-parent node) (uf-find parent))
	node
	)
    )
  )

(format t "~&終~%")
