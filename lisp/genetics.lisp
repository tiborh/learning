#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "genetics_lib.lisp")

(defun test-genetics ()
  (assert (eq (complement-base 'a) 't))
  (assert (eq (complement-base 't) 'a))
  (assert (eq (complement-base 'g) 'c))
  (assert (eq (complement-base 'c) 'g))
  (assert (equal (complement-strand '(a g g t)) '(t c c a)))
  (assert (equal (make-double '(g g a c t)) '((g c)(g c)(a t)(c g)(t a))))
  (assert (equal (count-bases '((g c)(a t)(t a)(t a)(c g))) '((a 3)(t 3)(g 2)(c 2))))
  (assert (equal (count-bases (make-double '(g a t t c))) '((a 3)(t 3)(g 2)(c 2))))
  (assert (equal (count-bases '(a g t a c t c t)) '((a 2)(t 3)(g 1)(c 2))))
  (assert (not (make-complementer nil)))
  (assert (equal (make-complementer '(a)) '(t)))
  (assert (equal (make-complementer '(a t g c)) '(t a c g)))
  (assert (equal (make-complementer '(a t g c c g t a)) '(t a c g g c a t)))
  (assert (prefixp '(g t c) '(g t c a t)))
  (assert (prefixp '(g t c) '(g t c )))
  (assert (prefixp nil '(g t c )))
  (assert (prefixp nil nil))
  (assert (not (prefixp '(g t c) '(t c a g t c))))
  (assert (not (prefixp '(g t c a) '(g t c))))
  (assert (appearsp nil nil))
  (assert (appearsp '(a) '(a)))
  (assert (appearsp nil '(a)))
  (assert (not (appearsp '(a) nil)))
  (assert (appearsp '(g t c) '(g t c)))
  (assert (appearsp '(g t c) '(g t c a t)))
  (assert (appearsp '(c a t) '(t c a t g)))
  (assert (not (appearsp '(c a t) '(c a c g t))))
  (assert (not (appearsp '(c a t) '(t c c g t c a))))
  (assert (coverp nil nil))
  (assert (not (coverp nil '(a))))
  (assert (coverp '(a g c) '(a g c a g c a g c)))
  (assert (not (coverp '(a g c) '(a g c t t g))))
  (assert (equal '(a) (prefix 1 '(a))))
  (assert (equal (prefix 4 '(c g a t t a g)) '(c g a t)))
  (assert (equal (prefix 4 '(c g a)) '(c g a)))
  (assert (not (prefix 0 '(c g a))))
  (assert (equal (prefix 1 '(c g a)) '(c)))
  (assert (equal (kernel '(a a a a a)) '(a)))
  (assert (equal (kernel '(a b c a b c a b c)) '(a b c)))
  (assert (equal (kernel '(a g g t c)) '(a g g t c)))
  (assert (validate-input '(a t g c)))
  (assert (validate-input '(a t g c a t g c)))
  (assert (validate-input nil))
  (assert (not (validate-input '(a t g c d))))
  (assert (not (validate-input '(z))))
  (assert (not (process-args nil)))
  (assert (equal (process-args '("a")) '(a)))
  (assert (equal (process-args '("a" "t" "g" "c")) '(a t g c)))
  (write-line "Tests have passed")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-genetics)
    (let ((strand (process-args *args*)))
      (if (validate-input strand)
	  (draw-dna strand)
	  (write-line "Invalid genetical sequence.")
	  )
      )
    )
