#!/usr/bin/env -S sbcl --script

(defconstant *args* (cdr *posix-argv*))

;; (load "dtrace.generic")
;; (dtrace func)

;; (princ *posix-argv*)
;; (terpri)

(format t "~&*posix-argv*: ~a~%" *posix-argv*)
(format t "~&args only: ~a~%" *args*)
