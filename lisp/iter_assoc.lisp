#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun it-assoc (k li)
  (dolist (it li)
    (when (equal k (car it))
      (return it)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (it-assoc 'a nil)))
(assert (equal '(a b c d) (it-assoc 'a '((a b c d)))))
(assert (null (it-assoc 'b '((a b c d)))))
(let ((c (pairlis '(a b c d) '(1 2 3 4))))
  (assert (equal '(b . 2) (assoc 'b c)))
  )
