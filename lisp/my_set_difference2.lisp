#!/usr/bin/env clisp

(load "my_funcs.lisp")

(defun my-set-diff (l1 l2 liu)
  (cond ((null l1) liu)
	((not (my-member (car l1) l2))
	  (my-set-diff (cdr l1) l2 (append liu (list (car l1))))
	 )
	(t (my-set-diff (cdr l1) l2 liu))
	)
  )

(defun my-set-difference (l1 l2)
  (my-set-diff l1 l2 nil)
  )
	      
(load "dtrace.generic")
(dtrace my-set-diff)

(let ((cmdline '(my-set-difference '(1 2 3 4 5) '(4 5 6 7 8)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
