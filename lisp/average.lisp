#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun average (&rest nums &aux (len (length nums)))
  (/ (apply #'+ nums) len 1.0)
  )

(defun make-num-list (args)
  (let ((li nil)
	(it nil)
	)
    (dolist (it args)
      (setf it (read-from-string it))
      (when (numberp it)
	(push it li)
	)
      )
    (reverse li)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let ((numlist (make-num-list *args*)))
    (format t "~&~a~%" numlist)
    (format t "~&average: ~a~%" (apply #'average numlist))
    )
  )
