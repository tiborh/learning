#!/usr/bin/env clisp

(defun f (n a b)
  (cond ((= n 1) b)
	(t (f (- n 1) (+ a b) a))
	)
  )

(defun tester (n)
  (when (< n 1)
    (format t "~&~a is not a positive number~%" n)
    (EXT:EXIT)
    )
  )

(defun filter-fun (args)
  (let ((n (parse-integer (first  args)))
	(a (parse-integer (second args)))
	(b (parse-integer (third  args)))
	)
    (tester n)
    (tester a)
    (tester b)
    (f n a b)
    )
  )

(load "dtrace.generic")
(dtrace f)

(cond ((null *args*) (format t "~&No args.~%"))
      ((< (length *args*) 3) (format t "~&Three integers are needed.~%"))
      (t (format t "~&Result: ~a~%" (filter-fun *args*)))
    )
