#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let ((v1 #(一 二 三 四 五))) 		; also (vector ...)
  (format t "~&A vector: ~a (~a)~%" v1 (type-of v1))
  (loop for i across v1
     do (princ i)(terpri)
       )
  )

(let ((v1 (make-array 2 :fill-pointer 0)))
  (format t "~&~s~%" v1)
  (vector-push "one" v1)
  (format t "~&~s~%" v1)
  (vector-push "two" v1)
  (format t "~&~s~%" v1)
  (vector-push "three" v1)
  (format t "~&~s~%" v1)		; no change
  (vector-pop v1)
  (format t "~&~s~%" v1)
  (vector-push "three" v1)
  (format t "~&~s~%" v1)
  )

(let ((v1 (make-array 2 :fill-pointer 0 :adjustable t)))
  (format t "~&~s~%" v1)
  (vector-push "one" v1)
  (format t "~&~s~%" v1)
  (vector-push "two" v1)
  (format t "~&~s~%" v1)
  (vector-push "three" v1)
  (format t "~&~s~%" v1)		; no change
  (adjust-array v1 3)
  (vector-push "three" v1)
  (format t "~&~s~%" v1)
  )


(write-line "for more, see arrays")
