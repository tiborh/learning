#!/usr/bin/env clisp

(defun get-num (num)
  (let ((numbers '((0 .  零)
		   (1 .  一)
		   (2 .  二)
		   (3 .  三)
		   (4 .  四)
		   (5 .  五)
		   (6 .  六)
		   (7 .  七)
		   (8 .  八)
		   (9 .  九)
		   (10 . 十))
	  ))
    (cdr (assoc num numbers))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(loop for i from 0 to 10
     do (princ (get-num i))
     )
