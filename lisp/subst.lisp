#!/usr/bin/env clisp

;; subst and subst-if are for tree substitutions.
;; substitute and substitute-if are for simple lists

(load "subst_common.lisp")

;; (load "dtrace.generic")
;; (dtrace my-subst-if)

(format t "~&original data:~%~a~%" *a-tree*)
(format t "~&subst 1 with 0:~%~a~%" (subst 0 1 *a-tree*))
(format t "~&substitute numbers less than 5 with nil:~%~a~%"
	(subst-if nil #'helper-func *a-tree*))
