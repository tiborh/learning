#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun concat-args(args &optional (sep " "))
  (if (NULL args)
      ""
      (concatenate 'string (car args) sep (concat-args (cdr args)))
  )
  )

(let ((res (concat-args *args*)))
  (format t "~&concat result: ~s~%" res)
  (format t "~&concat result: ~a~%" res)
)

(format t "~&終~%")
