#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun find-index (it li)
  (loop for i from 0 to (1- (length li))
     do (when (equal (nth i li) it)
	  (return-from find-index i)
	  )
       )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (= 1 (find-index 'a '(b a c d))))
(assert (= 0 (find-index 'a '(a b c d))))
(assert (= 3 (find-index 'a '(b c d a))))
