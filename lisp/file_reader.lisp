#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;;from the example of Section 9.11 in Touretzky's book

(defun read-all-objects (fh eof-indicator)
  "only regular lisp objects can be read this way, that is, file content that conforms to lisp syntax"
  (let ((result (read fh nil eof-indicator)))
    (if (eq result eof-indicator)
	nil
	(cons result (read-all-objects fh eof-indicator))
	)
    )
  )

(defun read-my-file (fn)
  (with-open-file (fh fn)
    (let ((file-content (read-all-objects fh (list '$eof$))))
      (format t "~&Number of objects read from the file: ~s." (length file-content))
      file-content)
    )
  )


;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~s~%" (read-my-file (car *args*)))
  )
