#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defstruct starship
  (name nil)
  (captain nil)
  )

(defstruct captain
  (name nil)
  (ship nil)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(setq enterprise (make-starship :name 'enterprise))
(setq kirk (make-captain :name 'kirk :ship enterprise))
(setf (starship-captain enterprise) kirk)

(write-line "Mutual reference in two structures results in stack overflow:")
(print enterprise)
