#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun iter-union (l0 l1)
  (let ((li (reverse l0)))
    (dolist (it l1 (nreverse li))
      (push it li)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((l0 nil)
      (l1 '(1))
      (l2 '(0 2)))
  (assert (null (iter-union l0 l0)))
  (assert (equal '(1) (iter-union l0 l1)))
  (assert (equal '(1) (iter-union l1 l0)))
  (assert (equal '(1 0 2) (iter-union l1 l2)))
  (assert (equal '(0 2 1) (iter-union l2 l1)))
  )
