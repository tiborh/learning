#!/usr/bin/env clisp

(load "number_players_host.lisp")
(load "number_players_guesser.lisp")

(defun game-controller (gameNum maxNum)
  (dotimes (i gameNum)
    (format t "~&~%Game number ~d~%" (+ i 1))
    (guessing-controller maxNum)
    )
  )

(defun proc-cli-arg (args)
  (let ((gameN (parse-integer (first args)))
	(maxN (parse-integer (second args)))
	)
    (when (< gameN 1)
      (format t "~&Number of games must be a positive number.~%")
      (EXT:EXIT)
      )
    (when (< maxN 1)
      (format t "~&Maximum number must be a positive number.~%")
      (EXT:EXIT)
      )
    (game-controller gameN maxN)
    )
  )

(defun abort-games ()
  (format t "~&Number of games and maxnum need to be given.~%")
  (EXT:EXIT)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(cond ((null *args*) (abort-games))
      ((< (length *args*) 2) (abort-games))
      (t (proc-cli-arg *args*))
      )

(format t "~&End of Games.~%")
