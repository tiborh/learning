#!/usr/bin/env clisp

(defun find-first-atom (x)
  (cond ((atom x) x)
	(t (find-first-atom (car x)))
	)
  )

(defun test-find-first-atom ()
  (assert (not (find-first-atom nil)))
  (assert (find-first-atom T))
  (assert (string= "first" (find-first-atom "first")))
  (assert (string= "first" (find-first-atom '((("first"))))))
  (assert (string= "first" (find-first-atom '((("first" "second") ("third" "fourth")) ("fifth" "sixth")))))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(test-find-first-atom)
