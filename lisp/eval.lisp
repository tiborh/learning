#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(setq form '(+ 1 1))
(format t "~a == ~a" form (eval form))
