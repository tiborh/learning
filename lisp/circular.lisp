(setq a #1='(A B C . #1#))

(load "sdraw.generic")

(sdraw a)

(format t "~&length of a: ~a~%" (length a))
