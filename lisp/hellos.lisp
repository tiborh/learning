#!/usr/bin/env -S sbcl --script

(defconstant *args* (cdr *posix-argv*))

(load "hello_func.lisp")

;; a comment
(if *args*
    (hellos (cdr *posix-argv*))
    (hello)
    )
