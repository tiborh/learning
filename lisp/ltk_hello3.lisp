#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "ltk/ltk")

(in-package :ltk)

(defun hello-3()
  (with-ltk ()
    (let* ((topf (make-instance 'toplevel
				    :class 'frame
				    :title "Hello!"
			     )
	     )
	   (l1 (make-instance 'label
			      :master topf
			      :text "Hello, World!"
			      :anchor :n
			      :width 30
			      :padding 20)
	     )
          (b1 (make-instance 'button
                             :master topf
                             :text "OK"
                             :command (lambda ()
					(format t "OK. Bye!~&")
					(setf *exit-mainloop* t))))
          (b2 (make-instance 'button
                             :master topf
                             :text "Cancel"
                             :command (lambda () (withdraw topf)))))
     (pack l1 :side :top)
     ;; side by side
     (pack b1 :side :left)
     (pack b2 :side :right)
     (configure topf :borderwidth 3)
     (configure topf :relief :raised)
     ;;     (wm-title topf "the title") ; another way to assign title to toplevel
     (minsize topf 300 100)
     )))

;; (load "dtrace.generic")
;; (dtrace func)

(hello-3)
