#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun read-a-num ()
  (write-line "Enter a number:")
  (do ((val (read)(read)))
      ((numberp val) val)
    (format t "~&Invalid number: ~a, please enter a valid one:~%" val)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(write-line "Infinite loop till a valid number is entered:")
(format t "~&num: ~a~%" (read-a-num))
