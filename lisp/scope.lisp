#!/usr/bin/env clisp

(defvar a "one")

(defun f1 ()
  (let ((b "two"))
    (format t "~&f1:~%a: ~s~%b: ~s~%" a b)
    )
  )

(defun f2 (a)
  (let* ((b "three")
	 (f3 (lambda () (format t "~&f3:~%a: ~s~%b: ~s~%" a b)))
	 )
    (f1)				; uses f2-local a, let*-local b
    (funcall f3)			; uses f2-local a, let*-local b
    )
  )

(f1)					; uses global a, local b
(f2 "four")
