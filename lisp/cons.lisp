#!/usr/bin/env clisp

(load "sdraw.generic")

(format t "~&Consing two symbols:~%")
(sdraw (cons 'a 'b))
(format t "~&Consing two symbols and nil:~%")
(sdraw (cons 'a (cons 'b nil)))
(let* ((a (list 0 1))
       (b (list 2 3))
       (c (cons a b))
       (d (cons a (cons b nil)))
      )
  (format t "~&Consing two lists:~%")
  (sdraw c)
  (format t "~& car of c: ~s~%" (car c))
  (format t "~& caar of c: ~s~%" (caar c))
  (format t "~& cdar of c: ~s~%" (cdar c))
  (format t "~& cadar of c: ~s~%" (cadar c))
  (format t "~& cdr of c: ~s~%" (cdr c))
  (format t "~& cadr of c: ~s~%" (cadr c))
  (format t "~& cddr of c: ~s~%" (cddr c))
  (format t "~& caddr of c: ~s~%" (caddr c))
  
  (format t "~&Consing two lists and nil:~%")
  (sdraw d)
  (format t "~& cdr of d: ~s~%" (cdr d))
  (format t "~& cadr of d: ~s~%" (cadr d))
  (format t "~& caadr of d: ~s~%" (caadr d))
  (format t "~& cadadr of d: ~s~%" (cadadr d))
  )

;; (load "dtrace.generic")
;; (dtrace func)

