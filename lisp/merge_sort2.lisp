#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;#!/usr/bin/env clisp
;; #!/usr/local/bin/sbcl --script
;; (defconstant *args* (cdr *posix-argv*))

(load "list_merge_fun.lisp")
(load "random_list_fun.lisp")

;; 'half' idea from http://www.codecodex.com/wiki/Merge_sort#Common_Lisp
;; merge_sort.lisp seems to be faster
(defun merge-sort (li)
  (cond ((null li) nil)
	((= (length li) 1) (list (car li)))
	(t (let ((half (truncate (/ (length li) 2))))
	     (merge-lists (merge-sort (subseq li 0 half))
			(merge-sort (subseq li half))
			)
	     )
	   )
	)
  )

;; (load "dtrace.generic")
;; (dtrace merge-sort)


(let ((li-len 10)
      (ceil 100)
      (li nil)
      (li-sorted nil)
      )
  (if (> (length *args*) 0)
      (setf li-len (parse-integer (car *args*)))
      )
  (if (> (length *args*) 1)
      (setf ceil (parse-integer (cadr *args*)))
      )
  (setf li (make-rand-list li li-len ceil t))
  (format t "~&list: ~a~%" li)
  (setf li-sorted (merge-sort li))
  (format t "~&sorted list: ~a~%" li-sorted)
  )

(princ "終")(terpri)
