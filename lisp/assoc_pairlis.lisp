#!/usr/bin/env clisp

(defvar a (reverse (pairlis '(a b c d) '(1 2 3 4))))

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&The pair list: ~a~%" a)
(format t "~&(assoc 'a a): ~a~%" (assoc 'a a))
(format t "~&value of the pair: ~a~%" (cdr (assoc 'a a)))
