#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun read-args (args)
  (if (null args)
      (return-from read-args nil)
      (format t "~&normal: ~s; read-from-string: ~s~%" (car args) (read-from-string (car args)))
      )
  (read-args (cdr args))
  )
       
(read-args *args*)

(format t "~&終~%")
