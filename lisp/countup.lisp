#!/usr/bin/env clisp

(defun countup-recurse (n c)
  (cond ((< n c) nil)
	(t (cons c (countup-recurse n (1+ c))))
	)
  )

(defun countup (n &optional (c 0))
  (countup-recurse n c)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (if (= 2 (length *args*))
      (format t "~&~a~%" (countup (parse-integer (car *args*)) (parse-integer (cadr *args*))))
      (format t "~&~a~%" (countup (parse-integer (car *args*))))
      )
  )
