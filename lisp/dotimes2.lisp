#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun loop0 (num)
  (let ((sum 0))
    (dotimes (ind num)
      (setf sum (+ sum ind))
      )
    )
  )

(defun loop1 (num)
  (let ((sum 0))
    (dotimes (ind num sum)
      (setf sum (+ sum ind))
      )
    )
  )

(defun loop2 (num)
  (let ((sum 0))
    (dotimes (ind num)
      (setf sum (+ sum ind))
      )
    (return-from loop2 sum)
    )
  )

(defun loop3 (num)
  (let ((sum 0))
    (dotimes (ind num)
      (setf sum (+ sum ind))
      (when (= (1+ ind) num)
	(return sum)
	)
      )
    )
  )


;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&By default, a loop returns nil: ~a~%" (loop0 5))
(format t "~&Third parameter can collect return value: ~a~%" (loop1 5))
(format t "~&Same achieved with return-from (from inside the function): ~a~%" (loop2 5))
(format t "~&Same achieved with return (from inside the loop): ~a~%" (loop3 5))
