#!/usr/bin/env clisp

(load "tree_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace tree-depth)

(let* ((a '('((a b (r)) a c (a d ((a (b) r) a)))))
       (b 'tree-depth)
       (c (cons b a))
       (d (cons b '('(a . b))))
       (d1 (cons b '('(((a))))))
       (e (cons b '('(a b c d))))
       (f (cons b '('((a . b) . (c . d)))))
      )
  (format t "~&~a: ~a~%" d (eval d))
  (format t "~&~a: ~a~%" d1 (eval d1))
  (format t "~&~a: ~a~%" e (eval e))
  (format t "~&~a: ~a~%" f (eval f))
  (format t "~&~a: ~a~%" c (eval c))
  )

