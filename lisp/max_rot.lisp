#!/usr/bin/env clisp

(load "digit_funcs.lisp")

(defvar rotvals (list))

(defun sub-rot (fn ln)
  (cond ((< ln 10) (push-digit fn ln))
	((< ln 100) (unshift-digit fn (first-to-last ln)))
	(t (setq ln (first-to-last ln))
	   (push (unshift-digit fn ln) rotvals)
	   (sub-rot (push-digit fn (first-digit ln)) (rest-digits ln))
	   )
	)
  )

(defun max-rot (n)
  (setq rotvals nil)
  (push n rotvals)
  (push (sub-rot 0 n) rotvals)
  (apply 'max rotvals)
  )

(defun test-max-rot ()
  (assert (= (max-rot 1) 1))
  (assert (= (max-rot 74) 74))
  (assert (= (max-rot 15) 51))
  (assert (= (max-rot 150) 501))
  (assert (= (max-rot 38458215) 85821534))
  (assert (= (max-rot 195881031) 988103115))
  (assert (= (max-rot 896219342) 962193428))
  (assert (= (max-rot 69418307) 94183076))
  (write-line "Tests have passed")
  )

;; (load "dtrace.generic")
;; (dtrace func)

;;(format t "~&: ~d~%" (last-to-first (parse-integer (first *args*))))

(if (null *args*)
    (progn
      (test-max-rot)
      (write-line "A positive integer arg is needed.")
      )
    (progn
      (format t "~&The maximum value: ~d~%" (max-rot (parse-integer (first *args*))))
      (format t "~&The list: ~a~%" rotvals)
      (format t "~&The sorted list: ~a~%" (sort rotvals #'<))
      )
    )
