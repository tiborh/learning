#!/usr/bin/env clisp

(load "my_funcs.lisp")

(defun my-intersection (l1 l2)
  (cond ((null l1) nil)
	((my-member (car l1) l2)
	 (cons (car l1) (my-intersection (cdr l1) l2))
	 )
	(t (my-intersection (cdr l1) l2))
	)
  )
	      
;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(my-intersection '(1 2 3 4 5) '(4 5 6 7 8)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
