#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun launch (n)
  (do ((l n (1- l)))
      ((< l 0)(format t "liftoff, it's a liftoff!"))
    (format t "~a..." l)
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(when *args*
  (launch (parse-integer (car *args*)))
  )
