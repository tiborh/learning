#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

;; (load "~/.sbclrc")
;; (ql:quickload :cl-ppcre :silent t)

;; (defun usage()
;;   (let ((file-ext (car (last (cl-ppcre::split "\\." (native-namestring *load-truename*)))))
;; 	(file-name (pathname-name *load-truename*))
;; 	)
;;     (format t "~&Usage:~&~c~a.~a day month year~%" #\tab file-name file-ext)
;;     )
;;   )

;; not used. source: http://cl-cookbook.sourceforge.net/dates_and_times.html
(defun day-of-week-calc (day month year)
  "Returns the day of the week as an integer.
   Sunday is 0. Works for years after 1752."
  (let ((offset '(0 3 2 5 0 3 5 1 4 6 2 4)))
    (when (< month 3)
      (decf year 1))
    (mod
     (truncate (+ year
                  (/ year 4)
                  (/ (- year)
                     100)
                  (/ year 400)
                  (nth (1- month) offset)
                  day
                  -1))
     7)
    )
  )

(defun day-of-week (day month year)
  "Return the day of the week given a DAY MONTH YEAR"
  (if (> 1900 year)
      -1
      (if (and (= year 1900) (= day 1))
	  -1
	  (nth 6 (multiple-value-list (decode-universal-time (encode-universal-time 0 0 0 day month year))))
	  )
      )
  )

(defun give-day(day-num)
  ;;(format t "~&~s~%" day-num)
  (case day-num
    (-1 "Cannot be used for dates before 2nd January 1900")
    (0 "Monday")
    (1 "Tuesday")
    (2 "Wednesday")
    (3 "Thursday")
    (4 "Friday")
    (5 "Saturday")
    (6 "Sunday")
    )
  )

;;or:
;;#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

;;(format t "~&~s~%" *load-truename*)

(if (> 3 (length *args*))
    (progn
      (load "mod_usage.lisp")
      (usage '("day" "month" "year"))
      )
    (format t "~&~a~%" (give-day (day-of-week (parse-integer (car *args*))
					      (parse-integer (cadr *args*))
					      (parse-integer (caddr *args*))
					      )
				 )
	    )
    )

(format t "~&終~%")
