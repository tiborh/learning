#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "iter_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(assert (= 0 (iter-length nil)))
(assert (= 1 (iter-length '(t))))
(assert (= 5 (iter-length '(1 2 3 4 5))))
