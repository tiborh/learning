#!/usr/bin/env clisp

(defun my-remove-if (func li)
  (cond ((null li) nil)
	((not (funcall func (car li)))
	 (cons (car li) (my-remove-if func (cdr li)))
	 )
	(t (my-remove-if func (cdr li)))
	)
  )

(defun my-remove-if-not (func li)
  (cond ((null li) nil)
	((funcall func (car li))
	 (cons (car li) (my-remove-if-not func (cdr li)))
	 )
	(t (my-remove-if-not func (cdr li)))
	)
  )


;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline1 '(my-remove-if #'oddp '(1 2 3 4 5 6 7)))
      (cmdline2 '(my-remove-if-not #'oddp '(1 2 3 4 5 6 7)))
      )
  (format t "~&~a: ~a~%" cmdline1 (eval cmdline1))
  (format t "~&~a: ~a~%" cmdline2 (eval cmdline2))
  )
