#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

;; source: https://www.tutorialspoint.com/lisp/lisp_do.htm
(defun approach-loop (a b)
  (do ((x a (+ 2 x))
       (y b (- y 2))
       )
      ((> x y)(- x y)) 			; return condition and result
    (format t "~& x = ~2d  y = ~2d~%" x y) ;when condition is fulfilled, this line is not reached.
    )
  )

;; variable, starting-value, value-update-procedure

(let ((a 0)
      (b 20)
      )
  (format t "~&a == ~d; b == ~d; result: ~a~%" a b (approach-loop a b))
  )
