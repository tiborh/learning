#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun find-largest (li-of-nums)
  (do* ((largest (car li-of-nums))
	(li (cdr li-of-nums) (cdr li))
	(it (car li) (car li))
	)
       ((null li) largest)
    (when (> it largest)
      (setf largest it)
      )
    )
  )

(defun test-find-largest ()
  (assert (null (find-largest nil)))
  (assert (= 0 (find-largest '(0))))
  (assert (= 3 (find-largest '(3 2 1 0))))
  (assert (= 3 (find-largest '(0 1 2 3))))
  (assert (= 3 (find-largest '(0 1 2 3 2 1 0))))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-find-largest)
    (format t "~&Largest: ~a~%" (find-largest (mapcar #'parse-integer *args*)))
    )
