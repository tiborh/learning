#!/usr/bin/env clisp

(defvar ROLLS 1000)
(defvar SIDES 6)

(unless (null *args*)
  (setq ROLLS (parse-integer (first *args*) :junk-allowed t))
  (unless (null (rest *args*))
    (setq SIDES (parse-integer (second *args*) :junk-allowed t))
    )
  )

(setq cast-collect (make-hash-table))
(dotimes (i SIDES)
  (setf (gethash i cast-collect) 0)
)

(dotimes (i ROLLS)
  (let ((points (random SIDES (make-random-state t))))
    (setf (gethash points cast-collect) (+ (gethash points cast-collect) 1))
    )
  )

;; (format t "~&~a~%" cast-collect)
(maphash #'(lambda (k v) (format t "~a => ~a (~f%)~%" (+ k 1) v (* (/ v ROLLS) 100))) cast-collect)

