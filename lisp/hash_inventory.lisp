#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "hash_funcs.lisp")

(defun create-inventory (li)
  (let ((inv (make-hash-table)))
    (dolist (it li)
      (let ((it-symb (intern it)))
	(if (key-exists-p it-symb inv)
	    (setf (gethash it-symb inv) (1+ (gethash it-symb inv)))
	    (setf (gethash it-symb inv) 1)
	    )
	)
      )
      inv
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (pretty-print-hash (create-inventory *args*))
  )
