#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "simple_struct_shared.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(setf a (make-simple))
(setf b (make-simple :id 1))
(format t "~&a: ~a~%" a)
(format t "~&b: ~a~%" b)
(format t "~&(equal a b): ~a~%" (equal a b))
(format t "~&(equalp a b): ~a~%" (equalp a b))
