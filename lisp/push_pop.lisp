#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defvar a (list))
(format t "~&empty list: ~s~%" a)
(push 1 a)
(format t "~&push a number: ~s~%" a)
(push 'b a)
(format t "~&push a symbol: ~s~%" a)
(push #\c a)
(format t "~&push a char: ~s~%" a)
(push "apple" a)
(format t "~&push a string: ~s~%" a)
(defvar b (pop a))
(format t "~&pop the lifo value (and assign it to b): a == ~s, b == ~s~%" a b)
