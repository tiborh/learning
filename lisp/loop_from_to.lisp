#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun usage_instruct ()
  (format t "~&Usage: <script> <from number> <to number>~%")
    )

(when (< (length *args*) 2)
  (usage_instruct)
  (EXT:EXIT)
  )
  
(let ((a (parse-integer (first *args*)))
      (b (parse-integer (second *args*)))
      )
  (when (< b a)
    (format t "~&First number must be less than second.~%") ; Dunno why the limitation
    )
  (loop for i from a to b
     do (format t "~&~a~%" i)
       )
  )
