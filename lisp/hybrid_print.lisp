#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; example from book:
(defun hybrid-princ-car (x)
  (princ "(")
  (hybrid-princ x)
  )

(defun hybrid-princ-cdr (li)
  (cond ((null li) (princ ")"))
	((atom li) (format t " . ~s)" li))
	(t (princ " ")
	   (hybrid-princ (car li))
	   (hybrid-princ-cdr (cdr li))
	   )
	)
  )

(defun hybrid-princ (li)
  (cond ((atom li) (princ li))
	(t (hybrid-princ-car (car li))
	   (hybrid-princ-cdr (cdr li))
	   )
	)
  )

(defun tester (li)
  (format t "~&~s: " li)
  (when li
    (hybrid-princ li)
    (terpri)
    )
  )

;; (load "dtrace.generic")
;; (dtrace hybrid-princ hybrid-princ-car hybrid-princ-cdr)

(write-line "testing (hybrid-princ)")
(tester nil)
(tester '(nil))
(tester 'a)
(tester '(a . nil))
(tester '(a . b))
(tester '(a . (b . nil)))
(tester '(a . (b . c)))
(tester '((a . nil) . (b . (c . d))))
