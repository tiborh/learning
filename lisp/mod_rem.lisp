#!/usr/bin/env clisp

(defun exit-with-message (msg)
  (format t msg)
  (ext:exit)
  )

(defun error-printer (x)
  (format t "~&~a cannot be parsed into number.~%" x)
  )

(defun break-down-error (n m)
  (unless n
    (error-printer "The first argument")
    )
  (unless m
    (error-printer "The second argument")
    )
)

(defun print-error-msg (n m)
  (if (and (not n) (not m))
      (error-printer "The first and second arguments")
      (break-down-error n m)
      )
  (ext:exit)
  )

(defun parse-args (args)
  (let ((n (parse-integer (first args) :junk-allowed t))
	(m (parse-integer (second args) :junk-allowed t))
	)
    (if (and n m)
	(return-from parse-args (list n m (mod n m) (rem n m)))
	(print-error-msg n m)
	)
    )
  )

(defun check-vars (args)
  (if (< (length args) 2)
      (exit-with-message "~&Two arguments are needed.~%")
      (parse-args args)
      )
  )

(defun check-args (args)
  (if (zerop (length args))
      (exit-with-message "~&Usage: <script-name> <arg0> <arg1>~%~targ0: integer~%~targ1: integer (modulus)~%")
      (check-vars *args*)
      )
  )

(let ((results (check-args *args*)))
  (format t "~&~a mod ~a == ~a (rem: ~a)~%"
	  (first results) (second results) (third results) (fourth results))
  )
