#!/usr/bin/env clisp

(load "char_conv_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)
(unless (null *args*)
  (let ((cl (coerce (car *args*) 'list))
	 )
    (format t "~&~a~%" (char-list-to-int-list cl))
    )
  )
