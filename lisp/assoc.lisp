#!/usr/bin/env clisp

(defun get-days-in (lang)
  (let ((days '((en (Monday Tuesday Wednesday Thursday Friday Saturday Sunday))
		(de (Montag Dienstag Mittwoch Donnerstag Freitag Samstag Sonntag))
		(ja (月曜日 火曜日 水曜日 木曜日 金曜日 土曜日 日曜日))
		)
	  )
	)
    (cadr (assoc lang days))
    )
  )

;; (load "dtrace.generic")
;; (dtrace get-days-in)

(format t "~&Days in English:  ~a~%" (get-days-in 'en))
(format t "~&Days in German:   ~a~%" (get-days-in 'de))
(format t "~&Days in Japanese: ~a~%" (get-days-in 'ja))


