#!/usr/bin/env clisp

(defun pairings (li1 li2)
  (cond ((null li1) nil)
      (t (cons (list (car li1) (car li2)) (pairings (cdr li1) (cdr li2))))
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((base 'pairings)
       (a '(list 'a 'b 'c))
       (b '(list 1 2 3))
       (c (list base a b))
       )
  (format t "~&~a~%" c)
  (format t "~&~a~%" (eval c))
  )
