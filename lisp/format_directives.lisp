#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun radix-series (n r)
  (cond ((= r 37) nil)
	(t (progn (format t (concatenate 'string "~&base ~2@a: ~" (write-to-string r) "r~%") r n)
		  (radix-series n (1+ r))
		  )
	   )
	)
  )

(defun radix-based (n)
  (radix-series n 2)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&Dollar is for floats: ~$~%" pi) ; $ to follow monetary conventions
(format t "~&let's try an int: ~$~%" 1)	     ; 1.00
(format t "~&five decimal places: ~5$~%" pi) ; five places after decimal point
(format t "~&same with f requires a comma: ~,5f~%" pi) ; float with five places after decimal point
(format t "~&using v as a prefix, precision number is a post-parameter: ~v$~%" 5 pi) ; 3.14159
(format t "~&using # as a prefix, there is one decimal: ~#$~%" pi) ; 3.1
(format t "~&same with integer: ~#$~%" 1)			   ; 1.0

(let ((sn 1)
      (bn 1000000)
      )
  (format t "~&d is for decimal integers: ~d~%" sn)	   ; 1000000
  (format t "~&a colon separates the thousands: ~:d~%" bn) ; 1,000,000
  (format t "~&which can be redefined: ~,,'.,4:d~%" (* 1000 bn)) ; 10.0000.0000
  (format t "~&an 'at' makes the plus visible: ~@d~%" sn)	 ; +1000000
  (format t "~&the two above can be combined: ~:@d~%" bn)	 ; +1,000,000
  (format t "~&right aligned: ~12@a<EOL>~%" bn)			 ; "     1000000<EOL>"
  (format t "~&right aligned with leading zeros: ~12,'0d<EOL>~%" bn) ; 000001000000<EOL>
  (format t "Useful e.g. for date formatting: ~4d-~2,'0d-~2,'0d" 2019 6 7) ; 2019-06-07
  (format t "~&left aligned: ~12a<EOL>~%" bn) ; 1000000     <EOL>
  (format t "~&x for hexadecimal: ~x~%" bn)   ; F4240
  (format t "~&o for octal numbers: ~o~%" bn) ; 3641100
  (format t "~&and b for binary: ~b~%" bn)    ; 11110100001001000000
  (format t "~&R for any other base:~%")
  (radix-based bn)			; ... base 36: LFLS
)
