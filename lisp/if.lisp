#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(if '()
    (print "never")
    (format t "~&'() is false~%")
    )

(if ()
    (print "never")
    (format t "() is false~%")
    )

(if 'nil
    (print "never")
    (format t "'nil is false~%")
    )

(if nil
    (print "never")
    (format t "nil is false~%")
    )

(if '1
    (format t "all else is true~%")
    (print "never")
    )
