(defun from-to-int-to-char-list (i0 i1)
  (cond ((= i0 i1) nil)
	(t (cons (code-char i0) (from-to-int-to-char-list (1+ i0) i1)))
	)
  )

(defun int-list-to-char-list (il)
  (cond ((null il) nil)
	(t (cons (code-char (parse-integer (car il))) (int-list-to-char-list (cdr il))))
	)
  )

(defun char-list-to-int-list (cl)
  (cond ((null cl) nil)
	(t (cons (char-code (car cl)) (char-list-to-int-list (cdr cl))))
	)
)
