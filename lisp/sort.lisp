#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(unless (null *args*)
  (format t "~&~s~%" *args*)
  (sort *args* #'string<)
  (format t "~&~s~%" *args*)
  (sort *args* #'string>)
  (format t "~&~s~%" *args*)
)
  
(format t "~&終~%")
