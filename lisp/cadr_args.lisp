#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let ((arg1 (car *args*))
      (arg2 (cadr *args*)))
  (unless (null arg1)
    (format t "~&First arg: ~s~%" arg1)
    )
  (unless (null arg2)
    (format t "~&cadr *args*: ~s~%" arg2)
    (if (numberp (read-from-string arg2))
	(format t "~&parsed to int: ~s~%" (parse-integer (cadr *args*)))
	(format t "~&Not a number.~%")
	)
    )
  )  
