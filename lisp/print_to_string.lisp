#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a '(1 2 3 4))
      (s (make-array '(0)
                     :element-type 'base-char
                     :fill-pointer 0
                     :adjustable t))
      )
  (with-output-to-string (sb s)
    (format sb "~a " "The list:")
    (format sb "~a~&" a))
  (princ s)
  )
