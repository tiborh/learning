#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defstruct simple
  (id nil)
  (attrib nil)
  )

;; (load "dtrace.generic")
;; (dtrace func)

;; automatically created functions:
(setf a (make-simple))

;; let's print it:
(format t "~&~a~%" a)
(format t "~&id: ~a~%" (simple-id a))
(format t "~&attrib: ~a~%" (simple-attrib a))

;; let's change attribs:
(setf (simple-id a) 'simpy)
(setf (simple-attrib a) 'very)

(format t "~&After changes:~%~s~%" a)
(format t "~&Is ~a a 'simple'? ~a~%" (simple-id a) (simple-p a))

;; changes without the accessors:
(setf a '#s(simple id 123 attrib "moderately"))

(format t "~&After further changes:~%~s~%" a)
