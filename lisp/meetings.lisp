#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defvar *acquaintances* nil)

(defun meet (person)
  "book example"
  (cond ((equal person (car *acquaintances*))
	 'we-just-met)
	((member person *acquaintances*)
	 'we-know-each-other)
	(t (progn
	     (push person *acquaintances*)
	     'we-have-not-yet-met
	     )
	   )
	)
  )

(defun forget (person)
    (cond ((equal person (car *acquaintances*))
	   (pop *acquaintances*)
	   )
	  ((member person *acquaintances*)
	   (progn
	     (setf *acquaintances* (remove person *acquaintances*))
	     person
	     )
	   )
	(t (format nil "I do not know ~a" person))
	   )
    )

(defun greeter (person)
  (format t "~&Hello, ~a!~%" person)
  (meet person)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&trying to forget from empty list: ~a~%" (forget 'alice))
(format t "~&~a~%" (greeter 'fred))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'joe))
(format t "~&~a~%" (greeter 'fred))
(format t "~&~a~%" (greeter 'joe))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&forgetting ~a~%" (forget 'joe))
(format t "~&~a~%" *acquaintances*)
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'joe))
(format t "~&forgetting ~a~%" (forget 'cindy))
(format t "~&~a~%" *acquaintances*)
(format t "~&~a~%" (greeter 'cindy))
(format t "~&trying to forget unknown person: ~a~%" (forget 'alice))
(format t "~&~a~%" *acquaintances*)
