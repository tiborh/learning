#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun pairs ()
  (do ((a 0 (1+ a))
       (prompt (read-line) (read-line))
       )					; for (this part ; ; and this part)
      ((> a 1) a)				; for ( ; this part ; ) + a return value
    (format t "~&~s~%" (cons a (cons prompt nil)))
    )
  )

(pairs)

(format t "~&終~%")
