#!/usr/bin/env clisp
;;#!/usr/bin/sbcl --script

(defun no-counter ()
  (let ((n (list 'n 0)))
    (incf (cadr n))
    )
  )

(defun counter ()
  (let ((n '(n 0)))			; '(n 0) constant variable
    (incf (cadr n))			; undefined behaviour
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(dotimes (i 5)
  (format t "~&i == ~a; no-counter() == ~a; counter() == ~a~%" i (no-counter) (counter))
  )
