#!/usr/bin/env clisp
;; source:
;; http://cl-cookbook.sourceforge.net/dates_and_times.html

(defconstant *day-names*
    '("Monday" "Tuesday" "Wednesday"
      "Thursday" "Friday" "Saturday"
      "Sunday"))

(defun say-time ()
  (multiple-value-bind
	(second minute hour date month year day-of-week dst-p tz)
      (get-decoded-time)
    (format t "The time is ~2,'0d:~2,'0d:~2,'0d, on ~a, ~d-~2,'0d-~2,'0d (GMT~@d)~a"
	    hour
	    minute
	    second
	    (nth day-of-week *day-names*)
	    year
	    month
	    date
	    (- tz)
	    (if dst-p
		" (dst)"
		""
		)
	    )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(say-time)
