#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "list_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let* ((n (parse-integer (car *args*)))
	 (li (make-a-list n))
	 )
    (do ((cnt 0 (1+ cnt))
	 (lst li (cdr lst))
	 )
	((null lst)(format t "~&List length: ~a~%" cnt))
      (format t "~&cnt: ~a; lst: ~a~%" cnt lst)
	)
    )
  )
