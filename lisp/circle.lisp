#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;or:
;;#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun circle-circum(r)
  (format t "~&Circum: ~10f~%" (* PI 2 r))
  )

(defun circle-area(r)
  (format t "~&  Area: ~10f~%" (* PI r r))
  )

(defun circle(r)
  (format t "~&Radius: ~5f~%" r)
  (circle-circum r)
  (circle-area r)
  )

(defun to-number(str)
  (with-input-from-string (in str)
    (read in)
    )
  )

(if (null *args*)
    (write-line "Radius is needed as argument.")
    (circle (to-number (car *args*)))
    )

(princ "終")(terpri)
