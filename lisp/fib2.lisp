#!/usr/bin/env clisp

(defconstant PHI (/ (+ 1 (sqrt 5)) 2))

(defun fib (n)
  (cond ((< n 3) 1)
	(t (floor (+ (* PHI (fib (- n 1))) 0.5)))
	)
  )

(load "fib_lib.lisp")
  
;; (load "dtrace.generic")
;; (dtrace fib)

;;(format t "~&PHI: ~a~%" PHI)

(if (null *args*)
    (format t "~&No args.~%")
    (proc-args *args*)
    )

