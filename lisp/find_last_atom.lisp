#!/usr/bin/env clisp

(defun find-last-atom (x)
  (cond ((not (listp x)) x)
	((null x) nil)
	((null (cdr x)) (find-last-atom (car x)))
	((atom (cdr x)) (cdr x))
	(t (find-last-atom (cdr x)))
	)
  )

(defun test-find-last-atom ()
  (assert (not (find-last-atom nil)))
  (assert (find-last-atom '(T)))
  (assert (eq 'first (find-last-atom '(((first))))))
  (assert (eq 'sixth (find-last-atom '(((first second) (third fourth)) (fifth sixth)))))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(test-find-last-atom)
