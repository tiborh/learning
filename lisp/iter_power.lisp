#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun pow (base exp)
  (let ((result 1))
    (dotimes (i exp)
      (setf result (* result base))
      )
    result
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let ((base (parse-integer (car *args*)))
	(exponent (parse-integer (cadr *args*)))
	)
    (format t "~&~a to the power of ~a is ~a~%" base exponent (pow base exponent))
    )	
  )
