#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

(defstruct pair
  left right)

(defstruct (pairb (:type list) (:conc-name nil))
  left right)

(defun mod-pair (p)
  (setf (pair-left p) "moo")
  )

(multiple-value-bind
      (l r)
    (truncate 1.5)
  (let ((p0 (make-pair :left l :right r))
	(p1 (make-pairb :left l :right r))
	)
    (format t "~&~s (type-of: ~s)~%" p0 (type-of p0))
    (mod-pair p0)
    (format t "~&~s (after passed to function by reference)~%" p0)
    (format t "~&~s (type-of: ~s)~%" p1 (type-of p1))
    )
  )

(format t "~&終~%")

