#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun get-value ()
  (write-line "Give me cookie!!!")
  (format t "~&Cookie? ")
  (let ((v (read)))
    v
    )
  )

(defun cookie-monster ()
  (let ((ans (get-value)))
    (if (or (eql  ans 'cookie) (eql  ans 'cookies))
	(write-line "Thank you!...Munch munch munch...BURP")
	(progn (format t "No want ~a.~%~%" ans)
	       (cookie-monster)
	       )
	)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(cookie-monster)
