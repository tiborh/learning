#!/usr/bin/env clisp

(defun all-equal (li)
  (cond ((null li) nil)
	((= 1 (length li)) t)
	(t (and (equal (car li) (cadr li)) (all-equal (cdr li))))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&All equal? ~a~%" (all-equal *args*))
  )
