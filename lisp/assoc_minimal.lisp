#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a '((1)(2)(3)))
      (b '((1 a)(2 b)(3 c)))
      (c '((1 (a b c))(2 (d e f))(3 (g h i))))
      )
  (format t "~&The minimal requirement to use assoc: ~a~%" a)
  (format t "Where (assoc 1 <the-list-of-lists>) is: ~a~%" (assoc 1 a))
  (format t "For a more meaningfull use: ~a~%" b)
  (format t "where (cdr (assoc 1 <LioLi>): ~a~%" (cdr (assoc 1 b)))
  (format t "or (cadr (assoc 1 <LioLi>): ~a~%" (cadr (assoc 1 b)))
  (format t "with a more complex structure: ~a~%" c)
  (format t "(cadr 1 <lioli>): ~a~%" (cadr (assoc 1 c)))
  )
