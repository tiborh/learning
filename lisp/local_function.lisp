#!/usr/bin/env clisp

(defun portmanteau (x)
  (flet ((intfunc1 (y)
	   (* x (+ y 2))
	   )
	 (intfunc2 (y)
	   (* x (- y 2))
	   )
	 )
	 (- (intfunc1 x) (intfunc2 x))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(loop for i from 1 to 10
   do (format t "~&~2d -> ~2d~%" i (portmanteau i))
     )
