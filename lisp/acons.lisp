#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(let ((alist '()))
  (format t "~&Starting with empty list: ~a~%" alist)
  (format t "~&acons adds a pair from (acons key value a_list): ~a~%" (acons 1 "one" alist))
  (format t "~&but there is no side effect, so alist == ~a~%" alist)
  (setf alist (acons 1 "one" (acons 2 "two" alist)))
  (format t "~&setf can give value~%  e.g. (setf a_list (acons k1 v1 (acons k2 v2 a_list))) makes: ~a~%" alist)
  (format t "~&assoc 1: ~a~%" (assoc 1 alist))
  (setf alist (acons 1 "一" alist))
  (format t "~&Adding same key, different value: ~a~%" alist)
  (format t "~&(assoc 1 a_list): ~a~%" (assoc 1 alist))
  )
