(defun lst-len (ls n)
  (cond ((null ls) n)
	(t (lst-len (rest ls) (1+ n)))
	)
  )

(defun my_length (ls)
  (lst-len ls 0)
  )
