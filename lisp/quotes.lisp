#!/usr/bin/env clisp

(write-line "single quote used, it inhibits evaluation")
(write '(* 2 3))
(write-line " ")
(write-line "double quote in place of single")
(write "(* 2 3)") ; if end quote is not used:
; *** - READ: comma is illegal outside of backquote
(write-line " ")
(write-line "single quote not used, so expression evaluated")
(write (* 2 3))
