#!/usr/bin/env clisp

(require "check_args.lisp")

(defvar la (length *args*))

;;(format t "~&args: ~S~&" *args*)
;;(format t "~&length of args: ~S~&" la)

(let ((la (length *args*)))
      (format t "~&length of args list: ~a~%" la) 
      (check-args la)
      (if (> la 0)
	  (format t "~&~S~&" *args*)
	  (format t "~&(zero args)~%")
	  )
      )
