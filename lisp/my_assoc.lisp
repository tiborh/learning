#!/usr/bin/env clisp

(defun my-assoc (scalar lioli)
  (cond ((null lioli) nil)
	((equal (caar lioli) scalar) (car lioli))
	(t (my-assoc scalar (cdr lioli)))
	)
  )

(defun test-my-assoc ()
  (let ((a '((1)(2)(3)))
	(b '((1 a)(2 b)(3 c)))
	(c '((1 (a b c))(2 (d e f))(3 (g h i))))
	)
    (assert (not (my-assoc 4 a)))
    (assert (equal (my-assoc 1 a) '(1)))
    (assert (equal (my-assoc 3 a) '(3)))
    (assert (equal (my-assoc 1 b) '(1 a)))
    (assert (equal (my-assoc 1 c) '(1 (a b c))))
    )
  (write-line "tests have passed")
  )
  
;; (load "dtrace.generic")
;; (dtrace func)

(test-my-assoc)
