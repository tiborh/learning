#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun usage()
  (format t "~&~a day-number~%" (native-namestring *load-truename*))
  )

(defun give-day(day-num)
  (case day-num
    (1 "Monday")
    (2 "Tuesday")
    (3 "Wednesday")
    (4 "Thursday")
    (5 "Friday")
    (6 "Saturday")
    (7 "Sunday")
    )
  )

(if (null *args*)
    (usage)
    (format t "~&~s~%" (give-day (parse-integer (car *args*))))
    )

(format t "~&終~%")
