#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

;; from: https://www.tutorialspoint.com/lisp/lisp_mapping_functions.htm

;; simple usage
;; this works:
(format t "~&~s~%" (mapcar '+ '(1 3 5 7 9 11 13) '( 2 4 6 8)))

(format t "~&~s~%" (mapcar '1+  '(23 34 45 56 67 78 89)))

(let ((a (list 1 2 3 4))
      (b (list 5 6 7 8 9)))
  (format t "~&1+ ~s == ~s~%" a (mapcar '1+ a))
  (format t "~&~s plus ~s == ~s~%" a b (mapcar '+ a b))
  (format t "~&~s times ~s == ~s~%" a b (mapcar '* a b))
  )
