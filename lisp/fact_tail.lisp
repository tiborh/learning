#!/usr/bin/env clisp

(load "fact_fun.lisp")
  
(defun factorial-wrapper (str)
  (let ((n (parse-integer str)))
    (when (<= 0 n)
      (format t "~&~a! == ~a~%" n (factorial n))
      )
    )
  T
  )

(load "dtrace.generic")
(dtrace fact-tail)

(every #'factorial-wrapper *args*)
