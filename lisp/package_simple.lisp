#!/usr/bin/env -S sbcl --script

;; source:
;; https://www.tutorialspoint.com/lisp/lisp_packages.htm

(make-package :tom)
(make-package :dick)
(make-package :harry)
(defun tom::hello () 
   (write-line "Hello! This is Tom's Tutorials Point")
   )
(defun dick::hello () 
   (write-line "Hello! This is Dick's Tutorials Point")
   )
(defun harry::hello () 
   (write-line "Hello! This is Harry's Tutorials Point")
)

(in-package :tom)
(hello)

(common-lisp-user::in-package :dick)
(hello)

(common-lisp-user::in-package :harry)
(hello)

(common-lisp-user::in-package :common-lisp-user)

(write-line "")

(tom::hello)
(dick::hello)
(harry::hello)

(format t "~&終~%")
