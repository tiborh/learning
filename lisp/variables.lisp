#!/usr/bin/env clisp

(format t "~&Global Variables~%")
(format t "defvar to define (and initialise) a variable: a ==")
(defvar a 0)
(format t "~2d ~%" a)
(format t "setq to set the value: a ==")
(setq a 3)
(format t "~2d" a) 

(format t "~&Local variables are defined with let or prog~%")
(format t "(for setting them, the same setq is used)~%")
;; texamples fro tutorialspoint

(let ((x 'a) (y 'b)(z 'c))
  (format t "x = ~a y = ~a z = ~a~%" x y z))

(prog ((x '(a b c))(y '(1 2 3))(z '(p q 10)))
   (format t "x = ~a y = ~a z = ~a~%" x y z))

