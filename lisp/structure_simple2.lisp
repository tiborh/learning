#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun print-simple (x stream depth)
  (format stream "#<simple ~a> (depth: ~s)" (simple-id x) depth)
  )

(load "simple_struct_shared.lisp")

(defstruct (simple2 (:include simple))
  (attrib2 nil)
  )

;; (load "dtrace.generic")
;; (dtrace func)

;; automatically created functions:
(setf a (make-simple))

;; let's print it:
(format t "~&~a~%" a)
(format t "~&id: ~a~%" (simple-id a))
(format t "~&attrib: ~a~%" (simple-attrib a))

;; let's change attribs:
(setf (simple-id a) 'simpy)
(setf (simple-attrib a) 'very)

(format t "~&After changes:~%~s~%" a)
(format t "~&attrib: ~a~%" (simple-attrib a))
(format t "~&Is ~a a 'simple'? ~a~%" (simple-id a) (simple-p a))

;; changes without the accessors:
(setf a '#s(simple id 123 attrib "moderately"))

(format t "~&After further changes:~%~s~%" a)
(format t "~&attrib: ~a~%" (simple-attrib a))

(setf b (make-simple2 :id 'b
		      :attrib 'good
		      :attrib2 "not very"
		      )
      )

(format t "~&printing b:~%~a~%" b)
