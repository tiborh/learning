#!/usr/bin/env clisp

(defun f (&optional val)
  "Examines scopes"
  (format t "~&val when entering fuction: ~s~%" val)
  (unless val
    (setq val 'something)
    (format t "~&val inside unless: ~s~%" val)
    )
  (format t "~&val after unless: ~s~%" val)
  )


;; (load "dtrace.generic")
;; (dtrace func)

(f)
(f "one")
