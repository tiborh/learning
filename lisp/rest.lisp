#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun restful (&rest args)
  "to illustrate how apply can be used to avoid list in a list errors with &rest"
  (cond ((null args) nil)
	(t (format t "~&~a ~a~%" (car args) args)
	   (apply #'restful (cdr args))	; a whole list as args, not a list of args
	   )
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (apply #'restful *args*)
  )
