#!/usr/bin/env clisp

(defun first-atom (a-list)
  (cond ((atom a-list) a-list)
	(t (first-atom (first a-list)))
	)
  )

(defun print-first-atom (a-list)
  (format t "~&~s --> ~s~%" a-list (first-atom a-list))
  )

(print-first-atom ())
(print-first-atom '(a))
(print-first-atom '(a b c))
(print-first-atom '(((a)) b c))
