#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defconstant N 2)
(defvar Ts 1000)

(setq ah (make-hash-table))
(setf (gethash '0 ah) 0)
(setf (gethash '1 ah) 0)

(format t "~&~s~%" ah)

;; (format t "~&Num of heads: ~a~%" (gethash '0 ah))
;; (format t "~&Num of tails: ~a~%" (gethash '1 ah))

(unless (null *args*)
  (setq Ts (parse-integer (first *args*) :junk-allowed t))
  )

(dotimes (i Ts)
  (let ((flip (random N (make-random-state t))))
    (setf (gethash flip ah) (+ (gethash flip ah) 1))
    )
  )

(format t "~&Num of heads: ~a (~f%)~%" (gethash '0 ah) (* (/ (gethash '0 ah) Ts) 100))
(format t "~&Num of tails: ~a (~f%)~%" (gethash '1 ah) (* (/ (gethash '1 ah) Ts) 100))
