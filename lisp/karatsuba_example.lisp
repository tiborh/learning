#!/usr/bin/env clisp

(defun split-num (num)
  (list (floor num 100) (mod num 100))
  )

(defun ndigits (num)
  (ceiling (log num 10))
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((x 6742)
       (y 5318)
       (ab (split-num x))
       (cd (split-num y))
       (xdigits (ndigits x))
       (ydigits (ndigits y))
       (ac (* (first ab)  (first cd)))
       (bd (* (second ab) (second cd)))
       (step3 (* (+ (first ab) (second ab)) (+ (first cd) (second cd))))
       (step4 (-(- step3 bd) ac))
       (step5a (* ac (expt 10 xdigits)))
       (step5c (* step4 (expt 10 (/ ydigits 2))))
       )
  (format t "~&x: ~d~%" x)
  (format t "~&y: ~d~%" y)

  (format t "~&a: ~d~%" (first ab))
  (format t "~&b: ~d~%" (second ab))

  (format t "~&c: ~d~%" (first cd))
  (format t "~&d: ~d~%" (second cd))

  (format t "~&number of x digits: ~d~%" xdigits)
  (format t "~&number of y digits: ~d~%" ydigits)

  (format t "~&step3 == (a + b) * (c + d) == ~d~%" step3)
  (format t "~&step4 == step3 - bd - ac == ~d" step4)
  (format t "~&step5a == ac * 10^xdigits == ~d~%" step5a)
  (format t "~&step5c == bd * 10^(ydigits/2) == ~d~%" step5c)
  (format t "~&x * y == step5a + bd + step5c == ~d~%" (+ step5a bd step5c))
  (format t "~&x * y == ~d~%" (* x y))
  )
