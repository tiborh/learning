#!/usr/bin/env -S sbcl --script

(loop
   (let ((ch (peek-char)))
     (if (null ch)
	 (return nil)
	 (progn
	   (format t "~&~s" (read-char *query-io* nil nil))
	   (unread-char ch)
	   (format t "~s~%" (read-char *query-io* nil nil))
	   )
       )
     )
   )
