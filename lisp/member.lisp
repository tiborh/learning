#!/usr/bin/env clisp

(defun my-member (nu li)
  (cond
    ((null li) nil)
    ((eql nu (car li)) li)
    (t (my-member nu (cdr li)))
    )
  )

(defun my-member2 (nu li)
  (cond
    ((null li) nil)
    ((equal nu (car li)) li)
    (t (my-member2 nu (cdr li)))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a (list 0 1 2 3 4 5))
      (b (list 0 1 nil 3 4 5))
      (c '(a b c d e f g))
      (d '("one" "two" "three" "four" "five"))
      )
  (format t "~&'member' is usually used in conditional expressions,~% so its return value rarely gets scrutinised. But...~%")
  (format t "our list: ~s~%" a)
  (format t "(member 3 the_list) == ~s~%" (member 3 a))
  (format t "modelling the same with own function: ~s~%" (my-member 3 a))
  (format t "testing for non-member: ~s vs ~s~%" (member 6 a) (my-member 6 a))
  (format t "testing with empty list: ~s vs ~s~%" (member 0 ()) (my-member 0 ()))
  (format t "testing for nil in the list of ~s: ~s vs ~s~%" b (member nil b) (my-member nil b))
  (format t "a symbol list: ~s~%" c)
  (format t "(member 'c symbol_list) == ~s~%" (member 'c c))
  (format t "(my-member 'c symbol_list) == ~s~%" (my-member 'c c))
  (format t "a string list: ~s~%" d)
  (format t "(member \"three\" string_list) == ~s~%" (member "three" d))
  (format t "(my-member  \"three\" string_list) == ~s~%" (my-member "three" d))
  (format t "but if we use equal instead of eql: ~s~%" (my-member2 "three" d))
  (format t "testing 'find' with 3: ~a~%" (find 3 a))
  (format t "testing 'find' with nil (in b): ~a~%" (find nil b))
  (format t "testing 'find' with nil (in a): ~a~%" (find nil a))
  )
