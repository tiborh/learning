(defparameter *a-tree*
  '(:agents
    ("foo"
     :results (1 0 3 9 4)
     )
    ("bar"
     :results (0 8 9 1 8)
     :sub (("tin" 30 30)
	   ("tin" 90 1))
     )
    ("baz"
     :results (3 7 1 4 4)
     )
    )
  )

(defun helper-func (x)
  (and (numberp x)
       (< x 5))
  )
