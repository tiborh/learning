#!/usr/bin/env clisp

(defun dec-to-bin-digit (n)
  (cond ((= n 0) "0")
	((= n 1) "1")
	)
  )

(defun dec-to-bin (n)
  (cond ((< n 2) (dec-to-bin-digit n))
	(t (concatenate 'string (dec-to-bin (floor (/ n 2))) (dec-to-bin-digit (mod n 2))))
	)
)
  
;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (write-line "No args. A natural number is expected.")
    (write-line (dec-to-bin (parse-integer (car *args*))))
    )
    
