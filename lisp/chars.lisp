#!/usr/bin/env clisp

(defun str-check (s)
  (assert (stringp s) (s) "~s: is not a string")
  )

(defun str-head (s)
  ;; (coerce (subseq s 0 1) 'character)
  (char s 0)
  )

(defun str-rest (s)
  (cond ((< (length s) 2) nil)
	(t (subseq s 1))
	)
  )

(defun str-cmp (s1 s2)
  (cond ((and (null s1) (null s2)) 0)
	((null s1) -1)
	((null s2) 1)
	(t (let ((c1 (str-head s1))
		 (c2 (str-head s2))
		 )
	     (cond ((char< c1 c2) -1)
		   ((char> c1 c2) 1)
		   (t (str-cmp (str-rest s1) (str-rest s2)))
		   )
	     )
	   )
	)
  )

(defun cmp (s1 s2)
  (str-check s1)
  (str-check s2)
  (str-cmp s1 s2)
  )

;; (load "dtrace.generic")
;; (dtrace str-cmp)

(format t "~&~c~c~%" #\a #\b)
(princ (coerce (subseq "apple" 0 1) 'character))
(prin1 (coerce (subseq "apple" 1 2) 'character))
(format t "~&~s~%" (coerce "apple" 'list))
(format t "~&~s~%" (coerce (coerce "apple" 'list) 'string))

;; named ones
;; #\Backspace
;; #\Tab
;; #\Linefeed
;; #\Page
;; #\Return
;; #\Rubout

(format t "~&~a~c~a~%" "appp" #\Backspace "le")
(format t "~&~c~c~c~%" #\a #\Tab #\b)
(format t "~&~c~c~c~%" #\a #\Linefeed #\b)
(format t "~&~c~c~c~%" #\a #\Page #\b)
(format t "~&~c~c~c~%" #\a #\Return #\b)
(format t "~&~c~c~c~%" #\a #\Rubout #\b)

(let* ((s1 "apple")
       (s2 "pear")
       (s3 "apricot")
       (s4 "apple")
       (s5 "apples")
       (l1 (list s1 s2 s3 s4 s5))
       )
       (format t "~&strings: ~s~%" l1)
       (format t "~&s1 cmp s2: ~a ~%" (cmp s1 s2))
       (format t "~&s1 cmp s3: ~a ~%" (cmp s1 s3))
       (format t "~&s2 cmp s3: ~a ~%" (cmp s2 s3))
       (format t "~&s1 cmp s4: ~a ~%" (cmp s1 s4))
       (format t "~&s1 cmp s5: ~a ~%" (cmp s1 s5))
       (format t "~&s5 cmp s1: ~a ~%" (cmp s5 s1))
  )
