#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a (cons 1 2))
      (b (cons 3 4))
      )
  (format t "~&~a and ~a~%" a b)
  )
