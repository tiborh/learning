#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun my-library (title author rating availability)
   (list :title title :author author :rating rating :availability availability)
)

(let ((book (my-library "Hunger Game" "Collins" 9 t)))
  (format t "~& the whole: ~s~%" book)
  (format t "~& symbol-plist: ~s~%" (symbol-plist 'book)) ; nil
  (format t "~&     title: ~s~%" (getf  book :title))
  (format t "~&    author: ~s~%" (getf  book :author))
  (format t "~&    rating: ~s~%" (getf  book :rating))
  (format t "~&available?: ~s~%" (getf  book :availability))
)

(format t "~&終~%")
