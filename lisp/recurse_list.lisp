#!/usr/bin/env clisp

(defun proc-args (args)
  (cond ((null args) nil)
      ((format t "~&~s~%" (first args)))
      (t (proc-args (rest args)))
      )
  )

(if (null *args*)
    (format t "~&No args.~%")
    (proc-args *args*)
    )

