(defun my-member (nu li)
  (cond
    ((null li) nil)
    ((equal nu (car li)) li)
    (t (my-member nu (cdr li)))
    )
  )
