#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(unless *args*
  (format t "~&an integer arg is needed~%")
  (EXT:EXIT)
  )

(defvar *odd-number* nil)
;; progn:
;;	1. evaluates forms, in the order in which they are given.
;;	2. The values of each form but the last are discarded.
;;	3. If progn appears as a top level form, then all forms within 
;;	   that progn are considered by the compiler to be top level forms.
(format t "~&Input was '~a'.~%" (if (oddp (parse-integer (first *args*)))
				  (progn (setf *odd-number* t) "odd number")
				  "even number")
	)
(format t "is it an odd number? ~a.~%" (string-capitalize (if *odd-number* "true" "false")))
