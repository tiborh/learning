#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun print-hash (ha)
  (maphash #'(lambda (k v) (format t "~&~a => ~a~%" k v)) ha)
  )

(let ((ha (make-hash-table))
      (i 0)
      )
  (dolist (s *args*)
    (setf (gethash s ha) i)
    (incf i)
    )
  (format t "~&the hash: ~s~%" ha)
  (print-hash ha)
  (if (< 1 (length *args*))
      (progn
	(remhash (nth 0 *args*) ha)
	(write-line "after removal of first:")
	(print-hash ha)
	(incf (gethash (nth 1 *args*) ha))
	(write-line "after incrementing first value:")
	(print-hash ha)
	)
      )
  )

(format t "~&終~%")
