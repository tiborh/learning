#!/usr/bin/env clisp

(defun print-args (args)
  (if (zerop (length args))
      (format t "use command line args")
      )
  (dolist (n args)
    (format t "~&~a~%" n)
    )
  )

(print-args *args*)
