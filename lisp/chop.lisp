#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun chop (li)
  (cond ((null li) nil)
	((atom li) li)
	((listp (car li)) (chop (car li)))
	(t (list (car li)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&nil: ~a~%" (chop nil))
(format t "~&'a: ~a~%" (chop 'a))
(format t "~&'(a): ~a~%" (chop '(a)))
(format t "~&'(a b): ~a~%" (chop '(a b)))
(format t "~&'((a) b): ~a~%" (chop '((a) b)))
(format t "~&'((a b) c): ~a~%" (chop '((a b) c)))
