#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun divider (num start)
  (let ((quotient (/ num start)))
    (cond ((< quotient 2) (list num))
	  ((integerp quotient) (cons start (divider quotient 2)))
	  (t (divider num (1+ start)))
	  )
    )
  )

(defun factors (num)
  (divider num 2)
  )

;; (load "dtrace.generic")
;; (dtrace divider)

(unless (null *args*)
  (format t "~&~a -> ~a~%" (car *args*) (factors (parse-integer (car *args*))))
  )
