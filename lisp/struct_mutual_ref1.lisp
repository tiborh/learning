#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun print-starship (ship stream depth)
  (format stream "#S(STARSHIP :NAME ~a)" (starship-name ship))
  )

(defun print-captain (capt stream depth)
  (format stream "#S(CAPTAIN :NAME ~a)" (captain-name capt))
  )

(defstruct (starship (:print-function print-starship))
  (name nil)
  (captain nil)
  )

(defstruct (captain (:print-function print-captain))
  (name nil)
  (ship nil)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(setq enterprise (make-starship :name 'enterprise))
(setq kirk (make-captain :name 'kirk :ship enterprise))
(setf (starship-captain enterprise) kirk)

(print enterprise)
(print kirk)
