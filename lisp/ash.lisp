#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let ((n 50))
  (format t "~&number: ~d~%" n)
  (format t "~&(ash n 1): ~d~%" (ash n 1))
  (format t "~&(ash n -1): ~d~%" (ash n -1))
  (format t "~&(ash n 2): ~d~%" (ash n 2))
  (format t "~&(ash n -2): ~d~%" (ash n -2))
  )
