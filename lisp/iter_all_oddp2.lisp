#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun all-oddp (li-o-nu)
  (when (null li-o-nu)
    (return-from all-oddp nil)
    )
  (do* ((li li-o-nu (cdr li))
	(all-odds (oddp (car li)) (oddp (car li))))
       ((or (null (cdr li)) (not all-odds)) all-odds)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (all-oddp nil)))
(assert (all-oddp '(1)))
(assert (null (all-oddp '(2))))
(assert (null (all-oddp '(1 3 5 6 7))))
(assert (all-oddp '(1 3 5 7 9)))
