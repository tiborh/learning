#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun ntack (li sym)
  (nconc li (list sym))
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a '(1 2 3))
      (b '(a b c))
      )
  (format t "~&a: ~a~%" a)
  (format t "~&b: ~a~%" b)
  (format t "~&ntack ~a 4: ~a~%" a (ntack a 4))
  (format t "~&a: ~a~%" a)
  (format t "~&ntack ~a d: ~a~%" b (ntack b 'd))
  (format t "~&b: ~a~%" b)
  )
