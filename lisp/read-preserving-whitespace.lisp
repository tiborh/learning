#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(let ((rd (read-preserving-whitespace)))
  (format t "~&|~s| (type-of: ~s)~%" rd (type-of rd))
)
(format t "~&終~%")
