#!/usr/bin/env clisp

(load "numlist_func.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(if (> (length *args*) 0)
  (let ((nstart (if (> (length *args*) 1)
		     (parse-integer (cadr *args*))
		     0
		     ))
	)
    (format t "~&~a~%" (numlist-to-from (parse-integer (car *args*)) nstart))
    )
  (format t "~&Usage: cmd <ending number> [starting number]~%")
  )
