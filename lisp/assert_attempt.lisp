#!/usr/bin/env clisp

(let ((sandwich '(ham on rye)))
    (assert (eql (car sandwich) 'chicken)
        (car sandwich)
        "I wanted a ~a, and not ~a sandwich." 'chicken (car sandwich))
    sandwich)
