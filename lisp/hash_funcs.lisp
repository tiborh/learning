(defun print-count-size (ha)
  (format t "~&count: ~a (size: ~a)~%" (hash-table-count ha) (hash-table-size ha))
  )

(defun create-hash (li &optional (init-val nil))
  (add-to-hash li (make-hash-table) init-val)
  )
		  		  
(defun add-to-hash (li ha &optional (init-val nil))
  (nreverse li)
  (dolist (it li)
    (setf (gethash it ha) init-val)
    )
  ha
  )

(defun remove-from-hash (li ha)
  (dolist (it li)
    (remhash it ha)
    )
  )

(defun get-keys (ha)
  (loop for key being the hash-keys of ha collect key)
  )

(defun get-values (ha)
  (loop for value being the hash-values of ha collect value)
  )

(defun get-hash-length (ha)
  (length (get-keys ha))
  )

(defun key-exists-p (key ht)
  "the second output value of gethash shows if key exists: limitation: only for symbol keys"
  (let ((key-existsp nil))
    (multiple-value-bind (hr0 hr1) (gethash key ht) (setf key-existsp hr1))
    key-existsp
    )
  )

(defun pretty-print-hash (ha)
  (maphash #'(lambda (k v) (format t "~&~a: ~a~%" k v)) ha)
  )

