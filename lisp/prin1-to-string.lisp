#!/usr/bin/env -S sbcl --script

(defstruct pair
  left right)

(let* ((p (make-pair :left "left" :right "right"))
       (pstring (prin1-to-string p))
       (pclone (read-from-string pstring))
      )
  (format t "~&~s~%" p)
  (format t "~&~s~%" pstring)
  (format t "~&~s~%" pclone)
  (format t "~&~s~%" (pair-left pclone))
  )

(format t "~&終~%")
