#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(write-line "use (intern string)")
(prin1 "sample-string")(terpri)
(prin1 (intern "sample-string"))(terpri)
(prin1 (type-of (intern "sample-string")))(terpri)
