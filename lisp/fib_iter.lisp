#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun fib (n)
  (do ((i  0 (1+ i))
       (f1 0 f2)
       (f2 1 (+ f1 f2))
       )
       ((= i n) (return f1))
    )
  )


;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (dolist (it *args*)
    (let ((n (parse-integer it)))
      (unless (< n 0)
	(format t "~&fib ~a == ~a~%" n (fib n))
	)
      )
    )
  )
