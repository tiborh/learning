#!/usr/bin/env clisp

(load "common_funcs.lisp")

(defun fib (n)
  (cond ((< n 3) 1)
	(t (add-nums (list (fib (- n 2)) (fib (- n 1)))))
	)
  )

(load "fib_lib.lisp")

(load "dtrace.generic")
(dtrace fib add-nums)

(if (null *args*)
    (format t "~&No args.~%")
    (proc-args *args*)
    )
