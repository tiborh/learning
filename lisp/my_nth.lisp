#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defvar alph '(a b c d e f g h i j k l m n o p q r s t u v w x y z))

(defun my-nth (n ls)
  (cond ((zerop n) (first ls))
	((null ls) NIL)
	(t (my-nth (- n 1) (rest ls)))
    )
  )

(defun main (n)
  (format t "~&~a~%" alph)
  ;;(format t "~&   ~ath: ~a~%" n (nth n alph))
  (format t "~&nth ~a: ~a~%" n (my-nth n alph))
  (return-from main "終")
  )

(if (null *args*)
    (format t "No args.")
    (let ((n (parse-integer (first *args*)))
	  )
      (assert (>= n 0) (n) "does not work with negative numbers: ~d" n)
      (format t "~&~a~%" (main n))
      )
)
