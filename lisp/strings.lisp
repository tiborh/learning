#!/usr/bin/env clisp

;; see is_palindrome.lisp for the use of subseq
;; see see chars.lisp for the use of coerce for conversion between string and (list of) chars

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a "write-line only works with strings"))
  (write-line a)
  (format t "~&type-of for string: ~s~%" (type-of a))
  (write-line (reverse a))
  (format t "~&Length of the string: ~d~%" (length a))
  (format t "~&First char: ~a~%" (char a 0))
  (format t "~&type of char result: ~a~%" (type-of (char a 0)))
  (format t "~&elt and aref also work: ~a ~a~%" (elt a 1) (aref a 2))
  (format t "~&last char: ~a~%" (car (last (concatenate 'list a))))
  (format t "~&in another way: ~a~%" (char a (1- (length a))))
  )

(let ((a 'easier)
      (b 'than)
      )
  (format t "~&~a~%" `(It is often ,a to use symbols ,b strings))
  )

(write-line (concatenate 'string "number to string: " (write-to-string 3214)))
(let* ((a "sample string")
       (al (concatenate 'list a))
       (a2 (loop for char across a
	      collect char))
       (b (make-array 0 :element-type 'character :fill-pointer 0 :adjustable t))
      )
  (format t "~&Concatenate to list: ~a~%" al)
  (format t "~&same with ~~s: ~s~%" al)
  (format t "~&loop across: ~s~%" a2)
  (dolist (ch al)
    (vector-push-extend ch b)
    )
  (format t "~&Put back together: ~s~%" b)
  (format t "~&type of variable: ~s~%" (type-of b))
  (format t "~&perceived length: ~a~%" (length b))
  (format t "~&Breaking up with map and lambda:")
  (map 'string #'(lambda (c) (print c)) a)
  (fresh-line)
  )

;; for string comparisons:
;; string=	string-equal		Checks if the values of the operands are all equal or not, if yes then condition becomes true.
;; string/=	string-not-equal	Checks if the values of the operands are all different or not, if values are not equal then condition becomes true.
;; string<	string-lessp		Checks if the values of the operands are monotonically decreasing.
;; string>	string-greaterp		Checks if the values of the operands are monotonically increasing.
;; string<=	string-not-greaterp	Checks if the value of any left operand is greater than or equal to the value of next right operand, if yes then condition becomes true.
;; string>=	string-not-lessp	Checks if the value of any left operand is less than or equal to the value of its right operand, if yes then condition becomes true.

;; for case control:
;; string-upcase	Converts the string to upper case
;; string-downcase	Converts the string to lower case
;; string-capitalize	Capitalizes each word in the string

;; for trimming
1	
;; -- string-trim
;; 	It takes a string of character(s) as first argument and a string as the second argument and returns a substring where all characters that are in the first argument are removed off the argument string.
;; -- String-left-trim
;; 	It takes a string of character(s) as first argument and a string as the second argument and returns a substring where all characters that are in the first argument are removed off the beginning of the argument string.
;; -- String-right-trim
;; 	It takes a string character(s) as first argument and a string as the second argument and returns a substring where all characters that are in the first argument are removed off the end of the argument string.


(defun bars (in-str)
  (concatenate 'string "|" in-str "|")
  )

(let ((a "       	\" -one-  ) \"   ")
      (filter (concatenate 'string (string #\Space) "\"-()" (string #\Tab)))
      )
  (write-line "Starting trim-test")
  (write-line (bars a))
  (write-line (bars (string-trim " " a)))
  (write-line (bars (string-trim filter a)))
  (write-line (bars (string-left-trim filter a)))
  (write-line (bars (string-right-trim filter a)))
  (write-line "Ending trim-test")
  )

(let ((a (list "one" "two" "three" "four"))
      (b (list "five" "six" "seven" "eight"))
      )
  (format t "~&Original:     ~a~%" a)
  (format t "~&format to concatenate:~{ ~a~}~%" a)
  (format t "~&Sorted (a-z): ~a~%" (sort a #'string<))
  (format t "~&Sorted (z-a): ~a~%" (sort a #'string>))
  (format t "~&The first list: ~a~%" a)
  (format t "~&The other list: ~a~%" b)
  (format t "~&Merge two lists (string<): ~a~%" (merge 'list a b #'string<))
  (format t "~&Merge two lists (string>): ~a~%" (merge 'list a b #'string>))
  )

;; for lots of ideas:
;; https://lispcookbook.github.io/cl-cookbook/strings.html
