#!/usr/bin/env clisp

(load "simple_text_game.params")

(defun dot-name (exp)
  (substitute-if #\_ (complement #'alphanumericp) (prin1-to-string exp))
  )

(defparameter *max-label-length* 12)
(defparameter *ellipsis-marker* "…")
(defparameter *ellipsis-space* (length *ellipsis-marker*))

(defun dot-label (exp)
  (if exp
      (let ((s (write-to-string exp :pretty nil)))
	(if (> (length s) *max-label-length*)
	    (concatenate 'string (subseq s 0 (- *max-label-length* *ellipsis-space*)) *ellipsis-marker*)
	    s)
	)
      ""
      )
  )

(defun nodes-dot (nodes)
  (mapc (lambda (node)			; like mapcar, but does not return the transformed list
	  (fresh-line)
	  (princ (dot-name (car node)))
	  (princ "[label=\"")
	  (princ (dot-label node))
	  (princ "\"];")
	  )
	nodes
	)
  )

(defun uedges-dot (edges)
  ;; unidirectioonal version of nodes-dot
  (maplist (lambda (lst)
	     (mapc (lambda (edge)
		   (unless (assoc (car edge) (cdr lst))
		     (fresh-line)
		     (princ (dot-name (caar lst)))
		     (princ "--")
		     (princ (dot-name (car edge)))
		     (princ "[label=\"")
		     (princ (dot-label (cdr edge)))
		     (princ "\"];")
		     )
		   )
		   (cdar lst)
		   )
	     )
	   edges)
  )

(defun edges-dot (edges)
  (mapc (lambda (node)
	  (mapc (lambda (edge)
		  (fresh-line)
		  (princ (dot-name (car node)))
		  (princ "->")
		  (princ (dot-name (car edge)))
		  (princ "[label=\"")
		  (princ (dot-label (cdr edge)))
		  (princ "\"];")
		  )
		(cdr node)
		)
	  )
	edges
	)
  )

(defun graph-dot (nodes edges)
  (princ "digraph{")
  (princ #\Linefeed)
  (nodes-dot nodes)
  (edges-dot edges)
  (princ #\Linefeed)
  (princ "}")
  (fresh-line)
  )

(defun ugraph-dot (nodes edges)
  (princ "graph{")
  (nodes-dot nodes)
  (uedges-dot edges)
  (fresh-line)
  (princ "}")
  (fresh-line)
  )

(defun dot-png (fname thunk)
  (with-open-file (*standard-output*
		   fname
		   :direction :output ; "write"
		   :if-exists :supersede) ; "overwrite"
    (funcall thunk))			  ; nullary function, that is, no argument
    (ext:shell (concatenate 'string "dot -Tpng -O " fname)
    )
  )

(defun graph-png (fname nodes edges)
  (dot-png fname (lambda () (graph-dot nodes edges)))
  )

(defun ugraph-png (fname nodes edges)
  (dot-png fname (lambda () (ugraph-dot nodes edges)))
  )
  
;; (load "dtrace.generic")
;; (dtrace func)

;; test dot-name
(print (dot-name 'living-room))
(print (dot-name 'stop!))
(print (dot-name 24))
(fresh-line)

;; test dot-label
(print (dot-label 'living-room))
(print (dot-label 'unbelivably-long))
(fresh-line)

;; test nodes-dot
(nodes-dot *nodes*)
(fresh-line)

;; test edges-dot
(edges-dot *edges*)
(fresh-line)

;; test graph-dot
(graph-dot *nodes* *edges*)
(fresh-line)
;; test ugraph-dot
(ugraph-dot *nodes* *edges*)
(fresh-line)

;; test dot-png and graph-png
(graph-png "wizard.dot" *nodes* *edges*)
;; test ugraph-png
(ugraph-png "wizard_u.dot" *nodes* *edges*)

(format t "~&map functions (mapc, map, maplist) need to be examined~%")
(format t "~&name of outfile needs to be printed~%")
(format t "~&to be continued~a~%" *ellipsis-marker*)

