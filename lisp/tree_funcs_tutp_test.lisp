#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

(load "tree_funcs_tutp.lisp")

(let* ((tr '((1 2 (3 4 5) ((7 8) (7 8 9)))))
       (mytree (make-tree 10))
       (mytree2 (make-tree 11))
       (newtree (add-child tr mytree))
       (newsmall (add-child mytree mytree2))
       )
  (format t "~&tr: ~s~%" tr)
  (format t "~&data in tr: ~s~%" (data tr))
  (format t "~&mytree: ~s~%" mytree)
  (format t "~&data in my-tree: ~s~%" (data mytree))
  (format t "~&first child of tr: ~s~%" (first-child tr))
  (format t "~&mytree added to tr: ~s~%" newtree)
  (format t "~&second tree: ~s~%" newsmall)
  )

(format t "~&終~%")
