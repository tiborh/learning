#!/usr/bin/env clisp

(defun sp (n ex)
  (cond ((= 1 ex) n)
	(t (* n (sp n (1- ex))))
  )
)

(defun self-power (n)
  (cond ((< n 0) nil)
	((= 0 n) 1)
	(t (sp n n))
	)
  )
  
;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let ((a (parse-integer (car *args*))))
    (format t "~&~a^~a => ~a~%" a a (self-power a))
    )
  )
