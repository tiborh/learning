#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun loop0 ()
  (do ((a  0 (1+ a))
       (b 10 (1- b))
       (c 0 (- a b)))			; works with previous value
       ((> a b) c)
       (format t "~&a: ~3d; b: ~3d; c: ~3d~%" a b c)
    )
  )

(defun loop1 ()
  (do* ((a  0 (1+ a))
       (b 10 (1- b))
       (c (- a b) (- a b)))		; works with current value
       ((> a b) c)
       (format t "~&a: ~3d; b: ~3d; c: ~3d~%" a b c)
    )
  )


;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&loop0: ~a~%" (loop0))
(format t "~&loop1: ~a~%" (loop1))
