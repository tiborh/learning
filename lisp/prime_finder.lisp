#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "factors_do_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

;;(format t "~&Primes found so far: ~a~%" collector)
(do ((num 2 (1+ num)))
    (nil)
  (when (= num (a-fact num))
    (format t "~a, " num)
    )
  )
