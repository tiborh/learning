#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun iter-reverse (li)
  (let ((il nil))
    (dolist (it li il)
      (push it il)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (iter-reverse nil)))
(assert (equal '(0) (iter-reverse '(0))))
(assert (equal '(a b c d) (iter-reverse '(d c b a))))

  
