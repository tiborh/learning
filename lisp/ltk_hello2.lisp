#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "ltk/ltk")

(in-package :ltk)

(defun hello-2()
  (with-ltk ()
   (let* ((f (make-instance 'frame))
          (b1 (make-instance 'button
                             :master f
                             :text "Button 1"
                             :command (lambda () (format t "Button1~&"))))
          (b2 (make-instance 'button
                             :master f
                             :text "Button 2"
                             :command (lambda () (format t "Button2~&")))))
     (pack f)
     ;; bellow one another
     ;; (pack b1)
     ;; (pack b2)
     ;; side by side
     (pack b1 :side :left)
     (pack b2 :side :left)
     (configure f :borderwidth 3)
     (configure f :relief :sunken)
     )))

;; (load "dtrace.generic")
;; (dtrace func)

(hello-2)
