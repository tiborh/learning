#!/usr/bin/env clisp

(load "fact_fun.lisp")

(defun get-arg (args)
  ;;(format t "~&args: ~s; listp? ~s~%" args (listp args))
  (if (null args)
      (format t "~&No args.~%")
      (let ((n (first args)))
	;;(format t "~&n: ~s~%" n)
	(when (stringp n)
	  (if (numberp (read-from-string n))
	      (factorial (parse-integer (first args) :junk-allowed t))
	      (format t "~&~s cannot be parsed as a number.~%" n)
	    )
	  )
	)
      )
  )

;;(load "dtrace.generic")
;;(dtrace get-arg)

(let ((result (get-arg *args*)))
  (when result
    (format t "~&~a! ~a~%" (first *args*) result)
    )
  )
