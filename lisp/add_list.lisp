#!/usr/bin/env clisp

(load "mod_give_list.lisp")

(defun add-list (lst)
  (cond ((null lst) 0)
	(t (+ (first lst) (add-list (rest lst))))
	)
  )

(defun add-list2 (&key lst from-index to-index)
  (cond ((= from-index to-index) (nth to-index lst))
	((= (- to-index from-index) 1) (+ (nth from-index lst) (nth to-index lst)))
	(t (let ((midpoint (floor (/ (+ to-index from-index) 2))))
	     (+ (add-list2 :lst lst :from-index from-index :to-index midpoint) (add-list2 :lst lst :from-index (+ midpoint 1) :to-index to-index))
	     )
	   )
	)
  )

(defun add-list2-wrapper (lst)
  (add-list2 :lst lst :from-index 0 :to-index (- (length lst) 1))
  )

;; (load "dtrace.generic")
;; (dtrace add-list2)

(when (null *args*)
  (format t "~&No args.~%")
  (EXT:EXIT)
  )

(let ((n (parse-integer (first *args*))))
  (when (> n 0)
    (let ((my-list (give-list n 1)))
      (format t "~& list: ~s~% sum: ~a~%" my-list (add-list my-list))
      (format t "~& the other sum: ~a~%" (add-list2-wrapper my-list))
      )
    )
  )
