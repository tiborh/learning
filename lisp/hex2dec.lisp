#!/usr/bin/env clisp

(defparameter hexpairs (reverse (pairlis (coerce "0123456789ABCDEF" 'list)
					 (list 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)
					 )
				)
  )

(defun hex2dec-char (c)
  (let ((r (cdr (assoc c hexpairs))))
    (if (null r)
	(progn (format t "~&Invalid hexadecimal character: ~a~%" c)
	       (EXT:EXIT)
	       )
	r
	)
    )
  )

(defun hex2dec-conv (char-list &optional (n16 0))
  (cond ((null char-list) 0)
	(t (+ (* (hex2dec-char (car char-list))
		 (expt 16 n16))
	      (hex2dec-conv (cdr char-list) (1+ n16))))
	)
  )

(defun hex2dec (s)
  (hex2dec-conv (coerce (reverse (string-upcase (first *args*))) 'list))
  )

;; (load "dtrace.generic")
;; (dtrace func)

(when *args*
  (format t "~&~a~%" (hex2dec (car *args*)))
  )
