#!/usr/bin/env clisp

(defun sum-numeric (li)
  (cond ((null li) 0)
	((numberp (car li))
	 (+ (car li)
	       (sum-numeric (cdr li))
	       )
	 )
	(t (sum-numeric (cdr li)))
	)
  )

(defun test-sum-numeric ()
  (assert (= (sum-numeric nil) 0))
  (assert (= (sum-numeric '(1 2 3.5)) 6.5))
  (assert (= (sum-numeric '(1 2 3.5 nil 5)) 11.5))
  (assert (= (sum-numeric '(nil)) 0))
  (assert (= (sum-numeric '(3 bears and 1 girl)) 4))
  (write-line "tests have passed")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(test-sum-numeric)
