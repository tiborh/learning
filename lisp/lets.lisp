#!/usr/bin/env clisp


(defvar a 4)
(defvar b 5)
(defconstant c 7)
(defvar sumab (+ a b))
(defvar prodab (* a b))
(format t "~&global:~%")
(format t "~&a = ~d, b = ~d, sum = ~d, prod = ~d~%" a b sumab prodab)

(defun af (b)
  (let (
	(a 3)
	(sumab (+ a b))
	(prodab (* a b))
	)
    (format t "~&a = ~d, b = ~d, sum = ~d, prod = ~d~%" a b sumab prodab)
    )
  )

(defun bf (b)
  (let* (
	(a 3)
	(sumab (+ a b))
	(prodab (* a b))
	)
    (format t "~&a = ~d, b = ~d, sum = ~d, prod = ~d~%" a b sumab prodab)
    )
  )

(defun cf (b)
  (let* (
	 (a 3)
	 ; (c 6) ;cannot overwrite constant
	 (sumab (+ a b c))
	 (prodab (* a b c))
	)
    (format t "~&a = ~d, b = ~d, c = ~d, sum = ~d, prod = ~d~%" a b c sumab prodab)
    )
  )



(format t "~&let:~%")
(af 2)
(format t "~&let*:~%")
(bf 2)
(format t "~&using constant:~%")
(cf 2)
