#!/usr/bin/env clisp

(defvar a (lambda (x) (incf x)))
(format t "~&a: ~s~%" a)
(let ((x 0))
  (format t "~&x: ~a~%" x)
  (format t "~&a: ~s~%" (funcall a x))
  (format t "~&x: ~a~%" x)
  )
