(defun fact (n)
  (cond ((zerop n) 1)
	(t (* n (fact (1- n))))
	)
  )

(defun fact-tail (n res)
  (cond ((zerop n) res)
	(t (fact-tail (1- n) (* res n)))
	)
  )

(defun factorial (n)
  (fact-tail n 1)
  )

	    
(defun fact_debug (n)
  (cond ((zerop n) (break "n is zero"))
	(t (* n (fact_debug (1- n))))
	)
  )
