#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "simple_struct_shared.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

;; automatically created functions:
(setf a (make-simple))

;; let's print it:
(format t "~&~a~%" a)
(format t "~&(simple-p #s(simple)): ~a~%" (simple-p #s(simple)))
(format t "~&(simple-p (make-simple)): ~a~%" (simple-p (make-simple)))
(write-line "let's describe it:")
(format t "~&~s~%" (describe #s(simple)))
(write-line "let's describe the object:")
(format t "~&~s~%" (describe a))
