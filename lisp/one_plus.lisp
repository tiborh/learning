#!/usr/bin/env clisp

(defun my-length (li)
  (if li
      (1+ (my-length (cdr li)))
      0)
  )

(load "dtrace.generic")
(dtrace my-length)

(let ((a (list 0 1 2 3 4)))
  (format t "~&The list: ~s~%" a)
  (format t "~&Length of the list: ~s~%" (my-length a))
  )
