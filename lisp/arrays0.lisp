#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(setq a '#('alpha "alpha" #\a 65)) 	;simplest way to create one
(format t "~&~s (~a)~%" a (type-of a))

(setq b (make-array 4 :initial-contents '('alpha "alpha" #\a 65))) ;a more formal way
(format t "~&~s (~a)~%" b (type-of b))

(assert (equalp a b))
