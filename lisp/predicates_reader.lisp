#!/usr/bin/env clisp

(defun line-reader (fh)
  (let ((ln (read-line fh nil)))
    (when ln
      (format t "~&~a~%" ln)
      (line-reader fh)
      )
    )
  )

(defun file-proc (fh)
  (line-reader fh)
  (close fh)
  )

(defun file-read-wrapper (fn)
  (let ((fh (open fn :if-does-not-exist nil)))
    (if fh
	(file-proc fh)
	(format t "~&Filename not found: ~s~%" fn)
      )    
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(file-read-wrapper "predicates.txt")

