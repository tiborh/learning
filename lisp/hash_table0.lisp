#!/usr/bin/env clisp
;;#!/usr/local/bin/sbcl --script
;;#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "hash_funcs.lisp")

(defconstant ABC '(a b c d e f g h i j k l m n o p q r s t u v w x y z))

(defun add-and-print (li ha)
  (print-count-size ha)
  (dolist (it li)
    (setf (gethash it ha) 0)
    (print-count-size ha)
    )
  )

(defun remove-and-print (li ha)
  (print-count-size ha)
  (dolist (it li)
    (remhash it ha)
    (print-count-size ha)
    )
  )

(defun grow-reduce-incremental (ha)
  (format t "~&rehash-size: ~a~%" (hash-table-rehash-size ha))
  (format t "~&rehash-threshold: ~a~%" (hash-table-rehash-threshold ha))
  (add-and-print abc ha)
  (write-line "--------------------")
  (remove-and-print abc ha)
  (write-line "")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(defvar b)

(setf b (make-hash-table))
(grow-reduce-incremental b)

(write-line "Same with predifined size")
(setf b (make-hash-table :size (length abc)))
(grow-reduce-incremental b)

(write-line "Same with predifined rehash size")
(setf b (make-hash-table :rehash-size (/ (length abc) 1.0)))
(grow-reduce-incremental b)

(write-line "Same with predifined threshold size")
(setf b (make-hash-table :rehash-threshold 1.0))
(grow-reduce-incremental b)

(defvar a)
(setf a (create-hash '(alpha beta gamma delta) 0))
(assert (= 4 (hash-table-count a)))
(print-count-size a)
(pprint a)
(setf a (add-to-hash '(alef bet gimel dalet he) a 1))
(assert (= 9 (hash-table-count a)))
(print-count-size a)
(pprint a)
(remhash 'he a)
(assert (= 8 (hash-table-count a)))
(print-count-size a)
(pprint a)
(assert (= 1 (gethash 'dalet a)))
(assert (null (gethash 'he a)))
(pprint (get-keys a))
(pprint (get-values a))
(remove-from-hash '(alpha beta gamma delta) a)
(assert (= 4 (hash-table-count a)))
(print-count-size a)
(pprint a)
(maphash #'(lambda (k v) (setf (gethash k a) (1+ v))) a)
(pprint a)
(loop for k being the hash-keys of a
     using (hash-value v)		; optional
   do (setf (gethash k a) (1+ v))
     )
(pprint a)

