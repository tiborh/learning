#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;#!/usr/bin/env clisp

(load "unit_test_v1_h.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(defun test-+ ()
  (check (= (+ -1 1) 0)
	 (= (+ 1 2) 3)
	 (= (+ 1 2) 4)
	 (= (+ (* 3 10) 3) 33)
	 )
  )

(test-+)
