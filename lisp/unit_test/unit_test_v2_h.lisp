;; source: Practical Common LIsp by Peter Seibel

(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a~%" result form)
  result
  )

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body)
  )

(defmacro combine-results (&body forms)
  (with-gensyms (result)
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result
       )
    )
  )

(defmacro check (&body forms)
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))
     )
  )
