#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;#!/usr/bin/env clisp

(load "unit_test_v0_h.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(defun test-+ ()
  (check (= (+ -1 1) 0))
  (check (= (+ 1 2) 3))
  (check (= (+ 1 2) 4))
  (check (= (+ (* 3 10) 3) 33))
  )

(test-+)
