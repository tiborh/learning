#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;#!/usr/bin/env clisp

(load "unit_test_v2_h.lisp")

(defun eval-result (func)
  (if (funcall func)
      (progn (princ "All tests have passed.")(terpri))
      (progn (princ "Fix is needed.")(terpri))
      )  
  )

(defun test0-+ ()
  (check (= (+ -1 1) 0)
	 (= (+ 1 2) 3)
	 (= (+ 1 2) 4)
	 (= (+ (* 3 10) 3) 33)
	 )
  )

(defun test1-+ ()
  (check (= (+ -1 1) 0)
	 (= (+ 1 2) 3)
	 (= (+ 1 3) 4)
	 (= (+ (* 3 10) 3) 33)
	 )
  )


    
;; (load "dtrace.generic")
;; (dtrace func)

(eval-result #'test0-+)
(eval-result #'test1-+)
