;; source: Practical Common LIsp by Peter Seibel

(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a~%" result form)
  )

(defmacro check (form)
  `(report-result ,form ',form)
  )
