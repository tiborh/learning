#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (write-line "No args. A natural number is expected.")
    (format t "~&~a~%" (parse-integer (reverse (car *args*))))
    )
