(defun read-list-from-file (filename)
  (let ((the-list nil))
    (with-open-file (fh filename
			:if-does-not-exist nil)
      (if (null fh)
	(format t "~&File does not exist: ~a~%" filename)
	(setf the-list (read fh))
	)
      )
    )
  )

(defun is-file-empty? (filename)
  (with-open-file (fh filename
		      :if-does-not-exist nil)
    (when (null fh)
      (let ((msg (concatenate 'string "File does not exist: " filename)))
	(error msg)
	)
      )
    (not (listen fh))
    )
  )

(defun write-list-to-file (list-of-items filename)
  (with-open-file (fh filename
		      :direction :output
		      :if-exists :supersede
		      ;; :if-exists :append
		      :if-does-not-exist :create
		      )
    (format fh "~&~s~%" (reverse list-of-items) fh)
    )
  (format t "~&Data has been written to: ~a~%" filename)
  )
