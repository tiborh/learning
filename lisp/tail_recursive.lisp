#!/usr/bin/env clisp

(load "my_length.lisp")

(load "dtrace.generic")
(dtrace lst-len)

(unless (null *args*)
  (my_length *args*)
  )
