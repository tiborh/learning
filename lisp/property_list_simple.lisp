#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(let ((person))
  (setf (get 'person 'name) 'Marge)
  (setf (get 'person 'place) '(New York))
  (setf (get 'person 'fav-book) 'Odyssey)

  (format t "~&Trying to print the whole: ~s~%" person) ; nill
  
  (format t "~&name: ~s~%" (get 'person 'name))
  (format t "~&place: ~s~%" (get 'person 'place))
  (format t "~&fav-book: ~s~%" (get 'person 'fav-book))

  )

(format t "~&終~%")
