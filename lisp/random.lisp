#!/usr/bin/env clisp

(defun cast-die (&optional (n_sides 6))
  (1+ (random n_sides (make-random-state t)))
  )

(dotimes (n 10)
  (format t "~&die 1: ~a~13tdie 2: ~a~%" (cast-die) (cast-die)
	  )
)
