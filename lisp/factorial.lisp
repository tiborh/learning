#!/usr/bin/env clisp

(defun factorial (n)
  (if (= n 0) 1
      (* n (factorial (- n 1)))))

(defun main (n)
  (loop for i from 0 to n
    do (format t "~&~d! ~3d~%" i (factorial i))
    )
)

(main (parse-integer (car *args*)))
