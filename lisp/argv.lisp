#!/usr/bin/env cl

(defun argv ()
  (or 
   #+CLISP *args*
   #+SBCL *posix-argv*  
   #+LISPWORKS system:*line-arguments-list*
   #+CMU extensions:*command-line-words*
   nil))

(format t "~&My command line: ~s~%" (argv))
