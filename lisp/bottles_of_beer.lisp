#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defparameter b "bottles")
(defparameter beer "of beer")
(defparameter on-wall "on the wall")

(defun sing-bottles (n)
  (when (= 1 n)
    (setf b "bottle")
    )
  (let ((n- (1- n)))
    (format t "~&~a ~a ~a ~a.~%" n b beer on-wall)
    (format t "~&~a ~a ~a.~%" n b beer)
    (write-line "Take one down.")
    (write-line "Pass it around.")
    (when (= 1 n-)
      (setf b "bottle")
      )
    (format t "~&~a ~a ~a ~a.~%" n- b beer on-wall)
    (format t "~%")
    )
  )

(defun bottles-of-beer (n)
  (cond ((= 0 n) (format t "~&No beer left.~%Time to go home.~%"))
	(t (progn (sing-bottles n)
		  (bottles-of-beer (1- n))
		  )
	   )
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (bottles-of-beer (parse-integer (car *args*)))
  )
