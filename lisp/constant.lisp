#!/usr/bin/env clisp

;; from https://www.tutorialspoint.com/lisp/lisp_constants.htm

(defconstant PI 3.141592)
(defun area-circle(rad)
   (terpri)
   (format t "Radius: ~5f" rad)
   (format t "~%Area: ~10f" (* PI rad rad)))
(area-circle 10)

