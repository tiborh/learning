#!/usr/bin/env clisp

(defun evens (li)
  (cond ((null li) nil)
	(t (if (evenp (car li))
	       (cons (car li) (evens (cdr li)))
	       (evens (cdr li))
	       )
	   )
	)
  )

(defun largest-even (li)
  (apply #'max (evens li))
  )

(defun to-integer-list (li)
  (cond ((null li) nil)
	(t (cons (parse-integer (car li)) (to-integer-list (cdr li))))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let ((inli (to-integer-list *args*))
	)
    (format t "~&~a => ~a~%" inli (largest-even inli))
    )
  )
