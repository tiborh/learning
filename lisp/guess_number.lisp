#!/usr/bin/env clisp

(defvar maxnum 1025)
(defvar mynum (random maxnum (make-random-state t)))

(format t "Guess my number (between 0 and ~d)." (- maxnum 1))

(loop
   (format t "~&Your guess: ")
   (let ((x (read)))
     (cond ((< x mynum)
	    (format t "~&Make it more.~%")
	    )
	   ((> x mynum)
	    (format t "~&Make it less.~%")
	    )
	   ((= x mynum)
	    (format t "~&That's it!~%My number was ~a.~%" mynum)
	    (return x)
	    )
	   (t (return nil))
	   )
     )
   )
