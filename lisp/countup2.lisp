#!/usr/bin/env clisp

(defun countup (n &optional (c 0))
  (labels ((countup-recurse (c)
	  (cond ((< n c) nil)
		(t (cons c (countup-recurse (1+ c))))
		)
	  ))
    (countup-recurse c)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (if (= 2 (length *args*))
      (format t "~&~a~%" (countup (parse-integer (car *args*)) (parse-integer (cadr *args*))))
      (format t "~&~a~%" (countup (parse-integer (car *args*))))
      )
  )
