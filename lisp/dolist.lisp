#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun make-a-list (n)
  (let ((a (list)))
    (dotimes (i 10 (nreverse a))
      (setf a (cons (1+ i) a))
      )
    )
  )

(dolist (n (make-a-list 10))
  (dolist (m (make-a-list 10))
    (format t "~4d" (* n m))
    )
  (format t "~&")
  )
