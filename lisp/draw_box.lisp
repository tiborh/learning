#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "draw_tools.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(when (= 2 (length *args*))
  (draw-box (parse-integer (car *args*))
	    (parse-integer (cadr *args*))
	    )
  )
