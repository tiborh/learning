#!/usr/bin/env clisp

(defun line-reader (fh)
  (do ((ln (read-line fh nil) (read-line fh nil))
       ;;(ls nil (append ls (list ln)))
       (ls nil (push ln ls))
       )
      ((null ln) ls)
    ;;(format t "~&~a~%" ln)
    )
  )

(defun file-read-wrapper (fn)
  (let ((fh (open fn :if-does-not-exist nil))
	(ls nil)
	)
    (if fh
	(progn
	  (setf ls (line-reader fh))
	  (close fh)
	  )
	(format t "~&Filename not found: ~s~%" fn)
	)
    (return-from file-read-wrapper ls)
    )
  )

;; (load "dtrace.generic")
;; (dtrace line-reader)

(if (null *args*)
    (loop
       (let ((inp (read-line *query-io* nil nil))) ; error handling turned off to avoid:
	 ;; *** - READ: input stream #<INPUT UNBUFFERED FILE-STREAM CHARACTER #P"/dev/fd/0" @4> has reached its end
	 (if (null inp)
	     (return nil)
	     (format t "~&~a~%" inp)
	     )
	 )
       )
    (let ((ls (file-read-wrapper (first *args*))))
      (format t "~&List length: ~s~%" (length ls))
      (format t "~&First elem: ~s~%" (car ls))
      (format t "~&Last elem: ~s~%" (car (last ls)))
      (unless (< 100 (length ls))
	(format t "~&~s~%" ls)
	(write-line "reverse:")
	(format t "~&~s~%" (reverse ls))
	)
      )
  )
