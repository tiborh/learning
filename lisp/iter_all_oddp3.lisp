#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; book example
(defun all-oddp (li-o-nu)
  (do ((li li-o-nu (cdr li)))
      ((null li) t)
    (when (evenp (first li))
      (return nil)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (all-oddp nil))			;different from own interpretation, where nill is not odd
(assert (all-oddp '(1)))
(assert (null (all-oddp '(2))))
(assert (null (all-oddp '(1 3 5 6 7))))
(assert (all-oddp '(1 3 5 7 9)))

