#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun counter ()
  (let ((b 9))
    (format t "~&b == ~s~%" b)
    (do ((a 0 (1+ a)))			; for (this part ; ; and this part)
	((= a b) a)				; for ( ; this part ; ) + a return value
      (format t "~&~ca == ~s~%" #\Tab a)
      )
    )
  )

(let ((a (counter)))
  (format t "~&returned: ~s~%" a)
  )

(format t "~&終~%")
