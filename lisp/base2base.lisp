#!/usr/bin/env clisp

(load "numconv_func.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(if (= (length *args*) 3)
    (progn
      (let ((num (car *args*))
	     (from-base (parse-integer (cadr *args*)))
	     (to-base (parse-integer (caddr *args*)))
	    )
	(format t "~&~a~%" (dec2other (other2dec num from-base) to-base))
	)
      )
    (format t "~&Usage cmd <number> <from base> <to base>~%")
    )
