#!/usr/local/bin/sbcl --script
;;#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "~/quicklisp/setup.lisp")
(ql:quickload :psgraph)

;; (load "dtrace.generic")
;; (dtrace func)

;; Set some drawing settings
(setf psgraph:*boxkind* "fill")
(setf psgraph:*boxgray* ".8")
(setf psgraph:*fontsize* 8)
(setf psgraph:*second-fontsize* 6)

;; This function will be given the name of a node, like 'A, and
;; return a list of the node's children.
(defun children (x)
  (cond ((eq x 'A) '(B C D))
	((member x '(B C D)) '(E F G))
	((member x '(E F G)) '(H))
	(t nil)))

;; This returns the information to be displayed for a node.
(defun info (x)
  (list (string x)))

;; This is the top-level function
(defun graph (&optional (shrink t))
  (with-open-file (output-stream "g.ps"
				 :direction :output
				 :if-exists :supersede)
    (psgraph:psgraph output-stream 'A #'children #'info shrink nil #'eq)))

(graph)
