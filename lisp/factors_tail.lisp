#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun divider (num start collector)
;;  (format t "~&collector: ~a~%" collector)
  (let ((quotient (/ num start)))
    (cond ((< quotient 2) (setf collector (append collector (list num))))
  	  ((integerp quotient)
	   (setf collector (append collector (list start)))
	   (divider quotient 2 collector)
	  )
  	  (t (divider num (1+ start) collector))
	  )
    )
  )


(defun factors (num)
  (divider num 2 nil)
  )

;; (load "dtrace.generic")
;; (dtrace divider)

(unless (null *args*)
  (format t "~&~a -> ~a~%" (car *args*) (factors (parse-integer (car *args*))))
  )
