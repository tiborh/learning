#!/usr/bin/env clisp
;; example from Common Lisp by David S. Touretzky

(defvar key 'ein)

(defun helper (entry)
  (equal key (first entry))
  )

(defun my-assoc (key table)
  (find-if #'(lambda (entry)
	       (equal key (first entry)))
	   table)
  )

(defun my-assoc2 (key table)
  (find-if #'helper table)
  )

(setf words
      '((one   ein)
	(two   zwei)
	(three drei)
	(four  vier)
	(five  fünf))
      )

(format t "~&words: ~s~%" words)
(format t "~&look up 'three (in my-assoc) : ~s~%" (my-assoc  'three words))
(format t "~&look up 'three (in my-assoc2): ~s~%" (my-assoc2 'three words))

