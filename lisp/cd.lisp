#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(require :sb-posix)
(if *args*
    (progn
      (sb-posix:chdir (car *args*))
      (format t "~&Changed to ~s~%" (sb-posix:getcwd))
      (write-line "(In the subshell only, of course.)")
      )
    (write-line "Where to?")
    )
