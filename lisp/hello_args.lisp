#!/usr/bin/env cl

(load "cl-argv.lisp")
(defvar *argv* (argv))

; a comment
(write-line "Hello, world!")

(if (> (length *argv*) 1)
    (format t "~&Command line arguments:~%~t~s~%" *argv*)
    (format t "~&No command line arguments.~%")
    )
