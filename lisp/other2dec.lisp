#!/usr/bin/env clisp

(load "numconv_func.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

;; (format t "~&~s~%" basepairs)

(when *args*
  (if (> (length *args*) 1)
      (format t "~&~a~%" (other2dec (car *args*) (parse-integer (cadr *args*))))
      (format t "~&~a~%" (other2dec (car *args*)))
      )
  )
