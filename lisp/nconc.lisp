#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(let ((a '(1 2 3))
      (b '(4 5 6))
      (c nil)
      )
  (format t "~&a: ~a~%" a)
  (format t "~&b: ~a~%" b)
  (format t "~&c: ~a~%" c)
  (format t "~&append a b: ~a~%" (append a b))
  (format t "~&a: ~a~%" a)
  (format t "~&nconc a b: ~a~%" (nconc a b))
  (format t "~&a: ~a~%" a)
  (format t "~&nconc c b: ~a~%" (nconc c b))
  (format t "~&c: ~a~%" c)
)
