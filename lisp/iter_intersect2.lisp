#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "iter_funcs.lisp")

(defun iter-intersect (l0 l1)
  (do* ((li l0 (cdr li))
	(it (car li) (car li))
	(colli nil)
	)
       ((null li)(reverse colli))
    (when (iter-member it l1)
      (push it colli)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (iter-intersect nil nil)))
(assert (null (iter-intersect nil '(1 2 3))))
(assert (null (iter-intersect '(1 2 3) nil)))
(assert (null (iter-intersect '(a b c) '(d e f))))
(assert (equal '(c d) (iter-intersect '(a b c d) '(c d e f))))
(assert (equal '(a b) (iter-intersect '(a b c d) '(e f b a))))
