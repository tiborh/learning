#!/usr/bin/env -S sbcl --script

;; This
(format t "~&~s~%" (let ((x 2))
  (+ x x)
  ))
;; is equivalent with
(format t "~&~s~%" ((lambda (x) (+ x x))
		    2)
	)

(format t "~&終~%")
