#!/usr/bin/env -S sbcl --script
(load "my-utils.lisp")
(load "sedge-reader.lisp")
(load "sedge-graphviz-writer.lisp")

(defun usage.info ()
  (write-line "Usage:")
  (format t "~&~c~a <filename of sedgewick list>~%" #\Tab (my-utils::script-filename))
  )
  
(if (null +args+)
    (usage.info)
    (let* ((sl (sedge-reader::file.read.wrapper (first +args+)))
	   (len (length (sedgelist-vertices sl)))
	   )
      ;; (format t "~&Number of vertices: ~s~%" (sedgelist-nu.vertices sl))
      ;; (format t "~&Number of edges: ~s~%" (sedgelist-nu.edges sl))
      ;; (format t "~&Edge list length: ~d~%" len)
      ;; (format t "~&First edge: ~s~%" (car (sedgelist-vertices sl)))
      ;; (format t "~&Last edge: ~s~%" (nth (1- len) (sedgelist-vertices sl)))
      ;;(format t "~&~s~%" sl) ; to be commented out for huge files
      (if (and (< 1 (length +args+)) (string= "digraph" (second +args+)))
	  (sedge-graphviz-writer::draw.digraph (sedgelist-vertices sl))
	  (sedge-graphviz-writer::draw.graph (sedgelist-vertices sl))
	  )
      )
    )

