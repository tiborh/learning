#!/usr/bin/env clisp

(load "tree_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((base 'tree-init)
       (a (cons base '('(((Goldilocks . and)) (the . 3) bears) 'q)))
       (b (cons base '('(a . b) 1)))
       (c (cons base '('(hark (harold the herald) barks) 'bark)))
      )
  (format t "~&~a: ~a~%" a (eval a))
  (format t "~&~a: ~a~%" b (eval b))
  (format t "~&~a: ~a~%" c (eval c))
  )
