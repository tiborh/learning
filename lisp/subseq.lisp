#!/usr/bin/env -S sbcl --script

(let* ((a "Abigail")
       (b (concatenate 'list a))
       )
  (format t "~&~s~%" (subseq a 1 4))
  (format t "~&~s~%" (subseq b 1 4))
  (format t "~&~s~%" (concatenate 'string (subseq b 1 4)))
  )

(format t "~&終~%")
