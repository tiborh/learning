#!/usr/bin/env clisp

(defun bin2dec-conv (char-list &optional (n2 0))
  (cond ((null char-list) 0)
	(t (+ (* (parse-integer (coerce (list (car char-list)) 'string)) (expt 2 n2))
	      (bin2dec-conv (cdr char-list) (1+ n2))))
	)
  )

(defun bin2dec (s)
  (bin2dec-conv (coerce (reverse (string-upcase (first *args*))) 'list))
  )

;; (load "dtrace.generic")
;; (dtrace func)

(when *args*
  (format t "~&~a~%" (bin2dec (car *args*)))
  )
