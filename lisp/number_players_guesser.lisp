(defun number-guesser (minN maxN)
  ;;(format t "~&min: ~d, max: ~d~%" minN maxN)
  (let ((ansN (+ minN (ash (- maxN minN) -1)))
	)
    (format t "~&Is it ~d?~%" ansN)
    (return-from number-guesser ansN)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

