#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun unfinished-business ()
  (cerror "This function has been left unfinished."
	  "It will need to be completed somehow.")
  (write-line "End of the function.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unfinished-business)
