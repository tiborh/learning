#!/usr/bin/env clisp

(defun countup-tail (n c li)
  (cond ((< n c) li)
	(t (countup-tail n (1+ c) (append li (list c))))
	)
  )

(defun countup (n &optional (c 0))
  (countup-tail n c nil)
  )

(load "dtrace.generic")
(dtrace countup-tail)

(unless (null *args*)
  (if (= 2 (length *args*))
      (format t "~&~a~%" (countup (parse-integer (car *args*)) (parse-integer (cadr *args*))))
      (format t "~&~a~%" (countup (parse-integer (car *args*))))
      )
  )
