#!/usr/bin/env clisp

(defun pv (name comment value)
  (format t "~&~4t~a (~a):~18t~s~%" name comment value)
  )

(format t "~&(incf var) and (decf var) changes the variable~%(just like ++var and --var do in C):~%")
(let ((a 0))
  (pv "a" "initial" a)
  (incf a)
  (pv "a" "incf a" a)
  (incf a 3)
  (pv "a" "incf a 3" a)
  (decf a)
  (pv "a" "decf a" a)
  (decf a 3)
  (pv "a" "decf a 3" a)
  )
