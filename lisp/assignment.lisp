#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun play-with-var ()
  (incf *a*)
  (format t "~&*a*: ~a (inside function)~%" *a*)
  )

(defun play-with-var-again (a-var)
  (incf a-var)
  (format t "~&*a*: ~a (passed to func as an arg)~%" a-var)
  )

(defun play-with-var-again2 (a-var)
  (set 'a-var 5)			; set needs a symbol
  (format t "~&a-var: ~a (set sets a symbol, not the original var)~%" a-var)
  (format t "~&(symbol-value of 'a-var): ~a~%" (symbol-value 'a-var))
  )


(defun def-again ()
  (defvar *a* nil)
  (format t "~&*a*: ~a (defining again has no effect)~%" *a*)
  )

(defun def-again-par ()
  (defparameter *b* nil)
  (format t "~&*a*: ~a (defining again has effect)~%" *b*)
  )


;; (load "dtrace.generic")
;; (dtrace func)

(write-line "use setq only for experiments in repl")
(write-line "setq needs a symbol as first parameter (expression is not accepted)")
(write-line "defvar without parameter creates a variable with no value.")
(defvar *a*)
;; (format t "~&~a~%" *a*) ; *** - EVAL: variable NO-VAL has no value
(write-line "setf (set field) can give value to a variable")
(write-line "to follow good style, use it with vars with no or nil value")
(setf *a* 1)				; for global variables give it "ears"
(format t "~&*a*: ~a~%" *a*)
(play-with-var)
(format t "~&*a*: ~a (after function)~%" *a*)
(def-again)
(play-with-var-again *a*)
(format t "~&*a*: ~a (after function)~%" *a*)
(play-with-var-again2 *a*)
(format t "~&*a*: ~a (after function)~%" *a*)
(write-line "lets do something similar with a parameter (defparameter) too")
(defparameter *b* nil)			; cannot be used with one parameter only
(format t "~&*b*: ~b~%" *b*)
(setf *b* 1)
(format t "~&*b*: ~b~%" *b*)
(def-again-par)
(format t "~&*b*: ~b (after function)~%" *b*)
