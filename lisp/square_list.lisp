#!/usr/bin/env clisp

(defun square-list (li)
  (cond ((null li) nil)
	(t (cons (expt (parse-integer (car li)) 2) (square-list (cdr li))))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (format t "~&<cmd> <num1> [<num2> ...]~%")
    (format t "~&~a~%" (square-list *args*))
    )
