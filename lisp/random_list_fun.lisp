(defun make-rand-list (li len-ceil num-ceil &optional (rand-length nil) (sorted nil))
  (let ((len (if rand-length
		 (random len-ceil (make-random-state t))
		 len-ceil
		 )
	  )
	)
    (dotimes (i len)
      (push (random num-ceil (make-random-state t)) li)
      )
    )
  (if sorted
      (sort li #'<)
      li
      )
  )
