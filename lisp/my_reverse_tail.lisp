#!/usr/bin/env clisp

(load "my_reverse_lib.lisp")

(load "dtrace.generic")
(dtrace my-rev-t)

(unless (null *args*)
  (format t "~&~a~%" (my-reverse-tail *args*))
  )
