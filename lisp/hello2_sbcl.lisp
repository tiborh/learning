#!/usr/bin/env -S sbcl --script

(load "hello_func.lisp")

;; a comment
(let ((name (cadr *posix-argv*)))
  (if name
      (hello name)
      (hello)
      )
  )
