#!/usr/bin/env clisp

(defun fib (n)
  (cond ((< n 3) 1)
	(t (+ (fib (- n 2))
	      (fib (- n 1))
	      )
	   )
	)
  )

(load "fib_lib.lisp")

;;(load "dtrace.generic")
;;(dtrace proc-args)

(if (null *args*)
    (format t "~&No args.~%")
    (proc-args *args*)
    )
