#!/usr/bin/env clisp

(defun to-bin-list (n bl)
  (setq bl (cons (mod n 2) bl))
  (cond ((< n 2) bl)
	(t (to-bin-list (floor n 2) bl))
	)
  )

;; (load "dtrace.generic")
;; (dtrace to-bin-list)

(if (null *args*)
    (format t "No args.")
    (let ((n (parse-integer (first *args*)))
	  )
      (assert (>= n 0) (n) "does not work with negative numbers: ~d" n)
      (format t "~&~a~%" (to-bin-list n (list)))
      )
)
