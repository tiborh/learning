#!/usr/bin/env clisp

(defun line-reader (fh)
  (do ((ln (read-line fh nil) (read-line fh nil)))
      ((null ln))
    (format t "~&~a~%" ln)
    )
  )

(defun file-read-wrapper (fn)
  (let ((fh (open fn :if-does-not-exist nil)))
    (if fh
	(progn
	  (line-reader fh)
	  (close fh)
	  )
	(format t "~&Filename not found: ~s~%" fn)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace line-reader)

(if (null *args*)
    (loop
       (let ((inp (read-line *query-io* nil nil))) ; error handling turned off to avoid:
	 ;; *** - READ: input stream #<INPUT UNBUFFERED FILE-STREAM CHARACTER #P"/dev/fd/0" @4> has reached its end
	 (if (null inp)
	     (return nil)
	     (format t "~&~a~%" inp)
	     )
	 )
       )
    (file-read-wrapper (first *args*))
  )
