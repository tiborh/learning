#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun no-args()
  (format t "~&No args have been provided.~%")
  )

(defun unhandled(typ)
  (format t "~&Unhandled type: ~s~%" typ)
  )

(defun proc-cons(lst)
  (format t "~&Placeholder for list processing~%~s~%" lst)
  )

(defun list-as-arg(inp)
  (if (null inp)
      (no-args)
      (if (eq 'CONS (type-of inp))
	  (proc-cons inp)
	  (unhandled(inp))
	  )
      )
  )

;;(format t "~&type of args: ~s~%" (type-of *args*))
(list-as-arg *args*)
  
(format t "~&終~%")
