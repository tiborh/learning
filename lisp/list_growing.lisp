#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defconstant FN "list_growing.txt")
(load "file_io_funcs.lisp")

(defun add-user-input-to-list (list)
  (write-line "Your input (one word/string only): ")
  (push (read) list)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((filename FN)
       (my-list nil)
       )
  (if (is-file-empty? FN)
      (push "empty" my-list)
      (setf my-list (read-list-from-file filename))
      )
  (terpri)
    
  (format t "~&The current list: ~a~%" my-list)
  (if (y-or-n-p "Would you like to add anything to the list?")
      (progn 
	(setf my-list (add-user-input-to-list my-list))
	(unless (null my-list)
	  (write-list-to-file my-list filename)
	  )
	)
      (write-line "Nothing to do this time.")
      )
  (write-line "Bye!")
  )
