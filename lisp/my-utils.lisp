(defpackage :my-utils
	      (:use :common-lisp :common-lisp-user)
	      (:export :+args+)
	      )
(defconstant +args+ (cdr *posix-argv*))
(defun my-utils::timestamp (&key (date-sep "-")(dt-sep "_")(time-sep ":"))
  (multiple-value-bind
	(second minute hour date month year)
      (get-decoded-time)
    (format nil "~d~a~2,'0d~a~2,'0d~a~2,'0d~a~2,'0d~a~2,'0d"
    	    year
	    date-sep
	    month
	    date-sep
	    date
	    dt-sep
	    hour
	    time-sep
	    minute
	    time-sep
	    second
    )
  )
  )
(defun my-utils::to-str (lst)
  (concatenate 'string (reverse lst))
  )
(defun my-utils::add-to-lst (elem lst)
    (unless (= 0 (length elem))
      (push (my-utils::to-str elem) lst)
      )
  lst
  )
(defun my-utils::string-split (str chr)
  (let ((str-lst (concatenate 'list str))
	(lst-out nil)
	(tmp-lst nil)
	)
    (dolist (c str-lst)
      (if (char= c chr)
	  (progn
	    (setf lst-out (my-utils::add-to-lst tmp-lst lst-out))
	    (setf tmp-lst nil)
;;	    (format t "~&match: ")
	    )
	  (push c tmp-lst)
	  )
;;      (format t "'~c'~%" c)
      )
    (setf lst-out (my-utils::add-to-lst tmp-lst lst-out))
    (reverse lst-out)
    )
  )
(defun my-utils::script-filename ()
  (car (last (my-utils::string-split (native-namestring *load-truename*) #\/)))
  )

(defun my-utils::usage-info (args)
  (format t "~&~a ~a~%" (my-utils::script-filename) args)
  )

(defun my-utils::range (n &key (start 0) (step 1))
  (when (minusp n)
    (warn "Number of items cannot be negative.")
    (quit)
    )
  (do ((i 0 (1+ i))
       (item start (+ item step))
       (result nil (push item result))
       )
      ((= i n) (reverse result))
    )
  )

(defun my-utils::from-to-int-to-char-list (i0 i1)
  (cond ((= i0 i1) nil)
	(t (cons (code-char i0) (my-utils::from-to-int-to-char-list (1+ i0) i1)))
	)
  )

(defun my-utils::capitals (&key (start "A") (end "Z"))
  (my-utils::from-to-int-to-char-list (char-code (char start 0)) (1+ (char-code (char end 0))))
  )

(defun my-utils::alphabet (&key (start "a") (end "z"))
  (mapcar 'string-downcase (my-utils::capitals :start start :end end))
  )
