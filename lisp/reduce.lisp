#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a (list 1 2 3 4))
       (b (list 5 6 7 8))
       (c (list a b '(9 10)))
       )
  (format t "~&a:            ~a~%" a)
  (format t "~&add elements: ~a~%" (reduce #'+ a))
  (format t "~&b:            ~a~%" b)
  (format t "~&add elements of a and b: ~a~%" (apply #'+ (reduce #'+ a) b))
  ;;first apply cannot be replaced with reduce (msg: REDUCE:
  ;; keyword arguments in ((5 6 7 8)) should occur pairwise)
  (format t "~&embedded list: ~a~%" c)
  (format t "~&addition with append and reduce: ~a~%" (reduce #'+ (reduce #'append c)))
  )
