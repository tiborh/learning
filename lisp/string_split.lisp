#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun string-to-chars (str)
  (concatenate 'list str)
  )

(defun chars-to-string (chars)
  (concatenate 'string chars)
  )

(defun proc-args (list)
  (if (null list)
      nil
      (cons (string-to-chars (car list)) (proc-args (cdr list)))
      )
  )

(defun reassemble (list)
  (if (null list)
      nil
      (cons (chars-to-string (car list)) (reassemble (cdr list)))
      )
  )

(defun concat (list)
  (if (null list)
      nil
      (concatenate 'string (car list) (concat (cdr list)))
      )
  )

(format t "~&~s~%" *args*)
(format t "~&~s~%" (proc-args *args*))
(format t "~&~s~%" (reassemble (proc-args *args*)))
(format t "~&~s~%" (concat (reassemble (proc-args *args*))))

(format t "~&終~%")
