#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a "\"a quoted string\"")
       (b "an unquoted string")
       (c (list))
      )
  (format t "~&the string: ~a~%" a)
  (format t "~&the result of read-from-string: ~a~%" (read-from-string a))
  (format t "~&the string: ~a~%" b)
  (format t "~&the result of read-from-string: ~a~%" (read-from-string b))
  (format t "~&read second: ~a~%" (read-from-string b t nil :start 2))
  (format t "~&read third: ~a~%" (read-from-string b t nil :start 11))
  (format t "~&type-of read: ~a~%" (type-of (read-from-string b t nil :start 11)))
  )
