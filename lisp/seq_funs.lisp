(defun fill-with (seq start &optional (step 1))
  ;; fill in seq with integers from start incremented by step
  (loop for i from 0 to (1- (length seq))
       do (setf (elt seq i) start)
;;       do (fill seq start :start i :end (1+ i))
       (setq start (+ start step))
       )
  (return-from fill-with seq)
  )
