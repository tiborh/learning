#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(let ((b 9))
  (format t "~&b == ~s~%" b)
  (do ((a 0 (1+ a)))			; for (this part ; ; and this part)
      ((= a b))				; for ( ; this part ; )
    (format t "~&~ca == ~s~%" #\Tab a)
    )
)
  
(format t "~&終~%")
