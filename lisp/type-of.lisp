#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun print-type(v)
  (format t "~&~s ~s~%" v (type-of v))
  )

(let
    ((x 10)
     (y 34.567)
     (ch nil)
     (n 123.78)
     (bg 11.0e+4)
     (r 124/2)
     (s "string")
     )
  (print-type x)
  (print-type y)
  (print-type ch)
  (print-type n)
  (print-type bg)
  (print-type r)
  (print-type s)
  (print-type #\s)
  (print-type #2A((1 2 3)(4 5 6)))
  )

(format t "~&終~%")
