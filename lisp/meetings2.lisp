#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defvar *acquaintances* nil)

(defun meet (person)
  "book example"
  (cond ((equal person (caar *acquaintances*))
	 (prog1
	     (list 'we-have-just-met (cadr (assoc person *acquaintances*)))
	   (incf (cadar *acquaintances*))
	   )
	 )
	((assoc person *acquaintances*)
	 (prog1
	     (list 'we-know-each-other (cadr (assoc person *acquaintances*)))
	   (incf (cadr (assoc person *acquaintances*)))
	   )
	 )
	(t (progn
	     (push (list person 1) *acquaintances*)
	     (list 'we-have-not-yet-met 0)
	     )
	   )
	)
  )

(defun greeter (person)
  (format t "~&Hello, ~a!~%" person)
  (let ((ret-pair (meet person)))
    (when (< 0 (cadr ret-pair))
      (if (= 1 (cadr ret-pair))
	  (write-line "We have met once.")
	  (format t "~&We met ~a times before.~%" (cadr ret-pair))
	  )
      )
    (car ret-pair)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(format t "~&~a~%" (greeter 'fred))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'joe))
(format t "~&~a~%" (greeter 'fred))
(format t "~&~a~%" (greeter 'joe))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" (greeter 'joe))
(format t "~&~a~%" (greeter 'cindy))
(format t "~&~a~%" *acquaintances*)
