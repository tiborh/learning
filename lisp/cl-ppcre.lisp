#!/usr/bin/env cl

(load "cl-argv.lisp")
(defvar *argv* (argv))

;; does not work
;; (require "ppcre")
;; (format t "~&~s~%" (ppcre:split "\\s+" "foo   bar baz"))

;; neither does this:
;; (ql:quickload "cl-ppcre")

;; but hopeful way ahead:
;; https://ebzzry.io/en/script-lisp/

;; or
;; https://stackoverflow.com/questions/25858237/confused-about-qlquickload-and-executable-scripts-in-sbcl
