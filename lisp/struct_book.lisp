#!/usr/bin/env -S sbcl --script

;; source:
;; https://www.tutorialspoint.com/lisp/lisp_structures.htm

(defstruct book 
   title 
   author 
   subject 
   book-id 
)

(defun book-printer (book)
  (format t "~&    Title: ~a~%" (book-title book))
  (format t "~&   Author: ~a~%" (book-author book))
  (format t "~&  Subject: ~a~%" (book-subject book))
  (format t "~&       ID: ~a~%" (book-book-id book))
  )

;; implicit functions defined:
;;   book-... (for each member)
;;   book-p (to determine if it is a member of the book class)
;;   make-book (to create the book)
;;   copy-book (to make a copy of the book)

(let* (
       (book1 (make-book :title "C Programming"
			 :author "Nuha Ali" 
			 :subject "C-Programming Tutorial"
			 :book-id "478")
	 )
       (book2 (make-book :title "Telecom Billing"
			 :author "Zara Ali" 
			 :subject "C-Programming Tutorial"
			 :book-id "501")
	 )
       (book3 (copy-book book1))
       )
  (format t "~&Book1:~%")
  (write-line "Raw:")
  (write book1)
  (fresh-line)
  (write-line "with book-printer:")
  (book-printer book1)
  (format t "~&Book2:~%")
  (book-printer book2)
  (format t "~&Book3 (book1 copied, id changed):~%")
  (setf (book-book-id book3) 100) 
  (book-printer book3)
  (write-line "To check if independent of book1 object:")
  (format t "~&id of book1: ~s~%" (book-book-id book1))
  )
  (format t "~&終~%")
