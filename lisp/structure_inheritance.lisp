#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun print-land-animal (x stream depth) ;will be used as a print function/method
  (format stream "#<land-animal ~a>"
	  (land-animal-name x)
	  )
  )

(defstruct animal
  (name nil)
  (condition 'extinct)
  )

(defstruct (animal-class (:include animal)) ;inheritance
  (insect nil)
  (fish nil)
  (amphibian nil)
  (reptile nil)
  (mammal nil)
  (bird nil)
  )

(defstruct (water-animal (:include animal-class))
  (fins 0)
  (scales nil)
  )

(defstruct (land-animal (:print-function print-land-animal)(:include animal-class)) ;print function connected
  (legs 0)
  (wings 0)
  )

;; (load "dtrace.generic")
;; (dtrace func)

;; instantiation
(setf a1 (make-animal-class :name "frog"
			    :condition 'extant
			    :amphibian t
			    )
      )

(setf a2 (make-water-animal :name "shark"
			    :condition 'extant
			    :fish t
			    :fins 8
			    :scales t
			    )
      )

(setf a3 (make-land-animal :name "elephant"
			   :condition 'extanct
			   :mammal t
			   :legs 4
			   )
      )

(format t "~&~a~%" a2)
(format t "~&~a~%" a3)
(format t "~&Is ~a an animal? ~a~%" (animal-name a1) (animal-p a1))
(format t "~&Is ~a a water animal? ~a~%" (animal-name a1) (water-animal-p a1))
(format t "~&Is ~a a water animal? ~a~%" (animal-name a2) (water-animal-p a2))
(format t "~&Type-of ~a? ~a~%" (animal-name a2) (type-of a2))

