#!/usr/bin/env clisp

;; from http://clhs.lisp.se/Body/f_terpri.htm

(defun with-terpri ()
  (format t "with terpri:~%")
  (write-string "some text")
  (terpri)
  (terpri)
  (write-string "more text"))

(defun with-fresh-line ()
  (format t "with fresh-line:~%")
  (write-string "some text")
  (fresh-line)
  (fresh-line)
  (write-string "more text"))

(with-terpri)
(fresh-line)
(format t "----------~%")
(with-fresh-line)
