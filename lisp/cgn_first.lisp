#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "ltk/ltk")
(load "cgn/cgn")
;;(in-package :ltk)
(in-package :cgn)

;; (load "dtrace.generic")
;; (dtrace func)

(with-gnuplot ( 'linux )
   (plot-function "cos(x)*sin(x)**2" )
   (set-range 'x 0 10)
   (set-range 'y -5 5)
   (set-grid 'on)
   (set-grid 'off)
   (set-title "My graphic")
   ;Plots a scatter graphic with xy errobars
   (plot-points '(1 2 3) '(2 4 5) :x_error '(0.1 0.2 0.3) :y_error '(0.2 0.4 0.5))
   ;You can save a postscript copy of the graphic
   (postscript-copy "cgn_firs.ps" )
   ;Now you can print you graphics
   (print-graphic )
   ;And finally we save the session
;;   (save-cgn "cgn_first_session.cgn" )
   )
