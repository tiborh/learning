#!/usr/local/bin/sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;#!/usr/bin/env clisp

;; #!/usr/local/bin/sbcl --script
;; (defconstant *args* (cdr *posix-argv*))

(load "list_merge_fun.lisp")
(load "random_list_fun.lisp")

(defun sort-two (n1 n2)
  (if (> n1 n2)
      (list n2 n1)
      (list n1 n2)
      )
  )

(defun merge-sort (li)
  (cond ((null li) nil)
	((= (length li) 1) (list (car li)))
	((= (length li) 2) (sort-two (car li) (cadr li)))
	(t (merge-lists (merge-sort (list (car li) (cadr li)))
			(merge-sort (cddr li))
			)
	   )
	)
  )

;; (load "dtrace.generic")
;; (dtrace merge-sort)


(let ((li-len 10)
      (ceil 100)
      (li nil)
      (li-sorted nil)
      )
  (if (> (length *args*) 0)
      (setf li-len (parse-integer (car *args*)))
      )
  (if (> (length *args*) 1)
      (setf ceil (parse-integer (cadr *args*)))
      )
  (setf li (make-rand-list li li-len ceil t))
  (format t "~&list: ~a~%" li)
  (setf li-sorted (merge-sort li))
  (format t "~&sorted list: ~a~%" li-sorted)
  )

(princ "終")(terpri)
