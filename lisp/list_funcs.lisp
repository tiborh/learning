(defun make-a-list (n &optional (s 'X))
  (when (> n 0)
    (cons s (make-a-list (1- n) s))
    )
  )

(defun make-a-seq (n &optional (start-from 1) (next-elem #'1+))
  (when (> n 0)
    (cons start-from (make-a-seq (1- n) (funcall next-elem start-from) next-elem))
    )
  )

(assert (equal (make-a-list 3) '(X X X)))
(assert (equal (make-a-list 3 1) '(1 1 1)))
(assert (equal (make-a-seq 3) '(1 2 3)))
(assert (equal (make-a-seq 4 1 (lambda (x) (* 2 x))) '(1 2 4 8)))

