#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun first-non-int (inli)
  (do ((li (cdr inli) (cdr li))
       (it (car inli) (car li))
       )
      ((not (integerp it)) it)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (first-non-int nil)))
(assert (null (first-non-int '(1 2 3))))
(assert (equal #\a (first-non-int '(1 2 3 #\a))))
(assert (equal #\a (first-non-int '(#\a 1 2 3))))
(assert (equal #\a (first-non-int '(0 #\a 1 2 3))))
(assert (equal #\a (first-non-int '(1 2 #\a 3))))
(assert (null (first-non-int '(1 2 nil 3))))
(assert (equal #\a (first-non-int '(1 2 #\a 3 #\b))))
