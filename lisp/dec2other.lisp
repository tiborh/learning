#!/usr/bin/env clisp

(load "numconv_func.lisp")
  
;; (load "dtrace.generic")
;; (dtrace dec-to-other)

(if (null *args*)
    (write-line "No args. A natural number is expected.")
    (write-line (if (> (length *args*) 1)
		    (dec2other (parse-integer (car *args*)) (parse-integer (cadr *args*)))
		    (dec2other (parse-integer (car *args*)))
		    )
		)
    )
    
