#!/usr/bin/env clisp

(defun my-mapcr (func li liu)
  (cond ((null li) liu)
	(t (my-mapcr func (cdr li)
		     (append liu (list (funcall func (car li)))))
	   )
	)
  )

(defun my-mapcar (func li)
  (my-mapcr func li nil)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(my-mapcar #'oddp '(1 2 3 4 5 6 7)))
      (cmdline2 '(my-mapcar #'1+ '(1 2 3 4 5 6 7)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  (format t "~&~a: ~a~%" cmdline2 (eval cmdline2))
  )
