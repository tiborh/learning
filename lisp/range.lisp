#!/usr/bin/env -S sbcl --script
;; (defconstant +args+ (cdr *posix-argv*))
(load "my-utils.lisp") ; alternatively
;; source: http://dnaeon.github.io/generating-sequences-in-common-lisp/

(unless (< 0 (length +args+))
  (my-utils::usage-info "<number of items> [start (default 0)] [step (default 1)]" )
  (quit)
  )    

(let* ((nargs (length +args+))
       (n (parse-integer (car +args+)))
       (start (if (< 1 nargs)
		  (parse-integer (cadr +args+))
		  0
		  )
	)
       (step (if (< 2 nargs)
		 (parse-integer (caddr +args+))
		 1
		 )
	 )
      )
  (format t "~&~a~%" (my-utils::range n :start start :step step))

  )

(format t "~&終~%")
