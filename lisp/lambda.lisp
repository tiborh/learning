#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(write ((lambda (a b c d)
	  (+ a b c d))
	1 2 3 4
	)
       )

;; two ways to bind lambda to variables:

;; (defvar foo)
;; (defvar a)
;; (setf a (lambda (&rest  vals)
;; 	  (unless (null vals)
;; 	    (mapcar 'parse-integer vals)
;; 	    )
;; 	  )
;;       )
;; (setf (symbol-function 'foo) (symbol-value 'a))

(setf (symbol-function 'foo) (lambda (&rest  vals)
			       (unless (null vals)
				 (mapcar 'parse-integer vals)
				 )
			       )
      )

(unless (null *args*)
  (format t "~&~s~%" (apply #'foo *args*))
)

(let ((foo (lambda (&rest  vals)
	     (unless (null vals)
	       (mapcar 'parse-integer vals)
	       )
	     )
	)
      )
  (unless (null *args*)
    (format t "~&~s~%" (apply foo *args*))
    )
  )
       
(format t "~&終~%")
