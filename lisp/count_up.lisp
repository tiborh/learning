#!/usr/bin/env clisp

(defun count-up (n &optional (s 0))
  (cond ((<= n s) (list s))
	(t (append (count-up (1- n) s) (list n)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (if (= 2 (length *args*))
      (format t "~&~a~%" (count-up (parse-integer (car *args*)) (parse-integer (cadr *args*))))
      (format t "~&~a~%" (count-up (parse-integer (car *args*))))
      )
  )
