#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun kilo-price (price grams)
  (format t "~&~4,1f (per kilogram)~%" (* 1000 (/ price grams)))
  )

(if (> 2 (length *args*))
    (progn
      (load "mod_usage.lisp")
      (usage '("price" "grams"))
      )
    (kilo-price (parse-integer (car *args*)) (parse-integer (cadr *args*)))
    )

(format t "~&終~%")
