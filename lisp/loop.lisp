#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(when (null *args*)
  (format t "~&No args.~%")
  (EXT:EXIT)
  )

(format t "~&You exit from a loop with 'return'.~%")
(let ((a (parse-integer (first *args*)))
      (d 0)
      )
  (cond ((> a 0) (setq d -1))
	((< a 0) (setq d 1))
	)
  (loop
     (format t "~&a == ~d~%" a)
     (when (= a 0)
       (return a))
     (setq a (+ a d))
     )
  )
