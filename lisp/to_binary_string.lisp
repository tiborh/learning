#!/usr/bin/env clisp

(defun to-bin-str (n s)
  (setq s (concatenate 'string (write-to-string (mod n 2)) s))
  (cond ((< n 2) s)
	(t (to-bin-str (floor n 2) s))
	)
  )

;; (load "dtrace.generic")
;; (dtrace bin)

(if (null *args*)
    (format t "No args.")
    (let ((n (parse-integer (first *args*)))
	  )
      (assert (>= n 0) (n) "does not work with negative numbers: ~d" n)
      (format t "~&~a~%" (to-bin-str n ""))
      )
)
