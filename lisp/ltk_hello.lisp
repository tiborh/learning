#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "ltk/ltk")

(in-package :ltk)

(defun ltk-hello()
  (with-ltk ()
    (let ((b (make-instance 'button 
			    :master nil
			    :text "Press Me"
			    :command (lambda ()
				       (format t "Hello World!~&"))
			    )
	    )
	  )
      (pack b)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(ltk-hello)
