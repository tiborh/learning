#!/usr/bin/env clisp

(load "common_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace get-nums)

(unless (null *args*)
  (let ((nli (get-nums *args*)))
    (format t "~&num list: ~s~%" nli)
    (format t "added: ~a~%" (add-nums nli))
    )
  )
