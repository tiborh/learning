#!/usr/bin/env clisp

(load "my_length2.lisp")

;; (load "dtrace.generic")
;; (dtrace lst-len)

(unless (null *args*)
  (format t "~&~a~%" (my_length *args*))
  )
