#!/usr/bin/env clisp

(defvar *c1* (list))
(defvar *c2* (list))
(defvar *c3* (list))
;; (defparameter *cols* '(*c1* *c2* *c3*))

(defun print-cols ()
  (format t "~&~s ~s ~s~%" *c1* *c2* *c3*)
  )

(defun give-reverse-list (n)
  (cond ((= n 0) nil)
	(t (cons n (give-reverse-list (- n 1))))
	)
  )

(defun give-list (n)
  (reverse (give-reverse-list n))
  )

(defun from-to (&key from-c to-c)
  (set to-c (cons (first (symbol-value from-c)) (symbol-value to-c)))
  (set from-c (rest (symbol-value from-c)))
  (print-cols)
  )

(defun hanoi-alt (&key number-of-c from-c to-c help-c)
  (when (> number-of-c 0)
    (hanoi-alt :number-of-c (- number-of-c 1) :from-c from-c :to-c help-c :help-c to-c)
    (from-to :from-c from-c :to-c to-c)
    (hanoi-alt :number-of-c (- number-of-c 1) :from-c help-c :to-c to-c :help-c from-c)
    )
  )

(defun hanoi (&key number-of-c from-c to-c help-c)
  (cond ((= number-of-c 1) (from-to :from-c from-c :to-c to-c))
	(t (hanoi :number-of-c (- number-of-c 1) :from-c from-c :to-c help-c :help-c to-c)
	   (from-to :from-c from-c :to-c to-c)
	   (hanoi :number-of-c (- number-of-c 1) :from-c help-c :to-c to-c :help-c from-c)
	   )
	)
)

;; (load "dtrace.generic")
;; (dtrace from-to hanoi hanoi-alt)

(when (null *args*)
  (format t "~&No height to first tower.~%")
  (EXT:EXIT)
  )

(let ((n (parse-integer (first *args*))))
  (when (> n 0)
    (setq *c1* (give-list n))
    (print-cols)
    (hanoi-alt :number-of-c n :from-c '*c1* :to-c '*c3* :help-c '*c2*)
    )
  )
