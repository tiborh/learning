#!/usr/bin/env clisp

(defun timestamp ()
  (multiple-value-bind
	(second minute hour date month year day-of-week dst-p tz)
      (get-decoded-time)
    (list year month date hour minute second) ; a sequence must be created
    )
  )

(defun get-timestamp-str ()
  (let ((stamp-list (timestamp)))
    (format nil "~4,'0d~2,'0d~2,'0d_~2,'0d~2,'0d~2,'0d" (first stamp-list) (second stamp-list) (third stamp-list) (fourth stamp-list) (fifth stamp-list) (sixth stamp-list))
    ;; where "~{num},'0d" means {num} width, and if shorter, zero-filled  
  )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(defparameter fn "log/with_open_file.txt")
(with-open-file (a-stream		; the stream to open
		 fn	; with filename
		 :direction :output	; write to file
		 ;;		 :if-exists :supersede	; overwrite if exists
		 :if-exists :append
		 :if-does-not-exist :create
		 )
  (princ (get-timestamp-str) a-stream)
  (princ ": A new log entry." a-stream)
  (fresh-line a-stream)
  (format t "~&Log entry has been added to ~a~%" fn)
  )
