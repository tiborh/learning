#!/usr/bin/env clisp

;; abstract data type
;;   some implementations: lists, strings, vectors
;; generic formula:
;;   (make-sequence sqtype sqsize &key :initial-element)

(load "seq_funs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

;; simple example
(defvar seq-tmpl "(make-sequence sqtype sqsize &key :initial-element)")
(defvar subst-if-tmpl "substitute-if newitem predicate sequence &key from-end start end count key")
(defvar a (make-sequence '(vector float) 12 :initial-element 0))
(defvar b (copy-seq a))
(format t  "~&Count of zeroes: ~a~%" (count 0 a))
(fill-with a 0)
(fill-with b 1 2)

(format t "~&generic formula: ~a~%" seq-tmpl)
(format t "~&A simple sequence is usually a vector:~%")
(format t "~&a: ~a (~a)~%" a (type-of a))
(format t "~&b: ~a~%" b)

(format t "~&Access element with elt: ~a ~a~%" (elt a 0) (elt a (1- (length a))))
(setf (elt a 5) 99)
(format t "~&It can also be used to change a value: ~a~%" a)
(format t "~&length is obvious~%")
(format t "~&For subseq, see strings.~%")
(format t "~&copy-seq: seems obvious. ~%")
(format t "~&fill to change default fill after creation.~%")
(let ((a (make-sequence '(vector integer) 5)))
  (format t "Before fill: ~a~%" a)
  (fill a '(f i l l))
  (format t "~&After fill: ~a~%" a)
  )
(format t "~&Replacing a part of a with b: ~a~%" (replace a b :start1 0 :end1 4 :start2 7))
(write-line "(reverse) has no side effect. (nreverse) reverses in place:")
(nreverse b)
(format t "~&b after nreverse: ~a~%" b)
(format t "~&Concatenate works the same as with strings. (type 'vector)~%")
(format t "~&~a~%" (concatenate 'vector a b))
(format t "~&'position' returns an index where a value is found (99 in a): ~a~%" (position 99 a))
(format t "~&'find' returns the element itself: ~a~%" (find 99 a))
(format t "~&generic formula for substitute-if:~%~a~%" subst-if-tmpl)
(format t "~&substitute odd numbers with .~%~a~%" (substitute-if #\. #'oddp a))
(format t "~&for more info:~% http://www.lispworks.com/documentation/HyperSpec/Body/c_sequen.htm~%")
