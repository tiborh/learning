#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;or:
;;#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun circle-circum(r)
  (format t "~&Circum: ~10f~%" (* PI 2 r))
  )

(defun circle-area(r)
  (format t "~&  Area: ~10f~%" (* PI r r))
  )

(defun circle(r)
  (format t "~&Radius: ~5f~%" r)
  (circle-circum r)
  (circle-area r)
  )

(defun to-number(str)
  (with-input-from-string (in str)
    (read in)
    )
  )

(defun parse-input(v)
  (cond ((null v) nil)
	(t (progn
	     (circle (to-number (car v)))
	     (write-line "")
	     (parse-input (cdr v))
	     )
	   )
	)
  )

(if (null *args*)
    (write-line "Radius is needed as argument.")
    (parse-input *args*)
    )

(princ "終")(terpri)
