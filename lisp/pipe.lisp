#!/usr/bin/env -S sbcl --script

(loop
   (let ((rd (read-char-no-hang)))
     (if (null rd)
	 (return nil)
	 (princ rd)
	 )
     )
)
