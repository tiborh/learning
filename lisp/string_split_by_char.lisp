#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun to-str (lst)
  (concatenate 'string (reverse lst))
  )

(defun add-to-lst (elem lst)
    (unless (= 0 (length elem))
      (push (to-str elem) lst)
      )
  lst
  )

(defun string-split (str chr)
  (let ((str-lst (concatenate 'list str))
	(lst-out nil)
	(tmp-lst nil)
	)
    (dolist (c str-lst)
      (if (char= c chr)
	  (progn
	    (setf lst-out (add-to-lst tmp-lst lst-out))
	    (setf tmp-lst nil)
	    (format t "~&match: ")
	    )
	  (push c tmp-lst)
	  )
      (format t "'~c'~%" c)
      )
    (setf lst-out (add-to-lst tmp-lst lst-out))
    (reverse lst-out)
    )
  )

(unless (> 2 (length *args*))
  (format t "~&~s~%" (string-split (car *args*) (car (concatenate 'list (cadr *args*)))))
  )

(format t "~&終~%")
