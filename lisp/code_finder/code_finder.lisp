#!/usr/bin/sbcl --script
;;#!/usr/local/bin/sbcl --script
;;#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "code_finder_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

;;(format t "~&~a~%" *pieces*)

(defun options-help ()
  (princ "<program> <num>")(terpri)
  (princ "    <num> == 0 for human coder and machine breaker")(terpri)
  (princ "    <num> == 1 for machine coder and human breaker")(terpri)
  (princ "    <num> == 2 for machine coder and machine breaker")(terpri)
  (princ "    <num> == 3 for human coder and human breaker")(terpri)
  )

(if (null (cdr *posix-argv*))
    (options-help)
    (game-selector (parse-integer (cadr *posix-argv*)))
  )

