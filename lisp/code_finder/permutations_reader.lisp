#!/usr/local/bin/sbcl --script
;;#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "code_finder_funcs.lisp")
(load "../file_io_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(defvar permutations (read-list-from-file *permutations-fn*))
;; (princ permutations)(terpri)
(princ "number of permutations: ")(princ (length permutations))(terpri)
(princ "number of elems in a permutation: ")(princ (length (car permutations)))(terpri)
