#!/usr/bin/env clisp

(defun find-last (x)
  (cond ((atom (cdr x)) x)
	(t (find-last (cdr x)))
	)
  )

(defun test-find-last ()
  (assert (not (find-last nil)))
  (assert (equal '(T) (find-last '(T))))
  (assert (equal '((("first"))) (find-last '((("first"))))))
  (assert (equal '(("fifth" "sixth")) (find-last '((("first" "second") ("third" "fourth")) ("fifth" "sixth")))))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(test-find-last)
