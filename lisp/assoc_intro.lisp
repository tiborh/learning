#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; pre-condition for assoc:
;;     each member must be a list
;;     in each member, the car is compared to given search value
;;     the first member is returned where the above comparison is true

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (assoc 'a nil)))
(assert (equal '(a b c d) (assoc 'a '((a b c d)))))
(assert (null (assoc 'b '((a b c d)))))
