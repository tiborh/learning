#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun addup (n)
  "Adds up the first n integers"
  (let ((sum 0))
    (dotimes (i (1+ n))
      (setf sum (+ sum i))
      )
    sum
    )
  )

(defun test-addup ()
  (assert (= 0 (addup 0)))
  (assert (= 1 (addup 1)))
  (assert (= 3 (addup 2)))
  (assert (= 10 (addup 4)))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-addup)
    (time (addup (parse-integer (car *args*))))
    )
