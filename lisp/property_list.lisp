#!/usr/bin/env -S sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(let ((plist (list :a 1 :b 2 :c 3 :d 4 :e 5)))
  (format t "~&The property list or plist: ~a~%" plist)
  (format t "~&getf :a => ~a~%" (getf plist :a))
  (format t "~&getf :e => ~a~%" (getf plist :e))
  )
