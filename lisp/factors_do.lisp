#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "factors_do_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace divider)

(unless (null *args*)
  (format t "~&~a -> ~a~%" (car *args*) (factors (parse-integer (car *args*))))
  )
