#!/usr/bin/env clisp

(load "countdown_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~a~%" (countdown (parse-integer (car *args*))))
  (format t "~&~a~%" (countdown-to-zero (parse-integer (car *args*))))
  )
