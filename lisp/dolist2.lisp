#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun square-list (li)
  (let ((res (list)))
    (dolist (it li (nreverse res))
      (push (* it it) res)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((li '(1 2 3 4 5)))
  (format t "~&~a~%" li)
  (format t "~&~a~%" (square-list li))
  )
