#!/usr/bin/env clisp

(defun print-chars (c n)
  (cond ((= n 0) "")
	(t (concatenate 'string c (print-chars c (- n 1))))
	)
  )

(defun test-arg (n)
  (when (stringp n)
    (if (numberp (read-from-string n))
	(let ((maxnum (parse-integer n :junk-allowed t)))
	  (when (>= maxnum 0)
	    (return-from test-arg maxnum)
	    )
	  )
	(format t "~&~s cannot be parsed as a number.~%" n)
	)
    )
  ;;(EXT:EXIT)
  (return-from test-arg 0)
  )

(defun proc-args (char args)
  (cond ((null args) NIL)
	((format t "~&~a~%" (print-chars char (test-arg (first args)))))
	(t (proc-args char (rest args)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace proc-args print-chars)

(cond ((null *args*) (format t "~&No args.~%"))
      (t (proc-args (first *args*) (rest *args*)))
      )
