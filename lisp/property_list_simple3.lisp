#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

;; source: https://www.tutorialspoint.com/lisp/lisp_symbols.htm

(setf (get 'annie 'age) 43)
(setf (get 'annie 'job) 'accountant)
(setf (get 'annie 'sex) 'female)
(setf (get 'annie 'children) 3)

(write-line "symbol-plist:")
(write (symbol-plist 'annie))
(remprop 'annie 'age)
(terpri)
(write-line "Age removed:")
(write (symbol-plist 'annie))

(format t "~&終~%")
