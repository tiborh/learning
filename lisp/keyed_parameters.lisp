#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

(defun keyed-fun (&key a b c d)
  (write (list a b c d))
  )

(keyed-fun :a "零" :b "一" :c "二" :d "三")

(format t "~&終~%")
