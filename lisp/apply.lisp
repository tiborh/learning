#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((a (list 1 2 3 4))
       (b (list 5 6 7 8))
       (c (list a b '(9 10)))
       )
  (format t "~&a:            ~a~%" a)
  (format t "~&add elements: ~a~%" (apply #'+ a))
  (format t "~&b:            ~a~%" b)
  (format t "~&add elements of a and b: ~a~%" (apply #'+ (apply #'+ a) b))
  (format t "~&embedded list: ~a~%" c)
  (format t "~&addition with append and apply: ~a~%" (apply #'+ (apply #'append c)))
  )
