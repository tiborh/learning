#!/usr/bin/env clisp

(load "char_conv_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~s~%" (int-list-to-char-list *args*))
  (format t "~&~a~%" (int-list-to-char-list *args*))
  )
