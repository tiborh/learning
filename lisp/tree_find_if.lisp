#!/usr/bin/env clisp

(load "tree_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(tree-find-if #'oddp '((2 4) (5 6) 7))))
  (format t "~&~a -> ~a~%" cmdline (eval cmdline))
  )
