#!/usr/bin/env clisp

(defun to-int-list (cl)
  (cond ((null cl) nil)
	(t (cons (char-int (car cl)) (to-int-list (cdr cl))))
	)
)
  
;; (load "dtrace.generic")
;; (dtrace func)
(unless (null *args*)
  (let ((cl (coerce (car *args*) 'list))
	 )
    (format t "~&~a~%" (to-int-list cl))
    )
  )
