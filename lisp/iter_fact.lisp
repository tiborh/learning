#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun fact (n)
  (let ((f 1))
    (dotimes (i n f)
      (setf f (* f (1+ i)))
      )
    )
  )

(defun iter-fact (n)
  (when (< n 0)
    (error "Only non-negative numbers are accepted.")
    )
  (if (= 0 n)
      1
      (fact n)
      )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(dolist (it *args*)
  (format t "~&~a! == ~a~%" it (iter-fact (parse-integer it)))
  )
