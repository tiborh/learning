(defun draw-line (n &optional (linemark "*"))
  (cond ((= 0 n) (terpri))
	(t (progn (princ linemark)
		  (draw-line (1- n) linemark)
		  )
	   )
	)
  )

(defun draw-lines (li)
  (cond ((null li) nil)
	(t (progn (draw-line (parse-integer (car li)))
		  (draw-lines (cdr li))
		  )
	   )
    )
  )

(defun draw-box (w h)
  (cond ((= 0 h) nil)
	(t (progn (draw-line w)
		  (draw-box w (1- h))
		  )
	   )
	)
)
