#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

;; source:
;; http://www.gigamonkeys.com/book/a-few-format-recipes.html

(let ((a (list "one" "two" "three" "four" "five")))
  (format t "~&normal: ~a~%" a)		; (one two three four five)
  (format t "~&raw: ~s~%" a)		; ("one" "two" "three" "four" "five")
  (format t "~&adding list separator: ~{~a, ~}~%" a) ; one, two, three, four, five,
  (format t "~&no list sep after last: ~{~a~^, ~}~%" a) ; one, two, three, four, five
  (format t "~&multi items as list: ~@{~a~^, ~}~%" 'a 'b 'c 'd 'e 'f) ; A, B, C, D, E, F
  (write-line "to be continued...")
  )
