(make-package :timestamp :use '(:common-lisp :common-lisp-user))
(defun timestamp::timestamp ()
  (multiple-value-bind
	(second minute hour date month year)
      (get-decoded-time)
    (format nil "~d-~2,'0d-~2,'0d_~2,'0d:~2,'0d:~2,'0d"
    	    year
	    month
	    date
	    hour
	    minute
	    second
    )
  )
)

