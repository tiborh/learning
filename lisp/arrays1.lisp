#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

;; (load "dtrace.generic")
;; (dtrace func)

(setq a (make-array 10))
(setf (aref a 9) 9)
(setf (aref a 0) 0)
(pprint a)
(setq b (adjust-array a 5))
(assert (= (length a) 10))		; not adjustable
(assert (= (length b) 5))
(pprint b)
(setq c (make-array 10 :adjustable t))
(setf (aref c 0) 0)
(setf (aref c 9) 9)
(pprint c)
(setq d (adjust-array c 11))
(assert (= (length c) 11))
(assert (= (length d) 11))
(assert (eq c d))
(assert (eql c d))
(assert (equal c d))
(setf (aref c 10) "alpha")
(assert (equal (aref c 10) (aref d 10))) ; they are not independent

(setq e (make-array 5))
(setq f (make-array 5))
(assert (not (eq e f)))
(assert (not (eql e f)))
(assert (not (equal e f)))		; they are independent
(assert (equalp e f))			; they are equal

(setq g (make-array 5 :initial-contents '(0 1 2 3 4)))
(setq h (make-array 5 :displaced-to g))
(assert (not (equal g h)))		; they are independent
(assert (equalp g h))

(setf i "alpha")
(pprint i)
(pprint (reverse i))
(pprint (aref i 0))
;; (setf (aref i 2) #\e) ; *** - Attempt to modify a read-only string: "alpha"
;; (setf (aref i 3) #\p)
;; (setf (aref i 4) #\h)
;; (pprint i)
