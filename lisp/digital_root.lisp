#!/usr/bin/env clisp

(load "digit_funcs.lisp")

(defun digital-root (n)
  (cond ((< n 0) nil)
	((< n 10) n)
	((digital-root (+ (digital-root (frest-digits n)) (last-digit n))))
	)
  )

(defun test-digital-root ()
  (assert (not (digital-root -1)))
  (assert (= (digital-root 0) 0))
  (assert (= (digital-root 5) 5))
  (assert (= (digital-root 16) 7))
  (assert (= (digital-root 76) 4))
  (assert (= (digital-root 942) 6))
  (assert (= (digital-root 132189) 6))
  (assert (= (digital-root 493193) 2))
  (write-line "Tests have passed.")
)

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-digital-root)
    (format t "~&digital root: ~d~%" (digital-root (parse-integer (first *args*))))
    )
