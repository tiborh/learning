#!/usr/bin/env -S sbcl --script

(let ((rd (read-char-no-hang)))
  (format t "~&|~s| (type-of: ~s)~%" rd (type-of rd)) ; accepts only pipe input
)
(format t "~&終~%")
