#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "family_tree_src.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(with-open-file (fh "family_tree.saved"
		    :direction :output)
  (format fh "~s~%" family)
  )
