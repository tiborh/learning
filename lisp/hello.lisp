#!/usr/bin/env -S sbcl --script
;#!/usr/bin/env clisp

; a comment
(write-line "Hello, world!")
(format t "Hello, again!~2&")		; & conditional, % unconditional
