#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun get-value (msg)
  (format t "~&~a " msg)
  (let ((v (read)))
    v
    )
  )

(defun wage-calc ()
  (let ((hourly-wage (get-value "Hourly wage?"))
	(hours-worked (get-value "Hours worked?"))
	)
    (format t "~&Gross pay: ~a~%" (* hourly-wage hours-worked))
  )
)
  
;; (load "dtrace.generic")
;; (dtrace func)

(wage-calc)
