#!/usr/bin/env clisp

(defun random-pairs ()
  (let ((MAXNUM 10))
    (unless (null *args*)
      (setf MAXNUM (parse-integer (first *args*)))
      )
    ;; variable, starting-value, value-update-procedure
    (do ((x 0 (random MAXNUM (make-random-state t)))
	 (y MAXNUM (random MAXNUM (make-random-state t)))
	 )
	
	((= x y)(list x y)) 			; return value
      (format t "~& x = ~2d; y = ~2d~%" x y)	; not reached in last loop
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((li (random-pairs)))
  (format t "~& x = ~2d; y = ~2d~%" (car li) (cadr li))
  )
