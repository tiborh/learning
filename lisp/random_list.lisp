#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;or:
;;#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(defun make-rand-list (li len ceil)
  (dotimes (i len)
	   (push (random ceil (make-random-state t)) li)
	   )
  li
  )

(let ((li-len 10)
      (ceil 100)
      (li nil)
      )
  (if (> (length *args*) 0)
      (setf li-len (parse-integer (car *args*)))
      )
  (if (> (length *args*) 1)
      (setf ceil (parse-integer (cadr *args*)))
      )
  (format t "~&list length: ~a~%" li-len)
  (format t "~&ceiling: ~a~%" ceil)
  (setf li (make-rand-list li li-len ceil))
  (format t "~&list: ~a~%" li)
)

(princ "終")(terpri)
