#!/usr/bin/env clisp

(defvar loopstart 0)

(defun fib (n)
  (cond ((= n 0) 1)
	((= n 1) 1)
	(t (+ (fib (- n 2)) (fib (- n 1))))
	)
  )

(defun fib-print (n)
  (let ((f (fib n)))
    (format t "~&fib(~a): ~10t~7d~%" n f)
    )
  )

(defun test-arg (n)
  (when (stringp n)
    (if (numberp (read-from-string n))
	(let ((maxnum (parse-integer n :junk-allowed t)))
	  (when (>= maxnum 0)
	    (return-from test-arg maxnum)
	    )
	  )
	(format t "~&~s cannot be parsed as a number.~%" n)
	)
    )
  (EXT:EXIT)
  )

(defun proc-arg (arg)
  (let ((maxnum (test-arg arg)))
    (loop for i from loopstart to maxnum
       do (fib-print i)  
	 )
    )
  )

;;(load "dtrace.generic")
;;(dtrace proc-args)

(if (null *args*)
    (format t "~&No args.~%")
    (proc-arg (first *args*))
    )
