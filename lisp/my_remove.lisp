#!/usr/bin/env clisp

(defun my-remove (x li)
  (cond ((null li) nil)
	((not (equal x (car li)))
	 (cons (car li) (my-remove x (cdr li)))
	 )
	(t (my-remove x (cdr li)))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let ((cmdline '(my-remove 4 '(1 2 3 4 5 6 7)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
