#!/usr/bin/env clisp

(defun revert-lines (fh)
  (let ((ln (read-line fh nil)))
    (when ln
      (revert-lines fh)
      (format t "~&~a~%" ln)
      )
    )
  )

(defun file-read-wrapper (fn)
  (let ((fh (open fn :if-does-not-exist nil)))
    (when fh
      (revert-lines fh)
      (close fh)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace revert-lines)

(file-read-wrapper "poem.txt")
