#!/usr/bin/env clisp

(defun portmanteau (x)
  (labels ((intfunc1 (y)
	     (* x (+ y 1))
	     )
	   (intfunc2 (y)
	     (* x (intfunc1 y))
	     )
	   )
    (- (intfunc2 x) (intfunc1 x))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(loop for i from 1 to 10
   do (format t "~&~3d -> ~3d~%" i (portmanteau i))
     )
