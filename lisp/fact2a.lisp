#!/usr/bin/env clisp

(defun fact (n)
  (cond ((= n 0) 1)
	(t (* n (fact (- n 1))))
	)
  )

(defun factorial (n)
  (if (< n 0)
      (format t "~&Only for numbers greater than or equal to zero.~%")
      (fact n)
      )
  )

(defun get-arg (n)
  (when (stringp n)
    (when (numberp (read-from-string n))
      (factorial (parse-integer n :junk-allowed t))
      )
    )
  )

(defun proc-args (args)
  (cond ((null args) nil)
	((let* (
		(n (first args))
		(f (get-arg n))
		)
	   (if f
	       (format t "~&~a! ~a~%" (parse-integer n :junk-allowed t) f)
	       (format t "~&~s cannot be parsed as a number.~%" n)
	       )
	   ))
	(t (proc-args (rest args)))
	)
  )

;;(load "dtrace.generic")
;;(dtrace proc-args)

(if (null *args*)
    (format t "~&No args.~%")
    (proc-args *args*)
    )
