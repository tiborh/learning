#!/usr/bin/env clisp

(load "string_funcs.lisp")

(defvar stopval 0)

(defun laugh (n)
  (cond ((zerop n) ())
	(t (cons 'ha (laugh (1- n))))
	)
  )

(defun test-arg (n)
  (when (stringp n)
    (if (numberp (read-from-string n))
	(let ((laughnum (parse-integer n :junk-allowed t)))
	  (when (>= laughnum 0)
	    (return-from test-arg laughnum)
	    )
	  )
	(format t "~&~s cannot be parsed as a number.~%" n)
	)
    )
  (EXT:EXIT)
  )

(defun laugh-str (numlaughs)
  (cond ((= 0 numlaughs) '(#\Backspace #\!))
	(t (append '(#\h #\a #\-) (laugh-str (1- numlaughs))))
	)
  )

(defun proc-arg (arg)
  (let ((numlaugh (test-arg arg)))
    (if (= 0 numlaugh)
	(format t "~&No laughing matter.~%")
	(progn
	  (format t "~&~a~%" (laugh numlaugh))
	  (format t "~&~a~%" (first-cap (coerce (laugh-str numlaugh) 'string)))
	  )
	)
    )
  )

;;(load "dtrace.generic")
;;(dtrace proc-args)

(if (null *args*)
    (format t "~&How many times?~%")
    (proc-arg (first *args*))
    )
