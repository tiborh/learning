#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))
(load "char_conv_funcs.lisp")

(defun how-to-use ()
  (princ "<script name> <from> <to>")
  (terpri)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (= 2 (length *args*))
    (format t "~&~a~%" (from-to-int-to-char-list (parse-integer (car *args*)) (parse-integer (cadr *args*))))
    (how-to-use)
    )
