#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defconstant *number-to-select* 4)
(defconstant *colours* '(black white red green blue yellow))

(defun find-differences (2b-guessed a-guess)
  (let ((score-collector nil))
    (loop for i from 0 to (1- *number-to-select*)
       do (cond ((eq (nth i a-guess) (nth i 2b-guessed)) (push (car *colours*) score-collector))
		((member (nth i a-guess) 2b-guessed) (push (cadr *colours*) score-collector))
		)
	 )
    (sort score-collector #'(lambda (x y) (string-lessp (symbol-name x) (symbol-name y))))
    )
  )

(defun compare-colour-lists (to-be-guessed a-guess)
  (cond ((equal to-be-guessed a-guess) 'match)
	(t (find-differences to-be-guessed a-guess))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (eq 'match (compare-colour-lists '(red green blue yellow) '(red green blue yellow))))
(assert (equal '(black) (compare-colour-lists '(red black white green) '(red blue yellow purple))))
(assert (equal '(black) (compare-colour-lists '(black red white green) '(blue red yellow purple))))
(assert (equal '(black) (compare-colour-lists '(black white red green) '(blue yellow red purple))))
(assert (equal '(black) (compare-colour-lists '(black white green red) '(blue yellow purple red))))
(assert (equal '(white white white white) (compare-colour-lists '(black white green red) (reverse '(black white green red)))))
(assert (equal '(black black white white) (compare-colour-lists '(red green blue yellow) '(red yellow blue green))))
(assert (equal '(black white) (compare-colour-lists '(green blue yellow red) '(red blue black white))))


