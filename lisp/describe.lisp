#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(if (null *args*)
    (write-line "Describes a common lisp command.")
    (describe (read-from-string (car *args*)))
)
(format t "~&終~%")
