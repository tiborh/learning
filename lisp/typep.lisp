#!/usr/bin/env clisp

;; special dynamic global variable, best put between asterisks
(defvar *x* 1)
(write "0 and 1 can be a bit or integer, but not character:")
(print (typep *x* 'integer))		; T
;; (write-line " ")
(print (typep *x* 'bit))		; T
;; (write-line " ")
(print (typep *x* 'character))		; NIL

(print "NIL means False (or F)")
