#!/usr/bin/env clisp

(defun find-first-odd (li)
  (cond ((null li) nil)
	((oddp (parse-integer (car li))) (parse-integer (car li)))
	(t (find-first-odd (cdr li)))
	)
  )

(defun test-find-first-odd ()
  (assert (null (find-first-odd '("2"))))
  (assert (= 1 (find-first-odd '("1"))))
  (assert (= 5 (find-first-odd '("2" "4" "5" "6" "7"))))
  (assert (= 1 (find-first-odd '("1" "two" "three"))))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace find-first-odd)

(if (null *args*)
    (test-find-first-odd)
    (format t "~&~a~%" (find-first-odd *args*))
  )
