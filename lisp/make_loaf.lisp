#!/usr/bin/env clisp

(defun make-loaf (n &optional (s 'X))
  (when (> n 0)
    (cons s (make-loaf (1- n) s))
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&~a~%" (make-loaf (parse-integer (car *args*))))
  )
