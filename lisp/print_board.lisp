#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "draw_tools.lisp")

(defun print-cell (ch)
  (princ " ")
  (if (not ch)
      (princ " ")
      (princ ch)
      )
  (princ " ")
)
  
(defun print-row (li)
  (print-cell (car li))
  (princ "|")
  (print-cell (cadr li))
  (princ "|")
  (print-cell (caddr li))
  )

(defun print-board (li)
  (print-row (car li))
  (terpri)
  (draw-line 11 "-")
  (print-row (cadr li))
  (terpri)
  (draw-line 11 "-")
  (print-row (caddr li))
  (terpri)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(print-board '((X O O)(nil X nil)(O nil X)))
