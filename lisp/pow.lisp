#!/usr/bin/env clisp

(defun pow (n p)
  (cond ((= p 0) 1)
	(t (* n (pow n (- p 1))))
	)
  )

(let ((n (parse-integer (first *args*))) ;base
      (p (parse-integer (second *args*))) ;exponent
      )
  (assert (>= p 0) (p) "does not work with negative powers: ~d" p)
  (format t "~&~a ^ ~a == ~a~%" n p (pow n p))
  )
