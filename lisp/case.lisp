#!/usr/bin/env clisp

(defconstant NDAYS 7)

;; (load "dtrace.generic")
;; (dtrace func)

(let ((d (random NDAYS (make-random-state t)))
      (txt-end " is my favourite day."))
  (case d
    (1 (format t "~& Monday~a~&" txt-end))
    (2 (format t "~& Tuesday~a~&" txt-end))
    (3 (format t "~& Wednesday~a~&" txt-end))
    (4 (format t "~& Thursday~a~&" txt-end))
    (5 (format t "~& Friday~a~&" txt-end))
    (6 (format t "~& Saturday~a~&" txt-end))
    (0 (format t "~& Sunday~a~&" txt-end))
    )
  )

