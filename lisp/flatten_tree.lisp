#!/usr/bin/env clisp

(load "tree_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace flatten)

(let* ((a '('((a b (r)) a c (a d ((a (b) r) a)))))
       (b (cons 'flatten a))
       (c '(flatten '(a . b)))
       (d '(flatten '((a b c d))))
       (d0 '(flatten '(a b c d)))
       (d1 '(flatten '(((a)))))
       (e '(flatten '((a . b) . (c . d))))
       (f '(flatten '(a b c (d e))))
       (g '(flatten '((a) b c (d e))))
      )
  (format t "~&~a: ~a~%" b (eval b))
  (format t "~&~a: ~a~%" c (eval c))
  (format t "~&~a: ~a~%" d0 (eval d0))
  (format t "~&~a: ~a~%" d (eval d))
  (format t "~&~a: ~a~%" d1 (eval d1))
  (format t "~&~a: ~a~%" e (eval e))
  (format t "~&~a: ~a~%" f (eval f))
  (format t "~&~a: ~a~%" g (eval g))
  )

