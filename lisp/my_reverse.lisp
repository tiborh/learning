#!/usr/bin/env clisp

(load "my_reverse_lib.lisp")

(load "dtrace.generic")
(dtrace my-rev)

(unless (null *args*)
  (format t "~&~a~%" (my-reverse0 *args*))
  )
