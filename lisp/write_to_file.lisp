#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defconstant *day-names*
    '("Monday" "Tuesday" "Wednesday"
      "Thursday" "Friday" "Saturday"
      "Sunday"))

(defun timestamp ()
  (multiple-value-bind
	(second minute hour date month year day-of-week dst-p tz)
      (get-decoded-time)
    (format nil "The time is ~2,'0d:~2,'0d:~2,'0d, on ~a, ~d-~2,'0d-~2,'0d (GMT~@d)~a"
	    hour
	    minute
	    second
	    (nth day-of-week *day-names*)
	    year
	    month
	    date
	    (- tz)
	    (if dst-p
		" (dst)"
		""
		)
	    )
    )
  )

(defun file-writer (fh)
  (write-line (timestamp) fh)
  )

(let* ((fn "write_to_file.txt")
       (fh (open fn :direction :output :if-exists :append :if-does-not-exist :create))
       )
  (file-writer fh)
  (close fh)
  (format t "~&date/time string has been written to: ~s~%" fn)
  )


(format t "~&終~%")
