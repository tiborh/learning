#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun iter-fact (n)
  (when (< n 0)
    (return-from iter-fact nil)
    )
  (when (= n 0)
    (return-from iter-fact 1)
    )
  (do* ((i 1 (1+ i))
       (fact 1 (* fact i))
       )
      ((= i n) fact
       )
    )
  )

(defun test-iter-fact ()
  (assert (= (iter-fact 0) 1))
  (assert (= (iter-fact 1) 1))
  (assert (= (iter-fact 2) 2))
  (assert (= (iter-fact 3) 6))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)


(if (null *args*)
    (test-iter-fact)
    (dolist (it *args*)
      (format t "~&~a! ~a~%" it (iter-fact (parse-integer it)))
      )
    )
