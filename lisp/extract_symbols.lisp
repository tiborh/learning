#!/usr/bin/env clisp

(defun extract-symbols (li)
  (cond ((null li) nil)
	((symbolp (car li))
	 (cons (car li)
	       (extract-symbols (cdr li))
	       )
	 )
	(t (extract-symbols (cdr li)))
	)
  )

(defun test-extract-symbols ()
  (assert (not (extract-symbols nil)))
  (assert (not (extract-symbols '(1 2 3.5))))
  (assert (equal (extract-symbols '(1 2 3.5 nil 5)) '(nil)))
  (assert (equal (extract-symbols '(nil)) '(nil)))
  (assert (equal (extract-symbols '(3 bears and 1 girl)) '(bears and girl)))
  (write-line "tests have passed")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(test-extract-symbols)
