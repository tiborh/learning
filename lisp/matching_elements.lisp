#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun find-matching-elements (l0 l1)
  (do* ((li0 l0 (cdr li0))
	(li1 l1 (cdr li1))
	(it0 (car l0) (car li0))
	(it1 (car l1) (car li1))
	(colli nil)
       )
      ((or (null li0) (null li1))(reverse colli))
    (when (equal it0 it1)
      (push it0 colli)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(assert (null (find-matching-elements nil nil)))
(assert (null (find-matching-elements '(1) nil)))
(assert (null (find-matching-elements nil '(1))))
(assert (null (find-matching-elements '(#\a #\p #\p #\l #\e) '(#\p #\e #\a #\r))))
(assert (equal '(#\r) (find-matching-elements '(#\c #\a #\r #\p #\e #\t) '(#\b #\i #\r #\d))))
(assert (equal '(#\e #\s #\s #\e) (find-matching-elements '(#\e #\s #\s #\e #\n #\c #\e) '(#\e #\s #\s #\e #\d))))
(assert (equal '(#\e #\n #\c #\e) (find-matching-elements '(#\e #\s #\s #\e #\n #\c #\e)
							  '(#\v #\a #\l #\e #\n #\c #\e))))
