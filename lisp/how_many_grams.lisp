#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(defun how-many-grams (price kilo-price)
  (format t "~&~4,1f grams~%" (/ 1000 (/ kilo-price price)))
  )

(if (> 2 (length *args*))
    (progn
      (load "mod_usage.lisp")
      (usage '("price" "kilo-price"))
      )
    (how-many-grams (parse-integer (car *args*)) (parse-integer (cadr *args*)))
    )

(format t "~&終~%")
