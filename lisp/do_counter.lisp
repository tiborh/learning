#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun dotimer (num)
  (do ((i 0 (1+ i)))
      ((= i num) i)
    (format t "~&i == ~a~%" i)
    )
  )


;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (write-line "calling dotimer")
  (format t "~&~a~%" (dotimer (parse-integer (car *args*))))
  )
