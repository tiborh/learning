#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun make-space (n)
  (cond ((= 0 n) nil)
	(t (progn (princ " ")
		  (make-space (1- n))
		  )
	   )
	)
  )

(defun make-dot (n &optional (dot "*"))
  (make-space n)
  (princ dot)
  )

(defun make-dots (li &optional (dot "X"))
  (cond ((null li) nil)
	(t (progn (make-dot (car li) dot)
		  (terpri)
		  (make-dots (cdr li) dot)
		  )
	   )
	)
  )

;; (defun make-dots (li &optional (dot "X"))
;;   (cond ((null li) nil)
;; 	(t (progn (make-dot (parse-integer (car li)) dot)
;; 		  (terpri)
;; 		  (make-dots (cdr li) dot)
;; 		  )
;; 	   )
;; 	)
;;   )

(defun make-range (minx maxx)
  (cond ((> minx maxx) nil)
	(t (cons minx (make-range (1+ minx) maxx)))
	)
  )

(defun get-value (msg)
  (format t "~&~a " msg)
  (let ((v (read)))
    v
    )
  )

(defun square (n)
  (* n n)
  )

(defun make-graph ()
  (let ((fun (get-value "Function to graph?"))
	(startx (get-value "Starting x value?"))
	(endx (get-value "Ending x value?"))
	(plotstr (get-value "Plotting string?"))
	)
    (format t "~&range:~a~%" (make-range startx endx))
    (make-dots (mapcar fun (make-range startx endx)) plotstr)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

;;(unless (null *args*)
  ;;(format t "~&~a~%" (make-range (parse-integer (car *args*)) (parse-integer (cadr *args*))))
  ;;(make-dots *args* "O")
  ;;)

(make-graph)
