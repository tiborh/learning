#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defconstant FN "list_growing1.txt")
(load "file_io_funcs.lisp")

(defstruct pair
  (key nil)
  (value nil)
  )

(defun add-user-input-to-list (a-list)
  (let ((a-pair (make-pair)))
    (princ "key: ")
    (setf (pair-key a-pair) (read))
    (princ "value: ")
    (setf (pair-value a-pair) (read))
    (push a-pair a-list)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(let* ((filename FN)
       (my-list (or (read-list-from-file filename)  nil)))
  (format t "~&The current list: ~a~%" my-list)
  (if (y-or-n-p "Would you like to add anything to the list?")
      (progn 
	(setf my-list (add-user-input-to-list my-list))
	(unless (null my-list)
	  (write-list-to-file my-list filename)
	  )
	)
      (write-line "Nothing to do this time.")
      )
  (write-line "Bye!")
  )
