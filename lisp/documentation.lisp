#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

(if (null *args*)
    (write-line "Gives the documentation string of a common lisp command.")
    (let ((doc-type 'function)
	  (doc-str nil)
	  )
      (if (< 1 (length *args*))
	  (setf doc-type (read-from-string (cadr *args*)))
	  )
      (setf doc-str (documentation (read-from-string (car *args*)) doc-type))
      (if (null doc-str)
	  (write-line "no doc string, you can also try 'describe.lisp'")
	  (format t "~&~s~%" doc-str)
	  )
      )
)
(format t "~&for reference, see http://www.lispworks.com/documentation/HyperSpec/Body/f_docume.htm~%")
