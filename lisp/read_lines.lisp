#!/usr/bin/env clisp

(defun line-reader (fh)
  (let ((ln (read-line fh nil)))
    (when ln
      (format t "~&~a~%" ln)
      (line-reader fh)
      )
    )
  )

(defun file-read-wrapper (fn)
  (let ((fh (open fn :if-does-not-exist nil)))
    (if fh
	(progn
	  (line-reader fh)
	  (close fh)
	  )
	(format t "~&Filename not found: ~s~%" fn)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace line-reader)

(unless (null *args*)
  (file-read-wrapper (first *args*))
  )
