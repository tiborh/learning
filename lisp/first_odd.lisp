#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun first-odd (inli)
  (do ((li (cdr inli) (cdr li))
       (it (car inli) (car li))
       )
      ((oddp it) it)
    (when (null li)
      (return nil)
      )
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (format t "~&first odd number: ~a~%" (first-odd (mapcar #'parse-integer *args*)))
  )
