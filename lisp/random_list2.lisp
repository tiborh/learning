#!/usr/bin/env -S sbcl --script
(defconstant *args* (cdr *posix-argv*))

;;or:
;;#!/usr/bin/env clisp

;; (load "dtrace.generic")
;; (dtrace func)

(load "random_list_fun.lisp")

(let ((len-ceil 10)
      (num-ceil 100)
      (rand-len nil)
      (sorted nil)
      (li nil)
      )
  (if (> (length *args*) 0)
      (setf len-ceil (parse-integer (car *args*)))
      )
  (if (> (length *args*) 1)
      (setf num-ceil (parse-integer (cadr *args*)))
      )
  (if (> (length *args*) 2)
      (setf rand-len t)
      )
  (if (> (length *args*) 3)
      (setf sorted t)
      )
  (format t "~&list length ceiling: ~a~%" len-ceil)
  (format t "~&number ceiling: ~a~%" num-ceil)
  (setf li (make-rand-list li len-ceil num-ceil rand-len sorted))
  (format t "~&list: ~a~%" li)
)

(princ "終")(terpri)
