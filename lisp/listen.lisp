#!/usr/bin/env -S sbcl --script

(loop
   (peek-char)
   (if (listen)
       (format t "~&~s~%" (read-char *query-io* nil nil))
       (return nil)
       )
   )
