#!/usr/bin/env clisp

(load "my_funcs.lisp")

(defun my-intersect (l1 l2 liu)
  (cond ((null l1) liu)
	((my-member (car l1) l2)
	 (my-intersect (cdr l1) l2 (append liu (list (car l1))))
	 )
	(t (my-intersect (cdr l1) l2 liu))
	)
  )

(defun my-intersection (l1 l2)
  (my-intersect l1 l2 nil)
  )
	      
(load "dtrace.generic")
(dtrace my-intersect)

(let ((cmdline '(my-intersection '(1 2 3 4 5) '(4 5 6 7 8)))
      )
  (format t "~&~a: ~a~%" cmdline (eval cmdline))
  )
