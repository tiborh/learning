#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

;; also good for pipe input and output
;; Ctrl-D to end gracefully

(loop
   (write (read-char))
   (write-line "")
   )
