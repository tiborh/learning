#!/usr/bin/env clisp

(defun constrain (x min max)
  (cond ((< x min) min)
	((> x max) max)
	(t x)
	)
  )

(let ((x 3))
  (assert (= x (constrain x -50 50)) (x) ("~a should have been produced" x))
  )
(let ((x 50))
  (assert (= x (constrain 92 -50 x)) (x) ("~a should have been produced" x))
  )
(let ((x -50))
  (assert (= x (constrain -51 x 50)) (x) ("~a should have been produced" x))
  )

