;; source:
;; https://www.tutorialspoint.com/lisp/lisp_tree.htm

(defun make-tree (item)
  "it creates a new node with item."
  (cons
   (if (atom item)
       (cons item nil)
       item
       )
   nil)
  )

(defun add-child (parent-tree child-tree)
  "adds a child tree"
  (setf (car parent-tree) (append (car parent-tree) child-tree))
  (return-from add-child parent-tree)
  )

(defun first-child (tree)
  "return the first child of the root node"
  (if (null tree)
      nil
      (cdr (car tree))
      )
  )

(defun next-sibling (tree)
   (cdr tree)
)

(defun data (tree)
   (car (car tree))
)

;; new content from here

(defun draw-end (fchild echars &optional debug)
  (let ((chld (car fchild))
	(trest (cdr fchild))
	)
    (if (consp chld)
	(draw-end chld echars debug)
	(format t " ~a;" chld)
	)
    (if (not (null trest))
	(draw-branch chld trest echars debug)
	)
    )
  )

(defun draw-branch (pnode fchild echars &optional debug)
  (if debug
      (progn
	(format t "~&in draw-branch~%")
	(format t "~&pnode: ~s~%" pnode)
	(format t "~&fchild: ~s~%" fchild)
	)
      )
  (if (consp pnode)
      (draw-branch (car pnode) fchild echars debug)
      (progn
	(format t "~&~c~a" #\Tab pnode)
	(if (not (null fchild))
	    (progn
	      (format t " ~a" echars)
	      (draw-end fchild echars debug)
	      )
	    )
	(format t "~&")
	)
      )
  )

(defun draw-tree (tree echars &optional debug)
  (if debug
      (progn
	(write-line "in draw-tree")
	(format t "~&tree: ~s~%" tree)
	)
      )
  (let ((pnode (car tree))
	(nrest (cdr tree))
	)
    (if (consp pnode)
	(progn
	  (draw-tree pnode echars debug)
	  (draw-branch (car pnode) nrest echars debug)
	  )
	(draw-branch pnode nrest echars debug)
	)
    )
  )

(defun draw-graph (tree &optional debug)
  (if debug
      (progn
	(write-line "in draw-graph")
	(format t "~&tree: ~s~%" tree)
	)
      )
  (write-line "graph {")
  (draw-tree (car tree) "--" debug)
  (write-line "}")
  )

(defun draw-digraph (tree &optional debug)
  (if debug
      (progn
	(write-line "in draw-digraph")
	(format t "~&tree: ~s~%" tree)
	)
      )
  (write-line "digraph {")
  (draw-tree (car tree) "->" debug)
  (write-line "}")
  )
