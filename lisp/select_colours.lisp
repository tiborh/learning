#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(load "code_finder_funcs.lisp")

;; (load "dtrace.generic")
;; (dtrace func)

(when (< 0 (length *args*))
  (let ((number-of-times (parse-integer (car *args*))))
    (princ "without duplicates:")(terpri)
    (dotimes (i number-of-times)
      (princ (select-colours-wrapper nil nil))(terpri)
      )
    (terpri)(princ "with duplicates:")(terpri)
    (dotimes (i number-of-times)
      (princ (select-colours-wrapper t nil))(terpri)
      )
    )
  )
