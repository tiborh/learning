#!/usr/bin/env clisp

(defun lh (li n)
  (cond ((= 0 n) nil)
	(t (cons (car li) (lh (cdr li) (1- n))))
	)
  )

(defun left-half (li)
  (lh li (ceiling (/ (length li) 2)))
  )

(defun test-left-half ()
  (assert (equal (left-half '(A B C D E F G)) '(A B C D)))
  (assert (equal (left-half '(i came i saw i conquered)) '(i came i)))
  (assert (equal (left-half '(1 2 3 4 5 6 7 8)) '(1 2 3 4)))
  (write-line "Tests have passed.")
  )

;; (load "dtrace.generic")
;; (dtrace func)

(if (null *args*)
    (test-left-half)
    (format t "~&~a~%" (left-half *args*))
    )
