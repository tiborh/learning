\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Asking and contributing to cgn}{3}
\contentsline {subsection}{\numberline {1.2}No warranty}{3}
\contentsline {section}{\numberline {2}Plotting functions}{3}
\contentsline {section}{\numberline {3}Plotting scatter graphics}{4}
\contentsline {section}{\numberline {4}Configuring gnuplot}{5}
\contentsline {section}{\numberline {5}Printing}{6}
\contentsline {section}{\numberline {6}Saving/loading}{6}
\contentsline {section}{\numberline {7}Creating animations}{8}
\contentsline {section}{\numberline {8}Debugging cgn}{8}
\contentsline {section}{\numberline {9}Inside Cgn}{8}
\contentsline {subsection}{\numberline {9.1}Extending Cgn}{9}
\contentsline {section}{\numberline {10}Seeing release info}{10}
\contentsline {section}{\numberline {11}Changelog}{11}
\contentsline {section}{\numberline {12}Bibliography}{12}
