#!/usr/bin/env -S sbcl --script
;;(defconstant *args* (cdr *posix-argv*))

(format t "~&raw *load-truename*: ~s~%" *load-truename*)
(format t "~&as native namestring: ~s~%" (native-namestring *load-truename*))
(format t "~&raw *load-pathname*: ~s~%" *load-pathname*)
(format t "~&type-of: ~s~%" (type-of *load-truename*))
(format t "~&directory part: ~s~%" (pathname-directory *load-truename*))
(format t "~&type-of: ~s~%" (type-of (pathname-directory *load-truename*)))
(format t "~&filename part: ~s~%" (pathname-name *load-truename*))

(format t "~&raw *default-pathname-defaults*: ~s~%" *default-pathname-defaults*)

;; (load "~/.sbclrc")
;; (ql:quickload :cl-ppcre :silent t)

;; (let ((file-ext (car (last (cl-ppcre::split "\\." (native-namestring *load-truename*)))))
;;       (file-name (pathname-name *load-truename*))
;;       )
;;   (format t "~&~a.~a~%" file-name file-ext)
;;   )

(format t "~&終~%")
