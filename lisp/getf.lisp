#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun add-pairs (v pairlist)
  (dolist (p pairlist)
    (setf (getf v (car p)) (cadr p))
    )
  v
  )

;; (load "dtrace.generic")
;; (dtrace func)

(setq a '())
(write-line "getf is a funky way to create lists of even number of elements")
(write-line "'a' is an empty list")
(prin1 a)(terpri)
(write-line "(getf a 'prop0) comes back with nil")
(prin1 (getf a 'prop0))(terpri)
(write-line "(getf a 'prop0 7) gives 7 but does not change 'a'")
(prin1 (getf a 'prop0 0))(terpri)
(write-line "(setf (getf a 'prop0) 0) is the way to give it value and save it")
(prin1 (setf (getf a 'prop0) 0))(terpri)
(write-line "now (getf a 'prop0) retrieves the value")
(prin1 (getf a 'prop0))(terpri)
(prin1 a)(terpri)
(write-line "and (remf a 'prop0) can remove a pair")
(prin1 (remf a 'prop0))(terpri)
(write-line "so 'a' is nil again:")
(prin1 a)(terpri)
(write-line "Several value pairs added:")
(setf a (setf a (add-pairs a '((one 1)(two 2)(three 3)))))
(prin1 a)(terpri)
(write-line "its type (to prove it remains a list):")
(prin1 (type-of a))(terpri)
