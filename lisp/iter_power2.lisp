#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun pow (base exp)
  (do ((n exp (1- n))
       (result 1 (* result base))
       )
      ((= 0 n) result)
    )
  )

;; (load "dtrace.generic")
;; (dtrace func)

(unless (null *args*)
  (let ((base (parse-integer (car *args*)))
	(exponent (parse-integer (cadr *args*)))
	)
    (format t "~&~a to the power of ~a is ~a~%" base exponent (pow base exponent))
    )	
  )
