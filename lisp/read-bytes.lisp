#!/usr/bin/env -S sbcl --script

(loop
   (let ((bt (read-byte *standard-input* nil nil)))
     (if (null bt)
	 (return nil)
	 (progn
	   (format t "~2,'0x " bt)
	   (cond ((= 10 bt) (format t "~%"))
		 ((= 32 bt) (format t " "))
		 )
	   )
	 )
     )
   )
;;(write-line "")
