#!/usr/bin/clisp

(defun aritheval (li)
  (cond ((null li) nil)
	((atom li) li)
	(t (eval (list (cadr li) (aritheval (car li)) (aritheval (caddr li)))))
	)
  )

(load "dtrace.generic")
(dtrace aritheval)

(assert (= (aritheval 4) 4))
(assert (= (aritheval '(1 + 1)) 2))
(assert (= (aritheval '(2 + (3 * 4)))  14))
(assert (= (aritheval '((2 * 2) - 3))  1))
(assert (= (aritheval '((1 + 2) + (3 * 4)))  15))
