use std::io;			// for input stream
use std::process;

fn main() {
    let mut input = String::new();
    println!("Give me a natural number: ");
    let res = io::stdin().read_line(&mut input);
    println!("result of read_line: {:?}",res);
    if !res.is_ok() {
	println!("Problem with reading user input: {}.",res.unwrap_err());
	process::exit(1);
    }
    input.pop();
    println!("input: '{}'",input);
    let num = input.parse::<u64>();
   
    if num.is_ok() {
	// understore to ignore variable:
	println!("unwrapped num: {:?}",num);
	println!("the number: {}", num.unwrap());
    } else {
	println!("There's a disturbance in the force: {}.",num.unwrap_err());
    }
}
