fn main() {
    let tup0 = (1234, 1.234);
    println!("tup0: {:?}",tup0);	// `({integer}, {float})` cannot be formatted with the default formatter
    let tup1:(i32, f64, u8, f32) = (500,6.4,1,29.29);
    println!("tup1: {:?}",tup1);
    let (x,y) = tup0;
    println!("elements of tup0 assigned to x & y: {} {}",x,y);
    let x = tup1.3;
    let y = tup1.2;
    println!("or individual assigments from tup1 x & y: {} {}",x,y);
    let tup2 = (("George", "Hacker"),(12, "High", "Street"),12345,("York","Yorkshire","UK"));
    println!("complex tuple: {:?}",tup2);
    println!("getting a data point: {}",(tup2.1).0);
}
