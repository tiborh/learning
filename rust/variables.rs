fn main() {
    let varx = 3;
    let vary = 7;
    let tup = (3,4);
    println!("x == {}, y == {}, x + y == {}",varx,vary,varx+vary);
    // reversal:
    println!("y == {1}, x == {0}",varx,vary);
    // same with variable replacement:
    println!("x == {x}, y == {y}, {x} + {y} == {z}",x=varx,y=vary,z=varx+vary);
    println!("tup == {:?}", tup); // debugging info
    let underscored = 1_123;
    println!("underscore can be use for clarity, but is ignored: {}",underscored);
}
