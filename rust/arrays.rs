fn main() {
    let a = [0,1,2,3,4];
    let b:[u16;5] = [1,2,3,4,5];
    let mut c = [0; 5];
    println!("a: {:?}",a);
    println!("b: {:?}",b);
    println!("c: {:?}",c);
    c[1] = 3;
    println!("c: {:?}",c);
    for n in b.iter() {
	println!("{}",n);
    }
    for i in 0..a.len() {
	println!("{}",a[i]);
    }
}
