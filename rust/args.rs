use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("The command line args: {:?}",args);
    for (nu,arg) in args.iter().enumerate() {
	println!("{}: {}",nu,arg);
    }
}
