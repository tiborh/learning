mod rect;

fn main() {
    
}

fn quattro() -> i8 {
    4
}

#[cfg(test)]			// compile with rustc --test option to execute below

mod some_tests {
    #[test]			// annotation
    fn test_basic() {
	assert!( 1 == 1 );
    }
    #[test]
    fn test_fail() {
	panic!("Something smells in Denmark.");
    }
    #[test]
    #[should_panic]
    fn test_wont_fail() {
	panic!("Something smells in Denmark.");
    }
    #[test]
    fn test_equality() {
	assert_eq!(super::quattro(), 2+2);
    }
    #[test]
    #[ignore]
    fn test_ignored() {
	panic!("Won't fail.");
    }
    #[test]
    #[should_panic]
    fn test_rect_is_not_square() {
	let r = super::rect::Rect { width: 50, height: 25 };
	assert!(r.is_square());
    }
    #[test]
    fn test_rect_is_square() {
	let r = super::rect::Rect { width: 45, height: 45 };
	assert!(r.is_square());
    }
    #[test]
    fn test_rect_describe() {
	let w = 50;
	let h = 25;
	let r = super::rect::Rect { width: w, height: h };
	let sides = r.describe();
	assert_eq!(w,sides.0);
	assert_eq!(h,sides.1);
    }
}
