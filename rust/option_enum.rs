fn char_matcher(samplestr:&String,nu:usize) {
    match samplestr.chars().nth(nu) {
	Some(letter) => println!("The letter found: {}",letter),
	None => println!("None was found.")
    }
}

fn main() {
    let five = String::from("alpha");
    println!("original string: {}",five);
    println!("fifth letter: {:?}",five.chars().nth(4));
    println!("sixth letter: {:?}",five.chars().nth(5));
    println!("This is why match is to be used.");
    char_matcher(&five,4);
    char_matcher(&five,5);
}
