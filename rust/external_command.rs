use std::process::Command;

fn main() {
    let mut cmd = Command::new("bash");
    cmd.arg("simple.sh");

    match cmd.output() {
	Ok(res) => {
	    println!("Raw result: {:?}",res);
	    println!("The output: {:?}",res.stdout);
	    unsafe {
		println!("As string: {}",String::from_utf8_unchecked(res.stdout));
	    }
	},
	Err(e) => println!("Should have had a second look: {:?}",e)
    }
}
