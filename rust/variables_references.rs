fn main() {
    let mut x = 10;
    let y = x;
    let xmr = &mut x;
    let yr = &y;

    println!("xmr == {}",xmr);	// 10
    *xmr = 12;
    println!("xmr == {}",xmr);	// 12
    println!("yr == {}",yr);
    let x = 5;
    println!("xmr == {}",xmr);	// 12 (points at the shadowed)
    let xmr = &x;
    println!("xmr == {}",xmr);	// 5 (points at what shadows)
}
