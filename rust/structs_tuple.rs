struct Colour(u8,u8,u8);

fn main() {
    let bg = Colour(127,127,127);
    println!("r: {}, g: {}, b: {}",bg.0,bg.1,bg.2);
}
