use std::env;

fn main() {
    let args:Vec<String> = env::args().collect();
    let defaultname = "Abel".to_string();
    let namestring = if args.len() > 1 {&args[1]} else {&defaultname};
    println!("Occupation for {}: {}", namestring, occ_matcher(get_occ(&namestring[..])));
}

fn occ_matcher(opt:Option<&str>) -> &str {
    match opt {
	Some(occ) => return occ,
	None => return "NA"
    }
}

fn get_occ(name: &str) -> Option<&str> {
    match name {
	"Abel" => Some("farmer"),
	"Cain" => Some("herdsman"),
	"Deborah" => Some("judge"),
	"David" => Some("king"),
	_ => None
    }
}
