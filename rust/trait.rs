struct Person {
    name: String,
    age: u8
}

// one way
impl Person {			// this is more like adding methods
    fn describe(&self) -> String {
	let mut outstr = String::from(&self.name);
	outstr.push_str(" ");
	outstr.push_str(&self.age.to_string());
	return outstr;
    }
}

// another way
impl ToString for Person {	// this is implementing an interface
    fn to_string(&self) -> String {
	return format!("Name: {}, Age: {}",self.name,self.age);
    }
}

// defining a new trait that can be implemented for Person struct
trait HasVoiceBox {
    fn speak(&self);
    fn can_speak(&self) -> bool;
}

// implementing defined trait
impl HasVoiceBox for Person{
    fn speak(&self) {
	println!("Hello, my name is {}.",self.name);
    }
    fn can_speak(&self) -> bool {
	if self.age > 1 {
	    true
	} else {
	    false
	}
    }
}

fn main() {
    let ad = Person { name: String::from("Adam"), age: 35 };
    println!("describing 'ad': {}",ad.describe());
    println!("to_string: {}",ad.to_string());
    if ad.can_speak() {
	println!("Making him speak:");
	ad.speak();
    }
}
