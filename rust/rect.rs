pub struct Rect {
    pub width:  u32,
    pub height: u32
}

impl Rect {
    pub fn describe(&self) -> (u32,u32) {
	println!("w:{}, h:{}",self.width,self.height);
	(self.width,self.height)
    }
    pub fn is_square(&self) -> bool {
	self.height == self.width
    }
}
