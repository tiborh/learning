use std::io;			// for input stream

fn main() {
    let mut input = String::new();
    println!("Your input: ");

    match io::stdin().read_line(&mut input) {
	// understore to ignore variable:
	Ok(_) => println!("Capitalising input: {}",input.to_uppercase()),
	Err(e) => println!("There's a disturbance in the force: {}",e)
    }
}
