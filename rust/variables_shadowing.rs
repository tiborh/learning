fn main() {
    let x = 5;
    {
	println!("x == {}",x);
	let x = x+1;		// shadowing
	println!("x == {}",x);
    }
    {
	// shadowing can change the type
	let a = "alef";
	println!("a == {}",a);
	let a = a.len();
	println!("a == {}",a);
    }
    println!("x == {}",x);
}
