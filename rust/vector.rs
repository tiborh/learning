fn main() {
    let mut animals = vec!["bear","fox","rabbit","lion","wolf"];
    for i in &animals {          //animals.iter() is the traditional way
        // in animals moves, in &animals borrows
        println!("an animal: {}",i);
    }
    animals[3] = "jackal";
    println!("the animals after change: {:?}",animals);
    println!("with index:");
    for (i,anim) in animals.iter().enumerate() {
        println!("{}: {}",i,anim);
    }
}
