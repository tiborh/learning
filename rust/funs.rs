fn main() {			// a function with no return val
    println!("five: {}",five());
    println!("double five: {}",double(five()));
    println!("is_odd(0): {}",is_odd(0));
    println!("is_odd(1): {}",is_odd(1));
    println!("is_odd(2): {}",is_odd(2));
    let tsample = (3,4,5);
    println!("sample tuple: {:?}",tsample);
    println!("tuple_demo(tsample): {:?}",tuple_demo(tsample));
    println!("factorial(5): {}",fact_recursive(5));
}

fn five() -> i32 {
    5
}

fn double(x: i32) -> i32 {
    2 * x
}

fn is_odd(x:i32) -> bool {
    if(x & 1) == 0 {
	false
    } else {
	true
    }
}

fn tuple_demo(t:(u8,u16,u32)) -> (f32, f64) {
    let x:f32 = t.0 as f32 + t.1 as f32;
    let y:f64 = t.2 as f64;
    (x,y)
}

fn fact_recursive(num: u64) -> u64 {
    match num {
	0 => 1,
	1 => 1,
	_ => fact_recursive(num - 1) * num,
    }
}
