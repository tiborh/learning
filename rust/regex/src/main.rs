extern crate regex;
use regex::Regex;

fn main() {
    let re = Regex::new(r"\w{5}").unwrap();	// raw string: r"..."
    // .unwrap(): no error expected
    let re1 = Regex::new(r"\d{1}").unwrap();
    let txt = "in principio creavit Deus caelum et terram";
    let re2 = Regex::new(r"(\w{5})").unwrap(); // () capture result
    let re3 = Regex::new(r"(\d{1})").unwrap();

    println!("is there a five letter string? {}", re.is_match(txt));
    println!("is there a digit? {}", re1.is_match(txt));

    match re2.captures(txt) {
	Some(captured) => {
	    println!("raw match struct: {:?}",captured);
	    println!("the match: {}",captured.get(0).unwrap().as_str());
	},
	None => println!("No match.")
    }

    match re3.captures(txt) {
	Some(captured) => println!("the match: {}",&captured[0]), // simplified
	None => println!("No match.")
    }
}
