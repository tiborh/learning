mod some_mod {
    fn private_msg() {
	println!("private");
    }
    pub fn print_msg(msg:&String) {
	private_msg();
	println!("msg: {}",msg);
    }
}

use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let defmsg = String::from("a message");
    some_mod::print_msg(if args.len() > 1 {&args[1]} else {&defmsg});
}
