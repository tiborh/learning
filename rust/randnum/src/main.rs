extern crate rand;
use rand::Rng;

fn main() {
    let minnu = 1;
    let upperbound = 7;
    // rand::thread_rng() is a random number generator
    let randnum = rand::thread_rng().gen_range(minnu,upperbound);
    println!("The die is cast: {}",randnum);
    // weighted boolean:
    let randbool = rand::thread_rng().gen_bool(0.5);
    println!("The coin is flipped: {}", if randbool {"head"} else {"tail"});
}
