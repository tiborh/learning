struct Colour {
    red:   u8,
    green: u8,
    blue:  u8
}

fn main() {
    let bg = Colour {red:127,green:127,blue:127};
    print_colour(&bg);		// if not by ref, only once it can be called.
}

fn print_colour(c:&Colour) {
    println!("Colour: r: {}, g: {}, b: {}",c.red,c.green,c.blue);
}
