use std::collections::HashMap;

fn main() {
    let mut marks = HashMap::new();
    marks.insert("Programming",96);
    marks.insert("UX",75);
    marks.insert("Design",80);
    marks.insert("Networks",83);
    println!("marks {:?}",marks);
    println!("length: {}",marks.len());
    let keyword = "Networks";
    println!("Looping through:");
    for (k,v) in &marks {
	println!("key: {}, value: {}",k,v);
    }
    find_subject(keyword,&marks);
    println!("Does it contain keyword {}? {}",keyword,marks.contains_key(keyword));
    marks.remove(keyword);
        println!("Does it contain keyword {}? {}",keyword,marks.contains_key(keyword));

    find_subject(keyword,&marks);
    find_subject("Algebra",&marks);
}

fn find_subject(keyword:&str,marks:&HashMap<&str,u32>) {
    match marks.get(keyword){
	Some(mark) => println!("The mark for {}: {}",keyword,mark),
	None => println!("No mark for {}.",keyword)
    }
}
