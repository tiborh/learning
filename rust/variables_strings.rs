use std::fs::File;
use std::io::prelude::*;

fn main() {
    let genki = "元気ですか？";
    let howdy = "How are you?";
    let emoji = "😀";
    let empty = "";
    println!("string1: '{}'",genki);
    println!("string2: '{}'",howdy);
    println!("string3: '{}'",empty);
    println!("string4: '{}'",emoji);
    
    println!("length1: {}",genki.len());
    println!("length2: {}",howdy.len());
    println!("length3: {}",empty.len());
    println!("length4: {}",emoji.len());
    
    println!("empty1?: {}",genki.is_empty());
    println!("empty2?: {}",howdy.is_empty());
    println!("empty3?: {}",empty.is_empty());
    
    println!("as bytes 1: {:?}",genki.as_bytes());
    println!("as bytes 2: {:?}",howdy.as_bytes());
    println!("as bytes 3: {:?}",empty.as_bytes());
    println!("as bytes 4: {:?}",emoji.as_bytes());
    
    let mut hello = String::from("Hello");
    println!("string:    '{}'",hello);
    hello.push(' ');
    println!(".push:     '{}'",hello);
    hello.push_str("world!");
    println!(".push_str: '{}'",hello);
    hello.insert(5,',');
    println!(".insert:   '{}'",hello);

    println!("\nPart 2:");
    let mut genesis = String::from("In principio creavit Deus caelum et terram.");

    println!("The string:  {}",genesis);
    println!("Its length:  {}",genesis.len());
    println!("Is it empty? {}",genesis.is_empty());
    println!("Does it contain 'in'? {}",genesis.contains("in"));
    println!("Does it contain 'In'? {}",genesis.contains("In"));
    println!("Does it contain 'est'? {}",genesis.contains("est"));
    println!("Tokenising:");
    for (i,token) in genesis.split_whitespace().enumerate() {
	println!("\t{}: {}",i,token);
    }
    let gen_vect: Vec<&str> = genesis.split_whitespace().collect();
    println!("Split and collected:");
    println!("{:?}",gen_vect);
    let reconnected = gen_vect.join("_");
    println!("Reconnected:");
    println!("{}",reconnected);
    genesis = reconnected.split("_").collect::<Vec<&str>>().join(" ");
    println!("Regenerated:");
    println!("{}",genesis);
    println!("joining strings with push:");
    genesis.push_str(" Terra autem erat inanis et vacua et tenebrae super faciem abyssi et spiritus Dei ferebatur super aquas.");
    println!("after push:  {}",genesis);

    println!("\nFive more methods:");
    println!("Replace:");
    let some_string = String::from("Rust is fantastic!");
    println!("original:      {}",some_string);
    println!("after replace: {}",some_string.replace("Rust","Perl"));

    println!("Iterating through lines");
    let filename = "Darkness.txt";
    let mut fh = File::open(filename).expect(format!("Can't open the file: {}",filename).as_ref());
    let mut contents = String::new();
    fh.read_to_string(&mut contents).expect(format!("Can't read file: {}",filename).as_ref());

    //println!("File Content: {}",contents);
    let numwidth = 4;
    for (i,line) in contents.lines().enumerate() {
	println!("{num:>width$}: {txt}",num=i,width=numwidth,txt=line);
    }

    let spaced = String::from("   It needs some trimming.        \n\t");
    println!("|{}|",spaced);
    println!("|{}|",spaced.trim().replace("some","no"));
    println!("nth char in string: {:?}",filename.chars().nth(4));
    println!("nth byte in string: {:?}",filename.bytes().nth(4));
    let suspicion_breeds_devils = "疑心暗鬼";
    println!("{}",suspicion_breeds_devils);
    byte_matcher(suspicion_breeds_devils,4);
    for a_byte in suspicion_breeds_devils.bytes() {
	print!("{:x} ",a_byte);
    }
    println!("");
    let ansi = "ansi";
    println!("{}",ansi);
    byte_matcher(ansi,4);
}

fn byte_matcher(s:&str,nu:usize) {
    match s.bytes().nth(nu) {
	Some(c) => println!("Byte at index {}: {:x}",nu,c),
	None => println!("No byte at index {}.", nu)
    }
}
