fn main() {
    println!("Tried to find an easy way to print the type of a variable.");
    println!("Especially for those cases when 'let' variables do not have a specific type.");
    println!("Have not managed to find a solution to this question yet.");
    println!("(actually, there are lots of crap answers out there but none were elegant or working so far)");
}

// for example:
// fn print_type_of<T>(_: &T) {
//     println!("{}", unsafe { std::intrinsics::type_name::<T>() });
// }
// error[E0658]: use of unstable library feature 'core_intrinsics': intrinsics are unlikely to ever be stabilized, instead they should be used through stabilized interfaces in the rest of the standard library
//  --> variable_types.rs:2:29
//   |
// 2 |     println!("{}", unsafe { std::intrinsics::type_name::<T>() });
//   |                             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// error: aborting due to previous error
//
// For more information about this error, try `rustc --explain E0658`.
