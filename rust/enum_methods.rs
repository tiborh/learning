#![allow(dead_code)]
enum Day {
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat,
    Sun
}

impl Day {
    fn is_weekday(&self) -> bool {
	match self {
	    &Day::Sat | &Day::Sun => return false,
	    _ => return true
	}
    }
}

fn examine_day(s:&str,d:Day) {
    println!("Is {}day weekday? {}",s,d.is_weekday());
}

fn main() {
    let d0:Day = Day::Mon;
    let d5:Day = Day::Fri;
    let d6:Day = Day::Sat;
    let d7:Day = Day::Sun;

    examine_day("Mon",d0);
    examine_day("Fri",d5);
    examine_day("Sat",d6);
    examine_day("Sun",d7);
}
