use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
	println!("First arg should be an 8 bit unsigned integer.");
	process::exit(1);
    }
    let nu = &args[1].parse::<u8>().unwrap();
    // unwrap to remove any error, even if it can
    //   cause panic if input is not found or cannot be correctly parsed
    match nu {
	1 => println!("It's one."),
	2 => println!("They're two."),
	3 | 4 => println!("Three or four."),
	// this is not logical "or" but pattern matching "or"
	5..=10 => println!("In the range of 5 to 10."),
	// ... has been deprecated for inclusive range
	// 11..15 => println!("In the range of 11 to 15, exclusive"),
	// error[E0658]: exclusive range pattern syntax is experimental
	_ => println!("It doesn't match any pattern.")
    }
}
