fn main() {
    let x = {
	let mut x = 4;
	x = x * 2;
	x + 1 // with ; -> unused arithmetic operation that must be used
    };
    println!("The value of x: {:?}",x);
}
