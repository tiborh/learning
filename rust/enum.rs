enum Drctn {
    North,
    South,
    West,
    East
}

fn main() {
    let mut s = String::with_capacity(13);
    s.push_str("Heading ");
    let pdirect:Drctn = Drctn::North;
    match pdirect {
        Drctn::North => s.push_str("North."),
        Drctn::South => s.push_str("South."),
        Drctn::West => s.push_str("West."),
        Drctn::East => s.push_str("East."),
    }
    println!("{}",s);
}
