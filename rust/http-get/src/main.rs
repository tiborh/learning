extern crate reqwest;

fn main() {
    // short version:
    // let response_text = reqwest::get("http://localhost/")
    // 	.expect("Unsuccessful request.")
    // 	.text().expect("Could not read the response text.");

    // println!("Response text: {}", response_text);
    
    // long version:
    // match reqwest::get("http://localhost/") {
    // 	Ok(mut response) => {
    // 	    if response.status() == reqwest::StatusCode::Ok {
    // 		match response.text() {
    // 		    Ok(text) => println!("Response Text: {}", text),
    // 		    Err(_) => println!("Could not read response text")
    // 		}
    // 	    } else {
    // 		println!("Response was not 200 OK.");
    // 	    }
	    
    // 	}
    // 	Err(_) => println!("Unsuccessful request.")
    // }
    
}

// short version:
// error[E0599]: no method named `text` found for opaque type `impl std::future::Future` in the current scope
//  --> src/main.rs:7:3
//   |
// 7 |     .text().expect("Could not read the response text.");
//   |      ^^^^ method not found in `impl std::future::Future`

// error: aborting due to previous error

// For more information about this error, try `rustc --explain E0599`.
// error: could not compile `http-get`.


// long version
// error[E0308]: mismatched types
//    --> src/main.rs:5:2
//     |
// 5   |     Ok(mut response) => {
//     |     ^^^^^^^^^^^^^^^^ expected opaque type, found enum `std::result::Result`
//     | 
//    ::: /home/tibor/.cargo/registry/src/github.com-1ecc6299db9ec823/reqwest-0.10.4/src/lib.rs:248:41
//     |
// 248 | pub async fn get<T: IntoUrl>(url: T) -> crate::Result<Response> {
//     |                                         ----------------------- the expected opaque type
//     |
//     = note: expected opaque type `impl std::future::Future`
//                       found enum `std::result::Result<_, _>`

// error[E0308]: mismatched types
//    --> src/main.rs:16:2
//     |
// 16  |     Err(_) => println!("Unsuccessful request.")
//     |     ^^^^^^ expected opaque type, found enum `std::result::Result`
//     | 
//    ::: /home/tibor/.cargo/registry/src/github.com-1ecc6299db9ec823/reqwest-0.10.4/src/lib.rs:248:41
//     |
// 248 | pub async fn get<T: IntoUrl>(url: T) -> crate::Result<Response> {
//     |                                         ----------------------- the expected opaque type
//     |
//     = note: expected opaque type `impl std::future::Future`
//                       found enum `std::result::Result<_, _>`

// error[E0599]: no associated item named `Ok` found for struct `reqwest::StatusCode` in the current scope
//  --> src/main.rs:6:51
//   |
// 6 |         if response.status() == reqwest::StatusCode::Ok {
//   |                                                      ^^
//   |                                                      |
//   |                                                      associated item not found in `reqwest::StatusCode`
//   |                                                      help: there is an associated constant with a similar name (notice the capitalization): `OK`

// error: aborting due to 3 previous errors

// Some errors have detailed explanations: E0308, E0599.
// For more information about an error, try `rustc --explain E0308`.
// error: could not compile `http-get`.
