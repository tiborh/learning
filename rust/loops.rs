fn main() {
    let mut count = 0;
    let res = loop {
	count += 1;
	if count == 10 {
	    break count * 2
	}
    };
    println!("The result is {}", res);

    count = 3;
    while count != 0 {
	println!("{}",count);
	count -= 1;
    }

    let a = ['零','一','二','三','四','五','六','七','八','九'];
    for elem in a.iter() {
	println!("the value is: {}",elem);
    }

    for num in (1..4).rev() {
	println!("{}",num);
    }

    let mut i = 0;
    loop {
	i += 1;
	if is_odd(i) {
	    continue;
	}
	println!("i == {}",i);
	if i == 10 {
	    break;
	}
    }
}

fn is_odd(x:i32) -> bool {
    if(x & 1) == 0 {
	false
    } else {
	true
    }
}
