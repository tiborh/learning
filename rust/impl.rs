mod rect;

fn main() {
    let rect1 = rect::Rect { width: 640, height: 480 };
    rect1.describe();
    println!("Is it a square? {}",rect1.is_square());
    let rect2 = rect::Rect { width: 720, height: 720 };
    rect2.describe();
    println!("Is it a square? {}",rect2.is_square());
}
