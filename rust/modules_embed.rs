mod some_mod {
    pub fn print_msg(msg:&String) {
	println!("msg: {}",msg);
    }

    pub mod emb_mod {
	pub fn print_msg() {	// even if the module is public, its function can be private
	    println!("embedded module");
	}
    }
}

use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let defmsg = String::from("a message");
    some_mod::print_msg(if args.len() > 1 {&args[1]} else {&defmsg});
    some_mod::emb_mod::print_msg();
}
