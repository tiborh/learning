1:1
In the beginning God created heaven, and earth.
in principio creavit Deus caelum et terram
1:2
And the earth was void and empty, and darkness was upon the face of the deep; and the spirit of God moved over the waters.
terra autem erat inanis et vacua et tenebrae super faciem abyssi et spiritus Dei ferebatur super aquas
1:3
And God said: Be light made. And light was made.
dixitque Deus fiat lux et facta est lux
1:4
And God saw the light that it was good; and he divided the light from the darkness.
et vidit Deus lucem quod esset bona et divisit lucem ac tenebras
1:5
And he called the light Day, and the darkness Night; and there was evening and morning one day.
appellavitque lucem diem et tenebras noctem factumque est vespere et mane dies unus
1:6
And God said: Let there be a firmament made amidst the waters: and let it divide the waters from the waters.
dixit quoque Deus fiat firmamentum in medio aquarum et dividat aquas ab aquis
1:7
And God made a firmament, and divided the waters that were under the firmament, from those that were above the firmament, and it was so.
et fecit Deus firmamentum divisitque aquas quae erant sub firmamento ab his quae erant super firmamentum et factum est ita
1:8
And God called the firmament, Heaven; and the evening and morning were the second day.
vocavitque Deus firmamentum caelum et factum est vespere et mane dies secundus
1:9
God also said; Let the waters that are under the heaven, be gathered together into one place: and let the dry land appear. And it was so done.
dixit vero Deus congregentur aquae quae sub caelo sunt in locum unum et appareat arida factumque est ita
1:10
And God called the dry land, Earth; and the gathering together of the waters, he called Seas. And God saw that it was good.
et vocavit Deus aridam terram congregationesque aquarum appellavit maria et vidit Deus quod esset bonum
1:11
And he said: let the earth bring forth green herb, and such as may seed, and the fruit tree yielding fruit after its kind, which may have seed in itself upon the earth. And it was so done.
et ait germinet terra herbam virentem et facientem semen et lignum pomiferum faciens fructum iuxta genus suum cuius semen in semet ipso sit super terram et factum est ita
1:12
And the earth brought forth the green herb, and such as yieldeth seed according to its kind, and the tree that beareth fruit, having seed each one according to its kind. And God saw that it was good.
et protulit terra herbam virentem et adferentem semen iuxta genus suum lignumque faciens fructum et habens unumquodque sementem secundum speciem suam et vidit Deus quod esset bonum
1:13
And the evening and the morning were the third day.
factumque est vespere et mane dies tertius
1:14
And God said: Let there be lights made in the firmament of heaven, to divide the day and the night, and let them be for signs, and for seasons, and for days and years:
dixit autem Deus fiant luminaria in firmamento caeli ut dividant diem ac noctem et sint in signa et tempora et dies et annos
1:15
To shine in the firmament of heaven, and to give light upon the earth, and it was so done.
ut luceant in firmamento caeli et inluminent terram et factum est ita
1:16
And God made two great lights: a greater light to rule the day; and a lesser light to rule the night: and the stars.
fecitque Deus duo magna luminaria luminare maius ut praeesset diei et luminare minus ut praeesset nocti et stellas
1:17
And he set them in the firmament of heaven to shine upon the earth.
et posuit eas in firmamento caeli ut lucerent super terram
1:18
And to rule the day and the night, and to divide the light and the darkness. And God saw that it was good.
et praeessent diei ac nocti et dividerent lucem ac tenebras et vidit Deus quod esset bonum
1:19
And the evening and morning were the fourth day.
et factum est vespere et mane dies quartus
1:20
God also said: let the waters bring forth the creeping creature having life, and the fowl that may fly over the earth under the firmament of heaven.
dixit etiam Deus producant aquae reptile animae viventis et volatile super terram sub firmamento caeli
1:21
And God created the great whales, and every living and moving creature, which the waters brought forth, according to their kinds, and every winged fowl according to its kind. And God saw that it was good.
creavitque Deus cete grandia et omnem animam viventem atque motabilem quam produxerant aquae in species suas et omne volatile secundum genus suum et vidit Deus quod esset bonum
1:22
And he blessed them, saying: Increase and multiply, and fill the waters of the sea: and let the birds be multiplied upon the earth.
benedixitque eis dicens crescite et multiplicamini et replete aquas maris avesque multiplicentur super terram
1:23
And the evening and morning were the fifth day.
et factum est vespere et mane dies quintus
1:24
And God said: Let the earth bring forth the living creature in its kind, cattle and creeping things, and beasts of the earth, according to their kinds. And it was so done.
dixit quoque Deus producat terra animam viventem in genere suo iumenta et reptilia et bestias terrae secundum species suas factumque est ita
1:25
And God made the beasts of the earth according to their kinds, and cattle, and every thing that creepeth on the earth after its kind. And God saw that it was good.
et fecit Deus bestias terrae iuxta species suas et iumenta et omne reptile terrae in genere suo et vidit Deus quod esset bonum
1:26
And he said: Let us make man to our image and likeness: and let him have dominion over the fishes of the sea, and the fowls of the air, and the beasts, and the whole earth, and every creeping creature that moveth upon the earth.
et ait faciamus hominem ad imaginem et similitudinem nostram et praesit piscibus maris et volatilibus caeli et bestiis universaeque terrae omnique reptili quod movetur in terra
1:27
And God created man to his own image: to the image of God he created him: male and female he created them.
et creavit Deus hominem ad imaginem suam ad imaginem Dei creavit illum masculum et feminam creavit eos
1:28
And God blessed them, saying: Increase and multiply, and fill the earth, and subdue it, and rule over the fishes of the sea, and the fowls of the air, and all living creatures that move upon the earth.
benedixitque illis Deus et ait crescite et multiplicamini et replete terram et subicite eam et dominamini piscibus maris et volatilibus caeli et universis animantibus quae moventur super terram
1:29
And God said: Behold I have given you every herb bearing seed upon the earth, and all trees that have in themselves seed of their own kind, to be your meat:
dixitque Deus ecce dedi vobis omnem herbam adferentem semen super terram et universa ligna quae habent in semet ipsis sementem generis sui ut sint vobis in escam
1:30
And to all beasts of the earth, and to every fowl of the air, and to all that move upon the earth, and wherein there is life, that they may have to feed upon. And it was so done.
et cunctis animantibus terrae omnique volucri caeli et universis quae moventur in terra et in quibus est anima vivens ut habeant ad vescendum et factum est ita
1:31
And God saw all the things that he had made, and they were very good. And the evening and morning were the sixth day.
viditque Deus cuncta quae fecit et erant valde bona et factum est vespere et mane dies sextus
