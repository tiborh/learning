fn main() {
    // full:
    let mut a_vect: Vec<i32> = Vec::new();
    println!("empty vector: {:?}",a_vect);
    a_vect.push(0);
    println!("after push: {:?}",a_vect);
    a_vect.remove(0);
    println!("after removal: {:?}",a_vect);
    // shorter:
    let b_vect = vec![1,2,3,4];
    println!("vector with content: {:?}",b_vect);
    println!("access an elem: {}",b_vect[2]);
    for nu in b_vect.iter() {
	print!("{} ",nu);
    }
    println!("");
    for (i,nu) in b_vect.iter().enumerate() {
	println!("{}: {}",i,nu);
    }
}
