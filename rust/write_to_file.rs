use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = "write_to_file.txt";
    let mut fh = File::create(filename)
	.expect(format!("Can't open the file: {}",filename).as_ref());
    fh.write_all(b"Welcome to the Wonderful World of Rust!\n")
    	.expect(format!("Can't write file: {}",filename).as_ref());
}
