extern crate serde_json;
extern crate serde;
#[macro_use]
extern crate serde_derive;

//use serde_json::Value as JsonValue; // alias
 
#[derive(Serialize, Deserialize)]

struct Person {
    name: String,
    age: u8,
    is_male: bool
}

fn main() {
   let json_str = r#"{
"name": "Alpha",
"age": 26,
"is_male": true
}"#;

    let res = serde_json::from_str(json_str);

    // if res.is_ok() {
    // 	let p: JsonValue = res.unwrap();
    // 	println!("Raw data: {:?}",p);
    // 	println!("The name is {}", p["name"]);
    // 	println!("w/a quotes: {}", p["name"].as_str().unwrap());
    // 	println!("The age is {}", p["age"]);
    // 	println!("Is gender male? {}", p["is_male"]);
    // } else {
    // 	println!("Sorry, could not pass the JSON.");
    // }

    if res.is_ok() {
	let p1: Person = res.unwrap();
	println!("The name is {}", p1.name);
//	println!("w/a quotes: {}", p1["name"].as_str().unwrap());
	println!("The age is {}", p1.age);
	println!("Is gender male? {}", p1.is_male);
    }
}
