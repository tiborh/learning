struct Colour {
    red:   u8,
    green: u8,
    blue:  u8
}

fn main() {
    let bg = Colour {red:127,green:127,blue:127};
    println!("r: {}, g: {}, b: {}",bg.red,bg.green,bg.blue);
}
