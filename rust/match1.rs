use std::env;			// for args
use std::process;		// for exit

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
	println!("Give your name as command line arg.");
	process::exit(1);
    }
    let name = &args[1];
    // to avoid error:
    // expected struct `std::string::String`, found `str`
    match name.as_ref() {
	"Open sesame" => println!("Hello, Ali Baba!"),
	"Sesame open" => println!("Word order!"),
	"Ken sent me" => println!("Good to see you. Larry!"),
	"Tib" => println!("Nice to see you again, Tiberius!"),
	_ => println!("Hello {}!",name)
    }
}
