#![allow(unused_variables)]
fn main() {
    let x = 1234;
    let y = 3456;
    println!("Only x ({}) is used, y is unused.",x);
}
