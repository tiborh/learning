use std::fs::File;
use std::io::prelude::*;


fn main() {
    let filename = "genesis.txt";
    let mut fh = File::open(filename).expect(format!("Can't open the file: {}",filename).as_ref());
    let mut contents = String::new();
    fh.read_to_string(&mut contents).expect(format!("Can't read file: {}",filename).as_ref());

    println!("File Contents: {}",contents);
}
