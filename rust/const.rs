fn main() {
    const UN32:u32 = 1920;
    const PI:f64 = std::f64::consts::PI;

    println!("UN32 is {}, PI is {}",UN32,PI);
}
