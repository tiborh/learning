package main			// package in the directory

import(
	"fmt"			// "format" module
	"log"			// for logging
	"os"			// "Args"
	//"strconv"		// string to float
)

func is_odd(num int) bool {
	return num & 1 != 0
}

func swap(first string, second string) (string,string) {
	return second,first
}

func main() {
	nu_args := len(os.Args[1:])
	args := os.Args[1:]
	log.Printf("Number of args: %d\n",nu_args)
	log.Printf("odd number of args? %t ",is_odd(nu_args))
	if (is_odd(nu_args)) {
		log.Fatal("You need an even number of args.")
	}
	for i := 1; i < nu_args; i+=2 {
		fmt.Printf("%s, %s => ",args[i-1],args[i])
		first,second := swap(args[i-1],args[i])
		fmt.Printf("%s, %s\n",first,second)
	}
}
