package main			// package in the directory

import "fmt"			// "format" module

func main() {
	fmt.Printf("hello, world\n")
	// alternatively: fmt.Println("...")
}
