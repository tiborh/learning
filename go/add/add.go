package main			// package in the directory

import(
	"fmt"			// "format" module
	"log"			// for logging
	"os"			// "Args"
	"strconv"		// string to float
)

// func add(x int, y int) int {
// 	return x + y
// }

func adder(in_arr []string) float64 {
	// sum := 0.0
	var sum float64 = 0.0
	for i, it := range  in_arr[0:] {
		it_num, err := strconv.ParseFloat(it,64)
		if (err != nil) {
			log.Print(err)
		} else {
			log.Printf("%d: %s (type: %T)",i,it,it)
			log.Printf("%d: %f (type: %T)",i,it_num,it_num)
			sum += it_num
		}
	}
	log.Printf("Sum in adder: %f",sum)
	return sum
}

func main() {
	nu_args := len(os.Args[1:])
	fmt.Printf("Number of args: %d\n",nu_args)
	fmt.Printf("Sum of args: %f\n",adder(os.Args[1:]))
}
