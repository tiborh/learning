#!/usr/bin/env clisp
;;or:
;;#!/usr/bin/sbcl --script

(defun ack (m n)
  (cond ((= m 0) (1+ n))
	((= n 0) (ack (1- m) 1))
	(t (ack (1- m) (ack m (1- n))))
	)
  )

;; (load "dtrace.generic")
;; (dtrace func)

(defun print-out (n m)
  (format t "~&(ackermann ~a ~a) is " n m)
  (format t "~a~%"(ack n m))
  )


(write-line "There will be a stack overflow at (ackermann 4 1)")
(loop for i from 0 to 5
     do (loop for j from 0 to 5
	   do (print-out i j)
	     )
     )
