      program test_random_seed
      implicit none
      integer, allocatable :: seed(:)
      integer :: n
C     empty:
      print *,"seed:"
      write (*, *) seed
      call random_seed(size = n)
C     fill up:
      allocate(seed(n))
      call random_seed(get=seed)
      print *,"n:",n
      print *,"seed:"
      write (*, *) seed
      end program test_random_seed
