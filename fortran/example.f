      program foo
C     from Modern Fortran, first example in the book on backside of front cover page
      implicit none
      integer :: a,b,n
      real :: x

      n = 1
      a = 0
      b = 0
      do n = 1, 10
         call add(a,n)
      end do

      end program foo

c$$$      function sum(a,b)
c$$$
c$$$      integer, intent(in) :: a,b
c$$$      integer :: sum
c$$$
c$$$      sum = a + b
c$$$
c$$$      end function sum

      subroutine add(a,b)

      integer, intent(in out) :: a
      integer, intent(in) :: b

      a = a + b
      print *, 'a = ',a

      end subroutine add
      
