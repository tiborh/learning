      program main
      implicit none
      interface
         subroutine collect_integers(int_values, num_values)
         integer, dimension(:), allocatable, intent(out) :: int_values
         integer, intent(in) :: num_values
         end subroutine collect_integers
      end interface
      
      integer, dimension(:), allocatable :: values
      integer :: num_of_vals = 5
      integer :: i

! Call the subroutine to collect integer values
      call collect_integers(values, num_of_vals)

      do i = 1, num_of_vals
         print *,i,":",values(i)
      end do
! Now the 'values' array contains the collected integer values
      
! ... (perform other operations with the collected values)
      
! Deallocate the array when no longer needed
      deallocate(values)
      end program main
      
      subroutine collect_integers(int_values, num_values)
      implicit none
      integer, dimension(:), allocatable, intent(out) :: int_values
      integer, intent(in) :: num_values
      integer :: i

! Allocate the array to store the collected integer values
      allocate(int_values(num_values))

! Collect the integer values into the array
      do i = 1, num_values
! Assume you collect the values from some source, e.g., user input
! Here, we'll just assign dummy values for demonstration
         int_values(i) = i * 2
      end do

! ... (perform other operations with the collected values)

! No need to deallocate the array here
      end subroutine collect_integers
