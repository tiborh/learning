      program dice
      implicit none
      interface
         subroutine cast(casts,faces,nums)
         integer, intent(in) :: casts,faces
         integer, dimension(:), allocatable, intent(out) :: nums
         end subroutine cast
      end interface
      integer :: casts = 5
      integer :: faces = 6
      integer :: argc,i
      integer, dimension(:), allocatable :: nums
      argc = iargc()
      if (argc == 0) then
         call help()
      else if (argc >= 1) then
         call get_num_from_args(casts,1)
         if (argc >= 2) then
            call get_num_from_args(faces,2)
         end if
      end if
      print *,"number of args:     ",argc
      print *,"number of rolls:    ",casts
      print *,"number of die faces:",faces
      call cast(casts,faces,nums)
      do i = 1, casts
         print *,"Roll",i,":",nums(i)
      end do
      deallocate(nums)
      end program dice

      subroutine help()
      character(len=512) :: arg
      call getarg(0, arg)
      print *,trim(arg)," <num_of_casts>"," <num_of_faces>"
      end subroutine help

      subroutine get_num_from_args(num,argnum)
      integer :: num,argnum
      character(len=10) :: arg
      if (iargc() >= argnum) then
         call getarg(argnum,arg)
         read(arg,*)num
      end if
      end subroutine get_num_from_args
      
      subroutine cast(casts,faces,nums)
      implicit none
      integer, intent(in) :: casts,faces
      integer, dimension(:), allocatable, intent(out) :: nums
      integer :: rand_int,counter
      counter = casts
      allocate(nums(casts))
      if (counter > 0 .and. faces >= 2) then
!     see in add_nums
         do while (counter > 0)
            call a_cast(faces,rand_int)
            print *,"rand_int:",rand_int,"(counter:",counter,")"
            nums(counter) = rand_int
            counter = counter - 1
         end do
      end if
      end subroutine cast

      subroutine a_cast(faces,rand_int)
      integer,intent(in)  :: faces
      integer,intent(out) :: rand_int
      real :: rand_val
      call random_number(rand_val)
      rand_int = 1 + int(rand_val * faces)
      end subroutine a_cast
