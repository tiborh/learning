      program main
      
      implicit none
      integer :: i
      real :: x,sum
      character(len=32) :: arg

      sum = 0.0
      
      do i = 1, command_argument_count()
         call get_command_argument(i,arg)
         read(arg,*)x
         sum = sum + x
      end do

      write(*,*)sum
      
      end program main
