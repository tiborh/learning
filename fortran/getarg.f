      program test_getarg
C     https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
      integer :: i
      character(len=32) :: arg

      do i = 1, iargc()
         call getarg(i, arg)
         write (*,*) arg
      end do
      
      end program
      
