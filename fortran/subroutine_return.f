      PROGRAM main
      REAL :: num, square_result
      num = 5.0
      CALL calculate_square(num, square_result)
      PRINT *, "Square of", num, "is", square_result
      END PROGRAM main
      
      SUBROUTINE calculate_square(x, result)
      REAL, INTENT(IN) :: x
      REAL, INTENT(OUT) :: result
      
      result = x * x
      END SUBROUTINE calculate_square
      
