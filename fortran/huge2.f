      program hugenum
      implicit none

!     two byte integer
      integer(kind = 2) :: twobytes
   
!     four byte integer
      integer(kind = 4) :: fourbytes
   
!     eight byte integer
      integer(kind = 8) :: eightbytes
   
!     sixteen byte integer
      integer(kind = 16) :: sixteenbytes
      
!     default integer 
      integer :: deflength

      print *, 'Integers:'
      print *, ' 2 bytes:',huge(twobytes)
      print *, ' 4 bytes:',huge(fourbytes)
      print *, ' 8 bytes:',huge(eightbytes)
      print *, '16 bytes:',huge(sixteenbytes)
      print *, ' default:',huge(deflength)
   
      end program hugenum
