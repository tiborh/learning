      program main
      implicit none

      !surprise: this only worked with interface definition
      interface add
      function add(x,y)
      real, intent(in) :: x,y
      end function
      end interface
      
      integer :: i
      real :: x,res
      character(len=32) :: arg

      res = 0.0
      
      do i = 1, iargc()
         call getarg(i, arg)
         read(arg,*)x
         res = add(res,x)
      end do

      write(*,*)res
      
      end program main

      function add(x,y) result(sum)
      implicit none

      real, intent(in) :: x,y
      real :: sum

      sum = x + y

      end function add
