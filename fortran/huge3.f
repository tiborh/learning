      program hugenum
      implicit none

!     four byte real
      real(kind = 4) :: fourbytes
   
!     eight byte real
      real(kind = 8) :: eightbytes
   
!     sixteen byte real
      real(kind = 16) :: sixteenbytes
      
!     default real 
      real :: deflength

      print *, 'Reals:'
      print *, ' 4 bytes:',tiny(fourbytes),huge(fourbytes)
      print *, ' 8 bytes:',tiny(eightbytes),huge(eightbytes)
      print *, '16 bytes:',tiny(sixteenbytes),huge(sixteenbytes)
      print *, ' default:',tiny(deflength),huge(deflength)
   
      end program hugenum
