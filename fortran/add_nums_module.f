      module algebra
      implicit none 
    
      contains      
      function add(x,y) result(sum)
      implicit none

      real, intent(in) :: x,y
      real :: sum
      
      sum = x + y

      end function add
      end module algebra

      program main
      use algebra
      implicit none
      
      integer :: i
      real :: x,res
      character(len=32) :: arg

      res = 0.0
      
      do i = 1, iargc()
         call getarg(i, arg)
         read(arg,*)x
         res = add(res,x)
      end do

      write(*,*)res
      
      end program main
