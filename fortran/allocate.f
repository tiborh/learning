      PROGRAM dynamic_array_example
      INTEGER, ALLOCATABLE :: numbers(:)
      INTEGER :: size, i

      PRINT *, "Enter the size of the array:"
      READ *, size

      ALLOCATE(numbers(size))

! Assigning values to the array elements
      DO i = 1, size
         numbers(i) = i * 2
      END DO

! Accessing and printing the array elements
      DO i = 1, size
         PRINT *, "Element", i, ":", numbers(i)
      END DO

      DEALLOCATE(numbers)       ! Deallocate the array when no longer needed
      END PROGRAM dynamic_array_example
      
