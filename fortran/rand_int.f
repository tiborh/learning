      PROGRAM random_integer_example
      INTEGER :: random_integer
      REAL :: random_value

      CALL RANDOM_NUMBER(random_value)
      random_integer = 1 + INT(random_value * 100)

      PRINT *, "Random integer:", random_integer
      END PROGRAM random_integer_example
      
