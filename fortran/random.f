      program test_random_number
      integer, parameter :: NUM = 5
      real :: r(NUM,NUM)
!     only from fortran2018:
!     call random_init(.true., .true.)
!     no effect on old compiler:
      call random_seed()
      call random_number(r)
      print *,r
c$$$      do i = 1,NUM
c$$$         do j = 1,NUM
c$$$            print *,r(i,j)
c$$$         end do
c$$$      end do
      end program
      
