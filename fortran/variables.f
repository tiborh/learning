!     source:
!     https://www.tutorialspoint.com/fortran/fortran_variables.htm
      program variableTesting
      implicit none

!     declaring variables
      integer :: total      
      real :: average 
      complex :: cx  
      logical :: done 
      character(len=80) :: message ! a string of 80 characters
      
!     assigning values
      total = 20000  
      average = 1666.67   
      done = .true.   
      message = "A big Hello from Tutorials Point" 
      cx = (3.0, 5.0)           ! cx = 3.0 + 5.0i

      Print *, 'Integer:',total
      Print *, 'Real:',average
      Print *, 'Complex:',cx
      Print *, 'Logical:',done
      Print *, 'Character: ',message
      
      end program variableTesting
      
