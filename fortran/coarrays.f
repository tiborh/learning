      program hello_coarrays
      implicit none
      
      integer :: a[*]
      integer :: i
      
      a = this_image()
!Each image declares a local
!copy of an integer “a.”
!Each image assigns its
!number (1, 2, 3, etc.) to “a.”
!Only image 1 will
!enter this if block.
!Iterates from 1 to the
      if (this_image() == 1) then
!total number of images
         do i = 1, num_images()
            print *, 'Value on image', i, 'is', a[i]
!For each remote image,
         end do
!image 1 will get the value
      end if
!of “a” on that image and
!print it to the screen.
      
      end program hello_coarrays
