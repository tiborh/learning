module PromptModule (
  promptFunction
  ) where

import System.Directory
import System.FilePath

promptFunction :: [String] -> Int -> IO String
promptFunction _ _ = do
    cwd <- System.Directory.getCurrentDirectory
    return ("[ghci " ++ System.FilePath.takeBaseName cwd ++ "]> ")
