if you do not have a .ghci file yet, 
    copy the ghci dir and the .ghci file into your home (~)
else
    you can copy the dir but append the .ghci file to yours (and see if there are no collisions)

when you start up ghci, the prompt should display correctly
(tip: if you precompile the module, it will load the .o file in place of the interpreted version)
