import System.Random

gen_rand_int :: Int -> Int -> IO Int
gen_rand_int from to = randomRIO (from, to)

main = do
  rand_int <- gen_rand_int 1 100
  print rand_int
