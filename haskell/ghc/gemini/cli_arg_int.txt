how do I parse a command line argument as an integer? 

You parse a command-line argument as an integer in Haskell using the `read` function along with proper error handling. Here's a breakdown:

**1. Getting the command-line arguments:**

First, you need to get the command-line arguments using `getArgs` from `System.Environment`:

```haskell
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    -- ... (parsing logic goes here)
```

**2. Parsing the argument as an integer using `reads`:**

The `reads` function is the recommended way to parse strings because it handles potential parsing errors gracefully. `reads` has the type `Read a => String -> [(a, String)]`. It returns a list of tuples. Each tuple contains a parsed value and the remaining unparsed part of the string. A successful parse will have an empty string as the remainder.

```haskell
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    case args of
        [numStr] -> case reads numStr of
            [(num, "")] -> do  -- Successful parse
                print ("You entered the number: " ++ show num)
                -- Use the parsed integer 'num' here
            _ -> putStrLn "Invalid input: Please enter a valid integer."
        _ -> putStrLn "Usage: program <integer>"
```

**Explanation:**

*   `case args of [numStr] -> ...`: This pattern matches a list containing exactly one argument, which is bound to the `numStr` variable.
*   `case reads numStr of ...`: This attempts to parse `numStr` as an integer.
*   `[(num, "")] -> ...`: This pattern matches a successful parse. `num` is the parsed integer, and `""` indicates that the entire string was parsed.
*   `_ -> putStrLn "Invalid input..."`: This handles cases where the string could not be parsed as an integer (e.g., if the user enters "abc" or "12.3").

**3. Handling other cases (no arguments or multiple arguments):**

The `case args of` statement also handles the cases where the user provides no arguments or more than one argument.

**Complete Example:**

```haskell
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    case args of
        [numStr] -> case reads numStr of
            [(num, "")] -> do
                let doubledNum = num * 2
                print ("The doubled number is: " ++ show doubledNum)
            _ -> putStrLn "Invalid input: Please enter a valid integer."
        [] -> putStrLn "Usage: program <integer>"
        _ -> putStrLn "Usage: program <integer> (Only one argument is accepted)"
```

**Example Usage:**

If you compile this program to `myprogram`:

*   `./myprogram 123`: Output: `The doubled number is: 246`
*   `./myprogram abc`: Output: `Invalid input: Please enter a valid integer.`
*   `./myprogram`: Output: `Usage: program <integer>`
*   `./myprogram 1 2`: Output: `Usage: program <integer> (Only one argument is accepted)`

**Using `readMaybe` (from `Text.Read`):**

A more modern and often preferred way is to use `readMaybe` from the `Text.Read` module. It returns a `Maybe Int`, which is `Just Int` on success and `Nothing` on failure. This avoids the need for nested `case` statements:

```haskell
import System.Environment
import Text.Read (readMaybe)
import Data.Maybe (fromMaybe)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [numStr] -> case readMaybe numStr of
            Just num -> do
                let doubledNum = num * 2
                print ("The doubled number is: " ++ show doubledNum)
            Nothing -> putStrLn "Invalid input: Please enter a valid integer."
        _ -> putStrLn "Usage: program <integer>"
```

Or using `fromMaybe` to provide a default value:

```haskell
import System.Environment
import Text.Read (readMaybe)
import Data.Maybe (fromMaybe)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [numStr] -> let num = fromMaybe 0 (readMaybe numStr) in do
                let doubledNum = num * 2
                print ("The doubled number is: " ++ show doubledNum)
        _ -> putStrLn "Usage: program <integer>"
```

This version is more concise and easier to read. Using `readMaybe` is generally the recommended approach for parsing command-line arguments in Haskell.
