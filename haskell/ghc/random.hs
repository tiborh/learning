import System.Random

gen_rand_int :: IO Int
gen_rand_int = randomRIO (1, 100) -- Generates a random Int between 1 and 100 (inclusive)

main = do
  rand_int <- gen_rand_int
  print rand_int
